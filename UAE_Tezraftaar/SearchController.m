//
//  SearchController.m
//
//
//  Created by Abdul Basit on 06/09/2017.
//  Copyright © 2017 Abdul Basit. All rights reserved.
//

#import "SearchController.h"
#import "GlobalStaticClass.h"

@interface SearchController ()
@end

@implementation SearchController{
    
    NSArray *searchResults;
    GlobalStaticClass *gblclass;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    gblclass = [GlobalStaticClass getInstance];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    self.headingLabel.text = [NSString stringWithFormat:@"Select %@", _type.capitalizedString];
    //self.searchItems = [[NSArray alloc] initWithObjects:@"abdul basit",@"abdul Qadir",@"Anas Rizvi",@"Ali Asghar",@"Ibrahim",@"Jahangir",@"SAfi",@"Saim Zubairi", nil];
    
}


#pragma mark - SessionOut

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:@"cell"];
    }
    
    
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor colorWithRed:(0/255.5) green:(93/255.f) blue:(146/255.f) alpha:1];
        tableView.backgroundColor = [UIColor colorWithRed:(0/255.5) green:(93/255.f) blue:(146/255.f) alpha:1];
    } else {
        
        UILabel *label = (UILabel*)[cell viewWithTag:2];
        label.text = self.searchItems[indexPath.row];
        label.textColor = [UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:1];
        
        if ([gblclass.selected_city isEqualToString:self.searchItems[indexPath.row]] || [gblclass.branch_name_onbording isEqualToString:self.searchItems[indexPath.row]]) {
            imageView.image = [UIImage imageNamed:@"check_select_acct.png"];
        } else {
            imageView.image = [UIImage imageNamed:@"Non.png"];
            
        }
        [cell.contentView addSubview:imageView];
        
    }
    
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
        
    } else {
        return self.searchItems.count;
    }
    
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        NSInteger result =  [self.searchItems indexOfObject:searchResults[indexPath.row]];
        [self.delegate didSelectCityOrBranch:result type:self.type];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    else {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        imageView.image = [UIImage imageNamed:@"check_select_acct.png"];
        [tableView reloadData];
        
        NSInteger result = indexPath.row;
        [self.delegate didSelectCityOrBranch:result type:self.type];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    searchResults = [self.searchItems filteredArrayUsingPredicate:resultPredicate];
}


-(BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showRecipeDetail"]) {
        NSIndexPath *indexPath = nil;
        
    }
}



//func willPresentSearchController(searchController: UISearchController) {
//    navigationController?.navigationBar.translucent = true
//}
//
//func willDismissSearchController(searchController: UISearchController) {
//    navigationController?.navigationBar.translucent = false
//}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)back_Action:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)cancel_Action:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

