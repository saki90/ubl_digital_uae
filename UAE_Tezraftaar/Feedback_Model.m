//
//  Feedback_Model.m
//
//  Created by   on 14/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Feedback_Model.h"
#import "FResponse.h"


NSString *const kFeedback_ModelFResponse = @"FResponse";


@interface Feedback_Model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Feedback_Model

@synthesize fResponse = _fResponse;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedFResponse = [dict objectForKey:kFeedback_ModelFResponse];
    NSMutableArray *parsedFResponse = [NSMutableArray array];
    if ([receivedFResponse isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedFResponse) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedFResponse addObject:[FResponse modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedFResponse isKindOfClass:[NSDictionary class]]) {
       [parsedFResponse addObject:[FResponse modelObjectWithDictionary:(NSDictionary *)receivedFResponse]];
    }

    self.fResponse = [NSArray arrayWithArray:parsedFResponse];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForFResponse = [NSMutableArray array];
    for (NSObject *subArrayObject in self.fResponse) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForFResponse addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForFResponse addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForFResponse] forKey:kFeedback_ModelFResponse];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.fResponse = [aDecoder decodeObjectForKey:kFeedback_ModelFResponse];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_fResponse forKey:kFeedback_ModelFResponse];
}

- (id)copyWithZone:(NSZone *)zone
{
    Feedback_Model *copy = [[Feedback_Model alloc] init];
    
    if (copy) {

        copy.fResponse = [self.fResponse copyWithZone:zone];
    }
    
    return copy;
}


@end
