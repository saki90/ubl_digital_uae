//
//  Create_MPin_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 15/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Create_MPin_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "FBEncryptorAES.h"
#import "Encrypt.h"

@interface Create_MPin_VC ()<NSURLConnectionDataDelegate>
{
     UIAlertController* alert;
    
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
//    UIStoryboard *storyboard;
//    UIViewController *vc;
    NSString *responsecode;
    
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    Encrypt *encrypt;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Create_MPin_VC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    
    txt_pin.delegate=self;
  //**  [self Play_bg_video];
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}



- (void)viewDidAppear:(BOOL)animated
{
 //**   [super viewDidAppear:animated];
 //**   [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
 //**   AVPlayerItem *p = [notification object];
 //**   [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
  //**  [self.avplayer play];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_submit:(id)sender
{
    
    if ([txt_pin.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter MPIN" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    else
    {
//        [self UserMobMpinReg:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            [self SSL_Call];
        }
    }
}

-(void) UserMobMpinReg:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
   [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    //NSLog(@"%@",gblclass.signup_relationshipid);
    
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"7c4d5f254b9e6edd",txt_pin.text,@"1234",[GlobalStaticClass getPublicIp],@"asdf", nil]
//                                                          forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
    
    
     //NSLog(@"%@", gblclass.user_id);
     //NSLog(@"%@",gblclass.signup_userid);
     //NSLog(@"%@",gblclass.Exis_relation_id);
     //NSLog(@"%@",gblclass.Udid);
     //NSLog(@"%@",txt_pin.text);
    
    
    NSDictionary *dictparam;
    
    if ([gblclass.From_Exis_login isEqualToString:@"1"])
    {
//        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1234",[GlobalStaticClass getPublicIp],@"abv",gblclass.Exis_relation_id,str_Otp,gblclass.Udid, nil] forKeys:[NSArray arrayWithObjects:@"strSessionId",@"IP",@"hCode",@"strRelationShipId",@"strActivationCode",@"strDeviceID", nil]];
//        
      
        //NSLog(@"%@",gblclass.signup_userid);
        //NSLog(@"%@",gblclass.signup_relationshipid);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",txt_pin.text);
        
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                      [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                      [encrypt encrypt_Data:@"asdf"],
                      [encrypt encrypt_Data:gblclass.signup_userid],
                      [encrypt encrypt_Data:gblclass.Exis_relation_id],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:txt_pin.text],
                      [encrypt encrypt_Data:@"0"], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"strUserId",
                                                                       @"StrRelationShipId",
                                                                       @"StrDeviceId",
                                                                       @"mPin",
                                                                       @"IsTouchLoign", nil]];
        
    }
    else
    {
//        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1234",[GlobalStaticClass getPublicIp],@"abv",gblclass.signup_relationshipid,str_Otp,gblclass.Udid, nil] forKeys:[NSArray arrayWithObjects:@"strSessionId",@"IP",@"hCode",@"strRelationShipId",@"strActivationCode",@"strDeviceID", nil]];
//        

         dictparam = [NSDictionary dictionaryWithObjects:
                      [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                       [encrypt encrypt_Data:@"asdf"],
                       [encrypt encrypt_Data:gblclass.signup_userid],
                       [encrypt encrypt_Data:gblclass.signup_relationshipid],
                       [encrypt encrypt_Data:gblclass.Udid],
                       [encrypt encrypt_Data:txt_pin.text],
                       [encrypt encrypt_Data:@"0"], nil]
                      
                                                 forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                          @"IP",
                                                          @"hCode",
                                                          @"strUserId",
                                                          @"StrRelationShipId",
                                                          @"StrDeviceId",
                                                          @"mPin",
                                                          @"IsTouchLoign", nil]];
        
    }

    
    
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1234",[GlobalStaticClass getPublicIp],@"asdf",gblclass.signup_userid,gblclass.signup_relationshipid,gblclass.Udid,txt_pin.text,@"0", nil]
//                                                          forKeys:[NSArray arrayWithObjects:@"strSessionId",@"IP",@"hCode",@"strUserId",@"StrRelationShipId",@"StrDeviceId",@"mPin",@"IsTouchLoign", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"UserMobMpinReg" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
  //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  [arr_que addObject:(NSDictionary *)[dic objectForKey:@"dt2"]];
              //for(dic in generalarray){
              responsecode=[dic objectForKey:@"Response"];
              
              NSString* exception = [dic objectForKey:@"strReturnMessage"];
              
              
              if ((responsecode=@"1"))
              {
                  [hud hideAnimated:YES];
                  
                  NSString *valueToSave = @"1";
                  [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"Mpin"];
                  
                  
                  NSString*  saki = [FBEncryptorAES encryptBase64String:txt_pin.text
                                                   keyString:FBEncryptorAES_String_Key  //key
                                               separateLines:[self.separateline isOn]];
                  
                  
                  NSString* st= [FBEncryptorAES decryptBase64String:@"asdf" keyString:FBEncryptorAES_String_Key];
                  
                  
                  //NSLog(@"encrypted: %@", saki);
                  
                  
                  [[NSUserDefaults standardUserDefaults] setObject:saki forKey:@"Mpin-pw"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  
                  gblclass.first_time_login_chck=@"1";
                  
                  gblclass.touch_id_Mpin=txt_pin.text;
                  
                   mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                   vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Mpin_login"];
                   [self presentViewController:vc animated:NO completion:nil];
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
              }
              else
              {
                  [self showAlert:exception :@"Attention"];
              }
              
              
              //NSLog(@"Response %@",[dic objectForKey:@"ID"]);
              
              [hud hideAnimated:YES];
              // [_generaltableview reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                       message:exception
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                       [alert1 show];
                   });
    //[alert release];
}



- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    BOOL stringIsValid;
    NSInteger MAX_DIGITS=4;
    
    if ([theTextField isEqual:txt_pin])
    {
        
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    
    if ([theTextField isEqual:txt_pin])
    {
        txt_pin.text=formattedOutput;
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_pin resignFirstResponder];
}

#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_back:(id)sender
{
    gblclass.user_login_name=@"";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_feature:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    
     [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_offer:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    
     [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_find_us:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    
     [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_faq:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
     [self presentViewController:vc animated:NO completion:nil];
    }
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
   [hud showAnimated:YES];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    {
        
        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            [self UserMobMpinReg:@""];
            
             [self.connection cancel];
        }
        else
        {
             [self.connection cancel];
            
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                            message:@"Authentication Failed."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                        [alertView show];
            
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self.connection cancel];
        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        
        gblclass.ssl_pin_check=@"0";
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"SSL Not Verified." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        return;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    return [envValue boolValue];
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



@end
