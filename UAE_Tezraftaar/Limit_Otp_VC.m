//
//  Limit_Otp_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 11/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Limit_Otp_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Limit_Otp_VC ()
{
    
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString *responsecode;
    NSString* chk_ssl;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_Otp;
    int txt_nam;
    NSString* str_pw;
    UIAlertController* alert;
    NSString* str_chk_msg;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Limit_Otp_VC
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    ssl_count = @"0";
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    
    txt1.delegate=self;
    txt2.delegate=self;
    txt3.delegate=self;
    txt4.delegate=self;
    txt5.delegate=self;
    txt6.delegate=self;
    
    NSString* subStr;
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below",subStr];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}


-(IBAction)btn_submit:(id)sender
{
    [txt1 resignFirstResponder];
    [txt2 resignFirstResponder];
    [txt3 resignFirstResponder];
    [txt4 resignFirstResponder];
    [txt5 resignFirstResponder];
    [txt6 resignFirstResponder];
    
    if ([txt1.text isEqualToString:@""] || [txt2.text isEqualToString:@""] || [txt3.text isEqualToString:@""] || [txt4.text isEqualToString:@""] || [txt5.text isEqualToString:@""] || [txt6.text isEqualToString:@""])
    {
        
        [self custom_alert:@"please enter 6 digits OTP that has been sent to your mobile number" :@"0"];
        return;
    }
    else
    {
        
        // [self checkinternet];
        // if (netAvailable)
        // {
        // [self Change_daily_limit:@""];
        // }
    }
    
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void) Change_QR_Code_TranLimit:(NSString *)strIndustry
{
    
    @try {
        
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        UIDevice *deviceInfo = [UIDevice currentDevice];
        
        //NSLog(@"Device name:  %@", deviceInfo.name);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.relation_id],
                                    [encrypt encrypt_Data:gblclass.amt_mp_limit],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],nil]
                                                              forKeys:[NSArray arrayWithObjects:@"userId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"relationship_Id",
                                                                       @"limitAmount",
                                                                       @"userProvidedOtp",
                                                                       @"strDeviceid",
                                                                       @"strToken",nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ChangeQRCodeTranLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  str_chk_msg = @"1";
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      str_chk_msg = @"2";
                      
                      [self slide_right];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:[dic objectForKey:@"OutstrReturnMessage"]
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                      [alert1 show];
                      
                  }
                  else
                  {
                      txt1.text=@"";
                      txt2.text=@"";
                      txt3.text=@"";
                      txt4.text=@"";
                      txt5.text=@"";
                      txt6.text=@"";
                      
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                      
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later" :@"0"];
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ( [chk_ssl isEqualToString:@"submit"])
        {
            chk_ssl=@"";
            [self Change_QR_Code_TranLimit:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"re_otp"])
        {
            chk_ssl=@"";
            [self Re_GenerateOTP:@""];
        }
        
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ( [chk_ssl isEqualToString:@"submit"])
    {
        chk_ssl=@"";
        [self Change_QR_Code_TranLimit:@""];
    }
    else if ([chk_ssl isEqualToString:@"re_otp"])
    {
        chk_ssl=@"";
        [self Re_GenerateOTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
         
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
   // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////





-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSUInteger MAX_DIGITS = 1;
    
    NSUInteger maxLength = 1;
    
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text length :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    
    NSString *str1 = @"hello ";
    str1 = [str1 stringByAppendingString:str];
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            if (textField.tag == 6)
            {
                [self.view endEditing:YES];
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                txt6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt1.text];
                str_Otp = [str_Otp stringByAppendingString:txt2.text];
                str_Otp = [str_Otp stringByAppendingString:txt3.text];
                str_Otp = [str_Otp stringByAppendingString:txt4.text];
                str_Otp = [str_Otp stringByAppendingString:txt5.text];
                str_Otp = [str_Otp stringByAppendingString:txt6.text];
                
                
                chk_ssl=@"submit";
                [self SSL_Call];
                
                
                return 0;
            }
            
            
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
        
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    
    if ([str_chk_msg isEqualToString:@"1"])
    {
        
        if (buttonIndex == 0)
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
            //[self mob_App_Logout:@""];
            
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 1)
        {
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:vc animated:YES completion:nil];
        }
        
    }
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt1 resignFirstResponder];
    [txt2 resignFirstResponder];
    [txt3 resignFirstResponder];
    [txt4 resignFirstResponder];
    [txt5 resignFirstResponder];
    [txt6 resignFirstResponder];
}


-(IBAction)btn_re_generate_otp:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"re_otp";
        [self SSL_Call];
    }
    
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
    
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
  
        
    }
    
    
}


-(void) Re_GenerateOTP:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString* str_acct_no=[NSString stringWithFormat:@"%@%@",gblclass.user_id,gblclass.relation_id];
    
    
    //    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],@"",str_acct_no,@"QRCODEDAILYLIMIT",@"Addition",@"300",@"QRCODEDAILYLIMIT",@"SMS",@"QRCODEDAILYLIMIT",gblclass.Udid,gblclass.token,@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil]];
    
    
  //  NSLog(@"%@",gblclass.arr_re_genrte_OTP_additn);
    
    
    //limit_QRCODE
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:0 ]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:6]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:9]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:13]], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    //NSLog(@"%@",dictparam);
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  //gblclass.check_review_acct_type=@"QR_Code";
                  
                  gblclass.Is_Otp_Required=@"1";
                  
                  //                  CATransition *transition = [ CATransition animation];
                  //                  transition.duration = 0.3;
                  //                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  //                  transition.type = kCATransitionPush;
                  //                  transition.subtype = kCATransitionFromRight;
                  //                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  txt1.text = @"";
                  txt2.text = @"";
                  txt3.text = @"";
                  txt4.text = @"";
                  txt5.text = @"";
                  txt6.text = @"";
            
                  
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
         {
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}



@end

