//
//  UIDeviceExtension.swift
//  UIDevice Specs
//
//  Created by Awais Khalid on 11/07/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//




import Foundation
import UIKit




extension UIDevice{
    
    
    
    static var CurrentDeviceHeight: CGFloat{
        struct Singleton{
            static let height = UIScreen.main.bounds.size.height
        }
        return Singleton.height
        
    }
    static var CurrentDeviceWidth: CGFloat{
        struct Singleton{
            static let width = UIScreen.main.bounds.size.width
        }
        return Singleton.width
    }
    
    static func isPhone() ->Bool{
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    static func isPad() ->Bool{
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static func isTV() ->Bool{
        return UIDevice.current.userInterfaceIdiom == .tv
    }
    
    
    
    enum Heights: CGFloat {
        case Inches_3_5 = 480   //iphone 4
        case Inches_4 = 568     //iphone 5
        case Inches_4_7 = 667   //iphone 6
        case Inches_5_5 = 736   //iphone 6plus,7plus
    }
    
    static func isSize(height: Heights) -> Bool {
        return CurrentDeviceHeight == height.rawValue
    }
    
    static func isSizeOrLarger(height: Heights) -> Bool {
        return CurrentDeviceHeight >= height.rawValue
    }
    
    static func isSizeOrSmaller(height: Heights) -> Bool {
        return CurrentDeviceHeight <= height.rawValue
    }
    
    
    
    // MARK: Retina Check
    
    
    
    
    
    
    
    
    
    
}
