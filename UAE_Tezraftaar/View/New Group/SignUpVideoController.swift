//
//  VideoBackgroundController.swift
//  UBL Digital
//
//  Created by Awais Khalid on 28/09/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//

import UIKit
import AVFoundation

//class SignUpVideoController: UIViewController {
//
//    var avPlayer: AVPlayer!
//    var avPlayerLayer: AVPlayerLayer!
//    var paused: Bool = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        let theURL = Bundle.main.url(forResource:"Signup Video - V5.1", withExtension: ".mp4")
//
//        avPlayer = AVPlayer(url: theURL!)
//        avPlayerLayer = AVPlayerLayer(player: avPlayer)
//        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
//        avPlayer.volume = 0
//        avPlayer.actionAtItemEnd = .none
//
//        avPlayerLayer.frame = view.layer.bounds
//        view.backgroundColor = .clear
//        view.layer.insertSublayer(avPlayerLayer, at: 0)
//
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(playerItemDidReachEnd(notification:)),
//                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
//                                               object: avPlayer.currentItem)
//    }
//    @objc func playerItemDidReachEnd(notification: Notification) {
//        let p: AVPlayerItem = notification.object as! AVPlayerItem
//        p.seek(to: kCMTimeZero)
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        avPlayer.play()
//        paused = false
//    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        avPlayer.pause()
//        paused = true
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
class SignUpVideoController: UIViewController{
    
    var gradientLayer: CAGradientLayer!
    override func viewDidLoad() {
        createGradientLayer()
    }
    func createGradientLayer(){
        
        gradientLayer = CAGradientLayer()
        //gradientLayer.locations = []
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 0).cgColor,UIColor(red: 0/255, green: 110/255, blue: 170/255, alpha: 1).cgColor]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
    }
    
    
}
