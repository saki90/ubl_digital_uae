//
//  UIViewExtension.swift
//  Test
//
//  Created by Awais Khalid on 11/10/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//

import Foundation
import UIKit

var circleLayer: CAShapeLayer!
var layerWithGradient: CAGradientLayer!

extension UIView{
    
    func addCircleView(borderWidth: CGFloat,color: CGColor){
        
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.

        let startAngle = -CGFloat.pi/2
        let endAngle = CGFloat.pi + CGFloat.pi/2

        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 10)/2, startAngle: startAngle, endAngle: endAngle, clockwise: true)

        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer = CAShapeLayer()
        circleLayer.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)

        circleLayer.masksToBounds = true
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = color
        circleLayer.lineWidth = borderWidth;

        // Add the circleLayer to the view's layer's sublayers
        layer.addSublayer(circleLayer)
        
   
        
        
        
    }
    
    func animateCircle(duration: TimeInterval, divideCircleBy: CGFloat) {
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1/divideCircleBy
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1/divideCircleBy
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
    func gradientLayer(firstColor: CGColor, secondColor: CGColor){
        
        layerWithGradient = CAGradientLayer()
        //layerWithGradient.locations = []
        
        layerWithGradient.frame = self.bounds
        layerWithGradient.colors = [firstColor,secondColor]
        self.layer.insertSublayer(layerWithGradient, at: 0)
        
        
    }
    
    
    
    
}
