//
//  UIButtonExtension.swift
//  UBL Digital
//
//  Created by Awais Khalid on 25/08/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//

import Foundation
import UIKit


extension UIButton{
    
    func resizingFont(){
        if(UIDevice.CurrentDeviceHeight == 480){
            
            self.titleLabel?.minimumScaleFactor = 0.5
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            self.titleLabel?.font = UIFont(name: (self.titleLabel?.font.fontName)!, size: (self.titleLabel?.font.pointSize)! / 1.3)
            
            
        }else if(UIDevice.CurrentDeviceHeight == 568 ){
            
            self.titleLabel?.minimumScaleFactor = 0.5
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            self.titleLabel?.font = UIFont(name: (self.titleLabel?.font.fontName)!, size: (self.titleLabel?.font.pointSize)! / 1.2)
            
            
        }else if(UIDevice.CurrentDeviceHeight == 667){
            
            self.titleLabel?.minimumScaleFactor = 0.5
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            self.titleLabel?.font = UIFont(name: (self.titleLabel?.font.fontName)!, size: (self.titleLabel?.font.pointSize)! / 1.1)
           
            
        }
        
        
    }
    
//    func changingButtonImageWhenPressed(previousImage: UIImage, imageAfterPressing: UIImage,sender: UIButton){
//
//        if sender.currentImage == UIImage.init(named: previousImage.) {
//
//            sender.setImage(imageAfterPressing, for: .normal)
//            sender.backgroundColor = UIColor.clear
//
//        }else if (sender.currentImage == imageAfterPressing){
//
//            sender.setImage( UIImage(named:previousImage), for: .normal)
//            sender.backgroundColor = UIColor.white
//
//        }
//
//    }
    
    
}
