//
//  UITextFieldExtension.swift
//  LMS
//
//  Created by Awais Khalid on 12/07/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//

import Foundation
import UIKit






extension UITextField {
    func setBottomBorder(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        
        border.frame = CGRect.init(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        
        
        border.borderColor = UIColor(red: red, green: green, blue: blue, alpha: alpha).cgColor
        border.borderWidth = borderWidth
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
    
    func resizingFont(){
        if(UIDevice.CurrentDeviceHeight == 480){
            self.adjustsFontSizeToFitWidth = true
            
            self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! / 1.3)
            
            
        }else if(UIDevice.CurrentDeviceHeight == 568 ){
            
            self.adjustsFontSizeToFitWidth = true
            self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! / 1.2)
            
        }else if(UIDevice.CurrentDeviceHeight == 667){
            
            
            self.adjustsFontSizeToFitWidth = true
            self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! / 1.1)
            
        }
        
        
    }
}


//case Inches_3_5 = 480   //iphone 4
//case Inches_4 = 568     //iphone 5
//case Inches_4_7 = 667   //iphone 6
//case Inches_5_5 = 736   //iphone 6plus,7plus

