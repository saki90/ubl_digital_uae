//
//  UIViewExtension.swift
//  TouchId
//
//  Created by awais khalid on 30/11/2017.
//  Copyright © 2017 awais khalid. All rights reserved.
//

import Foundation
import UIKit


//MARK: -Variables
private let qrButton: UIButton = {
   
    let button = UIButton()
    let image = UIImage(named: "QR")
    button.setImage(image, for: .normal)
    button.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
}()
private let offersButton: UIButton = {
    
    let button = UIButton()
    let image = UIImage(named: "Offers")
    button.setImage(image, for: .normal)
    button.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
}()
private let locateUsButton: UIButton = {
    
    let button = UIButton()
    let image = UIImage(named: "Locate-Us")
    button.setImage(image, for: .normal)
    button.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
}()
private let faqsButton: UIButton = {
    
    let button = UIButton()
    let image = UIImage(named: "FAQs")
    button.setImage(image, for: .normal)
    button.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
}()

//Labels
private let qrLabel: UILabel = {
    
    let label = UILabel()
    label.text = "QR"
    label.textAlignment = NSTextAlignment.center
    label.font = UIFont(name: "Aspira-Regular", size: 11)
    label.textColor = UIColor.white
    
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
}()
private let offersLabel: UILabel = {
    
    let label = UILabel()
    label.text = "Offers"
    label.textAlignment = NSTextAlignment.center
    label.font = UIFont(name: "Aspira-Regular", size: 11)
    label.textColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
}()
private let locateUsLabel: UILabel = {
    
    let label = UILabel()
    label.text = "Locate Us"
    label.textAlignment = NSTextAlignment.center
    label.font = UIFont(name: "Aspira-Regular", size: 11)
    label.textColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
}()
private let faqsLabel: UILabel = {
    
    let label = UILabel()
    label.text = "FAQs"
    label.textAlignment = NSTextAlignment.center
    label.font = UIFont(name: "Aspira-Regular", size: 11)
    label.textColor = UIColor.white
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
}()

//Extension
extension UIView{
    
    func insertTabBar(){
        
        //UIButton
        self.addSubview(qrButton)
        self.addSubview(offersButton)
        self.addSubview(locateUsButton)
        self.addSubview(faqsButton)
        
        //UIlabels
        self.addSubview(qrLabel)
        self.addSubview(offersLabel)
        self.addSubview(locateUsLabel)
        self.addSubview(faqsLabel)
        
        setupLayout()
        
    }
    
    private func setupLayout(){
                        // Buttons
        //Adding contraints to QRButton
        qrButton.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        qrButton.heightAnchor.constraint(equalToConstant: 54).isActive = true
        qrButton.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        qrButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        qrButton.addTarget(self, action: #selector(generateAlert), for: .touchUpInside)
        
        //Adding contraints to OffersButton
        offersButton.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        offersButton.heightAnchor.constraint(equalToConstant: 54).isActive = true
        offersButton.leadingAnchor.constraint(equalTo: qrButton.trailingAnchor).isActive = true
        offersButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        offersButton.addTarget(self, action: #selector(generateAlert), for: .touchUpInside)
        
        //Adding contraints to LocateUsButton
        locateUsButton.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        locateUsButton.heightAnchor.constraint(equalToConstant: 54).isActive = true
        locateUsButton.leadingAnchor.constraint(equalTo: offersButton.trailingAnchor).isActive = true
        locateUsButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        locateUsButton.addTarget(self, action: #selector(generateAlert), for: .touchUpInside)

        //Adding contraints to QRButton
        faqsButton.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        faqsButton.heightAnchor.constraint(equalToConstant: 54).isActive = true
        faqsButton.leadingAnchor.constraint(equalTo: locateUsButton.trailingAnchor).isActive = true
        faqsButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        faqsButton.addTarget(self, action: #selector(generateAlert), for: .touchUpInside)
        
        
        
        
            // Labels
        //Adding contraints to QRLabel
        qrLabel.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        qrLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        qrLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        qrLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        //Adding contraints to OffersLabel
        offersLabel.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        offersLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        offersLabel.leadingAnchor.constraint(equalTo: qrLabel.trailingAnchor).isActive = true
        offersLabel.centerYAnchor.constraint(equalTo: qrLabel.centerYAnchor).isActive = true

        //Adding contraints to LocateUsLabel
        locateUsLabel.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        locateUsLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        locateUsLabel.leadingAnchor.constraint(equalTo: offersLabel.trailingAnchor).isActive = true
        locateUsLabel.centerYAnchor.constraint(equalTo: offersLabel.centerYAnchor).isActive = true


        //Adding contraints to FAQsLabel
        faqsLabel.widthAnchor.constraint(equalToConstant: frame.width / 4).isActive = true
        faqsLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        faqsLabel.leadingAnchor.constraint(equalTo: locateUsLabel.trailingAnchor).isActive = true
        faqsLabel.centerYAnchor.constraint(equalTo: locateUsLabel.centerYAnchor).isActive = true
        
    }
    @objc func generateAlert(){
        
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
}



