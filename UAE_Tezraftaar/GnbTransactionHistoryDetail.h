//
//  GnbTransactionHistoryDetail.h
//
//  Created by   on 23/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbTransactionHistoryDetail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *lblToAccNick;
@property (nonatomic, assign) id lblSerialNo;
@property (nonatomic, assign) id dlblReceivedBy;
@property (nonatomic, strong) NSString *lblToAcc;
@property (nonatomic, strong) NSString *dlblStatus;
@property (nonatomic, strong) NSString *lblFromAcc;
@property (nonatomic, strong) NSString *lblAmount;
@property (nonatomic, strong) NSString *dlblTranID;
@property (nonatomic, strong) NSString *dlblAmount;
@property (nonatomic, assign) id lblFCPVouchertId;
@property (nonatomic, assign) id lblReceivedBy;
@property (nonatomic, assign) id dlblDepositedTo;
@property (nonatomic, assign) id dlblBank;
@property (nonatomic, strong) NSString *dlblTranType;
@property (nonatomic, assign) id lblBank;
@property (nonatomic, assign) id dlblFCPVouchertId;
@property (nonatomic, assign) id lblDepositedTo;
@property (nonatomic, strong) NSString *dlblToAcc;
@property (nonatomic, strong) NSString *lblTranType;
@property (nonatomic, strong) NSString *lblStatus;
@property (nonatomic, assign) id dlblVoucherExpDate;
@property (nonatomic, strong) NSString *lblTranID;
@property (nonatomic, strong) NSString *lblComments;
@property (nonatomic, strong) NSString *dlblComments;
@property (nonatomic, assign) id lblVoucherExpDate;
@property (nonatomic, assign) id dlblSerialNo;
@property (nonatomic, strong) NSString *lblInitByCustomer;
@property (nonatomic, strong) NSString *dlblInitByCustomer;
@property (nonatomic, strong) NSString *dlblToAccNick;
@property (nonatomic, strong) NSString *dlblFromAcc;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
