//
//  Table.h
//
//  Created by   on 03/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface loadutilitycompany : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double tTID;
@property (nonatomic, assign) id mERCHANTID;
@property (nonatomic, strong) NSString *cOMPANYNAME;
@property (nonatomic, strong) NSString *tCACCESSKEY;
@property (nonatomic, assign) id pAYTYPE;
@property (nonatomic, assign) double lENGTHMAX;
@property (nonatomic, assign) double lENGTHMIN;
@property (nonatomic, strong) NSString *nAME;
@property (nonatomic, strong) NSString *tTACCESSKEY;
@property (nonatomic, strong) NSString *sHOWCONSUMERNO;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
