//
//  add_bill_Otp_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 01/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface add_bill_Otp_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

