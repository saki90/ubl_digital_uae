//
//  GnbTranansactionHistory.m
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbTranansactionHistory.h"


NSString *const kGnbTranansactionHistoryStatus = @"status";
NSString *const kGnbTranansactionHistoryAmount = @"Amount";
NSString *const kGnbTranansactionHistoryTransactionId = @"transaction_id";
NSString *const kGnbTranansactionHistoryTransactionDate = @"transaction_date";
NSString *const kGnbTranansactionHistoryTCACCESSKEY = @"TC_ACCESS_KEY";
NSString *const kGnbTranansactionHistoryTrandisplayId = @"trandisplay_id";
NSString *const kGnbTranansactionHistoryTtName = @"tt_name";


@interface GnbTranansactionHistory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbTranansactionHistory

@synthesize status = _status;
@synthesize amount = _amount;
@synthesize transactionId = _transactionId;
@synthesize transactionDate = _transactionDate;
@synthesize tCACCESSKEY = _tCACCESSKEY;
@synthesize trandisplayId = _trandisplayId;
@synthesize ttName = _ttName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [self objectOrNilForKey:kGnbTranansactionHistoryStatus fromDictionary:dict];
            self.amount = [self objectOrNilForKey:kGnbTranansactionHistoryAmount fromDictionary:dict];
            self.transactionId = [self objectOrNilForKey:kGnbTranansactionHistoryTransactionId fromDictionary:dict];
            self.transactionDate = [self objectOrNilForKey:kGnbTranansactionHistoryTransactionDate fromDictionary:dict];
            self.tCACCESSKEY = [self objectOrNilForKey:kGnbTranansactionHistoryTCACCESSKEY fromDictionary:dict];
            self.trandisplayId = [self objectOrNilForKey:kGnbTranansactionHistoryTrandisplayId fromDictionary:dict];
            self.ttName = [self objectOrNilForKey:kGnbTranansactionHistoryTtName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.status forKey:kGnbTranansactionHistoryStatus];
    [mutableDict setValue:self.amount forKey:kGnbTranansactionHistoryAmount];
    [mutableDict setValue:self.transactionId forKey:kGnbTranansactionHistoryTransactionId];
    [mutableDict setValue:self.transactionDate forKey:kGnbTranansactionHistoryTransactionDate];
    [mutableDict setValue:self.tCACCESSKEY forKey:kGnbTranansactionHistoryTCACCESSKEY];
    [mutableDict setValue:self.trandisplayId forKey:kGnbTranansactionHistoryTrandisplayId];
    [mutableDict setValue:self.ttName forKey:kGnbTranansactionHistoryTtName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.status = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryStatus];
    self.amount = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryAmount];
    self.transactionId = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryTransactionId];
    self.transactionDate = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryTransactionDate];
    self.tCACCESSKEY = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryTCACCESSKEY];
    self.trandisplayId = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryTrandisplayId];
    self.ttName = [aDecoder decodeObjectForKey:kGnbTranansactionHistoryTtName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_status forKey:kGnbTranansactionHistoryStatus];
    [aCoder encodeObject:_amount forKey:kGnbTranansactionHistoryAmount];
    [aCoder encodeObject:_transactionId forKey:kGnbTranansactionHistoryTransactionId];
    [aCoder encodeObject:_transactionDate forKey:kGnbTranansactionHistoryTransactionDate];
    [aCoder encodeObject:_tCACCESSKEY forKey:kGnbTranansactionHistoryTCACCESSKEY];
    [aCoder encodeObject:_trandisplayId forKey:kGnbTranansactionHistoryTrandisplayId];
    [aCoder encodeObject:_ttName forKey:kGnbTranansactionHistoryTtName];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbTranansactionHistory *copy = [[GnbTranansactionHistory alloc] init];
    
    if (copy) {

        copy.status = [self.status copyWithZone:zone];
        copy.amount = [self.amount copyWithZone:zone];
        copy.transactionId = [self.transactionId copyWithZone:zone];
        copy.transactionDate = [self.transactionDate copyWithZone:zone];
        copy.tCACCESSKEY = [self.tCACCESSKEY copyWithZone:zone];
        copy.trandisplayId = [self.trandisplayId copyWithZone:zone];
        copy.ttName = [self.ttName copyWithZone:zone];
    }
    
    return copy;
}


@end
