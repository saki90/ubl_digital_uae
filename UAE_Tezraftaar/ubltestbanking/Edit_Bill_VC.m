//
//  Edit_Bill_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 02/08/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Edit_Bill_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Edit_Bill_VC ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    NSDictionary* dic;
    MBProgressHUD* hud;
    UIAlertController  *alert;
    NSArray* split_bill;
    NSString* str_tt_id;
    UIStoryboard *storyboard;
    UIViewController *vc;
    //  UIAlertView *alert;
    NSString* chk_ssl;
    NSString* str_msg;
    NSString* billType;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Edit_Bill_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    @try {
        
        
        self.transitionController = [[TransitionDelegate alloc] init];
        gblclass=[GlobalStaticClass getInstance];
        encrypt = [[Encrypt alloc] init];
        
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        
        self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        [[UITextField appearance] setTintColor:[UIColor blackColor]];
        
        //NSLog(@"%@",gblclass.Edit_bill_type );
        
        //NSLog(@"%@",gblclass.Edit_bill_type);
        
        ssl_count = @"0";
//        NSLog(@"%@",gblclass.arr_prepaid_data);
        if ( [gblclass.Edit_bill_type isEqualToString:@"Utility"])
        {
            
            lbl_header.text=@"EDIT BILL";
            billType=@"Utility Bill";
            str_msg = @"Bill nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            if ([gblclass.arr_bill_load_all count]>0)
            {
                split_bill=[[NSArray alloc] init];
                
//                NSLog(@"%@",gblclass.arr_bill_load_all);
                
                split_bill = [[gblclass.arr_bill_load_all objectAtIndex:0] componentsSeparatedByString: @"|"];
                
                
                str_tt_id=[split_bill objectAtIndex:0];
                txt_bill_customer_id.text = [split_bill objectAtIndex:3];
                txt_bill_type.text = [NSString stringWithFormat:@"%@ -  %@",[split_bill objectAtIndex:2],[split_bill objectAtIndex:4]];
                txt_bill_nick.text = [split_bill objectAtIndex:6];
            }
            
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"OB"])
        {
            
            lbl_header.text=@"EDIT MOBILE BILL";
            billType=@"Mobile Bill";
            str_msg = @"Mobile bill nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            if ([gblclass.arr_bill_ob count]>0)
            {
                split_bill=[[NSArray alloc] init];
                split_bill = [[gblclass.arr_bill_ob objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
                
                
                str_tt_id=[split_bill objectAtIndex:1];
                txt_bill_customer_id.text=[split_bill objectAtIndex:2];
                txt_bill_type.text=[split_bill objectAtIndex:10];
                txt_bill_nick.text=[split_bill objectAtIndex:3];
            }
            
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"UBL_BP"])
        {
            lbl_header.text=@"EDIT UBL BILL";
            billType=@"UBL Bill";
            str_msg = @"UBL bill nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            //NSLog(@"%@",gblclass.arr_bill_ublbp);
            
            if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
            {
                //NSLog(@"%@",gblclass.arr_bill_ublbp);
                
                str_tt_id=[gblclass.arr_bill_ublbp objectAtIndex:0];
                txt_bill_customer_id.text=[gblclass.arr_bill_ublbp objectAtIndex:1];
                txt_bill_type.text=[gblclass.arr_bill_ublbp objectAtIndex:2];
                txt_bill_nick.text=[gblclass.arr_bill_ublbp objectAtIndex:3];
            }
            else if ([gblclass.arr_bill_ublbp count]>0)
            {
                
                split_bill=[[NSArray alloc] init];
                split_bill = [[gblclass.arr_bill_ublbp objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
                
                
                str_tt_id=[split_bill objectAtIndex:1];
                txt_bill_customer_id.text=[split_bill objectAtIndex:2];
                txt_bill_type.text=[split_bill objectAtIndex:10];
                txt_bill_nick.text=[split_bill objectAtIndex:3];
                
            }
            
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"ISP"])
        {
            lbl_header.text=@"EDIT ISP BILL";
            billType=@"ISP Bill";
            str_msg = @"ISP Bill nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            if ([gblclass.arr_bill_isp count]>0)
            {
                
                split_bill=[[NSArray alloc] init];
                split_bill=[[gblclass.arr_bill_isp objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
                
                str_tt_id=[split_bill objectAtIndex:1];
                txt_bill_customer_id.text=[split_bill objectAtIndex:2];
                txt_bill_type.text=[split_bill objectAtIndex:10];
                txt_bill_nick.text=[split_bill objectAtIndex:3];
            }
            
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"Mobile TopUp"])
        {
            lbl_header.text=@"EDIT MOBILE TOPUP";
            billType=@"Mobile Topup";
            str_msg = @"Mobile Topup nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            str_tt_id=[gblclass.arr_prepaid_data objectAtIndex:3];
            txt_bill_customer_id.text=[gblclass.arr_prepaid_data objectAtIndex:4];
            txt_bill_type.text=[gblclass.arr_prepaid_data objectAtIndex:5];
            txt_bill_nick.text=[gblclass.arr_prepaid_data objectAtIndex:6];
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"Prepaid Voucher"])
        {
            txt_bill_customer_id.hidden=YES;
            
            lbl_header.text=@"EDIT PREPAID VOUCHER";
            billType=@"Prepaid Voucher";
            str_msg = @"Prepaid voucher nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            str_tt_id=[gblclass.arr_prepaid_data objectAtIndex:3];
            txt_bill_customer_id.text=[gblclass.arr_prepaid_data objectAtIndex:4];
            txt_bill_type.text=[gblclass.arr_prepaid_data objectAtIndex:5];
            txt_bill_nick.text=[gblclass.arr_prepaid_data objectAtIndex:6];
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"Wiz"])
        {
            lbl_header.text=@"EDIT WIZ TOPUP";
            billType=@"Wiz Topup";
            str_msg = @"Wiz topup nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            str_tt_id=[gblclass.arr_prepaid_data objectAtIndex:3];
            txt_bill_customer_id.text=[gblclass.arr_prepaid_data objectAtIndex:4];
            txt_bill_type.text=[gblclass.arr_prepaid_data objectAtIndex:5];
            txt_bill_nick.text=[gblclass.arr_prepaid_data objectAtIndex:6];
        }
        else if ([gblclass.Edit_bill_type isEqualToString:@"school"])
        {
            lbl_header.text=@"SCHOOL";
            billType=@"School";
            str_msg = @"School nick changed successfully";
            txt_bill_nick.placeholder=[NSString stringWithFormat:@"%@ Nick ", billType];
            lbl_title.text=[NSString stringWithFormat:@"Edit %@ Details", billType];
            
            
//            NSLog(@"%@",gblclass.arr_school_edit);
            
//            [gblclass.arr_school_edit addObject:[split_bill objectAtIndex:1]];//Consumer No.
//            [gblclass.arr_school_edit addObject:[split_bill objectAtIndex:3]];//Nick
//            [gblclass.arr_school_edit addObject:[split_bill objectAtIndex:5]];//Registered ID
//            [gblclass.arr_school_edit addObject:[split_bill objectAtIndex:6]];//TT ID
            
            
            str_tt_id = [gblclass.arr_school_edit objectAtIndex:3];
            txt_bill_customer_id.text = [gblclass.arr_school_edit objectAtIndex:0];
            
            txt_bill_type.text = [gblclass.arr_school_edit objectAtIndex:4];
            txt_bill_nick.text = [gblclass.arr_school_edit objectAtIndex:1];
        }
        
        
        //    [APIdleManager sharedInstance].onTimeout = ^(void){
        //        //  [self.timeoutLabel  setText:@"YES"];
        //
        //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        //
        //         storyboard = [UIStoryboard storyboardWithName:
        //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
        //         vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        //
        //        [self presentViewController:vc animated:YES completion:nil];
        //
        //        APIdleManager * cc1=[[APIdleManager alloc] init];
        //        // cc.createTimer;
        //
        //        [cc1 timme_invaletedd];
        //
        //    };
        
        [hud hideAnimated:YES];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Please try again later." :@"0"];
        
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    if ([theTextField isEqual:txt_bill_nick])
    {
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    
    
    return YES ;
}




- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_save:(id)sender
{
    
    [hud showAnimated:YES];
    NSString *nick_name = [txt_bill_nick.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([nick_name isEqualToString:@""])
    {
        [hud hideAnimated:YES];
    //    [self custom_alert:[NSString stringWithFormat:@"Enter %@ Nick",billType] :@"0"];
        [self custom_alert:[NSString stringWithFormat:@"Enter bill nick"] :@"0"];
        return;
    }
    else if ([nick_name length] < 5)
    {
        [hud hideAnimated:YES];
        [self custom_alert:[NSString stringWithFormat:@"Nickname should be between 5 to 30 characters"] :@"0"];
        txt_bill_nick.text = nick_name;
        return;
    }
    else if ([txt_bill_customer_id.text isEqualToString:@""])
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Enter Customer ID" :@"0"];
        return;
    }
    else
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"update";
            [self SSL_Call];
        }
    }
}

-(void) Edit_Bill:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam;
        
        if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                          [encrypt encrypt_Data:str_tt_id],
                          [encrypt encrypt_Data:txt_bill_customer_id.text],
                          [encrypt encrypt_Data:txt_bill_nick.text],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"UserId",
                                                             @"tt_id",
                                                             @"consumerNo",
                                                             @"nick",
                                                             @"Device_ID",
                                                             @"Token",
                                                             @"strSessionId",
                                                             @"IP", nil]];
        }
        else
        {
            
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                          [encrypt encrypt_Data:str_tt_id],
                          [encrypt encrypt_Data:txt_bill_customer_id.text],
                          [encrypt encrypt_Data:txt_bill_nick.text],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"UserId",
                                                             @"tt_id",
                                                             @"consumerNo",
                                                             @"nick",
                                                             @"Device_ID",
                                                             @"Token",
                                                             @"strSessionId",
                                                             @"IP", nil]];
        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"UpdateBills" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //            NSError *error;
                  //            NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      [hud hideAnimated:YES];
                       
                      //     [self custom_alert:@"Record Updated Successfully" :@"1"];
                      
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:str_msg
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      [alert1 show];
                      
                      //NSLog(@"%@",gblclass.Edit_bill_type);
                      
                      if ([gblclass.Edit_bill_type isEqualToString:@"Mobile TopUp"] || [gblclass.Edit_bill_type isEqualToString:@"Prepaid Voucher"] || [gblclass.Edit_bill_type isEqualToString:@"Wiz"])
                      {
                           
                          if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
                          {
                              
                              gblclass.direct_pay_frm_Acctsummary=@"0";
                              
                              CATransition *transition = [ CATransition animation];
                              transition.duration = 0.3;
                              transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                              transition.type = kCATransitionPush;
                              transition.subtype = kCATransitionFromRight;
                              [self.view.window.layer addAnimation:transition forKey:nil];
                              
                              
                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                              vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
                              
                              [self presentViewController:vc animated:NO completion:nil];
                          }
                          else
                          {
                              
                              CATransition *transition = [ CATransition animation];
                              transition.duration = 0.3;
                              transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                              transition.type = kCATransitionPush;
                              transition.subtype = kCATransitionFromRight;
                              [self.view.window.layer addAnimation:transition forKey:nil];
                              
                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                              vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                              
                              [self presentViewController:vc animated:NO completion:nil];
                          }
                          
                      }
                      else if ([gblclass.Edit_bill_type isEqualToString:@"school"])
                      {
                          CATransition *transition = [ CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"school_list"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else
                      {
                          
                          if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
                          {
                              
                              gblclass.direct_pay_frm_Acctsummary=@"0";
                              
                              CATransition *transition = [ CATransition animation];
                              transition.duration = 0.3;
                              transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                              transition.type = kCATransitionPush;
                              transition.subtype = kCATransitionFromRight;
                              [self.view.window.layer addAnimation:transition forKey:nil];
                              
                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                              vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
                              
                              [self presentViewController:vc animated:NO completion:nil];
                          }
                          else
                          {
                              CATransition *transition = [ CATransition animation];
                              transition.duration = 0.3;
                              transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                              transition.type = kCATransitionPush;
                              transition.subtype = kCATransitionFromRight;
                              [self.view.window.layer addAnimation:transition forKey:nil];
                              
                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                              vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                              
                              [self presentViewController:vc animated:NO completion:nil];
                          }
                          
                      }
                      
                  }
                  //              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"])
                  //              {
                  //
                  //                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //                  //
                  //                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  //
                  //                  //                      [alert addAction:ok];
                  //                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                  [hud hideAnimated:YES];
                  //
                  //                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  //
                  //                  return ;
                  //
                  //              }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      
                      
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                      
                      //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                  [alert addAction:ok];
                      //
                      //                  [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  [hud hideAnimated:YES];
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  
                  [self custom_alert:@"Retry"  :@"0"];
                  
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        
        [hud hideAnimated:YES];
        
        [self custom_alert:exception.reason  :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception.reason  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}









-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        //  [self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window. layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}



-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}
//
-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"update"])
        {
            chk_ssl=@"";
            [self Edit_Bill:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"update"])
    {
        chk_ssl=@"";
        [self Edit_Bill:@""];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            [self custom_alert:statusString  :@"0"];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_bill_nick resignFirstResponder];
}

-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

