//
//  MyAnnoo.h
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnoo : MKAnnotationView

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;

@property (nonatomic, readonly) NSString *reuseIdentifier;

// Defaults to NO. Becomes YES when tapped/clicked on in the map view.
@property (nonatomic, getter=isSelected) BOOL selected;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;


@end
