//
//  Acct_statemnt_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 24/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Acct_statemnt_VC.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "GnbAccountTransactionDetail.h"
#import "Act_Statemnt_Summary_ViewController.h"
#import "APIdleManager.h"
#import "ubltestbanking-Swift.h"
#import <Charts/Charts.h>
#import "CustomTransitionVController.h"
#import "Encrypt.h"


@interface Acct_statemnt_VC ()<NSURLConnectionDataDelegate,UIViewControllerTransitioningDelegate>
{
    NSMutableArray* arr_acct_statement;
    UILabel* label;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
    GnbAccountTransactionDetail *classObj;
    GnbRefreshAcct *refreshaccount;
    NSDictionary *dic;
    UIStoryboard *storyboard;
    UIViewController *vc;
    
    NSMutableArray* acc_regid;
    NSMutableArray* acc_name;
    NSMutableArray* acc_type;
    NSUInteger *set;
    NSString* cr_card_hidden;
    NSString* cell_index;
    UIButton *det_button;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* img_name;
    NSString* img_sign;
    NSString* signn;
    NSInteger cre_balc;
    NSInteger de_balc;
    NSString* chck_datt;
    NSString* acct_summary_curr;
    
    
    //Graph ::
    
    int previousStepperValue;
    int totalNumber;
    int total_balcccc;
    NSMutableArray* datt;
    NSMutableArray* arr_graph;
    NSMutableArray* arr_graph_datt;
    NSMutableArray* arr_graph_balc;
    
    NSString* acct_name,*acct_no,*acct_balc;
    NSDate *to_date,*frm_date;
    NSDateFormatter * frm_dat,* to_dat;
    NSDateComponents *dateComponents_frm;
    
    NSNumberFormatter *format;
    NSString *temp;
    float number2;
    NSDate  *dt_frm;
    NSDate* dt_to;
    UILabel *nochartText;
    CGRect frameForAnimationView;
    BOOL runOnce;
    
    UIImageView *imv_sign;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UIView *tableHeader;
@property (weak, nonatomic) IBOutlet UIView *AnimationView;


@end

@implementation Acct_statemnt_VC
@synthesize table;
@synthesize transitionController;

@synthesize searchResult;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //  self.transitionController = [[TransitionDelegate alloc] init];
    
    @try {
        
        chck_datt=@"";
        cre_balc=0;
        
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        encrypt = [[Encrypt alloc] init];
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        ssl_count = @"0";
        
        //    cre_balc=cre_balc+20;
        //    //NSLog(@"%ld",(long)cre_balc );
        
        self.transitionController = [[TransitionDelegate alloc] init];
        classObj=[[GnbAccountTransactionDetail alloc] init];
        gblclass=[GlobalStaticClass getInstance];
        arr_graph=[[NSMutableArray alloc] init];
        arr_graph_datt=[[NSMutableArray alloc] init];
        arr_graph_balc=[[NSMutableArray alloc] init];
        
        _arrayOfValues=[[NSMutableArray alloc] init];
        
        
        _drpdwnview.hidden=YES;
        vw_datt.hidden=YES;
        
        arr_acct_statement=[[NSMutableArray alloc] init];
        acc_regid=[[NSMutableArray alloc] init];
        acc_name=[[NSMutableArray alloc] init];
        acc_type=[[NSMutableArray alloc] init];
        
        // _arrayOfDates=[[NSMutableArray alloc] init];
        //
        _arrayOfDates=[[NSMutableArray alloc] init];
        
        
        
        for (dic in gblclass.actsummaryarr)
        {
            set = [gblclass.actsummaryarr indexOfObject:dic];
            //NSLog(@"%d",set);
            
            refreshaccount = [[GnbRefreshAcct alloc] initWithDictionary:dic];
            [acc_name addObject:refreshaccount.accountName];
            [acc_regid addObject:refreshaccount.registeredAccountId];
            [acc_type addObject:refreshaccount.accountTypeDesc];
        }
        
        _txt_account.text=gblclass.defaultaccountname;
        
        if([gblclass.chk_acct_statement isEqualToString:@"0"])
        {
            
            
            if ([gblclass.summary_acct_name isEqualToString:@""])
            {
                _txt_account.text=gblclass.summary_acct_name;
            }
            //        else
            //        {
            //            _txt_account.text=gblclass.is_default_acct_id_name;
            //        }
            
            
            //        [self checkinternet];
            //        if (netAvailable)
            //        {
            //            [self SSL_Call];
            //        }
            
            
        }
        else
        {
            // _txt_account.text=gblclass.is_default_acct_id_name;
        }
        
   
        if ([[gblclass.arr_graph_name objectAtIndex:0] isEqualToString:@"0"] || [[gblclass.arr_graph_name objectAtIndex:0] length] == 0 )
        {
            acct_name=gblclass.is_default_acct_id_name;
            acct_no=gblclass.is_default_acct_no;
            acct_balc=gblclass.is_default_m3_balc;
            
            //gblclass.tot_balc;
            
            
            NSString *str_acctz =acct_balc;
            
            str_acctz = [str_acctz stringByReplacingOccurrencesOfString:@","
                                                             withString:@""];
            
            NSString *mystring =[NSString stringWithFormat:@"%@",str_acctz];
            NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
            NSNumberFormatter *formatter = [NSNumberFormatter new];
            [formatter setMinimumFractionDigits:2];
            [formatter setMaximumFractionDigits:2];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            lbl_balc.text=[NSString stringWithFormat:@"%@ %@",gblclass.is_default_currency,[formatter stringFromNumber:number]];
            acct_summary_curr=gblclass.is_default_currency;
            
        }
        else
        {
            
            //NSLog(@"%@",gblclass.arr_graph_name);
            
            format = [[NSNumberFormatter alloc]init];
            [format setNumberStyle:NSNumberFormatterDecimalStyle];
            [format setRoundingMode:NSNumberFormatterRoundHalfUp];
            [format setMaximumFractionDigits:2];
            
            number2=[[gblclass.arr_graph_name objectAtIndex:3] floatValue];
            
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            
              NSLog(@"%@",gblclass.arr_graph_name);
            
            acct_name=[gblclass.arr_graph_name objectAtIndex:1];
            acct_no=[gblclass.arr_graph_name objectAtIndex:4];      //4 2
            acct_balc=temp;
            gblclass.defaultaccountno=acct_no;
            
            
            NSString *str_acctz =acct_balc;
            
            str_acctz = [str_acctz stringByReplacingOccurrencesOfString:@","
                                                             withString:@""];
            
            NSString *mystring =[NSString stringWithFormat:@"%@",str_acctz];
            NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
            NSNumberFormatter *formatter = [NSNumberFormatter new];
            [formatter setMinimumFractionDigits:2];
            [formatter setMaximumFractionDigits:2];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            lbl_balc.text=[NSString stringWithFormat:@"%@ %@",[gblclass.arr_graph_name objectAtIndex:7],[formatter stringFromNumber:number]];
            acct_summary_curr = [gblclass.arr_graph_name objectAtIndex:7];
            
            if ([[gblclass.arr_graph_name objectAtIndex:6] isEqualToString:@"UBL UAE CC"])
            {
                lbl_debit.text=@"Available Limit";
                lbl_credit.text=@"Outstanding Amount";
                
                NSLog(@"%@",gblclass.arr_graph_name);
                
                lbl_tot_de.text=[gblclass.arr_graph_name objectAtIndex:8];
                lbl_tot_cre.text=[gblclass.arr_graph_name objectAtIndex:3];
            }
        }
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"acct_statement";
            [self SSL_Call];
        }
        
        
        lbl_name.text=acct_name;
        
        if ([gblclass.is_default_acct_no length]==0)
        {
            gblclass.is_default_acct_no=@"00000000000000";
        }
        
        //NSLog(@"%@",gblclass.arr_graph_name);
        
        
        if([[gblclass.arr_graph_name objectAtIndex:0] isEqualToString:@"1"])
        {
            
            if ([[gblclass.arr_graph_name objectAtIndex:5] isEqualToString:@"1"])
            {
                NSTextAttachment * attach = [[NSTextAttachment alloc] init];
                attach.image = [UIImage imageNamed:@"check_select_acct.png"];
                attach.bounds = CGRectMake(10, 0, 15, 15);
                NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
                
                NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:acct_no];
                NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
                [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
                
                [mutableAttriStr appendAttributedString:imageStr];
                lbl_acct.attributedText = mutableAttriStr;
            }
            else
            {
                lbl_acct.text=acct_no;
            }
            
        }
        else if ([gblclass.frm_acct_default_chck isEqualToString:@"1"])
        {
            NSTextAttachment * attach = [[NSTextAttachment alloc] init];
            attach.image = [UIImage imageNamed:@"check_select_acct.png"];
            attach.bounds = CGRectMake(10, 0, 15, 15);
            NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
            
            NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:acct_no];
            NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
            [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
            
            [mutableAttriStr appendAttributedString:imageStr];
            lbl_acct.attributedText = mutableAttriStr;
        }
        else
        {
            lbl_acct.text=acct_no;
        }
        
        //NSLog(@"%@",gblclass.summary_acct_no);
        //NSLog(@"%@",gblclass.defaultaccountno);
        //NSLog(@"%@",gblclass.summary_acct_name);
        
        
        if ([gblclass.tot_balc isEqualToString:@"NaN"] || [gblclass.tot_balc isEqualToString:@""])
        {
            // gblclass.tot_balc=@"0";
        }
        
        
        NSDate *currentDate = [NSDate date];
        dateComponents_frm = [[NSDateComponents alloc] init];
        [dateComponents_frm setDay:-90];  //-120
        frm_date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents_frm toDate:currentDate options:0];
        //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, frm_date);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy"];
        NSString *dateFormatter_frm = [dateFormatter stringFromDate:frm_date];
        
        NSString *dateFormatter_to = [dateFormatter stringFromDate:currentDate];
        
        lbl_dat_frm.text= [NSString stringWithFormat:@"%@",dateFormatter_frm];
        lbl_dat_to.text=[NSString stringWithFormat:@"%@",dateFormatter_to];
      
        scrl.contentSize=CGSizeMake(0,663);
        scrl.panGestureRecognizer.enabled=YES;
        scrl.pinchGestureRecognizer.enabled=YES;
        
        _myGraph.delegate=self;
        
        
        // Basit Changed
        frameForAnimationView = _AnimationView.frame;
        runOnce = NO;
        
        
        nochartText = [[UILabel alloc] initWithFrame:CGRectMake(_chartView.frame.size.width/2 - 100, _chartView.frame.size.height / 2 - 10, 200, 20)];
        nochartText.textColor = [UIColor whiteColor];
        nochartText.textAlignment = NSTextAlignmentCenter;
        nochartText.numberOfLines = 2;
        nochartText.adjustsFontSizeToFitWidth = YES;
        nochartText.text = @"No Transaction found in selected date range";
        [_chartView addSubview:nochartText];
        nochartText.hidden = YES;
        
        
        
        _chartView.delegate = self;
        
        _chartView.chartDescription.enabled = NO;
        
        _chartView.dragEnabled = YES;
        [_chartView setScaleEnabled:YES];
        _chartView.pinchZoomEnabled = YES;
        _chartView.drawGridBackgroundEnabled = NO;
        _chartView.rightAxis.enabled = YES;
        _chartView.leftAxis.enabled = YES;
        [_chartView setVisibleYRangeWithMinYRange:-100 maxYRange:100 axis:AxisDependencyRight];
        [_chartView.viewPortHandler setMaximumScaleX:5];
        [_chartView.viewPortHandler setMaximumScaleY:5];
        _chartView.scaleXEnabled = YES;
        _chartView.scaleYEnabled = YES;
        _chartView.legend.enabled = YES;
        
        
        
        
        _chartView.xAxis.labelCount = 5;
        _chartView.xAxis.labelPosition = XAxisLabelPositionBottom;
        _chartView.xAxis.drawLabelsEnabled = YES;
        _chartView.xAxis.labelTextColor = [UIColor whiteColor];
        _chartView.xAxis.gridLineWidth = 1;
        _chartView.xAxis.axisLineWidth = 1;
        _chartView.xAxis.gridColor = [UIColor whiteColor];
        
        
        
        
        
        
        _chartView.rightAxis.drawLabelsEnabled = NO;
        _chartView.rightAxis.drawGridLinesEnabled = NO;
        _chartView.rightAxis.axisLineWidth = 1;
        _chartView.rightAxis.granularityEnabled = NO;
        _chartView.rightAxis.labelCount = 0;
        _chartView.rightAxis.axisLineColor = [UIColor whiteColor];
        _chartView.rightAxis.zeroLineColor = [UIColor whiteColor];
        
        
        
        
        
        
        _chartView.leftAxis.drawLabelsEnabled = NO;
        _chartView.leftAxis.drawGridLinesEnabled = NO;
        _chartView.leftAxis.axisLineWidth = 1;
        _chartView.leftAxis.granularityEnabled = NO;
        _chartView.leftAxis.labelCount = 0;
        _chartView.leftAxis.drawLimitLinesBehindDataEnabled = YES;
        _chartView.leftAxis.axisLineColor = [UIColor whiteColor];
        _chartView.leftAxis.zeroLineColor = [UIColor whiteColor];
        
        
        
        
        
        
        
        
        BalloonMarker *marker = [[BalloonMarker alloc]
                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                                 font: [UIFont systemFontOfSize:12.0]
                                 textColor: UIColor.whiteColor
                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        
        marker.chartView = _chartView;
        marker.minimumSize = CGSizeMake(80.f, 40.f);
        _chartView.marker = marker;
        
        _chartView.legend.form = ChartLegendFormLine;
        
        _sliderX.value = 45.0;
        _sliderY.value = 100.0;
        
        _chartView.legend.enabled = NO;
        
        _chartView.backgroundColor=[UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0];
        _AnimationView.backgroundColor = [UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0];
        
        
        
    }
    @catch (NSException *exception)
    {
        gblclass.custom_alert_msg=@"Try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    
}


-(void)viewWillAppear:(BOOL)animated {
    
    
    [table reloadData];
    
    if (!runOnce) {
        
        
        
        lbl_name.alpha = 0;
        lbl_name.frame = CGRectMake(lbl_name.frame.origin.x, lbl_name.frame.origin.y+10, lbl_name.frame.size.width, lbl_name.frame.size.height);
        lbl_acct.alpha = 0;
        lbl_acct.frame = CGRectMake(lbl_acct.frame.origin.x, lbl_acct.frame.origin.y+10, lbl_acct.frame.size.width, lbl_acct.frame.size.height);
        lbl_balc.alpha = 0;
        lbl_balc.frame = CGRectMake(lbl_balc.frame.origin.x, lbl_balc.frame.origin.y+10, lbl_balc.frame.size.width, lbl_balc.frame.size.height);
        
        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            
            lbl_name.alpha = 1;
            lbl_name.frame = CGRectMake(lbl_name.frame.origin.x, lbl_name.frame.origin.y-10, lbl_name.frame.size.width, lbl_name.frame.size.height);
            lbl_acct.alpha = 1;
            lbl_acct.frame = CGRectMake(lbl_acct.frame.origin.x, lbl_acct.frame.origin.y-10, lbl_acct.frame.size.width, lbl_acct.frame.size.height);
            lbl_balc.alpha = 1;
            lbl_balc.frame = CGRectMake(lbl_balc.frame.origin.x, lbl_balc.frame.origin.y-10, lbl_balc.frame.size.width, lbl_balc.frame.size.height);
            
            
        } completion:^(BOOL finished) {
            
            runOnce = YES;
            
        }];
        
    }
    
    
    
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    CustomTransitionVController *myCustonTrans = [[CustomTransitionVController alloc] init];
    myCustonTrans.centerPoint = gblclass.centrePoint;
    return myCustonTrans;
    
    
}

-(void)animateTable {
    
    
    self.tableHeader.frame = CGRectMake(self.tableHeader.frame.origin.x, self.tableHeader.frame.origin.y + 30, _tableHeader.frame.size.width, _tableHeader.frame.size.height);
    
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y + 30, table.frame.size.width, table.frame.size.height);
    
    
    // self.chartView.frame = CGRectMake(self.chartView.frame.origin.x, self.chartView.frame.origin.y - 40, _chartView.frame.size.width, _chartView.frame.size.height);
    
    
    [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        
        self.tableHeader.frame = CGRectMake(self.tableHeader.frame.origin.x, self.tableHeader.frame.origin.y - 30, _tableHeader.frame.size.width, _tableHeader.frame.size.height);
        
        self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y - 30 , table.frame.size.width, table.frame.size.height);
        
        
        //self.chartView.frame = CGRectMake(self.chartView.frame.origin.x, self.chartView.frame.origin.y + 40, _chartView.frame.size.width, _chartView.frame.size.height);
        
        
    } completion:^(BOOL finished) {
        
    }];
}


- (void)setDataCount:(int)count range:(double)range
{
    
    
    // Basit Changed
          frameForAnimationView = _AnimationView.frame;
          runOnce = NO;
          
          
          nochartText = [[UILabel alloc] initWithFrame:CGRectMake(_chartView.frame.size.width/2 - 100, _chartView.frame.size.height / 2 - 10, 200, 20)];
          nochartText.textColor = [UIColor whiteColor];
          nochartText.textAlignment = NSTextAlignmentCenter;
          nochartText.numberOfLines = 2;
          nochartText.adjustsFontSizeToFitWidth = YES;
          nochartText.text = @"No Transaction found in selected date range";
          [_chartView addSubview:nochartText];
          nochartText.hidden = YES;
          
          
          
          _chartView.delegate = self;
          
          _chartView.chartDescription.enabled = NO;
          
          _chartView.dragEnabled = YES;
          [_chartView setScaleEnabled:YES];
          _chartView.pinchZoomEnabled = YES;
          _chartView.drawGridBackgroundEnabled = NO;
          _chartView.rightAxis.enabled = YES;
          _chartView.leftAxis.enabled = YES;
          [_chartView setVisibleYRangeWithMinYRange:-100 maxYRange:100 axis:AxisDependencyRight];
          [_chartView.viewPortHandler setMaximumScaleX:5];
          [_chartView.viewPortHandler setMaximumScaleY:5];
          _chartView.scaleXEnabled = YES;
          _chartView.scaleYEnabled = YES;
          _chartView.legend.enabled = YES;
          
          
          
          
          _chartView.xAxis.labelCount = 5;
          _chartView.xAxis.labelPosition = XAxisLabelPositionBottom;
          _chartView.xAxis.drawLabelsEnabled = YES;
          _chartView.xAxis.labelTextColor = [UIColor whiteColor];
          _chartView.xAxis.gridLineWidth = 1;
          _chartView.xAxis.axisLineWidth = 1;
          _chartView.xAxis.gridColor = [UIColor whiteColor];
          
          
          
          
          
          
          _chartView.rightAxis.drawLabelsEnabled = NO;
          _chartView.rightAxis.drawGridLinesEnabled = NO;
          _chartView.rightAxis.axisLineWidth = 1;
          _chartView.rightAxis.granularityEnabled = NO;
          _chartView.rightAxis.labelCount = 0;
          _chartView.rightAxis.axisLineColor = [UIColor whiteColor];
          _chartView.rightAxis.zeroLineColor = [UIColor whiteColor];
          
          
          
          
          
          
          _chartView.leftAxis.drawLabelsEnabled = NO;
          _chartView.leftAxis.drawGridLinesEnabled = NO;
          _chartView.leftAxis.axisLineWidth = 1;
          _chartView.leftAxis.granularityEnabled = NO;
          _chartView.leftAxis.labelCount = 0;
          _chartView.leftAxis.drawLimitLinesBehindDataEnabled = YES;
          _chartView.leftAxis.axisLineColor = [UIColor whiteColor];
          _chartView.leftAxis.zeroLineColor = [UIColor whiteColor];
          
          
          
          
          
          
          
          
          BalloonMarker *marker = [[BalloonMarker alloc]
                                   initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                                   font: [UIFont systemFontOfSize:12.0]
                                   textColor: UIColor.whiteColor
                                   insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
          
          marker.chartView = _chartView;
          marker.minimumSize = CGSizeMake(80.f, 40.f);
          _chartView.marker = marker;
          
          _chartView.legend.form = ChartLegendFormLine;
          
          _sliderX.value = 45.0;
          _sliderY.value = 100.0;
          
          _chartView.legend.enabled = NO;
          
          _chartView.backgroundColor=[UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0];
          _AnimationView.backgroundColor = [UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    _AnimationView.frame = frameForAnimationView;
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    
    //    let formato:BarChartFormatter = BarChartFormatter()
    //    let xaxis:XAxis = XAxis()
    //    Next, go ahead at the end of the for in loop and pass the index and axis to your formatter variable:
    //
    //        formato.stringForValue(Double(i), axis: xaxis)
    //        After looping add the format to your axis variable:
    //
    //        xaxis.valueFormatter = formato
    //        The final step is to add the new xaxisformatted to the barChartView:
    //
    //        barChartView.xAxis.valueFormatter = xaxis.valueFormatter
    
    
    CustomValueFormatter *formato = [[CustomValueFormatter alloc] init];
    
    ChartXAxis *xaxis = [[ChartXAxis alloc] init];
    
    
    
    for (int i = 0; i < count; i++)
    {
        //double val = arc4random_uniform(range) + 3;
        
        NSString *dateString = _arrayOfDates[i];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateString];
        NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
        [formato stringForValue:timeInMiliseconds axis:xaxis];
        
        [values addObject:[[ChartDataEntry alloc] initWithX:timeInMiliseconds y:[_arrayOfValues[i] doubleValue]]];
        
        
    }
    
    
    if (_arrayOfValues.count == 0) {
        
        _chartView.data =  [[LineChartData alloc] init];
        nochartText.hidden = NO;
        
        
    }
    
    else {
        nochartText.hidden = YES;
        
    }
    
    
    xaxis.valueFormatter = formato;
    _chartView.xAxis.valueFormatter = xaxis.valueFormatter;
    
    
    LineChartDataSet *set1 = nil;
    NSLog(@"%ld", (long)_chartView.data.dataSetCount);
    self.chartView.data.dataSetCount == 0;
    NSLog(@"%ld", (long)_chartView.data.dataSetCount);
    if (_chartView.data.dataSetCount != 1 && _chartView.data.dataSetCount >0)
    {
        set1 = (LineChartDataSet *)_chartView.data.dataSets[0];
        [set1 notifyDataSetChanged];
//        set1.values = values;
        [set1 entries];
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
        
        [UIView animateWithDuration:1 animations:^{
            
            _AnimationView.frame = CGRectMake(self.view.frame.size.width+_AnimationView.frame.origin.x, _AnimationView.frame.origin.y, _AnimationView.frame.size.width, _AnimationView.frame.size.height);
            [self animateTable];
        }];
    }
    else
    {
        //LineChartDataSet
    //  set1 = [[LineChartDataSet alloc] initWithValues:values label:@""];
        set1 = [[LineChartDataSet alloc] initWithEntries:values];
        [set1 setMode:LineChartModeHorizontalBezier];
        
                set1.lineDashLengths = @[@5.f, @2.5f];
                set1.highlightLineDashLengths = @[@5.f, @2.5f];
//              [set1 setColor:UIColor.COLOR_WHITE];
                [set1 setColor:UIColor.whiteColor];
                [set1 setCircleColor:UIColor.blackColor];
                set1.lineWidth = 1.0;
                set1.circleRadius = 3.0;
                set1.drawCircleHoleEnabled = NO;
                set1.valueFont = [UIFont systemFontOfSize:9.f];
                set1.formLineDashLengths = @[@5.f, @2.5f];
                set1.formLineWidth = 1.0;
                set1.formSize = 15.0;
        
        
        set1.drawValuesEnabled = NO;
        [set1 setColor:[UIColor whiteColor]];
        [set1 setCircleColor:[UIColor whiteColor]];
        
        set1.lineWidth = 2;
        set1.circleRadius = 5;
        set1.highlightLineDashPhase =0;
        set1.lineDashPhase = 0;
        set1.drawCircleHoleEnabled = NO;
        set1.formLineWidth = 2;
        set1.valueFont = [UIFont systemFontOfSize:2];
        
        
        
        
        //        NSArray *gradientColors = @[
        //                                    (id)[ChartColorTemplates colorFromString:@"#00ff0000"].CGColor,
        //                                    (id)[ChartColorTemplates colorFromString:@"#ffff0000"].CGColor
        //                                    ];
        //        CGGradientRef gradient = CGGradientCreateWithColors(nil, (CFArrayRef)gradientColors, nil);
        //
        //        set1.fillAlpha = 1.f;
        //        set1.fill = [ChartFill fillWithLinearGradient:gradient angle:90.f];
        //        set1.drawFilledEnabled = YES;
        //
        //        CGGradientRelease(gradient);
        
        
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
        [self loadChartView];
        _chartView.data = data;
        
        [UIView animateWithDuration:1 animations:^{
            
            _AnimationView.frame = CGRectMake(self.view.frame.size.width+_AnimationView.frame.origin.x, _AnimationView.frame.origin.y, _AnimationView.frame.size.width, _AnimationView.frame.size.height);
            [self animateTable];
        }];
    }
}



-(void)Graph_load1
{
    // Basit Changed
    
    
    _chartView.delegate = self;
    
    _chartView.chartDescription.enabled = NO;
    
    _chartView.dragEnabled = YES;
    [_chartView setScaleEnabled:YES];
    _chartView.pinchZoomEnabled = YES;
    _chartView.drawGridBackgroundEnabled = NO;
    
    // x-axis limit line
    //    ChartLimitLine *llXAxis = [[ChartLimitLine alloc] initWithLimit:10.0 label:@"Index 10"];
    //    llXAxis.lineWidth = 4.0;
    //    llXAxis.lineDashLengths = @[@(10.f), @(10.f), @(0.f)];
    //    llXAxis.labelPosition = ChartLimitLabelPositionRightBottom;
    //    llXAxis.valueFont = [UIFont systemFontOfSize:10.f];
    
    //[_chartView.xAxis addLimitLine:llXAxis];
    
    //    _chartView.xAxis.gridLineDashLengths = @[@0.0, @0.0];
    //    _chartView.xAxis.gridLineDashPhase = 0.f;
    //    _chartView.xAxis.centerAxisLabelsEnabled = NO;
    //    _chartView.xAxis.labelCount = 5;
    //    _chartView.xAxis.gridLineWidth = 1;
    //    _chartView.xAxis.axisLineWidth = 1;
    // _chartView.xAxis.gridColor = [UIColor blueColor];
    
    _chartView.xAxis.labelCount = 5;
    _chartView.xAxis.labelPosition = XAxisLabelPositionBottom;
    _chartView.xAxis.drawLabelsEnabled = YES;
    _chartView.xAxis.labelTextColor = [UIColor whiteColor];
    _chartView.xAxis.gridLineWidth = 1;
    _chartView.xAxis.axisLineWidth = 1;
    _chartView.xAxis.gridColor = [UIColor whiteColor];
    _chartView.xAxis.axisLineColor = [UIColor whiteColor];
    
    
    //    [_chartView.xAxis setValueFormatter:<#(id<IChartAxisValueFormatter> _Nullable)#>]
    //
    //    IAxisValueFormatter
    
    _chartView.rightAxis.drawLabelsEnabled =NO;
    _chartView.rightAxis.drawGridLinesEnabled = NO;
    _chartView.rightAxis.axisLineWidth = 1;
    _chartView.rightAxis.granularityEnabled = NO;
    _chartView.rightAxis.labelCount = 0;
    _chartView.rightAxis.axisLineColor = [UIColor whiteColor];
    _chartView.rightAxis.zeroLineColor = [UIColor whiteColor];
    
    
    _chartView.leftAxis.drawLabelsEnabled = NO;
    _chartView.leftAxis.drawGridLinesEnabled = NO;
    _chartView.leftAxis.axisLineWidth = 1;
    _chartView.leftAxis.granularityEnabled = NO;
    _chartView.leftAxis.labelCount = 0;
    _chartView.leftAxis.drawLimitLinesBehindDataEnabled = YES;
    _chartView.leftAxis.axisLineColor = [UIColor whiteColor];
    _chartView.leftAxis.zeroLineColor = [UIColor whiteColor];
    
    
    
    
    //    ChartLimitLine *ll1 = [[ChartLimitLine alloc] initWithLimit:150.0 label:@"Upper Limit"];
    //    ll1.lineWidth = 4.0;
    //    ll1.lineDashLengths = @[@5.f, @5.f];
    //    ll1.labelPosition = ChartLimitLabelPositionRightTop;
    //    ll1.valueFont = [UIFont systemFontOfSize:10.0];
    //
    //    ChartLimitLine *ll2 = [[ChartLimitLine alloc] initWithLimit:-30.0 label:@"Lower Limit"];
    //    ll2.lineWidth = 4.0;
    //    ll2.lineDashLengths = @[@5.f, @5.f];
    //    ll2.labelPosition = ChartLimitLabelPositionRightBottom;
    //    ll2.valueFont = [UIFont systemFontOfSize:10.0];
    //
    //    ChartYAxis *leftAxis = _chartView.leftAxis;
    //
    //
    //
    //    [leftAxis removeAllLimitLines];
    //    //[leftAxis addLimitLine:ll1];
    //    //[leftAxis addLimitLine:ll2];
    //    //leftAxis.axisMaximum = 200.0;
    //    //leftAxis.axisMinimum = 0.0;
    //    leftAxis.gridLineDashLengths = @[@0.f, @0.f];
    //    leftAxis.drawZeroLineEnabled = YES;
    //    leftAxis.drawLimitLinesBehindDataEnabled = NO;
    //
    //    _chartView.rightAxis.enabled = YES;
    
    //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
    //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
    
    
    
    BalloonMarker *marker = [[BalloonMarker alloc]
                             initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                             font: [UIFont systemFontOfSize:12.0]
                             textColor: UIColor.whiteColor
                             insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    
    marker.chartView = _chartView;
    marker.minimumSize = CGSizeMake(80.f, 40.f);
    _chartView.marker = marker;
    
    _chartView.legend.form = ChartLegendFormLine;
    
    _sliderX.value = 45.0;
    _sliderY.value = 100.0;
    
    
    
    
    
    // [self slidersValueChanged:nil];
    
    
    _chartView.legend.enabled = NO;
    
    //_chartView.xAxis.valueFormatter
    
    // [_chartView spinWithDuration:2.0 fromAngle:_chartView.rotationAngle toAngle:[_chartView rotation] + 360.f easingOption:ChartEasingOptionEaseInCubic];
    
    
    //[_chartView animateWithXAxisDuration:3.0 easingOption:ChartEasingOptionEaseInBack];
    
    
    
    
    
    
    
    //    [_chartView animateWithXAxisDuration:2.5];
    //    [_chartView animateWithXAxisDuration:2.5 easing:^double(NSTimeInterval, NSTimeInterval) {
    //
    //    }];
    //[_chartView animateWithXAxisDuration:2.5 easing:(Dou)(2.0,2.0)]
    //[_chartView animateWithXAxisDuration:<#(NSTimeInterval)#> easing:<#^double(NSTimeInterval, NSTimeInterval)easing#>]
    
    
    _chartView.backgroundColor=[UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0];
}


- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis {
    
    return  @"hello";
}




- (void)updateChartData
{
    //    if (self.shouldHideData)
    //    {
    //        _chartView.data = nil;
    //        return;
    //    }
    
    
    
    [self setDataCount:20.0 range:100.0];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==table)
    {
        return [arr_acct_statement count];    //count number of row from counting array hear cataGorry is An Array
    }
    else
    {
        if([acc_regid count]==0)
        {
            return 0;
        }
        else
        {
            //NSLog(@"%@",gblclass.actsummaryarr);
            return [gblclass.actsummaryarr count];
            
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        
        static NSString *MyIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 48, 46)];
        //   imv_sign = [[UIImageView alloc]initWithFrame:CGRectMake(54,8, 10, 10)];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
        }
        
        if(tableView==table)
        {
            NSArray* split = [[arr_acct_statement objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            //For Header ::
            
            //NSLog(@"%lu",(unsigned long)[gblclass.arr_graph_name count]);
            
            if ([gblclass.arr_graph_name count]>5)
            {
                
                NSLog(@"%lu",(unsigned long)[gblclass.arr_graph_name count]);
                NSLog(@"%@",gblclass.arr_graph_name);
                //if ([[gblclass.arr_graph_name objectAtIndex:6] isEqualToString:@"Credit Card Account"])
                
                if([gblclass.acct_summary_section isEqualToString:@"Credit Card Account"])
                {
                    //NSLog(@"Credit Card Account");
                    
                    label=(UILabel*)[cell viewWithTag:1];
                    label.text=@"";
                    label.font=[UIFont systemFontOfSize:9];
                    [cell.contentView addSubview:label];
                    
                    
                    label=(UILabel*)[cell viewWithTag:2];
                    label.text=@"";
                    label.font=[UIFont systemFontOfSize:8];
                    [cell.contentView addSubview:label];
                    
                    label=(UILabel*)[cell viewWithTag:4];
                    label.text=[split objectAtIndex:1];
                    label.font=[UIFont systemFontOfSize:8];
                    [cell.contentView addSubview:label];
                    
                    
                    
                    //For Amount ::
                    
                    NSString* s;
                    
                    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"()"];
                    s = [[[split objectAtIndex:4] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @"-"];
                    //NSLog(@"%@", s);
                    
                    
                    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"()"];
                    NSRange range = [[split objectAtIndex:4] rangeOfCharacterFromSet:cset];
                    
                    
                    
                    if (range.location == NSNotFound)
                    {
                        // no ( or ) in the string
                        //NSLog(@" no ( or ) in the string");
                        
                        label=(UILabel*)[cell viewWithTag:6];
                        label.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[split objectAtIndex:4]];
                        //  label.font=[UIFont systemFontOfSize:9];
                        [cell.contentView addSubview:label];
                        
                        
                        img_name=@"Debit-22.png";
                        img_sign=@"Plus-b.png";
                        
                        imv.image=[UIImage imageNamed:img_name];
                        [cell.contentView addSubview:imv];
                    }
                    else
                    {
                        // ( or ) are present
                        //NSLog(@"( or ) are present");
                        
                        NSString* s;
                        
                        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"()"];
                        s = [[[split objectAtIndex:4] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
                        
                        
                        label=(UILabel*)[cell viewWithTag:6];
                        label.text=[NSString stringWithFormat:@"%@ -%@",acct_summary_curr,s];
                        //    label.font=[UIFont systemFontOfSize:9];
                        [cell.contentView addSubview:label];
                        
                        img_name=@"Credit-1.png";
                        img_sign=@"Minus-o.png";
                        
                        imv.image=[UIImage imageNamed:img_name];
                        [cell.contentView addSubview:imv];
                        
                    }
                    
                    
                    //For Currency debit credit::
                    if([cr_card_hidden isEqualToString:@"1"])
                    {
                        
                    }
                    else
                    {
                        label=(UILabel*)[cell viewWithTag:3];
                        label.text=[split objectAtIndex:3];
                        label.font=[UIFont systemFontOfSize:7];
                        [cell.contentView addSubview:label];
                    }
                    
                    //            //signn=@"";
                    //            if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                    //            {
                    //                img_name=@"Credit-1.png";
                    //                img_sign=@"Minus-o.png";
                    //                // signn=@"+";
                    //            }
                    //            else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                    //            {
                    //                img_name=@"Debit-2.png";
                    //                img_sign=@"Plus-b.png";
                    //                // signn=@"-";
                    //            }
                    //            else
                    //            {
                    //                img_sign=@"";
                    //            }
                    //
                    //            imv.image=[UIImage imageNamed:img_name];
                    //            [cell.contentView addSubview:imv];
                    
                }
                else
                {
                    
                    
                    
                    label=(UILabel*)[cell viewWithTag:1];
                    label.text=@"Balance";
                    label.font=[UIFont systemFontOfSize:9];
                    [cell.contentView addSubview:label];
                    
                    
                    //For Date ::
                    
                    label=(UILabel*)[cell viewWithTag:2];
                    label.text=[split objectAtIndex:1];
                    label.font=[UIFont systemFontOfSize:8];
                    [cell.contentView addSubview:label];
                    
                    //For Currency debit credit::
                    if([cr_card_hidden isEqualToString:@"1"])
                    {
                        
                    }
                    else
                    {
                        label=(UILabel*)[cell viewWithTag:3];
                        label.text=[split objectAtIndex:3];
                        label.font=[UIFont systemFontOfSize:7];
                        [cell.contentView addSubview:label];
                    }
                    
                    //For Amount ::
                    
                    label=(UILabel*)[cell viewWithTag:4];
                    
                    NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
                    NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
                    NSNumberFormatter *formatter = [NSNumberFormatter new];
                    [formatter setMinimumFractionDigits:2];
                    [formatter setMaximumFractionDigits:2];
                    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    
                    //        if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                    //        {
                    //            signn=@"+";
                    //        }
                    //        else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                    //        {
                    //            signn=@"-";
                    //        }
                    //        else
                    //        {
                    //             signn=@"";
                    //        }
                    
                    label.text=[NSString stringWithFormat:@"     %@ %@",acct_summary_curr,[formatter stringFromNumber:number]];
                    label.font=[UIFont systemFontOfSize:11];
                    [cell.contentView addSubview:label];
                    
                    
                    //For PKR ::
                    
                    label=(UILabel*)[cell viewWithTag:5];
                    label.text=gblclass.base_currency;
                    label.font=[UIFont systemFontOfSize:7];
                    [cell.contentView addSubview:label];
                    
                    
                    //For Amount ::
                    
                    label=(UILabel*)[cell viewWithTag:6];
                    label.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[split objectAtIndex:4]];
                    // label.font=[UIFont systemFontOfSize:9];
                    [cell.contentView addSubview:label];
                    
                    
                    //NSLog(@"Button Index :: %ld",(long)indexPath.row);
                    
                    det_button=( UIButton*)[cell viewWithTag:100];
                    
                    
                    
                    //for Image ::
                    
                    
                    
                    img_sign=@"";
                    
                    
                    imv_sign=(UIImageView*)[cell viewWithTag:7];
                    
                    imv_sign.image=[UIImage imageNamed:img_sign];
                    [cell.contentView addSubview:imv_sign];
                    
                    
                    //signn=@"";
                    if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                    {
                        img_name=@"Credit-1.png";
                        img_sign=@"Minus-o.png";
                        // signn=@"+";
                        
                    }
                    else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                    {
                        img_name=@"Debit-22.png";
                        img_sign=@"Plus-b.png";
                        // signn=@"-";
                        
                    }
                    else
                    {
                        img_sign=@"";
                    }
                    
                    //Image Cr/Dr
                    imv.image=[UIImage imageNamed:img_name];
                    [cell.contentView addSubview:imv];
                    
                    
                    //+/-
                    imv_sign.image=[UIImage imageNamed:img_sign];
                    [cell.contentView addSubview:imv_sign];
                    
                    
                    
                    
                    //for Image Plus sign ::
                    
                    //        UIImageView *imv_sign = [[UIImageView alloc]initWithFrame:CGRectMake(54,8, 10, 10)];
                    
                    //        if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                    //        {
                    //            img_sign=@"Minus-o.png"; //Cleared
                    //        }
                    //        else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                    //        {
                    //            img_sign=@"Plus-b.png"; //Balance
                    //        }
                    //        else
                    //        {
                    //
                    //        }
                    
                    
                    
                    cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
                    [det_button addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.contentView addSubview:det_button];
                    
                }
                
                
            }       //[gblclass.arr_graph_name count]>5
            else
            {
                
                
                
                label=(UILabel*)[cell viewWithTag:1];
                label.text=@"Balance";
                label.font=[UIFont systemFontOfSize:9];
                [cell.contentView addSubview:label];
                
                
                //For Date ::
                
                label=(UILabel*)[cell viewWithTag:2];
                label.text=[split objectAtIndex:1];
                label.font=[UIFont systemFontOfSize:8];
                [cell.contentView addSubview:label];
                
                //For Currency debit credit::
                if([cr_card_hidden isEqualToString:@"1"])
                {
                    
                }
                else
                {
                    label=(UILabel*)[cell viewWithTag:3];
                    label.text=[split objectAtIndex:3];
                    label.font=[UIFont systemFontOfSize:7];
                    [cell.contentView addSubview:label];
                }
                
                //For Amount ::
                
                label=(UILabel*)[cell viewWithTag:4];
                
                NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
                NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                [formatter setMinimumFractionDigits:2];
                [formatter setMaximumFractionDigits:2];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                //        if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                //        {
                //            signn=@"+";
                //        }
                //        else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                //        {
                //            signn=@"-";
                //        }
                //        else
                //        {
                //             signn=@"";
                //        }
                
                label.text=[NSString stringWithFormat:@"     %@ %@",acct_summary_curr,[formatter stringFromNumber:number]];
                label.font=[UIFont systemFontOfSize:11];
                [cell.contentView addSubview:label];
                
                
                //For PKR ::
                
                label=(UILabel*)[cell viewWithTag:5];
                label.text=gblclass.base_currency;
                label.font=[UIFont systemFontOfSize:7];
                [cell.contentView addSubview:label];
                
                
                //For Amount ::
                
                label=(UILabel*)[cell viewWithTag:6];
                label.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[split objectAtIndex:4]];
                //  label.font=[UIFont systemFontOfSize:9];
                [cell.contentView addSubview:label];
                
                
                //NSLog(@"Button Index :: %ld",(long)indexPath.row);
                
                det_button=( UIButton*)[cell viewWithTag:100];
                
                
                
                //for Image ::
                
                
                
                img_sign=@"";
                
                
                imv_sign=(UIImageView*)[cell viewWithTag:7];
                
                imv_sign.image=[UIImage imageNamed:img_sign];
                [cell.contentView addSubview:imv_sign];
                
                
                //signn=@"";
                if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                {
                    img_name=@"Credit-1.png";
                    img_sign=@"Minus-o.png";
                    // signn=@"+";
                    
                }
                else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                {
                    img_name=@"Debit-22.png";
                    img_sign=@"Plus-b.png";
                    // signn=@"-";
                    
                }
                else
                {
                    img_sign=@"";
                }
                
                //Image Cr/Dr
                imv.image=[UIImage imageNamed:img_name];
                [cell.contentView addSubview:imv];
                
                
                //+/-
                imv_sign.image=[UIImage imageNamed:img_sign];
                [cell.contentView addSubview:imv_sign];
                
                
                
                
                //for Image Plus sign ::
                
                //        UIImageView *imv_sign = [[UIImageView alloc]initWithFrame:CGRectMake(54,8, 10, 10)];
                
                //        if ([[split objectAtIndex:3] isEqualToString:@"Dr"])
                //        {
                //            img_sign=@"Minus-o.png"; //Cleared
                //        }
                //        else if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
                //        {
                //            img_sign=@"Plus-b.png"; //Balance
                //        }
                //        else
                //        {
                //
                //        }
                
                
                
                cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
                [det_button addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:det_button];
                
            }
            
        }
        
        else
        {
            
            label=(UILabel*)[cell viewWithTag:7];
            label.text=[acc_name objectAtIndex:indexPath.row];
            label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
        }
        
        return cell;
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.drpdwnview.hidden=YES;
    
    //   [tableView cellForRowAtIndexPath:indexPath].selected=false;
    
    cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    
    
    //NSLog(@"%@",tableView);
    
    
    if(tableView==table)
    {
        gblclass.acct_statmnt_indexpath=cell_index;
        gblclass.chk_acct_statement=@"1";
        
        
        CGRect myrect = [table rectForRowAtIndexPath:indexPath];
        
        UITableViewCell *cell =  [table cellForRowAtIndexPath:indexPath];
        
        
        gblclass.acct_summary_curr=acct_summary_curr;
        
        
        gblclass.acct_statement_name=lbl_name.text;
        gblclass.acct_statement_id=lbl_acct.text;
        
        
        
        CGPoint p = [cell.superview convertPoint:cell.center toView:self.view];
        
        gblclass.centrePoint = p;
        
        //CGRect frame = [self.view convertRect:cell.frame fromView:self.view.frame];
        
        
        //NSLog(@"%f: hieght %f : Cell %f",table.frame.origin.y,table.frame.size.height,cell.frame.origin.y);
        
        myrect = CGRectOffset(myrect, -table.contentOffset.x, -table.contentOffset.y);
        gblclass.chk_acct_statement=@"1";
        //[self performSegueWithIdentifier:@"acct_statement_detail" sender:nil];
        
        UIViewController *vc3 = [self.storyboard instantiateViewControllerWithIdentifier:@"Act_Statemnt_Summary_ViewController"];
        vc3.modalTransitionStyle = UIModalPresentationCustom;
        vc3.transitioningDelegate = self;
        [self presentViewController:vc3 animated:YES completion:nil];
        
        
        
        
        
        //        [self performSegueWithIdentifier:@"acct_statement_detail" sender:nil];
        //
        //        CATransition *transition = [ CATransition animation ];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
    else
    {
        if([[acc_type objectAtIndex:indexPath.row] isEqualToString:@"UBL UAE CC"]||[[acc_type objectAtIndex:indexPath.row]isEqualToString:@"Credit Card Account"])
        {
            
            cr_card_hidden = @"1";
        }
        else
        {
            cr_card_hidden=@"0";
        }
        
        //NSLog(@"click%@",  [acc_type objectAtIndex:indexPath.row]);
        self.drpdwnview.hidden=true;
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        _txt_account.text=[acc_name objectAtIndex:indexPath.row];
        [self Account_statment_history:gblclass.user_id acct_id:[acc_regid objectAtIndex:indexPath.row]];
    }
}

- (void)yourButtonClicked:(UIButton *)sender
{
    
    CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:self.table];
    NSIndexPath *indexPath1 = [self.table indexPathForRowAtPoint:buttonPosition1];
    [self tableView:self.table didSelectRowAtIndexPath:indexPath1];
    
    gblclass.acct_statmnt_indexpath=cell_index;
    gblclass.chk_acct_statement=@"1";
    [self performSegueWithIdentifier:@"acct_statement_detail" sender:nil];
}




-(void) Account_statment_history:(NSString *)user_id  acct_id:(NSString*)acct_id
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSDate *currentDate = [NSDate date];
        dateComponents_frm = [[NSDateComponents alloc] init];
        [dateComponents_frm setDay:-90];  //-120
        frm_date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents_frm toDate:currentDate options:0];
        //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, frm_date);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy"];
        NSString *dateFormatter_frm = [dateFormatter stringFromDate:frm_date];
        
        NSString *dateFormatter_to = [dateFormatter stringFromDate:currentDate];
        //NSLog(@"%@",dateFormatter_to);
        
        NSDictionary *dictparam;
        NSString* acct_type,*br_code,*acct_no1;
        
        NSLog(@"%lu", (unsigned long)[gblclass.arr_graph_name  count]);
          NSLog(@"%@", gblclass.arr_graph_name);
        if ([gblclass.wiz_card_chck isEqualToString:@"1"])
        {
            acct_type = [gblclass.arr_graph_name objectAtIndex:10];
            br_code = [gblclass.arr_graph_name objectAtIndex:9];
            acct_no1 = [gblclass.arr_graph_name objectAtIndex:8];
        }
        else
        {
            acct_type = gblclass.is_default_acct_type;
            br_code = gblclass.is_default_br_code;
            acct_no1 = acct_no;
        }
        
        if ([chck_datt isEqualToString:@""])
        {
            lbl_dat_frm.text= [NSString stringWithFormat:@"%@",dateFormatter_frm];
            lbl_dat_to.text=[NSString stringWithFormat:@"%@",dateFormatter_to];
            
            //gblclass.M3sessionid
            //gblclass.base_currency
            
            
            
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:user_id],
                          [encrypt encrypt_Data:acct_id],
                          [encrypt encrypt_Data:acct_summary_curr],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:dateFormatter_frm],
                          [encrypt encrypt_Data:dateFormatter_to],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token],
                          [encrypt encrypt_Data:@"1"],
                          [encrypt encrypt_Data:@"2"],
                          [encrypt encrypt_Data:acct_type],
                          [encrypt encrypt_Data:br_code],
                          [encrypt encrypt_Data:acct_no1],
                          [encrypt encrypt_Data:@"123"], nil]
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strFromAccountId",
                                                             @"strCurrency",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"FromDate",
                                                             @"ToDate",
                                                             @"Device_ID",
                                                             @"Token",
                                                             @"sort_order1",
                                                             @"sort_order2",
                                                             @"strAccountType",
                                                             @"strBrCode",
                                                             @"strAccountNo",
                                                             @"strCDG", nil]];
        }
        else
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:user_id],
                          [encrypt encrypt_Data:acct_id],
                          [encrypt encrypt_Data:acct_summary_curr],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:lbl_dat_frm.text],
                          [encrypt encrypt_Data:lbl_dat_to.text],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token],
                          [encrypt encrypt_Data:@"1"],
                          [encrypt encrypt_Data:@"2"],
                          [encrypt encrypt_Data:acct_type],
                          [encrypt encrypt_Data:br_code],
                          [encrypt encrypt_Data:acct_no1],
                          [encrypt encrypt_Data:@"123"], nil]
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strFromAccountId",
                                                             @"strCurrency",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"FromDate",
                                                             @"ToDate",
                                                             @"Device_ID",
                                                             @"Token",
                                                             @"sort_order1",
                                                             @"sort_order2",
                                                             @"strAccountType",
                                                             @"strBrCode",
                                                             @"strAccountNo",
                                                             @"strCDG", nil]];
        }
        
        
        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        //AccountStatementV3
        [manager POST:@"AccountStatementV2" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  //  NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   NSString *jsonString = [encrypt de_crypt_Data:responseObject];
                  //  NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                  //  id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                  //  dic = (NSDictionary *)json;
                  
                  //**          gblclass.arr_graph_name = [[NSMutableArray alloc] init];
                  gblclass.arr_acct_statement_global=[[NSMutableArray alloc] init];
                  gblclass.arr_acct_statement1=[[NSMutableArray alloc] init];
                  [arr_acct_statement removeAllObjects];
                  [gblclass.arr_acct_statement_global removeAllObjects];
                  
//                  NSString* response1 = [dic objectForKey:@"Response"];
//                  
//                
//                  
//                  
//                  NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
//                  
//                  if ([response1 rangeOfCharacterFromSet:set].location == NSNotFound)
//                  {
//                      //NSLog(@"NO SPECIAL CHARACTER");
//                      //return [response1 stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//                      
//                      NSLog(@"number");
//                  }
//                  else
//                  {
//                      //NSLog(@"HAS SPECIAL CHARACTER");
//                      //textField.text=@"";
//                      
//                      NSLog(@"not number");
//                    //  return 0;
//                  }
                  
                  
                  
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  //         account_typedesc=[[NSMutableArray alloc] init];
                  classObj = [[GnbAccountTransactionDetail alloc] initWithDictionary:dic];
                  
 
                  NSMutableArray  *a ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
                  a=[[NSMutableArray alloc] init];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"Exception: Cannot find table 0."])
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=@"No Transaction found in selected date range.";
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      [arr_acct_statement removeAllObjects];
                      [gblclass.arr_acct_statement_global removeAllObjects];
                      lbl_tot_de.text = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,@"0.0"];
                      lbl_tot_cre.text = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,@"0.0"];
                      
                      [table reloadData];
                      
                      [_arrayOfValues removeAllObjects];
                      [arr_graph_datt removeAllObjects];
                      [_arrayOfDates removeAllObjects];
                      
                      [self setDataCount:(double)_arrayOfValues.count range:100.0];
                      
                      [_chartView animateWithXAxisDuration:0.0];
                      
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"Exception: Column 'Description' does not belong to table GnbAccountTransactionDetail."])
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=@"No Transaction found in selected date range.";
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      [arr_acct_statement removeAllObjects];
                      [gblclass.arr_acct_statement_global removeAllObjects];
                      lbl_tot_de.text=@"0.0";
                      lbl_tot_cre.text=@"0.0";
                      
                      [table reloadData];
                      
                      [_arrayOfValues removeAllObjects];
                      [arr_graph_datt removeAllObjects];
                      [_arrayOfDates removeAllObjects];
                      
                      [self setDataCount:(double)_arrayOfValues.count range:100.0];
                      
                      [_chartView animateWithXAxisDuration:0.0];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      arr_graph= [dic objectForKey:@"outdt"];   //Graph Data Load ....
                      
                      gblclass.arr_acct_statement1=
                      [(NSDictionary *)[dic objectForKey:@"outdtDataset2"] objectForKey:@"GnbAccountTransactionDetail2"];    //Statement Data Load ....
                      
                      
                      [_arrayOfValues removeAllObjects];
                      
                      //            NSUInteger *set;
                      for (dic in gblclass.arr_acct_statement1)
                      {
                          NSString* desc = [dic objectForKey:@"Description"];
                          [a addObject:desc];
                          
                          NSString* datt21=[dic objectForKey:@"Date"];
                          
                          
                          NSString *str = datt21;
                          NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                          [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                          
                          NSDate *date = [dateFormat dateFromString:str];
                          
                          //NSLog(@"%@",date);
                          [dateFormat setDateFormat:@"dd-MMM-yyyy"];//  MMM dd,yyyy this match the one you want to be
                          NSString *converted_datt = [dateFormat stringFromDate:date];
                          //NSLog(@"%@", converted_datt);
                          
                          
                          
                          [a addObject:converted_datt];
                          
                          if(!([[dic objectForKey:@"Debit"] length]==0))
                          {
                              NSString* debit=[dic objectForKey:@"Debit"];
                              [a addObject:debit];
                              [a addObject:@"Dr"];
                              de_balc=de_balc+[debit floatValue];
                              
                          }
                          else
                          {
                              NSString* credit=[dic objectForKey:@"Credit"];
                              [a addObject:credit];
                              [a addObject:@"Cr"];
                              cre_balc=cre_balc+[credit floatValue];
                          }
                          
                          NSString* balc=[dic objectForKey:@"Balance"];
                          [a addObject:balc];
                          [_arrayOfValues addObject:[balc stringByReplacingOccurrencesOfString:@"," withString:@""]];
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                          [arr_acct_statement addObject:bbb];
                          [gblclass.arr_acct_statement_global addObject:bbb];
                          
                          
                          [gblclass.arr_graph_name addObject:bbb]; //**
                          
                          
                          //NSLog(@"%@", bbb);
                          
                          [a removeAllObjects];
                          
                          //                  //NSLog(@"%d",set);
                          
                      }
                      
                      // Remove first row for Opening Balance ::::::
                      
                      if ([arr_acct_statement count]>0)
                      {
                          
                          [arr_acct_statement  removeLastObject];
                          [gblclass.arr_acct_statement_global removeLastObject];
                          
                          
                          //[arr_acct_statement removeObjectAtIndex:0];
                          // [gblclass.arr_acct_statement_global removeObjectAtIndex:0];
                      }
                      
                      if ([gblclass.arr_acct_statement1 count] ==0)
                      {
                          //  [self showAlert:@"No Record Found"];
                          
                          //        [self showAlert:@"No Record Found" :@"Attention"];
                          
                          gblclass.custom_alert_msg=@"No Record Found";
                          gblclass.custom_alert_img=@"0";
                          
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                          vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                          vc.view.alpha = alpha1;
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          [arr_acct_statement removeAllObjects];
                          [table reloadData];
                          
                      }
                      
                      
                      NSString *mystring =[NSString stringWithFormat:@"%ld",(long)de_balc];
                      NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
                      NSNumberFormatter *formatter2 = [NSNumberFormatter new];
                      [formatter2 setMinimumFractionDigits:2];
                      [formatter2 setMaximumFractionDigits:2];
                      [formatter2 setNumberStyle:NSNumberFormatterDecimalStyle];
                      
                      
                      lbl_tot_de.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[formatter2 stringFromNumber:number]];
                      
                      NSString *mystring2 =[NSString stringWithFormat:@"%ld",(long)cre_balc];
                      NSNumber *number2 = [NSDecimalNumber decimalNumberWithString:mystring2];
                      NSNumberFormatter *formatter3 = [NSNumberFormatter new];
                      [formatter3 setMinimumFractionDigits:2];
                      [formatter3 setMaximumFractionDigits:2];
                      [formatter3 setNumberStyle:NSNumberFormatterDecimalStyle];
                      
                      lbl_tot_cre.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[formatter3 stringFromNumber:number2]];
                      
                      cre_balc=0;
                      de_balc=0;
                      
                      
//                      NSLog(@"%@",gblclass.arr_graph_name);
                      
                      if ([[gblclass.arr_graph_name objectAtIndex:0] isEqualToString:@"1"])
                      {
                          
                          //NSLog(@"%@",[gblclass.arr_graph_name objectAtIndex:6]);
                          
                          if ([[gblclass.arr_graph_name objectAtIndex:6] isEqualToString:@"Credit Card Account"])
                          {
                              
                              lbl_debit.text=@"Available Limit";
                              lbl_credit.text=@"Outstanding Amount";
                              
//                              NSLog(@"%@",gblclass.arr_graph_name);
                              
                              lbl_tot_de.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[gblclass.arr_graph_name objectAtIndex:8]];
                              
                              lbl_tot_cre.text=[NSString stringWithFormat:@"%@ %@",acct_summary_curr,[gblclass.arr_graph_name objectAtIndex:3]];
                          }
                      }
                      
                      [table reloadData];
                      [self Graph_call];
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@""];
                      
                      [arr_acct_statement removeAllObjects];
                      [gblclass.arr_acct_statement_global removeAllObjects];
                      lbl_tot_de.text=@"0.0";
                      lbl_tot_cre.text=@"0.0";
                      
                      [table reloadData];
                      
 
                      [_arrayOfValues removeAllObjects];
                      [arr_graph_datt removeAllObjects];
                      [_arrayOfDates removeAllObjects];
                      
                      [self setDataCount:(double)_arrayOfValues.count range:100.0];
                      
                      [_chartView animateWithXAxisDuration:0.0];
                      
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
 
                  [self custom_alert:@"Retry" :@""];
    
              }];
        
    }
    @catch (NSException *exception)
    {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
        
    }
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}



-(IBAction)btn_back:(id)sender
{
    
    [gblclass.arr_graph_name  removeAllObjects];
    
    gblclass.landing = @"1";
    
    gblclass.frm_acct_default_chck=@"0";
    
     
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
//    CATransition *transition = [ CATransition animation ];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [ self.view.window. layer addAnimation:transition forKey:nil];
//
//    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    [gblclass.arr_graph_name  removeAllObjects];
    
    gblclass.frm_acct_default_chck=@"0";
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_dropdown:(id)sender
{
    if(self.drpdwnview.hidden)
    {
        self.drpdwnview.hidden = false;
    }
    else
    {
        self.drpdwnview.hidden = true;
    }
}

-(IBAction)btn_click
{
    
    if(self.drpdwnview.hidden)
    {
        self.drpdwnview.hidden = false;
        
    }
    else
    {
        self.drpdwnview.hidden = true;
    }
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(IBAction)btn_Pay:(id)sender
{
    gblclass.frm_acct_default_chck=@"0";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.drpdwnview.hidden=YES;
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        //        [self mob_App_Logout:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}




-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
 
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}






-(IBAction)btn_datt_frm:(id)sender
{
    if (vw_datt.isHidden==YES)
    {
        
        datt_picker.hidden=NO;
        datt_picker_to.hidden=YES;
        
        vw_datt.hidden=NO;
        chck_datt=@"from";
    }
    else
    {
        vw_datt.hidden=YES;
    }
}

-(IBAction)btn_datt_to:(id)sender
{
    if (vw_datt.isHidden==YES)
    {
        
        datt_picker.hidden=YES;
        datt_picker_to.hidden=NO;
        
        
        vw_datt.hidden=NO;
        chck_datt=@"to";
    }
    else
    {
        vw_datt.hidden=YES;
    }
}

-(IBAction)btn_datt_cancel:(id)sender
{
    vw_datt.hidden=YES;
}

-(IBAction)btn_datt_done:(id)sender
{
    
    if ([chck_datt isEqualToString:@"from"])
    {
        
        [datt_picker setMaximumDate:[NSDate date]];
        frm_dat = [[NSDateFormatter alloc] init];
        frm_date = datt_picker.date;
        [frm_dat setDateFormat:@"MMM dd, yyyy"];
        
        NSString *dateString = [frm_dat stringFromDate:frm_date];
        lbl_dat_frm.text = [NSString stringWithFormat:@"%@",dateString];
        
        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
        
        dt_frm=[to_dat dateFromString:lbl_dat_frm.text];
        dt_to=[to_dat dateFromString:lbl_dat_to.text];
        
        vw_datt.hidden=YES;
        
        return ;
    }
    else
    {
        
        [datt_picker setMaximumDate:[NSDate date]];
        to_dat = [[NSDateFormatter alloc] init];
        to_date = datt_picker_to.date;
        [to_dat setDateFormat:@"MMM dd, yyyy"];  //MMM dd,yyyy  "dd-MMM-yyyy
        
        NSString *dateString = [to_dat stringFromDate:to_date];
        lbl_dat_to.text = [NSString stringWithFormat:@"%@",dateString];
        
        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
        
        vw_datt.hidden=YES;
        
        
        dt_frm=[to_dat dateFromString:lbl_dat_frm.text];
        dt_to=[to_dat dateFromString:lbl_dat_to.text];
        
        if ([dt_frm compare:dt_to] == NSOrderedDescending)
        {
            //NSLog(@"date1 is later than date2");
            //NSLog(@"To date is less then From date.");
            
            gblclass.custom_alert_msg=@"To Date is Less than From Date ";
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            return;
        }
        else if ([dt_frm compare:dt_to] == NSOrderedAscending)
        {
            //NSLog(@"date1 is earlier than date2");
            
        }
        else
        {
            //NSLog(@"dates are the same");
        }
        
        
        
        if (to_date > frm_date)
        {
            //NSLog(@">");
        }
        else
        {
            //NSLog(@"<");
        }
        
    }
    
    
    //    NSComparisonResult datt_compare=[lbl_dat_frm.text compare:lbl_dat_to.text];
    
    
}


-(IBAction)btn_datt_click:(id)sender
{
    
    @try {
        
        //NSLog(@"%@",gblclass.defaultaccountno);
        //    [self Account_statment_history:gblclass.user_id acct_id:gblclass.defaultaccountno];
        
        
        //NSLog(@"%@",gblclass.arr_graph_name);
        
        if ([gblclass.arr_graph_name count]>5)
        {
            if ([[gblclass.arr_graph_name objectAtIndex:6] isEqualToString:@"Credit Card Account"])
            {
                lbl_debit.text=@"Available Limit";
                lbl_credit.text=@"Outstanding Amount";
            }
        }
        
        if ([dt_frm compare:dt_to] == NSOrderedDescending)
        {
            //NSLog(@"date1 is later than date2");
            //NSLog(@"To date is less then From date.");
            
            gblclass.custom_alert_msg=@"To Date is Less than From Date ";
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            return;
        }
        else if ([dt_frm compare:dt_to] == NSOrderedAscending)
        {
            //NSLog(@"date1 is earlier than date2");
            
        }
        else
        {
            //NSLog(@"dates are the same");
        }
        
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"acct_statement";
            [self SSL_Call];
        }
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
}






// *********************** For Graph ******************************

- (void)hydrateDatasets1
{
    // Reset the arrays of values (Y-Axis points) and dates (X-Axis points / labels)
    //**    if (!self.arrayOfValues) self.arrayOfValues = [[NSMutableArray alloc] init];
    if (!self.arrayOfDates) self.arrayOfDates = [[NSMutableArray alloc] init];
    
    //**   [self.arrayOfValues removeAllObjects];
    [self.arrayOfDates removeAllObjects];
    
    previousStepperValue = self.graphObjectIncrement.value;
    totalNumber = 0;
    NSDate *baseDate = [NSDate date];
    BOOL showNullValue = true;
    
    // Add objects to the array based on the stepper value
    for (int i = 0; i < [_arrayOfValues count]; i++)
    {
        //NSLog(@"%@",_arrayOfValues);
        
        //  [self.arrayOfValues addObject:@"12"];       //@([self getRandomFloat])]; // Random values for the graph
        if (i == 0)
        {
            [self.arrayOfDates addObject:baseDate]; // Dates for the X-Axis of the graph
        }
        else if (showNullValue && i == 4)
        {
            [self.arrayOfDates addObject:[self dateForGraphAfterDate:self.arrayOfDates[i-1]]]; // Dates for the X-Axis of the graph
            
            self.arrayOfValues[i] = @(BEMNullGraphValue);
            //NSLog(@"%@",_arrayOfValues);
        }
        else
        {
            [self.arrayOfDates addObject:[self dateForGraphAfterDate:self.arrayOfDates[i-1]]]; // Dates for the X-Axis of the graph
        }
        
        totalNumber = totalNumber + [[self.arrayOfValues objectAtIndex:i] intValue]; // All of the values added together
    }
}


- (void)hydrateDatasets
{
    
    // Reset the arrays of values (Y-Axis points) and dates (X-Axis points / labels)
    //    if (!arr_graph_balc)
    //        arr_graph_balc = [[NSMutableArray alloc] init];
    
    //    if (!arr_graph_datt)
    //        arr_graph_datt = [[NSMutableArray alloc] init];
    
    //  [arr_graph_balc removeAllObjects];
    //   [arr_graph_datt removeAllObjects];
    
    previousStepperValue = self.graphObjectIncrement.value;
    totalNumber = 0;
    NSDate *baseDate = [NSDate date];
    //    BOOL showNullValue = true;
    
    
    
    //********************** LINE VALUE GET ****************************
    
    
    //  _arrayOfValues=@[@"12.34",@"15.34",@"19",@"12",@"30.34",@"35",@"45.34",@"56.34"];
    
    
    
    
    
    //  _arrayOfDates=@[@"Jan",@"Feb",@"March",@"April",@"May",@"June",@"July",@"Aug",@"Sept",@"Sept"];
    
    // Add objects to the array based on the stepper value
    for (int i = 0; i < [_arrayOfValues count]; i++)
    {
        //[self.arrayOfValues addObject:@([self getRandomFloat])]; // Random values for the graph
        
        
        //NSLog(@"%@", self.arrayOfDates[i]);
        
        if (i == 0)
        {
            [self.arrayOfDates addObject:baseDate]; // Dates for the X-Axis of the graph
            //NSLog(@"%@",_arrayOfValues);
            
        }
        
        
        //        else if (showNullValue && i == 4)
        //        {
        //            [self.arrayOfDates addObject:[self dateForGraphAfterDate:self.arrayOfDates[i-1]]]; // Dates for the X-Axis of the graph
        //
        //         //   self.arrayOfValues[i] = @(BEMNullGraphValue);
        //
        //             //NSLog(@"%@",_arrayOfValues);
        //        }
        else
        {
            
            [self.arrayOfDates addObject:[self dateForGraphAfterDate:self.arrayOfDates[i-1]]]; // Dates for the X-Axis of the graph
            
            //NSLog(@"%@",_arrayOfDates);
        }
        
        //  //NSLog(@"%@",[arr_graph_balc objectAtIndex:i]);
        
        //NSLog(@"%@",_arrayOfValues);
        
        //**     totalNumber = totalNumber + [[_arrayOfValues objectAtIndex:i] intValue]; // All of the values added together
        
        //NSLog(@"%d",totalNumber);
    }
    
    //********************** END LINE VALUE GET ****************************
    
    
}

-(void)Graph_load
{
    //For Graph :::
    
    datt=[[NSMutableArray alloc] init];
    datt=@[@"20-jun",@"21-jun",@"22-jun",@"23-jun",@"24-jun",@"25-jun",@"26-jun",@"27-jun"];
    
    
    [self hydrateDatasets];
    
    // Create a gradient to apply to the bottom portion of the graph
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = {
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 0.0
    };
    
    // Apply the gradient to the bottom portion of the graph
    self.myGraph.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
    
    // Enable and disable various graph properties and axis displays
    self.myGraph.enableTouchReport = YES;
    self.myGraph.enablePopUpReport = YES;
    self.myGraph.enableYAxisLabel = YES;
    self.myGraph.autoScaleYAxis = YES;
    self.myGraph.alwaysDisplayDots = NO;
    self.myGraph.enableReferenceXAxisLines = YES;
    self.myGraph.enableReferenceYAxisLines = YES;
    self.myGraph.enableReferenceAxisFrame = YES;
    
    // Draw an average line
    self.myGraph.averageLine.enableAverageLine = YES;
    self.myGraph.averageLine.alpha = 0.6;
    self.myGraph.averageLine.color = [UIColor darkGrayColor];
    self.myGraph.averageLine.width = 1.5;
    self.myGraph.averageLine.dashPattern = @[@(1),@(1)];
    
    // Set the graph's animation style to draw, fade, or none
    self.myGraph.animationGraphStyle = BEMLineAnimationDraw;
    
    // Dash the y reference lines
    self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
    
    // Show the y axis values with this format string
    self.myGraph.formatStringForValues = @"%.1f";
    
    // Setup initial curve selection segment
    self.curveChoice.selectedSegmentIndex = self.myGraph.enableBezierCurve;
    
    // The labels to report the values of the graph when the user touches it
    self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
    
    // self.labelDates.text = @"between now and later";
    
    
}



- (NSDate *)dateForGraphAfterDate:(NSDate *)date
{
    
    NSTimeInterval secondsInTwentyFourHours = 24 * 60 * 60;
    NSDate *newDate = [date dateByAddingTimeInterval:secondsInTwentyFourHours];
    return newDate;
    
    
    //    NSTimeInterval secondsInTwentyFourHours = 24 * 60 * 60;
    //
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    //
    //    // NSString *dateFormatter_frm = [dateFormatter stringFromDate:secondsInTwentyFourHours];
    //
    ////    NSDate *newDate = [date dateByAddingTimeInterval:secondsInTwentyFourHours];  //secondsInTwentyFourHours
    ////    NSString *dateFormatter_frm = [dateFormatter stringFromDate:newDate];
    ////    NSDate* datt =[dateFormatter dateFromString:dateFormatter_frm];
    //
    //    //NSLog(@"%@",datt);
    //
    //    return date;
}

- (NSString *)labelForDateAtIndex:(NSInteger)index
{
    
    // NSDate *date = self.arrayOfDates[index];
    
    //   NSDate* date= arr_graph_datt[index];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"MM-dd";    //@"MMM/dd"
    //    NSString *label = [df stringFromDate:date];
    
    if ([arr_graph_datt count] < index)
    {
        index=[arr_graph_datt count];
    }
    
    NSString* label1=arr_graph_datt[index];
    return [label1 substringWithRange:NSMakeRange(0,6)];
    
    
    
    //    //NSDate *date = self.arrayOfDates[index];
    //
    //
    //    NSDate* date= [datt objectAtIndex:index];  //[arr_graph_datt objectAtIndex:index];
    //
    //
    //    //NSLog(@"%@",date);
    //
    //    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //    df.dateFormat = @"MMM/dd";
    //    NSString *label =[datt objectAtIndex:index]; //[df stringFromDate:date];
    //
    //    return label;
}

#pragma mark - Graph Actions

// Refresh the line graph using the specified properties
- (IBAction)refresh:(id)sender
{
    [self hydrateDatasets];
    
    UIColor *color;
    if (self.graphColorChoice.selectedSegmentIndex == 0) color = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
    else if (self.graphColorChoice.selectedSegmentIndex == 1) color = [UIColor colorWithRed:255.0/255.0 green:187.0/255.0 blue:31.0/255.0 alpha:1.0];
    else if (self.graphColorChoice.selectedSegmentIndex == 2) color = [UIColor colorWithRed:0.0 green:140.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    self.myGraph.enableBezierCurve = (BOOL) self.curveChoice.selectedSegmentIndex;
    self.myGraph.colorTop = color;
    self.myGraph.colorBottom = color;
    self.myGraph.backgroundColor = color;
    self.view.tintColor = color;
    self.labelValues.textColor = color;
    self.navigationController.navigationBar.tintColor = color;
    
    self.myGraph.animationGraphStyle = BEMLineAnimationFade;
    [self.myGraph reloadGraph];
}

- (float)getRandomFloat
{
    float i1 = (float)(arc4random() % 1000000) / 100 ;
    return i1;
}

- (IBAction)addOrRemovePointFromGraph:(id)sender
{
    if (self.graphObjectIncrement.value > previousStepperValue)
    {
        // Add point
        [self.arrayOfValues addObject:@([self getRandomFloat])];
        NSDate *newDate = [self dateForGraphAfterDate:(NSDate *)[self.arrayOfDates lastObject]];
        [self.arrayOfDates addObject:newDate];
        [self.myGraph reloadGraph];
        
    } else if (self.graphObjectIncrement.value < previousStepperValue)
    {
        // Remove point
        [self.arrayOfValues removeObjectAtIndex:0];
        [self.arrayOfDates removeObjectAtIndex:0];
        [self.myGraph reloadGraph];
    }
    
    previousStepperValue = self.graphObjectIncrement.value;
}

- (IBAction)displayStatistics:(id)sender
{
    [self performSegueWithIdentifier:@"showStats" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    //    if ([segue.identifier isEqualToString:@"showStats"]) {
    //        StatsViewController *controller = segue.destinationViewController;
    //        controller.standardDeviation = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculateLineGraphStandardDeviation] floatValue]];
    //        controller.average = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculatePointValueAverage] floatValue]];
    //        controller.median = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculatePointValueMedian] floatValue]];
    //        controller.mode = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculatePointValueMode] floatValue]];
    //        controller.minimum = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculateMinimumPointValue] floatValue]];
    //        controller.maximum = [NSString stringWithFormat:@"%.2f", [[self.myGraph calculateMaximumPointValue] floatValue]];
    //        controller.snapshotImage = [self.myGraph graphSnapshotImage];
    //    }
}


#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph
{
    //NSLog(@"%lu",(unsigned long)[self.arrayOfValues count]);  //[self.arrayOfValues count]
    return (int)[self.arrayOfValues count];
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index
{
    //NSLog(@"%f",[[self.arrayOfValues objectAtIndex:index] doubleValue]);
    return [[self.arrayOfValues objectAtIndex:index] doubleValue];
}

# pragma mark - SimpleLineGraph Delegate

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index
{
    
    //NSLog(@"%@",_arrayOfDates);
    
    
    NSString *label1 = [self labelForDateAtIndex:index];
    
    //NSLog(@"%@",[label1 stringByReplacingOccurrencesOfString:@" " withString:@"\n"]);
    
    
    return [label1 stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
    
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index
{
    
    @try {
        
        
        //NSLog(@"%ld",(long)index);
        
        if ([self.arrayOfValues count] <index)
        {
            self.labelValues.text = [NSString stringWithFormat:@"%@", [self.arrayOfValues objectAtIndex:index]];
            
            //NSLog(@"%@",[NSString stringWithFormat:@"%@", [self.arrayOfValues objectAtIndex:index]]);
        }
        
        //    self.labelDates.text = [NSString stringWithFormat:@"in %@", [self labelForDateAtIndex:index]];
        //     //NSLog(@"%@",[NSString stringWithFormat:@"in %@", [self labelForDateAtIndex:index]]);
        
    } @catch (NSException *exception)
    {
        
    }
    
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.labelValues.alpha = 0.0;
        self.labelDates.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        //self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
        // self.labelDates.text = [NSString stringWithFormat:@"between %@ and %@", [self labelForDateAtIndex:0], [self labelForDateAtIndex:self.arrayOfDates.count - 1]];
        
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.labelValues.alpha = 1.0;
            self.labelDates.alpha = 1.0;
        } completion:nil];
    }];
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph
{
    if ([arr_graph_balc count]>0)
    {
        
        self.labelValues.text = [NSString stringWithFormat:@"%i", [[self.myGraph calculatePointValueSum] intValue]];
        self.labelDates.text = [NSString stringWithFormat:@"between %@ and %@", [self labelForDateAtIndex:0], [self labelForDateAtIndex:arr_graph_datt.count - 1]];  //self.arrayOfDates.count - 1
    }
}


/* - (void)lineGraphDidFinishDrawing:(BEMSimpleLineGraphView *)graph {
 // Use this method for tasks after the graph has finished drawing
 } */

- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph
{
    return @"   PKR";
}



#pragma mark - Optional Datasource Customizations
/*
 This section holds a bunch of graph customizations that can be made.  They are commented out because they aren't required.  If you choose to uncomment some, they will override some of the other delegate and datasource methods above.
 
 */

- (NSInteger)baseIndexForXAxisOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    return 0;
}

- (NSInteger)incrementIndexForXAxisOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    return 2;
}

- (NSArray *)incrementPositionsForXAxisOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    NSMutableArray *positions = [NSMutableArray array];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger previousDay = -1;
    
    //NSLog(@"%@",self.arrayOfDates);
    
    for(int i = 0; i < self.arrayOfDates.count; i++)
    {
        NSDate *date = self.arrayOfDates[i];
        NSDateComponents * components = [calendar components:NSCalendarUnitDay fromDate:date];
        NSInteger day = components.day;
        
        if(day != previousDay)
        {
            [positions addObject:@(i)];
            previousDay = day;
        }
    }
    return positions;
    
}

- (CGFloat)baseValueForYAxisOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    NSNumber *minValue = [graph calculateMinimumPointValue];
    //Let's round our value down to the nearest 100
    double min = minValue.doubleValue;
    double roundPrecision = 100;
    double offset = roundPrecision / 2;
    double roundedVal = round((min - offset) / roundPrecision) * roundPrecision;
    
    return roundedVal;
}

- (CGFloat)incrementValueForYAxisOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    NSNumber *minValue = [graph calculateMinimumPointValue];
    NSNumber *maxValue = [graph calculateMaximumPointValue];
    double range = maxValue.doubleValue - minValue.doubleValue;
    
    float increment = 1.0;
    if (range <  10) {
        increment = 2;
    } else if (range < 100) {
        increment = 10;
    } else if (range < 500) {
        increment = 50;
    } else if (range < 1000) {
        increment = 100;
    } else if (range < 5000) {
        increment = 500;
    } else {
        increment = 1000;
    }
    
    return increment;
}




-(void)Graph_call
{
    
    @try {
        
        
        
        
        //    if ([str_response isEqualToString:@"0"])
        //    {
        
        //        arr_graph= [dic objectForKey:@"outdt"];
        
        //NSLog(@"%@", arr_graph);
        
        //NSLog(@"Done");
        
        
        //         account_typedesc=[[NSMutableArray alloc] init];
        classObj = [[GnbAccountTransactionDetail alloc] initWithDictionary:dic];
        
        NSMutableArray  *a ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
        a=[[NSMutableArray alloc] init];
        
        NSMutableArray  *b ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
        b=[[NSMutableArray alloc] init];
        
        //NSLog(@"%lu",(unsigned long)[arr_graph count]);
        
        [_arrayOfDates removeAllObjects];
        [_arrayOfValues removeAllObjects];
        
        if ([arr_graph count]>0)
        {
            
            for (dic in arr_graph)
            {
                NSString* datttt=[dic objectForKey:@"Date"];
                [_arrayOfDates addObject:datttt];
                [arr_graph_datt addObject:datttt];
                
                //
                
                NSString* balc=[dic objectForKey:@"Balance"];
                //       [_arrayOfValues addObject:balc];
                [_arrayOfValues addObject:[balc stringByReplacingOccurrencesOfString:@"," withString:@""]];
                
                //    NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                
                //    [arr_graph_datt addObject:a];
                // [arr_acct_statement addObject:bbb];
                //[gblclass.arr_acct_statement_global addObject:bbb];
                //      //NSLog(@"%@", bbb);
                
                [a removeAllObjects];
                
            }
            
            [self setDataCount:(double)_arrayOfValues.count range:100.0];
            
            //      [_chartView animateWithXAxisDuration:3.0 easingOption:ChartEasingOptionEaseInBack];
            
            //[self Graph_load];
            
            
            //  [self hydrateDatasets1];
            
            
            
            //JAHANGIR GRAPH
            CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
            size_t num_locations = 2;
            CGFloat locations[2] = { 0.0, 1.0 };
            CGFloat components[8] = {
                1.0, 1.0, 1.0, 1.0,
                1.0, 1.0, 1.0, 0.0
            };
            
            // Apply the gradient to the bottom portion of the graph
            //self.myGraph.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
            
            
            UIColor *color;
            //if (self.graphColorChoice.selectedSegmentIndex == 0) color = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
            //else if (self.graphColorChoice.selectedSegmentIndex == 1) color = [UIColor colorWithRed:255.0/255.0 green:187.0/255.0 blue:31.0/255.0 alpha:1.0];
            //else if (self.graphColorChoice.selectedSegmentIndex == 2) color = [UIColor colorWithRed:0.0 green:140.0/255.0 blue:255.0/255.0 alpha:1.0];
            color = [UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
            //  [UIColor colorWithRed:0.0 green:0/255.0 blue:255.0/255.0 alpha:1.0];
            //color = [UIColor clearColor];
            self.myGraph.enableBezierCurve = YES;
            self.myGraph.colorTop = color;
            self.myGraph.colorBottom = color;
            self.myGraph.backgroundColor = color;
            self.view.tintColor = color;
            self.labelValues.textColor = color;
            self.navigationController.navigationBar.tintColor = color;
            
            // Enable and disable various graph properties and axis displays
            self.myGraph.enableTouchReport = YES;
            self.myGraph.enablePopUpReport = YES;
            self.myGraph.enableYAxisLabel = NO;
            self.myGraph.enableXAxisLabel = YES;
            self.myGraph.autoScaleYAxis = YES;
            self.myGraph.alwaysDisplayDots = YES;
            self.myGraph.enableReferenceXAxisLines = YES;
            self.myGraph.enableReferenceYAxisLines = YES;
            self.myGraph.enableReferenceAxisFrame = YES;
            
            // Draw an average line
            self.myGraph.averageLine.enableAverageLine = NO;
            self.myGraph.averageLine.alpha = 0.6;
            self.myGraph.averageLine.color = [UIColor darkGrayColor];
            self.myGraph.averageLine.width = 0.5;
            self.myGraph.averageLine.dashPattern = @[@(2),@(2)];
            
            // Set the graph's animation style to draw, fade, or none
            self.myGraph.animationGraphStyle = BEMLineAnimationDraw;
            
            // Dash the y reference lines
            self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
            
            // Show the y axis values with this format string
            self.myGraph.formatStringForValues = @"%.2f";
            
            // Setup initial curve selection segment
            //self.curveChoice.selectedSegmentIndex = self.myGraph.enableBezierCurve;
            //Jahangir till here
            
            [self.myGraph reloadGraph];
            [hud hideAnimated:YES];
            
        }
        
        [hud hideAnimated:YES];
        
    }
    @catch (NSException *exception)
    {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
//        gblclass.custom_alert_msg=@"Try again later.";
//        gblclass.custom_alert_img=@"0";
//        
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}

//}






-(void) Graph:(NSString *)user_id  acct_id:(NSString*)acct_id
{
    
    
    @try {
        
        
        
        //    [self.view addSubview:hud];
        //    [hud show:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //  // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        
        NSDate *currentDate = [NSDate date];
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:-90];  //-120
        NSDate *sevenDaysAgo = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
        //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, sevenDaysAgo);
        
        
        to_dat  = [[NSDateFormatter alloc] init];
        [to_dat setDateFormat:@"dd-MMM-yyyy"];
        NSString *dateFormatter_frm = [to_dat stringFromDate:sevenDaysAgo];
        
        NSString *dateFormatter_to = [to_dat stringFromDate:currentDate];
        
        //NSLog(@"%@",dateFormatter_to);
        
        
        
        
        NSDictionary *dictparam;
        
        if ([chck_datt isEqualToString:@""])
        {
            lbl_dat_frm.text= [NSString stringWithFormat:@"%@",dateFormatter_frm];
            lbl_dat_to.text=[NSString stringWithFormat:@"%@",dateFormatter_to];
            
            //gblclass.M3sessionid
            
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:user_id],
                          [encrypt encrypt_Data:acct_id],
                          [encrypt encrypt_Data:gblclass.base_currency],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:dateFormatter_frm],
                          [encrypt encrypt_Data:dateFormatter_to],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strFromAccountId",
                                                             @"strCurrency",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"FromDate",
                                                             @"ToDate",
                                                             @"Device_ID",
                                                             @"Token", nil]];
        }
        else
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:user_id],
                          [encrypt encrypt_Data:acct_id],
                          [encrypt encrypt_Data:gblclass.base_currency],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:lbl_dat_frm.text],
                          [encrypt encrypt_Data:lbl_dat_to.text],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.token], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strFromAccountId",
                                                             @"strCurrency",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"FromDate",
                                                             @"ToDate",
                                                             @"Device_ID",
                                                             @"Token", nil]];
        }
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"AccountStatementV2" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  //NSError *error;
                  //  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  dic=(NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSString* str_response;
                  str_response=[dic objectForKey:@"Response"];
                  
                  
                  if ([str_response isEqualToString:@"0"])
                  {
                      
                      arr_graph= [dic objectForKey:@"outdt"];
                      
                      //NSLog(@"%@", arr_graph);
                      
                      //NSLog(@"Done");
                      
                      
                      //         account_typedesc=[[NSMutableArray alloc] init];
                      classObj = [[GnbAccountTransactionDetail alloc] initWithDictionary:dic];
                      
                      NSMutableArray  *a ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
                      a=[[NSMutableArray alloc] init];
                      
                      NSMutableArray  *b ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
                      b=[[NSMutableArray alloc] init];
                      
                      //NSLog(@"%lu",(unsigned long)[arr_graph count]);
                      
                      [_arrayOfDates removeAllObjects];
                      [_arrayOfValues removeAllObjects];
                      
                      if ([arr_graph count]>0)
                      {
                          
                          for (dic in arr_graph)
                          {
                              NSString* datttt=[dic objectForKey:@"Date"];
                              [_arrayOfDates addObject:datttt];
                              [arr_graph_datt addObject:datttt];
                              
                              //
                              
                              NSString* balc=[dic objectForKey:@"Balance"];
                              //       [_arrayOfValues addObject:balc];
                              [_arrayOfValues addObject:[balc stringByReplacingOccurrencesOfString:@"," withString:@""]];
                              
                              //    NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                              
                              //    [arr_graph_datt addObject:a];
                              // [arr_acct_statement addObject:bbb];
                              //[gblclass.arr_acct_statement_global addObject:bbb];
                              //      //NSLog(@"%@", bbb);
                              
                              [a removeAllObjects];
                              
                          }
                          
                          [self setDataCount:(double)_arrayOfValues.count range:100.0];
                          
                          //      [_chartView animateWithXAxisDuration:3.0 easingOption:ChartEasingOptionEaseInBack];
                          
                          //[self Graph_load];
                          
                          
                          //  [self hydrateDatasets1];
                          
                          
                          
                          //JAHANGIR GRAPH
                          CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
                          size_t num_locations = 2;
                          CGFloat locations[2] = { 0.0, 1.0 };
                          CGFloat components[8] = {
                              1.0, 1.0, 1.0, 1.0,
                              1.0, 1.0, 1.0, 0.0
                          };
                          
                          // Apply the gradient to the bottom portion of the graph
                          //self.myGraph.gradientBottom = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
                          
                          
                          UIColor *color;
                          //if (self.graphColorChoice.selectedSegmentIndex == 0) color = [UIColor colorWithRed:31.0/255.0 green:187.0/255.0 blue:166.0/255.0 alpha:1.0];
                          //else if (self.graphColorChoice.selectedSegmentIndex == 1) color = [UIColor colorWithRed:255.0/255.0 green:187.0/255.0 blue:31.0/255.0 alpha:1.0];
                          //else if (self.graphColorChoice.selectedSegmentIndex == 2) color = [UIColor colorWithRed:0.0 green:140.0/255.0 blue:255.0/255.0 alpha:1.0];
                          color = [UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
                          //  [UIColor colorWithRed:0.0 green:0/255.0 blue:255.0/255.0 alpha:1.0];
                          //color = [UIColor clearColor];
                          self.myGraph.enableBezierCurve = YES;
                          self.myGraph.colorTop = color;
                          self.myGraph.colorBottom = color;
                          self.myGraph.backgroundColor = color;
                          self.view.tintColor = color;
                          self.labelValues.textColor = color;
                          self.navigationController.navigationBar.tintColor = color;
                          
                          // Enable and disable various graph properties and axis displays
                          self.myGraph.enableTouchReport = YES;
                          self.myGraph.enablePopUpReport = YES;
                          self.myGraph.enableYAxisLabel = NO;
                          self.myGraph.enableXAxisLabel = YES;
                          self.myGraph.autoScaleYAxis = YES;
                          self.myGraph.alwaysDisplayDots = YES;
                          self.myGraph.enableReferenceXAxisLines = YES;
                          self.myGraph.enableReferenceYAxisLines = YES;
                          self.myGraph.enableReferenceAxisFrame = YES;
                          
                          // Draw an average line
                          self.myGraph.averageLine.enableAverageLine = NO;
                          self.myGraph.averageLine.alpha = 0.6;
                          self.myGraph.averageLine.color = [UIColor darkGrayColor];
                          self.myGraph.averageLine.width = 0.5;
                          self.myGraph.averageLine.dashPattern = @[@(2),@(2)];
                          
                          // Set the graph's animation style to draw, fade, or none
                          self.myGraph.animationGraphStyle = BEMLineAnimationDraw;
                          
                          // Dash the y reference lines
                          self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
                          
                          // Show the y axis values with this format string
                          self.myGraph.formatStringForValues = @"%.2f";
                          
                          // Setup initial curve selection segment
                          //self.curveChoice.selectedSegmentIndex = self.myGraph.enableBezierCurve;
                          //Jahangir till here
                          
                          [self.myGraph reloadGraph];
                          
                      }
                      
                  }
                  
                  //NSLog(@"%@",arr_graph_datt);
                  //NSLog(@"%@",arr_graph_balc);
                  //NSLog(@"%@",_arrayOfValues);
                  
                  [table reloadData];
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  
                  gblclass.custom_alert_msg=@"Retry";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];//502931109944
        
        
    }
    @catch (NSException *exception)
    {
        gblclass.custom_alert_msg=@"Try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


-(void)loadChartView {
    
    if (_arrayOfDates.count == 0 || _arrayOfDates == nil)
    {
        return;
    }
    
    
    _chartView.xAxis.labelCount = 5;
    _chartView.xAxis.labelPosition = XAxisLabelPositionBottom;
    _chartView.xAxis.drawLabelsEnabled = YES;
    _chartView.xAxis.labelTextColor = [UIColor whiteColor];
    _chartView.xAxis.gridLineWidth = 1;
    _chartView.xAxis.axisLineWidth = 1;
    _chartView.xAxis.gridColor = [UIColor whiteColor];
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}


-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        NSString* account_id;
        if ([chk_ssl isEqualToString:@"acct_statement"])
        {
            chk_ssl=@"";
            
//            NSLog(@"%@",gblclass.arr_graph_name);
            
            if ([gblclass.arr_graph_name count]>0)
            {
                if ([[gblclass.arr_graph_name objectAtIndex:0] isEqualToString:@"1"])
                {
                    account_id = [gblclass.arr_graph_name objectAtIndex:2];
                }
                else
                {
                    account_id =gblclass.is_default_acct_id;   //gblclass.defaultaccountno
                }
                
            }
            else
            {
                account_id = gblclass.defaultaccountno;
            }
            
            
//            NSLog(@"%@",gblclass.arr_graph_name);
            [self Account_statment_history:gblclass.user_id acct_id:account_id];
            
            
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
            
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    NSString* account_id;
    [self.connection cancel];
    
    //NSLog(@"%@",gblclass.arr_graph_name);
    
    if ([chk_ssl isEqualToString:@"acct_statement"])
    {
        chk_ssl=@"";
        
//        NSLog(@"%@",gblclass.arr_graph_name);
        
        if ([gblclass.arr_graph_name count]>0)
        {
            if ([[gblclass.arr_graph_name objectAtIndex:0] isEqualToString:@"1"])
            {
                account_id = [gblclass.arr_graph_name objectAtIndex:2];
            }
            else
            {
                account_id =gblclass.is_default_acct_id;   //gblclass.defaultaccountno
            }
            
        }
        else
        {
            account_id = gblclass.defaultaccountno;
        }
        
        
//        NSLog(@"%@",gblclass.user_id);
        [self Account_statment_history:gblclass.user_id acct_id:account_id];
        //        [self.connection cancel];
        
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
        //        [self.connection cancel];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (error.code == NSURLErrorTimedOut)
        // handle error as you want
        [hud hideAnimated:YES];
    [self custom_alert:error.localizedDescription :@"0"];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
    
    [self.connection cancel];
    
    [hud hideAnimated:YES];
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


- (BOOL)gestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UISwipeGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

 
- (void)createPDFFromTableview:(UITableView *)tableview
{
    [tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    [self renderTableView:tableview];
    
    int rows = [tableview numberOfRowsInSection:0];
    int numberofRowsInView = 17;
    for (int i = 0; i < rows/numberofRowsInView; i++) {
        [tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(i+1)*numberofRowsInView inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        [self renderTableView:tableview];
    }
}

- (void)renderTableView:(UITableView*)tableview
{
    UIGraphicsBeginImageContext(tableview.frame.size);
    [tableview.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *tableviewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginPDFPage();
    [tableviewImage drawInRect:tableview.frame];
}




- (IBAction)clickMe:(id)sender {
    
    
    @try {
        
        
        
        [hud showAnimated:YES];
        
        // save all table
        CGRect frame = table.frame;
        frame.size.height = table.contentSize.height;
        //  table.frame = frame;
        
        CGRect priorBounds = table.bounds;
        CGSize fittedSize = [table sizeThatFits:CGSizeMake(priorBounds.size.width, table.contentSize.height)];
        table.bounds = CGRectMake(0, 0, fittedSize.width, fittedSize.height);
        
        CGRect pdfPageBounds = CGRectMake(0, 0, frame.size.width, frame.size.height); // Change this as your need
        NSMutableData *pdfData = [[NSMutableData alloc] init];
        
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil); {
            for (CGFloat pageOriginY = 0; pageOriginY < fittedSize.height; pageOriginY += pdfPageBounds.size.height) {
                UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil);
                
                CGContextSaveGState(UIGraphicsGetCurrentContext()); {
                    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, -pageOriginY);
                    [table.layer renderInContext:UIGraphicsGetCurrentContext()];
                } CGContextRestoreGState(UIGraphicsGetCurrentContext());
            }
        } UIGraphicsEndPDFContext();
        
        table.bounds = priorBounds; // Reset the tableView
        
        NSString *filePathPDF = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        
        
        NSDate* dattttt = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
        NSString *dateFormatter_frm = [dateFormatter stringFromDate:frm_date];
        
        NSString *dateFormatter_to = [dateFormatter stringFromDate:dattttt];
        //NSLog(@"%@",dateFormatter_to);
        
        NSString* file_name=[NSString stringWithFormat:@"Acct_statemnt_%@%@",dateFormatter_to,@".pdf"];
        
        // Use the pdfData to
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        filePathPDF = [documentsPath stringByAppendingPathComponent:file_name]; //Add the file name
        BOOL written = [pdfData writeToFile:filePathPDF atomically:YES];
        
        
        
        if(written)
        {
            [hud hideAnimated:YES];
            //NSLog(@"saved.");
            [self custom_alert:@"Saved sucessfully." :@"1"];
        }
        else
        {
            [hud hideAnimated:YES];
            //NSLog(@"not saved");
            [self custom_alert:@"Not Save,Try again later." :@"0"];
        }
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
    
    //    UIGraphicsBeginImageContext(table.contentSize);
    //    [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //    [table.layer renderInContext:UIGraphicsGetCurrentContext()];
    //
    //    int rows = [table numberOfRowsInSection:0];
    //    int numberofRowsInView = 4;
    //    for (int i =0; i < rows/numberofRowsInView; i++) {
    //        [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(i+1)*numberofRowsInView inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //        [table.layer renderInContext:UIGraphicsGetCurrentContext()];
    //
    //    }
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIImageView *myImage=[[UIImageView alloc]initWithImage:image];
    //    UIGraphicsEndImageContext();
    //
    //
    //    [self createPDFfromUIView:myImage saveToDocumentsWithFileName:@"img02.pdf"];
    
    
    
    
    
    //    UIGraphicsBeginImageContext(table.contentSize);
    //    [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //    [table.layer renderInContext:UIGraphicsGetCurrentContext()];
    //
    //    int rows = [table numberOfRowsInSection:0];
    //    int numberofRowsInView = 4;
    //
    //    for (int i =0; i < rows/numberofRowsInView; i++)
    //    {
    //        [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(i+1)*numberofRowsInView inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    //        [table.layer renderInContext:UIGraphicsGetCurrentContext()];
    //
    //    }
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIImageView *myImage=[[UIImageView alloc]initWithImage:image];
    //    UIGraphicsEndImageContext();
    //
    //
    //    [self createPDFfromUIView:myImage saveToDocumentsWithFileName:@"omg04.pdf"];
    
    
}

- (NSMutableData*)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    //NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
    
    return pdfData;
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end

