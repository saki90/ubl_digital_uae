//
//  Find_Us_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TBXML.h"
#import "BranchesObject.h"
#import "ParkPlaceMark.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "RadioButton.h"
#import "MyAnnoo.h"
#import <CoreLocation/CoreLocation.h>



@interface Find_Us_VC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,MBProgressHUDDelegate,RadioButtonDelegate>
{
    MKMapView *map;
    MBProgressHUD *HUD;
    NSFileManager *fileManager;
    CLLocationManager *locationManager;
    IBOutlet UITextField *city;
    IBOutlet UITextField *area;
    IBOutlet UIButton* searchButton;
    IBOutlet UILabel * bnk_Branch,*bnk_City,*bnk_Faxno,*bnk_Phoneno;
    IBOutlet UIView *bnk_InfoPage;
    IBOutlet UIButton *bnk_Close;
    NSString *documentDirectory;
    NSString *infoPath;
    NSMutableDictionary *citiesBranches,*totalBranchesInfo;
    NSMutableArray *pastUrls,*karachiCityArray,*selectedCityBranches;
    NSMutableArray *autocompleteUrls,*bankInfo,*banksPlotingPoditions,*citiesArray;
    __weak IBOutlet UIButton *btn_GetDirection;
    NSArray *arrRoutePoints;
    MKPolyline *objPolyline,*previousPolyline;
    UITableView *autocompleteTableView,*branchesTableView;
    CLLocationCoordinate2D originValue,destinationValue;
    
    
    
    //  CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    
    CLLocationManager *locationManager1;
    CLGeocoder *geocoder;
    int locationFetchCounter;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}

@property (nonatomic, strong) NSXMLParser *xmlParser;

@property (nonatomic, strong) NSMutableArray *arrNeighboursData;

@property (nonatomic, strong) NSMutableDictionary *dictTempDataStorage;

@property (nonatomic, strong) NSMutableString *foundValue;

@property (nonatomic, strong) NSString *currentElement;


@property (nonatomic,strong) CLLocationManager *locationManager_s;


@property (nonatomic,strong) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UIView *mapBar;
@property (strong, nonatomic) IBOutlet UIView *searchBar;
@property (nonatomic, assign) BOOL netAvailable;
@property (nonatomic, retain) MBProgressHUD *HUD;
@property (nonatomic, retain) NSString *isATM;
@property (nonatomic, retain) NSMutableArray *pastUrls,*annotationArray;
@property (nonatomic, retain) NSMutableArray *autocompleteUrls,*autocompleteBranchesUrls,*selectedCityBranches;
@property (nonatomic, retain) UITableView *autocompleteTableView,*branchesTableView;
@property (nonatomic, strong) MKPolyline *objPolyline;
@property (nonatomic, strong) UIImage *pinkAnotImage;


@property (nonatomic,strong) IBOutlet UILabel *lbl_Faxno,*lbl_Phoneno;
@property (nonatomic,strong) IBOutlet UITextField *city;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *indicatorView;
@property (nonatomic,strong) IBOutlet UITextField *area;

@property (nonatomic, retain) IBOutlet MKMapView *map;
@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic,strong) IBOutlet UIButton *back;
@property (nonatomic,strong) RadioButton *rb0 ,*rb1,*rb2,*rb3;
-(void)backbp;



- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring:(UITextField *)fieldName;
- (void) updateInterfaceWithReachability: (Reachability*) curReach;
-(void)checkinternet;
-(void)beforeViewDidLoad;
-(IBAction)search:(UIButton*)seder;
- (IBAction)getCurrentLocation;
-(IBAction)bnk_Close:(UIButton*)seder;
-(IBAction)getDirectionBtnTapped:(UIButton *)sender;
-(IBAction)clearContext:(UIButton *)sender;
-(IBAction)slideView:(UIButton *)sender;
-(IBAction)slideViewDown:(UIButton *)sender;

@property(readonly, nonatomic) CLLocationCoordinate2D coordinate;

-(IBAction)btn_back:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
- (IBAction)getCurrentLocation:(id)sender;


@end

