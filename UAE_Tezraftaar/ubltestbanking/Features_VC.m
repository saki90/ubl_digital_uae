//
//  Features_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Features_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>


@interface Features_VC ()
{
    //    UIStoryboard *mainStoryboard;
    //    UIViewController *vc;
    //    GlobalStaticClass* gblclass;
    //    NSDictionary *dic;
    MBProgressHUD* hud;
    //    NSMutableArray  *a;
    //    NSMutableArray* arr_offer;
    //    NSMutableArray* gblCls_Offer;
    //    UILabel* label;
    //    UIImageView* img;
//    UIWebView* web;
    NSString* data;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIAlertController* alert;
    NSString* str_chk;
    GlobalStaticClass* gblclass;
}

@end

@implementation Features_VC
//@synthesize webView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    gblclass=[GlobalStaticClass getInstance];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    
    NSURL* url = [[NSBundle mainBundle] URLForResource:@"features" withExtension:@"html"];
    
//    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    vw_scrl.contentSize=CGSizeMake(0,600);
    
    
    if ([ gblclass.app_features isEqualToString:@"1"])
    {
        lbl_heading.text=@"ABOUT";
        [pro_img setImage:[UIImage imageNamed:@"About.png"]];
    }
    else if ([ gblclass.app_features isEqualToString:@"2"])
    {
        lbl_heading.text=@"APP FEATURES";
        [pro_img setImage:[UIImage imageNamed:@"App-Features.png"]];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_back:(id)sender
{
    [self slide_left];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//    
//}
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
//{
//    [hud hideAnimated:YES];
//}

-(IBAction)btn_feature_details:(id)sender
{
    gblclass.app_features=@"1";
    
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature_detail"]; //feature_detail receive_qr_login
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_App_Features_details:(id)sender
{
    gblclass.app_features=@"2";
    
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature_detail"]; //feature_detail receive_qr_login
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_home:(id)sender
{
    
    // [self slide_right];
    
}


-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            [self custom_alert:@"This application needs 'Location Services' to be turned on." :@"0"];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(IBAction)btn_feature:(id)sender
{
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    // gblclass.tab_bar_login_pw_check=@"login";
    
    //    [self slide_right];
    
    
    //saki  [self peekabooSDK:@"deals"];
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    //
    //     [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    
    
    
    
    //saki    [self peekabooSDK:@"locator"];
    
    
    
    //    [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //
    //     [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
}

@end

