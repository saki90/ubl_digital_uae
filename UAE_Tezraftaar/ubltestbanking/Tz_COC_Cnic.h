//
//  Tz_COC_Cnic.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 29/04/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tz_COC_Cnic : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    IBOutlet UITableView* table_relation;
    IBOutlet UITableView* table_city;
    IBOutlet UIView* vw_table;
    IBOutlet UITextField *txt_name;
    IBOutlet UITextField *txt_cnic;
    IBOutlet UITextField *txt_contact_num;
    IBOutlet UITextField *txt_address;
    IBOutlet UITextField *txt_relation;
    IBOutlet UITextField *txt_email;
    IBOutlet UITextField *txt_city;
    IBOutlet UILabel *lbl_header;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


@end

NS_ASSUME_NONNULL_END
