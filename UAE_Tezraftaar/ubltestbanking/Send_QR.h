//
//  Send_QR.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"

@interface Send_QR : UIViewController
{
    IBOutlet UILabel* lbl_amnt;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

