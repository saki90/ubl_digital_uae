//
//  MyAnnoo.m
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "MyAnnoo.h"

@implementation MyAnnoo

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if(selected)
    {
        //Add your custom view to self...
        UIView *myview = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
        myview.backgroundColor = [UIColor redColor];
        [self addSubview:myview];
    }
    else
    {
        //Remove your custom view...
    }
}


- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    return 0; //***
}





@end
