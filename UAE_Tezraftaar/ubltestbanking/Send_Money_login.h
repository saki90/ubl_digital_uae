//
//  Send_Money_login.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 14/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface Send_Money_login : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_amnt;
    IBOutlet UITextField* txt_desp;
    IBOutlet UITextField* txt_name_frm;
    IBOutlet UITextField* txt_comment;
    IBOutlet UITextField* txt_qr_pin;
    
    
    IBOutlet UITableView* table_from;
    IBOutlet UIView* vw_table;
    IBOutlet UITextField* txt_acct_from;
    IBOutlet UILabel* lbl_acct_frm;
    IBOutlet UILabel* lbl_balance;
    
    
    IBOutlet UIButton* btn_qr;
    IBOutlet UIButton* btn_cancl;
    IBOutlet UIButton* btn_pay;
    
    
    IBOutlet UITextField* txt1;
    IBOutlet UITextField* txt2;
    IBOutlet UITextField* txt3;
    IBOutlet UITextField* txt4;
    IBOutlet UITextField* txt5;
    IBOutlet UITextField* txt6;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@end

