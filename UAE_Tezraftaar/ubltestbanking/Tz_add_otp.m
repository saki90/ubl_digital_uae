//
//  Tz_add_otp.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 02/07/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import "Tz_add_otp.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"

@interface Tz_add_otp ()
{
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    UIAlertController* alert;
    NSString* str_Otp;
    NSString* alert_chck;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pw;
    int txt_nam;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* chk_service_call;

}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Tz_add_otp
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    lbl_heading.text = @"OTP AUTHENTICATION"; //gblclass.bill_type;
    
    //[NSString stringWithFormat:@"ADD %@",gblclass.acct_add_type]; //
    //gblclass.Mobile_No
    
    chk_service_call = @"0";
    ssl_count = @"0";
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    // Set textfield cursor color ::
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    //    arr_pin_pattern=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    //    arr_pw_pin=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    
    
    
    //To add a new payee via smartphone a four digits One Time Passcode is sent to your mobile number *******3456 via SMS. Please enter the sent code below.
    
    NSString *subStr ;//= [str substringWithRange:NSMakeRange(0, 20)];
    
    //NSLog(@"%@", gblclass.Mobile_No);
    alert_chck=@"";
    
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below.",subStr];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
}

-(IBAction)btn_submit:(id)sender
{
    
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
        
        return;
    }
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"next";
        [self SSL_Call];
    }

}

-(void) clearOTP
{
txt_1.text=@"";
txt_2.text=@"";
txt_3.text=@"";
txt_4.text=@"";
txt_5.text=@"";
txt_6.text=@"";
    
}


-(void)slideLeft {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
}

-(void)slideRight {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger MAX_DIGITS = 1;
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            NSLog(@"%@",nextResponder);
            NSLog(@"%@",[textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]);
            
            //saki 4 Nov 2020
//            if (nextResponder)
//            {
//                [textField resignFirstResponder];
//                [nextResponder becomeFirstResponder];
//            }
            
            
            if (textField.tag == 6)
            {
//                [hud showAnimated:YES];
//                [self.view addSubview:hud];
//                [hud showAnimated:YES];
//
//                //  textField.tag = [@"7" integerValue];
//
//                txt_6.enabled = NO;
//                txt_6.text=string;
//
//                str_Otp=@"";
//                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                NSLog(@"**** ALERT ***** SIX");
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                [hud showAnimated:YES];
                [hud hideAnimated:YES afterDelay:130];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                chk_ssl = @"next";
                [self SSL_Call];
                
             // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                return 0;
            }
            
             textField = nil;
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            } 
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
    return YES;
}

-(void)call_ssl_service
{
    chk_service_call = @"1";
    [self SSL_Call];
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}

-(IBAction)btn_Re_Generate_OTP:(id)sender
{
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"re_send_otp";
        [self SSL_Call];
    }
    
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
   // gblclass.chck_qr_send=@"0";
    
//    CATransition *transition = [ CATransition animation ];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    //[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:130];
//    [self.view addSubview:hud];
   // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"next"])
        {
            //[self ValidateTezraftaarPayeeAddition:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            //[self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"next"])
    {
        NSLog(@"%@",gblclass.acct_add_type);
        if ([gblclass.acct_add_type isEqualToString:@"cash"])
        {
            [self PostTezraftaarPayeeAddition_coc:@""];
        }
        else
        {
            [self PostTezraftaarPayeeAddition:@""];
        }
        //[self PostTezraftaarPayeeAddition:@""];
        
        //[self PostTezraftaarPayeeAddition:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"re_send_otp"])
    {
        [self re_generate_otp:@""];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"iban"])
    {
        //   [self AddAccountTitleFetch:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) re_generate_otp:(NSString *)strIndustry {

    @try {

       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);

        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:gblclass.default_acct_num],
                                           [encrypt encrypt_Data:@"A"],
                                           [encrypt encrypt_Data:@"3"],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:3]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:2]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:gblclass.tz_city],//city
                                           [encrypt encrypt_Data:gblclass.beneficiary_Bank],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:4]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:5]],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryMsgType",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"email",
                                                                              @"strAccountNo",nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ValidateTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];

                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];

                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"OtpMessage"] :@"1"];
                  }
                  else
                  {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];
    }
}


-(void) PostTezraftaarPayeeAddition_coc:(NSString *)strIndustry {

    @try {

       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
        NSLog(@"%@",[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]);
        NSLog(@"%@",gblclass.user_id);

        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:gblclass.default_acct_num],
                                           [encrypt encrypt_Data:gblclass.tz_pay_type],//1,2,3
                                           [encrypt encrypt_Data:gblclass.acctitle],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:3]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:gblclass.tz_city], //city 6
                                           [encrypt encrypt_Data:gblclass.beneficiary_Bank],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:2]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:4]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:5]],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:7]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:str_Otp],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryAddr2",
                                                                              @"beneficiaryAddr3",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"beneficiaryNick",
                                                                              @"beneficiaryEmail",
                                                                              @"strAccountNo",
                                                                              @"strOTPPIN",nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PostTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:[dic objectForKey:@"strReturnMessage"]
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
                      //[self slideRight];
//                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];

                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"trnsfer_tezraftaar_vc"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
//                      gblclass.check_review_acct_type = @"tezraftaar_bene_add";
//                      gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];

//                      [hud hideAnimated:YES];
//                      CATransition *transition = [ CATransition animation];
//                      transition.duration = 0.3;
//                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                      transition.type = kCATransitionPush;
//                      transition.subtype = kCATransitionFromRight;
//                      [self.view.window.layer addAnimation:transition forKey:nil];
//
//                      NSLog(@"%@",gblclass.story_board);
//                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
//                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];
    }
}



-(void) PostTezraftaarPayeeAddition:(NSString *)strIndustry {

    @try {

       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
        NSLog(@"%@",[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]);
        NSLog(@"%@",gblclass.user_id);

        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:gblclass.default_acct_num],
                                           [encrypt encrypt_Data:gblclass.tz_pay_type],//1,2,3
                                           [encrypt encrypt_Data:gblclass.acctitle],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:3]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:gblclass.tz_city], //City 6
                                           [encrypt encrypt_Data:gblclass.beneficiary_Bank], //UBL
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],//[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:4]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:5]],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:7]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],
                                           [encrypt encrypt_Data:str_Otp],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryAddr2",
                                                                              @"beneficiaryAddr3",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"beneficiaryNick",
                                                                              @"beneficiaryEmail",
                                                                              @"strAccountNo",
                                                                              @"strOTPPIN",nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PostTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];

                      [alertView show];
                      //[self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];

                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"trnsfer_tezraftaar_vc"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
//                      gblclass.check_review_acct_type = @"tezraftaar_bene_add";
//                      gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];

//                      [hud hideAnimated:YES];
//                      CATransition *transition = [ CATransition animation];
//                      transition.duration = 0.3;
//                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                      transition.type = kCATransitionPush;
//                      transition.subtype = kCATransitionFromRight;
//                      [self.view.window.layer addAnimation:transition forKey:nil];
//
//                      NSLog(@"%@",gblclass.story_board);
//                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
//                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
//                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];
    }
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  [hud hideAnimated:YES];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
            
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

@end
