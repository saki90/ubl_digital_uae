//
//  Trnsfer_Other_Acct_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 17/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Trnsfer_Other_Acct_VC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_purpose;
    IBOutlet UITableView* table_bill;
    IBOutlet UITableView* table_relationship;
    IBOutlet UITableView* table_Purpose_relationship;
    IBOutlet UITextField* txt_email;
    IBOutlet UIButton* btn_email_chk;
    IBOutlet UILabel* lbl_vw_heading;
   
     IBOutlet UILabel* lbl_note;
     IBOutlet UILabel* lbl_msg;
    
    IBOutlet UIButton* btn_submit;
    IBOutlet UIView* purpose_with_view;
    IBOutlet UIView* purpose_without_view;
    IBOutlet UIView* relationship_view;
    IBOutlet UIButton* btn_check_mail;
    
    IBOutlet UITextField* txt_email_with;
    IBOutlet UITextField* txt_email_without;
    IBOutlet UITextField* txt_Utility_bill_name;
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    IBOutlet UIView* vw_purpose;
    IBOutlet UIView* vw_bill;
    IBOutlet UIView* vw_relationship;
    IBOutlet UIView* vw_relationship_purpose;
    
    IBOutlet UIView* vw_table;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@property(nonatomic,strong) IBOutlet UITextField* txt_relationship_purpose;
@property(nonatomic,strong) IBOutlet UITextField* txt_relationship;
@property(nonatomic,strong) IBOutlet UITextField* txt_purpose;
@property(nonatomic,strong) IBOutlet UITextField* txt_acct_from;
@property(nonatomic,strong) IBOutlet UITextField* txt_acct_to;
@property(nonatomic,strong) IBOutlet UITextField* txt_amnt_with;
@property(nonatomic,strong) IBOutlet UITextField* txt_comment_with;
@property(nonatomic,strong) IBOutlet UITextField* txt_amnt_without;
@property(nonatomic,strong) IBOutlet UITextField* txt_comment_without;
@property(nonatomic,strong) IBOutlet UILabel* lbl_bank_name;
@property(nonatomic,strong) IBOutlet UITextField* txt_t_pin_without;
@property(nonatomic,strong) IBOutlet UILabel* lbl_t_pin_without;
@property(nonatomic,strong) IBOutlet UILabel* lbl_email_with;
@property(nonatomic,strong) IBOutlet UILabel* lbl_email_without;
@property(nonatomic,strong) IBOutlet UITextField* txt_t_pin_with;
@property(nonatomic,strong) IBOutlet UILabel* lbl_t_pin_with;
@property(nonatomic,strong) IBOutlet UILabel* lbl_balance;
@property(nonatomic,strong) IBOutlet UILabel* lbl_acct_frm;

//Relationship

@property(nonatomic,strong) IBOutlet UITextField* txt_relationship_amount;
@property(nonatomic,strong) IBOutlet UITextField* txt_relationship_comment;
@property(nonatomic,strong) IBOutlet UITextField* txt_relationship_T_pin;
@property(nonatomic,strong) IBOutlet UILabel* lbl_relationship_T_pin;

-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_combo_to:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_email_chck:(id)sender;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_purpose:(id)sender;
-(IBAction)btn_addpayee:(id)sender;

-(IBAction)btn_Utility_bill_names:(id)sender;

-(IBAction)btn_relationship:(id)sender;
-(IBAction)btn_relationship_purpose:(id)sender;
-(IBAction)btn_vw_hide:(id)sender;
-(IBAction)btn_review:(id)sender;

@end

