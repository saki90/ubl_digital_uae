//
//  Tz_COC_Cnic.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 29/04/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import "Tz_COC_Cnic.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"

@interface Tz_COC_Cnic ()
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    NSString* ssl_count;
    Encrypt *encrypt;
    NSArray *split;
    UILabel* label;
    UIImageView* img;
    NSString* relation_code;
    NSString* email;
    NSString * vw_down_chck,* Keyboard_show,*chk_vw;
    
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Tz_COC_Cnic
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    gblclass=[GlobalStaticClass getInstance];
    chk_ssl = @"";
    ssl_count = @"0";
    encrypt = [[Encrypt alloc] init];
    
    txt_address.delegate = self;
    txt_contact_num.delegate = self;
    txt_cnic.delegate = self;
    txt_name.delegate = self;
    txt_email.delegate = self;
    txt_relation.delegate = self;
    txt_city.delegate = self;
    email = @"";
    vw_down_chck=@"0";
    chk_vw = @"0";
    
    NSLog(@"%@",gblclass.arr_tezraftar_relation);
    vw_table.hidden = YES;
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    table_relation.hidden=YES;
    table_city.hidden = YES;
//    [self checkinternet];
//    if (netAvailable)
//    {
//        chk_ssl=@"GetTezRaftarList";
//        [self SSL_Call];
//    }
    
}


- (void)slideLeft
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_relation:(id)sender
{
      lbl_header.text = @"Select Relation";
    
       [txt_relation resignFirstResponder];
       [txt_contact_num resignFirstResponder];
       [txt_address resignFirstResponder];
       [txt_name resignFirstResponder];
       [txt_email resignFirstResponder];
       [txt_cnic resignFirstResponder];
    
    vw_down_chck = @"1";
    
    if ([chk_vw isEqualToString:@"1"])
    {
        if (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
        else if (self.view.frame.origin.y < 0)
        {
            if ([vw_down_chck isEqualToString:@"1"])
            {
                [self setViewMovedUp:NO];
                vw_down_chck = @"0";
            }
        }
    }
    
    if (table_relation.isHidden==YES)
    {
        [table_relation bringSubviewToFront:self.view];
        vw_table.hidden=NO;
        table_relation.hidden = NO;
        table_city.hidden = YES;
       // [table_relation reloadData];
    }
    else
    {
        table_relation.hidden=YES;
        table_city.hidden = YES;
        vw_table.hidden=YES;
    }
}


-(IBAction)btn_combo_city:(id)sender
{
    lbl_header.text = @"Select City";
    
    [txt_email resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
 //   lbl_header.text = @"Select city";
    
      vw_down_chck = @"1";
//    search.hidden = NO;
//    search.text = @"";
//    search_chck = @"city";
//    [table_city reloadData];
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
        if (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
        else if (self.view.frame.origin.y < 0)
        {
            if ([vw_down_chck isEqualToString:@"1"])
            {
                [self setViewMovedUp:NO];
                vw_down_chck = @"0";
            }
        }
    }
    
    if (table_city.isHidden==YES)
    {
        vw_table.hidden = NO;
        table_city.hidden = NO;
        table_relation.hidden = YES;
    }
    else
    {
        vw_table.hidden = YES;
        table_city.hidden = YES;
        table_relation.hidden = YES;
    }
}


-(IBAction)btn_close:(id)sender
{
    vw_table.hidden = YES;
    table_relation.hidden = YES;
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_next:(id)sender
{
    
       [txt_relation resignFirstResponder];
       [txt_contact_num resignFirstResponder];
       [txt_address resignFirstResponder];
       [txt_name resignFirstResponder];
       [txt_email resignFirstResponder];
       [txt_cnic resignFirstResponder];
       [txt_city resignFirstResponder];
    
    vw_down_chck = @"1";
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
    
    if (self.view.frame.origin.y >= 0)
       {
           [self setViewMovedUp:YES];
       }
       else if (self.view.frame.origin.y < 0)
       {
           if ([vw_down_chck isEqualToString:@"1"])
           {
               [self setViewMovedUp:NO];
               vw_down_chck = @"0";
           }
       }
    }
    
    if ([txt_name.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter name" :@"0"];
    }
    else if ([txt_cnic.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter cnic number" :@"0"];
    }
    else if (txt_cnic.text.length <13)
    {
        [self custom_alert:@"Invalid cnic number" :@"0"];
    }
    else if ([txt_contact_num.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter contact number" :@"0"];
    }
    else if ([txt_address.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter address" :@"0"];
    }
    else if ([txt_relation.text isEqualToString:@""])
    {
        [self custom_alert:@"Please select relation" :@"0"];
    }
    else if ([txt_city.text isEqualToString:@""])
    {
        [self custom_alert:@"Please select city" :@"0"];
    }
    else if ([txt_email.text isEqualToString:@" "])
    {
        [self custom_alert:@"Please enter email address" :@"0"];
    }
//    else if(![self validateEmail:[txt_email text]])
//    {
//        [self custom_alert:@"Invalid email address" :@"0"];
//    }
    else if (![txt_email.text isEqualToString:@""])
    {
        if (![self validateEmail:[txt_email text]])
        {
            [self custom_alert:@"Invalid email address" :@"0"];
            return;
        }
        else if ([self validateEmail:[txt_email text]])
        {
            email = txt_email.text;
        }
        
            gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
            
            gblclass.acctitle = txt_name.text;
            [gblclass.chck_tezraftr_bene_type isEqualToString:@"cnic"];
            
            [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_cnic.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
            [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_email.text];
           // gblclass.tz_city = txt_city.text;
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
            [self presentViewController:vc animated:NO completion:nil];
        
    }
    else
    {
        
        gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
        
        gblclass.acctitle = txt_name.text;
        [gblclass.chck_tezraftr_bene_type isEqualToString:@"cnic"];
        
        [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
        [gblclass.arr_tezrftar_bene_additn addObject:txt_cnic.text];
        [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
        [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
        [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
        [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
        [gblclass.arr_tezrftar_bene_additn addObject:txt_email.text];
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
        [self presentViewController:vc animated:NO completion:nil];
                             
        
//        [self checkinternet];
//        if (netAvailable)
//        {
//            chk_ssl = @"next";
//            [self SSL_Call];
//        }
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    vw_down_chck = @"1";
    [txt_relation resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_cnic resignFirstResponder];
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
//    if ([textField isEqual:txt_cnic])
//    {
//        MAX_DIGITS=13;
//
//        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
//
//        [textField addTarget:self
//                      action:@selector(textFieldDidChange:)
//            forControlEvents:UIControlEventEditingChanged];
//
//        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//    }
    
    if ([textField isEqual:txt_cnic])
    {
        MAX_DIGITS=13;
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_contact_num])
    {
        MAX_DIGITS=14;
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_name])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_email])
    {
        MAX_DIGITS=50;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@._ "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    else if ([textField isEqual:txt_address])
    {
        MAX_DIGITS=60;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu",(unsigned long)[gblclass.arr_tezraftar_relation count]);
   // return [gblclass.arr_tezraftar_relation count];    //count number of row from counting array hear cataGorry is An Array
    
    NSLog(@"%lu",(unsigned long)[gblclass.arr_tezraftar_relation count]);
    if(tableView == table_relation)
    {
        return [gblclass.arr_tezraftar_relation count];
    }
    else if (tableView == table_city)
    {
       return [gblclass.arr_tezrftar_city count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    if (tableView == table_relation)
    {
        
        split = [[gblclass.arr_tezraftar_relation objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        // cell.textLabel.text = [split objectAtIndex:1];//[arr_act_type objectAtIndex:indexPath.row];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
        //    if ([gblclass.story_board isEqualToString:@"iPhone_X"])
        //    {
        //        cell.textLabel.font=[UIFont systemFontOfSize:13];
        //    }
        //    else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
        //    {
        //        cell.textLabel.font=[UIFont systemFontOfSize:14];
        //    }
        //    else
        //    {
        //        cell.textLabel.font=[UIFont systemFontOfSize:12];
        //    }
        
    }
    else if(tableView == table_city)
    {
        split = [[gblclass.arr_tezrftar_city objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //City Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        //label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    table_relation.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_city.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // Yourstring = [arr_act_type objectAtIndex:indexPath.row];
    
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    //    UIStoryboard *mainStoryboard;
    //    UIViewController *vc;
    
  
    if(tableView == table_relation)
    {
        [table_relation reloadData];
        split = [[gblclass.arr_tezraftar_relation objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        txt_relation.text = [split objectAtIndex:1];
        relation_code = [split objectAtIndex:0];
        
        NSLog(@"%@",[gblclass.arr_tezraftar_relation objectAtIndex:indexPath.row]);
        
        NSLog(@"%@",@"saki Add paye 2 option did select first");
        NSLog(@"%@",@"saki Add paye 2 option did select End");
        
//        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
//        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
    }
    else if(tableView == table_city)
      {
           [table_city reloadData];
           split = [[gblclass.arr_tezrftar_city objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
           gblclass.tz_city = [split objectAtIndex:0];
           txt_city.text = [split objectAtIndex:1];
           
      }
      
      UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
      UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
      myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
    
    vw_table.hidden = YES;
    table_relation.hidden = YES;
    table_city.hidden = YES;
    
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:130];
//    [self.view addSubview:hud];
   // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"review"])
        {
          //  [self payTezraftaarSubmit:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            //[self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
           // [self mob_App_Logout:@""];
        } else if ([chk_ssl isEqualToString:@"payAccountOverload"]) {
            chk_ssl=@"";
           // [self payAccount:@""];
        }
        else if ([chk_ssl isEqualToString:@"category"])
        {
            chk_ssl = @"";
          //  [self Get_Sub_Category:@""];
            
        } else if ([chk_ssl isEqualToString:@"next"]) {
            chk_ssl=@"";
            [self ValidateTezraftaarPayeeAddition:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"review"])
    {
        //[self payTezraftaarSubmit:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
        NSLog(@"%@",gblclass.arr_payee_list);
        
       // [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
       // [self mob_App_Logout:@""];
    } else if ([chk_ssl isEqualToString:@"payAccountOverload"])
    {
        chk_ssl=@"";
       // [self payAccount:@""];
    }
    else if ([chk_ssl isEqualToString:@"category"])
    {
        chk_ssl = @"";
      //  [self Get_Sub_Category:@""];
        
    } else if ([chk_ssl isEqualToString:@"next"]) {
        chk_ssl=@"";
        [self ValidateTezraftaarPayeeAddition:@""];
    }
    
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}

- (void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) GetTezRaftarList:(NSString *)strIndustry {

    @try {
        
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:gblclass.M3sessionid],
//                                    [encrypt encrypt_Data:str_acct_no_frm_O],
//                                    [encrypt encrypt_Data:gblclass.token], nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
//                                                                       @"strDeviceID",
//                                                                       @"strSessionId",
//                                                                       @"AccountNumber",
//                                                                       @"Token", nil]];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:@""], nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"date", nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetTezRaftarList" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;

                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                       gblclass.tz_accounts = [[dic objectForKey:@"dataset"] objectForKey:@"Beneficiary"];
                       gblclass.tz_fees = [[dic objectForKey:@"dataset"] objectForKey:@"GetTRFBeneListRsp"];
                       gblclass.tz_purpose = [NSArray arrayWithArray:[[dic objectForKey:@"dataset"] objectForKey:@"RemitPuropose"]];
                       gblclass.tz_note = [NSString stringWithFormat:@"%@ \n\n%@",[dic objectForKey:@"AnnounceNote"], [dic objectForKey:@"Note"]];
//                       _textView.text = gblclass.tz_note;
//
//                      [self populatePurposes];
                                            

                  }
                  else
                  {
                      gblclass.tz_accounts = [[NSArray alloc] init];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }

              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];

    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];

    }

}


-(void) ValidateTezraftaarPayeeAddition:(NSString *)strIndustry {

    @try {
        
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:gblclass.M3sessionid],
//                                    [encrypt encrypt_Data:str_acct_no_frm_O],
//                                    [encrypt encrypt_Data:gblclass.token], nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
//                                                                       @"strDeviceID",
//                                                                       @"strSessionId",
//                                                                       @"AccountNumber",
//                                                                       @"Token", nil]];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@"A"],
                                           [encrypt encrypt_Data:@"3"],
                                           [encrypt encrypt_Data:txt_name.text],
                                           [encrypt encrypt_Data:txt_cnic.text],
                                           [encrypt encrypt_Data:@"281"],
                                           [encrypt encrypt_Data:@"UBL"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:txt_contact_num.text],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:relation_code],
                                           [encrypt encrypt_Data:txt_relation.text],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:email],
                                           [encrypt encrypt_Data:@""],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryMsgType",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"email",
                                                                              @"strAccountNo",nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ValidateTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;

                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
                      
                      [hud hideAnimated:YES];
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      NSLog(@"%@",gblclass.story_board);
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                                            

                  }
                  else
                  {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }

              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];

    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];

    }

}




- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)keyboardWillHide
{
    
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        chk_vw = @"1";
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
    }
    else
    {
        chk_vw = @"0";
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)textViewTapped:(UITapGestureRecognizer *)tap {
     //DO SOMTHING
     
     NSLog(@"TAPPP");
     NSLog(@"%@",tap);
     vw_down_chck = @"1";
 }

 #pragma mark - Gesture recognizer delegate

 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
 }


@end
