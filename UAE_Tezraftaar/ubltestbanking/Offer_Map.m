//
//  Offer_Map.m
//  ubltestbanking
//
//  Created by Mehmood on 05/09/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Offer_Map.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"


@interface Offer_Map ()
{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    NSDictionary *dic;
    MBProgressHUD* hud;
    CLLocationCoordinate2D currentLocation;
//    GMSCameraPosition *camera;
    UIAlertController *alert;
    
    NSMutableArray *rowdata;
    NSMutableArray *numberpfrecords;
    NSString *values;
    NSMutableArray* arr_get_offer_map;
    NSArray* split;
    
    UIAlertView *alert1;
}

@property (strong, nonatomic) NSMutableArray *waypoints;
@property (strong, nonatomic) NSMutableArray *waypointStrings;
@property(strong, nonatomic) NSMutableArray *landmarksOnRoute;


@property (nonatomic, weak) IBOutlet UILabel* summaryLabel;
@property (nonatomic, weak) IBOutlet UITextField *remoteHostLabel;
@property (nonatomic, weak) IBOutlet UIImageView *remoteHostImageView;
@property (nonatomic, weak) IBOutlet UITextField *remoteHostStatusField;
@property (nonatomic, weak) IBOutlet UIImageView *internetConnectionImageView;
@property (nonatomic, weak) IBOutlet UITextField *internetConnectionStatusField;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;


@end

@implementation Offer_Map
@synthesize locationManager1;
//@synthesize mapView;
@synthesize google_mapview;

Boolean noStartSetsz2 = TRUE;
CLLocationCoordinate2D startPointsz2;



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    
    arr_get_offer_map=[[NSMutableArray alloc] init];
    
    if(kCFCoreFoundationVersionNumber_iOS_8_4)
    {
        [locationManager1 requestAlwaysAuthorization];
    }
    
    
    locationManager1 = [[CLLocationManager alloc] init];
    locationManager1.distanceFilter = kCLDistanceFilterNone;
    locationManager1.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    locationManager1.delegate = self;
    [locationManager1 startUpdatingLocation];
    
    
    if ([gblclass.arr_current_location count]>1)
    {
        
        double source_lati= [[gblclass.arr_current_location objectAtIndex:0] floatValue];
        double source_longi=[[gblclass.arr_current_location objectAtIndex:1] floatValue];
        
        if (source_longi==0.00 || source_lati==0.00)
        {
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
            {
                
                [hud hideAnimated:YES];
                
                gblclass.custom_alert_msg=@"This application needs 'Location Services' to be turned on.";
                gblclass.custom_alert_img=@"0";
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                
                //                alert1 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
                //                                                    message:@"This application needs 'Location Services' to be turned on."
                //                                                   delegate:nil
                //                                          cancelButtonTitle:@"OK"
                //                                          otherButtonTitles:nil];
                //                [alert1 show];
                
                return;
            }
            else
            {
                [hud hideAnimated:YES];
                
                gblclass.custom_alert_msg=@"Could not find your current location, Please try again later.";
                gblclass.custom_alert_img=@"0";
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                
                //                alert1 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
                //                                                    message:@"Could not find your current location, Please try again later."
                //                                                   delegate:nil
                //                                          cancelButtonTitle:@"OK"
                //                                          otherButtonTitles:nil];
                //                [alert1 show];
            }
            
            
            return;
        }
        
        
        NSString* detail=[NSString stringWithFormat:@"getCatDetailByLatAndLong?Latitude=%f&Longitude=%f",source_lati,source_longi];
        
        
        NSString *myURL1=[NSString stringWithFormat:@"%@%@",merito_All_category ,detail];
        
        NSURL *url = [[NSURL alloc] initWithString:myURL1];
        NSURLRequest *myreq=[NSURLRequest requestWithURL:url];
        NSURLConnection *myConnection = [NSURLConnection connectionWithRequest:myreq delegate:self];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading ...";
        [hud showAnimated:YES];
    }
    else
    {
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=@"This application needs 'Location Services' to be turned on.";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //             alert1 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
            //                                                            message:@"This application needs 'Location Services' to be turned on."
            //                                                           delegate:nil
            //                                                  cancelButtonTitle:@"OK"
            //                                                  otherButtonTitles:nil];
            //            [alert1 show];
            
            return;
        }
        else
        {
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=@"Could not find your current location, Please try again later.";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            //            alert1 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
            //                                                            message:@"Could not find your current location, Please try again later."
            //                                                           delegate:nil
            //                                                  cancelButtonTitle:@"OK"
            //                                                  otherButtonTitles:nil];
            //            [alert1 show];
        }
        
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    NSString *remoteHostName = @"www.apple.com";
    NSString *remoteHostLabelFormatString = NSLocalizedString(@"Remote Host: %@", @"Remote host label format string");
    self.remoteHostLabel.text = [NSString stringWithFormat:remoteHostLabelFormatString, remoteHostName];
    
    //    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    //    [self.hostReachability startNotifier];
    //    [self updateInterfaceWithReachability:self.hostReachability];
    
//    self.internetReachability = [Reachability reachabilityForInternetConnection];
//    [self.internetReachability startNotifier];
//    [self updateInterfaceWithReachability:self.internetReachability];
//
//    [mapView setMapType:MKMapTypeSatellite];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)StartTheCode:(NSTimer*)t
{
    
    @try {
        
        _coordinates_1 = [[NSMutableArray alloc] init];
//        _routeController = [[LRouteController alloc] init];
        
        
        //NSLog(@"%@",gblclass.arr_current_location);
        
        
        NSArray* split = [[gblclass.arr_ph_address  objectAtIndex:[gblclass.Offer_id integerValue]] componentsSeparatedByString: @"|"];
        
        double source_lati  = currentLocation.latitude;
        double source_longi = currentLocation.longitude;
        
        if (source_lati == 0)
        {
            source_lati = 24.831189;
            source_lati = 67.034617;
        }
        
        double desti_lati= [[split objectAtIndex:2] floatValue];
        double desti_longi=[[split objectAtIndex:3] floatValue];
        
//        camera = [GMSCameraPosition cameraWithLatitude:24.831189 longitude:67.034617 zoom:12];
//        mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
//        mapView.settings.compassButton = YES;
//        mapView.settings.myLocationButton = YES;
//
//        mapView.delegate = self;
//        //   [self.view addSubview:mapView];
//
//        _markerStart = [GMSMarker new];
//        _markerStart.title = @"Start";
//
//        _markerFinish = [GMSMarker new];
//        _markerFinish.title = @"Finish";
        
    }
    @catch (NSException *exception)
    {
        
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power.
    
    [manager stopUpdatingLocation];
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (abs(howRecent) < 15.0)
    {
        // Update your marker on your map using location.coordinate.latitude
        //and location.coordinate.longitude);
        //        double source_lati= location.coordinate.latitude;//currentLocation.latitude;
        //        double source_longi=location.coordinate.longitude;//currentLocation.longitude;
    }
    
}

- (void)updateCurrentLocation
{
    
    //  sleep(2);
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    currentLocation = [location coordinate];
    
    if(noStartSetsz2)
    {
        startPointsz2 = currentLocation;
        noStartSetsz2 = FALSE;
    }
    
//    [self.mapView animateToLocation:currentLocation];
}


- (void)loadView2
{
    
    @try {
        
        gblclass=[GlobalStaticClass getInstance];
        
        
        //NSLog(@"%lu",(unsigned long)[gblclass.arr_current_location count]);
        
        
        
        //        if ([gblclass.arr_current_location count]==0)
        //        {
        //            [hud hideAnimated:YES];
        //
        //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to get current location !"  preferredStyle:UIAlertControllerStyleAlert];
        //
        //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //            [alert addAction:ok];
        //            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        //        else
        //        {
        //            [hud hideAnimated:YES];
        //
        ////            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to get current location !"  preferredStyle:UIAlertControllerStyleAlert];
        ////
        ////            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        ////            [alert addAction:ok];
        ////            [self presentViewController:alert animated:YES completion:nil];
        //
        //
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                            message:@"Record Not Found"
        //                                                           delegate:self
        //                                                  cancelButtonTitle:@"OK"
        //                                                  otherButtonTitles:nil];
        //            [alert show];
        //
        //
        //            return;
        //        }
        //
        
        
        
        
        //        double source_lati= [[gblclass.arr_current_location objectAtIndex:0] floatValue];
        //        double source_longi=[[gblclass.arr_current_location objectAtIndex:1] floatValue];
        
        double source_lati= [@"24.8278273" floatValue];
        double source_longi=[@"67.0350265" floatValue];
        
        
        
        double desti_lati= [[split objectAtIndex:2] floatValue];
        double desti_longi=[[split objectAtIndex:3] floatValue];
        
        ////NSLog(@"%lu",(unsigned long)[gblclass.arr_current_location count]);
        
        
        if (source_lati==0)
        {
            source_lati=24.7509717;
            source_longi=67.0861803;
        }
        
        if (source_longi==0 || source_lati==0)
        {
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=@"Unable to get current location !";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to get current location !"  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
        
        
        //  [self updateCurrentLocation];
        
        
        //777  CLLocation *myLocation = mapView.myLocation;
        //NSLog(@"%@",gblclass.arr_current_location);
        
        
//        camera = [GMSCameraPosition cameraWithLatitude:source_lati longitude:source_longi zoom:12];
//
//        GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectMake(0,0, 10, 25) camera:camera];
//        self.mapView.settings.myLocationButton = YES;
        
        
        
        //For Current Location Annotation :::::
        
        //        GMSMarker *marker = [[GMSMarker alloc] init];
        //        marker.position = camera.target;
        //        marker.snippet = @"Current Location";
        //        marker.appearAnimation = kGMSMarkerAnimationPop;
        //        mapView.settings.compassButton = YES;
        //        mapView.accessibilityElementsHidden = NO;
        //        mapView.settings.myLocationButton=YES;
        //        mapView.myLocationEnabled = YES;
        //        mapView.userInteractionEnabled=YES;
        //        mapView.settings.scrollGestures = YES;
        //        mapView.settings.zoomGestures = YES;
        //
        //
        //        marker.map = mapView;
        //        //uvForMap=mapView;
        //       //google_mapview=mapView;
        //        self.view = mapView;
        //        //self.uvForMap = mapView;
        //
        //
        //
        //        desti_lati=24.8085132;
        //        desti_longi=67.0356702;
        
        
//        GMSMarker *marker3;
        
        
        for (int i=0; i<[arr_get_offer_map count]; i++)
        {
            
//            marker3 = [[GMSMarker alloc] init];
            
            split = [[arr_get_offer_map objectAtIndex:i] componentsSeparatedByString:@"|"];
            
            desti_lati=[[split objectAtIndex:0] floatValue];
            desti_longi=[[split objectAtIndex:1] floatValue];
            
//            marker3.position = CLLocationCoordinate2DMake(desti_lati,desti_longi);
//            marker3.title = [split objectAtIndex:3];
//            marker3.snippet=[NSString stringWithFormat:@"Phone = %@",[split objectAtIndex:5]];
//            marker3.icon = [UIImage imageNamed:@"Pin-Red"];
//            marker3.map = mapView;
            
        }
        
//        self.view=mapView;
        
        
        
        
        //Create Region
        //777        MKCoordinateRegion myregion;
        
        
        
        //        //NSLog(@"%@",[gblclass.arr_current_location objectAtIndex:0]);
        //        //NSLog(@"%@",[gblclass.arr_current_location objectAtIndex:1]);
        
        
        if (source_lati==0)
        {
            source_lati=24.7509717;
            source_longi=67.0861803;
        }
        
        //  NSString *desti_lati=[gblclass.arr_get_direction objectAtIndex:2];
        // NSString *desti_longi=[gblclass.arr_get_direction objectAtIndex:3];
        
        // CLLocation *source = [[CLLocation alloc] initWithLatitude:[source_lati doubleValue]longitude:[source_longi doubleValue]];
        
        
        desti_lati=24.8134623;
        desti_longi=67.0276744;
        
        
        //777       CLLocation *source = [[CLLocation alloc] initWithLatitude:source_lati longitude:source_longi];
        //777       CLLocation *desti = [[CLLocation alloc] initWithLatitude:desti_lati longitude:desti_longi];
        
        //For Route draw ::
        
        //   [self callGoogleServiceToGetRouteDataFromSource:source toDestination:desti onMap:mapView];
        
        
        [hud hideAnimated:YES];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Unable to get current location !";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to get current location !"  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

//- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
//{
//    UIView *view = [[UIView alloc] init];
//    view.frame = CGRectMake(0, 0, 280, 40);
//    view.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.4 alpha:1.0];
//
//    return view;
//}


//-(void)callGoogleServiceToGetRouteDataFromSource:(CLLocation *)sourceLocation toDestination:(CLLocation *)destinationLocation onMap:(GMSMapView *)mapView_
//{
//    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false", sourceLocation.coordinate.latitude,  sourceLocation.coordinate.longitude, destinationLocation.coordinate.latitude,  destinationLocation.coordinate.longitude];
//    
//    NSURL *url = [NSURL URLWithString:[baseUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    
//    //NSLog(@"Url: %@", url);
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        
//        GMSMutablePath *path = [GMSMutablePath path];
//        
//        NSError *error = nil;
//        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//        
//        NSArray *routes = [result objectForKey:@"routes"];
//        
//        NSDictionary *firstRoute = [routes objectAtIndex:0];
//        
//        NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
//        
//        NSArray *steps = [leg objectForKey:@"steps"];
//        
//        int stepIndex = 0;
//        
//        CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
//        
//        for (NSDictionary *step in steps) {
//            
//            NSDictionary *start_location = [step objectForKey:@"start_location"];
//            stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
//            [path addCoordinate:[self coordinateWithLocation:start_location]];
//            
//            NSString *polyLinePoints = [[step objectForKey:@"polyline"] objectForKey:@"points"];
//            GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:polyLinePoints];
//            for (int p=0; p<polyLinePath.count; p++) {
//                [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
//            }
//            
//            
//            if ([steps count] == stepIndex)
//            {
//                NSDictionary *end_location = [step objectForKey:@"end_location"];
//                stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
//                [path addCoordinate:[self coordinateWithLocation:end_location]];
//            }
//        }
//        
//        GMSPolyline *polyline = nil;
//        polyline = [GMSPolyline polylineWithPath:path];
//        polyline.strokeColor = [UIColor blueColor];
//        polyline.strokeWidth = 3.f;
//        polyline.map = mapView_;
//        
//        
//        self.view = mapView;
//        
//        [hud hideAnimated:YES];
//        
//        
//    }];
//}
//
//-(CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
//{
//    double latitude = [[location objectForKey:@"lat"] doubleValue];
//    double longitude = [[location objectForKey:@"lng"] doubleValue];
//    
//    return CLLocationCoordinate2DMake(latitude, longitude);
//}

-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_home:(id)sender
{
    NSString *first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"first_time"];
    
    //NSLog(@"%@",first_time_chk);
    
    
    if ([first_time_chk isEqualToString:@"first_time"] || [first_time_chk isEqualToString:@"second_time"])
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
    }
    else
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];  //Mobile_next
        [self presentViewController:vc animated:NO completion:nil];
        
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    
}










- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // inform the user
    //NSLog(@"Connection failed! Error - %@ %@",
    [error localizedDescription],
    [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey];
    
    
    gblclass.custom_alert_msg=[error localizedDescription];
    gblclass.custom_alert_img=@"0";
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
    
    //    UIAlertView*   alrt=[[UIAlertView alloc] initWithTitle:@"Attention"
    //                                                   message:[error localizedDescription]
    //                                                  delegate:nil
    //                                         cancelButtonTitle:@"OK"
    //                                         otherButtonTitles:nil];
    //
    //    [alrt show];
    
    
    [hud hideAnimated:YES];
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
    int errorCode = httpResponse.statusCode;
    NSString *fileMIMEType = [[httpResponse MIMEType] lowercaseString];
    sleep(2);
    //NSLog(@"response is %d, %@", errorCode, fileMIMEType);
}




- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSString *My_string;
    My_string=@"";
    //    NSString *a;
    //NSLog(@"data is %@", data);
    //    NSString *str;
    My_string=@"";
    My_string=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"string is %@", My_string);
    
    NSError *error;
    
    NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    //    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (error)
    {
        //NSLog(@"JSONObjectWithData error: %@", error);
        [hud hideAnimated:YES];
        
        UIAlertView*  alrt=[[UIAlertView alloc] initWithTitle:@"Attention"
                                                      message:@"Please Try Later.."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        
        [alrt show];
        
        return;
        
    }
    
    
    numberpfrecords = [[NSMutableArray alloc]init];
    
    for (NSMutableDictionary *dictionary in array)
    {
        rowdata = [[NSMutableArray alloc]init];
        
        
        NSString*  Latitude = dictionary[@"Latitude"];
        [rowdata addObject:Latitude];
        
        NSString* Longitude = dictionary[@"Longitude"];
        [rowdata addObject:Longitude];
        
        NSString* DetailId = dictionary[@"DetailId"];
        [rowdata addObject:DetailId];
        
        NSString* DetailName = dictionary[@"DetailName"];
        [rowdata addObject:DetailName];
        
        NSString* CategoryId = dictionary[@"CategoryId"];
        [rowdata addObject:CategoryId];
        
        NSString* Phone = dictionary[@"Phone"];
        [rowdata addObject:Phone];
        
        
        values = [rowdata componentsJoinedByString:@"|"];
        [numberpfrecords addObject:values];
        
        [arr_get_offer_map addObject:values];
        
    }
    
    
    // [table reloadData];
    
    [self loadView2];
    
    [hud hideAnimated:YES];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeeded!");
    
    [hud hideAnimated:YES];
}




















/*!
 * Called by Reachability whenever status changes.
 */
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}


- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    if (reachability == self.hostReachability)
    {
        [self configureTextField:self.remoteHostStatusField imageView:self.remoteHostImageView reachability:reachability];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        
        self.summaryLabel.hidden = (netStatus != ReachableViaWWAN);
        NSString* baseLabelText = @"";
        
        if (connectionRequired)
        {
            baseLabelText = NSLocalizedString(@"Cellular data network is available.\nInternet traffic will be routed through it after a connection is established.", @"Reachability text if a connection is required");
        }
        else
        {
            baseLabelText = NSLocalizedString(@"Cellular data network is active.\nInternet traffic will be routed through it.", @"Reachability text if a connection is not required");
        }
        self.summaryLabel.text = baseLabelText;
    }
    
    if (reachability == self.internetReachability)
    {
        [self configureTextField:self.internetConnectionStatusField imageView:self.internetConnectionImageView reachability:reachability];
        
        
        
        //NSLog(@"*********** Connected *************");
    }
    
}


- (void)configureTextField:(UITextField *)textField imageView:(UIImageView *)imageView reachability:(Reachability *)reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus)
    {
        case NotReachable:        {
            statusString = NSLocalizedString(@"Access Not Available", @"Text field text for access is not available");
            imageView.image = [UIImage imageNamed:@"stop-32.png"] ;
            /*
             Minor interface detail- connectionRequired may return YES even when the host is unreachable. We cover that up here...
             */
            connectionRequired = NO;
            
            
            //NSLog(@"*********** Not Connected *************");
            
            break;
        }
            
        case ReachableViaWWAN:        {
            statusString = NSLocalizedString(@"Reachable WWAN", @"");
            imageView.image = [UIImage imageNamed:@"WWAN5.png"];
            break;
        }
        case ReachableViaWiFi:        {
            statusString= NSLocalizedString(@"Reachable WiFi", @"");
            imageView.image = [UIImage imageNamed:@"Airport.png"];
            break;
        }
    }
    
    if (connectionRequired)
    {
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
    
    textField.text = statusString;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}






@end

