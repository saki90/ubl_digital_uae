
//
//  Landing_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 03/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Landing_VC.h"
#import "GnbRefreshAcct.h"
#import "Table_de.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "PieChartView.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Slide_menu_VC.h"
#import "Encrypt.h"


@interface Landing_VC ()<NSURLConnectionDataDelegate,SlidemenuDelegate>
{
    NSMutableArray* account_name;
    
    NSMutableArray* account_number;
    
    NSMutableArray* branch;
    NSMutableArray *account_typedesc;
    NSMutableArray *account_currencydesc;
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    NSMutableArray* balc_date;
    NSMutableArray* avail_bal;
    NSString* Pw_status_chck;
    GlobalStaticClass *gblclass;
    GnbRefreshAcct *classObj ;
    Table_de *service_tble1;
    NSString *flag;
    //int *dictionaryindex;
    //  NSNumber *number;
    NSDictionary *dic;
    UIButton *exesender;
    MBProgressHUD *hud;
    NSMutableArray *sections;
    NSMutableArray *rows;
    float bal;
    double value2;
    NSNumberFormatter *formatter;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UIButton *det_button;
    Table_de *table_obj;
    NSString* cell_index;
    UILabel* label;
    UIButton* paybtn;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSMutableArray *xrows;
    NSUserDefaults *user_default;
    NSString* chk_ssl;
    NSString* debit_lock_chck;
    NSNumberFormatter *format_2;
    NSString *temp_1;
    float number_2;
    BOOL runOnce;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* tch_chk;
    NSString* t_n_c;
    NSString* user,* pass;
    NSString * chck_show_btn;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UIImageView *dwbitImg;
@property (weak, nonatomic) IBOutlet UIImageView *creditImg;

@end

@implementation Landing_VC
@synthesize transitionController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud hideAnimated:YES afterDelay:130];
    
    encrypt = [[Encrypt alloc] init];
    ssl_count = @"0";
     _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
//    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction)];
//    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.view addGestureRecognizer:swipeGesture];
//
      user_default = [NSUserDefaults standardUserDefaults];
//    gblclass.arr_graph_name=[[NSMutableArray alloc] init];
      self.transitionController = [[TransitionDelegate alloc] init];
    
    
    gblclass =  [GlobalStaticClass getInstance];
    table_obj = [Table_de new];
    debit_lock_chck = @"no";
    tch_chk = @"";
    t_n_c = [user_default valueForKey:@"t&c"];
    
    
    chck_show_btn = @"0";
    // btn_summary.titleLabel.text = @"Show Balance";
     [btn_summary setTitle:@"Show Balance" forState:UIControlStateNormal];
     btn_summary.tintColor = [UIColor blackColor];
     btn_summary.backgroundColor = [UIColor colorWithRed:236/255.0 green:239/255.0 blue:248/255.0 alpha:1.0];
    
    
    btn_summary.layer.cornerRadius = 100; // this value vary as per your desire
    btn_summary.clipsToBounds = YES;

    
  //  NSLog(@"%@",gblclass.storyboardidentifer);
   // if([gblclass.storyboardidentifer isEqualToString:@"accounts"])
    if ([gblclass.landing isEqualToString:@"1"])
    {
        btn_back.hidden=NO;
        // img_back.hidden=NO;
        
        //[hud showAnimated:YES];
        //[self.view addSubview:hud];
        gblclass.landing = @"0";
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"load";
            [self SSL_Call];
        }
    }
    else
    {
        btn_back.hidden=YES;
        //  img_back.hidden=YES;
        
        
   //     NSLog(@"%@",gblclass.actsummaryarr);
        [self parsearray];
        
        //        [self checkinternet];
        //        if (netAvailable)
        //        {
        //            chk_ssl=@"load";
        //            [self SSL_Call];
        //        }
        
        
        //29 march 2017
        // [self parsearray];
    }
   
   
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setPositiveFormat:@"###0.##"];
    
    self.profile_view.hidden=YES;
    
    // For camera check :::
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        
    }
    
    welcome_name.text = [NSString stringWithFormat:@"Welcome %@",gblclass.welcome_msg];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"MMMM dd, yyyy"];//@"dd/MM/YYYY"
    
    NSString *date_String=[dateformate stringFromDate:[NSDate date]];
    dateformate.dateFormat = @"EEEE";
    NSString *dayName = [dateformate stringFromDate:[NSDate date]];
    
    welcome_date.text=[NSString stringWithFormat:@"%@",date_String];
    welcome_day.text=[NSString stringWithFormat:@"Today is %@",dayName];
    
    
    _Profile_imageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    _Profile_imageView.layer.cornerRadius=25;
    _Profile_imageView.layer.borderWidth=2.0;
    _Profile_imageView.layer.masksToBounds = YES;
    _Profile_imageView.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    
    //229,233,242
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    
    // getting an NSString
    //    NSString *pro_img = [user_default stringForKey:@"profile_Img"];
    NSData *pro_img = [user_default dataForKey:@"profile_Img"];
    
    // self.Profile_imageView.image =pro_img;
    
    _Profile_imageView.image = nil;
    if (pro_img.length==0)
    {
        [self.Profile_imageView setImage:[UIImage imageNamed:@"profile_img1.png"]]; //Profile.png
    }
    else
    {
     // NSData *imageData4 = [user_default dataForKey:@"profile_Img"];
        UIImage *contactImage = [UIImage imageWithData:pro_img];
        
        // Update the UI elements with the saved data
        _Profile_imageView.image = contactImage;
        _Profile_imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        //**[self.Profile_imageView setImage:[UIImage imageNamed:pro_img]];
    }
    
    
    //    NSMutableArray *valueArray = [[NSMutableArray alloc] initWithObjects:
    //                                  [NSNumber numberWithInt:1],
    //                              //    [NSNumber numberWithInt:1],
    //                                  //                                  [NSNumber numberWithInt:1],
    //                                  //                                  [NSNumber numberWithInt:3],
    //                                  //                                  [NSNumber numberWithInt:2],
    //                                  nil];
    //
    //    NSMutableArray *colorArray = [[NSMutableArray alloc] initWithObjects:
    //                                  [UIColor blueColor],
    //                                //  [UIColor redColor],
    //                                  //                                  [UIColor whiteColor],
    //                                  //                                  [UIColor greenColor],
    //                                  //                                  [UIColor purpleColor],
    //                                  nil];
    //
    //    // 必须先创建一个相同大小的container view，再将PieChartView add上去
    //    // UIView *container = [[UIView alloc] initWithFrame:CGRectMake(130, 400, 250, 250)]; //X(320 - 250) / 2
    //    PieChartView* pieView = [[PieChartView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    //    [container addSubview:pieView];
    //
    //    pieView.mValueArray = [NSMutableArray arrayWithArray:valueArray];
    //    pieView.mColorArray = [NSMutableArray arrayWithArray:colorArray];
    //    pieView.mInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(20, 350, 300, 80)];
    //    pieView.mInfoTextView.backgroundColor = [UIColor clearColor];
    //    pieView.mInfoTextView.editable = NO;
    //    pieView.mInfoTextView.userInteractionEnabled = NO;
    //    //[self.view addSubview:container];
    //
    //
    //    //_pie_view = [[UIView alloc] initWithFrame:CGRectMake(10,20,100,100)];
    //    _pie_view.alpha = 0.5;
    //    _pie_view.layer.cornerRadius = 65;
    //    _pie_view.backgroundColor = [UIColor colorWithRed:236/255.0 green:239/255.0 blue:246/255.0 alpha:1.0];
    
    
    NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    runOnce = NO;
    
    
    //    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    //
    //    if (status != ALAuthorizationStatusAuthorized) {
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Please give this app permission to access your photo library in your settings app!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    //        [alert show];
    //    }
    
    
    //***************** Photo library permission  start :: *****************
    
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        //NSLog(@"%zd", [group numberOfAssets]);
        
     //   NSLog(@"permission");
        
    }
                     failureBlock:^(NSError *error)
     {
         if (error.code == ALAssetsLibraryAccessUserDeniedError)
         {
             //NSLog(@"user denied access, code: %zd", error.code);
             
//             UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Please give this app permission to access your photo library in your settings app!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
//             [alert1 show];
//             
//             return ;
         }
         else
         {
             //NSLog(@"Other error code: %zd", error.code);
         }
         
     }];
    
    //***************** Photo library permission  End :: *****************
    
    
    
    
    //***************** Camera permission ********************
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input)
    {
        //[_session addInput:_input];
    }
        
    //***************** Camera permission End ********************
}


-(void)viewWillAppear:(BOOL)animated
{
    
    if (!runOnce)
    {
        self.navigationBar.frame = CGRectMake( self.navigationBar.frame.origin.x, self.navigationBar.frame.origin.y , self.navigationBar.frame.size.width, self.navigationBar.frame.size.height - 40);
        
        _Profile_imageView.frame = CGRectMake(_Profile_imageView.frame.origin.x, _Profile_imageView.frame.origin.y, _Profile_imageView.frame.size.height *2, _Profile_imageView.frame.size.width*2);
        
        
        self.accountOutlet.frame = CGRectMake(self.accountOutlet.frame.origin.x, self.view.frame.size.height + self.accountOutlet.frame.size.height, self.accountOutlet.frame.size.width, self.accountOutlet.frame.size.height);
        
        self.payOutlet.frame = CGRectMake(self.payOutlet.frame.origin.x, self.view.frame.size.height + self.payOutlet.frame.size.height, self.payOutlet.frame.size.width, self.payOutlet.frame.size.height);
        
        
        _dwbitImg.frame = CGRectMake(_dwbitImg.frame.origin.x - 40, _dwbitImg.frame.origin.y, _dwbitImg.frame.size.width, _dwbitImg.frame.size.height);
        _dwbitImg.alpha = 0;
        
        lbl_date_dr.frame = CGRectMake(lbl_date_dr.frame.origin.x - 20, lbl_date_dr.frame.origin.y, lbl_date_dr.frame.size.width, lbl_date_dr.frame.size.height);
        lbl_date_dr.alpha = 0;
        
        lbl_tran_amount_dr.frame = CGRectMake(lbl_tran_amount_dr.frame.origin.x - 20, lbl_tran_amount_dr.frame.origin.y, lbl_tran_amount_dr.frame.size.width, lbl_tran_amount_dr.frame.size.height);
        lbl_tran_amount_dr.alpha = 0;
        
        lbl_type_dr.frame = CGRectMake(lbl_type_dr.frame.origin.x - 20, lbl_type_dr.frame.origin.y, lbl_type_dr.frame.size.width, lbl_type_dr.frame.size.height);
        lbl_type_dr.alpha = 0;
        
        _creditImg.frame = CGRectMake(_creditImg.frame.origin.x + 20, _creditImg.frame.origin.y, _creditImg.frame.size.width, _creditImg.frame.size.height);
        _creditImg.alpha = 0;
        
        lbl_date_cr.frame = CGRectMake(lbl_date_cr.frame.origin.x + 20, lbl_date_cr.frame.origin.y, lbl_date_cr.frame.size.width, lbl_date_cr.frame.size.height);
        lbl_date_cr.alpha = 0;
        
        lbl_tran_amount_cr.frame = CGRectMake(lbl_tran_amount_cr.frame.origin.x + 20, lbl_tran_amount_cr.frame.origin.y, lbl_tran_amount_cr.frame.size.width, lbl_tran_amount_cr.frame.size.height);
        lbl_tran_amount_cr.alpha = 0;
        
        lbl_type_cr.frame = CGRectMake(lbl_type_cr.frame.origin.x + 20, lbl_type_cr.frame.origin.y, lbl_type_cr.frame.size.width, lbl_type_cr.frame.size.height);
        lbl_type_cr.alpha = 0;
        
        welcome_name.alpha = 0;
        welcome_day.alpha = 0;
        welcome_date.alpha = 0;
        
        if ([gblclass.story_board isEqualToString:@"iPhone_X"])
        {
            _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width + 5, _circleImg.frame.size.height+ 5);
            _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width + 5, _sarcle.frame.size.height+ 5);
            _totalLabel.alpha = 0;
            _upper_amount.alpha = 0;
            lbl_num_to_word.alpha = 0;
        }
        else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
        {
            _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width + 120, _circleImg.frame.size.height+ 120);
            
            _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width + 120, _sarcle.frame.size.height+ 120);
            
            _totalLabel.alpha = 0;
            _upper_amount.alpha = 0;
            lbl_num_to_word.alpha = 0;
        }
        else
        {
            _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width + 25, _circleImg.frame.size.height+ 25);
            _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width + 25, _sarcle.frame.size.height+ 25);
            _totalLabel.alpha = 0;
            _upper_amount.alpha = 0;
            lbl_num_to_word.alpha = 0;
        }
        
//        _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width + 25, _circleImg.frame.size.height+ 25);
//        _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width + 25, _sarcle.frame.size.height+ 25);
//        _totalLabel.alpha = 0;
//        _upper_amount.alpha = 0;
//        lbl_num_to_word.alpha = 0;
        
        [UIView animateWithDuration:0.7 animations:^{
            
            if ([gblclass.story_board isEqualToString:@"iPhone_X"])
            {
                _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width - 5, _circleImg.frame.size.height - 5);
                _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width - 5, _sarcle.frame.size.height- 5);
                _totalLabel.alpha = 1;
                _upper_amount.alpha = 1;
                lbl_num_to_word.alpha = 1;
            }
            else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
            {
                _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width - 120, _circleImg.frame.size.height - 120);
                _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width - 120, _sarcle.frame.size.height- 120);
                _totalLabel.alpha = 1;
                _upper_amount.alpha = 1;
                lbl_num_to_word.alpha = 1;
            }
            else
            {
                _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width - 25, _circleImg.frame.size.height - 25);
                _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width - 25, _sarcle.frame.size.height- 25);
                _totalLabel.alpha = 1;
                _upper_amount.alpha = 1;
                lbl_num_to_word.alpha = 1;
            }
            
//            _circleImg.frame = CGRectMake(_circleImg.frame.origin.x, _circleImg.frame.origin.y, _circleImg.frame.size.width - 25, _circleImg.frame.size.height - 25);
//            _sarcle.frame = CGRectMake(_sarcle.frame.origin.x, _sarcle.frame.origin.y, _sarcle.frame.size.width - 25, _sarcle.frame.size.height- 25);
//            _totalLabel.alpha = 1;
//            _upper_amount.alpha = 1;
//            lbl_num_to_word.alpha = 1;
            
        } completion:^(BOOL finished) {
            
            NSMutableArray *valueArray = [[NSMutableArray alloc] initWithObjects:
                                          [NSNumber numberWithInt:1],
                                          //    [NSNumber numberWithInt:1],
                                          //                                  [NSNumber numberWithInt:1],
                                          //                                  [NSNumber numberWithInt:3],
                                          //                                  [NSNumber numberWithInt:2],
                                          nil];
            
            NSMutableArray *colorArray = [[NSMutableArray alloc] initWithObjects:
                                          [UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0],
                                          //  [UIColor redColor],
                                          //                                  [UIColor whiteColor],
                                          //                                  [UIColor greenColor],
                                          //                                  [UIColor purpleColor],
                                          nil];
            
            // 必须先创建一个相同大小的container view，再将PieChartView add上去
            // UIView *container = [[UIView alloc] initWithFrame:CGRectMake(130, 400, 250, 250)]; //X(320 - 250) / 2
            
            PieChartView* pieView;
            if ([gblclass.story_board isEqualToString:@"iPhone_X"])
            {
                pieView = [[PieChartView alloc] initWithFrame:CGRectMake(0, 0, 225, 225)];
            }
            else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
            {
                pieView = [[PieChartView alloc] initWithFrame:CGRectMake(0, 0, 290, 290)];
            }
            else
            {
                pieView = [[PieChartView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
            }
           
            
            
            pieView.mValueArray = [NSMutableArray arrayWithArray:valueArray];
            pieView.mColorArray = [NSMutableArray arrayWithArray:colorArray];
            pieView.mInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(20, 350, 300, 80)];
            pieView.mInfoTextView.backgroundColor = [UIColor clearColor];
            pieView.mInfoTextView.editable = NO;
            pieView.mInfoTextView.userInteractionEnabled = NO;
            //[self.view addSubview:container];
            
            
            //_pie_view = [[UIView alloc] initWithFrame:CGRectMake(10,20,100,100)];
            _pie_view.alpha = 0.5;
            _pie_view.layer.cornerRadius = 65;
            _pie_view.backgroundColor = [UIColor colorWithRed:236/255.0 green:239/255.0 blue:246/255.0 alpha:1.0];
            [container addSubview:pieView];
            
        }];
        
        [UIView animateWithDuration:0.8 animations:^{
            
            
            _dwbitImg.frame = CGRectMake(_dwbitImg.frame.origin.x + 20, _dwbitImg.frame.origin.y, _dwbitImg.frame.size.width, _dwbitImg.frame.size.height);
            _dwbitImg.alpha = 1;
            
            
            lbl_date_dr.frame = CGRectMake(lbl_date_dr.frame.origin.x + 20, lbl_date_dr.frame.origin.y, lbl_date_dr.frame.size.width, lbl_date_dr.frame.size.height);
            lbl_date_dr.alpha = 1;
            lbl_tran_amount_dr.frame = CGRectMake(lbl_tran_amount_dr.frame.origin.x + 20, lbl_tran_amount_dr.frame.origin.y, lbl_tran_amount_dr.frame.size.width, lbl_tran_amount_dr.frame.size.height);
            lbl_tran_amount_dr.alpha = 1;
            lbl_type_dr.frame = CGRectMake(lbl_type_dr.frame.origin.x + 20, lbl_type_dr.frame.origin.y, lbl_type_dr.frame.size.width, lbl_type_dr.frame.size.height);
            lbl_type_dr.alpha = 1;
            
            _creditImg.frame = CGRectMake(_creditImg.frame.origin.x - 20, _creditImg.frame.origin.y, _creditImg.frame.size.width, _creditImg.frame.size.height);
            _creditImg.alpha = 1;
            lbl_date_cr.frame = CGRectMake(lbl_date_cr.frame.origin.x - 20, lbl_date_cr.frame.origin.y, lbl_date_cr.frame.size.width, lbl_date_cr.frame.size.height);
            lbl_date_cr.alpha = 1;
            lbl_tran_amount_cr.frame = CGRectMake(lbl_tran_amount_cr.frame.origin.x - 20, lbl_tran_amount_cr.frame.origin.y, lbl_tran_amount_cr.frame.size.width, lbl_tran_amount_cr.frame.size.height);
            lbl_tran_amount_cr.alpha = 1;
            lbl_type_cr.frame = CGRectMake(lbl_type_cr.frame.origin.x - 20, lbl_type_cr.frame.origin.y, lbl_type_cr.frame.size.width, lbl_type_cr.frame.size.height);
            lbl_type_cr.alpha = 1;
            
            
            welcome_name.alpha = 1;
            welcome_day.alpha = 1;
            welcome_date.alpha = 1;
            
            
        }];
        
        
        [UIView animateWithDuration:1
                              delay:0
             usingSpringWithDamping:0.5
              initialSpringVelocity:1
                            options:UIViewAnimationOptionCurveEaseIn animations:^{
                                //Animations
                                
                                
                                self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x,  self.navigationBar.frame.origin.y  , self.navigationBar.frame.size.width, self.navigationBar.frame.size.height + 40);
                           
//saki 4 july 2019
                                if ([gblclass.story_board isEqualToString:@"iPhone_X"])
                                {
                                    self.accountOutlet.frame = CGRectMake(self.accountOutlet.frame.origin.x, self.view.frame.size.height - 25 - self.accountOutlet.frame.size.height, self.accountOutlet.frame.size.width, self.accountOutlet.frame.size.height);
                                }
                                else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
                                {
                                    self.accountOutlet.frame = CGRectMake(self.accountOutlet.frame.origin.x, self.view.frame.size.height - 25 - self.accountOutlet.frame.size.height, self.accountOutlet.frame.size.width, self.accountOutlet.frame.size.height);
                                }
                                else
                                {
                                    self.accountOutlet.frame = CGRectMake(self.accountOutlet.frame.origin.x, self.view.frame.size.height - 1 - self.accountOutlet.frame.size.height, self.accountOutlet.frame.size.width, self.accountOutlet.frame.size.height);
                                }
                                
                                
                                if ([gblclass.story_board isEqualToString:@"iPhone_X"])
                                {
                                    self.payOutlet.frame = CGRectMake(self.payOutlet.frame.origin.x, self.view.frame.size.height - 25 -  self.payOutlet.frame.size.height, self.payOutlet.frame.size.width, self.payOutlet.frame.size.height);
                                }
                                else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
                                {
                                    self.payOutlet.frame = CGRectMake(self.payOutlet.frame.origin.x, self.view.frame.size.height - 25 -  self.payOutlet.frame.size.height, self.payOutlet.frame.size.width, self.payOutlet.frame.size.height);
                                }
                                else
                                {
                                    self.payOutlet.frame = CGRectMake(self.payOutlet.frame.origin.x, self.view.frame.size.height - 1 -  self.payOutlet.frame.size.height, self.payOutlet.frame.size.width, self.payOutlet.frame.size.height);
                                }
                                
                            }
                         completion:^(BOOL finished) {
                             //Completion Block
                         }];
        
        
        [UIView animateWithDuration:1.2
                              delay:0
             usingSpringWithDamping:0.5
              initialSpringVelocity:1
                            options:UIViewAnimationOptionCurveEaseIn animations:^{
                                //Animations
                                
                                _Profile_imageView.frame = CGRectMake(_Profile_imageView.frame.origin.x, _Profile_imageView.frame.origin.y,_Profile_imageView.frame.size.height /2, _Profile_imageView.frame.size.width /2);
                                
                            }
                         completion:^(BOOL finished) {
                             //Completion Block
                         }];
        
        runOnce = YES;
        
    }
}



-(void)ChangeImage
{
    [self viewDidAppear:NO];
}

-(void)swipeAction
{
    [self btn_more:self];
}


- (void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    NSData *pro_img = [user_default dataForKey:@"profile_Img"];
    
    NSData *pro_img1 = [user_default dataForKey:@"profile_Img"];
    
    _Profile_imageView.image = nil;
    if (pro_img.length==0)
    {
        [self.Profile_imageView setImage:[UIImage imageNamed:@"profile_img1.png"]]; //profile_img1  Profile.png
    }
    else
    {
        
        UIImage *contactImage = [UIImage imageWithData:pro_img];
        
        // Update the UI elements with the saved data
        _Profile_imageView.image = contactImage;
        _Profile_imageView.contentMode = UIViewContentModeScaleAspectFill;
        
    }
}


-(void)viewLoad
{
    
}

- (void) myInstanceMethod1
{
    //     user_default = [NSUserDefaults standardUserDefaults];
    //     NSData *pro_img = [user_default dataForKey:@"profile_Img"];
    //
    //    // UIImage *contactImage = [UIImage imageWithData:pro_img];
    //    _Profile_imageView.image = nil;
    //
    //
    //    if (pro_img.length==0)
    //    {
    //        [self.Profile_imageView setImage:[UIImage imageNamed:@"Profile.png"]];
    //    }
    //    else
    //    {
    //        //  NSData *imageData4 = [user_default dataForKey:@"profile_Img"];
    //        UIImage *contactImage = [UIImage imageWithData:pro_img];
    //
    //        // Update the UI elements with the saved data
    //        _Profile_imageView.image = contactImage;
    //    }
    
}

//- (void)applicationWillEnterForeground:(UIApplication *)application
//{
//    //NSLog(@"app will enter foreground");
//}
//
//- (void)applicationDidBecomeActive:(UIApplication *)application
//{
//    //NSLog(@"app did become active");
//}
//- (void)appDidBecomeActive:(NSNotification *)notification
//{
//    //NSLog(@"did become active notification");
//}




- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    
}

- (void)playerStartPlaying
{
    
}



-(UIResponder *)nextResponder
{
    //NSLog(@"**** nextResponder ****");
    [[APIdleManager sharedInstance] didReceiveInput];
    
    return [super nextResponder];
}

-(void) AccountSummary:(NSString *)strIndustry{
    
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //@"xt41arwyh320udbarhw35ywl"
    
    NSString* pww,*usser;
    
    if ([gblclass.From_Exis_login isEqualToString:@"1"])
    {
        usser = gblclass.signup_username;
        pww = gblclass.signup_pw;
    }
    else
    {
        usser = gblclass.sign_in_username;
        pww = gblclass.sign_in_pw;
    }
    
    
    //NSLog(@"%@",gblclass.sign_in_username);
    //NSLog(@"%@",gblclass.signup_username);
    //NSLog(@"%@",gblclass.Udid);
    //NSLog(@"%@",pww);
    //NSLog(@"%@",gblclass.M3sessionid);
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:usser,pww,gblclass.Udid,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],@"0", nil] forKeys:[NSArray arrayWithObjects:@"LoginName",@"strPassword",@"strDeviceID",@"strSessionId",@"IP",@"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"mobSignIn" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //         NSError *error;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //gblclass.actsummaryarr =[[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbRefreshAcct"];
              
              gblclass.token=[dic objectForKey:@"Token"];
              
              gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
              gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
              gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
              gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
              gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
              
              
              gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
              
              gblclass.actsummaryarr =  [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
              
              //NSLog(@"%@",gblclass.actsummaryarr );
              
              //NSLog(@"Done");
              [self parsearray];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              //NSLog(@"%@",[error localizedDescription]);
              
              gblclass.custom_alert_msg=@"Retry";
              gblclass.custom_alert_img=@"0";
              
              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
              vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
              vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
              vc.view.alpha = alpha1;
              [self presentViewController:vc animated:NO completion:nil];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to login, Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    view.tintColor = [UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f];
    
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
    header.textLabel.text = [account_typedesc objectAtIndex:section];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *Title;
    if([account_typedesc count]==0)
    {
        return @"N/A";
    }
    else
    {
        NSString *itag = [account_typedesc objectAtIndex:section];
        Title = itag;
    }
    
    return Title;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"numberOfSectionsInTableView: %lu",(unsigned long)[account_typedesc  count]);
    
    if([account_typedesc  count] == 0)
    {
        return 0;
    }
    else
    {
        return [account_typedesc  count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    andr wala
    if([account_typedesc count] == 0)
    {
        return 0;
    }
    else
    {
        
        NSMutableArray *xrows = [sections objectAtIndex:section ];
        
        return [xrows count];
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UIStoryboard *mainStoryboard;
    //    UIViewController *vc;
    
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    
    cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    NSMutableArray *xrows1 = [sections objectAtIndex:indexPath.section];
    
    Table_de *table_obj1 = [Table_de new];
    table_obj = [xrows1 objectAtIndex:indexPath.row];
    
    
    
    gblclass.summary_acct_no=table_obj1.accountNo;
    
    
    gblclass.defaultaccountno=table_obj1.registeredAccountId;
    gblclass.chk_acct_statement=@"0";
    
    
    gblclass.summary_acct_name=table_obj1.accountName;
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // UIButton *button;
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    xrows = [sections objectAtIndex:indexPath.section];
    
    
    table_obj = [Table_de new];
    table_obj = [xrows objectAtIndex:indexPath.row];
    
    @try {
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        if([table_obj.accountTypeDesc isEqualToString:@"Credit Card Account"]||[table_obj.accountTypeDesc isEqualToString:@"UBL UAE CC"])
        {
            [formatter stringFromNumber:table_obj.balance];
            label=(UILabel*)[cell viewWithTag:1];
            label.text=table_obj.accountName;
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text=table_obj.cardNumber;
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            NSString *mystring =[NSString stringWithFormat:@"%@",table_obj.m3Balance];
            NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
            
            
            if (table_obj.m3Balance.length==0)
            {
                label.text=@"0";
            }
            else
            {
                label.text=[NSString stringWithFormat:@"%@ %@", gblclass.base_currency,[formatter stringFromNumber:number]];
            }
            
            // label.text=@"Balance: ";
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:4];
            if ([table_obj.currencyDescr isEqualToString:@""] || table_obj.currencyDescr.length==0)
            {
                label.text=gblclass.base_currency;
            }
            else
            {
                label.text=table_obj.currencyDescr;
            }
            
            label.font=[UIFont systemFontOfSize:7];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            
            //NSLog(@"%@",table_obj.m3Balance);
            
            
            
            if (table_obj.creditLimit.length==0)
            {
                label.text=@"0";
            }
            else
            {
                NSString *mystringlmi =[NSString stringWithFormat:@"%@",table_obj.creditLimit];
                NSNumber *numberlmi = [NSDecimalNumber decimalNumberWithString:mystringlmi];
                
                label.text=[NSString stringWithFormat:@"%@ %@", gblclass.base_currency,[formatter stringFromNumber:numberlmi]];
            }
            
            [cell.contentView addSubview:label];
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            
            [paybtn setTitle:@"Pay" forState:UIControlStateNormal];
            //   paybtn.tag = indexPath.row;
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [paybtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            paybtn.hidden=NO;
            [cell.contentView addSubview:paybtn];
            
        }
        else
        {
            
            
            
            label=(UILabel*)[cell viewWithTag:1];
            //NSLog(@"%@",table_obj.accountName);
            label.text=table_obj.accountName;
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text=table_obj.accountNo;
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text=table_obj.branchName;
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:4];
            if ([table_obj.currencyDescr isEqualToString:@""] || table_obj.currencyDescr.length==0)
            {
                label.text=gblclass.base_currency;
            }
            else
            {
                label.text=table_obj.currencyDescr;
            }
            label.font=[UIFont systemFontOfSize:7];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            //NSLog(@"%@",table_obj.accountTypeDesc);
            
            
            //NSLog(@"%@",table_obj.m3Balance);
            NSString *mystring =[NSString stringWithFormat:@"%@",table_obj.m3Balance];
            NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
            // NSNumberFormatter *formatter = [NSNumberFormatter new];
            
            
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            if ([mystring isEqualToString:@"(null)"])
            {
                number=[NSNumber numberWithInt:0];//[NSDecimalNumber decimalNumberWithDecimal:@"12"];
            }
            ;
            
            label.text=[NSString stringWithFormat:@"%@ %@", gblclass.base_currency,[formatter stringFromNumber:number]];
            [cell.contentView addSubview:label];
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            
            [paybtn setTitle:nil forState:UIControlStateNormal];
            //   paybtn.tag = indexPath.row;
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [paybtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            paybtn.hidden=NO;
            [cell.contentView addSubview:paybtn];
            
            
            if([table_obj.accountTypeDesc isEqualToString:@"UBL WIZ Card"]) //Omni AccountUBL WIZ Card
            {
                paybtn=(UIButton*)[cell viewWithTag:10];
                
                [paybtn setTitle:@"Load" forState:UIControlStateNormal];
                //   paybtn.tag = indexPath.row;
                cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
                [paybtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                paybtn.hidden=NO;
                [cell.contentView addSubview:paybtn];
                
            }
        }
        
        return cell;
        
    }
    @catch (NSException *exception)
    {
        //NSLog(@"Error  :: %@",exception);
    }
    
}

- (void)yourButtonClicked:(UIButton *)sender
{
    //NSLog(@"%ld",(long)sender.tag);
    
    UITableViewCell *cell = (UITableViewCell *)sender.superview;
    NSIndexPath *indexPath = [table1 indexPathForCell:cell];
    
    NSIndexPath *indexPath2 = [NSIndexPath indexPathWithIndex:[sender tag]];
    //NSLog(@"%ld",(long)indexPath2);
    
    int rowNum = [(UIButton*)sender tag];
    
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *clickedButtonIndexPath = [table1 indexPathForCell:clickedCell];
    
    
    //NSLog(@"%@",xrows);
    table_obj = [Table_de new];
    table_obj = [xrows objectAtIndex:0];
    
    
    CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:self.table1];
    NSIndexPath *indexPath1 = [self.table1 indexPathForRowAtPoint:buttonPosition1];
    
    gblclass.direct_pay_frm_Acctsummary_nick=table_obj.accountName;
    gblclass.direct_pay_frm_Acctsummary_customer_id=table_obj.accountNo;
    
    //NSLog(@"%@",table_obj.accountTypeDesc);
    if ([table_obj.accountTypeDesc  isEqual: @"UBL WIZ Card"])
    {
        gblclass.direct_pay_frm_Acctsummary=@"1";
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"wiz_bill"]; //pay_within_acct
        
        [self presentViewController:vc animated:NO completion:nil];
    }
    else if ([table_obj.accountTypeDesc  isEqual: @"Credit Card Account"])
    {
        gblclass.direct_pay_frm_Acctsummary=@"1";
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"UBP_bill"]; //pay_within_acct
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


-(void)parsearray
{
    
    @try {
        
        NSMutableArray *a = [[NSMutableArray alloc] init];
        NSMutableArray *aa = [[NSMutableArray alloc] init];
        account_name = [[NSMutableArray alloc]init];
        account_number= [[NSMutableArray alloc]init];
        account_currencydesc = [[NSMutableArray alloc]init];
        account_typedesc = [[NSMutableArray alloc]init];
        rows= [[NSMutableArray alloc] init];
        sections= [[NSMutableArray alloc] init];
        
        gblclass.arr_transfer_within_acct = [[NSMutableArray alloc] init];
        gblclass.arr_transfer_within_acct_to = [[NSMutableArray alloc] init];
        branch= [[NSMutableArray alloc]init];
        
        balc_date= [[NSMutableArray alloc]init];
        avail_bal= [[NSMutableArray alloc]init];
        
        //int value = [number intValue];
        NSUInteger *set;
        //NSLog(@"this is count of global array %lu",(unsigned long)[ gblclass.actsummaryarr count]);
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"Sort_Order_ACC" ascending:YES];
        
        gblclass.actsummaryarr=[gblclass.actsummaryarr sortedArrayUsingDescriptors:@[sort]];
        
        //NSLog(@"%@",gblclass.actsummaryarr);
        
        for (dic in gblclass.actsummaryarr)
        {
           // set = [gblclass.actsummaryarr indexOfObject:dic];
            //           //NSLog(@"%d",set);
            
            if([gblclass.storyboardidentifer isEqualToString:@"accounts"])
            {
                classObj = [[Table_de alloc] initWithDictionary:dic];
            }
            else
            {
                classObj = [[GnbRefreshAcct alloc] initWithDictionary:dic];
            }
            
            //NSLog(@"%@",classObj.accountName);
            
            if([classObj.isDefault isEqualToString:@"1"])
            {
                gblclass.defaultaccountno=classObj.registeredAccountId;
                gblclass.defaultaccountname = classObj.accountName;
                gblclass.is_default_m3_balc=classObj.m3Balance;
            }
            
            //NSLog(@"%@",classObj.accountTypeDesc);
            
            if([classObj.accountTypeDesc isEqualToString:@"Deposit Account"])
            {
                if([classObj.countryCode integerValue] == 1000)
                {
                    
                    //NSLog(@"%@",classObj.m3Balance);
                    if([classObj.ccy isEqualToString:@"PKR"])
                    {
                        self.upper_currency.text = classObj.ccy;
                        
                        if (classObj.m3Balance==0)
                        {
                            //NSLog(@"%@",classObj.balance);
                            if (classObj.balance.length>0)
                            {
                                [avail_bal addObject:classObj.balance];
                            }
                            else
                            {
                                [avail_bal addObject:@"0"];
                            }
                            
                        }
                        else
                        {
                            [avail_bal addObject:classObj.m3Balance];
                        }
                    }
                    
                }
                
                if([classObj.countryCode integerValue] == 4000)
                {
                    self.upper_currency.text = classObj.ccy;
                    
                    if([classObj.ccy isEqualToString:@"AED"])
                    {
                        [avail_bal addObject:classObj.m3Balance];
                    }
                }
                
                if([classObj.countryCode integerValue] == 4096)
                {
                    self.upper_currency.text = classObj.ccy;
                    
                    if([classObj.ccy isEqualToString:@"QAR"])
                    {
                        [avail_bal addObject:classObj.m3Balance];
                    }
                }
                
                if([classObj.countryCode integerValue] == 4112)
                {
                    self.upper_currency.text = classObj.ccy;
                }
            }
            
            if(![account_typedesc containsObject:classObj.accountTypeDesc] && set != 0)
            {
                [account_typedesc addObject:classObj.accountTypeDesc];
                [sections addObject:rows];
                rows= [[NSMutableArray alloc] init];
            }
            else if( set == 0)
            {
                [account_typedesc addObject:classObj.accountTypeDesc];
            }
            
            [rows  addObject:classObj];
            
            
            // ************************
            
            
            NSString* acct_name=[dic objectForKey:@"account_name"];
            
            if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
            {
                acct_name=@"N/A";
                [a addObject:acct_name];
            }
            else
            {
                [a addObject:acct_name];
            }
            
            
            NSString* acct_no=[dic objectForKey:@"account_no"];
            if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
            {
                acct_no=@"N/A";
                [a addObject:acct_no];
            }
            else
            {
                [a addObject:acct_no];
            }
            
            
            NSString* acct_id=[dic objectForKey:@"registered_account_id"];
            if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
            {
                acct_id=@"N/A";
                [a addObject:acct_id];
            }
            else
            {
                [a addObject:acct_id];
            }
            
            NSString* privileges=[dic objectForKey:@"privileges"];
            privileges = [privileges substringWithRange:NSMakeRange(1,1)]; //(1,1) //Second bit 1 for withDraw ..
            
            if ([privileges isEqualToString:@"0"])
            {
                privileges=@"0";
                [a addObject:privileges];
            }
            else
            {
                [a addObject:privileges];
            }
            
            NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
            if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
            {
                account_type_desc=@"N/A";
                [a addObject:account_type_desc];
            }
            else
            {
                [a addObject:account_type_desc];
            }
            //account_type
            
            NSString* account_type=[dic objectForKey:@"account_type"];
            if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
            {
                account_type=@"N/A";
                [a addObject:account_type];
            }
            else
            {
                [a addObject:account_type];
            }
            
            NSString* m3_balc=[dic objectForKey:@"m3_balance"];
            
            
            //((NSString *)[NSNull null] == m3_balc)
            //[m3_balc isEqualToString:@" "] ||
            
            
            //27 OCt New Sign IN Method :::
            
            if (m3_balc ==(NSString *) [NSNull null])
            {
                m3_balc=@"0";
                [a addObject:m3_balc];
            }
            else
            {
                [a addObject:m3_balc];
            }
            
            
            NSString* branch_name=[dic objectForKey:@"branch_name"];
            if ( branch_name == (NSString *)[NSNull null])
            {
                branch_name=@"N/A";
                [a addObject:branch_name];
            }
            else
            {
                [a addObject:branch_name];
            }
            
            NSString* available_balance=[dic objectForKey:@"available_balance"];
            if ( available_balance == (NSString *)[NSNull null])
            {
                available_balance=@"0";
                [a addObject:available_balance];
            }
            else
            {
                [a addObject:available_balance];
            }
            
            NSString* br_code=[dic objectForKey:@"br_code"];
            if ( br_code == (NSString *)[NSNull null])
            {
                br_code=@"0";
                [a addObject:br_code];
            }
            else
            {
                [a addObject:br_code];
            }
            
            NSString* ccy=[dic objectForKey:@"ccy"];
            if ( ccy == (NSString *)[NSNull null])
            {
                ccy=@"0";
                [a addObject:ccy];
            }
            else
            {
                [a addObject:ccy];
            }
            
            
            NSString *bbb = [a componentsJoinedByString:@"|"];
            
            [gblclass.arr_act_statmnt_name addObject:bbb];
            
          
            
            if ([privileges isEqualToString:@"1"])
            {
                if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                {
//                    gblclass.arr_transfer_within_acct = [[NSMutableArray alloc] init];
                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                }
            }
            
           // NSLog(@"%@",gblclass.arr_transfer_within_acct);
            
            NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
            privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
            
            
            if ([privileges_to isEqualToString:@"1"])
            {
//                gblclass.arr_transfer_within_acct_to = [[NSMutableArray alloc] init];
                [gblclass.arr_transfer_within_acct_to addObject:bbb];
            }
             
            //NSLog(@"%@", bbb);
            [a removeAllObjects];
            [aa removeAllObjects];
 
            
            // ************************
        }
        
        [sections addObject:rows];
        
        value2=0;
        for(int i = 0;i<[avail_bal count];i++)
        {
            
            bal =[[avail_bal objectAtIndex:i] doubleValue];
            
            value2 =value2 + [[avail_bal objectAtIndex:i] doubleValue];
            
            self.upper_amount.text=[NSString stringWithFormat:@"%f",value2];
            //}
        }
        
        format_2 = [[NSNumberFormatter alloc]init];
        [format_2 setNumberStyle:NSNumberFormatterDecimalStyle];
        [format_2 setRoundingMode:NSNumberFormatterRoundHalfUp];
        //   [format_2 setMaximumFractionDigits:2];
        //   [format_2 setPositiveSuffix:@"+"];
        
        //  _upper_amount.text=@"9999999";
        number_2= [_upper_amount.text integerValue];
        
        
        
        //Jahangir code from
        
        //        number_2 = number_2;
        //        value2 = number_2 ;
        
        NSString *wordNumber;
        if (number_2 >= 10000000)
        {
            number_2 = (int) number_2 / 1000000;
            wordNumber = @"Million";
        }
        else
        {
            NSNumber *numberValue = [NSNumber numberWithFloat:number_2]; //needs to be NSNumber!
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterSpellOutStyle];
            wordNumber = [numberFormatter stringFromNumber:numberValue];
        }
        /*
         else if (number_2 >= 100001){
         number_2 = (int) number_2 / 100000;
         wordNumber = @"Hundred Thousand";
         }
         else if (number_2 >= 1001){
         number_2 = (int) number_2 / 1000;
         wordNumber = @"Thousand";
         }
         else if (number_2 >= 101){
         number_2 = (int) number_2 / 100;
         wordNumber = @"Hundred";
         }
         else {
         number_2 = (int) number_2;
         wordNumber = @"Rupees";
         }*/
        
        //Jahangir code till here
        
        
        //[format_2 stringFromNumber:number_2]
        
        //number_2   value2
        if (value2 >= 10000000)
        {
            
            NSTextAttachment * attach = [[NSTextAttachment alloc] init];
            attach.image = [UIImage imageNamed:@"Plus-b.png"];
            attach.bounds = CGRectMake(0, 10, 10,10);
            NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
            
            NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.f",number_2]];
            NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:30.0]};
            [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
            
            [mutableAttriStr appendAttributedString:imageStr];
            self.upper_amount.attributedText = mutableAttriStr;
        }
        else
        {
            self.upper_amount.text = [format_2 stringFromNumber:[NSNumber numberWithFloat:number_2]];
        }
        
        //temp_1 = [format_2 stringFromNumber:[NSNumber numberWithFloat:number_2]];
        //NSLog(@"%@",temp_1);
        
        
        //        self.upper_amount.text=temp_1;
        //        float number2=bal;
        //        NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
        //        [format setNumberStyle:NSNumberFormatterDecimalStyle];
        //        [format setRoundingMode:NSNumberFormatterRoundHalfUp];
        //        [format setMaximumFractionDigits:2];
        
        
        
        
        
        //        NSString *temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
        //        //NSLog(@"%@",temp);
        //
        //        NSString *mystring =[NSString stringWithFormat:@"%@",self.upper_amount.text];
        //        NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
        //
        //        NSNumberFormatter *formatter = [NSNumberFormatter new];
        //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        //
        //        gblclass.tot_balc=[NSString stringWithFormat:@"%@",[formatter stringFromNumber:number]];
        //
        //
        //        NSString* middle_num=[temp_1 stringByReplacingOccurrencesOfString:@"," withString:@""];
        //
        //        NSInteger anInt = [middle_num integerValue] ;
        //
        
        //Remarked by Jahangir
        //convert to words
        //NSNumber *numberValue = [NSNumber numberWithInt:anInt]; //needs to be NSNumber!
        //        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        //        [numberFormatter setNumberStyle:NSNumberFormatterSpellOutStyle];
        //        wordNumber = [numberFormatter stringFromNumber:numberValue];
        //NSLog(@"Answer: %@", wordNumber);
        
        lbl_num_to_word.text=[wordNumber capitalizedString];
        //Remarked till here
        [self.table1 reloadData];
        
        
        [self checkinternet];
        if (netAvailable)
        {
//            chk_ssl=@"transaction";
//            [self SSL_Call];

              [self Display_GetLast_DrCr_TransHistory:@""];
        }
        
        
        //   [self Display_GetLast_DrCr_TransHistory:@""];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Retry";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to login, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
    }
    
}



-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)btn_more:(id)sender
{

    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
//    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    Slide_menu_VC *vc1 = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
//    vc1.delegate=self;
//
//    vc1.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
//    [vc1 setTransitioningDelegate:transitionController];
//    vc1.modalPresentationStyle= UIModalPresentationCustom;
//    [self presentViewController:vc1 animated:YES completion:nil];
    
}


-(IBAction)btn_account:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        [self presentViewController:vc animated:NO completion:nil];
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
}


-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:YES completion:nil];;
}
-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay // bill_all
    
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        //[self mob_App_Logout:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(IBAction)btn_ref:(id)sender
{
    [self.table1 reloadData];
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UIImagePickerController *picker;
    
    
    switch (popup.tag)
    {
        case 1:
        {
            switch (buttonIndex)
            {
                case 0:
                    //  Gallery ::
                    
                    picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.allowsEditing = YES;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    [self presentViewController:picker animated:YES completion:NULL];
                    
                    self.profile_view.hidden=YES;
                    
                    break;
                case 1:
                    //  Camera ::
                    
                    _session = [[AVCaptureSession alloc] init];
                    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                    NSError *error = nil;

                    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
                    if (_input)
                    {
                        //[_session addInput:_input];
                        
                        
                        picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        [self presentViewController:picker animated:YES completion:NULL];
                        
                        self.profile_view.hidden=YES;
                    }
                    else
                    {
                        //NSLog(@"Error: %@", error);
                        
//                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Allow UBL Digital App to access your Camera in your Phone Settings"];
//                        NSRange selectedRange = NSMakeRange(22, 4); // 4 characters, starting at index 22
//
//                        [string beginEditing];
//
//                        [string addAttribute:NSFontAttributeName
//                                       value:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]
//                                       range:selectedRange];
//
//
//                       UIAlertController* alert  = [UIAlertController alertControllerWithTitle:(NSString *)string message:@"This will enable you to upload your Profile picture on the app" preferredStyle:UIAlertControllerStyleAlert];
//
//
//
//                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                        [alert addAction:ok];
//                        [self presentViewController:alert animated:YES completion:nil];
//
//                         [string endEditing];
                        
                        
                        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Allow UBL Digital App to access your Camera in your Phone Settings"
                                                                        message:@"This will enable you to upload your Profile picture on the app"
                                                                       delegate:nil                                             cancelButtonTitle:nil
                                                              otherButtonTitles:@"OK", nil];
                        [alert1 show];

                        return;
                    }
//
//
//
                    
//                    NSString *mediaType = AVMediaTypeVideo;
//                    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
//                    if(authStatus == AVAuthorizationStatusAuthorized)
//                    {
//                        // do your logic
//                    }
//                    else if(authStatus == AVAuthorizationStatusDenied)
//                    {
//                        // denied
//                    }
//                    else if(authStatus == AVAuthorizationStatusRestricted)
//                    {
//                        // restricted, normally won't happen
//                    }
//                    else if(authStatus == AVAuthorizationStatusNotDetermined)
//                    {
//                        // not determined?!
//                        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
//                            if(granted)
//                            {
//                                NSLog(@"Granted access to %@", mediaType);
//                            }
//                            else
//                            {
//                                NSLog(@"Not granted access to %@", mediaType);
//                            }
//                        }];
//                    }
//                    else
//                    {
//                        // impossible, unknown authorization status
//                    }
                    
                    
                    
              
                    
                    
                    break;
            }
        default:
            break;
        }
    }
}


- (IBAction)btn_profile:(UIButton *)sender
{
    self.profile_view.hidden=NO;
    
    
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        //NSLog(@"%zd", [group numberOfAssets]);
        
       // NSLog(@"permission");
        
    }
    failureBlock:^(NSError *error)
     {
         if (error.code == ALAssetsLibraryAccessUserDeniedError)
         {
             //NSLog(@"user denied access, code: %zd", error.code);

             UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Allow UBL Digital App to access your Photos in your Phone Settings" message:@"This will enable you to upload your Profile picture on the app" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
             [alert1 show];
             
             return ;
         }
         else
         {
             //NSLog(@"Other error code: %zd", error.code);
         }
         
     }];
    
 
        ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    
        if (status != ALAuthorizationStatusAuthorized)
        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Please give this app permission to access your photo library in your settings app!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
//            [alert show];
            
         //   NSLog(@"Permission Not");
        }
        else
        {
           // NSLog(@"Permission Ok");
            
            UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                    @"Select From Gallery",
                                    @"Select From Camera",
                                    nil];
            popup.tag = 1;
            [popup showInView:self.view];
        }
}

- (IBAction)takePhoto:(UIButton *)sender
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    self.profile_view.hidden=YES;
    
}

- (IBAction)selectPhoto:(UIButton *)sender
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    self.profile_view.hidden=YES;
    
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    
    
    NSString* str=[NSString stringWithFormat:@"%@",[info objectForKey:@"UIImagePickerControllerReferenceURL"]];
    
    //      user_default=[NSUserDefaults standardUserDefaults];
    //    // saving an NSString
    //    [user_default setObject:str forKey:@"profile_Img"];
    
    
    NSURL *url = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    NSString *ref = url.absoluteString;
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.Profile_imageView.image = chosenImage;
    
    
    // Create instances of NSData
    UIImage *contactImage = _Profile_imageView.image;
    NSData *imageData4 = UIImageJPEGRepresentation(contactImage, 100);
    [user_default setObject:imageData4 forKey:@"profile_Img"];
    [user_default synchronize];
    
    
    NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
    
    NSData *imageData = UIImageJPEGRepresentation(_Profile_imageView.image, 1.0);
    
    BOOL result = [imageData writeToFile:imageName atomically:YES];
    
    //NSLog(@"Saved to %@? %@", imageName, (result? @"YES": @"NO"));
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
//    NSString* str=[NSString stringWithFormat:@"%@",[info objectForKey:@"UIImagePickerControllerReferenceURL"]];
//
//    //    user_default=[NSUserDefaults standardUserDefaults];
//    //    // saving an NSString
//    //    [user_default setObject:str forKey:@"profile_Img"];
//
//
//    NSURL *url = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
//    NSString *ref = url.absoluteString;
//
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    self.Profile_imageView.image = chosenImage;
//
//
//    // Create instances of NSData
//    UIImage *contactImage = _Profile_imageView.image;
//    NSData *imageData4 = UIImageJPEGRepresentation(contactImage, 100);
//    [user_default setObject:imageData4 forKey:@"profile_Img"];
//    [user_default synchronize];
//
//
//    NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
//    NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
//
//    NSData *imageData = UIImageJPEGRepresentation(_Profile_imageView.image, 1.0);
//
//    BOOL result = [imageData writeToFile:imageName atomically:YES];
//
//    //NSLog(@"Saved to %@? %@", imageName, (result? @"YES": @"NO"));
//
//    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _profile_view.hidden=YES;
}


- (IBAction)btn_acct_summary:(UIButton *)sender
{
    
    
    if ([chck_show_btn isEqualToString:@"0"])
    {
        [btn_summary setTitle:@"" forState:UIControlStateNormal];
        btn_summary.backgroundColor = [UIColor clearColor];
        chck_show_btn = @"1";
    }
    else if ([chck_show_btn isEqualToString:@"1"])
    {
        [btn_summary setTitle:@"Show Balance" forState:UIControlStateNormal];
        btn_summary.tintColor = [UIColor blackColor];
        //  [btn_summary.titleLabel setFont:[UIFont boldSystemFontOfSize:20.f]];
        btn_summary.backgroundColor = [UIColor colorWithRed:236/255.0 green:239/255.0 blue:248/255.0 alpha:1.0];
        chck_show_btn = @"0";
    }

    
//    [self checkinternet];
//    if (netAvailable)
//    {
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
//
//        [self presentViewController:vc animated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window. layer addAnimation:transition forKey:nil];
//
//    }
}




///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
         //   [self mob_Sign_In:@""];
            [self AccountSummary2:@""];
        }
        else if ([chk_ssl isEqualToString:@"transaction"])
        {
            chk_ssl=@"";
            [self Display_GetLast_DrCr_TransHistory:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
            
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
      //  [self mob_Sign_In:@""];
        [self AccountSummary2:@""];
    }
    else if ([chk_ssl isEqualToString:@"transaction"])
    {
        chk_ssl=@"";
        [self Display_GetLast_DrCr_TransHistory:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Check Internet -
-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            UIAlertView *magalert33 = [[UIAlertView alloc] initWithTitle:@"Notification" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [magalert33 show];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) Display_GetLast_DrCr_TransHistory:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.user_id]);
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.registd_acct_id_trans]);
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.base_currency]);
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.M3sessionid]);
//        NSLog(@"%@",[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]]);
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.Udid]);
//        NSLog(@"%@",[encrypt encrypt_Data:gblclass.token]);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.registd_acct_id_trans],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strFromAccountId",
                                                                       @"strCurrency",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"DisplayGetLastDrCrTransHistory" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      return;
                  }
                  
                   if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                   {
                       
                       arr_display_trans=[[NSMutableArray alloc] init];
                       arr_display_trans=[(NSDictionary *)[dic2 objectForKey:@"outdtDataset"] objectForKey:@"Table"];
                  
                  
                  if ([arr_display_trans count]==0)
                  {
                      [hud hideAnimated:YES];
                      
                      lbl_date_dr.text=@"-";
                      lbl_tran_amount_dr.text=@"-";
                      lbl_type_dr.text=@"-";
                      
                      lbl_date_cr.text=@"-";
                      lbl_tran_amount_cr.text=@"-";
                      lbl_type_cr.text=@"-";
                      
                  }
                  else
                  {
                      
                      for (dic2 in arr_display_trans)
                      {
                          
                          
                          NSString* DR_TRAN_DATE=[dic2 objectForKey:@"DR_TRAN_DATE"];
                          double DR_TRAN_AMOUNT=[[dic2 objectForKey:@"DR_TRAN_AMOUNT"] doubleValue] ;
                          
                          //                          NSString* DR_TRAN_TYPE=[dic2 objectForKey:@"DR_TRAN_TYPE"];
                          //                          double DR_STATUS=[[dic2 objectForKey:@"DR_STATUS"] doubleValue];
                          
                          
                          NSString* CR_TRAN_DATE=[dic2 objectForKey:@"CR_TRAN_DATE"];
                          double CR_TRAN_AMOUNT=[[dic2 objectForKey:@"CR_TRAN_AMOUNT"] doubleValue];
                          
                          //                          NSString* CR_TRAN_TYPE=[dic2 objectForKey:@"CR_TRAN_TYPE"];
                          //                          double CR_STATUS=[[dic2 objectForKey:@"CR_STATUS"] doubleValue];
                          
                          NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                          [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
                          
                          if (!(DR_TRAN_DATE==(NSString *)[NSNull null]))
                          {
//                              NSString *actDate =DR_TRAN_DATE;
//                              NSString *nDate = [[[[actDate componentsSeparatedByString:@"("] objectAtIndex:1] componentsSeparatedByString:@")"] objectAtIndex:0];
//                              NSDate *date_dr = [NSDate dateWithTimeIntervalSince1970:([nDate doubleValue] / 1000)];
//                              NSString *debit = [dateFormatter stringFromDate:date_dr];
                              
                              lbl_date_dr.text= DR_TRAN_DATE;//[NSString stringWithFormat:@"%@",debit];
                          }
                          else
                          {
                              lbl_date_dr.text=@"-";
                          }
                          
                          if (!(CR_TRAN_DATE==(NSString *)[NSNull null]))
                          {
                              
//                              NSString *actDate2 =CR_TRAN_DATE;
//                              NSString *nDate2 = [[[[actDate2 componentsSeparatedByString:@"("] objectAtIndex:1] componentsSeparatedByString:@")"] objectAtIndex:0];
//                              NSDate *date_cr = [NSDate dateWithTimeIntervalSince1970:([nDate2 doubleValue] / 1000)];
//                              NSString *credit = [dateFormatter stringFromDate:date_cr];
                              
                              lbl_date_cr.text = CR_TRAN_DATE;// [NSString stringWithFormat:@"%@",credit];
                          }
                          else
                          {
                              lbl_date_cr.text=@"-";
                          }
                          
                          
                          
                          NSString *mystring1 =[NSString stringWithFormat:@"%.2f",DR_TRAN_AMOUNT];
                          NSNumber *number1 = [NSDecimalNumber decimalNumberWithString:mystring1];
                          NSNumberFormatter *formatter1 = [NSNumberFormatter new];
                          [formatter1 setMinimumFractionDigits:2];
                          [formatter1 setMaximumFractionDigits:2];
                          [formatter1 setNumberStyle:NSNumberFormatterDecimalStyle];
                          
                          //NSLog(@"%@",[formatter1 stringFromNumber:number1]);
                          
                          lbl_tran_amount_dr.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter1 stringFromNumber:number1]];
                          
                          lbl_type_dr.text=@"Last Debit";    //[NSString stringWithFormat:@"%.2f",DR_STATUS];
                          
                          
                          
                          NSString *mystring2 =[NSString stringWithFormat:@"%.2f",CR_TRAN_AMOUNT];
                          NSNumber *number2 = [NSDecimalNumber decimalNumberWithString:mystring2];
                          NSNumberFormatter *formatter2 = [NSNumberFormatter new];
                          [formatter2 setMinimumFractionDigits:2];
                          [formatter2 setMaximumFractionDigits:2];
                          [formatter2 setNumberStyle:NSNumberFormatterDecimalStyle];
                          
                          //NSLog(@"%@",[formatter2 stringFromNumber:number2]);
                          
                          lbl_tran_amount_cr.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter2 stringFromNumber:number2]];
                          
                          
                          
                          lbl_type_cr.text=@"Last Credit";        //[NSString stringWithFormat:@"%.2f",CR_STATUS];
                          
                         // gblclass.peekabo_kfc_userid = @"";
                       //   NSLog(@"%@",gblclass.peekabo_kfc_userid);
                          
                          //saki 22 july 2019
//                          if (gblclass.peekabo_kfc_userid.length == 0)
//                          {
//                              [self Get_MobUser_Login_CreateDate:@""];
//                          }
  
                      }
                    }
                  }
                
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
    }
}


-(void) Get_MobUser_Login_CreateDate:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"1"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"ChannelId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetMobUserLoginCreateDate" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      NSString* msg = [dic2 objectForKey:@"strEncryptUserId"];
                      [[NSUserDefaults standardUserDefaults] setObject:msg forKey:@"peekabo_kfc"];
                      [[NSUserDefaults standardUserDefaults] synchronize];
                  }
 
                  [self segue1];
                  
                  //                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  //                  [self presentViewController:vc animated:YES completion:nil];
                  
                 // [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                 // [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
       // [self custom_alert:@"Please try again later." :@"0"];
        
    }
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //     // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  // NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}



-(IBAction)btn_debit:(id)sender
{
    
    [gblclass.arr_graph_name removeAllObjects];
    
    [gblclass.arr_graph_name addObject:@"0"];
    
    
    
    //NSLog(@"%@",gblclass.arr_graph_name);
    
    gblclass.frm_acct_default_chck=@"1";
    gblclass.chk_acct_statement=@"0";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}


-(IBAction)btn_credit:(id)sender
{
    
    [gblclass.arr_graph_name removeAllObjects];
    
    [gblclass.arr_graph_name addObject:@"0"];
    
    //NSLog(@"%@",gblclass.arr_graph_name);
    
    gblclass.frm_acct_default_chck=@"1";
    gblclass.chk_acct_statement=@"0";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                       
                       
                   });
    
}



-(void) AccountSummary2:(NSString *)strIndustry{
    
    @try {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        [hud hideAnimated:YES afterDelay:130];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //      // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        
        NSString* pww,*usser;
        
        if ([gblclass.From_Exis_login isEqualToString:@"1"])
        {
            usser = [NSString stringWithFormat:@"%@",gblclass.signup_userid];
            pww = gblclass.signup_pw;
        }
        else
        {
            usser = gblclass.user_id;
            pww = gblclass.sign_in_pw;
        }
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:usser],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AccountSummaryV2" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //NSError *error;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  gblclass.actsummaryarr = [[NSMutableArray alloc] init];
                  
                  //Condition -78
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
//                     NSLog(@"%@",gblclass.actsummaryarr);
//                     NSLog(@"%@",gblclass.arr_act_statmnt_name);
                     
                      gblclass.actsummaryarr =[[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbRefreshAcct"];
                      
                      //NSLog(@"Done");
                     // NSLog(@"%@",gblclass.actsummaryarr);
                      [self parsearray];
                  }
                  else
                  {
                      
                  }
                  
                  //    [self parse_header];
                  //     [self Parse_summary];
                  
                  //**               [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
//                  gblclass.custom_alert_msg=@"Retry";
//                  gblclass.custom_alert_img=@"0";
//
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
//        gblclass.custom_alert_msg=@"Try again later.";
//        gblclass.custom_alert_img=@"0";
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
    }
    
    
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}





-(void) mob_Sign_In:(NSString *)strIndustry
{
    
   [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    ///   manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    // manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    gblclass.arr_transfer_within_acct_to=[[NSMutableArray alloc] init];
    
    NSDictionary *dictparam;
    
//    gblclass.sign_in_username=txt_login.text;
//    gblclass.sign_in_pw=txt_pw1.text;
    
 //   NSLog(@"%@",gblclass.Udid);
    
    
    //touch_id
    if ([tch_chk isEqualToString:@"1"])
    {
        
        //NSLog(@"%lu",(unsigned long)user.length);
        
        //        txt_login.text = txt_tch_login.text;
        //        txt_pw1.text = txt_tch_pw.text;
        
        
        //27 march 2017
        //        if ([user isEqualToString:@""] || user.length==0)
        //        {
        //            [hud hideAnimated:YES];
        //
        //            [self custom_alert:@"Touch Id not working properly, Please try again later." :@"0"];
        //
        ////            alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Touch Id not working properly, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        ////            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        ////            [alert addAction:ok];
        ////            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        
        
        
        if ([pass isEqualToString:@""] || pass.length==0)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Touch Id not working properly, Please try again later." :@"0"];
            return;
        }
        
        
        //public void mobSignInRooted(string LoginName, string strPassword, string strDeviceID, string strSessionId, string IP, string hCode, string isTouchLogin, Int16 isRooted, string isTnCAccepted)
        
        
        //        t_n_c = @"0";
        //        gblclass.device_chck = @"0";
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        
//        if ([btn_next_press isEqualToString:@"1"])
//        {
//            dictparam = [NSDictionary dictionaryWithObjects:
//                         [NSArray arrayWithObjects:[encrypt encrypt_Data:txt_login.text],
//                          [encrypt encrypt_Data:txt_pw1.text],
//                          [encrypt encrypt_Data:gblclass.Udid],
//                          [encrypt encrypt_Data:@"1234"],
//                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                          [encrypt encrypt_Data:gblclass.setting_ver],
//                          [encrypt encrypt_Data:@"0"],
//                          [encrypt encrypt_Data:gblclass.device_chck],
//                          [encrypt encrypt_Data:t_n_c], nil]
//                                                    forKeys:[NSArray arrayWithObjects:@"LoginName",
//                                                             @"strPassword",
//                                                             @"strDeviceID",
//                                                             @"strSessionId",
//                                                             @"IP",
//                                                             @"hCode",
//                                                             @"isTouchLogin",
//                                                             @"isRooted",
//                                                             @"isTnCAccepted", nil]];
//        }
//        else
//        {
            //frm finger :: user
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:@""],
                          [encrypt encrypt_Data:pass],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:gblclass.setting_ver],
                          [encrypt encrypt_Data:@"1"],
                          [encrypt encrypt_Data:gblclass.device_chck],
                          [encrypt encrypt_Data:t_n_c], nil]
                                                    forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                             @"strPassword",
                                                             @"strDeviceID",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"hCode",
                                                             @"isTouchLogin",
                                                             @"isRooted",
                                                             @"isTnCAccepted", nil]];
     //   }
        
    }
    else
    {
//        gblclass.sign_in_username=txt_login.text;
        //    gblclass.sign_in_pw=txt_pw1.text;
//        NSLog(@"%@",gblclass.user_login_name);
//        NSLog(@"%@",gblclass.sign_in_username);
//        NSLog(@"%@",gblclass.sign_in_pw);
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSString*usser, *pww;
        
        if ([gblclass.From_Exis_login isEqualToString:@"1"])
        {
            
            //NSLog(@"%@",gblclass.signup_login_name);
            usser = gblclass.signup_login_name; //signup_username
            pww = gblclass.signup_pw;
        }
        else
        {
            usser = gblclass.sign_in_username;
            pww = gblclass.sign_in_pw;
        }
        
        
//        NSLog(@"saki User :%@",usser);
//        NSLog(@"saki Pw :%@",pww);
        
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:usser],
                      [encrypt encrypt_Data:pww],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:gblclass.M3sessionid],
                      [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                      [encrypt encrypt_Data:gblclass.setting_ver],
                      [encrypt encrypt_Data:@"0"],
                      [encrypt encrypt_Data:gblclass.device_chck],
                      [encrypt encrypt_Data:t_n_c], nil]
                                                forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                         @"strPassword",
                                                         @"strDeviceID",
                                                         @"strSessionId",
                                                         @"IP",
                                                         @"hCode",
                                                         @"isTouchLogin",
                                                         @"isRooted",
                                                         @"isTnCAccepted", nil]];
    }
    
    // NSLog(@"%@",dictparam);
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"mobSignInRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         @try {
             
             dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             NSDictionary *dic1 = dic;
             
             NSMutableArray*  a = [[NSMutableArray alloc] init];
             NSMutableArray*  aa = [[NSMutableArray alloc] init];
             //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
             
             gblclass.sign_Up_chck = @"no";
             gblclass.From_Exis_login = @"0"; // signup check
             
             //NSLog(@"%@",gblclass.actsummaryarr);
             if ([[dic objectForKey:@"Response"] integerValue]==-2)
             {
                 [hud hideAnimated:YES];
  //saki 23 aug 2019 [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
             {
                 [hud hideAnimated:YES];
                 
                 alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                 [alert addAction:ok];
                 
                 //777   [self presentViewController:alert animated:YES completion:nil];
                 
//                 storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                 //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
//
//                 vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_pw"];
                 //777                   [self presentViewController:vc animated:NO completion:nil];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
             {
                 
                 [hud hideAnimated:YES];
              //   [self custom_alert:@"forgot pass" :@"0"];
                 
                 //                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 //                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup"];
                 //                 [self presentViewController:vc animated:NO completion:nil];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
             {
                 [hud hideAnimated:YES];
                // txt_pw1.text = @"";
             //saki 23 aug 2019    [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-50)
             {
                 [hud hideAnimated:YES];
                 [self showAlert:@"Your login password has been changed you have to signup again." :@"Attention"];
                 
                 
                 gblclass.token=@"";
                 Pw_status_chck=@"";
                 gblclass.T_Pin=@"";
                 gblclass.welcome_msg=@"";
                 gblclass.user_login_name=@"";
                 gblclass.daily_limit=@"";
                 gblclass.monthly_limit=@"";
                 gblclass.user_id=@"";
                 gblclass.is_default_acct_id=@"";
                 gblclass.is_default_acct_id_name=@"";
                 gblclass.is_default_acct_no=@"";
                 gblclass.is_default_m3_balc=@"";
                 gblclass.registd_acct_id_trans=@"";
                 
                 [gblclass.arr_transfer_within_acct removeAllObjects];
                 [gblclass.arr_transfer_within_acct_to removeAllObjects];
                 [gblclass.arr_transaction_history removeAllObjects];
                 
                 NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
                 [removeUD removeObjectForKey:@"Touch_ID"];
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 NSUserDefaults * first_1 = [NSUserDefaults standardUserDefaults];
                 [first_1 removeObjectForKey:@"enable_touch"]; //first_time
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 NSUserDefaults * first_2 = [NSUserDefaults standardUserDefaults];
                 [first_2 removeObjectForKey:@"second_time"];
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                 [self presentViewController:vc animated:YES completion:nil];
                 
                 return ;
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==101)
             {
                 [a removeAllObjects];
                 
                 [self.avplayer pause];
                 
                 gblclass.token = [dic objectForKey:@"Token"];
                 Pw_status_chck = [dic objectForKey:@"Response"];
                 gblclass.T_Pin = [dic objectForKey:@"pinMessage"];
                 gblclass.welcome_msg = [dic objectForKey:@"strWelcomeMesage"];
                 gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                 gblclass.daily_limit = [dic objectForKey:@"strDailyLimit"];
                 gblclass.monthly_limit = [dic objectForKey:@"strMonthlyLimit"];
                 //saki  gblclass.user_id=[dic objectForKey:@"strUserId"];
                 gblclass.is_qr_payment = [dic objectForKey:@"is_QRPayment"];
                 gblclass.is_isntant_allow = [dic objectForKey:@"isInstantAllow"];
                 
                 
                 //NSLog(@"%@",[[dic objectForKey:@"strMonthlyLimit"] objectForKey:@"objdtUserInfo"]);
                 
               //  NSLog(@"%@",gblclass.daily_limit);
                // NSLog(@"%@",gblclass.monthly_limit);
                 
                 //NSLog(@"%@",[dic objectForKey:@"userId"]);
                 // gblclass.user_id=[dic objectForKey:@"userId"];
                 
                 gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                 
                 //gblclass.actsummaryarr =
                 NSArray* array= [[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                 gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                 gblclass.relation_id=[[array objectAtIndex:0]objectForKey:@"RelationshipID"];
                 gblclass.franchise_btn_chck=[[array objectAtIndex:0]objectForKey:@"isFA"];
                 
              //   NSLog(@"%@",gblclass.franchise_btn_chck);
                 
                 gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                 //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                 gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                 
                 //                 NSString *mobileno_ =[[array objectAtIndex:0]objectForKey:@"userId"];
                 
                 NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 if(mobileno==(NSString *)[NSNull null])
                 {
                     gblclass.Mobile_No =@"";
                 }
                 else
                 {
                     gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 }
                 
                 gblclass.arr_user_pw_table2 = [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                 gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                 
                 NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                 
                 if (Email ==(NSString *) [NSNull null])
                 {
                     Email=@"N/A";
                     [a addObject:Email];
                 }
                 else
                 {
                     // [a addObject:Email];
                     gblclass.user_email=Email;
                 }
                 
                 NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                 
                 if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                 {
                     FundsTransferEnabled=@"N/A";
                     [a addObject:FundsTransferEnabled];
                 }
                 else
                 {
                     // [a addObject:FundsTransferEnabled];
                 }
                 
                 NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                 
                 if (Mobile_No == (NSString *)[NSNull null])
                 {
                     Mobile_No=@"N/A";
                     [a addObject:Mobile_No];
                 }
                 else
                 {
                     // [a addObject:Mobile_No];
                 }
                 
                 //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                 //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                 //              [a removeAllObjects];
                 //              bbb=@"";
                 
                 
                 //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                 
                 gblclass.actsummaryarr =
                 [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                 
                 //NSLog(@"%@",gblclass.actsummaryarr);
                 
                 
                 //     NSUInteger *set;
                 for (dic in gblclass.actsummaryarr)
                 {
                     
                     NSString* acct_name=[dic objectForKey:@"account_name"];
                     
                     if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                     {
                         acct_name=@"N/A";
                         [a addObject:acct_name];
                     }
                     else
                     {
                         [a addObject:acct_name];
                     }
                     
                     
                     NSString* acct_no=[dic objectForKey:@"account_no"];
                     if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                     {
                         acct_no=@"N/A";
                         [a addObject:acct_no];
                     }
                     else
                     {
                         [a addObject:acct_no];
                     }
                     
                     
                     NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                     if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                     {
                         acct_id=@"N/A";
                         [a addObject:acct_id];
                     }
                     else
                     {
                         [a addObject:acct_id];
                     }
                     
                     NSString* is_default=[dic objectForKey:@"is_default"];
                     NSString* is_default_curr=[dic objectForKey:@"ccy"];
                     NSString* is_default_name=[dic objectForKey:@"account_name"];
                     NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                     NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                     NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                     NSString* is_account_type=[dic objectForKey:@"account_type"];
                     NSString* is_br_code=[dic objectForKey:@"br_code"];
                     
                     
                     //privileges
                     NSString* privileges=[dic objectForKey:@"privileges"];
                     //777         NSString* pri=[dic objectForKey:@"privileges"];
                     privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //(1,1) //Second bit 1 for withDraw ..
                     
                     
                     if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"1"])
                     {
                         
                         gblclass.is_default_acct_id=acct_id;
                         gblclass.is_default_acct_id_name=is_default_name;
                         gblclass.is_default_acct_no=is_default_acct_no;
                         gblclass.is_default_m3_balc=is_default_balc;
                         gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                         gblclass.is_default_currency=is_default_curr;
                         gblclass.is_default_acct_type=is_account_type;
                         gblclass.is_default_br_code=is_br_code;
                         
//                         NSLog(@"%@",acct_id);
//                         NSLog(@"%@",is_default_name);
//                         NSLog(@"%@",is_default_acct_no);
//                         NSLog(@"%@",is_default_balc);
//                         NSLog(@"%@",is_default_regstd_acct_id);
//                         NSLog(@"%@",is_default_curr);
//                         NSLog(@"%@",is_account_type);
//                         NSLog(@"%@",is_br_code);
                         
                     }
                     else if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"0"])
                     {
                         debit_lock_chck = @"yes";
                         
                         gblclass.is_debit_lock = @"1";
                         
                         gblclass.is_default_acct_id=acct_id;
                         gblclass.is_default_acct_id_name=is_default_name;
                         gblclass.is_default_acct_no=is_default_acct_no;
                         gblclass.is_default_m3_balc=is_default_balc;
                         gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                         gblclass.is_default_currency=is_default_curr;
                         gblclass.is_default_acct_type=is_account_type;
                         gblclass.is_default_br_code=is_br_code;
                         
//                         NSLog(@"%@",acct_id);
//                         NSLog(@"%@",is_default_name);
//                         NSLog(@"%@",is_default_acct_no);
//                         NSLog(@"%@",is_default_balc);
//                         NSLog(@"%@",is_default_regstd_acct_id);
//                         NSLog(@"%@",is_default_curr);
//                         NSLog(@"%@",is_account_type);
//                         NSLog(@"%@",is_br_code);
                     }
                     
                     
                     if ([privileges isEqualToString:@"0"])
                     {
                         privileges=@"0";
                         [a addObject:privileges];
                     }
                     else
                     {
                         [a addObject:privileges];
                     }
                     
                     NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                     if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                     {
                         account_type_desc=@"N/A";
                         [a addObject:account_type_desc];
                     }
                     else
                     {
                         [a addObject:account_type_desc];
                     }
                     //account_type
                     
                     NSString* account_type=[dic objectForKey:@"account_type"];
                     if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                     {
                         account_type=@"N/A";
                         [a addObject:account_type];
                     }
                     else
                     {
                         [a addObject:account_type];
                     }
                     
                     NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                     
                     
                     //((NSString *)[NSNull null] == m3_balc)
                     //[m3_balc isEqualToString:@" "] ||
                     
                     
                     //27 OCt New Sign IN Method :::
                     
                     if (m3_balc ==(NSString *) [NSNull null])
                     {
                         m3_balc=@"0";
                         [a addObject:m3_balc];
                     }
                     else
                     {
                         [a addObject:m3_balc];
                     }
                     
                     
                     NSString* branch_name=[dic objectForKey:@"branch_name"];
                     if ( branch_name == (NSString *)[NSNull null])
                     {
                         branch_name=@"N/A";
                         [a addObject:branch_name];
                     }
                     else
                     {
                         [a addObject:branch_name];
                     }
                     
                     NSString* available_balance=[dic objectForKey:@"available_balance"];
                     if ( available_balance == (NSString *)[NSNull null])
                     {
                         available_balance=@"0";
                         [a addObject:available_balance];
                     }
                     else
                     {
                         [a addObject:available_balance];
                     }
                     
                     NSString* br_code=[dic objectForKey:@"br_code"];
                     if ( br_code == (NSString *)[NSNull null])
                     {
                         br_code=@"0";
                         [a addObject:br_code];
                     }
                     else
                     {
                         [a addObject:br_code];
                     }
                     
                     NSString* ccy=[dic objectForKey:@"ccy"];
                     if ( ccy == (NSString *)[NSNull null])
                     {
                         ccy=@"0";
                         [a addObject:ccy];
                     }
                     else
                     {
                         [a addObject:ccy];
                     }
                     
                     
                     NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                     [gblclass.arr_act_statmnt_name addObject:bbb];
                     
                     if ([privileges isEqualToString:@"1"])
                     {
                         if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                         {
                             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                         }
                     }
                     
                     
                     NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
                     privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
                     
                     
                     if ([privileges_to isEqualToString:@"1"])
                     {
                         [gblclass.arr_transfer_within_acct_to addObject:bbb];
                     }
                     
                     //NSLog(@"%@", bbb);
                     [a removeAllObjects];
                     [aa removeAllObjects];
                 }
                 
                 //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                 //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                 //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                 
                 
                 if ([debit_lock_chck isEqualToString:@"yes"])
                 {
                     
                //     NSLog(@"%lu", (unsigned long)[gblclass.arr_transfer_within_acct count]);
                 //    NSLog(@"%@",gblclass.arr_transfer_within_acct);
                     
                     if ([gblclass.arr_transfer_within_acct count]>0)
                     {
                         NSArray*  split_debit = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
                         
                      //   NSLog(@"%@",[split_debit objectAtIndex:0]);
                         
                         gblclass.is_default_acct_id = [split_debit objectAtIndex:2];
                         gblclass.is_default_acct_id_name = [split_debit objectAtIndex:0];
                         gblclass.is_default_acct_no = [split_debit objectAtIndex:1];
                         gblclass.is_default_m3_balc = [split_debit objectAtIndex:6];
                         gblclass.registd_acct_id_trans = [split_debit objectAtIndex:2];
                         gblclass.is_default_currency = [split_debit objectAtIndex:10];
                         gblclass.is_default_acct_type = [split_debit objectAtIndex:5];
                         gblclass.is_default_br_code = [split_debit objectAtIndex:9];
                         
                     }
                     
                 }
  
                 //saki 23 aug 2019
//                 NSString* pekaboo = [user_default valueForKey:@"peekabo_kfc"];
//
//                 if (pekaboo.length ==0)
//                 {
//                     [self Get_MobUser_Login_CreateDate:@""];
//                 }
//                 else
//                 {
                     // [hud hideAnimated:YES];
                     [self segue1];
//                 }
                 //10 oct [hud hideAnimated:YES];
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-54)
             {
                 [hud hideAnimated:YES];
     //saki 23 aug 2019  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
             }
             else
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[dic objectForKey:@"ReturnMessage"] :@"0"];
             }
             
         }
         @catch (NSException *exception)
         {
             [hud hideAnimated:YES];
           //  NSLog(@"%@",exception.description);
           //  NSLog(@"%@",exception);
             
             [self custom_alert:Call_center_msg :@"0"];
         }
     }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              [self custom_alert:Call_center_msg :@"0"];
              
          }];
}


-(void)segue1
{
    [self getCredentialsForServer_touch:@"touch_enable"];
    
    
    if ([tch_chk isEqualToString:@"1"])
    {
        
            //    NSLog(@"%@",gblclass.actsummaryarr);
                [self parsearray];
        
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"touch_id"];  //act_summary pie_chart
//        [self presentViewController:vc animated:NO completion:nil];
//
//        return;
    }
    
    if ([Pw_status_chck isEqualToString:@"101"] || [Pw_status_chck isEqualToString:@"0"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        gblclass.isLogedIn=YES;

             //   NSLog(@"%@",gblclass.actsummaryarr);
                [self parsearray];
        
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        CATransition *transition = [ CATransition animation ];
//        transition.duration = 0.3;
//        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    
}


-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try
    {
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
        //        // Create dictionary of search parameters
        //        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        //
        //        // Look up server in the keychain
        //        NSDictionary* found = nil;
        //        CFDictionaryRef foundCF;
        //        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        //
        //        // Check if found
        //        found = (__bridge NSDictionary*)(foundCF);
        //        if (!found)
        //            return;
        //
        //        // Found
        //        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        //
        //        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        //
        //        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        //
        //        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        return ;
    }
    
}

-(void) getCredentialsForServer:(NSString*)server
{
    @try {
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //27 march 2017
        //    user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        user=@"";
        pass = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        return ;
    }
    
}


@end

