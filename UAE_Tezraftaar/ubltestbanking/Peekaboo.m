//
//  Peekaboo.m
//  Connect Sample
//
//  Created by Zain Sajjad on 20/03/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <PeekabooConnect/PeekabooConnect.h>
#import <PeekabooConnect/PeekabooConnect.h>
#import "Peekaboo.h"

@implementation Peekaboo {
    PeekabooConnect *peekabooConnect;
}

+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

//saki  1 Nov 21
- (UIViewController *) getPeekabooUIViewController:(NSDictionary *)params
{
    if (!peekabooConnect) {
        peekabooConnect = [[PeekabooConnect alloc] init];
        [peekabooConnect initializePeekaboo];
    }
    NSDictionary *defaultParams = @{
        @"environment" : @"production",
        @"pkbc": @"app.com.brd", // Your Peekaboo Connect Id here
        @"country": @"United Arab Emirates",
        @"initialRoute": @"/",
//        @"userId": @"9T", // Required for Redemption or transaction
//        @"enableRedemption": @YES, // Required for Redemption
        @"disableGoogleAnalytics": @YES, // For disabling google analytics [This is required if you're using smaller version]
    };
    NSMutableDictionary *fullParams = [[NSMutableDictionary alloc] initWithDictionary:defaultParams];
    [fullParams addEntriesFromDictionary:params];
    return [peekabooConnect getPeekabooUIViewController:fullParams];
}


@end
