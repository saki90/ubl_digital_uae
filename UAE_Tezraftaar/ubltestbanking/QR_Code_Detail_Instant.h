//
//  QR_Code_Detail_Instant.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QR_Code_Detail_Instant : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_amnt;
    IBOutlet UITextField* txt_pin;
    IBOutlet UITextField* txt_cell;
    IBOutlet UILabel* lbl_name;
    IBOutlet UITextField* txt_comment;
    IBOutlet UITableView* table_from;
    IBOutlet UIView* vw_table;
    IBOutlet UILabel* _lbl_acct_frm;
    IBOutlet UITextField* txt_acct_from;
    IBOutlet UILabel* lbl_balance;
    IBOutlet UITextField* txt_qr_pin;
    IBOutlet UITextField* txt_name;
}
@end

