//
//  Branch_Act_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Credit_card_act_VC.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"

@interface Credit_card_act_VC ()
{
    NSMutableArray* cc_name;
    
    NSMutableArray *cc_number,*account_typedesc;
   // NSMutableArray *account_currencydesc;
    
    NSMutableArray *cc_balcdate;
    NSMutableArray *cc_osbal;
    
    GlobalStaticClass *gblclass;
    GnbRefreshAcct *classObj ;
    NSString *flag;
    //int *dictionaryindex;
    //  NSNumber *number;
    NSDictionary *dic;
    UIButton *exesender;
    
}
@end

@implementation Credit_card_act_VC
@synthesize ddMenu;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    flag=@"1";
   
    gblclass =  [GlobalStaticClass getInstance];
    
    cc_name= [[NSMutableArray alloc]init];
    cc_number= [[NSMutableArray alloc]init];
   // account_currencydesc = [[NSMutableArray alloc]init];
    cc_balcdate= [[NSMutableArray alloc]init];
    cc_osbal= [[NSMutableArray alloc]init];
   
    account_typedesc= [[NSMutableArray alloc]init];

    //int value = [number intValue];
    NSUInteger *set;
    for (dic in gblclass.actsummaryarr)
    {
        set = [gblclass.actsummaryarr indexOfObject:dic];
        NSLog(@"%d",set);
        classObj = [[GnbRefreshAcct alloc] initWithDictionary:dic];
        [account_typedesc addObject:classObj.accountTypeDesc];
        
        //    for(int i = 0;i<[account_typedesc count];i++  ){
        //        NSDictionary *dic =[gblclass.actsummaryarr objectAtIndex:i];
        //        GnbRefreshAcct *classObj = [[GnbRefreshAcct alloc] initWithDictionary:dic];
        //
        // NSLog(@"%d",dictionaryindex);
     //   NSLog(@"ACount des %@",[account_typedesc objectAtIndex:set] );
        if([[account_typedesc objectAtIndex:set] isEqual:@"Credit Card Account"]){
            
            [cc_name addObject:classObj.accountName];
            [cc_number addObject:classObj.cardNumber];

            if([classObj.ccBalanceDate isEqualToString:nil] || [classObj.ccBalanceDate isEqualToString:@" "] || [classObj.ccBalanceDate isEqualToString:@"null"] || [classObj.ccBalanceDate isEqualToString:@"NULL"] || [classObj.ccBalanceDate length]==0){
                [cc_balcdate addObject:@"N/A"];
            }else{
                [cc_balcdate addObject:classObj.ccBalanceDate];
            }
            ////balance date
            if([classObj.ccBalance isEqualToString:@"0"] ||[classObj.ccBalance isEqualToString:nil] || [classObj.ccBalance isEqualToString:@" "] || [classObj.ccBalance isEqualToString:@"null"] || [classObj.ccBalance isEqualToString:@"NULL"] || [classObj.ccBalance length]==0){
                [cc_osbal addObject:@"N/A"];
                 NSLog(@"ACount des %@",classObj.ccBalance );
            }else{
                 NSLog(@"ACount des %@",classObj.ccBalance );
                [cc_osbal addObject:classObj.ccBalance];//  [branch addObject:classObj.branchName];
            }
            
            ///avail balance
//            if([classObj.balance isEqualToString:nil] || [classObj.balance isEqualToString:@" "] || [classObj.balance isEqualToString:@"null"] || [classObj.balance isEqualToString:@"NULL"] || [classObj.balance length]==0){
//                [avail_bal addObject:@"N/A"];
//            }else{
//                [avail_bal addObject:classObj.balance];
//            }
            //  number = [NSNumber numberWithInt:dictionaryindex];
            // int value = [number intValue];
            //number = [NSNumber numberWithInt:value + 1];
            //           dictionaryindex = [number intValue];
            //            number = [NSNumber numberWithInt:dictionaryindex];
            //            number = [NSNumber numberWithInt:value + 1];
            
            //  dictionaryindex = [number intValue];
            
        }
        else{
        }
    }
    ddMenu.hidden=YES;
    
    //    table1.delegate=self;
    //    table2.delegate=self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)ddMenuShow:(UIButton *)sender
{
    if ([flag isEqualToString:@"1"]) {
        flag=@"0";
        // sender.tag = 1;
        self.ddMenu.hidden = NO;
        [sender setTitle:@"Account ▲" forState:UIControlStateNormal];
    } else {
        sender.tag = 0;
        flag=@"1";
        exesender =sender;
        self.ddMenu.hidden = YES;
        [sender setTitle:@"Account ▼" forState:UIControlStateNormal];
    }
}

- (IBAction)ddMenuSelectionMade:(UIButton *)sender
{
    self.ddText.text = sender.titleLabel.text;
    [self.ddMenuShowButton setTitle:@"Account ▼" forState:UIControlStateNormal];
    self.ddMenuShowButton.tag = 0;
    self.ddMenu.hidden = YES;
    switch (sender.tag) {
        case 1:
            self.view.backgroundColor = [UIColor redColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor blueColor];
            break;
        case 3:
            self.view.backgroundColor = [UIColor greenColor];
            break;
            
        default:
            break;
    }
}



//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if (tableView==table1)
//    {
//        return 1;
//    }
//    else
//    {
//        return 2;
//    }
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    NSMutableArray* cc_name;
//    
//    NSMutableArray *cc_number,*account_typedesc;
//    // NSMutableArray *account_currencydesc;
//    
//    NSMutableArray *cc_balcdate;
//    NSMutableArray *cc_osbal;
    
    GlobalStaticClass *gblclass;
    GnbRefreshAcct *classObj ;

    
    
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    self.ddMenu.hidden=true;
    [exesender setTitle:@"Account ▼" forState:UIControlStateNormal];
    flag=@"1";
    // cell.textLabel.text=[account_name objectAtIndex:indexPath.row];
    self.cc_name.text =[cc_name objectAtIndex:indexPath.row];
    self.cc_num.text=[cc_number objectAtIndex:indexPath.row];
  //  self.act_branch.text=[branch objectAtIndex:indexPath.row];
    self.cc_balcdate.text=[cc_balcdate objectAtIndex:indexPath.row];
    self.cc_outstandingbal.text=[cc_osbal objectAtIndex:indexPath.row];
//    self.upper_amount.text=[avail_bal objectAtIndex:indexPath.row];
//    self.upper_currency.text=[account_currencydesc objectAtIndex:indexPath.row];
    
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (tableView==table1)
    //    {
    //  NSLog(@"%lu",(unsigned long)[account_name count]);
    
    return [cc_name count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (tableView==table)
    {
        
        cell.textLabel.text=[cc_name objectAtIndex:indexPath.row];
        self.cc_name.text =[cc_name objectAtIndex:0];
        self.cc_num.text=[cc_number objectAtIndex:0];
        //  self.act_branch.text=[branch objectAtIndex:indexPath.row];
        self.cc_balcdate.text=[cc_balcdate objectAtIndex:0];
        self.cc_outstandingbal.text=[cc_osbal objectAtIndex:0];
        
        
    }
    else
    {
        //cell for second table
    }
    return cell;
}

@end
