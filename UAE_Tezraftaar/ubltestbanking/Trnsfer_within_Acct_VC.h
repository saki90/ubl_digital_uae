//
//  Trnsfer_within_Acct_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 16/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Trnsfer_within_Acct_VC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_bill;
    IBOutlet UILabel* lbl_vw_header;
    
    
    IBOutlet UIButton* btn_combo_to_chk;
    IBOutlet UIButton* btn_submit;
    IBOutlet UITextField* txt_Utility_bill_name;
    
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    IBOutlet UIView* vw_bill;
    IBOutlet UIView* vw_table;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@property(nonatomic,strong) IBOutlet UITextField* txt_acct_from;
@property(nonatomic,strong) IBOutlet UILabel* lbl_acct_frm;
@property(nonatomic,strong) IBOutlet UITextField* txt_acct_to;
@property(nonatomic,strong) IBOutlet UITextField* txt_t_pin;
@property(nonatomic,strong) IBOutlet UITextField* txt_amnt;
@property(nonatomic,strong) IBOutlet UITextField* txt_comment;
@property(nonatomic,strong) IBOutlet UILabel* lbl_t_pin;
@property(nonatomic,strong) IBOutlet UILabel* lbl_balance;


-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_combo_to:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_addpayee:(id)sender;
-(IBAction)btn_vw_hide:(id)sender;
-(IBAction)btn_review:(id)sender;


@end

