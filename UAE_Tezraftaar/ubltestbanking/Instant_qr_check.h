//
//  Instant_qr_check.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 31/03/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Instant_qr_check : UIViewController
{
    IBOutlet UIButton* btn_receive;
    IBOutlet UIButton* btn_send;
    //  UIImageView *imgView;
    IBOutlet UIImageView* img_logo;
    IBOutlet UIView* vw_black;
    IBOutlet UILabel* lbl_name;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_social;
    IBOutlet UIView* vw_normal;
}

//@property (nonatomic,retain) UIImageView *imgView;

@end

