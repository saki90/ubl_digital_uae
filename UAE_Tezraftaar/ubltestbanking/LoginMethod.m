//
//  LoginMethod.m
//
//  Created by   on 22/01/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LoginMethod.h"


NSString *const kLoginMethodReturnMessage = @"ReturnMessage";
NSString *const kLoginMethodUserId = @"UserId";
NSString *const kLoginMethodResponse = @"Response";
NSString *const kLoginMethodPinPattern = @"PinPattern";
NSString *const kLoginMethodStatus = @"Status";
NSString *const kLMethod = @"Signup";


@interface LoginMethod ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LoginMethod

@synthesize returnMessage = _returnMessage;
@synthesize userId = _userId;
@synthesize response = _response;
@synthesize pinPattern = _pinPattern;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.returnMessage = [self objectOrNilForKey:kLoginMethodReturnMessage fromDictionary:dict];
            self.userId = [[self objectOrNilForKey:kLoginMethodUserId fromDictionary:dict] doubleValue];
            self.response = [self objectOrNilForKey:kLoginMethodResponse fromDictionary:dict];
            self.pinPattern = [self objectOrNilForKey:kLoginMethodPinPattern fromDictionary:dict];
            self.status = [self objectOrNilForKey:kLoginMethodStatus fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.returnMessage forKey:kLoginMethodReturnMessage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kLoginMethodUserId];
    [mutableDict setValue:self.response forKey:kLoginMethodResponse];
    [mutableDict setValue:self.pinPattern forKey:kLoginMethodPinPattern];
    [mutableDict setValue:self.status forKey:kLoginMethodStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.returnMessage = [aDecoder decodeObjectForKey:kLoginMethodReturnMessage];
    self.userId = [aDecoder decodeDoubleForKey:kLoginMethodUserId];
    self.response = [aDecoder decodeObjectForKey:kLoginMethodResponse];
    self.pinPattern = [aDecoder decodeObjectForKey:kLoginMethodPinPattern];
    self.status = [aDecoder decodeObjectForKey:kLoginMethodStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_returnMessage forKey:kLoginMethodReturnMessage];
    [aCoder encodeDouble:_userId forKey:kLoginMethodUserId];
    [aCoder encodeObject:_response forKey:kLoginMethodResponse];
    [aCoder encodeObject:_pinPattern forKey:kLoginMethodPinPattern];
    [aCoder encodeObject:_status forKey:kLoginMethodStatus];
}

- (id)copyWithZone:(NSZone *)zone
{
    LoginMethod *copy = [[LoginMethod alloc] init];
    
    if (copy) {

      //  copy.returnMessage = [self.returnMessage copyWithZone:zone];
        copy.userId = self.userId;
        copy.response = [self.response copyWithZone:zone];
        copy.pinPattern = [self.pinPattern copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
    }
    
    return copy;
}


@end
