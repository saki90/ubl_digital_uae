//
//  Payee_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 25/05/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Payee_VC.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"

@interface Payee_VC ()
{
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSArray* split;
    UIAlertController  *alert;
}
@end

@implementation Payee_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    scrl.contentSize=CGSizeMake(0,350);
    vw_qr.hidden=YES;
    
    @try {
        
        gblclass.chk_payee_management_select=@"0";
        lbl_name.text=gblclass.is_default_acct_id_name;
     // Payfrm_selected_item_value=gblclass.is_default_acct_no;
        
        
//        NSLog(@"%@",gblclass.arr_transfer_within_acct);
        // split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        //Franchise check ::
        
//        NSLog(@"%@",gblclass.franchise_btn_chck);
        
        if ([gblclass.franchise_btn_chck isEqualToString:@"0"])
        {
            vw_franchise_chck.hidden=YES;
            scrl.contentSize=CGSizeMake(0,350);
        }
        else if ([gblclass.franchise_btn_chck isEqualToString:@"1"])
        {
            vw_franchise_chck.hidden=NO;
        }
        else
        {
            vw_franchise_chck.hidden=YES;
            scrl.contentSize=CGSizeMake(0,350);
        }
        
        NSTextAttachment * attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:@"Check_acct.png"];
        attach.bounds = CGRectMake(10, 0, 15, 15);
        NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
        
        NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:gblclass.is_default_acct_no];
        NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
        [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
        
        [mutableAttriStr appendAttributedString:imageStr];
        _lbl_acct_frm.attributedText = mutableAttriStr;
        
        //  _lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        
        //    if ([split objectAtIndex:6]==0)
        //    {
        //        balc=@"0";
        //    }
        //    else
        //    {
        //        balc=[split objectAtIndex:6];
        //    }
        
        
        NSNumber *number = [NSNumber numberWithFloat:23034];
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
      //  [numberFormatter setFormatterBehavior:NSNumberFormatterBehaviorDefault];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSLog(@"%@", [numberFormatter stringFromNumber:number]);
        
        NSString *string = gblclass.is_default_m3_balc;
      if ([string rangeOfString:@"."].location == NSNotFound)
      {
//              NSLog(@"string does not contain Dot");
              NSNumber *myDoubleNumber = [NSNumber numberWithFloat:[gblclass.is_default_m3_balc doubleValue]];
          
                 //[myDoubleNumber stringValue];
//                 NSLog(@"%@",[myDoubleNumber stringValue]);
                 
                 NSString *mystring =[NSString stringWithFormat:@"%@",gblclass.is_default_m3_balc];   //
                 NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
                 NSNumberFormatter *formatter = [NSNumberFormatter new];
                 [formatter setMinimumFractionDigits:2];
                 [formatter setMaximumFractionDigits:2];
                 [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                  
              //   NSLog(@"%@",[formatter stringFromNumber:number]);
                  
                 NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
                 NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
                 [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
                 
                 [mutableAttriStr1 appendAttributedString:imageStr1];
                 // _lbl_balance.attributedText = mutableAttriStr1;
                 
                 _lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
          
      }
      else
      {
          
             NSString *mystring =[NSString stringWithFormat:@"%@",gblclass.is_default_m3_balc];
             NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
             NSNumberFormatter *formatter = [NSNumberFormatter new];
             [formatter setMinimumFractionDigits:2];
             [formatter setMaximumFractionDigits:2];
             [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
          
          _lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
          
//        NSLog(@"string contains Dot!");
      // _lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,gblclass.is_default_m3_balc];
        
//        NSNumber *myDoubleNumber = [NSNumber numberWithFloat:[gblclass.is_default_m3_balc doubleValue]];
//
//        //[myDoubleNumber stringValue];
//        NSLog(@"%@",[myDoubleNumber stringValue]);
//
//        NSString *mystring =[NSString stringWithFormat:@"%@",gblclass.is_default_m3_balc];   //
//        NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
//        NSNumberFormatter *formatter = [NSNumberFormatter new];
//        [formatter setMinimumFractionDigits:2];
//        [formatter setMaximumFractionDigits:2];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//
//     //   NSLog(@"%@",[formatter stringFromNumber:number]);
//
//        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
//        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
//        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
//
//        [mutableAttriStr1 appendAttributedString:imageStr1];
//        // _lbl_balance.attributedText = mutableAttriStr1;
//
//        _lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
        
      }
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissAllViewControllers:) name:@"YourDismissAllViewControllersIdentifier" object:nil];
        
        
//        NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
        //gblclass.arr_transfer_within_acct
        if ([gblclass.arr_transfer_within_acct_deposit_chck count]>1)
        {
            btn_pay_within_acct_enable.enabled = YES;
        }
        else
        {
            btn_pay_within_acct_enable.enabled = NO;
        }
        
        //self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        
        [scrl setBackgroundColor:[UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0]];
        
    }
    @catch (NSException *exception)
    {
        [self custom_alert:@"Try again later." :@"0"];
    }
}


- (void)dismissAllViewControllers:(NSNotification *)notification
{
    // dismiss all view controllers in the navigation stack
    
    [self dismissViewControllerAnimated:NO completion:^{}];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"viewDidAppear call");
    
    vw_qr.hidden=YES;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    vw_qr.frame = CGRectMake(0, 650, 320, 100);
    
    [UIView commitAnimations];
}


-(IBAction)btn_pay_other:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        gblclass.chck_transfer_other_acct_frm=@"0";
        
        [gblclass.arr_payee_list removeAllObjects];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_other_acct"]; //  pay_other_acct payee_list
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
    }
}

-(IBAction)btn_back:(id)sender
{
    
    gblclass.landing = @"1";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
    // [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)btn_pay_withmy_acct:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        gblclass.chck_qr_send=@"0";
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_within_acct"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}

- (IBAction)btn_fbrPayment:(id)sender
{
    [self slideRight];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"SelectBusinessPaymentTypeVC"]; //SelectBusinessPaymentTypeVC
    [self presentViewController:vc animated:NO completion:nil];
   
}

- (IBAction)btn_tezraftr:(id)sender
{
   
    gblclass.chk_tezraftar = @"0";
    [self checkinternet];
    if (netAvailable) {
        [self slideRight];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"trnsfer_tezraftaar_vc"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}

-(IBAction)btn_prepaid_service:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
}

-(IBAction)btn_shopping:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"online_shopping"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
}

 

- (void) slideRight {
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_Pay:(id)sender
{
//    [self checkinternet];
//    if (netAvailable) {
    
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
        [self presentViewController:vc animated:NO completion:nil];
        [self slideRight];
//    }
}

//- (void) slideright {
//    CATransition *transition = [ CATransition animation ];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [ self.view.window. layer addAnimation:transition forKey:nil];
//}

-(IBAction)btn_account:(id)sender {

    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:NO completion:nil];
    [self slideRight];
}

-(IBAction)btn_bill:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
        
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}

-(IBAction)btn_QR_code:(id)sender
{
    
    if ([gblclass.is_qr_payment isEqualToString:@"0"])
    {
        
        [self showAlert:@"QR PIN is not generated, Kindly generate your QR PIN." :@"Attention"];
        
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"qrcode_pin"];
        //
        //        [self presentViewController:vc animated:NO completion:nil];
    }
    else if ([gblclass.is_qr_payment isEqualToString:@"2"])
    {
        
        [self custom_alert:@"Your generated PIN is not active" :@"0"];
        
        
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"qrcode_pin"];
        //
        //        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        
        
//        NSLog(@"%@",gblclass.is_qr_payment);
        
        vw_qr.hidden=NO;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        vw_qr.frame = CGRectMake(0, 426, 320, 100);
        
        [UIView commitAnimations];
        
        
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"QR_Code"];
        //
        //        [self presentViewController:vc animated:NO completion:nil];
        //
        //        CATransition *transition = [ CATransition animation ];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
    
}

-(IBAction)btn_vw_qr_cancel:(id)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    vw_qr.frame = CGRectMake(0, 650, 320, 100);
    
    [UIView commitAnimations];
    
}

-(IBAction)btn_scan_pay:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"QR_Code"];
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_school_fee:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"school_list"];
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            //       [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_send_qr:(id)sender
{
    
    gblclass.chck_qr_send=@"0";
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr"]; //send_money   send_qr
    
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}



-(IBAction)btn_franchise_payment:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"franchise_data"];
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_receive_qr:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"receive_qr_list"]; //receive_qr
    
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_Mobile_topup:(id)sender
{
    gblclass.bill_click = @"Mobile TopUp";
    gblclass.bill_type=[NSString stringWithFormat:@"Add Mobile TopUp"];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaidservices"]; //receive_qr
    
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"qrcode_pin"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}

@end

