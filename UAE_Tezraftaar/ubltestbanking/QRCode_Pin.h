//
//  QRCode_Pin.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 02/03/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "TransitionDelegate.h"

@interface QRCode_Pin : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_qr_pin;
    IBOutlet UITextField* txt_Re_qr_pin;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

