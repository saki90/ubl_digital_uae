//
//  QR_Code_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 21/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"


@interface QR_Code_VC : UIViewController

{
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_purpose;
    IBOutlet UIButton* btn_submit;
    IBOutlet UIView* purpose_with_view;
    IBOutlet UIView* purpose_without_view;
    IBOutlet UIButton* btn_check_mail;
    
    IBOutlet UITextField* txt_email_with;
    IBOutlet UITextField* txt_email_without;
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@property(nonatomic,strong) IBOutlet UITextField* txt_acct_from;

@end

