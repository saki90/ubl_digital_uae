//
//  Slide_menu_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 10/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Slide_menu_VC.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Landing_VC.h"
#import "MBProgressHUD.h"
#import "Settings.h"
#import "Encrypt.h"

@interface Slide_menu_VC () <SettingsDelegate>
{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIStoryboard *storyboard;
    GlobalStaticClass* gblclass;
    NSMutableArray* arr_load_data;
    UIImageView* img;
    UILabel* label;
    APIdleManager* timer_class;
    NSUserDefaults *user_default;
    NSString* chck_datt;
    NSArray *paths;
    UIImage* image;
    NSString* touch_id;
    Landing_VC* lan_prof_img;
    MBProgressHUD *hud;
    NSString* chk_ssl;
    NSString* tch_chk;
    BOOL isNavigate;
    Encrypt *encrypt;
    NSString* ssl_count;
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong,nonatomic) UIViewController *modal;
@end

@implementation Slide_menu_VC
@synthesize table;

static Slide_menu_VC *instance = nil;

+(Slide_menu_VC *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [Slide_menu_VC new];
            
        }
    }
    return instance;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @try {
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        encrypt = [[Encrypt alloc] init];
        
//        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
       
        
        UISwipeGestureRecognizer *swipeRightOrange = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
        swipeRightOrange.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swipeRightOrange];
        
        bg_view.backgroundColor=[UIColor colorWithRed:33/255.0 green:31/255.0 blue:34/255.0 alpha:1.0];
        
        vw_logout.hidden = YES;
        ssl_count = @"0";
        gblclass=[GlobalStaticClass getInstance];
        lbl_pin_message.text= gblclass.T_Pin;
        lbl_daily_limits.text= gblclass.daily_limit;
        lbl_monthly_limits.text= gblclass.monthly_limit;
        
        user_default = [NSUserDefaults standardUserDefaults];
        lan_prof_img = [[Landing_VC alloc] init];
        
        btn_logout_ok.layer.cornerRadius = 10; // this value vary as per your desire
        btn_logout_ok.layer.borderColor = [UIColor whiteColor].CGColor;
        btn_logout_ok.clipsToBounds = YES;
        
        btn_logout_close.layer.cornerRadius = 10; // this value vary as per your desire
        btn_logout_close.layer.borderColor = [UIColor whiteColor].CGColor;
        btn_logout_close.clipsToBounds = YES;
        
        gblclass.setting_chck_Tch_ID = @"1";
        
        [table reloadData];
        arr_load_data=[[NSMutableArray alloc] init];
        [self getCredentialsForServer_touch:@"touch_enable"];
        
        gblclass.chk_payee_management_select=@"0";
        touch_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"enable_touch"];
        
        //touch_id
        if ([tch_chk isEqualToString:@"11"])
        {
            // arr_load_data=[[NSMutableArray alloc] initWithArray:@[@"Account",@"Pay",@"Payee Management",@"eTransaction History",@"Feed back",@"Settings",@"Terms & Conditions",@"Touch ID"]];
            
            btn_touch_chk.hidden=NO;
            img_tch.hidden=NO;
        }
        else
        {
            // arr_load_data=[[NSMutableArray alloc] initWithArray:@[@"Account",@"Pay",@"Payee Management",@"eTransaction History",@"Feed back",@"Settings",@"Terms & Conditions"]];
            
            btn_touch_chk.hidden=YES;
            img_tch.hidden=YES;
        }
        
        
        arr_load_data=[[NSMutableArray alloc] initWithArray:@[@"Account",@"Pay",@"Payee Management",@"eTransaction History",@"Feedback",@"Settings",@"Terms & Conditions"]];
 
        
        self.Profile_imageView.image = [self loadImage1: @"test1"];
        self.profile_view.hidden=YES;
        
        // For camera check :::
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }
        
        
        //  self.Profile_imageView.image = [self loadImage];
        
        _Profile_imageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
        _Profile_imageView.layer.cornerRadius=25;
        _Profile_imageView.layer.borderWidth=2.0;
        _Profile_imageView.layer.masksToBounds = YES;
        _Profile_imageView.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        
        // getting an NSString
        //  NSString *pro_img = [user_default stringForKey:@"profile_Img"];
        NSData *pro_img = [user_default dataForKey:@"profile_Img"];
        
        
 
        
        if (pro_img.length==0)
        {
            [self.Profile_imageView setImage:[UIImage imageNamed:@"profile_img1.png"]];
        }
        else
        {
            
            
            //        NSData *imageData4 = [user_default dataForKey:@"profile_Img"];
            UIImage *contactImage = [UIImage imageWithData:pro_img];
            
            // Update the UI elements with the saved data
            _Profile_imageView.image = contactImage;
            _Profile_imageView.contentMode = UIViewContentModeScaleAspectFill;
            
            // [self.Profile_imageView setImage:[UIImage imageNamed:image]];
        }
        
        lbl_acct_name.text=gblclass.is_default_acct_id_name;
        lbl_acct_name1.text=gblclass.is_default_acct_id_name;
        
        NSTextAttachment * attach = [[NSTextAttachment alloc] init];
        attach.image = [UIImage imageNamed:@"Check_acct.png"];
        attach.bounds = CGRectMake(0, 0, 10, 10);
        NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
        
        NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:gblclass.is_default_acct_no];
        NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
        [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
        
        [mutableAttriStr appendAttributedString:imageStr];
        lbl_acct_no.attributedText = mutableAttriStr;
        
//        [APIdleManager sharedInstance].onTimeout = ^(void){
//            //  [self.timeoutLabel  setText:@"YES"];
//            
//            
//            //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//            
//            storyboard = [UIStoryboard storyboardWithName:
//                          gblclass.story_board bundle:[NSBundle mainBundle]];
//            vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//            
//            
//            [self presentViewController:vc animated:YES completion:nil];
//            
//            
//            APIdleManager * cc1=[[APIdleManager alloc] init];
//            // cc.createTimer;
//            
//            [cc1 timme_invaletedd];
//            
//        };
        
        //
        //    NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        //    NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
        //    UIImage *image = [UIImage imageWithContentsOfFile:imageName];
        
        //    _Profile_imageView.image=[UIImage imageWithContentsOfFile:imageName];
        
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception.reason);
    }
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [table reloadData];
}

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    
    [self btn_btn:self];
    //NSLog(@"");
    
}

-(void)viewWillAppear:(BOOL)animated {
    if(isNavigate) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_close:(id)sender
{
    
    [UIView animateWithDuration:1 animations:^{
        self.modal.view.frame = CGRectMake(40, 50, 290, 568); //0, 568, 320, 284
    } completion:^(BOOL finished) {
        [self.modal.view removeFromSuperview];
        [self.modal removeFromParentViewController];
        self.modal = nil;
    }];
    
    // [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_Generate_TPIN:(id)sender
{
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"re_generate_T_Pin"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)saki_T
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"re_generate_T_Pin"];
    [self presentViewController:vc animated:YES completion:nil];
    
    // [self.view.superview.frame =];
}

-(IBAction)btn_Feedback:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feed_back"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)dismissSecondVC:(id)sender
{
    // [self dismissViewControllerAnimated:YES completion:nil];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)dismissSlideMenu
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Transaction_histry:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"transaction_history"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.4;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromLeft;
    //    [self.view.window.layer addAnimation:transition forKey:nil];
    //    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_btn:(id)sender
{
    
    gblclass.landing = @"1";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.4;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self dismissViewControllerAnimated:NO completion:nil];
//
//    [self.delegate ChangeImage];
    
}

-(void)slideLeft
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)slideRight
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_load_data count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    //   NSString* img_check;
    
    // For Image::
    
    img=(UIImageView*)[cell viewWithTag:1];
    // [img setImage:@"accounts.png"];
    
    //For Header ::
    
    label=(UILabel*)[cell viewWithTag:2];
    label.text=[arr_load_data objectAtIndex:indexPath.row];
    label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    //    if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Home"])
    //    {
    //        gblclass.storyboardidentifer=@"accounts";
    //        img.image=[UIImage imageNamed:@"home_slide.png"];
    //    }
    
    if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Account"])
    {
        img.image=[UIImage imageNamed:@"Account_setting.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Pay"])
    {
        img.image=[UIImage imageNamed:@"Pay_setting.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Payee Management"])
    {
        img.image=[UIImage imageNamed:@"add_payee_slide.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"eTransaction History"])
    {
        img.image=[UIImage imageNamed:@"eTransaction-History_setting.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"QR PIN"])
    {
        img.image=[UIImage imageNamed:@"QR-icon1.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Feedback"])
    {
        img.image=[UIImage imageNamed:@"FeedBack_setting.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Settings"])
    {
        img.image=[UIImage imageNamed:@"Settings_setting.png"];
    }
    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Terms & Conditions"])
    {
        img.image=[UIImage imageNamed:@"terms-&-condition-icon.png"];
    }
    //    else if ([[arr_load_data objectAtIndex:indexPath.row] isEqualToString:@"Touch ID"])
    //    {
    //        img.image=[UIImage imageNamed:@"touch_id.png"];
    //    }
    
    [cell.contentView addSubview:img];
    
    
    //colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor colorWithRed:0/255.0 green:124/255.0 blue:197/255.0 alpha:1.0]];
    [cell setSelectedBackgroundView:bgColorView];
    
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isNavigate=YES;
    
    if ([tch_chk isEqualToString:@"11"])
    {
        switch (indexPath.row)
        {

            case 0:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                [self presentViewController:vc animated:NO completion:nil];

                break;
            case 1:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"payee"];  //payee_list  payee
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 2:

                gblclass.chk_payee_management_select=@"1";
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"payee_list"]; //payee_list addpayee
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 3:

                self.view.hidden = YES;
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"transaction_history"];
                [self presentViewController:vc animated:NO completion:nil];
                break;


            case 4:

                self.view.hidden = YES;
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feed_back"];
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 5:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"settings"];
                [self presentViewController:vc animated:NO completion:nil];

                break;


            case 6:

                self.view.hidden = YES;
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"termsAndConditions"];
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 7:


                //              gblclass.touch_id_frm_menu=@"1";

                //              mainStoryboard=[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //              vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"touch_id"];
                //              [self presentViewController:vc animated:NO completion:nil];
                //

                //                [self.avplayer pause];
                //
                //                [hud showAnimated:YES];
                //                [self.view addSubview:hud];
                //
                //                [self checkinternet];
                //                if (netAvailable)
                //                {
                //                    chk_ssl=@"logout";
                //                    [self SSL_Call];
                //                }

                break;

            default: break;

        }

    }
    else
    {


        switch (indexPath.row)
        {

            case 0:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                [self presentViewController:vc animated:NO completion:nil];

                break;
            case 1:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"payee"];  //payee_list  payee
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 2:

                gblclass.chk_payee_management_select=@"1";
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"payee_list"]; //payee_list addpayee
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 3:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"transaction_history"];
                [self presentViewController:vc animated:NO completion:nil];

                break;


            case 4:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feed_back"];
                [self presentViewController:vc animated:NO completion:nil];

                break;

            case 5:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"settings"];
                [self presentViewController:vc animated:NO completion:nil];

                break;


            case 6:

                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"termsAndConditions"];
                [self presentViewController:vc animated:NO completion:nil];

                break;

                //        case 7:
                //
                //            [self.avplayer pause];
                //
                //            [hud showAnimated:YES];
                //            [self.view addSubview:hud];
                //
                //            [self checkinternet];
                //            if (netAvailable)
                //            {
                //                chk_ssl=@"logout";
                //                [self SSL_Call];
                //            }
                //
                //            break;

            default: break;

        }

    }
    
//    NSLog(@"%@",@"saki transaction did select click first");
    
//    NSLog(@"saki %@",gblclass.story_board);

//    CATransition *transition = [ CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//
//
//    [self performSegueWithIdentifier:@"transaction_segues" sender:self];
    
    
     self.view.hidden = YES;
    
    
//     NSLog(@"%@",@"saki transaction did select after view hide");
//
//    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    NSLog(@"%@",@"saki transaction did select after mainstoryboard");
//
//    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"transaction_history"];
//    NSLog(@"%@",@"saki transaction did select after vc");
//
//    [self presentViewController:vc animated:NO completion:nil];
//    NSLog(@"%@",@"saki transaction did select after presentview controller");
//
//    NSLog(@"%@",@"saki transaction did select after animation start");

    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
//    NSLog(@"%@",@"saki transaction did select after animation End");
    
    
//        NSLog(@"%@",@"saki transaction did select click End");
    
}



-(IBAction)btn_home:(id)sender
{
    gblclass.landing = @"1";
    
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)log_out
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Close", nil];
                       [alert1 show];
                       
                   });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
            
        }
    }
    else if(buttonIndex == 1)
    {
//        gblclass.payment_login_chck=@"1";
//
//        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self presentViewController:vc animated:YES completion:nil];
    }
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //      // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  
//                  NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
//                  
//                   [navigationArray removeAllObjects];    // This is just for remove all view controller from navigation stack.
//                //  [navigationArray removeObjectAtIndex: 2];  // You can pass your index here
//                  self.navigationController.viewControllers = navigationArray;
//                //  [navigationArray release];
                  
                  
                  
                  CATransition *transition = [CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:NO completion:nil];

                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



-(IBAction)btn_change_MPin:(id)sender
{
    //change_mpin
    mainStoryboard=[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"change_mpin"];
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_touch_id:(id)sender
{
    gblclass.touch_id_frm_menu=@"1";
    
    [self slideRight];
    mainStoryboard=[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"touch_id"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_logout_close:(id)sender
{
    vw_logout.hidden = YES;
}

-(IBAction)btn_logout_ok:(id)sender
{
    vw_logout.hidden = YES;
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"logout";
        [self SSL_Call];
    }
}

-(IBAction)btn_logout:(id)sender
{
    
    [self.avplayer pause];
    
    vw_logout.hidden = NO;
    
   // [self showAlert:@"Are you sure you want to Logout":@"Confirm Logout"];
 
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    //   NSString* str=[NSString stringWithFormat:@"%@",[info objectForKey:@"UIImagePickerControllerReferenceURL"]];
    
    //    NSURL *url = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    //    NSString *ref = url.absoluteString;
    
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    //   NSData *imageData2 = UIImagePNGRepresentation(chosenImage);
    
    //    user_default=[NSUserDefaults standardUserDefaults];
    //    [user_default setObject:str forKey:@"profile_Img"];
    
    self.Profile_imageView.image = chosenImage;
    
    
    // Create instances of NSData save
    UIImage *contactImage = _Profile_imageView.image;
    NSData *imageData4 = UIImageJPEGRepresentation(contactImage, 100);
    [user_default setObject:imageData4 forKey:@"profile_Img"];
    [user_default synchronize];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    //    user_default=[NSUserDefaults standardUserDefaults];
    //    [user_default setObject:imageData2 forKey:@"profile_Img"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if (chosenImage != nil)
    {
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                    NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:@"test1.png"];
        NSData* data = UIImagePNGRepresentation(chosenImage);
        [data writeToFile:path atomically:YES];
    }
    
    
    NSData *data=UIImagePNGRepresentation(image);
    image=[UIImage imageWithData:data];
    UIImageWriteToSavedPhotosAlbum(image, nil,nil, nil);
    
    
    
    NSString *imagePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *imageName = [imagePath stringByAppendingPathComponent:@"MainImage.jpg"];
    
    NSData *imageData = UIImageJPEGRepresentation(_Profile_imageView.image, 1.0);
    
    BOOL result = [imageData writeToFile:imageName atomically:YES];
    
    //NSLog(@"Saved to %@? %@", imageName, (result? @"YES": @"NO"));
    
    [self saveImage: image: @"test2"];
    
    
    //      [lan_prof_img myInstanceMethod1];
    //     UIImage *contactImage2 = [UIImage imageWithData:imageData];
    //     lan_prof_img.imageView_saki.image=contactImage2;
    
    
    
}

- (void)saveImage:(UIImage*)image:(NSString*)imageName
{
    
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *pathss = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory1 = [pathss objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    //NSLog(@"image saved");
    
}


//loading an image

- (UIImage*)loadImage1:(NSString*)imageName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}


- (UIImage*)loadImage
{
    if (!([paths count]>0))
    {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path1 = [documentsDirectory stringByAppendingPathComponent:@"test2.png"];
        image = [UIImage imageWithContentsOfFile:path1];
        
        NSFileHandle* myFileHandle = [NSFileHandle fileHandleForReadingAtPath:path1];
        UIImage* loadedImage = [UIImage imageWithData:[myFileHandle readDataToEndOfFile]];
        
        //self.Profile_imageView.image = loadedImage;
        
    }
    
    return image;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UIImagePickerController *picker;
    
    
    switch (popup.tag)
    {
        case 1:
        {
            switch (buttonIndex)
            {
                case 0:
                    //  Gallery ::
                    
                    picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.allowsEditing = YES;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    [self presentViewController:picker animated:YES completion:NULL];
                    self.profile_view.hidden=YES;
                    
                    break;
                    
                    
                case 1:
                    //  Camera ::
                    
                    _session = [[AVCaptureSession alloc] init];
                    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                    NSError *error = nil;
                    
                    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
                    if (_input)
                    {
                        //[_session addInput:_input];
                        
                        
                        picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.allowsEditing = YES;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        [self presentViewController:picker animated:YES completion:NULL];
                        
                        self.profile_view.hidden=YES;
                    }
                    else
                    {
                        //NSLog(@"Error: %@", error);
                        
                        //                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Allow UBL Digital App to access your Camera in your Phone Settings"];
                        //                        NSRange selectedRange = NSMakeRange(22, 4); // 4 characters, starting at index 22
                        //
                        //                        [string beginEditing];
                        //
                        //                        [string addAttribute:NSFontAttributeName
                        //                                       value:[UIFont fontWithName:@"Helvetica-Bold" size:12.0]
                        //                                       range:selectedRange];
                        //
                        //
                        //                       UIAlertController* alert  = [UIAlertController alertControllerWithTitle:(NSString *)string message:@"This will enable you to upload your Profile picture on the app" preferredStyle:UIAlertControllerStyleAlert];
                        //
                        //
                        //
                        //                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                        //                        [alert addAction:ok];
                        //                        [self presentViewController:alert animated:YES completion:nil];
                        //
                        //                         [string endEditing];
                        
                        
                        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Allow UBL Digital App to access your Camera in your Phone Settings"
                                                                        message:@"This will enable you to upload your Profile picture on the app"
                                                                       delegate:nil                                             cancelButtonTitle:nil
                                                              otherButtonTitles:@"OK", nil];
                        [alert1 show];
                        
                        return;
                    }
                    
                    break;
            }
        default:
            
            break;
        }
            
    }
    
}


- (IBAction)btn_profile:(UIButton *)sender
{
    self.profile_view.hidden=NO;
    
    
    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        //NSLog(@"%zd", [group numberOfAssets]);
        
//        NSLog(@"permission");
        
    }
      failureBlock:^(NSError *error)
     {
         if (error.code == ALAssetsLibraryAccessUserDeniedError)
         {
             //NSLog(@"user denied access, code: %zd", error.code);
             
             UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Allow UBL Digital App to access your Photos in your Phone Settings" message:@"This will enable you to upload your Profile picture on the app" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
             [alert1 show];
             
             return ;
         }
         else
         {
             //NSLog(@"Other error code: %zd", error.code);
         }
         
     }];
    
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    
    if (status != ALAuthorizationStatusAuthorized)
    {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Please give this app permission to access your photo library in your settings app!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        //            [alert show];
        
//        NSLog(@"Permission Not");
    }
    else
    {
//        NSLog(@"Permission Ok");
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Select From Gallery",
                                @"Select From Camera",
                                nil];
        popup.tag = 1;
        [popup showInView:self.view];
    }
    
    
//    ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
//    [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//        //NSLog(@"%zd", [group numberOfAssets]);
//    } failureBlock:^(NSError *error)
//     {
//         if (error.code == ALAssetsLibraryAccessUserDeniedError)
//         {
//             //NSLog(@"user denied access, code: %zd", error.code);
//         }
//         else
//         {
//             //NSLog(@"Other error code: %zd", error.code);
//         }
//     }];
//
//
//    //    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
//    //
//    //    if (status != ALAuthorizationStatusAuthorized) {
//    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Please give this app permission to access your photo library in your settings app!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
//    //        [alert show];
//    //    }
//
//
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Select From Gallery",
//                            @"Select From Camera",
//                            nil];
//    popup.tag = 1;
//    [popup showInView:self.view];
    
}

- (IBAction)dismissAction:(id)sender
{
    
    [self btn_btn:self];
}




///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
        
    }
    
    //    if ([self isSSLPinning])
    //    {
    //        [self printMessage:@"Making pinned request"];
    //    }
    //    else
    //    {
    //        [self printMessage:@"Making non-pinned request"];
    //    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (error.code == NSURLErrorTimedOut)
        // handle error as you want
        [hud hideAnimated:YES];
    [self custom_alert:error.localizedDescription :@"0"];
}


- (NSData *)skabberCert
{
    
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -
-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            //            UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Attention" message:@"Internet Access Not Available" preferredStyle:UIAlertControllerStyleAlert];
            //            UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            //                CATransition *transition = [CATransition animation];
            //                transition.duration = 0.3;
            //                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
            //                transition.type = kCATransitionPush;
            //                transition.subtype = kCATransitionFromRight;
            //                [ self.view.window. layer addAnimation:transition forKey:nil];
            //
            //                [self slideLeft];
            //                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            //                vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
            //                [self presentViewController:vc animated:NO completion:nil];
            //
            //            }];
            //            [alertController addAction:alertAction];
            //            [self presentViewController:alertController animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}




-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try {
        
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}





@end

