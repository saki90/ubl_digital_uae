//
//  Broken.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 26/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Broken : NSObject
- (float)firmwareVersion;
- (BOOL) isDeviceJailbroken;
- (BOOL) isAppVersionCracked;
- (BOOL) isAppStoreVersion;
@end
