//
//  Forgot_pw_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 07/01/2019.
//  Copyright © 2019 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <WebKit/WebKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface Forgot_pw_VC : UIViewController
{
    
    IBOutlet WKWebView *webView;
    IBOutlet UILabel* lbl_header;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

NS_ASSUME_NONNULL_END
