//
//  Utility_Bill_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 22/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Utility_Bill_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_combo_frm;
    IBOutlet UITextField* txt_combo_bill_names;
    IBOutlet UILabel* lbl_bill_mangmnt;
    IBOutlet UILabel* lbl_due_dat;
    IBOutlet UILabel* lbl_amnts;
    IBOutlet UITextField* txt_comment;
    IBOutlet UITextField* txt_amnt;
    IBOutlet UILabel* lbl_t_pin;
    IBOutlet UITextField* txt_t_pin;
    IBOutlet UIButton* btn_submit;
    IBOutlet UILabel* lbl_status;
    IBOutlet UILabel* lbl_nick;
    IBOutlet UILabel* lbl_curr_balc;
    IBOutlet UITextField* txt_Utility_bill_name;
    IBOutlet UILabel* lbl_note;
    IBOutlet UITextField* txt_Combo_box;
    IBOutlet UITextField* txt_cc_amnt;
    IBOutlet UITextField* txt_cc_comment;
    IBOutlet UIView* vw_postpaid;
    IBOutlet UIButton* btn_combo;
    IBOutlet UITableView* table_CC;
    
    
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_bill;
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    IBOutlet UIView* vw_bill;
    IBOutlet UILabel* lbl_customers_id;
    IBOutlet UILabel* lbl_company;
    IBOutlet UILabel* _lbl_acct_frm;
    IBOutlet UIView* vw_table;
    IBOutlet UIView* vw_acct;
    IBOutlet UIButton* btn_review_pay_chck;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

-(IBAction)btn_back:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_bill_names:(id)sender;
-(IBAction)btn_Utility_bill_names:(id)sender;
-(IBAction)btn_addpayee:(id)sender;

@end

