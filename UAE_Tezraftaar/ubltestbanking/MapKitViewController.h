//
//  MapKitViewController.h
//  RMS App
//
//  Created by INCISIVESOFT MM1 on 06/03/2014.
//  Copyright (c) 2014 INCISIVESOFT MM1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapKitViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
    
    CLLocationCoordinate2D originValue,destinationValue;
}

@property (weak, nonatomic) IBOutlet UILabel *labelLatitude;
@property (weak, nonatomic) IBOutlet UILabel *labelLongitude;
@property (weak, nonatomic) IBOutlet UILabel *labelAltitude;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) IBOutlet MKMapView* mapview;

@end

