//
//  Next_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/05/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Next_VC.h"
#import "Globals.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "Globals.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "Peekaboo.h"


@interface Next_VC ()<NSURLConnectionDataDelegate,CLLocationManagerDelegate>
{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
    NSString *UDID1;
    NSUserDefaults *defaults;
    CLLocationManager *myLcationManager;
    NSString* lati_pekaboo,*longi_pekaboo;
}
@end

@implementation Next_VC
static UIView* loadingView = nil;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//     NSLog(@"Next VC Started .....");
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass=[GlobalStaticClass getInstance];
    defaults = [NSUserDefaults standardUserDefaults];
    
    // login proceed btn check
    gblclass.str_processed_login_chk = @"0";
    
    responseData = [NSMutableData new];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"0" forKey:@"t&c"];
    [defaults synchronize];
    
    NSString* ud=[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    UDID1 = [ud stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [self Guid_key_chain_method_make];
    
    
    //*** 18 May  lcoation update close ....
    
    if ([CLLocationManager locationServicesEnabled])
    {
        
        //NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
            
            myLcationManager = [[CLLocationManager alloc] init];
            [myLcationManager requestAlwaysAuthorization];
            myLcationManager.distanceFilter = kCLDistanceFilterNone;
            myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            myLcationManager.delegate = self;
            [myLcationManager startUpdatingLocation];
        }
        else
        {
            myLcationManager = [[CLLocationManager alloc] init];
            myLcationManager.distanceFilter = kCLDistanceFilterNone;
            myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            myLcationManager.delegate = self;
            [myLcationManager startUpdatingLocation];
        }
    }
    
    [self Play_bg_video];
    
     

}

- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    //saki 
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
    [manager stopUpdatingLocation];
    
    gblclass.arr_current_location = [[NSMutableArray alloc] init];
    float latitude = myLcationManager.location.coordinate.latitude;
    float longitude = myLcationManager.location.coordinate.longitude;
    
    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
    
    lati_pekaboo = lati;
    longi_pekaboo = longi;
    
//    NSLog(@"lati %@",lati);
//    NSLog(@"lati %@",longi);
    
    [gblclass.arr_current_location addObject:lati];
    [gblclass.arr_current_location addObject:longi];
    
   // [self peekaboo_geo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) Guid_key_chain_method_make
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *GUID = [NSString stringWithFormat:@"%@",UDID1];
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
//        NSLog(@"Guid Already Exists");
        
        
        
        gblclass.Udid = UDID1;
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Already Exists", nil)
        //                                                        message:NSLocalizedString(@"Please update it instead.",)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
    else
    {
        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setValue:@"1"  forKey:@"udid_chk"];
        [defaults synchronize];
        
//        NSLog(@"Guid create");
        keychainItem[(__bridge id)kSecValueData] = [GUID dataUsingEncoding:NSUTF8StringEncoding]; //Our password
        
        OSStatus sts = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
//        NSLog(@"Error Code: %d", (int)sts);
    }
    
}


-(IBAction)btn_next:(id)sender
{

    [defaults setObject:@"0" forKey:@"enable_touch"];
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];  //new_exist  next_ubl
    [self presentViewController:vc animated:NO completion:nil];
    [self slide_right];
    
//    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_exist"];  //new_exist  next_ubl
//    [self presentViewController:vc animated:NO completion:nil];
//
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
    [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
    
}


-(IBAction)btn_feature:(id)sender
{
    
    [hud showAnimated:YES];
    //
    //
    [self custom_alert:@"Please register your User." :@"0"];
    
    
    
    //  [self checkinternet];
    //   if (netAvailable)
    //   {
    
    //       chk_ssl=@"qr";
    //       [self SSL_Call];
    
    //        gblclass.chk_qr_demo_login=@"1";
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [self.view.window.layer addAnimation:transition forKey:nil];
    //
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //   receive_qr_login  feature
    //         [self presentViewController:vc animated:NO completion:nil];
    //  }
    
    
    
    //    [self slide_right];
    //
    //    gblclass.chk_qr_demo_login=@"1";
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //  receive_qr_login   feature
    //    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
 
    //_lbl.text = valueEmoj;
    
     [self custom_alert:Offer_msg :@""];
    
//    [gblclass Pekaboo];

//     [self custom_alert:gblclass.Offer_msg1 :@""];
    
//    gblclass.tab_bar_login_pw_check=@"login";
      //[self peekabooSDK:@"deals"];
    
//      [self peekabooSDK:@{@"type": @"deals",}];

    
}

-(IBAction)btn_find_us:(id)sender
{

    
      gblclass.forget_pw_btn_click=@"0";
//      [self peekabooSDK:@{@"type": @"locator",}];

    
    //  [self peekabooSDK:@"locator"];
//    gblclass.forgot_pw_click = @"0";

//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//
//
////    new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)peekaboo_geo
{
    
    //    NSURL *url = [NSURL URLWithString:@"https://secure-sdk.peekaboo.guru/njxklawnxioakwmskoajsan"];
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    //
    //  //  NSData *requestData = [@"name=testname&suggestion=testing123" dataUsingEncoding:NSUTF8StringEncoding];
    //
    //   // NSDictionary* headers = [NSHTTPCookie requestHeaderFieldsWithCookies:authCookies];
    //
    //    [request setHTTPMethod:@"POST"];
    //   // [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //  //  [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    //  //  [request setHTTPBody: requestData];
    // //   [request setAllHTTPHeaderFields:headers];
    //
    //    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    
    NSURL *httpsURL = [NSURL URLWithString:@"https://secure-sdk.peekaboo.guru/njxklawnxioakwmskoajsan"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:lati_pekaboo forHTTPHeaderField:@"js6nwf"];
    [request setValue:longi_pekaboo forHTTPHeaderField:@"pan3ba"];
    [request setValue:@"7db159dc932ec461c1a6b9c1778bb2b0" forHTTPHeaderField:@"ownerkey"];
    // request.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
    [request setHTTPMethod:@"POST"];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
  //[self.connection start];
    
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [responseData setLength:0];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    //[connection release];
    NSString* responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    NSLog(@"%@",responseString);
    //  [viewController returnHtmlFromPost:responseString];
    //  [responseString release];
}

-(void)parse_geo_location
{
    
}

//- (void)peekabooSDK:(NSString *)type {
//    
//    NSLog(@"%@",gblclass.peekabo_kfc_userid);
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"United Arab Emirates",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
//}//Pakistan

@end

