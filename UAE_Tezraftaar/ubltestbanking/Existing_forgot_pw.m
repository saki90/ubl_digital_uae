//
//  Existing_forgot_pw.m
//  ubltestbanking
//
//  Created by Mehmood on 07/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Existing_forgot_pw.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"


@interface Existing_forgot_pw ()
{
    MBProgressHUD* hud;
    UIAlertController* alert;
    GlobalStaticClass *gblclass;
}
@end

@implementation Existing_forgot_pw

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    
    [self checkinternet];
    if (netAvailable)
    {
        
        NSString *urlAddress = @"http://www.ubldirect.com/Corporate/ebank.aspx";
        
        //Create a URL object.
//        NSURL *url = [NSURL URLWithString:urlAddress];
//
//        //URL Requst Object
//        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//
//        //Load the request in the UIWebView.
//        [_webView loadRequest:requestObj];
//
//        _webView.delegate = self;
        
        
        
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        WKWebView *wkWebView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:configuration];
        wkWebView.navigationDelegate = self;
        [self.view addSubview:wkWebView];
        
        
        NSURL *url=[NSURL URLWithString:@"http://www.ubldirect.com/Corporate/ebank.aspx"];
        NSURLRequest *request =[NSURLRequest requestWithURL:url];
        [wkWebView loadRequest:request];
        
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    [hud hideAnimated:YES];
//    
//    //NSLog(@"%@",error);
//}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end

