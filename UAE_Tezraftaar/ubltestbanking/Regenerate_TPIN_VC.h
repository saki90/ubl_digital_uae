////
////  Regenerate_TPIN_VC.h
////  ubltestbanking
////
////  Created by Mehmood on 22/02/2016.
////  Copyright © 2016 ammar. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "TransitionDelegate.h"
//
//@class Expand_Table_VC;
//
//@interface Regenerate_TPIN_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
//{
//    IBOutlet UIButton* btn_submit;
//    IBOutlet UITableView* table;
//}
//
//@property (strong, nonatomic) Expand_Table_VC *detailViewController;
//@property (nonatomic) BOOL clearsSelectionOnViewWillAppear NS_AVAILABLE_IOS(3_2); // defaults to YES. If YES, any selection is cleared in viewWillAppear:
//@property (nonatomic, strong, nullable) UIRefreshControl *refreshControl NS_AVAILABLE_IOS(6_0) __TVOS_PROHIBITED;
//@property (nonatomic, strong) TransitionDelegate *transitionController;
//
//
//@property (strong, nonatomic) IBOutlet UITableView *MyTb;
//-(IBAction)btn_submit:(id)sender;
//-(IBAction)btn_account:(id)sender;
//-(IBAction)btn_more:(id)sender;
//-(IBAction)btn_back:(id)sender;
//
//@end
