//
//  Payee_detail_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 23/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Payee_detail_VC : UIViewController
{
    IBOutlet UITableView* table;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


-(IBAction)btn_new_payee:(id)sender;

@end

