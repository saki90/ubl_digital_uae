//
//  Tz_Review.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tz_Review : UIViewController<UITextFieldDelegate>
{
    //**********Reachability*********
        Reachability* internetReach;
        BOOL netAvailable;
    //**********Reachability*********
    
    IBOutlet UILabel *lbl_name_heading;
    IBOutlet UILabel *lbl_cnic_heading;
    IBOutlet UILabel *lbl_name;
    IBOutlet UILabel *lbl_cnic;
    IBOutlet UILabel *lbl_contact_num;
    IBOutlet UILabel *lbl_address;
    IBOutlet UILabel *lbl_relation;
    IBOutlet UITextField *txt_nick;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

NS_ASSUME_NONNULL_END
