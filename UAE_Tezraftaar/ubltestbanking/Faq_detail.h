//
//  Faq_detail.h
//  ubltestbanking
//
//  Created by Mehmood on 06/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <WebKit/WebKit.h>

@interface Faq_detail : UIViewController
{
    
    IBOutlet WKWebView* webview1;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

//@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

