//
//  Feedback_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 22/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Feedback_VC.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
 
#import "SWRevealViewController.h"
#import "Slide_menu_VC.h"
#import "TransitionDelegate.h"
#import <UIKit/UIKit.h>
#import "APIdleManager.h"
#import "FResponse.h"
#import "Feedback_Model.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Feedback_VC ()<NSURLConnectionDataDelegate>
{
    UIAlertController *alert;
    NSDictionary *dic;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSString* chk_ssl;
    NSString* Token_feedback;
    NSString* str_Para_Session_ID;
    Encrypt *encrypt;
    NSString* ssl_count;
     NSString * vw_down_chck;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Feedback_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    // hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    ssl_count = @"0";
    vw_down_chck = @"";
    
     [[UITextView appearance] setTintColor:[UIColor blackColor]];
    txt_comment.layer.borderColor=[[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0] CGColor];
    txt_comment.layer.borderWidth=2.0;
    
    [btn_submit setBackgroundColor:[UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    
    txt_comment.delegate=self;
    txt_comment.autocorrectionType = UITextAutocorrectionTypeNo;
    
    gblclass=[GlobalStaticClass getInstance];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    txt_comment.text = @"Enter FeedBack";
    txt_comment.textColor = [UIColor lightGrayColor];
    
    
    chk_ssl = @"load";
    [self SSL_Call];
    
    
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //        storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //
    //        [self presentViewController:myController animated:YES completion:nil];
    //
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if ([txt_comment.text  isEqual:@"Enter FeedBack"]) {
        txt_comment.text = @"";
        txt_comment.textColor = [UIColor blackColor];
    }
    return YES;
}


-(void)textViewDidEndEditing:(UITextView *)textView {
    
    
    if ([txt_comment.text  isEqual:@""]) {
        txt_comment.text = @"Enter FeedBack";
        txt_comment.textColor = [UIColor lightGrayColor];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_submit:(id)sender
{
    
    //  [self custom_alert:@"Try again later." :@"0"];
    
    [txt_comment resignFirstResponder];
    
    if ([txt_comment.text isEqualToString:@"Enter FeedBack"])  {
        [self custom_alert:@"Please Enter Message"  :@"0"];
        return;
    } else if ([txt_comment.text isEqualToString:@""])  {
        [self custom_alert:@"Please Enter Message"  :@"0"];
        return;
    } else if ([txt_comment.text length] >1200) {
        [self custom_alert:@"Feedback can be maximum 1200 characters"  :@"0"];
        return;
    }
    else
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        
//        NSLog(@"%@",gblclass.arr_transfer_within_acct);
//
//        NSLog(@"%@",gblclass.is_default_br_code);
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"comment";
            [self SSL_Call];
            //[self Post_comment:@""];
        }
        
        
    }
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                       
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        // [self mob_App_Logout:@""];
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}





-(void) Do_Customer_FeedBack:(NSString *)strIndustry
{
    
    @try {
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //  [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
        
        
        
        //        NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        //
        //        NSLog(@"%@",gblclass.is_default_br_code);
        //
        //        NSString* br_codes;
        //        if (gblclass.is_default_br_code ==(NSString *) [NSNull null] || [gblclass.is_default_br_code isEqualToString:@"(null)"])
        //        {
        //            br_codes = [split objectAtIndex:9];
        //        }
        //        else
        //        {
        //            br_codes = gblclass.is_default_br_code;
        //        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.user_login_name],
                                    [encrypt encrypt_Data:gblclass.user_email],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"customerName",
                                                                       @"customerEmail", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        // UserFeedBack
        [manager POST:@"DoCustomerFeedBack" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSString* response;
                  
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  gblclass.arr_acct_statement1=[[NSMutableArray alloc] init];
                  
                  //                  NSMutableArray* arr2;
                  
                  //                  arr2=[[NSMutableArray alloc] init];
                  //                  gblclass.arr_acct_statement1=[(NSDictionary *) dic objectForKey:@"FResponse"];
                  
                  
                  response=[dic objectForKey:@"Response"];
                  
                  
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  
                  
                  if ([response isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      Token_feedback = [dic objectForKey:@"Token_feedback"];
                      str_Para_Session_ID =[dic objectForKey:@"strParaSessionID"];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]  :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[error localizedDescription]  :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later."  :@"0"];
        //NSLog(@"%@",exception.reason);
    }
    
}







-(void) Post_comment:(NSString *)strIndustry
{
    
    @try {
         
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //  [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
        
         NSString* br_codes;
        if ([gblclass.arr_transfer_within_acct count]>0)
        {
            
        NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        
//        NSLog(@"%@",gblclass.is_default_br_code);
        
       
        if (gblclass.is_default_br_code ==(NSString *) [NSNull null] || [gblclass.is_default_br_code isEqualToString:@"(null)"])
        {
            br_codes = [split objectAtIndex:9];
        }
        else
        {
            br_codes = gblclass.is_default_br_code;
        }
            
        }
        else
        {
            br_codes = gblclass.is_default_br_code;
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:Token_feedback],
                                    [encrypt encrypt_Data:gblclass.user_login_name],
                                    [encrypt encrypt_Data:gblclass.user_email],
                                    [encrypt encrypt_Data:txt_comment.text],
                                    [encrypt encrypt_Data:gblclass.Mobile_No],
                                    [encrypt encrypt_Data:br_codes],
                                    [encrypt encrypt_Data:gblclass.is_default_acct_no],
                                    [encrypt encrypt_Data:str_Para_Session_ID],nil]
                                                              forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"token_feedback",
                                                                       @"customerName",
                                                                       @"customerEmail",
                                                                       @"customerComments",
                                                                       @"customerPhone",
                                                                       @"branchCode",
                                                                       @"accountNo",
                                                                       @"ParaSessionID", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        //UserFeedBack  GetCustomerFeedBack
        [manager POST:@"customerfeedbackwithtoken" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSString* response;
                  
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  gblclass.arr_acct_statement1=[[NSMutableArray alloc] init];
                  
                  NSMutableArray* arr2;
                  
                  arr2=[[NSMutableArray alloc] init];
                  gblclass.arr_acct_statement1=[(NSDictionary *) dic objectForKey:@"FResponse"];
                  
                  
                  response=[dic objectForKey:@"Response"];
                  
                  
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //
                      //                      [alert addAction:ok];
                      //                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  
                  
                  
                  if ([response isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      //                  [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"]  :@"1"];
                      txt_comment.text=@"";
                      UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Success" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                         
                          gblclass.landing = @"1";
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
                          [self presentViewController:vc animated:NO completion:nil];
                          
                      }];
                      [alertController addAction:alertAction];
                      [self presentViewController:alertController animated:YES completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]  :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:[error localizedDescription]  :@"0"];
                  
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
                  
              }];
        
    } @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        //NSLog(@"%@",exception.reason);
    }
    
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//    if (buttonIndex == 0)
//    {
//        //Do something
//        //NSLog(@"1");
//    }
//    else if(buttonIndex == 1)
//    {
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//         [self presentViewController:vc animated:NO completion:nil];
//    }
//
//}


-(IBAction)btn_settings:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}


#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height*0.180;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

//#define kOFFSET_FOR_KEYBOARD 30.0
//
//-(void)keyboardWillShow
//{
//    // Animate the current view out of the way
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)keyboardWillHide
//{
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}

//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:sender.text])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}

//method to move the view up/down whenever the keyboard is shown/dismissed
//-(void)setViewMovedUp:(BOOL)movedUp
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
//
//    CGRect rect = self.view.frame;
//
// //   //NSLog(@"%d",rect );
//
//    if (movedUp)
//    {
//        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
//        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//
//
//        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
//
//
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//
//    }
//    else
//    {
//        // revert back to the normal state.
//        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
//        [self.view endEditing:YES];
//    }
//    self.view.frame = rect;
//    [UIView commitAnimations];
//}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [txt_comment resignFirstResponder];
    
    //[self keyboardWillHide];
}


//- (void)textViewDidEndEditing:(UITextView *)textView
//{
//    [textView resignFirstResponder];
//}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    
//    NSLog(@"%@",txt_comment.text);
    if ([txt_comment.text isEqualToString:@"Saki xXxx xxXx 43##"])
    {
        txt_comment.text = @"";
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"udi"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    return YES;
}


- (BOOL)isAcceptableTextLength:(NSUInteger)length
{
    return length <= 25;
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSInteger MAX_DIGITS=1200;
    
    
    if ([theTextField isEqual:txt_comment])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789. "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    gblclass.landing = @"1";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
//    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_Pay:(id)sender
{
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //      // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}




///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            
            [self Do_Customer_FeedBack:@""];
        }
        else if ([chk_ssl isEqualToString:@"comment"])
        {
            chk_ssl=@"";
            
            [self Post_comment:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
    
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    //    [self AccountSummary:@""];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        //        [self.connection cancel];
        [self Do_Customer_FeedBack:@""];
    }
    else if ([chk_ssl isEqualToString:@"comment"])
    {
        chk_ssl=@"";
        //        [self.connection cancel];
        [self Post_comment:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        //        [self.connection cancel];
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            [self custom_alert:statusString  :@"0"];
            
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

