//
//  ParkPlaceMark.m
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "ParkPlaceMark.h"

@implementation ParkPlaceMark
@synthesize coordinate,title,subtitle;

- (NSString *)subtitle{
    return subtitle;
}
- (NSString *)title{
    return title;
}


-(id)initWithCoordinate:(CLLocationCoordinate2D)c{
    self = [super init];
    if (self) {
        coordinate = c;
    }
    return self;
    
    
}

-(void) setTitle: (NSString*) newTitle
{
    title = newTitle ;
}

-(void) setSubtitle: (NSString*) newSubtitle
{
    subtitle = newSubtitle;
}


-(void) dealloc {
    self.title = nil;
    self.subtitle = nil;
    //[super dealloc];
}


@end
