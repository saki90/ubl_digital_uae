//
//  RadioButton.h
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RadioButtonDelegate <NSObject>
-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString*)groupId;
@end


@interface RadioButton : UIView
{
    NSString *_groupId;
    NSUInteger _index,_width,_height;
    CGRect btnFrame;
    UIButton *_button;
}
@property(nonatomic,retain)NSString *groupId;
@property(nonatomic,assign)NSUInteger index;
@property(nonatomic,assign)NSUInteger width;
@property(nonatomic,assign)NSUInteger height;
//@property(nonatomic,retain)UIButton *button;

//-(id)initWithGroupId:(NSString*)groupId index:(NSUInteger)index;
-(void)handleButtonTap;
-(id)initWithGroupId:(NSString*)groupId index:(NSUInteger)index andFrame:(CGRect)frame;
+(void)addObserverForGroupId:(NSString*)groupId observer:(id)observer;
+(void)buttonSelected:(RadioButton*)radioButton;
+(void) getradiobtn:(NSUInteger *)index andgroup:(NSString*)groupid;

@end
