//
//  QRCode_pin_otp.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 02/03/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "TransitionDelegate.h"


@interface QRCode_pin_otp : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    IBOutlet UILabel* lbl_switch;
    IBOutlet UISwitch* myswitch;
    IBOutlet UIView* vw_switch;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

