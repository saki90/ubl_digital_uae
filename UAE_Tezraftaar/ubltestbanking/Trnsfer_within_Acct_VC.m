//
//  Trnsfer_within_Acct_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 16/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Trnsfer_within_Acct_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "PayeePayDetail.h"
#import "Load_payee_list_Model.h"
#import "TransitionDelegate.h"
#import "APIdleManager.h"
#import "AppDelegate.h"
#import "Encrypt.h"

@interface Trnsfer_within_Acct_VC ()<NSURLConnectionDataDelegate>
{
    NSMutableArray* arr_act_;
    GlobalStaticClass* gblclass;
    NSDictionary* dic;
    NSUInteger *set;
    NSArray* split;
    NSArray* split_to;
    UIAlertController *alert;
    MBProgressHUD* hud;
    NSString* str_acct_name_frm;
    NSString* str_acct_id_frm;
    NSString* str_acct_no_frm;
    NSString* str_acct_name_to;
    NSString* str_acct_no_to;
    NSString* str_acct_id_to;
    NSString* base_ccy;
    NSString* email;
    NSString* ran;
    NSString* privalages;
    NSString* acct_type;
    NSString* chk_acct_type;
    
    NSString* branch_code;
    NSString* bank_name;
    NSString* bank_imd;
    NSString* access_key;
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    NSArray* split_bill;
    UIImageView* img;
    UILabel* label;
    
    NSString* acct_frm_chck, *acct_to_chck;
    
    UIControl   *backCover;
    
    NSString* tt_accesskey,*tc_accesskey,*tt_id,*registered_consumer_id,*bill_id,*lbl_customer_id,*lbl_consumer_nick,*payfrom_acct_id,*cmbpay_frm_selected_item_text,*cmbpay_frm_selected_item_value,*amnts,*comment,*str_ccy,*lbl_transaction_type,*guid,*otp_pin;
    
    NSNumber *number;
    NSString* chk_ssl;
    NSAttributedString * imageStr1;
    NSNumberFormatter *numberFormatter;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Trnsfer_within_Acct_VC
@synthesize txt_acct_from;
@synthesize txt_acct_to;
@synthesize txt_t_pin;
@synthesize lbl_t_pin;
@synthesize txt_amnt;
@synthesize txt_comment;
@synthesize transitionController;
@synthesize lbl_balance;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    arr_bill_names=[[NSMutableArray alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    gblclass.arr_review_withIn_myAcct=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    ssl_count = @"0";
    txt_t_pin.delegate=self;
    txt_acct_to.delegate=self;
    txt_acct_from.delegate=self;
    txt_amnt.delegate=self;
    txt_comment.delegate=self;
    
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    lbl_t_pin.hidden=YES;
    txt_t_pin.hidden=YES;
    
    
    
    
    
    // [btn_submit setBackgroundColor:  [UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    
    if ([gblclass.is_debit_lock isEqualToString:@"0"])
    {
        txt_acct_from.text = gblclass.is_default_acct_id_name;
        str_acct_name_frm = gblclass.is_default_acct_id_name;
        str_acct_no_frm = gblclass.is_default_acct_no;
        str_acct_id_frm = gblclass.is_default_acct_id;
        _lbl_acct_frm.text = gblclass.is_default_acct_no;
        
        acct_frm_chck=gblclass.is_default_acct_no;
        
        btn_submit.enabled = YES;
    }
    else
    {
        txt_acct_from.text =  @"-";
        str_acct_name_frm = @"-";
        str_acct_no_frm = @"-";
        str_acct_id_frm = @"-";
        _lbl_acct_frm.text = @"-";
        
        acct_frm_chck = @"-";
        
        btn_submit.enabled = NO;
    }
    
    
    
    
    
    //WITH IMAGE ::
    
    //    UILabel * Label = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 200, 60)];
    //    [self.view addSubview:Label];
    
    
//    NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    if ([gblclass.arr_transfer_within_acct count]>0)
    {
        
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0 || [[split objectAtIndex:6] isEqualToString:@""])
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        NSString *mystring =[NSString stringWithFormat:@"%@",balc];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        
        
        //lbl_balance.attributedText = mutableAttriStr1;
        
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[numberFormatter stringFromNumber:number]];
        
    }
    else
    {
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString *mystring =[NSString stringWithFormat:@"%@",@"0"];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        
        //lbl_balance.attributedText = mutableAttriStr1;
        
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
    }
    
    
    
    //    for (dic in gblclass.actsummaryarr)
    //    {
    //        set = [gblclass.actsummaryarr indexOfObject:dic];
    //        //NSLog(@"%d",set);
    
    //        refreshaccount = [[GnbRefreshAcct alloc] initWithDictionary:dic];
    //        [acc_name addObject:refreshaccount.accountName];
    //        [acc_regid addObject:refreshaccount.registeredAccountId];
    //        [acc_type addObject:refreshaccount.accountTypeDesc];
    //    }
    
    
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    
    txt_Utility_bill_name.text= gblclass.pay_combobill_name;
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    //        [self load_pay_list:@""];
    //    }
 
    
    [hud hideAnimated:YES];
    
 
    
    if ([gblclass.chck_qr_send isEqualToString:@"1"])
    {
        
        //NSLog(@"%@",gblclass.arr_qr_code);
        
        //  split = [[gblclass.arr_qr_code objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        txt_acct_to.text=[gblclass.arr_qr_code objectAtIndex:0];
        str_acct_name_to=[gblclass.arr_qr_code objectAtIndex:0];
        str_acct_no_to=[gblclass.arr_qr_code objectAtIndex:1];
        str_acct_id_to=[gblclass.arr_qr_code objectAtIndex:2];
        
        acct_to_chck=[gblclass.arr_qr_code objectAtIndex:1];
        
        btn_combo_to_chk.enabled=NO;
        
    }
    else
    {
        btn_combo_to_chk.enabled=YES;
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.hidden=YES;
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
}


- (void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"viewDidAppear call");
    
    [super viewWillDisappear:animated];
    
    if (self.presentedViewController)
    {
        [table_to reloadData];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    if (tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];    //count number of row from counting array hear cataGorry is An Array
    }
    else if(tableView==table_to)
    {
        return [gblclass.arr_transfer_within_acct_to count];
    }
    else
    {
        return [arr_bill_names count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        NSString *numberString =[NSString stringWithFormat:@"%@",[split objectAtIndex:6]];
        NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:decimalNumber]];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if(tableView==table_to)
    {
        
        split = [[gblclass.arr_transfer_within_acct_to objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
        
    }
    else
    {
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        cell.textLabel.text = [split_bill objectAtIndex:0];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    
    table_to.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [table_from reloadData];
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        txt_acct_from.text=[split objectAtIndex:0];
        str_acct_name_frm=[split objectAtIndex:0];
        _lbl_acct_frm.text=[split objectAtIndex:1];
        str_acct_no_frm=[split objectAtIndex:1];
        str_acct_id_frm=[split objectAtIndex:2];
        // lbl_balance.text=[split objectAtIndex:6];
        
        
        acct_frm_chck=[split objectAtIndex:1];
        
        _lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        NSString *mystring =[NSString stringWithFormat:@"%@",balc];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        //    lbl_balance.attributedText = mutableAttriStr1;
        
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
    else if(tableView==table_to)
    {
        [table_to reloadData];
        gblclass.indexxx=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        
        
        //NSLog(@"%@",[NSString stringWithFormat:@"%ld",(long)indexPath.row]);
        
        //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct_to count]);
        
        split_to = [[gblclass.arr_transfer_within_acct_to objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        
        txt_acct_to.text=[split_to objectAtIndex:0];
        str_acct_name_to=[split_to objectAtIndex:0];
        str_acct_no_to=[split_to objectAtIndex:1];
        str_acct_id_to=[split_to objectAtIndex:2];
        
        acct_to_chck=[split_to objectAtIndex:1];
        
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        gblclass.pay_combobill_name=[split_bill objectAtIndex:0];
        
        switch (indexPath.row)
        {
            case 0:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"]; //utility_bill   //bill_all
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
            case 1:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_voucher"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 2:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"online_shopping"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 3:
                
                 
                
                break;
                
            case 4:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_within_acct"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 5:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_other_acct"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 6:
                
                //                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                //    [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            default: break;
                
        }
        
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=YES;
        table_to.hidden=YES;
        txt_t_pin.text=@"";
    }
    
    vw_table.hidden=YES;
    
}

-(IBAction)btn_back:(id)sender
{
    
    gblclass.chck_qr_send=@"0";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
    //[self dismissModalStack];
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];
    //    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    //    vc.view.alpha = alpha1;
    //    [self presentViewController:vc animated:NO completion:nil];
    
    
    // [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}

- (void)dismissViewControllerAnimated:(BOOL)flag
                           completion:(void (^ _Nullable)(void))completion
{
    //NSLog(@"%s ", __PRETTY_FUNCTION__);
    [super dismissViewControllerAnimated:flag completion:completion];
    //    [self dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
}

-(void)dealloc
{
    //NSLog(@"First Dealloc");
}

-(IBAction)btn_Utility_bill_names:(id)sender
{
    if (table_bill.isHidden==YES)
    {
        vw_bill.hidden=NO;
        table_bill.hidden=NO;
        vw_from.hidden=YES;
        table_from.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        table_bill.hidden=YES;
        vw_bill.hidden=YES;
    }
}

-(IBAction)btn_combo_frm:(id)sender
{
    [self.view endEditing:YES];
    lbl_vw_header.text = @"Select Your Account";
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        
        gblclass.custom_alert_msg=@"Only One Account Exist.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        [hud hideAnimated:YES];
        return;
    }
    
    if (table_from.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=YES;
        vw_from.hidden=YES;
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
    
}

-(IBAction)btn_combo_to:(id)sender
{
    [self.view endEditing:YES];
    lbl_vw_header.text = @"Select Account to Transfer";
    
    if ([gblclass.arr_transfer_within_acct_to count]==1)
    {
        gblclass.custom_alert_msg=@"Only One Account Exist";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        
        [hud hideAnimated:YES];
        
        return;
    }
    
    if (table_to.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=YES;
        vw_from.hidden=YES;
        vw_to.hidden=NO;
        table_to.hidden=NO;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        [table_to bringSubviewToFront:self.view];
        vw_table.hidden=NO;
    }
    else
    {
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=YES;
    }
    
}

-(IBAction)btn_submit:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    NSArray* split1 = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    //   NSArray* arr=[[split1 objectAtIndex:3] componentsSeparatedByString: @""];
    ran=[split objectAtIndex:3];
    // privalages=[ran substringWithRange:NSMakeRange(2,1)];
    acct_type=[split1 objectAtIndex:5];
    
    
    chk_acct_type=@"0";
    // CHeck Account Type SY,CM,O
    
    if ([acct_type isEqualToString:@"SY"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    else if ([acct_type isEqualToString:@"CM"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    if ([acct_type isEqualToString:@"O"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Do Not Deposit" :@"0"];
        
//        gblclass.custom_alert_msg=@"Do Not Deposit";
//        gblclass.custom_alert_img=@"0";
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
        
        return;
    }
}


-(void)Pay_Account
{
    // Check Privalages 111000
    
    if ([privalages isEqualToString:@"0"])
    {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Do Not Deposit" :@"0"];
        
        return;
    }
    
    if ([txt_acct_from.text isEqualToString:@" "] || txt_acct_from.text.length==0)
    {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Select From Account" :@"0"];
        return;
        
    }
    else if ([txt_acct_to.text isEqualToString:@" "] || txt_acct_to.text.length==0)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Select To Account" :@"0"];
        return;
    }
    else if ([txt_acct_from.text isEqualToString:txt_acct_to.text])
    {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Select Different Account" :@"0"];
        return;
    }
    else if ([txt_amnt.text isEqualToString:@" "] || txt_amnt.text.length==0)
    {
         [hud hideAnimated:YES];
         [self custom_alert:@"Select Amount" :@"0"];
         return;
    }
    else if ([txt_comment.text isEqualToString:@" "] || txt_comment.text.length==0)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Select Comment" :@"0"];
        return;
    }
    
    else
    {
        
        if (lbl_t_pin.hidden==YES && txt_t_pin.hidden==YES)
        {
            lbl_t_pin.hidden=NO;
            txt_t_pin.hidden=NO;
            [hud hideAnimated:YES];
        }
        else if ([txt_t_pin.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
        }
        else
        {
            
            // [self load_pay_list:@""];
            [self Pay_Account:@""];
        }
    }
    
}


-(void) Pay_Account:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    base_ccy=gblclass.base_currency; // [split objectAtIndex:0];
    
    
    //gblclass.arr_user_pw_table2
    
    //    NSArray* split1 = [[gblclass.arr_user_pw_table2 objectAtIndex:0] componentsSeparatedByString: @"|"];
    email=gblclass.user_email; //@"bashir.hasan@ubl.com.pk";//[split1 objectAtIndex:1];
    
    
    if (email.length==0)
    {
        email=@"";
    }
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],str_acct_id_frm,str_acct_name_frm,str_acct_no_frm,[txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""],base_ccy,str_acct_id_to,str_acct_name_to,str_acct_no_to,bank_name,@"",access_key,email,@"",@"",@"1111",bank_imd,txt_comment.text,@"",str_acct_no_to,@"UBL",uuid, nil] forKeys:
                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"payAnyOneAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",@"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",@"strEmail",@"strIBFTRelation",@"strIBFTPOT",@"strtxtPin",@"payAnyOneFromBankImd",@"strTxtComments",@"payAnyOneToBranchCode",@"payAnyOneToAccountNo",@"strBankCode",@"strGuid", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                   message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert1 show];
                  
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert2 show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
   
    if (buttonIndex == 0)
    {
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
 
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
}





-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:YES completion:nil];
}


-(void)load_pay_list:(NSString *)strIndustry
{
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //NSLog(@"%@",gblclass.user_id);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],gblclass.is_default_acct_id, nil] forKeys:
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"payAnyOneAccountID", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"LoadPayeeList" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSMutableArray* arr_load_payee_list;
              
              arr_load_payee_list=[[NSMutableArray alloc] init];
              arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              
              NSUInteger *set;
              for (dic in arr_load_payee_list)
              {
                  set = [arr_load_payee_list indexOfObject:dic];
                  
                  branch_code=[dic objectForKey:@"BRANCH_CODE"];
                  access_key=[dic objectForKey:@"TT_NAME"];
                  bank_name=[dic objectForKey:@"BANK_NAME"];
                  bank_imd=[dic objectForKey:@"BANK_IMD"];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height*0.180;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self keyboardWillHide];
    
    
    [txt_comment resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    
    vw_bill.hidden=YES;
    // table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    vw_from.hidden=YES;
    // table_from.hidden=YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
    //    if(string.length > 0)
    //    {
    
    //NSLog(@"%@",textField);
    if ([textField isEqual:txt_amnt])
    {
        MAX_DIGITS=13;
        
        //NSLog(@"%lu",txt_amnt.text.length);
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        
//        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
//        [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSLog(@"%@", [currencyFormatter stringFromNumber:[NSNumber numberWithInt:555555555555]]);
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([textField isEqual:txt_t_pin])
    {
        MAX_DIGITS=4;
        
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        
        //
        //        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        //        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        //
        //        [textField addTarget:self
        //                      action:@selector(textFieldDidChange:)
        //            forControlEvents:UIControlEventEditingChanged];
        //
        //        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        //        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([textField isEqual:txt_comment])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
    }
    
    
    //NSLog(@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS);
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    
}

-(void)textFieldDidChange:(UITextField *)theTextField
{
    if ([theTextField isEqual:txt_amnt])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText1 intValue]]];
        
      //  NSLog(@"%@",formattedOutput);
        //@(555555555555);
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        
        txt_amnt.text=[nf stringFromNumber:myNumber];
    }
    
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    //[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    //    NSRange r;
    //    NSString *s = txt; //[self copy];
    //    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
    //        s = [s stringByReplacingCharactersInRange:r withString:@""];
    
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithData:[txt dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    NSString *s = attributedStr.string;
    
    return s;
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}

-(IBAction)btn_review:(id)sender
{
    
    @try {
        
        [hud showAnimated:YES];
        double frm_balc,to_balc;
        
        NSNumber* lbl_balc = number; //[number stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSString* txt_amnt2 = [txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        frm_balc=[lbl_balc doubleValue];
        to_balc=[txt_amnt2 doubleValue];
        
        
        if ([acct_to_chck isEqualToString:@""] || acct_to_chck.length==0)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Select To Account"  :@"0"];
            return;
        }
        
        if ([acct_frm_chck isEqualToString:acct_to_chck])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Duplicate Account"  :@"0"];
            
            return;
        }
        if (to_balc>frm_balc)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Available balance is less than the transaction amount"  :@"0"];
            return;
        }
        if([txt_amnt.text isEqualToString:@""])
        {
            [self custom_alert:@"Enter Amount"  :@"0"];
            [hud hideAnimated:YES];
            return;
        }
        else if([txt_amnt.text isEqualToString:@"0"])
        {
            [self custom_alert:@"Please enter a valid amount"  :@"0"];
            [hud hideAnimated:YES];
            
            return;
        }
        else if([txt_comment.text isEqualToString:@""])
        {
            
            txt_comment.text=@"";
        }
        
        
        [gblclass.arr_values removeAllObjects];
        [gblclass.arr_review_withIn_myAcct removeAllObjects];
        
        [gblclass.arr_review_withIn_myAcct addObject:txt_acct_to.text];
        [gblclass.arr_review_withIn_myAcct addObject:str_acct_no_to];
        [gblclass.arr_review_withIn_myAcct addObject:txt_acct_from.text];
        [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
        [gblclass.arr_review_withIn_myAcct addObject:txt_amnt.text];
        [gblclass.arr_review_withIn_myAcct addObject:txt_comment.text];
        
        gblclass.check_review_acct_type = @"My_acct";
        
        base_ccy=gblclass.base_currency;
        email=gblclass.user_email;
        
        if (email.length==0)
        {
            email=@"";
        }
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:str_acct_id_frm];
        [gblclass.arr_values addObject:str_acct_name_frm];
        [gblclass.arr_values addObject:str_acct_no_frm];
        [gblclass.arr_values addObject:[txt_amnt.text
                                        stringByReplacingOccurrencesOfString:@"," withString:@""]];
        [gblclass.arr_values addObject:base_ccy];
        [gblclass.arr_values addObject:str_acct_id_to]; //  3894493
        [gblclass.arr_values addObject:str_acct_name_to];
        [gblclass.arr_values addObject:str_acct_no_to];
        [gblclass.arr_values addObject:@"UBL"]; // bank_name
        [gblclass.arr_values addObject:@"949"];
        [gblclass.arr_values addObject:@"FT"];  //  3P
        [gblclass.arr_values addObject:email];
        [gblclass.arr_values addObject:@""];
        [gblclass.arr_values addObject:@""];
        [gblclass.arr_values addObject:@"1111"];
        [gblclass.arr_values addObject:@"588974"]; //0  bank_imd
        [gblclass.arr_values addObject:txt_comment.text];
        [gblclass.arr_values addObject:@""];
        [gblclass.arr_values addObject:str_acct_no_to];
        [gblclass.arr_values addObject:@""];
        [gblclass.arr_values addObject:@""];
        
        
        //        branch_code=[dic objectForKey:@"BRANCH_CODE"];
        //        access_key=[dic objectForKey:@"TT_NAME"];
        //        bank_name=[dic objectForKey:@"BANK_NAME"];
        //        bank_imd=[dic
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"review";
            [self SSL_Call];
        }
        
        //[self SSL_Call];
        
        
        //   [self Pay_Account_Within:@""];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Please try again later."  :@"0"];
    }
    
}



-(void) Pay_Account_Within:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //    NSString *uuid = [[NSUUID UUID] UUIDString];
    //    NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userID",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"payAnyOneAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"StrCCY",
                                                                   @"payAnyOneToAccountID",
                                                                   @"lblPayTo",
                                                                   @"lblVAccNo",
                                                                   @"lblBank",
                                                                   @"strAccType",
                                                                   @"ttAccessKey",
                                                                   @"strEmail",
                                                                   @"strIBFTRelation",
                                                                   @"strIBFTPOT",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
 
//              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
//              gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
//              gblclass.rev_Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
//              gblclass.rev_Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
//              gblclass.reviewCommentFieldText=txt_comment.placeholder;
            
 
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
 
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
                  gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
                  gblclass.rev_Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
                  gblclass.rev_Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
                  gblclass.reviewCommentFieldText=txt_comment.placeholder;
                  
                  [hud hideAnimated:YES];
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
//                  NSLog(@"%@",gblclass.story_board);
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
 
                  //                  if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                  //                  {
                  //                      //lbl_otp.enabled=YES;
                  //                     // txt_otp.enabled=YES;
                  //                  }
                  //                  else
                  //                  {
                  //                     // lbl_otp.enabled=NO;
                  //                     // txt_otp.enabled=NO;
                  //                  }
 
                  // [self pay_Confirm_Submit:@""];
                  
 
                  //     [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
 
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              {
                  [hud hideAnimated:YES];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]]  :@"0"];
  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
 
          }];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}






///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
////    if ([self isSSLPinning])
////    {
////        [self printMessage:@"Making pinned request"];
////    }
////    else
////    {
////        [self printMessage:@"Making non-pinned request"];
////    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            [self Pay_Account_Within:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        [self Pay_Account_Within:@""];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];  
            [self custom_alert:statusString :@"0"];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end

