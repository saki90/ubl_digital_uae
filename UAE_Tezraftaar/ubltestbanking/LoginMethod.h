//
//  LoginMethod.h
//
//  Created by   on 22/01/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LoginMethod : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id returnMessage;
@property (nonatomic, assign) double userId;
@property (nonatomic, strong) NSString *response;
@property (nonatomic, strong) NSString *pinPattern;
@property (nonatomic, strong) NSString *status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
