//
//  Review_payee_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 08/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"




@interface Review_payee_VC : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,UITextViewDelegate>
{
    IBOutlet UILabel* lbl_name_to;
    IBOutlet UILabel* lbl_name_To_acct_no;
    IBOutlet UILabel* lbl_name_frm;
    IBOutlet UILabel* lbl_name_frm_acct_no;
    IBOutlet UILabel* lbl_amnt;
    IBOutlet UILabel* lbl_datt;
    IBOutlet UILabel* lbl_comment;
    IBOutlet UILabel* lbl_otp;
    IBOutlet UITextField* txt_otp;
    
    IBOutlet UIButton* btn_pay;
    IBOutlet UIButton* btn_cancel;
    IBOutlet UIButton* btn_ok;
    
    IBOutlet UITextView* txt_msg;
    IBOutlet UITextView* txt_msg_sch;
    IBOutlet UILabel* lbl_comment_heading;
    IBOutlet UILabel* dateHeading;
    
    
    //School Fee
    
    IBOutlet UILabel* lbl_name_to_sch;
    IBOutlet UILabel* lbl_name_To_acct_no_sch;
    IBOutlet UILabel* lbl_name_frm_sch;
    IBOutlet UILabel* lbl_name_frm_acct_no_sch;
    IBOutlet UILabel* lbl_amnt_sch;
    IBOutlet UILabel* lbl_std_id;
    IBOutlet UILabel* lbl_voucher_id;
    IBOutlet UILabel* lbl_comment_sch;
    IBOutlet UILabel* lbl_otp_sch;
    IBOutlet UITextField* txt_otp_sch;
    IBOutlet UILabel* lbl_comment_heading_sch;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UILabel* lbl_trans_amt;
    IBOutlet UITextView* txt_ft_tax;
    IBOutlet UITextView* txt_ft_msg;
    IBOutlet UILabel* lbl_ft_msg;
    IBOutlet UITextField* lbl_ft_msg1;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

-(IBAction)btn_close:(id)sender;
-(IBAction)btn_pay:(id)sender;

//School Fee
@property (strong, nonatomic) IBOutlet UIView *vw_school;


// FBRView
@property (strong, nonatomic) IBOutlet UIView *fbrView;
@property (strong, nonatomic) IBOutlet UILabel *payToLabel;
@property (strong, nonatomic) IBOutlet UILabel *payFromLabel;
@property (strong, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;
@property (strong, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIView *otpView;
@property (strong, nonatomic) IBOutlet UITextField *BusinessOtp;


// Tezraftaar View
@property (strong, nonatomic) IBOutlet UIView *tezraftaarView;
@property (strong, nonatomic) IBOutlet UILabel *payToAccountName;
@property (strong, nonatomic) IBOutlet UILabel *payToAccountNumber;
@property (strong, nonatomic) IBOutlet UILabel *payFromAccountName;
@property (strong, nonatomic) IBOutlet UILabel *payFromAccountNumber;
@property (strong, nonatomic) IBOutlet UILabel *purposeLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountInAed;
@property (strong, nonatomic) IBOutlet UILabel *conversionRate;
@property (strong, nonatomic) IBOutlet UILabel *conversionRateLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountInPkr;
@property (strong, nonatomic) IBOutlet UILabel *serviceFee;
@property (strong, nonatomic) IBOutlet UILabel *serviceFeeLabel;
@property (strong, nonatomic) IBOutlet UILabel *lbl_service_fee_txt;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UILabel *comentsLabel;
@property (strong, nonatomic) IBOutlet UILabel *tezraftaarOtpHeading;
@property (strong, nonatomic) IBOutlet UITextField *tezraftaarOpt;
@property (strong, nonatomic) IBOutlet UITextView *tezraftaarDescriptionTextView;

@end

