//
//  AppDelegate+AppDelegate.m
//  UblLocationSwift
//
//  Created by Basit on 01/01/2018.
//  Copyright © 2018 M3Tech. All rights reserved.
//

#import "AppDelegate+AppDelegate.h"

@implementation AppDelegate (AppDelegate)



-(void)writeToPlist : (NSDictionary*) data  plistName : (NSString*) plistName  {
    
    NSDictionary *dictionary = data;
    NSFileManager *documentDirectory =  NSFileManager.defaultManager;
    NSURL *fileUrl =  [documentDirectory URLsForDirectory:documentDirectory inDomains:NSUserDomainMask].lastObject;
    
    if ([NSKeyedArchiver archiveRootObject:dictionary toFile:fileUrl.path]) {
        
        NSLog(@"true");
    }
    
    
    NSString *loadedDic =  [NSKeyedUnarchiver unarchiveObjectWithFile:fileUrl.path];
    NSLog(@"%@", loadedDic);
    
}

-(NSDictionary*)readingFromPlist : (NSString*) plistName {
    
   // NSDictionary *dictionary = data;
    NSFileManager *documentDirectory =  NSFileManager.defaultManager;
    NSURL *fileUrl =  [documentDirectory URLsForDirectory:documentDirectory inDomains:NSUserDomainMask].lastObject;
    
    NSDictionary *array;
    NSDictionary *loadedDic = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithFile:fileUrl.path];
    array = loadedDic;
    
    if (array != nil) {
        
        return @{@"NotFound" : @0};
    }
    
    return array;
    
}

-(void)appLaunchStatus {
    
    NSDictionary* flag = [self readingFromPlist:@"AppLaunchStatus.plist"];
    NSDictionary* continueApplication = [self readingFromPlist:@"ContinueApplication.plist"];
    NSLog(@"%@", flag);
    
    if (![flag  isEqual: @{@"NotFound" : @0}]) {
        
        int isNetBankingOn = flag[@"isNetBankingOn"];
        int isSignUpCompleted = flag[@"isSignUpCompleted"];
        int hasAccount = flag[@"hasAccount"];
        
        if (isNetBankingOn == 0 && isSignUpCompleted == 1 && hasAccount == 1){
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"TouchIdSignIn4ViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
            
        }
        else if(isNetBankingOn == 0 && isSignUpCompleted == 0 && hasAccount == 0){
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"LetsGetStartedViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
            
            
        }
        
        else if(isNetBankingOn == 1 && isSignUpCompleted == 1 && hasAccount == 1){
            
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"TouchIdSignIn3ViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
            
        }
        
        
        
    }
    
    else {
        
        NSLog(@"flag Not Found");
        
    }
    
}

@end
