//
//  UUID_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 30/07/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UUID_VC : UIViewController
{
    IBOutlet UILabel* lbl_udid;
    IBOutlet UILabel* lbl_udid_kychain;
    IBOutlet UILabel* lbl_udid_kyc_lib;
}
@end
