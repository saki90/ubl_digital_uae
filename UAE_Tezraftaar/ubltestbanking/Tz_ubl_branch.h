//
//  Tz_ubl_branch.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 25/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tz_ubl_branch : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UISearchResultsUpdating,UIGestureRecognizerDelegate>
{
    IBOutlet UITextField* txt_select_iban_acct;
    IBOutlet UITextField* txt_iban;
    IBOutlet UITextField* txt_name;
    IBOutlet UITextField* txt_select_branch;
    IBOutlet UITextField* txt_acct_numbr;
    IBOutlet UITextField* txt_contact_num;
    IBOutlet UITextField* txt_address;
    IBOutlet UITextField* txt_city;
    IBOutlet UITextField* txt_relation;
    IBOutlet UITextField* txt_email;
    IBOutlet UIView* vw_branch_acct;
    IBOutlet UITableView* table_iban;
    IBOutlet UIView* vw_table;
    IBOutlet UITableView* table_city;
    IBOutlet UITableView* table_branch;
    IBOutlet UITableView* table_relation;
    IBOutlet UITableView* table_bank;
    IBOutlet UISearchBar* search;
    IBOutlet UILabel* lbl_header;
    
    //**********Reachability*********
       Reachability* internetReach;
       BOOL netAvailable;
       //**********Reachability*********
    
    BOOL isFiltered;
    NSMutableArray *arrsearchresult ,*arrsearchname;
    NSMutableArray *arrfullname;
}

@property (nonatomic, strong) NSMutableArray *searchResult;
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

NS_ASSUME_NONNULL_END
