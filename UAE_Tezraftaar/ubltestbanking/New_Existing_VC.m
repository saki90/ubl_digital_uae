//
//  New_Existing_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 31/05/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "New_Existing_VC.h"
#import <AVFoundation/AVFoundation.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>
 

@interface New_Existing_VC ()
{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
}
@end

@implementation New_Existing_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    
    gblclass.str_processed_login_chk=@"0";
    
    [self Play_bg_video];
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
    
    [self.avplayer play];
    
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_next:(id)sender
{
    [sender setBackgroundImage:[UIImage imageNamed:@"existing_user1.png"] forState:UIControlStateHighlighted];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];  //act_summary pie_chart
    [self presentViewController:vc animated:YES completion:nil];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_Signup:(id)sender
{
    
    // [sender setBackgroundImage:[UIImage imageNamed:@"New-User.png"] forState:UIControlStateHighlighted];
    
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setValue:@"0" forKey:@"t&c"];
//    [defaults synchronize];
//    
//    NSLog(@"%@", gblclass.chk_device_jb);
//    
//    if ([gblclass.chk_device_jb isEqualToString:@"1"])
//    {
//        gblclass.isDeviceJailBreaked = @"1";  //JB check];
//        [defaults setValue:@"1"  forKey:@"isDeviceJailBreaked"];
//        [defaults synchronize];
//    }
    
    gblclass.str_processed_login_chk=@"0";
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
    [self presentViewController:vc animated:NO completion:nil];
    
}



-(IBAction)btn_new_Signup:(id)sender
{
    
    // [sender setBackgroundImage:[UIImage imageNamed:@"New-User.png"] forState:UIControlStateHighlighted];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"0" forKey:@"t&c"];
    [defaults synchronize];
    
    NSLog(@"%@", gblclass.chk_device_jb);
    
    if ([gblclass.chk_device_jb isEqualToString:@"1"])
    {
        gblclass.isDeviceJailBreaked = @"1";  //JB check];
        [defaults setValue:@"1"  forKey:@"isDeviceJailBreaked"];
        [defaults synchronize];
    }
    
    gblclass.forget_pw_btn_click=@"1";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    //new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(IBAction)btn_back:(id)sender
{
    [self slide_left];
//    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
//
//    [self presentViewController:vc animated:NO completion:nil];
    
    
      [self dismissViewControllerAnimated:NO completion:nil];
}



-(IBAction)btn_feature:(id)sender
{
    
    [hud showAnimated:YES];
    //
    //
    [self custom_alert:@"Please register your User." :@"0"];
    
    
    
    //  [self checkinternet];
    //   if (netAvailable)
    //   {
    
    //       chk_ssl=@"qr";
    //       [self SSL_Call];
    
    //        gblclass.chk_qr_demo_login=@"1";
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [self.view.window.layer addAnimation:transition forKey:nil];
    //
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //   receive_qr_login  feature
    //         [self presentViewController:vc animated:NO completion:nil];
    //  }
    
    
    
    //    [self slide_right];
    //
    //    gblclass.chk_qr_demo_login=@"1";
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //  receive_qr_login   feature
    //    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
//    gblclass.tab_bar_login_pw_check=@"login";
//    [self peekabooSDK:@"deals"];

    //saki     [self custom_alert:Offer_msg :@""];
}

-(IBAction)btn_find_us:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    
    
    
    //saki     [self peekabooSDK:@"locator"];
    
    
    
    //    [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
}


@end

