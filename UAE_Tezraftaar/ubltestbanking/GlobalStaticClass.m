//
//  GlobalStaticClass.m
//  ubltestbanking
//
//  Created by ammar on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>

@interface GlobalStaticClass ()
{
    APIdleManager* timer_class;
    NSUserDefaults *defaults;
    NSTimer *timer;
}

@end


@implementation GlobalStaticClass

@synthesize SSL_name;
@synthesize is_iPhoneX;
@synthesize SSL_type;
@synthesize wiz_card_chck;
@synthesize trans_amt;
@synthesize arr_resend_otp_cnic;
@synthesize ft_msg;
@synthesize ft_tax;
@synthesize gbl_curr;
@synthesize landing;

@synthesize lati_pekabo;
@synthesize longi_pekabo;

@synthesize arr_mobiletopup;
@synthesize arr_internet;
@synthesize arr_postpaid;
@synthesize story_board;
@synthesize arr_school_list;
@synthesize device_name;
@synthesize nic_email;
@synthesize nic_mobile_no;
@synthesize nic_account_no;
@synthesize signUp_through_cnic;
@synthesize txt_cnic_signup;
@synthesize txt_atmCard_signup;
@synthesize txt_atmPin_signup;
@synthesize jb_chk_login_back;
@synthesize SSL_Certificate_name;
@synthesize chk_device_jb;
@synthesize isDeviceJailBreaked;
@synthesize actsummaryarr;
@synthesize reviewCommentFieldText;
@synthesize pin_pattern;
@synthesize daily_limit;
@synthesize monthly_limit;
@synthesize T_Pin;
@synthesize welcome_msg;
@synthesize screen_size;
//@synthesize user_name;
@synthesize transaction_history;
@synthesize arr_acct_statement1;
@synthesize arr_acct_statement_global;
@synthesize acct_statmnt_indexpath;
@synthesize storyboardidentifer;
@synthesize atmcardlist;
@synthesize arr_act_statmnt_name;
@synthesize user_id;
@synthesize arr_get_Forgot_pass;
@synthesize arr_send_Forgot_pass;
@synthesize arr_transaction_history;
@synthesize transaction_id;
@synthesize is_default_acct_id;
@synthesize M3sessionid;
@synthesize arr_bill_elements;
@synthesize tot_balc;
@synthesize atm_req_gender_flag;
@synthesize atm_req_gender_flag_value;
@synthesize is_default_m3_balc;
@synthesize is_touch_chk;
@synthesize touch_id_frm_menu;
@synthesize arr_re_genrte_OTP_additn;
@synthesize registd_acct_id_trans;
@synthesize sign_Up_chck;
@synthesize Is_Otp_Required;
@synthesize rev_transaction_type;
@synthesize rev_Str_IBFT_POT;
@synthesize rev_Str_IBFT_Relation;
@synthesize chk_qr_demo_login;
@synthesize acct_statement_name;
@synthesize acct_statement_id;
@synthesize qr_code_pin;
@synthesize is_default_currency;
@synthesize summary_currency;
@synthesize is_qr_payment;
@synthesize sign_up_token;
@synthesize Chk_instant_qr_payment;
@synthesize instant_debit_chk;
@synthesize instant_credit_chk;
@synthesize arr_instant_qr;
@synthesize arr_qr_index;
@synthesize M3Session_ID_instant;
@synthesize token_instant;
@synthesize chk_term_condition;
@synthesize relation_id;
@synthesize amt_mp_limit;
@synthesize arr_instant_p2p;
@synthesize arr_franchise_data;
@synthesize arr_franchise_data_selected;
@synthesize franchise_index;
@synthesize arr_franchise_data_OCL;
@synthesize franchise_btn_chck;
@synthesize acct_summary_section;
@synthesize acct_summary_curr;
@synthesize is_isntant_allow;
@synthesize instant_qr_pin;
@synthesize deposit_slip;
@synthesize chk_payee_management_select;
@synthesize is_default_acct_type;
@synthesize tc_access_key;
@synthesize is_default_br_code;
@synthesize arr_new_signup;
@synthesize cnic_no;
@synthesize frist_last_debt_no;
@synthesize sign_up_user;
@synthesize chk_new_signup_back;
@synthesize isEmail_Exist;
@synthesize btn_reg_selected;
@synthesize dob;
@synthesize forget_pw_btn_click;
@synthesize isFromOnlineShoping;
@synthesize isLogedIn;
@synthesize isIphoneX;
@synthesize onlineShopingRefFieldName;
@synthesize device_chck;
@synthesize chk_termNCondition;
@synthesize TnC_chck_On;
@synthesize forgot_pw_click;
@synthesize camera_back;
@synthesize login_tch_btn_txt;
@synthesize otp_chck;
@synthesize register_acct_back;
@synthesize conti_session_id;
@synthesize is_debit_lock;
@synthesize setting_chck_Tch_ID;
@synthesize termsAndconditions;
@synthesize descriptionDetailModel;
@synthesize arr_school_name;
@synthesize chck_103;
@synthesize school_fee_data;
@synthesize arr_school_edit;
@synthesize arr_school_amnt;
@synthesize Registered_customer_id;
@synthesize peekabo_kfc_userid;
@synthesize arr_daily_limit;
@synthesize bill_type_chck;

@synthesize arr_resend_with_atm_signup_email;
@synthesize arr_resend_with_atm_signup_dob;
@synthesize str_processed_login_chk;

@synthesize vc;


//@synthesize base_currency;
@synthesize arr_transaction_detail;
@synthesize chk_acct_statement;
@synthesize user_email;
@synthesize user_login_name;
@synthesize arr_get_pay_details;
@synthesize arr_transfer_within_acct;
@synthesize arr_transfer_within_acct_deposit_chck;
@synthesize arr_transfer_within_acct_table2;
@synthesize arr_user_pw_table2;
@synthesize is_default_acct_id_name;
@synthesize is_default_acct_no;
@synthesize summary_acct_no;
@synthesize arr_review_withIn_myAcct;
@synthesize add_payee_email;

//Pay Btn from account summary
@synthesize direct_pay_frm_Acctsummary;
@synthesize direct_pay_frm_Acctsummary_nick;
@synthesize direct_pay_frm_Acctsummary_customer_id;



//Login table2 items

@synthesize Mobile_No;
@synthesize UserType;
@synthesize base_currency;



// AddPayee for Fund Transfer
@synthesize acc_type;
@synthesize bankname;
@synthesize bankimd;
@synthesize fetchtitlebranchcode;
@synthesize acc_number;
@synthesize acctitle;
@synthesize acct_nick;

//signup
@synthesize signup_countrycode;
@synthesize signup_acctype;
@synthesize signup_relationshipid;

@synthesize signup_activationcode;
@synthesize signup_userid;
@synthesize signup_username;

// account creation
@synthesize user_name_onbording;
@synthesize cnic_onbording;
@synthesize mobile_number_onbording;
@synthesize email_onbording;
@synthesize nationality_onbording;
@synthesize branch_code_onbording;
@synthesize debitcard_name_onbording;
@synthesize tracking_pin_onbording;

@synthesize request_id_onbording;
@synthesize step_no_onbording;
@synthesize step_no_help_onbording;
@synthesize selected_services_onbording;
@synthesize selected_account_onbording;
@synthesize parent_vc_onbording;
@synthesize current_location_onbording;
@synthesize otpPin;
@synthesize cnic;
@synthesize isAccountSelected;
@synthesize map_call_service;
@synthesize noOfAvailableAttempts;
@synthesize noOfPerformedAttempts;
@synthesize getOnBoardProducts;
@synthesize sr_number;
@synthesize branch_name_onbording;
@synthesize isHideAdditionalServices;
@synthesize rm_avalibilityTimings;
@synthesize timmerLimitOnBording;
@synthesize remainingTimeOnBording;
@synthesize retValue;
@synthesize secureCode;

@synthesize isLocationSelected;
@synthesize selectedLongitude;
@synthesize selctedLatitude;
@synthesize selcted_marker_title;
@synthesize selected_city;
@synthesize selcted_country;
@synthesize selcted_branch_code;

// edit payee

@synthesize selectaccount;
@synthesize pay_combobill_name;

@synthesize arr_bill_ob;
@synthesize arr_bill_isp;
@synthesize arr_bill_load_all;
@synthesize arr_bill_ublbp;
@synthesize indexxx;
@synthesize arr_values;
@synthesize arr_other_pay_list;
@synthesize arr_payee_list;
@synthesize chck_UBLBP_Bill_type;
@synthesize bill_type;
@synthesize strAccessKey_for_add;
@synthesize acct_add_type;
@synthesize acct_add_header_type;
@synthesize add_bill_type;
@synthesize signup_mobile;
@synthesize Chck_Mpin_login;
@synthesize first_time_login_chck;
@synthesize touch_id_enable;
@synthesize touch_id_Mpin;
@synthesize Mpin_login;
@synthesize From_Exis_login;
@synthesize Exis_relation_id;
@synthesize Otp_mobile_verifi;
@synthesize arr_qr_code;
@synthesize Udid;
@synthesize Edit_bill_type;
@synthesize Offer_id;
@synthesize arr_Get_Detail_Desc;
@synthesize ssl_pin_check;
@synthesize arr_ph_address;
@synthesize arr_add_fav;
@synthesize arr_Get_add_fav;
@synthesize offer_name;
@synthesize arr_search_offer_chck;
@synthesize arr_search_offer;
@synthesize arr_graph_name;
@synthesize arr_get_direction;
@synthesize arr_current_location;
@synthesize exis_user_mobile_pin;
@synthesize bill_click;
@synthesize arr_prepaid;
@synthesize arr_prepaid_data;
@synthesize faq_index;
@synthesize app_features;
@synthesize direct_pay_frm_Acctsummary_registed_customer_id;
@synthesize locationCity;
@synthesize locationDataArray;
@synthesize arr_transfer_within_acct_to;
@synthesize chck_arr_transfer_other_acct_frm;
@synthesize ibft_relation;
@synthesize centrePoint;
@synthesize payment_login_chck;
@synthesize custom_alert_img;
@synthesize custom_alert_msg;
@synthesize ppv_domination_id;
@synthesize mainurl1;
@synthesize header_user;
@synthesize header_pw;
@synthesize ssl_pinning_url1;
@synthesize frm_acct_default_chck;
@synthesize token;
@synthesize setting_ver;
@synthesize chk_tezraftar;
@synthesize arr_tezraftar_relation;
@synthesize arr_tezrftar_bene_additn;
@synthesize default_acct_num;
@synthesize arr_tezrftar_bank;
@synthesize arr_tezrftar_city;
@synthesize arr_tezrftar_branch;
@synthesize chck_tezraftr_bene_type;
@synthesize tz_pay_type;
@synthesize tz_city;
@synthesize beneficiary_Bank;

//@synthesize signin_userid;
@synthesize scan_qr_amnt;
@synthesize chck_qr_send;
@synthesize arr_reset_otp_onbarding;
@synthesize arr_get_On_Board_Additional;
@synthesize selcted_branch_address;
@synthesize selcted_branch_contact;

@synthesize controllers_Refrence_Stack;
@synthesize isRedirected;

static int requestDomain = 0;
static GlobalStaticClass *instance = nil;
static NSTimer *timer = nil;

// FBRPayment
@synthesize fbrTcAccessKey;
@synthesize fbrCompanyName;
@synthesize fbrTypeName;
@synthesize fbrAccessKey;
@synthesize fbrPSID;
@synthesize fbrAmount;
@synthesize fbrTtId;
@synthesize fbrRegisteredConsumerId;
@synthesize fbrDueDate;
@synthesize fbrRegisteredAccountId;
@synthesize fbrConsumerNo;
@synthesize fbrNick;
@synthesize fbrBillId;
@synthesize fbrOTPRequired;
@synthesize fbrDescription;
@synthesize fbrTransctionAmount;
@synthesize fbrClientName;

// VirtualWiz
@synthesize wizLoadAmount;
@synthesize wizRequestProfileNo;
@synthesize wizTrasnctionType;
@synthesize wizExistingCardDetails;
@synthesize wizTransctionAmount;
@synthesize strReturnMessage;

// Tezraftaar
@synthesize chk_tezraftaar_transfer;
@synthesize tz_accounts;
@synthesize tz_purpose;
@synthesize tz_fees;
@synthesize tz_note;
@synthesize tz_selectedAccountNo;
@synthesize tezraftaarOTPRequired;
@synthesize tezraftaarReturnMessage;
@synthesize Offer_msg1;


// Named pasteBoard
@synthesize pasteBoard;

+(GlobalStaticClass *)getInstance {
    @synchronized(self) {
        if(instance==nil) {
            instance= [GlobalStaticClass new];
            
        }
    }
    return instance;
}



- (void)SSL_name_set:(NSString *)txt
{
    NSLog(@"%@", txt);
    
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:txt forKey:@"ssl"];
    [defaults synchronize];
}

-(void)SSL_name_get
{
    //  NSLog(@"%@", txt);
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[defaults valueForKey:@"ssl"]);
    SSL_name = [defaults valueForKey:@"ssl"];
}

+(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


-(void)Pekaboo
{
//    NSURL *jsCodeLocation;
//    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
//    RCTRootView *rootView =
//    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
//                         moduleName        : @"peekaboo"
//                         initialProperties :
//     @{
//       @"environment" : @"production", // Supported are beta, stage, production
//       @"pkbc" : @"app.com.brd", // Will be provided by Peekaboo Business team
//       @"type": @"deals", // Supported are deals, locator, availibility
//       }
//                          launchOptions    : nil];
//    UIViewController *vc = [[UIViewController alloc] init];
//    vc.view = rootView;
//    [self presentViewController:vc animated:YES completion:nil];
    
    
    NSString *str = @"Happy to help you &#xUU1999UU";
    
    NSData *data = [str dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *valueUnicode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    NSData *dataa = [valueUnicode dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
    
    Offer_msg1 = valueEmoj;
    
    //_lbl.text = valueEmoj;
    
}

+(NSString *)getPublicIp {
    
    return  @"1.1.1.1";
    
//    //    NSHost *publicIP = [[[NSHost currentHost] addresses] objectAtIndex:0];
//    //    NSError *error = nil;
//
//    switch (requestDomain) {
//        case 0:{
//
//            NSString* url = @"https://api.ipify.org/";
//            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:0 timeoutInterval:40];
//            NSURLResponse* response=nil;
//            NSError* error=nil;
//            NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//
//
//
//            //            NSURL *url = [NSURL URLWithString:@"https://api.ipify.org/"];
//            //            NSString *ipAddress = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
//            //            NSLog(@"My public IP address is: %@", ipAddress);
//
//            if (!(error != nil)) {
//                NSString *ipAddress = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                requestDomain = 0;
//                return ipAddress;
//            }
//
//
//        }
//
//        case 1:{
//
//            NSString* url = @"https://freegeoip.net/json/";
//            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:0 timeoutInterval:40];
//            NSURLResponse* response=nil;
//            NSError* error=nil;
//            NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//
//
//
//            if (!(error != nil)) {
//                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
//                                                                     options:NSJSONReadingMutableContainers
//                                                                       error:&error];
//                NSString* ipAddress = json[@"ip"];
//                requestDomain = 1;
//                return json;
//            }
//        }
//
//            //        case 2:{
//            //
//            //            NSString* url = @"https://api.ipify.org/";
//            //            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:0 timeoutInterval:5];
//            //            NSURLResponse* response=nil;
//            //            NSError* error=nil;
//            //            NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//            //            NSString *ipAddress = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            //
//            //            if (!(error != nil)) {
//            //                requestDomain = 0;
//            //                return ipAddress;
//            //            }
//            //        }
//            //
//            //        case 3:{
//            //
//            //            NSString* url = @"https://api.ipify.org/";
//            //            NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:0 timeoutInterval:5];
//            //            NSURLResponse* response=nil;
//            //            NSError* error=nil;
//            //            NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//            //            NSString *ipAddress = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            //            if (!(error != nil)) {
//            //                requestDomain = 0;
//            //                return ipAddress;
//            //            }
//            //        }
//
//        default:{
//            requestDomain = 0;
//            return @"Default";
//        }
//    }
//
//
//    //
//    //    NSURL *url2 = [NSURL URLWithString:@"https://freegeoip.net/json/"];
//    //    NSString *ipAddress2 = [NSString stringWithContentsOfURL:url2 encoding:NSUTF8StringEncoding error:&error];
//    //    NSLog(@"My public IP address is: %@", ipAddress2);
//    //
//    //    return ipAddress;
//
}

-(void) createTimer {
    
}

+(void) btn_offer {
    
}

+(void) btn_faq {
    
}

+(void) btn_findUs {
    
}

+(void) btn_feature {
    
}

//+(void)logout_session:(NSString* )value
//{
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
//                                    gblclass.story_board bundle:[NSBundle mainBundle]];
//       
//        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//        
//        
//      //  [self presentViewController:myController animated:YES completion:nil];
//        
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//        return value;
//        
//    };
//}

@end
