//
//  UBLBP_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 25/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "UBLBP_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface UBLBP_VC ()<NSURLConnectionDataDelegate>
{
    NSMutableArray* arr_Get_UBLBill_load;
    NSMutableArray* arr_UBP_bill;
    NSMutableArray* arr_UBP_bill_payment;
    GlobalStaticClass* gblclass;
    NSDictionary* dic;
    MBProgressHUD* hud;
    UIAlertController  *alert;
    NSMutableArray* arr_UBLBP_creditCard;
    NSMutableArray* arr_credit_min_full_payment;
    
    NSArray* split;
    NSArray* split_bill;
    
    NSString* payfrom_acct_id;
    NSString* Payfrm_selected_item_text;
    NSString* Payfrm_selected_item_value;
    NSString* str_amnt;
    NSString* lbl_customer_id;
    NSString* lbl_compny_name;
    NSString* str_comment;
    NSString* str_access_key;
    NSString* str_Tc_access_key;
    NSString* str_ccy;
    NSString* str_TT_accesskey;
    NSString* str_tt_id;
    NSString* str_tt_name;
    NSString* str_regt_consumer_id;
    NSString* str_build;
    NSString* lb_customer_id;
    NSString* lbl_customer_nick;
    NSString* status;
    NSString* str_bill_id;
    NSString* str_due_datt;
    NSString* PAYMENT_STATUS;
    NSNumber* IS_PAYBLE_1;
    NSArray* split_credit;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    
    NSMutableArray* arr_bill_UBLBP;
    NSMutableArray* arr_bill_OB;
    NSMutableArray* arr_bill_ISP;
    NSMutableArray* arr_bill_UBP;
    NSMutableArray* arr_get_bill_load;
    NSMutableArray* search;
    NSString* search_flag;
    NSArray* splits;
    UILabel* label;
    UIImageView* img;
    NSString *min_val,*max_val;
    NSString* logout_chck;
    NSString *lbl_Statement_Date;
    NSString *current_Date;
    NSString* chk_ssl;
    NSString* consumer_no;
    NSNumberFormatter *numberFormatter;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation UBLBP_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    
    self.transitionController = [[TransitionDelegate alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    arr_Get_UBLBill_load=[[NSMutableArray alloc] init];
    arr_UBP_bill=[[NSMutableArray alloc] init];
    arr_UBLBP_creditCard=[[NSMutableArray alloc] init];
    arr_credit_min_full_payment=[[NSMutableArray alloc] init];
    arr_bill_names=[[NSMutableArray alloc] init];
    search=[[NSMutableArray alloc] init];
    
    arr_bill_UBLBP=[[NSMutableArray alloc] init];
    arr_bill_UBP=[[NSMutableArray alloc] init];
    arr_bill_OB=[[NSMutableArray alloc] init];
    arr_bill_ISP=[[NSMutableArray alloc] init];
    arr_get_bill_load=[[NSMutableArray alloc] init];
    gblclass.arr_review_withIn_myAcct=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
     txt_comment.autocorrectionType = UITextAutocorrectionTypeNo;
     txt_comment_credit.autocorrectionType = UITextAutocorrectionTypeNo;
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    // [hud showAnimated:YES];
    
    ssl_count = @"0";
    search_flag=@"0";
    txt_combo_bill_names.delegate=self;
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    vw_payment.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    lbl_t_pin.hidden=YES;
    txt_t_pin.hidden=YES;
    vw_credit_card.hidden=YES;
    lbl_t_pin_credit.hidden=YES;
    txt_t_pin_credit.hidden=YES;
    
    txt_amounts.delegate=self;
    txt_credit_amount.delegate=self;
    txt_t_pin_credit.delegate=self;
    txt_comment_credit.delegate=self;
    txt_comment.delegate=self;
    txt_t_pin.delegate=self;
    lbl_t_pin_drive.hidden=YES;
    txt_t_pin_drive.hidden=YES;
    
    
    // [btn_submit setBackgroundColor: [UIColor colorWithRed:1/255.0f green:112/255.0f blue:181/255.0f alpha:1.0f]];
    txt_combo_frm.text= gblclass.is_default_acct_id_name;
    
    Payfrm_selected_item_text=gblclass.is_default_acct_id_name;
    Payfrm_selected_item_value=gblclass.is_default_acct_no;
    payfrom_acct_id=gblclass.is_default_acct_id;
    _lbl_acct_frm.text=gblclass.is_default_acct_no;
    
    //**   [self Get_Bill:@""];
    
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    txt_Utility_bill_name.text= gblclass.pay_combobill_name;
    
    
    if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
    {
        
        //  [hud showAnimated:YES];
        vw_credit_card.hidden=NO;
        vw_table.hidden=YES;
        btn_del.hidden=YES;
        btn_edit.hidden=YES;
        
        gblclass.reviewCommentFieldText = txt_comment_credit.placeholder;
        lbl_bill_type_heading.text=@"UBL Credit Card Payment";
        
        lbl_bill_type.text=@"UBL Credit Card Payment";
        lbl_loan_acct.text=gblclass.direct_pay_frm_Acctsummary_customer_id;
        lbl_compny_name=@"UBL Credit Card";
        
        lbl_credit_card_heading.text = @"CreditCard Account#";
        lbl_credit_card_no.text = gblclass.direct_pay_frm_Acctsummary_customer_id;
        
        str_TT_accesskey=@"UBLBP";
        
        str_access_key=@"UAE_CBP"; //CCP
        str_tt_id=@"0";
        str_regt_consumer_id=gblclass.direct_pay_frm_Acctsummary_registed_customer_id;
        str_bill_id=@"0";
        lbl_customer_nick=gblclass.direct_pay_frm_Acctsummary_nick;
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"load_creditCard_bill";
            [self SSL_Call];
        }
        
        //    [self Get_CreditCard_Bill_Payment:@""];
        
    }
    else
    {
        gblclass.reviewCommentFieldText = txt_comment.placeholder;
        splits=[[gblclass.arr_bill_ublbp objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
        
        txt_combo_bill_names.text=[NSString stringWithFormat:@"%@ %@",[splits objectAtIndex:3],[splits objectAtIndex:2]];
        
        lbl_bill_type.text=[splits objectAtIndex:0];
        lbl_loan_acct.text=[splits objectAtIndex:2];
        lbl_compny_name=[splits objectAtIndex:10];
        str_access_key=[splits objectAtIndex:8];
        
        lbl_customer_id=[splits objectAtIndex:2];
        str_Tc_access_key=[splits objectAtIndex:12];
        
        
        btn_del.hidden=NO;
        btn_edit.hidden=NO;
    }
    
    
    
    //NSLog(@"%@",gblclass.direct_pay_frm_Acctsummary);
    if ([[splits objectAtIndex:0]isEqualToString:@"UBL Cashline"]) //UBL Drive
    {
        lbl_credit_card_heading.text=@"Repayment Account#";
        
        
        [self checkinternet];
        if (netAvailable)
        {
            vw_credit_card.hidden=NO;
            chk_ssl=@"load_ubl_cashline";
            consumer_no=[splits objectAtIndex:2];
            [self SSL_Call];
            
            //  [self Get_Bill_Payment:[splits objectAtIndex:2]];
        }
        
    }
    else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"] && [gblclass.direct_pay_frm_Acctsummary isEqualToString:@"0"])
    {
        
        lbl_credit_card_heading.text=@"CreditCard Account#";
        
        [self checkinternet];
        if (netAvailable)
        {
            // [hud showAnimated:YES];
            
            vw_credit_card.hidden=NO;
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"load_ubl_creditcard";
                [self SSL_Call];
                
            }
            
            // [self Get_CreditCard_Bill_Payment:@""];
        }
    }
    
    if ([gblclass.arr_bill_ublbp count]>0)
    {
        
        //  txt_combo_bill_names.text=[NSString stringWithFormat:@"%@ %@",[gblclass.arr_bill_load_all objectAtIndex:3],[gblclass.arr_bill_load_all objectAtIndex:2]];
        
        
        split_bill = [[gblclass.arr_bill_ublbp objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
        
        //  split_bill=[NSMutableArray arrayWithArray:[gblclass.arr_bill_load_all objectAtIndex:[gblclass.indexxx integerValue]]];
        
        str_tt_name=[split_bill objectAtIndex:0];
        str_tt_id=[split_bill objectAtIndex:1];
        lbl_customer_id=[split_bill objectAtIndex:2];
        str_due_datt=[split_bill objectAtIndex:4];
        str_amnt=[split_bill objectAtIndex:5];
        status=[split_bill objectAtIndex:6];
        str_bill_id=[split_bill objectAtIndex:7];
        str_access_key=[split_bill objectAtIndex:8];
        str_regt_consumer_id=[split_bill objectAtIndex:9];
        lbl_compny_name=[split_bill objectAtIndex:10];
        str_TT_accesskey=[split_bill objectAtIndex:12];
        lbl_customer_nick=[split_bill objectAtIndex:3];
        PAYMENT_STATUS=[split_bill objectAtIndex:13];
        IS_PAYBLE_1=[split_bill objectAtIndex:14];
        
        
        
        lbl_credit_card_no.text=lbl_customer_id;
        lbl_bill_type_heading.text=str_tt_name;
        
        //        lbl_customers_id.text=[split_bill objectAtIndex:2];
        //       lbl_amnts.text=[split_bill objectAtIndex:5];
        //       lbl_due_dat.text=[split_bill objectAtIndex:4];
        lbl_status.text=[split_bill objectAtIndex:6];
        txt_combo_bill_names.text=[split_bill objectAtIndex:3];
        //        lbl_company_name.text=[split_bill objectAtIndex:10];
        _lbl_acct_frm.text=gblclass.is_default_acct_no;
        //        lbl_mobile_no.text=[split_bill objectAtIndex:2];
        
        
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split_bill objectAtIndex:5]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split_bill objectAtIndex:5];
        }
        
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        //       txt_amnts.attributedText = mutableAttriStr1;
        
        
        
    }
    
    
    if ([lbl_compny_name isEqualToString:@"UBL Address"] || [lbl_compny_name isEqualToString:@"UBL Drive"] || [lbl_compny_name isEqualToString:@"UBL Money"])
    {
        [hud hideAnimated:YES];
    }
    
    vw_acct.layer.cornerRadius = 5;
    vw_acct.layer.masksToBounds = YES;
    
    
    vw_table.hidden=YES;
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    vw_credit_card.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    attach1.bounds = CGRectMake(10, 8, 20, 10);
    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    
    NSString* balc;
    
    if ([split objectAtIndex:6]==0)
    {
        balc=@"0";
    }
    else
    {
        balc=[split objectAtIndex:6];
    }
    
    
    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    
    [mutableAttriStr1 appendAttributedString:imageStr1];
    _lbl_balance.attributedText = mutableAttriStr1;
    
    
    //   [hud hideAnimated:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

-(void) Get_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:@"ISP,OB,UBP,UBLBP"],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abcd"], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strTcAccessKey",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              arr_Get_UBLBill_load= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
              arr_bill_UBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              arr_bill_OB= [(NSDictionary *)[dic objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
              arr_bill_ISP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
              arr_bill_UBLBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              NSMutableArray* a;
              a=[[NSMutableArray alloc] init];
              
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              // For UBLBP :::::
              
              for (dic in arr_bill_UBLBP) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS.length==0 || [PAYMENT_STATUS isEqualToString:@" "] || [PAYMENT_STATUS isEqualToString:nil])
                  {
                      PAYMENT_STATUS=@"N/A";
                      [a addObject:PAYMENT_STATUS];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              [hud hideAnimated:YES];
              [table_to reloadData];
              
              if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
              {
                  txt_combo_bill_names.text=gblclass.direct_pay_frm_Acctsummary_nick;
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}


-(void) Get_Bill_Payment:(NSString *)strIndustry
{
    
    @try {
        
        
        NSError *error1 = nil;
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //lbl_customer_id
        
        
        if ([str_access_key isEqualToString:@"RBP"] || [str_access_key isEqualToString:@"rbp"])
        {
            str_access_key=@"";
            str_access_key=@"UBPBP";
        }
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:str_access_key],
                                    [encrypt encrypt_Data:lbl_compny_name],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",strIndustry]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strAccessKey",
                                                                       @"strBillType",
                                                                       @"strConsumersNo",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"GetBillPayment" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                 //NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSMutableArray* a;
                  a=[[NSMutableArray alloc] init];
                  arr_UBP_bill_payment=[[NSMutableArray alloc] init];
                  
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  //    [hud hideAnimated:YES];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
 
                  }
 
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      NSString* lblDueDate=[dic objectForKey:@"lblDueDate"];
                      if (lblDueDate ==(NSString *)[NSNull null] || [lblDueDate isEqualToString:@""])
                      {
                          lblDueDate=@"N/A";
                          [a addObject:lblDueDate];
                      }
                      else
                      {
                          [a addObject:lblDueDate];
                          lbl_due_dat.text=lblDueDate;
                      }
                      lbl_due_dat.text=lblDueDate;
                      
                      NSString* lblStatementDate=[NSString stringWithFormat:@"%@",[dic objectForKey:@"lblStatementDate"]];
                      if ([lblStatementDate isEqualToString:@""] || lblStatementDate ==(NSString *)[NSNull null])
                      {
                          lblStatementDate=@"N/A";
                          [a addObject:lblStatementDate];
                      }
                      else
                      {
                          [a addObject:lblStatementDate];
                      }
                      lbl_statement_due_dat.text=lblStatementDate;
                      
                      NSString* lblMinPaymentB4DueDate=[dic objectForKey:@"lblMinPaymentB4DueDate"];
                      if ([lblMinPaymentB4DueDate isEqualToString:@""] || lblMinPaymentB4DueDate ==(NSString *)[NSNull null])
                      {
                          lblMinPaymentB4DueDate=@"N/A";
                          [a addObject:lblMinPaymentB4DueDate];
                      }
                      else
                      {
                          [a addObject:lblMinPaymentB4DueDate];
                          
                      }
                      
                      
                      NSString* lblMinPaymentAfterDueDate=[dic objectForKey:@"lblMinPaymentAfterDueDate"];
                      if ([lblMinPaymentAfterDueDate isEqualToString:@""] || lblMinPaymentAfterDueDate ==(NSString *)[NSNull null])
                      {
                          lblMinPaymentAfterDueDate=@"N/A";
                          [a addObject:lblMinPaymentAfterDueDate];
                      }
                      else
                      {
                          [a addObject:lblMinPaymentAfterDueDate];
                          lbl_mini_after_due_dat.text=lblMinPaymentAfterDueDate;
                      }
                      
                      
                      NSString* bbb = [a componentsJoinedByString:@"|"];
                      //NSLog(@"%@", bbb);
                      [arr_UBP_bill_payment addObject:bbb];
                      [a removeAllObjects];
                      
                      
                      NSDateComponents *dateComponents_frm;
                      NSDate *to_date,*frm_date;
                      
                      NSDate *currentDate = [NSDate date];
                      //                  dateComponents_frm = [[NSDateComponents alloc] init];
                      //                  [dateComponents_frm setDay:-90];  //-120
                      //                  frm_date = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents_frm toDate:currentDate options:0];
                      //                  //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, frm_date);
                      
                      
                      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                      [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
                      //                NSString *dateFormatter_frm = [dateFormatter stringFromDate:frm_date];
                      
                      current_Date = [dateFormatter stringFromDate:currentDate];
                      //NSLog(@"%@",current_Date);
                      
                      //   //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateFormatter_frm]);
                      
                      
                      
                      lbl_Statement_Date = lblDueDate; //lblStatementDate;
                      
                      // Convert string to date object
                      NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                      [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                      NSDate *date = [dateFormat dateFromString:lbl_Statement_Date];
                      
                      // Convert date object to desired output format
                      [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                      lbl_Statement_Date = [dateFormat stringFromDate:date];
                      
                      
                      
                      split_credit=[[NSArray alloc] init];
                      
                      split_credit = [[arr_UBP_bill_payment objectAtIndex:0] componentsSeparatedByString: @"|"];
                      
                      
                      if ([lbl_Statement_Date compare:current_Date] == NSOrderedDescending)
                      {
                          //Before Due date :
                          //NSLog(@"date1 is later than date2");
                          
                          txt_combo_payment.text=@"Mimimum Payment Before Due Date";
                          lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                          lbl_due_dat.text=[split_credit objectAtIndex:0];
                          lbl_credit_amount.text=[split_credit objectAtIndex:2];
                          txt_credit_amount.hidden=YES;
                          lbl_credit_amount.hidden=NO;
                          
                          vw_table.hidden=YES;
                          
                      }
                      else if ([lbl_Statement_Date compare:current_Date] == NSOrderedAscending)
                      {
                          //NSLog(@"date1 is earlier than date2");
                          //After Due date :
                          
                          txt_combo_payment.text=@"Mimimum Payment After Due Date";
                          lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                          lbl_due_dat.text=[split_credit objectAtIndex:0];
                          lbl_credit_amount.text=[split_credit objectAtIndex:3];
                          txt_credit_amount.hidden=YES;
                          lbl_credit_amount.hidden=NO;
                          
                          vw_table.hidden=YES;
                      }
                      else
                      {
                          //Same Due date :
                          //NSLog(@"dates are the same");
                          
                          txt_combo_payment.text=@"Mimimum Payment Before Due Date";
                          lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                          lbl_due_dat.text=[split_credit objectAtIndex:0];
                          lbl_credit_amount.text=[split_credit objectAtIndex:2];
                          txt_credit_amount.hidden=YES;
                          lbl_credit_amount.hidden=NO;
                          
                          vw_table.hidden=YES;
                      }
                      
                      [hud hideAnimated:YES];
                      vw_credit_card.hidden=NO;
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      
                      btn_review.enabled=NO;
                      btn_cashline_combo.enabled=NO;
                      
                      NSString* str_msg=[dic objectForKey:@"strReturnMessage"];
                      
                      if (str_msg ==(NSString *) [NSNull null])
                      {
                          str_msg=@"";
                      }
                      
                      [self custom_alert:str_msg :@"0"];
                  }
                  
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Try again later." :@"0"];
                  
                //[self custom_alert:[error localizedDescription] :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        NSError* err1;
        
        //NSLog(@"%@",[err1 localizedDescription]);
        
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) Get_CreditCard_Bill_Payment:(NSString *)strIndustry
{
    
    @try {
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
        {
            lbl_customer_id=gblclass.direct_pay_frm_Acctsummary_customer_id;
            str_access_key=@"UAE_CBP"; //CCP
        }
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:lbl_customer_id],
                                    [encrypt encrypt_Data:str_access_key],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"sAccountNumber",
                                                                       @"strAccessKey",
                                                                       @"strCCY",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetCreditCardBillPayment" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSMutableArray* a;
                  a=[[NSMutableArray alloc] init];
                  [arr_UBLBP_creditCard removeAllObjects];
                  
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      NSString* lblDueDate=[dic objectForKey:@"lblDueDate"];
                      if (lblDueDate ==(NSString *)[NSNull null] || [lblDueDate isEqualToString:@""])
                      {
                          lblDueDate=@"N/A";
                          [a addObject:lblDueDate];
                      }
                      else
                      {
                          [a addObject:lblDueDate];
                      }
                      lbl_due_dat.text=lblDueDate;
                      
                      NSString* lblStatementDate=[NSString stringWithFormat:@"%@",[dic objectForKey:@"lblStatementDate"]];
                      if ([lblStatementDate isEqualToString:@""] || lblStatementDate ==(NSString *)[NSNull null])
                      {
                          lblStatementDate=@"N/A";
                          [a addObject:lblStatementDate];
                      }
                      else
                      {
                          [a addObject:lblStatementDate];
                      }
                      lbl_statement_due_dat.text=lblStatementDate;
                      
                      NSString* lblMinPaymentB4DueDate=[dic objectForKey:@"lblMinPayment"];
                      if ([lblMinPaymentB4DueDate isEqualToString:@""] || lblMinPaymentB4DueDate ==(NSString *)[NSNull null])
                      {
                          lblMinPaymentB4DueDate=@"N/A";
                          [a addObject:lblMinPaymentB4DueDate];
                      }
                      else
                      {
                          [a addObject:lblMinPaymentB4DueDate];
                      }
                      
                      lbl_mini_B4_due_dat.text=lblMinPaymentB4DueDate;
                      
                      NSString* lblMinPaymentAfterDueDate=[dic objectForKey:@"lblFullPayment"];
                      if ([lblMinPaymentAfterDueDate isEqualToString:@""] || lblMinPaymentAfterDueDate ==(NSString *)[NSNull null])
                      {
                          lblMinPaymentAfterDueDate=@"N/A";
                          [a addObject:lblMinPaymentAfterDueDate];
                      }
                      else
                      {
                          [a addObject:lblMinPaymentAfterDueDate];
                      }
                      
                      lbl_mini_after_due_dat.text=lblMinPaymentAfterDueDate;
                      
                      NSString* bbb = [a componentsJoinedByString:@"|"];
                      //NSLog(@"%@", bbb);
                      [arr_UBLBP_creditCard addObject:bbb];
                      [a removeAllObjects];
                      
                      vw_credit_card.hidden=NO;
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      btn_review.enabled=NO;
                      
                      NSString* return_msg = [dic objectForKey:@"strReturnMessage"];
                      
                      if (return_msg ==(NSString *)[NSNull null])
                      {
                          return_msg = @"";
                      }
                      
                      [self custom_alert:return_msg :@"0"];
                      
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

-(IBAction)btn_submit:(id)sender
{
    
    //    if (PAYMENT_STATUS== "U")
    //    {
    //
    //        if (IS_PAYBLE == "0")
    //        {
    //            “Can’t Pay”
    //        }
    //
    //        else if (PAYABLE_AMOUNT > 0){
    //            “Payable”
    //        }
    //
    //        Else
    //        {
    //            “Can’t Pay”
    //        }
    //    }
    //
    //    Else
    //    {
    //        “Can’t Pay”
    //    }
    
    
    if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
    {
        [self  Pay_Credit_Card_Frm_summary_VC:@""];
        
        
        [self Pay_CC_Bill:@""];
        
        return;
    }
    
    
    
    
    if ([txt_combo_bill_names.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Bill" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    
    
    
    if ([lbl_compny_name isEqualToString:@"UBL Drive"])
    {
        [self check_UBL_Drive_check];
        
    }
    else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
    {
        [self check_UBL_Credit_Card];
    }
    else
    {
        //        [self Pay_Bill:@""];
        //    }
        
        
        
        if ([txt_combo_bill_names.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Bill" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if([txt_amounts.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if([txt_comment.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        PAYMENT_STATUS=@"U";
        if ([PAYMENT_STATUS isEqualToString:@"U"])
        {
            IS_PAYBLE_1=@"1";
            if ([IS_PAYBLE_1  isEqual: @"0"])
            {
                //NSLog(@"return");
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                
                return;
            }
            else if (IS_PAYBLE_1 > @"0")
            {
                //pay
                //NSLog(@"Pay");
                
                if (lbl_t_pin_drive.hidden==YES && txt_t_pin_drive.hidden==YES)
                {
                    lbl_t_pin_drive.hidden=NO;
                    txt_t_pin_drive.hidden=NO;
                    [hud hideAnimated:YES];
                    
                }
                else if ([txt_t_pin_drive.text isEqualToString:@""])
                {
                    
                    
                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [hud hideAnimated:YES];
                    return;
                    
                }
                else
                {
                    //  [self Pay_Bill:@""];
                    [self Pay_Bill_Credit_Card:@""];
                }
                
            }
            else
            {
                
                [hud hideAnimated:YES];
                //cannt pay
                //NSLog(@"cannt pay");
                
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            [hud hideAnimated:YES];
            //cantt pay
            //NSLog(@"cannt pay");
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}

-(void)check_UBL_Credit_Card
{
    if ([txt_combo_frm.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Account" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_combo_bill_names.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Bill" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_combo_payment.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Payment Type" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_combo_payment.text isEqualToString:@"User Specified"])
    {
        if ([txt_credit_amount.text isEqualToString:@""])
        {
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
    }
    
    else if([txt_comment_credit.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    PAYMENT_STATUS=@"U";
    if ([PAYMENT_STATUS isEqualToString:@"U"])
    {
        IS_PAYBLE_1=@"2";
        if ([IS_PAYBLE_1  isEqual: @"0"])
        {
            //NSLog(@"return");
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if (IS_PAYBLE_1 > @"1")
        {
            //pay
            //NSLog(@"Pay");
            
            //            if (lbl_t_pin_credit.hidden==YES && txt_t_pin_credit.hidden==YES)
            //            {
            //                lbl_t_pin_credit.hidden=NO;
            //                txt_t_pin_credit.hidden=NO;
            //                [hud hideAnimated:YES];
            //
            //            }
            //            else if ([txt_t_pin_credit.text isEqualToString:@""])
            //            {
            //
            //
            //                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
            //
            //                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //                [alert addAction:ok];
            //
            //                [self presentViewController:alert animated:YES completion:nil];
            //                [hud hideAnimated:YES];
            //                return;
            //
            //            }
            //            else
            //            {
            [self Pay_Bill_Credit_Card:@""];
            //            }
            
        }
        else
        {
            
            [hud hideAnimated:YES];
            //cannt pay
            //NSLog(@"cannt pay");
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        [hud hideAnimated:YES];
        //cantt pay
        //NSLog(@"cannt pay");
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}



-(void)check_UBL_Drive_check
{
    if ([txt_combo_bill_names.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Bill" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_amounts.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_comment.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    PAYMENT_STATUS=@"U";
    if ([PAYMENT_STATUS isEqualToString:@"U"])
    {
        IS_PAYBLE_1=@"2";
        if ([IS_PAYBLE_1  isEqual: @"0"])
        {
            //NSLog(@"return");
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if (IS_PAYBLE_1 > 0)
        {
            //pay
            //NSLog(@"Pay");
            
            //            if (lbl_t_pin_drive.hidden==YES && txt_t_pin_drive.hidden==YES)
            //            {
            //                lbl_t_pin_drive.hidden=NO;
            //                txt_t_pin_drive.hidden=NO;
            //                [hud hideAnimated:YES];
            //
            //            }
            //            else if ([txt_t_pin_drive.text isEqualToString:@""])
            //            {
            //
            //
            //                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
            //
            //                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //                [alert addAction:ok];
            //
            //                [self presentViewController:alert animated:YES completion:nil];
            //                [hud hideAnimated:YES];
            //                return;
            //
            //            }
            //            else
            //            {
            
            [self Pay_Bill:@""];
            
            return;
            //            }
            
        }
        else
        {
            
            [hud hideAnimated:YES];
            //cannt pay
            //NSLog(@"cannt pay");
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        [hud hideAnimated:YES];
        //cantt pay
        //NSLog(@"cannt pay");
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

-(void) Pay_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_Get_UBLBill_load=[[NSMutableArray alloc] init];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    txt_t_pin.text=@"1111";
    
    //NSLog(@"%@", gblclass.user_id);
    //NSLog(@"%@", payfrom_acct_id);
    //NSLog(@"%@", Payfrm_selected_item_text);
    //NSLog(@"%@", Payfrm_selected_item_value);
    //NSLog(@"%@", str_amnt);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_compny_name);
    //NSLog(@"%@", txt_comment.text);
    //NSLog(@"%@", str_access_key);
    //NSLog(@"%@", str_Tc_access_key);        //str_TT_accesskey
    //NSLog(@"%@", gblclass.base_currency);
    //NSLog(@"%@", txt_t_pin.text);
    //NSLog(@"%@", str_TT_accesskey);
    //NSLog(@"%@", str_tt_id);
    //NSLog(@"%@", str_regt_consumer_id);
    //NSLog(@"%@", str_bill_id);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_customer_nick);
    //NSLog(@"%@", uuid);
    
    
    NSString* t_pin;
    NSString* comment;
    NSString* Amount;
    
    
    if ([lbl_compny_name isEqualToString:@"UBL Drive"])
    {
        t_pin=@"1111";//txt_t_pin_drive.text;
        comment=txt_comment.text;
        Amount=txt_amounts.text;
    }
    else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
    {
        t_pin=txt_t_pin_credit.text;
        comment=txt_comment_credit.text;
        
        if ([txt_combo_payment.text isEqualToString:@"User Specified"])
        {
            Amount=txt_credit_amount.text;
        }
        else
        {
            Amount=lbl_credit_amount.text;
        }
        
    }
    else
    {
        Amount=txt_amounts.text;
        t_pin=@"1111";//txt_t_pin_drive.text;
        comment=txt_comment.text;
    }
    
    
    [gblclass.arr_values addObject:gblclass.user_id];
    [gblclass.arr_values addObject:gblclass.M3sessionid];
    [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
    [gblclass.arr_values addObject:payfrom_acct_id];
    [gblclass.arr_values addObject:Payfrm_selected_item_text];
    [gblclass.arr_values addObject:Payfrm_selected_item_value];
    [gblclass.arr_values addObject:Amount];
    [gblclass.arr_values addObject:lbl_customer_id];
    [gblclass.arr_values addObject:lbl_compny_name];
    [gblclass.arr_values addObject:comment];
    [gblclass.arr_values addObject:str_access_key];
    [gblclass.arr_values addObject:@""];
    [gblclass.arr_values addObject:@""];
    [gblclass.arr_values addObject:gblclass.base_currency];
    [gblclass.arr_values addObject:@"1111"];
    [gblclass.arr_values addObject:str_TT_accesskey];
    [gblclass.arr_values addObject:str_tt_id];
    [gblclass.arr_values addObject:str_regt_consumer_id];
    [gblclass.arr_values addObject:str_bill_id];
    [gblclass.arr_values addObject:lbl_customer_id];
    [gblclass.arr_values addObject:lbl_customer_nick];
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,
    //                                                                   [GlobalStaticClass getPublicIp],payfrom_acct_id,Payfrm_selected_item_text,Payfrm_selected_item_value,Amount,lbl_customer_id,
    //                                                                   lbl_compny_name,comment,str_access_key,@"UBLBP",@"",gblclass.base_currency,t_pin,str_TT_accesskey,str_tt_id,
    //                                                                   str_regt_consumer_id,str_bill_id,lbl_customer_id,lbl_customer_nick,uuid, nil] forKeys:
    //
    //                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
    //                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
    //                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"strtxtPin",
    //                                @"strTTAccessKey",@"strTT_ID",@"strRegisteredConsumersId",@"strBillId",@"strlbCustomerId",@"strlblConsumerNick",@"strGuid", nil]];
    //
    //
    //    //NSLog(@"%@",dictparam);
    
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:payfrom_acct_id],
                                [encrypt encrypt_Data:Payfrm_selected_item_text],
                                [encrypt encrypt_Data:Payfrm_selected_item_value],
                                [encrypt encrypt_Data:Amount],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:lbl_compny_name],
                                [encrypt encrypt_Data:comment],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:@"UBLBP"],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:t_pin],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:str_tt_id],
                                [encrypt encrypt_Data:str_regt_consumer_id],
                                [encrypt encrypt_Data:str_bill_id],
                                [encrypt encrypt_Data:lbl_customer_nick],
                                [encrypt encrypt_Data:uuid], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"strtxtPin",
                                                                   @"strTTAccessKey",
                                                                   @"strTT_ID",
                                                                   @"strRegisteredConsumersId",
                                                                   @"strBillId",
                                                                   @"strlblConsumerNick",
                                                                   @"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayUBLBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  //                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfull"
                  //                                                                  message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                  //                                                                 delegate:self
                  //                                                        cancelButtonTitle:@"Ok"
                  //                                                        otherButtonTitles:nil, nil];
                  //
                  //                  [alert show];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(void) Pay_Bill_Credit_Card:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_Get_UBLBill_load=[[NSMutableArray alloc] init];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    txt_t_pin.text=@"1111";
    //NSLog(@"%@", gblclass.user_id);
    //NSLog(@"%@", payfrom_acct_id);
    //NSLog(@"%@", Payfrm_selected_item_text);
    //NSLog(@"%@", Payfrm_selected_item_value);
    //NSLog(@"%@", str_amnt);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_compny_name);
    //NSLog(@"%@", txt_comment.text);
    //NSLog(@"%@", str_access_key);
    //NSLog(@"%@", str_Tc_access_key);//str_TT_accesskey
    //NSLog(@"%@", gblclass.base_currency);
    //NSLog(@"%@", txt_t_pin.text);
    //NSLog(@"%@", str_TT_accesskey);
    //NSLog(@"%@", str_tt_id);
    //NSLog(@"%@", str_regt_consumer_id);
    //NSLog(@"%@", str_bill_id);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_customer_nick);
    //NSLog(@"%@", uuid);
    
    
    NSString* t_pin;
    NSString* comment;
    NSString* Amount;
    
    
    if ([lbl_compny_name isEqualToString:@"UBL Drive"])
    {
        t_pin=@"1111"; //txt_t_pin_drive.text;
        comment=txt_comment.text;
        Amount=txt_amounts.text;
    }
    else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
    {
        t_pin=@"1111"; //txt_t_pin_credit.text;
        comment=txt_comment_credit.text;
        
        if ([txt_combo_payment.text isEqualToString:@"User Specified"])
        {
            Amount=txt_credit_amount.text;
        }
        else
        {
            Amount=lbl_credit_amount.text;
        }
        
    }
    else
    {
        t_pin=@"1111"; //txt_t_pin_drive.text;
        comment=txt_comment.text;
        Amount=txt_amounts.text;
    }
    
    // Comment Two times appear ::::
    
    //    if ([str_access_key isEqualToString:@"RBP"] || [str_access_key isEqualToString:@"rbp"])
    //    {
    //        str_access_key=@"";
    //        str_access_key=@"UBPBP";
    //    }
    
    
    [gblclass.arr_values addObject:gblclass.user_id];
    [gblclass.arr_values addObject:gblclass.M3sessionid];
    [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
    [gblclass.arr_values addObject:payfrom_acct_id];
    [gblclass.arr_values addObject:Payfrm_selected_item_text];
    [gblclass.arr_values addObject:Payfrm_selected_item_value];
    [gblclass.arr_values addObject:Amount];
    [gblclass.arr_values addObject:lbl_customer_id];
    [gblclass.arr_values addObject:lbl_compny_name];
    [gblclass.arr_values addObject:comment];
    [gblclass.arr_values addObject:str_access_key];
    [gblclass.arr_values addObject:@"UBLBP"];
    [gblclass.arr_values addObject:@""];
    [gblclass.arr_values addObject:gblclass.base_currency];
    [gblclass.arr_values addObject:@"1111"];
    [gblclass.arr_values addObject:str_TT_accesskey];
    [gblclass.arr_values addObject:str_tt_id];
    [gblclass.arr_values addObject:str_regt_consumer_id];
    [gblclass.arr_values addObject:str_bill_id];
    [gblclass.arr_values addObject:lbl_customer_nick];
    [gblclass.arr_values addObject:lbl_customer_id];
    
    
    
    //str_TT_accesskey
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:payfrom_acct_id],
                                [encrypt encrypt_Data:Payfrm_selected_item_text],
                                [encrypt encrypt_Data:Payfrm_selected_item_value],
                                [encrypt encrypt_Data:Amount],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:lbl_compny_name],
                                [encrypt encrypt_Data:comment],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:@"UBLBP"],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:t_pin],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:str_tt_id],
                                [encrypt encrypt_Data:str_regt_consumer_id],
                                [encrypt encrypt_Data:str_bill_id],
                                [encrypt encrypt_Data:lbl_customer_nick],
                                [encrypt encrypt_Data:uuid], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"strtxtPin",
                                                                   @"strTTAccessKey",
                                                                   @"strTT_ID",
                                                                   @"strRegisteredConsumersId",
                                                                   @"strBillId",
                                                                   @"strlblConsumerNick",
                                                                   @"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayUBLBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  //                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfull"
                  //                                                                  message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                  //                                                                 delegate:self
                  //                                                        cancelButtonTitle:@"Ok"
                  //                                                        otherButtonTitles:nil, nil];
                  //
                  //                  [alert show];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(void) Pay_Credit_Card_Frm_summary_VC:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_Get_UBLBill_load=[[NSMutableArray alloc] init];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    //NSLog(@"%@", gblclass.user_id);
    //NSLog(@"%@", payfrom_acct_id);
    //NSLog(@"%@", Payfrm_selected_item_text);
    //NSLog(@"%@", Payfrm_selected_item_value);
    //NSLog(@"%@", str_amnt);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_compny_name);
    //NSLog(@"%@", txt_comment.text);
    //NSLog(@"%@", str_access_key);
    //NSLog(@"%@", str_Tc_access_key);//str_TT_accesskey
    //NSLog(@"%@", gblclass.base_currency);
    //NSLog(@"%@", txt_t_pin.text);
    //NSLog(@"%@", str_TT_accesskey);
    //NSLog(@"%@", str_tt_id);
    //NSLog(@"%@", str_regt_consumer_id);
    //NSLog(@"%@", str_bill_id);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_customer_nick);
    //NSLog(@"%@", uuid);
    
    
    NSString* t_pin;
    NSString* comment;
    NSString* Amount;
    
    
    if ([lbl_compny_name isEqualToString:@"UBL Drive"])
    {
        t_pin=txt_t_pin_drive.text;
        comment=txt_comment.text;
        Amount=txt_amounts.text;
    }
    else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
    {
        t_pin=txt_t_pin_credit.text;
        comment=txt_comment_credit.text;
        
        if ([txt_combo_payment.text isEqualToString:@"User Specified"])
        {
            Amount=txt_credit_amount.text;
        }
        else
        {
            Amount=lbl_credit_amount.text;
        }
        
    }
    else
    {
        t_pin=txt_t_pin_drive.text;
        comment=txt_comment.text;
        Amount=txt_amounts.text;
    }
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:payfrom_acct_id],
                                [encrypt encrypt_Data:Payfrm_selected_item_text],
                                [encrypt encrypt_Data:Payfrm_selected_item_value],
                                [encrypt encrypt_Data:Amount],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:lbl_compny_name],
                                [encrypt encrypt_Data:comment],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:t_pin],
                                [encrypt encrypt_Data:str_TT_accesskey],
                                [encrypt encrypt_Data:str_tt_id],
                                [encrypt encrypt_Data:str_regt_consumer_id],
                                [encrypt encrypt_Data:str_bill_id],
                                [encrypt encrypt_Data:lbl_customer_nick],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:txt_comment_credit.text],
                                [encrypt encrypt_Data:uuid], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccountNumber",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strTxtComments",
                                                                   @"strCCY",
                                                                   @"strTxtTPin",
                                                                   @"PayCCFromAccountID",
                                                                   @"PayCCFromCCAccountID",
                                                                   @"strGuid",
                                                                   @"strAccessKey", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayUBLBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription] :@"0"];
              
          }];
}



-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//    if (buttonIndex == 0)
//    {
//        //Do something
//        //NSLog(@"1");
//    }
//    else if(buttonIndex == 1)
//    {
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
//        [self presentModalViewController:vc animated:YES];
//    }
//
//}

-(IBAction)btn_Utility_bill_names:(id)sender
{
    if (table_bill.isHidden==YES)
    {
        vw_bill.hidden=NO;
        table_bill.hidden=NO;
        vw_from.hidden=YES;
        table_from.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_bill.hidden=YES;
        vw_bill.hidden=YES;
        vw_table.hidden=YES;
    }
    
}


-(IBAction)btn_combo_frm:(id)sender
{
    
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    
    
    if (table_from.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=NO;
        table_payment.hidden=YES;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
        table_payment.hidden=YES;
    }
}

-(IBAction)btn_bill_names:(id)sender
{
    if (table_to.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=NO;
        table_to.hidden=NO;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        [table_to bringSubviewToFront:self.view];
        vw_table.hidden=NO;
    }
    else
    {
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=YES;
    }
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            return [search count];
        }
        else
        {
            return [arr_get_bill_load count];
        }
        //arr_UBP_bill
    }
    else if (tableView==table_payment)
    {
        return [arr_credit_min_full_payment count];
    }
    else
    {
        return [arr_bill_names count];
    }
    
    
    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        //        cell.textLabel.text = [split objectAtIndex:0];
        //        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
       // label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
       // label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        NSString *numberString = [NSString stringWithFormat:@"%@", [split objectAtIndex:6]];
        NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [numberFormatter stringFromNumber:decimalNumber];
        label.textColor=[UIColor whiteColor];
    //    label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        //        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        //        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        //        attach1.bounds = CGRectMake(10, 8, 20, 10);
        //        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        //
        //        NSString* balc;
        //
        //        if ([split objectAtIndex:6]==0)
        //        {
        //            balc=@"0";
        //        }
        //        else
        //        {
        //            balc=[split objectAtIndex:6];
        //        }
        //
        //
        //        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        //        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:16]};
        //        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        //
        //        [mutableAttriStr1 appendAttributedString:imageStr1];
        //       // cell.attributedText = mutableAttriStr1;
        //
        //        [cell.contentView addSubview:[NSString stringWithFormat:@"%@",mutableAttriStr1]];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if (tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [NSString stringWithFormat:@"%@  %@",[split_bill objectAtIndex:3],[split_bill objectAtIndex:2]];
            cell.textLabel.textColor=[UIColor whiteColor];
           // cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        else
        {
            split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [NSString stringWithFormat:@"%@  %@",[split_bill objectAtIndex:3],[split_bill objectAtIndex:2]];
            cell.textLabel.textColor=[UIColor whiteColor];
           // cell.textLabel.font=[UIFont systemFontOfSize:11];
        }
        
    }
    else if(tableView==table_payment)
    {
        split_credit = [[arr_credit_min_full_payment objectAtIndex:indexPath.row] componentsSeparatedByString: @""];
        cell.textLabel.text = [split_credit objectAtIndex:0];
        cell.textLabel.textColor=[UIColor whiteColor];
        
        if ([gblclass.story_board isEqualToString:@"iPhone_X"])
        {
             cell.textLabel.font=[UIFont systemFontOfSize:14];
        }
        else  if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
        {
             cell.textLabel.font=[UIFont systemFontOfSize:14];
        }
        else
        {
             cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
       
    }
    else
    {
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        cell.textLabel.text = [split_bill objectAtIndex:0];
        cell.textLabel.textColor=[UIColor whiteColor];
       // cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_payment.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [table_from reloadData];
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        txt_combo_frm.text=[split objectAtIndex:0];
        Payfrm_selected_item_text=[split objectAtIndex:0];
        Payfrm_selected_item_value=[split objectAtIndex:1];
        payfrom_acct_id=[split objectAtIndex:2];
        _lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        _lbl_balance.attributedText = mutableAttriStr1;
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
        
        
        //        table_from.hidden=YES;
        //        vw_from.hidden=YES;
        //        vw_credit_card.hidden=YES;
        
        
    }
    else if (tableView==table_to)
    {
        
        if ([search_flag isEqualToString:@"1"])
        {
            split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        }
        else
        {
            split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        }
        
        
        str_tt_name=[split_bill objectAtIndex:0];
        str_tt_id=[split_bill objectAtIndex:1];
        lbl_customer_id=[split_bill objectAtIndex:2];
        str_due_datt=[split_bill objectAtIndex:4];
        str_amnt=[split_bill objectAtIndex:5];
        status=[split_bill objectAtIndex:6];
        str_bill_id=[split_bill objectAtIndex:7];
        str_access_key=[split_bill objectAtIndex:8];
        str_Tc_access_key=[split_bill objectAtIndex:12];
        str_regt_consumer_id=[split_bill objectAtIndex:9];
        lbl_compny_name=[split_bill objectAtIndex:10];
        str_TT_accesskey=[split_bill objectAtIndex:12];
        lbl_customer_nick=[split_bill objectAtIndex:3];
        PAYMENT_STATUS=[split_bill objectAtIndex:13];
        IS_PAYBLE_1=[split_bill objectAtIndex:14];
        
        
        lbl_amnts.text=[split_bill objectAtIndex:5];
        //   lbl_due_dat.text=[split_bill objectAtIndex:4];
        lbl_status.text=[split_bill objectAtIndex:6];
        txt_combo_bill_names.text=[NSString stringWithFormat:@"%@  %@",[split_bill objectAtIndex:3],[split_bill objectAtIndex:2]];
        
        
        lbl_bill_type.text=[split_bill objectAtIndex:10];
        lbl_loan_acct.text=[split_bill objectAtIndex:2];
        
        vw_credit_card.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        txt_t_pin_drive.text=@"";
        txt_amounts.text=@"";
        txt_comment.text=@"";
        vw_table.hidden=YES;
        
        search_flag=@"0";
        [table_to reloadData];
        
        if ([lbl_compny_name isEqualToString:@"UBL Cashline"]) //UBL Drive
        {
            [self Get_Bill_Payment:lbl_customer_id];
        }
        else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
        {
            [hud showAnimated:YES];
            vw_credit_card.hidden=NO;
            [self Get_CreditCard_Bill_Payment:@""];
        }
        
        [gblclass.arr_bill_elements removeAllObjects];
        if ([[split_bill objectAtIndex:12] isEqualToString:@"OB"])
        {
            [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
            
            //NSLog(@" arr %@",gblclass.arr_bill_elements);
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"OB_bill"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else if ([[split_bill objectAtIndex:12] isEqualToString:@"ISP"])
        {
            [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
            
            //NSLog(@" arr %@",gblclass.arr_bill_elements);
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else if ([[split_bill objectAtIndex:12] isEqualToString:@"UBP"])
        {
            [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
            
            //NSLog(@" arr %@",gblclass.arr_bill_elements);
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        
        
    }
    else if(tableView==table_payment)
    {
        txt_combo_payment.text=[arr_credit_min_full_payment objectAtIndex:indexPath.row];
        vw_payment.hidden=YES;
        
        //arr_credit_min_full_payment
        
        //NSLog(@"%lu",(unsigned long)[arr_UBP_bill_payment count]);
        
        
        if ([[splits objectAtIndex:0]isEqualToString:@"UBL Cashline"]) //UBL Drive
        {
            
            if ([arr_UBP_bill_payment count]>0)
            {
                split_credit = [[arr_UBP_bill_payment objectAtIndex:0] componentsSeparatedByString: @"|"];
                
                //NSLog(@"%@",txt_combo_payment.text);
                
                
                if ([txt_combo_payment.text isEqualToString:@"Mimimum Payment Before Due Date"])
                {
                    if ([split_credit count]>0)
                    {
                        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                        lbl_due_dat.text=[split_credit objectAtIndex:0];
                        lbl_credit_amount.text=[split_credit objectAtIndex:2];
                        txt_credit_amount.hidden=YES;
                        lbl_credit_amount.hidden=NO;
                        
                        vw_table.hidden=YES;
                    }
                }
                else if ([txt_combo_payment.text isEqualToString:@"Mimimum Payment After Due Date"])
                {
                    if ([split_credit count]>0)
                    {
                        
                        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                        lbl_due_dat.text=[split_credit objectAtIndex:0];
                        lbl_credit_amount.text=[split_credit objectAtIndex:3];
                        txt_credit_amount.hidden=YES;
                        lbl_credit_amount.hidden=NO;
                        
                        vw_table.hidden=YES;
                    }
                }
                
                else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
                {
                    lbl_credit_amount.text=@"";
                    lbl_credit_amount.hidden=YES;
                    txt_credit_amount.hidden=NO;
                    vw_table.hidden=YES;
                }
            }
            
        }
        else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
        {
            
            if ([arr_UBLBP_creditCard count]>0)
            {
                split_credit   = [[arr_UBLBP_creditCard objectAtIndex:0] componentsSeparatedByString: @"|"];
                
                if ([split_credit count]>0)
                {
                    lbl_credit_amount.text=[split_credit objectAtIndex:2];
                    min_val=[split_credit objectAtIndex:2];
                    max_val=[split_credit objectAtIndex:3];
                }
                
                //NSLog(@"%@",txt_combo_payment.text);
                
                
                if ([lbl_credit_card_heading.text isEqualToString:@"CreditCard Account#"] ||  [lbl_bill_type_heading.text isEqualToString:@"UBL Credit Card Payment"])
                {
                    if ([txt_combo_payment.text isEqualToString:@"Min Payment"])
                    {
                        if ([split_credit count]>0)
                        {
                            lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                            lbl_due_dat.text=[split_credit objectAtIndex:0];
                            lbl_credit_amount.text=[split_credit objectAtIndex:2];
                            txt_credit_amount.hidden=YES;
                            lbl_credit_amount.hidden=NO;
                            
                            vw_table.hidden=YES;
                        }
                    }
                    else if ([txt_combo_payment.text isEqualToString:@"Full Payment"])
                    {
                        if ([split_credit count]>0)
                        {
                            
                            lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                            lbl_due_dat.text=[split_credit objectAtIndex:0];
                            lbl_credit_amount.text=[split_credit objectAtIndex:3];
                            txt_credit_amount.hidden=YES;
                            lbl_credit_amount.hidden=NO;
                            
                            vw_table.hidden=YES;
                        }
                    }
                    else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
                    {
                        lbl_credit_amount.hidden=YES;
                        txt_credit_amount.hidden=NO;
                        
                        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                        lbl_due_dat.text=[split_credit objectAtIndex:0];
                        lbl_credit_amount.text=@"";
                        min_val=[split_credit objectAtIndex:2];
                        max_val=[split_credit objectAtIndex:3];
                        
                        vw_table.hidden=YES;
                    }
                    
                }
                else
                {
                    
                    if ([txt_combo_payment.text isEqualToString:@"Mimimum Payment Before Due Date"])
                    {
                        if ([split_credit count]>0)
                        {
                            lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                            lbl_due_dat.text=[split_credit objectAtIndex:0];
                            lbl_credit_amount.text=[split_credit objectAtIndex:2];
                            txt_credit_amount.hidden=YES;
                            lbl_credit_amount.hidden=NO;
                            
                            vw_table.hidden=YES;
                        }
                    }
                    else if ([txt_combo_payment.text isEqualToString:@"Mimimum Payment After Due Date"])
                    {
                        if ([split_credit count]>0)
                        {
                            
                            lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                            lbl_due_dat.text=[split_credit objectAtIndex:0];
                            lbl_credit_amount.text=[split_credit objectAtIndex:3];
                            txt_credit_amount.hidden=YES;
                            lbl_credit_amount.hidden=NO;
                            
                            vw_table.hidden=YES;
                        }
                    }
                    else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
                    {
                        lbl_credit_amount.hidden=YES;
                        txt_credit_amount.hidden=NO;
                        
                        min_val=[split_credit objectAtIndex:2];
                        max_val=[split_credit objectAtIndex:3];
                        
                        vw_table.hidden=YES;
                    }
                }
            }
        }
    }
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_payment.separatorStyle = UITableViewCellSeparatorStyleNone;
    vw_table.hidden = YES;
    
}



-(IBAction)btn_back:(id)sender
{
    
    gblclass.direct_pay_frm_Acctsummary=@"0";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [txt_comment resignFirstResponder];
    
    
    //  vw_table.hidden=YES;
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_payment.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    
    [txt_combo_bill_names resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_amounts resignFirstResponder];
    [txt_comment_credit resignFirstResponder];
    [txt_t_pin_drive resignFirstResponder];
    [txt_credit_amount resignFirstResponder];
    [txt_t_pin_credit resignFirstResponder];
    
    //  [self keyboardWillHide];
}


#define kOFFSET_FOR_KEYBOARD 65.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [search  removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", substring];
    
    search= [NSMutableArray arrayWithArray: [arr_get_bill_load filteredArrayUsingPredicate:resultPredicate]];
    
    //    }
    
    
    table_to.hidden=NO;
    [table_to reloadData];
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    BOOL stringIsValid=NO;
    
    
    if ([theTextField isEqual:txt_amounts])
    {
        
        MAX_DIGITS=13;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_comment])
    {
        MAX_DIGITS = 50;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    if ([theTextField isEqual:txt_comment_credit])
    {
        MAX_DIGITS = 50;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    if ([theTextField isEqual:txt_credit_amount])
    {
        
        // MAX_DIGITS=4;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//
//        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_t_pin])
    {
        
        MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_combo_bill_names])
    {
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([theTextField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            
            search_flag=@"1";
            [self   searchAutocompleteEntriesWithSubstring:txt_combo_bill_names.text];
            
            //return [textField.text stringByReplacingCharactersInRange:range withString:set];
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            
            return 0;
        }
    }
    
    
    if ([theTextField isEqual:txt_t_pin_credit])
    {
        
        MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    if ([theTextField isEqual:txt_amounts])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
//        txt_amounts.text=formattedOutput;


        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_amounts.text = [nf stringFromNumber:myNumber];
        
    }
    
    
    if ([theTextField isEqual:txt_credit_amount])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        //        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        //        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText1 intValue]]];
        
        //  NSLog(@"%@",formattedOutput);
        //@(555555555555);
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        
        txt_credit_amount.text=[nf stringFromNumber:myNumber];
    }
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(IBAction)btn_credit_card:(id)sender
{
    
    [arr_credit_min_full_payment removeAllObjects];
    split_credit = [[arr_UBP_bill_payment objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    if ([lbl_Statement_Date compare:current_Date] == NSOrderedDescending)
    {
        //Before Due date :
        //NSLog(@"date1 is later than date2");
        
        txt_combo_payment.text=@"Mimimum Payment Before Due Date";
        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
        lbl_due_dat.text=[split_credit objectAtIndex:0];
        lbl_credit_amount.text=[split_credit objectAtIndex:2];
        txt_credit_amount.hidden=YES;
        lbl_credit_amount.hidden=NO;
        
        vw_table.hidden=YES;
        
        [arr_credit_min_full_payment addObject:@"Mimimum Payment Before Due Date"];
        [arr_credit_min_full_payment addObject:@"Mimimum Payment After Due Date"];
        [arr_credit_min_full_payment addObject:@"User Specified"];
        
    }
    else if ([lbl_Statement_Date compare:current_Date] == NSOrderedAscending)
    {
        //NSLog(@"date1 is earlier than date2");
        //After Due date :
        
        txt_combo_payment.text=@"Mimimum Payment After Due Date";
        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
        lbl_due_dat.text=[split_credit objectAtIndex:0];
        lbl_credit_amount.text=[split_credit objectAtIndex:3];
        txt_credit_amount.hidden=YES;
        lbl_credit_amount.hidden=NO;
        
        vw_table.hidden=YES;
        
        [arr_credit_min_full_payment addObject:@"Mimimum Payment Before Due Date"];
        [arr_credit_min_full_payment addObject:@"Mimimum Payment Before Due Date"];
        [arr_credit_min_full_payment addObject:@"User Specified"];
    }
    else
    {
        //Same Due date :
        //NSLog(@"dates are the same");
        
        //        txt_combo_payment.text=@"Mimimum Payment Before Due Date";
        lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
        lbl_due_dat.text=[split_credit objectAtIndex:0];
        //        lbl_credit_amount.text=[split_credit objectAtIndex:2];
        txt_credit_amount.hidden=YES;
        lbl_credit_amount.hidden=NO;
        
        vw_table.hidden=YES;
        
        
        if ([arr_UBLBP_creditCard count]>0)
        {
            split_credit   = [[arr_UBLBP_creditCard objectAtIndex:0] componentsSeparatedByString: @"|"];
            
            if ([split_credit count]>0)
            {
                
                // lbl_credit_amount.text=[split_credit objectAtIndex:2];
                min_val=[split_credit objectAtIndex:2];
                max_val=[split_credit objectAtIndex:3];
            }
            
            
            if ([lbl_credit_card_heading.text isEqualToString:@"CreditCard Account#"] ||  [lbl_bill_type_heading.text isEqualToString:@"UBL Credit Card Payment"])
            {
                //                if ([txt_combo_payment.text isEqualToString:@"Min Payment"])
                //                {
                if ([split_credit count]>0)
                {
                    lbl_statement_due_dat.text=[split_credit objectAtIndex:1];
                    lbl_due_dat.text=[split_credit objectAtIndex:0];
                    //                        lbl_credit_amount.text=[split_credit objectAtIndex:2];
                    //                        txt_credit_amount.hidden=YES;
                    //                        lbl_credit_amount.hidden=NO;
                }
                //}
            }
        }
        
        
        
        if ([lbl_credit_card_heading.text isEqualToString:@"CreditCard Account#"] ||  [lbl_bill_type_heading.text isEqualToString:@"UBL Credit Card Payment"])
        {
            
            [arr_credit_min_full_payment addObject:@"Min Payment"];
            [arr_credit_min_full_payment addObject:@"Full Payment"];
            [arr_credit_min_full_payment addObject:@"User Specified"];
        }
        else
        {
            [arr_credit_min_full_payment addObject:@"Mimimum Payment Before Due Date"];
            [arr_credit_min_full_payment addObject:@"Mimimum Payment After Due Date"];
            [arr_credit_min_full_payment addObject:@"User Specified"];
        }
        
    }
    
    
    
    [table_payment reloadData];
    vw_payment.hidden=NO;
    
    vw_table.hidden=NO;
    table_from.hidden=YES;
    table_payment.hidden=NO;
    
}

-(IBAction)btn_vw_hide:(id)sender
{
    //    txt_combo_payment.text=@"";
    vw_table.hidden=YES;
}

-(IBAction)btn_review:(id)sender
{
    
    @try {
        
        //*** Updated 7 feb 2017 ::
        
        [txt_combo_bill_names resignFirstResponder];
        [txt_comment resignFirstResponder];
        [txt_t_pin resignFirstResponder];
        [txt_amounts resignFirstResponder];
        [txt_comment_credit resignFirstResponder];
        [txt_t_pin_drive resignFirstResponder];
        [txt_credit_amount resignFirstResponder];
        [txt_t_pin_credit resignFirstResponder];
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"";
            [self SSL_Call];
        }
        
        
    } @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"please try again later." :@"0"];
    }
}






-(void) Pay_CC_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //  NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    
    NSString *amount = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    double amounts_roundup = ceil([amount doubleValue]);
    
    NSString* amounts=[NSString stringWithFormat:@"%.2f",amounts_roundup];
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[gblclass.arr_values objectAtIndex:0],[gblclass.arr_values objectAtIndex:1],
    //                                                                   [gblclass.arr_values objectAtIndex:2],[gblclass.arr_values objectAtIndex:3],[gblclass.arr_values objectAtIndex:4],[gblclass.arr_values objectAtIndex:5],amounts,[gblclass.arr_values objectAtIndex:7],
    //                                                                   [gblclass.arr_values objectAtIndex:8],[gblclass.arr_values objectAtIndex:9],[gblclass.arr_values objectAtIndex:10],[gblclass.arr_values objectAtIndex:11],[gblclass.arr_values objectAtIndex:12],[gblclass.arr_values objectAtIndex:13],[gblclass.arr_values objectAtIndex:10],@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil] forKeys:
    //
    //                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
    //                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
    //                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",
    //                                @"strlblTransactionType",@"strCCY",@"tcAccessKey",@"M3Key", nil]];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:gblclass.direct_pay_frm_Acctsummary_customer_id],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amount],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccountNumber",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strTxtComments",
                                                                   @"strCCY",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayCCBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  logout_chck=@"1";
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"lblReturnConfirmMessage"]] :@"0"];
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              {
                  [hud hideAnimated:YES];
                  UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                   message:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert1 show];
                  
                  //   [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"1"];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription] :@"0"];
              
          }];
}




-(void) Pay_Bills:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //  NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    
    NSString *amount = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    double amounts_roundup = ceil([amount doubleValue]);
    
    NSString* amounts=[NSString stringWithFormat:@"%.2f",amounts_roundup];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",@"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayUBLBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  logout_chck=@"1";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"1"];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription] :@"0"];
              
          }];
}


-(IBAction)btn_Edit_Bill:(id)sender
{
    gblclass.Edit_bill_type=@"UBL_BP";
    
    
    if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
    {
        gblclass.arr_bill_ublbp =[[NSMutableArray alloc] init];
        
        lbl_bill_type.text=@"UBL Credit Card Payment";
        lbl_loan_acct.text=gblclass.direct_pay_frm_Acctsummary_customer_id;
        lbl_compny_name=@"UBL Credit Card";
        
        str_TT_accesskey=@"UBLBP";
        
        str_access_key=@"CCP";
        str_tt_id=@"0";
        str_regt_consumer_id=gblclass.direct_pay_frm_Acctsummary_registed_customer_id;
        str_bill_id=@"0";
        lbl_customer_nick=gblclass.direct_pay_frm_Acctsummary_nick;
        
        [gblclass.arr_bill_ublbp addObject:str_tt_id];
        [gblclass.arr_bill_ublbp addObject:str_regt_consumer_id];
        [gblclass.arr_bill_ublbp addObject:lbl_bill_type.text];
        [gblclass.arr_bill_ublbp addObject:lbl_customer_nick];
        
        //NSLog(@"%lu",(unsigned long)[gblclass.arr_bill_ublbp count]);
        
    }
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"edit_bill"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_Delete_Bill:(id)sender
{
    UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Confirm Delete"
                                                     message:@"Are you sure, you want to delete?"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert3 show];
    
    // [self Delete_Bills:@""];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        if ([logout_chck isEqualToString:@"1"])
        {
            logout_chck=@"0";
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
            //[self mob_App_Logout:@""];
        }
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        
        [hud showAnimated:YES];
        
        [self Delete_Bills:@""];
        
        
        //        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        //        [self presentModalViewController:vc animated:YES];
    }
}



-(void) Delete_Bills:(NSString *)strIndustry
{
    
    @try {
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:str_tt_id],
                                    [encrypt encrypt_Data:lbl_customer_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                       @"tt_id",
                                                                       @"consumerNo",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strSessionId",
                                                                       @"IP", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"DeleteBills" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      
                      //                       [self custom_alert:@"Record Delete Successfully" :@"0"];
                      
                      UIAlertView *alert4 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:@"UBL Bill deleted successfully"
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert4 show];
                      
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  [self custom_alert:[error localizedDescription] :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        @try {
            
            
            if ([chk_ssl isEqualToString:@"load_creditCard_bill"])
            {
                chk_ssl=@"";
                
                [self Get_CreditCard_Bill_Payment:@""];
                
                return;
            }
            else if ([chk_ssl isEqualToString:@"load_ubl_cashline"])
            {
                chk_ssl=@"";
                
                [self Get_Bill_Payment:consumer_no];
                
                
                return;
            }
            else if ([chk_ssl isEqualToString:@"load_ubl_creditcard"])
            {
                chk_ssl=@"";
                
                [self Get_CreditCard_Bill_Payment:@""];
                
                return;
            }
            else if ([chk_ssl isEqualToString:@"logout"])
            {
                chk_ssl=@"";
                
                [self mob_App_Logout:@""];
                
                return;
            }
            
            
            
            
            
            [hud showAnimated:YES];
            
            NSString* t_pin;
            NSString* comment;
            NSString* Amount;
            
            
            [gblclass.arr_review_withIn_myAcct removeAllObjects];
            [gblclass.arr_values removeAllObjects];
            
            [gblclass.arr_review_withIn_myAcct addObject:lbl_customer_nick];  //**lbl_bill_type.text
            [gblclass.arr_review_withIn_myAcct addObject:lbl_loan_acct.text];
            [gblclass.arr_review_withIn_myAcct addObject:txt_combo_frm.text];
            [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
            
            
            if ([lbl_compny_name isEqualToString:@"UBL Drive"])
            {
                
                if (([txt_amounts.text isEqualToString:@""]))
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter your amount" :@"0"];
                    
                    return;
                }
                else if (([txt_amounts.text isEqualToString:@"0"]))
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter your valid amount" :@"0"];
                    
                    return;
                }
                else  if (([txt_comment.text isEqualToString:@""]))
                {
                    txt_comment.text=@"";
                }
                
                t_pin=@"1111";//txt_t_pin_drive.text;
                comment=txt_comment.text;
                Amount=txt_amounts.text;
                
                
                gblclass.check_review_acct_type=@"UBLBP";
                
                [gblclass.arr_review_withIn_myAcct addObject:Amount];
                [gblclass.arr_review_withIn_myAcct addObject:comment];
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:payfrom_acct_id];
                [gblclass.arr_values addObject:Payfrm_selected_item_text];
                [gblclass.arr_values addObject:Payfrm_selected_item_value];
                [gblclass.arr_values addObject:Amount];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_compny_name];
                [gblclass.arr_values addObject:comment];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_TT_accesskey];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:gblclass.base_currency];
                [gblclass.arr_values addObject:@"1111"];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_tt_id];
                [gblclass.arr_values addObject:str_regt_consumer_id];
                [gblclass.arr_values addObject:str_bill_id];
                [gblclass.arr_values addObject:lbl_customer_nick];
                
                
            }
            else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
            {
                
                NSString *originalString = lbl_credit_amount.text;
                NSMutableString *strippedString = [NSMutableString
                                                   stringWithCapacity:originalString.length];
                
                NSScanner *scanner = [NSScanner scannerWithString:originalString];
                NSCharacterSet *numbers = [NSCharacterSet
                                           characterSetWithCharactersInString:@"0123456789,."];
                
                while ([scanner isAtEnd] == NO)
                {
                    NSString *buffer;
                    
                    if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                    {
                        [strippedString appendString:buffer];
                        
                    }
                    else
                    {
                        [scanner setScanLocation:([scanner scanLocation] + 1)];
                    }
                }
                
                //NSLog(@"%@", strippedString); // "123123123"
                
                NSString *amounts_chk = [strippedString stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                t_pin=txt_t_pin_credit.text;
                comment=txt_comment_credit.text;
                
                if (([txt_combo_payment.text isEqualToString:@""]))
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Please select your payment type" :@"0"];
                    
                    return;
                }
                else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
                {
                    
                    if ([txt_credit_amount.text isEqualToString:@""])
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter your amount" :@"0"];
                        
                        return;
                    }
                    else if ([txt_credit_amount.text isEqualToString:@"0"])
                    {
                        [hud hideAnimated:YES];
                        
                        [self custom_alert:@"Enter your valid amount" :@"0"];
                        
                        return;
                    }
                    
                    // Minimum balc check remove
                    //            else if ([txt_credit_amount.text doubleValue] < [amounts_chk doubleValue])
                    //            {
                    //                [hud hideAnimated:YES];
                    //                 [self custom_alert:[NSString stringWithFormat:@"Minimum amount cannot be less then %@",min_val] :@"0"];
                    //
                    ////                UIAlertView* alert1=[[UIAlertView alloc] initWithTitle:@"ATTENTION" message:[NSString stringWithFormat:@"Minimum amount cannot be less then %@",min_val] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    ////
                    ////                [alert1 show];
                    //
                    //                return;
                    //            }
                    else if (![txt_credit_amount.text isEqualToString:@""])
                    {
                        
                        Amount=txt_credit_amount.text;
                    }
                    else if ([txt_comment_credit.text isEqualToString:@""])
                    {
                        //                [hud hideAnimated:YES];
                        
                        txt_comment_credit.text=@"";
                        
                    }
                    else
                    {
                        Amount=txt_credit_amount.text;
                    }
                    
                }
                //       else if ([txt_comment_credit.text isEqualToString:@""])
                //       {
                //           [hud hideAnimated:YES];
                //
                //           txt_comment_credit.text=@"";
                //
                ////           UIAlertView* alert1=[[UIAlertView alloc] initWithTitle:@"ATTENTION" message:@"Enter your comment" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                ////
                ////           [alert1 show];
                //
                ////           return;
                //       }
                else if (![lbl_credit_amount.text isEqualToString:@""])
                {
                    Amount=amounts_chk;
                }
                else
                {
                    Amount = strippedString;  //lbl_credit_amount.text;
                }
                
                
                
                gblclass.check_review_acct_type=@"UBLBP";
                
                [gblclass.arr_review_withIn_myAcct addObject:Amount];
                [gblclass.arr_review_withIn_myAcct addObject:comment];
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:payfrom_acct_id];
                [gblclass.arr_values addObject:Payfrm_selected_item_text];
                [gblclass.arr_values addObject:Payfrm_selected_item_value];
                [gblclass.arr_values addObject:Amount];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_compny_name];
                [gblclass.arr_values addObject:comment];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_TT_accesskey];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:gblclass.base_currency];
                [gblclass.arr_values addObject:@"1111"];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_tt_id];
                [gblclass.arr_values addObject:str_regt_consumer_id];
                [gblclass.arr_values addObject:str_bill_id];
                [gblclass.arr_values addObject:lbl_customer_nick];
                
                
                
            }
            else if ([lbl_compny_name isEqualToString:@"UBL Cashline"])
            {
                NSString *originalString = lbl_credit_amount.text;
                NSMutableString *strippedString = [NSMutableString
                                                   stringWithCapacity:originalString.length];
                
                NSScanner *scanner = [NSScanner scannerWithString:originalString];
                NSCharacterSet *numbers = [NSCharacterSet
                                           characterSetWithCharactersInString:@"0123456789,."];
                
                while ([scanner isAtEnd] == NO)
                {
                    NSString *buffer;
                    
                    if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                    {
                        [strippedString appendString:buffer];
                        
                    }
                    else
                    {
                        [scanner setScanLocation:([scanner scanLocation] + 1)];
                    }
                }
                
                //NSLog(@"%@", strippedString); // "123123123"
                Amount=strippedString;
                
                
                t_pin=txt_t_pin_credit.text;
                comment=txt_comment_credit.text;
                
                if (([txt_combo_payment.text isEqualToString:@""]))
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Please select your payment type" :@"0"];
                    
                    
                    return;
                }
                else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
                {
                    
                    
                    //  Amount=txt_credit_amount.text;
                    if ([txt_credit_amount.text isEqualToString:@""])
                    {
                        
                        [hud hideAnimated:YES];
                        
                        [self custom_alert:@"Enter your amount" :@"0"];
                        
                        return;
                    }
                    else if ([txt_credit_amount.text isEqualToString:@"0"])
                    {
                        
                        [hud hideAnimated:YES];
                        
                        [self custom_alert:@"Enter your valid amount" :@"0"];
                        
                        return;
                    }
                    else if (![txt_credit_amount.text isEqualToString:@""])
                    {
                        Amount=txt_credit_amount.text;
                    }
                    else if ([txt_comment_credit.text isEqualToString:@""])
                    {
                        
                        txt_comment_credit.text=@"";
                        comment=txt_comment_credit.text;
                        //                return;
                    }
                    else
                    {
                        Amount=txt_credit_amount.text;
                        
                    }
                    
                }
                else if ([txt_credit_amount.text isEqualToString:@""])
                {
                    txt_comment_credit.text=@"";
                    
                    
                }
                else if ([txt_credit_amount.text isEqualToString:@""])
                {
                    txt_credit_amount.text=@"";
                    Amount=txt_credit_amount.text;
                    
                    
                }
                else
                {
                    Amount=strippedString;  //lbl_credit_amount.text;
                }
                
                
                gblclass.check_review_acct_type=@"UBLBP";
                
                [gblclass.arr_review_withIn_myAcct addObject:Amount];
                [gblclass.arr_review_withIn_myAcct addObject:comment];
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:payfrom_acct_id];
                [gblclass.arr_values addObject:Payfrm_selected_item_text];
                [gblclass.arr_values addObject:Payfrm_selected_item_value];
                [gblclass.arr_values addObject:Amount];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_compny_name];
                [gblclass.arr_values addObject:comment];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_TT_accesskey];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:gblclass.base_currency];
                [gblclass.arr_values addObject:@"1111"];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_tt_id];
                [gblclass.arr_values addObject:str_regt_consumer_id];
                [gblclass.arr_values addObject:str_bill_id];
                [gblclass.arr_values addObject:lbl_customer_nick];
                
            }
            else
            {
                Amount=txt_amounts.text;
                t_pin=@"1111";//txt_t_pin_drive.text;
                comment=txt_comment.text;
                
                if ([txt_amounts.text isEqualToString:@""])
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter your amount" :@"0"];
                    
                    return;
                }
                else if ([txt_amounts.text isEqualToString:@"0"])
                {
                    
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter your valid amount" :@"0"];
                    
                    
                    return;
                }
                else   if ([txt_comment.text isEqualToString:@""])
                {
                    
                    txt_comment.text=@"";
                    
                }
                
                
                gblclass.check_review_acct_type=@"UBLBP";
                
                [gblclass.arr_review_withIn_myAcct addObject:Amount];
                [gblclass.arr_review_withIn_myAcct addObject:comment];
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:payfrom_acct_id];
                [gblclass.arr_values addObject:Payfrm_selected_item_text];
                [gblclass.arr_values addObject:Payfrm_selected_item_value];
                [gblclass.arr_values addObject:Amount];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_compny_name];
                [gblclass.arr_values addObject:comment];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_TT_accesskey];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:gblclass.base_currency];
                [gblclass.arr_values addObject:@"1111"];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_tt_id];
                [gblclass.arr_values addObject:str_regt_consumer_id];
                [gblclass.arr_values addObject:str_bill_id];
                [gblclass.arr_values addObject:lbl_customer_nick];
                
            }
            
            if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
            {
                //  [self  Pay_Credit_Card_Frm_summary_VC:@""];
                
                
                [self Pay_CC_Bill:@""];
                
                return;
            }
            else
            {
                [self Pay_Bills:@""];
            }
            
            
        }
        @catch (NSException *exception)
        {
            
            [hud hideAnimated:YES];
            //cantt pay
            //NSLog(@"cannt pay");
            
            [self custom_alert:@"Try again later." :@"0"];
            
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    @try {
        
        
        [self.connection cancel];
        
        if ([chk_ssl isEqualToString:@"load_creditCard_bill"])
        {
            chk_ssl=@"";
            //            [self.connection cancel];
            [self Get_CreditCard_Bill_Payment:@""];
            
            
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"load_ubl_cashline"])
        {
            chk_ssl=@"";
            //          [self.connection cancel];
            [self Get_Bill_Payment:consumer_no];
            
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"load_ubl_creditcard"])
        {
            chk_ssl=@"";
            
            //            [self.connection cancel];
            [self Get_CreditCard_Bill_Payment:@""];
            
            
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            //            [self.connection cancel];
            [self mob_App_Logout:@""];
            
            
            
            return;
        }
        
        
        
        //        [self.connection cancel];
        
        [hud showAnimated:YES];
        
        NSString* t_pin;
        NSString* comment;
        NSString* Amount;
        
        
        [gblclass.arr_review_withIn_myAcct removeAllObjects];
        [gblclass.arr_values removeAllObjects];
        
        [gblclass.arr_review_withIn_myAcct addObject:lbl_customer_nick];  //**lbl_bill_type.text
        [gblclass.arr_review_withIn_myAcct addObject:lbl_loan_acct.text];
        [gblclass.arr_review_withIn_myAcct addObject:txt_combo_frm.text];
        [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
        
        
        if ([lbl_compny_name isEqualToString:@"UBL Drive"])
        {
            
            if (([txt_amounts.text isEqualToString:@""]))
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Enter your amount" :@"0"];
                
                return;
            }
            else if (([txt_amounts.text isEqualToString:@"0"]))
            {
                
                [hud hideAnimated:YES];
                
                [self custom_alert:@"Enter your valid amount" :@"0"];
                return;
            }
            else  if (([txt_comment.text isEqualToString:@""]))
            {
                txt_comment.text=@"";
            }
            
            t_pin=@"1111";//txt_t_pin_drive.text;
            comment=txt_comment.text;
            Amount=txt_amounts.text;
            
            gblclass.check_review_acct_type=@"UBLBP";
            [gblclass.arr_review_withIn_myAcct addObject:Amount];
            [gblclass.arr_review_withIn_myAcct addObject:comment];
            
            [gblclass.arr_values addObject:gblclass.user_id];
            [gblclass.arr_values addObject:gblclass.M3sessionid];
            [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
            [gblclass.arr_values addObject:payfrom_acct_id];
            [gblclass.arr_values addObject:Payfrm_selected_item_text];
            [gblclass.arr_values addObject:Payfrm_selected_item_value];
            [gblclass.arr_values addObject:Amount];
            [gblclass.arr_values addObject:lbl_customer_id];
            [gblclass.arr_values addObject:lbl_compny_name];
            [gblclass.arr_values addObject:comment];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_TT_accesskey];
            [gblclass.arr_values addObject:@""];
            [gblclass.arr_values addObject:gblclass.base_currency];
            [gblclass.arr_values addObject:@"1111"];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_tt_id];
            [gblclass.arr_values addObject:str_regt_consumer_id];
            [gblclass.arr_values addObject:str_bill_id];
            [gblclass.arr_values addObject:lbl_customer_nick];
        }
        else if ([lbl_compny_name isEqualToString:@"UBL Credit Card"])
        {
            NSString *originalString = lbl_credit_amount.text;
            NSMutableString *strippedString = [NSMutableString
                                               stringWithCapacity:originalString.length];
            
            NSScanner *scanner = [NSScanner scannerWithString:originalString];
            NSCharacterSet *numbers = [NSCharacterSet
                                       characterSetWithCharactersInString:@"0123456789,."];
            
            while ([scanner isAtEnd] == NO)
            {
                NSString *buffer;
                
                if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                {
                    [strippedString appendString:buffer];
                }
                else
                {
                    [scanner setScanLocation:([scanner scanLocation] + 1)];
                }
            }
            
            //NSLog(@"%@", strippedString); // "123123123"
            
            NSString *amounts_chk = [strippedString stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            t_pin=txt_t_pin_credit.text;
            comment=txt_comment_credit.text;
            
            if (([txt_combo_payment.text isEqualToString:@""]))
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please select your payment type" :@"0"];
                return;
            }
            else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
            {
                if ([txt_credit_amount.text isEqualToString:@""])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Enter your amount" :@"0"];
                    return;
                }
                else if ([txt_credit_amount.text isEqualToString:@"0"])
                {
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter your valid amount" :@"0"];
                    return;
                }
                
                // Minimum balc check remove
                //            else if ([txt_credit_amount.text doubleValue] < [amounts_chk doubleValue])
                //            {
                //                [hud hideAnimated:YES];
                //                 [self custom_alert:[NSString stringWithFormat:@"Minimum amount cannot be less then %@",min_val] :@"0"];
                //
                ////                UIAlertView* alert1=[[UIAlertView alloc] initWithTitle:@"ATTENTION" message:[NSString stringWithFormat:@"Minimum amount cannot be less then %@",min_val] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                ////
                ////                [alert1 show];
                //
                //                return;
                //            }
                else if (![txt_credit_amount.text isEqualToString:@""])
                {
                    
                    Amount=txt_credit_amount.text;
                }
                else if ([txt_comment_credit.text isEqualToString:@""])
                {
                    //                [hud hideAnimated:YES];
                    
                    txt_comment_credit.text=@"";
                }
                else
                {
                    Amount=txt_credit_amount.text;
                }
            }
            //       else if ([txt_comment_credit.text isEqualToString:@""])
            //       {
            //           [hud hideAnimated:YES];
            //
            //           txt_comment_credit.text=@"";
            //
            ////           UIAlertView* alert1=[[UIAlertView alloc] initWithTitle:@"ATTENTION" message:@"Enter your comment" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            ////
            ////           [alert1 show];
            //
            ////           return;
            //       }
            else if (![lbl_credit_amount.text isEqualToString:@""])
            {
                Amount=amounts_chk;
            }
            else
            {
                Amount = strippedString;  //lbl_credit_amount.text;
            }
            
            
            gblclass.check_review_acct_type=@"UBLBP";
            
            [gblclass.arr_review_withIn_myAcct addObject:Amount];
            [gblclass.arr_review_withIn_myAcct addObject:comment];
            
            [gblclass.arr_values addObject:gblclass.user_id];
            [gblclass.arr_values addObject:gblclass.M3sessionid];
            [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
            [gblclass.arr_values addObject:payfrom_acct_id];
            [gblclass.arr_values addObject:Payfrm_selected_item_text];
            [gblclass.arr_values addObject:Payfrm_selected_item_value];
            [gblclass.arr_values addObject:Amount];
            [gblclass.arr_values addObject:lbl_customer_id];
            [gblclass.arr_values addObject:lbl_compny_name];
            [gblclass.arr_values addObject:comment];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_TT_accesskey];
            [gblclass.arr_values addObject:@""];
            [gblclass.arr_values addObject:gblclass.base_currency];
            [gblclass.arr_values addObject:@"1111"];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_tt_id];
            [gblclass.arr_values addObject:str_regt_consumer_id];
            [gblclass.arr_values addObject:str_bill_id];
            [gblclass.arr_values addObject:lbl_customer_nick];
            
            
            
        }
        else if ([lbl_compny_name isEqualToString:@"UBL Cashline"])
        {
            NSString *originalString = lbl_credit_amount.text;
            NSMutableString *strippedString = [NSMutableString
                                               stringWithCapacity:originalString.length];
            
            NSScanner *scanner = [NSScanner scannerWithString:originalString];
            NSCharacterSet *numbers = [NSCharacterSet
                                       characterSetWithCharactersInString:@"0123456789,."];
            
            while ([scanner isAtEnd] == NO)
            {
                NSString *buffer;
                
                if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
                {
                    [strippedString appendString:buffer];
                    
                }
                else
                {
                    [scanner setScanLocation:([scanner scanLocation] + 1)];
                }
            }
            
            //NSLog(@"%@", strippedString); // "123123123"
            Amount=strippedString;
            
            
            t_pin=txt_t_pin_credit.text;
            comment=txt_comment_credit.text;
            
            if (([txt_combo_payment.text isEqualToString:@""]))
            {
                
                [hud hideAnimated:YES];
                [self custom_alert:@"Please select your payment type" :@"0"];
                return;
            }
            else if ([txt_combo_payment.text isEqualToString:@"User Specified"])
            {
                
                
                //  Amount=txt_credit_amount.text;
                if ([txt_credit_amount.text isEqualToString:@""])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Enter your amount" :@"0"];
                    return;
                }
                else if ([txt_credit_amount.text isEqualToString:@"0"])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Enter your valid amount" :@"0"];
                    return;
                }
                else if (![txt_credit_amount.text isEqualToString:@""])
                {
                    Amount=txt_credit_amount.text;
                }
                else if ([txt_comment_credit.text isEqualToString:@""])
                {
                    txt_comment_credit.text=@"";
                    comment=txt_comment_credit.text;
                    //                return;
                }
                else
                {
                    Amount=txt_credit_amount.text;
                    
                }
                
            }
            else if ([txt_credit_amount.text isEqualToString:@""])
            {
                txt_comment_credit.text=@"";
            }
            else if ([txt_credit_amount.text isEqualToString:@""])
            {
                txt_credit_amount.text=@"";
                Amount=txt_credit_amount.text;
                
            }
            else
            {
                Amount=strippedString;  //lbl_credit_amount.text;
            }
            
            
            gblclass.check_review_acct_type=@"UBLBP";
            
            [gblclass.arr_review_withIn_myAcct addObject:Amount];
            [gblclass.arr_review_withIn_myAcct addObject:comment];
            
            [gblclass.arr_values addObject:gblclass.user_id];
            [gblclass.arr_values addObject:gblclass.M3sessionid];
            [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
            [gblclass.arr_values addObject:payfrom_acct_id];
            [gblclass.arr_values addObject:Payfrm_selected_item_text];
            [gblclass.arr_values addObject:Payfrm_selected_item_value];
            [gblclass.arr_values addObject:Amount];
            [gblclass.arr_values addObject:lbl_customer_id];
            [gblclass.arr_values addObject:lbl_compny_name];
            [gblclass.arr_values addObject:comment];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_TT_accesskey];
            [gblclass.arr_values addObject:@""];
            [gblclass.arr_values addObject:gblclass.base_currency];
            [gblclass.arr_values addObject:@"1111"];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_tt_id];
            [gblclass.arr_values addObject:str_regt_consumer_id];
            [gblclass.arr_values addObject:str_bill_id];
            [gblclass.arr_values addObject:lbl_customer_nick];
            
        }
        else
        {
            Amount=txt_amounts.text;
            t_pin=@"1111";//txt_t_pin_drive.text;
            comment = [txt_comment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            
            
            if ([txt_amounts.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Enter your amount" :@"0"];
                return;
            }
            else if ([txt_amounts.text isEqualToString:@"0"])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Enter your valid amount" :@"0"];
                return;
            }
            else   if ([txt_comment.text isEqualToString:@""])
            {
                txt_comment.text=@"";
            }
            
            
            gblclass.check_review_acct_type=@"UBLBP";
            
            [gblclass.arr_review_withIn_myAcct addObject:Amount];
            [gblclass.arr_review_withIn_myAcct addObject:comment];
            
            [gblclass.arr_values addObject:gblclass.user_id];
            [gblclass.arr_values addObject:gblclass.M3sessionid];
            [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
            [gblclass.arr_values addObject:payfrom_acct_id];
            [gblclass.arr_values addObject:Payfrm_selected_item_text];
            [gblclass.arr_values addObject:Payfrm_selected_item_value];
            [gblclass.arr_values addObject:Amount];
            [gblclass.arr_values addObject:lbl_customer_id];
            [gblclass.arr_values addObject:lbl_compny_name];
            [gblclass.arr_values addObject:comment];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_TT_accesskey];
            [gblclass.arr_values addObject:@""];
            [gblclass.arr_values addObject:gblclass.base_currency];
            [gblclass.arr_values addObject:@"1111"];
            [gblclass.arr_values addObject:str_access_key];
            [gblclass.arr_values addObject:str_tt_id];
            [gblclass.arr_values addObject:str_regt_consumer_id];
            [gblclass.arr_values addObject:str_bill_id];
            [gblclass.arr_values addObject:lbl_customer_nick];
            
        }
        
        if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
        {
            //  [self  Pay_Credit_Card_Frm_summary_VC:@""];
            
            
            [self Pay_CC_Bill:@""];
            
            return;
        }
        else
        {
            [self Pay_Bills:@""];
        }
        
        
    }
    @catch (NSException *exception)
    {
        
        //        [self.connection cancel];
        [hud hideAnimated:YES];
        //cantt pay
        //NSLog(@"cannt pay");
        
        [self custom_alert:@"Try again later." :@"0"];
        
    }
    
    //    [self.connection cancel];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

