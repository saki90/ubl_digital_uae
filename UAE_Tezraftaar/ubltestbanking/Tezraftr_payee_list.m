//
//  Tz_payees_VC.m
//  ubltestbanking
//
//  Created by Asim Khan on 2/12/19.
//  Copyright © 2019 ammar. All rights reserved.
//

#import "Tezraftr_payee_list.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Tezraftr_payee_list ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    UILabel* label;
    UIImageView* img;
    NSArray* split;
    NSArray* split_bill;
    NSString* img_name;
    NSArray* animalIndexTitles;
    NSArray* table_header;
    NSArray* data;
    NSMutableArray* arr_other_pay_list;
    NSMutableArray* a;
    NSDictionary* dic;
    UIAlertController *alert;
    NSMutableArray* A,*B, *C, *D,*E,*F,*G,*H, *I, *J, *K, *L, *M, *N, *O, *P, *Q, *R, *S, *T, *U, *V, *W, *X, *Y, *Z;
    NSString *ChkStr;
    UIStoryboard *storyboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    
    UIButton*  editbutton;
    NSString* cell_index;
    NSString* check_edit_btn;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Tezraftr_payee_list
@synthesize transitionController;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    
    //animalIndexTitles
    //gblclass.indexxx=@"234";
    ssl_count = @"0";
    check_edit_btn=@"0";
    
    A=[[NSMutableArray alloc] init];
    B=[[NSMutableArray alloc] init];
    C=[[NSMutableArray alloc] init];
    D=[[NSMutableArray alloc] init];
    E=[[NSMutableArray alloc] init];
    F=[[NSMutableArray alloc] init];
    G=[[NSMutableArray alloc] init];
    H=[[NSMutableArray alloc] init];
    I=[[NSMutableArray alloc] init];
    J=[[NSMutableArray alloc] init];
    K=[[NSMutableArray alloc] init];
    L=[[NSMutableArray alloc] init];
    M=[[NSMutableArray alloc] init];
    N=[[NSMutableArray alloc] init];
    O=[[NSMutableArray alloc] init];
    P=[[NSMutableArray alloc] init];
    Q=[[NSMutableArray alloc] init];
    R=[[NSMutableArray alloc] init];
    S=[[NSMutableArray alloc] init];
    T=[[NSMutableArray alloc] init];
    U=[[NSMutableArray alloc] init];
    V=[[NSMutableArray alloc] init];
    W=[[NSMutableArray alloc] init];
    X=[[NSMutableArray alloc] init];
    Y=[[NSMutableArray alloc] init];
    Z=[[NSMutableArray alloc] init];
    
    table.hidden=YES;
    gblclass.arr_payee_list=[[NSMutableArray alloc] init];
    
    // table_header=@[@"Utility Bill",@"Mobile Bill",@"UBL Bill",@"ISP Bill"];
    
    table_header = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
    
    //**   data = [gblclass.arr_other_pay_list sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    [self sortTableData];
    [table reloadData];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"viewDidAppear call");
    //**    [super viewWillDisappear:animated];
    
    check_edit_btn=@"0";
    table.hidden=NO;
    [table reloadData];
}

-(void)sortTableData {
    
    a =[[NSMutableArray alloc] init];
    
//    NSLog(@"%@",gblclass.tz_accounts);
    
    
    for (dic in gblclass.tz_accounts) {
        
        NSString* ACCOUNT_NO=[dic objectForKey:@"BeneID"]; //BeneAccountNo
        
        if (![ACCOUNT_NO isEqual:[NSNull null]]) {
            [a addObject:ACCOUNT_NO];
        }
        else {
            [a addObject:@"N/A"];
        }
        
        NSString* BRANCH_CODE=[dic objectForKey:@"BeneBank"];
        
        if (![BRANCH_CODE isEqual:[NSNull null]]) {
            [a addObject:BRANCH_CODE];
        } else {
            [a addObject:@"0000"];
        }
        
        NSString* BANK_NAME=[dic objectForKey:@"BeneBankName"];
        
        if (![BANK_NAME isEqual:[NSNull null]]) {
            [a addObject:BANK_NAME];
        }
        else {
            [a addObject:@"N/A"];
        }
        
        NSString* BANK_ID=[dic objectForKey:@"BeneID"];
        
        if (![BANK_ID isEqual:[NSNull null]]) {
            [a addObject:BANK_ID];
        }
        else {
            [a addObject:@"N/A"];
        }
        
        NSString* ACCOUNT_NAME=[dic objectForKey:@"BeneName"];
        
        if (![ACCOUNT_NAME isEqual:[NSNull null]]) {
            [a addObject:ACCOUNT_NAME];
        }
        else {
            [a addObject:@"N/A"];
        }
        
        NSString* MEMBERSHIP_ID=[dic objectForKey:@"MemberShipID"];
        
        if (![MEMBERSHIP_ID isEqual:[NSNull null]]) {
            [a addObject:MEMBERSHIP_ID];
        }
        else {
            [a addObject:@"N/A"];
        }
        
        NSString* BeneSerialNo=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BeneSerialNo"]];
        
        if (![BeneSerialNo isEqual:[NSNull null]]) {
            [a addObject:BeneSerialNo];
        }
        else
        {
            [a addObject:@"N/A"];
        }
        
        NSString* beneficiaryNick=[dic objectForKey:@"beneficiaryNick"];
      
        if (![beneficiaryNick isEqual:[NSNull null]] && (![beneficiaryNick  isEqual: @"null"]) && (!(beneficiaryNick == nil)))
        {
            [a addObject:beneficiaryNick];
        }
        else
        {
            [a addObject:ACCOUNT_NAME];
        }
        
        NSString* beneficiaryEmail=[dic objectForKey:@"beneficiaryEmail"];
        
        if (![beneficiaryEmail isEqual:[NSNull null]] && (![beneficiaryEmail  isEqual: @"null"]) && (!(beneficiaryEmail == nil))) {
            [a addObject:beneficiaryEmail];
        }
        else {
            [a addObject:@""];
        }
        
        NSString *bbb = [a componentsJoinedByString:@"|"];
        
        if (![ACCOUNT_NAME isEqual:[NSNull null]]) {
            ChkStr = [ACCOUNT_NAME substringWithRange:NSMakeRange(0, 1)];
        } else {
            ChkStr=@"N/A";
        }
        
        if ([ChkStr isEqualToString:@"A"] || [ChkStr isEqualToString:@"a"])
        {
            [A addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"B"] || [ChkStr isEqualToString:@"b"])
        {
            [B addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"C"] || [ChkStr isEqualToString:@"c"])
        {
            [C addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"D"] || [ChkStr isEqualToString:@"d"])
        {
            [D addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"E"] || [ChkStr isEqualToString:@"e"])
        {
            [E addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"F"] || [ChkStr isEqualToString:@"f"])
        {
            [F addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"G"] || [ChkStr isEqualToString:@"g"])
        {
            [G addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"H"] || [ChkStr isEqualToString:@"h"])
        {
            [H addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"I"] || [ChkStr isEqualToString:@"i"])
        {
            [I addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"J"] || [ChkStr isEqualToString:@"j"])
        {
            [J addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"K"] || [ChkStr isEqualToString:@"k"])
        {
            [K addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"L"] || [ChkStr isEqualToString:@"l"])
        {
            [L addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"M"] || [ChkStr isEqualToString:@"m"])
        {
            [M addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"N"] || [ChkStr isEqualToString:@"n"])
        {
            [N addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"O"] || [ChkStr isEqualToString:@"o"])
        {
            [O addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"P"] || [ChkStr isEqualToString:@"p"])
        {
            [P addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"Q"] || [ChkStr isEqualToString:@"q"])
        {
            [Q addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"R"] || [ChkStr isEqualToString:@"r"])
        {
            [R addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"S"] || [ChkStr isEqualToString:@"s"])
        {
            [S addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"T"] || [ChkStr isEqualToString:@"t"])
        {
            [T addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"U"] || [ChkStr isEqualToString:@"u"])
        {
            [U addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"V"] || [ChkStr isEqualToString:@"v"])
        {
            [V addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"W"] || [ChkStr isEqualToString:@"w"])
        {
            [W addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"X"] || [ChkStr isEqualToString:@"x"])
        {
            [X addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"Y"] || [ChkStr isEqualToString:@"y"])
        {
            [Y addObject:bbb];
        }
        else if ([ChkStr isEqualToString:@"Z"] || [ChkStr isEqualToString:@"z"])
        {
            [Z addObject:bbb];
        }
        else
        {
            [Z addObject:bbb];
        }
        
        //A,*B, *C, *D,*E,*F,*G,*H, *I, *J, *K, *L, *M, *N, *O, *P, *Q, *R, *S, *T, *U, *V, *W, *X, *Y, *Z;
        
        
        //**       [arr_other_pay_list addObject:bbb];
        //**       [gblclass.arr_other_pay_list addObject:bbb];
        
        //NSLog(@"%@", bbb);
        [a removeAllObjects];
        
    }
    
//    [table_header enumerateObjectsUsingBlock:^(NSString * _Nonnull x, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSString* filter = @"%K BEGINSWITH[c] %@";
//        NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, @"BeneName", x];
//
//        switch (idx) {
//            case 0:
//                A = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 1:
//                B = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 2:
//                C = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 3:
//                D = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 4:
//                E = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 5:
//                F = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 6:
//                G = [gblclass.tz_accounts   filteredArrayUsingPredicate:predicate];
//                break;
//            case 7:
//                H = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 8:
//                I = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 9:
//                J = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 10:
//                K = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 11:
//                L = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 12:
//                M = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 13:
//                N = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 14:
//                O = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 15:
//                P = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 16:
//                Q = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 17:
//                R = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 18:
//                S = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 19:
//                T = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 20:
//                U = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 21:
//                V = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 22:
//                W = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 23:
//                X = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 24:
//                Y = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            case 25:
//                Z = [gblclass.tz_accounts  filteredArrayUsingPredicate:predicate];
//                break;
//            default:
//                break;
//        }
//    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) slideLeft {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
    //saki 20 nov 2018
    //    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];//payee  pay_other_acct
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}


//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    CGRect frame = tableView.frame;
//
////    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(220, 10, 50, 30)]; //frame.size.width-60
////    addButton.titleLabel.text = @"+";
////    addButton.tintColor=[UIColor blackColor];
////    addButton.backgroundColor = [UIColor greenColor];
////
////    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(210, 10, 50, 30)];
////    title.text = @"Reminders";
////
////    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
////    [tableView addSubview:title];
////    [self.view addSubview:addButton];
//
////
////    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
////    button.titleLabel.text=@"Add Card";
////    [addButton addTarget:self action:@selector(SomeMethod:) forControlEvents:UIControlEventTouchUpInside];
////    return addButton;
////
////    return table.tableHeaderView;
//}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //    UIButton *addButton = [[UIButton alloc] init] ;//]WithFrame:CGRectMake(245, 5, 50, 30)];
    //
    //    addButton.titleLabel.text = @"Saqib Ahmed";
    //    addButton.backgroundColor = [UIColor redColor];
    //
    //
    //    [table.tableHeaderView addSubview:addButton];
    //
    //
    //    return table.tableHeaderView;
    
    
    
    
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(320.0, 0.0, 320.0, 20.0)];
    // myView.backgroundColor=[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0];
    
    myView.backgroundColor= [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    [label1 setFrame:CGRectMake(15.0, 0.0, 180.0, 30.0)];
    label1.text= [table_header objectAtIndex:section];
    label1.textColor=[UIColor blackColor];
    label1.font=[UIFont systemFontOfSize:12];
    label1.tag = section;
    label1.hidden = NO;
    [label1 setBackgroundColor:[UIColor clearColor]];
    // [button1 addTarget:self action:@selector(insertParameter:) forControlEvents:UIControlEventTouchDown];
    [myView addSubview:label1];
    
    
    return myView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (indexPath.section==0)
    {
        split_bill = [[A objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //    label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //     label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:101];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==1)
    {
        split_bill = [[B objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
    }
    else if (indexPath.section==2)
    {
        split_bill = [[C objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //     label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //     label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==3)
    {
        split_bill = [[D objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
       // label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
        
    }
    else if (indexPath.section==4)
    {
        split_bill = [[E objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==5)
    {
        split_bill = [[F objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
       // label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==6)
    {
        split_bill = [[G objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==7)
    {
        split_bill = [[H objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==8)
    {
        split_bill = [[I objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==9)
    {
        split_bill = [[J objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==10)
    {
        split_bill = [[K objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
       // label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //     label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==11)
    {
        split_bill = [[L objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
    }
    else if (indexPath.section==12)
    {
        split_bill = [[M objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        //label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:101];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
    }
    else if (indexPath.section==13)
    {
        split_bill = [[N objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==14)
    {
        split_bill = [[O objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //      label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==15)
    {
        split_bill = [[P objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
       // label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==16)
    {
        split_bill = [[Q objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==17)
    {
        split_bill = [[R objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==18)
    {
        split_bill = [[S objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
    }
    else if (indexPath.section==19)
    {
        split_bill = [[T objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==20)
    {
        split_bill = [[U objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==21)
    {
        split_bill = [[V objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
      //  label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==22)
    {
        split_bill = [[W objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==23)
    {
        split_bill = [[X objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==24)
    {
        split_bill = [[Y objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
        
    }
    else if (indexPath.section==25)
    {
        split_bill = [[Z objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:7]; //4
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
     //   label.text = [NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:1],[split_bill objectAtIndex:0]];
         label.text = [split_bill objectAtIndex:0];
        label.textColor=[UIColor blackColor];
        //       label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"Edit_payee.png"];
        [cell.contentView addSubview:img];
        
        editbutton=(UIButton*)[cell viewWithTag:100];
        
        [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:editbutton];
        
    }
    
    
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 414, 1)];/// change size as you need.
    
    separatorLineView.backgroundColor = [UIColor colorWithRed:198/255.0 green:197/255.0 blue:202/255.0 alpha:1.0];
    // you can also put image here
    [cell.contentView addSubview:separatorLineView];
    
    return cell;
}


- (void)yourButtonClicked:(UIButton *)sender
{
    
    @try {
        
        check_edit_btn=@"1";
        
        CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:table];
        NSIndexPath *indexPath1 = [table indexPathForRowAtPoint:buttonPosition1];
        [self tableView:table didSelectRowAtIndexPath:indexPath1];
        
        
//        NSLog(@"%@",gblclass.arr_payee_list);
//        NSLog(@"%@",gblclass.chk_payee_management_select);
        if ([gblclass.chk_payee_management_select isEqualToString:@"1"])
        {
            
            [gblclass.arr_payee_list removeAllObjects];
            split_bill=[[NSArray alloc] init];
            
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath1.row];
            
            if (indexPath1.section==0)
            {
                split_bill = [[A objectAtIndex:indexPath1.row] componentsSeparatedByString:@"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
            }
            else if (indexPath1.section==1)
            {
                split_bill = [[B objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==2)
            {
                split_bill = [[C objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==3)
            {
                split_bill = [[D objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
                
            }
            else if (indexPath1.section==4)
            {
                split_bill = [[E objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==5)
            {
                split_bill = [[F objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==6)
            {
                split_bill = [[G objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==7)
            {
                split_bill = [[H objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==8)
            {
                split_bill = [[I objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==9)
            {
                split_bill = [[J objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==10)
            {
                split_bill = [[K objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==11)
            {
                split_bill = [[L objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==12)
            {
                split_bill = [[M objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
                
            }
            else if (indexPath1.section==13)
            {
                split_bill = [[N objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==14)
            {
                split_bill = [[O objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==15)
            {
                split_bill = [[P objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==16)
            {
                split_bill = [[Q objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==17)
            {
                split_bill = [[R objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==18)
            {
                split_bill = [[S objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==19)
            {
                split_bill = [[T objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==20)
            {
                split_bill = [[U objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==21)
            {
                split_bill = [[V objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==22)
            {
                split_bill = [[W objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==23)
            {
                split_bill = [[X objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
            }
            else if (indexPath1.section==24)
            {
                split_bill = [[Y objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
            else if (indexPath1.section==25)
            {
                split_bill = [[Z objectAtIndex:indexPath1.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];
                
            }
        }
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"Edit_Tz"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [table_header indexOfObject:title];  //[animalIndexTitles indexOfObject:title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:  (NSInteger)section
{
    switch (section)
    {
        case 0:
            return [A count];
            break;
            
        case 1:
            return [B count];
            break;
            
        case 2:
            return [C count];
            break;
            
        case 3:
            return [D count];
            break;
            
        case 4:
            return [E count];
            break;
            
        case 5:
            return [F count];
            break;
            
        case 6:
            return [G count];
            break;
            
        case 7:
            return [H count];
            break;
            
        case 8:
            return [I count];
            break;
            
        case 9:
            return [J count];
            break;
            
        case 10:
            return [K count];
            break;
            
        case 11:
            return [L count];
            break;
            
        case 12:
            return [M count];
            break;
            
        case 13:
            return [N count];
            break;
            
        case 14:
            return [O count];
            break;
            
        case 15:
            return [P count];
            break;
            
        case 16:
            return [Q count];
            break;
            
        case 17:
            return [R count];
            break;
            
        case 18:
            return [S count];
            break;
            
        case 19:
            return [T count];
            break;
            
        case 20:
            return [U count];
            break;
            
        case 21:
            return [V count];
            break;
            
        case 22:
            return [W count];
            break;
            
        case 23:
            return [X count];
            break;
            
        case 24:
            return [Y count];
            break;
            
        case 25:
            return [Z count];
            break;
            
            
        default:
            return 0;
    }
    
    
    ////A,*B, *C, *D,*E,*F,*G,*H, *I, *J, *K, *L, *M, *N, *O, *P, *Q, *R, *S, *T, *U, *V, *W, *X, *Y, *Z;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [table_header count];//numberOfSections;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return table_header;
    //return animalIndexTitles;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //  UIButton *addButton;
    
    
    
    switch (section)
    {
        case 0:
            return [table_header objectAtIndex:section];
            break;
            
        case 1:
            return [table_header objectAtIndex:section];
            break;
            
        case 2:
            return [table_header objectAtIndex:section];
            break;
            
        case 3:
            return [table_header objectAtIndex:section];
            break;
            
        case 4:
            return [table_header objectAtIndex:section];
            break;
            
        case 5:
            return [table_header objectAtIndex:section];
            break;
            
        case 6:
            return [table_header objectAtIndex:section];
            break;
            
        case 7:
            return [table_header objectAtIndex:section];
            break;
            
        case 8:
            return [table_header objectAtIndex:section];
            break;
            
        case 9:
            return [table_header objectAtIndex:section];
            break;
            
        case 10:
            return [table_header objectAtIndex:section];
            break;
            
        case 11:
            return [table_header objectAtIndex:section];
            break;
            
        case 12:
            return [table_header objectAtIndex:section];
            break;
            
        case 13:
            return [table_header objectAtIndex:section];
            break;
            
        case 14:
            return [table_header objectAtIndex:section];
            break;
            
        case 15:
            return [table_header objectAtIndex:section];
            break;
            
        case 16:
            return [table_header objectAtIndex:section];
            break;
            
        case 17:
            return [table_header objectAtIndex:section];
            break;
            
        case 18:
            return [table_header objectAtIndex:section];
            break;
            
        case 19:
            return [table_header objectAtIndex:section];
            break;
            
        case 20:
            return [table_header objectAtIndex:section];
            break;
            
        case 21:
            return [table_header objectAtIndex:section];
            break;
            
        case 22:
            return [table_header objectAtIndex:section];
            break;
            
        case 23:
            return [table_header objectAtIndex:section];
            break;
            
        case 24:
            return [table_header objectAtIndex:section];
            break;
            
        case 25:
            return [table_header objectAtIndex:section];
            break;
            
            
        default:
            return 0;
    }
    
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        
        // split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
//        NSLog(@"%@",gblclass.chk_payee_management_select);
        if ([gblclass.chk_payee_management_select isEqualToString:@"0"])
        {
            
            [gblclass.arr_payee_list removeAllObjects];
            split_bill=[[NSArray alloc] init];
            
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            
            if (indexPath.section==0)
            {
                split_bill = [[A objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==1)
            {
                split_bill = [[B objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==2)
            {
                split_bill = [[C objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==3)
            {
                split_bill = [[D objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
                
            }
            else if (indexPath.section==4)
            {
                split_bill = [[E objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==5)
            {
                split_bill = [[F objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==6)
            {
                split_bill = [[G objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==7)
            {
                split_bill = [[H objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==8)
            {
                split_bill = [[I objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==9)
            {
                split_bill = [[J objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==10)
            {
                split_bill = [[K objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
                
            }
            else if (indexPath.section==11)
            {
                split_bill = [[L objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==12)
            {
                split_bill = [[M objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
                
            }
            else if (indexPath.section==13)
            {
                split_bill = [[N objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==14)
            {
                split_bill = [[O objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==15)
            {
                split_bill = [[P objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==16)
            {
                split_bill = [[Q objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==17)
            {
                split_bill = [[R objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==18)
            {
                split_bill = [[S objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
                
            }
            else if (indexPath.section==19)
            {
                split_bill = [[T objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==20)
            {
                split_bill = [[U objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==21)
            {
                split_bill = [[V objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==22)
            {
                split_bill = [[W objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==23)
            {
                split_bill = [[X objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==24)
            {
                split_bill = [[Y objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            else if (indexPath.section==25)
            {
                split_bill = [[Z objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:0]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:1]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:3]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:4]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:2]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:5]];
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:6]];//beneSrno
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:7]];//nick
                [gblclass.arr_payee_list addObject:[split_bill objectAtIndex:8]];//email
                
            }
            
//            NSLog(@"%@",gblclass.arr_payee_list);
            if ([check_edit_btn isEqualToString:@"0"])
            {
                gblclass.chk_tezraftar = @"1";
                
                CATransition *transition = [ CATransition animation];
                transition.duration = 0.3;
                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [self.view.window.layer addAnimation:transition forKey:nil];
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"trnsfer_tezraftaar_vc"];
                
                [self presentViewController:vc animated:NO completion:nil];
            }
            
        }
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



-(void) Get_Pay_Details:(NSString *)strIndustry
{
    
    
    @try {
        
        
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        gblclass.arr_get_pay_details=[[NSMutableArray alloc] init];
        arr_other_pay_list=[[NSMutableArray alloc] init];
        a=[[NSMutableArray alloc] init];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Device_ID",@"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"GetPayDetails" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  gblclass.arr_other_pay_list=[[NSMutableArray alloc] init];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [hud hideAnimated:YES];
//                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
//                      gblclass.arr_get_pay_details= [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbPayAnyOneList"];
//
//                      //                      NSUInteger *set;
//
//
//                      for (dic in gblclass.arr_get_pay_details)
//                      {
//
//                          // set = [gblclass.arr_get_pay_details indexOfObject:dic];
//
//
//
//
//
//
//                          NSString* USER_ACCOUNT_ID=[dic objectForKey:@"USER_ACCOUNT_ID"];
//
//
//                          //                      if (USER_ACCOUNT_ID==@"")
//                          //                      {
//                          //                          USER_ACCOUNT_ID=@"N/A";
//                          //                          [a addObject:USER_ACCOUNT_ID];
//                          //                      }
//                          //                      else
//                          //                      {
//
//
//                          [a addObject:USER_ACCOUNT_ID];
//                          //                      }
//
//
//
//                          NSString* TT_ACCESS_KEY=[dic objectForKey:@"TT_ACCESS_KEY"];
//                          //                      if (TT_ACCESS_KEY.length==0 || [TT_ACCESS_KEY isEqualToString:@" "] || [TT_ACCESS_KEY isEqualToString:nil])
//                          //                      {
//                          //                          TT_ACCESS_KEY=@"N/A";
//                          //                          [a addObject:TT_ACCESS_KEY];
//                          //                      }
//                          //                      else
//                          //                      {
//                          [a addObject:TT_ACCESS_KEY];
//                          //                      }
//
//                          NSString* ACCOUNT_NO=[dic objectForKey:@"ACCOUNT_NO"];
//                          //                      if ([ACCOUNT_NO isEqualToString:@""])
//                          //                      {
//                          //                          ACCOUNT_NO=@"N/A";
//                          //                          [a addObject:ACCOUNT_NO];
//                          //                      }
//                          //                      else
//                          //                      {
//                          [a addObject:ACCOUNT_NO];
//                          //                      }
//
//                          //BANK_CODE
//
//                          NSString* BANK_CODE=[dic objectForKey:@"BANK_CODE"];
//                          //                      if ([ACCOUNT_NO isEqualToString:@""])
//                          //                      {
//                          //                          ACCOUNT_NO=@"N/A";
//                          //                          [a addObject:ACCOUNT_NO];
//                          //                      }
//                          //                      else
//                          //                      {
//                          [a addObject:BANK_CODE];
//                          //                      }
//
//
//                          NSString* BRANCH_CODE=[dic objectForKey:@"BRANCH_CODE"];
//                          //                      if ([BRANCH_CODE isEqual:@""])
//                          //                      {
//                          //                          BRANCH_CODE=@"N/A";
//                          //                          [a addObject:BRANCH_CODE];
//                          //                      }
//                          //                      else
//                          //                      {
//                          if (![ACCOUNT_NAME isEqual:[NSNull null]])
//                          {
//                              [a addObject:BRANCH_CODE];
//                          }
//                          else
//                          {
//                              [a addObject:@"0000"];
//                          }
//
//                          //                      }
//
//                          NSString* BANK_IMD=[dic objectForKey:@"BANK_IMD"];
//                          //                      if ([BANK_IMD isEqual:@""])
//                          //                      {
//                          //                          BANK_IMD=@"N/A";
//                          //                          [a addObject:BANK_IMD];
//                          //                      }
//                          //                      else
//                          //                      {
//                          [a addObject:BANK_IMD];
//                          //                      }
//
//                          NSString* TYPE=[dic objectForKey:@"TYPE"];
//                          //                      if ([BANK_IMD isEqual:@""])
//                          //                      {
//                          //                          BANK_IMD=@"N/A";
//                          //                          [a addObject:BANK_IMD];
//                          //                      }
//                          //                      else
//                          //                      {
//                          [a addObject:TYPE];
//                          //                      }
//
//                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
//
//                          //                          if ([ACCOUNT_NO isEqualToString:@"03332392279"])
//                          //                          {
//                          //                              //NSLog(@"@skdjhfkjs");
//                          //                          }
//
//
//
//
//
//                          if ([ChkStr isEqualToString:@"A"] || [ChkStr isEqualToString:@"a"])
//                          {
//                              [A addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"B"] || [ChkStr isEqualToString:@"b"])
//                          {
//                              [B addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"C"] || [ChkStr isEqualToString:@"c"])
//                          {
//                              [C addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"D"] || [ChkStr isEqualToString:@"d"])
//                          {
//                              [D addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"E"] || [ChkStr isEqualToString:@"e"])
//                          {
//                              [E addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"F"] || [ChkStr isEqualToString:@"f"])
//                          {
//                              [F addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"G"] || [ChkStr isEqualToString:@"g"])
//                          {
//                              [G addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"H"] || [ChkStr isEqualToString:@"h"])
//                          {
//                              [H addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"I"] || [ChkStr isEqualToString:@"i"])
//                          {
//                              [I addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"J"] || [ChkStr isEqualToString:@"j"])
//                          {
//                              [J addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"K"] || [ChkStr isEqualToString:@"k"])
//                          {
//                              [K addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"L"] || [ChkStr isEqualToString:@"l"])
//                          {
//                              [L addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"M"] || [ChkStr isEqualToString:@"m"])
//                          {
//                              [M addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"N"] || [ChkStr isEqualToString:@"n"])
//                          {
//                              [N addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"O"] || [ChkStr isEqualToString:@"o"])
//                          {
//                              [O addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"P"] || [ChkStr isEqualToString:@"p"])
//                          {
//                              [P addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"Q"] || [ChkStr isEqualToString:@"q"])
//                          {
//                              [Q addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"R"] || [ChkStr isEqualToString:@"r"])
//                          {
//                              [R addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"S"] || [ChkStr isEqualToString:@"s"])
//                          {
//                              [S addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"T"] || [ChkStr isEqualToString:@"t"])
//                          {
//                              [T addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"U"] || [ChkStr isEqualToString:@"u"])
//                          {
//                              [U addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"V"] || [ChkStr isEqualToString:@"v"])
//                          {
//                              [V addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"W"] || [ChkStr isEqualToString:@"w"])
//                          {
//                              [W addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"X"] || [ChkStr isEqualToString:@"x"])
//                          {
//                              [X addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"Y"] || [ChkStr isEqualToString:@"y"])
//                          {
//                              [Y addObject:bbb];
//                          }
//                          else if ([ChkStr isEqualToString:@"Z"] || [ChkStr isEqualToString:@"z"])
//                          {
//                              [Z addObject:bbb];
//                          }
//                          else
//                          {
//                              [Z addObject:bbb];
//                          }
//
//                          //A,*B, *C, *D,*E,*F,*G,*H, *I, *J, *K, *L, *M, *N, *O, *P, *Q, *R, *S, *T, *U, *V, *W, *X, *Y, *Z;
//
//
//                          //**       [arr_other_pay_list addObject:bbb];
//                          //**       [gblclass.arr_other_pay_list addObject:bbb];
//
//                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];

                      }
                  
                      
                      [hud hideAnimated:YES];
                      table.hidden=NO;
                      [table reloadData];
                      
                      [self.connection cancel];
//                  }
                  //              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"])
                  //              {
                  //
                  //                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //                  //
                  //                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  //
                  //                  //                      [alert addAction:ok];
                  //                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                  [hud hideAnimated:YES];
                  //
                  //                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  //
                  //                  return ;
                  //              }
//                  else
//                  {
//                      
//                      [self.connection cancel];
//                      [hud hideAnimated:YES];
//                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//                  }
                  
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}







-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        //[self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window. layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}




-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}



-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_account:(id)sender
{
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_new_payee:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
        
        [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self Get_Pay_Details:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self Get_Pay_Details:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            [self custom_alert:statusString :@"0"];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end


