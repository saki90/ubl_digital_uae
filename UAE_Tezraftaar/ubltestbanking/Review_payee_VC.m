
//
//  Review_payee_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 08/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Review_payee_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Review_payee_VC ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    NSDictionary* dic;
    UIAlertController  *alert;
    NSString* privalages;
    NSString* transaction_type,*Str_IBFT_POT,*Str_IBFT_Relation;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSDateFormatter *dateFormatter;
    NSString *selectedDepositAccountId;
    NSString *selectedDepositAccountNumber;
    NSString *selectedDepositAccountName;
    NSString * vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Review_payee_VC
@synthesize transitionController;
@synthesize conversionRateLabel;
@synthesize serviceFeeLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    self.fbrView.hidden = YES;
     vw_down_chck=@"0";
    
    @try {
        
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        encrypt = [[Encrypt alloc] init];
        self.transitionController = [[TransitionDelegate alloc] init];
        self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        self.fbrView.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        self.vw_school.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        self.tezraftaarView.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        self.otpView.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEEE, dd MMM yyyy"];
        NSString *dateFormatter_frm = [dateFormatter stringFromDate:[NSDate date]];
        
        // NSString *dateFormatter_to = [dateFormatter stringFromDate:currentDate];
        txt_ft_msg.delegate = self;
        txt_ft_tax.delegate = self;
        txt_msg_sch.delegate = self;
        _tezraftaarDescriptionTextView.delegate = self;
        lbl_ft_msg1.delegate = self;
        
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textViewTapped:)];
        tap.delegate = self;
        tap.numberOfTapsRequired = 1;
        [txt_ft_msg addGestureRecognizer:tap];
        [txt_ft_tax addGestureRecognizer:tap];
        [txt_msg_sch addGestureRecognizer:tap];
        [_tezraftaarDescriptionTextView addGestureRecognizer:tap];
        [lbl_ft_msg addGestureRecognizer:tap];
        [lbl_ft_msg1 addGestureRecognizer:tap];
         
       
        
        ssl_count = @"0";
        txt_otp.delegate=self;
        txt_otp_sch.delegate=self;
        _tezraftaarOpt.delegate=self;
        lbl_datt.text=dateFormatter_frm;
        btn_ok.hidden=YES;
        
        lbl_otp.hidden=YES;
        txt_otp.hidden=YES;
        txt_otp.text=@"";
        _vw_school.hidden = YES;
        
        lbl_heading.text = @"REVIEW PAYMENT";
        
//        NSLog(@"%@",gblclass.check_review_acct_type);
        
        if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        {
            lbl_otp.hidden=NO;
            txt_otp.hidden=NO;
            
            
            //For school fee
            lbl_otp_sch.hidden=NO;
            txt_otp_sch.hidden=NO;
        }
        else
        {
            lbl_otp.hidden=YES;
            txt_otp.hidden=YES;
            
            //For school fee
            lbl_otp_sch.hidden=YES;
            txt_otp_sch.hidden=YES;
        }
        
//        NSLog(@"%@",gblclass.isFromOnlineShoping);
//        NSLog(@"%@",gblclass.reviewCommentFieldText);
        
        if ([gblclass.isFromOnlineShoping isEqualToString:@"true"])
        {
            lbl_comment_heading.text=@" Reference#";
        }
        else
        {
            lbl_comment_heading.text=@"Comment";
            //lbl_comment_heading.hidden = NO;
        }
        
        lbl_comment_heading.text = gblclass.reviewCommentFieldText;
        
//        NSLog(@"%@",gblclass.strReturnMessage);
        
       
        if ((!gblclass.ft_msg.length) == 0)
        {
            txt_ft_msg.text = gblclass.ft_msg;
        }
        
        if ((!gblclass.ft_tax.length) ==0)
        {
            txt_ft_tax.text = gblclass.ft_tax;
        }
        else
        {
             txt_ft_tax.text = @"";
        }
    
        
        if ([gblclass.check_review_acct_type isEqualToString:@"My_acct"])
        {
            // Within My ACCOUNT ::
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            //  lbl_name_to.font=  [UIFont fontWithName:@"Aspira Light" size:10];
            
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
            if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
            {
                lbl_otp.hidden=NO;
                txt_otp.hidden=NO;
            }
            else
            {
                lbl_otp.hidden=YES;
                txt_otp.hidden=YES;
            }
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Other_acct"])
        {
            if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
            {
                lbl_otp.hidden=NO;
                txt_otp.hidden=NO;
            }
            else
            {
                lbl_otp.hidden=YES;
                txt_otp.hidden=YES;
            }
            
            // Other Account ::
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:0] isEqual:@"ibft_pot"])
            {
                //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
                
                lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
                lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
                lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
                lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:4];
                lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:5]];
                
                lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:6];
//              lbl_trans_amt.text =
                
                if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:6] length]>0)
                {
                    lbl_comment_heading.text = @"Comment";
                }
                else
                {
                    lbl_comment_heading.hidden = YES;
                }
                
                if (![gblclass.strReturnMessage isEqual:[NSNull null]]) {
                    txt_msg.text = gblclass.strReturnMessage;
                }
            }
            else if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:0] isEqual:@"ibft relation & pot"])
            {
                lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
                lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
                lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
                lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:4];
                lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:5]];
                
                lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:6];
                
                if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:6] length]>0)
                {
                    lbl_comment_heading.text = @"Comment";
                }
                else
                {
                    lbl_comment_heading.text = @"Comment";
//                    lbl_comment_heading.hidden = YES;   saki 25 march 2019
                }
                
            }
            else if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:0] isEqual:@"purpose"])
            {
                //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
                
                lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
                lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
                lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
                lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:4];
                lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:6]];
                
                lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:7];
                
                if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:7] length]>0)
                {
                    lbl_comment_heading.text = @"Comment";
                }
                else
                {
                    lbl_comment_heading.hidden = NO;
                }
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Mobile_Topup"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Prepaid_voucher"])
        {
//            NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            //[gblclass.arr_review_withIn_myAcct objectAtIndex:1]
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:6];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Wiz"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Online_shopping"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @" Reference#";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"Utility_bill"])
        {
//            NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:5]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:6];
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:6] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"OB_bill"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            //44f9457735d218470947d584e794a4887ef9d607
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"isp"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"UBLBP"])
        {
            //NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code"])
        {
//            NSLog(@"%@",gblclass.arr_qr_code);
            
            //        NSString* SEP_PAN = @"00";
            //             NSString* SEP_MOBILE = @"01";
            //             NSString* SEP_ACCOUNT = @"08";
            //           //NSString* SEP_RFU = {"03","04","05","06","07","08","09"};
            //             NSString* SEP_MERCHANT_NAME = @"0A";
            //             NSString* SEP_MERCHANT_CATEGORY_CODE = @"0B";
            //             NSString* SEP_MERCHANT_CITY = @"0C";
            //             NSString* SEP_MERCHANT_COUNTRY_CODE = @"0D";
            //             NSString* SEP_MERCHANT_CURRENCY_CODE = @"0E";
            
            
            
            lbl_name_to.text=[gblclass.arr_qr_code objectAtIndex:2];
            lbl_name_To_acct_no.text=[gblclass.arr_qr_code objectAtIndex:1];
            //        lbl_name_frm.text=gblclass.is_default_acct_id_name;         //[gblclass.arr_qr_code objectAtIndex:2];
            //        lbl_name_frm_acct_no.text=gblclass.is_default_acct_no;      //[gblclass.arr_qr_code objectAtIndex:3];
            lbl_amnt.text=[gblclass.arr_qr_code objectAtIndex:9];
            lbl_comment.text=[gblclass.arr_qr_code objectAtIndex:10];
            
            if ([[gblclass.arr_qr_code objectAtIndex:10] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
            lbl_name_frm.text=[gblclass.arr_qr_code objectAtIndex:11];
            lbl_name_frm_acct_no.text=[gblclass.arr_qr_code objectAtIndex:12];
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code_p2p"])
        {
//            NSLog(@"%@",gblclass.arr_qr_code);
            
            //        NSString* SEP_PAN = @"00";
            //             NSString* SEP_MOBILE = @"01";
            //             NSString* SEP_ACCOUNT = @"08";
            //           //NSString* SEP_RFU = {"03","04","05","06","07","08","09"};
            //             NSString* SEP_MERCHANT_NAME = @"0A";
            //             NSString* SEP_MERCHANT_CATEGORY_CODE = @"0B";
            //             NSString* SEP_MERCHANT_CITY = @"0C";
            //             NSString* SEP_MERCHANT_COUNTRY_CODE = @"0D";
            //             NSString* SEP_MERCHANT_CURRENCY_CODE = @"0E";
            
            
            
            lbl_name_to.text=[gblclass.arr_qr_code objectAtIndex:1];
            lbl_name_To_acct_no.text=[gblclass.arr_qr_code objectAtIndex:2];
            //        lbl_name_frm.text=gblclass.is_default_acct_id_name;         //[gblclass.arr_qr_code objectAtIndex:2];
            //        lbl_name_frm_acct_no.text=gblclass.is_default_acct_no;      //[gblclass.arr_qr_code objectAtIndex:3];
            lbl_amnt.text=[gblclass.arr_qr_code objectAtIndex:3];
            lbl_comment.text=[gblclass.arr_qr_code objectAtIndex:4];
            
            lbl_name_frm.text=[gblclass.arr_qr_code objectAtIndex:5];
            lbl_name_frm_acct_no.text=[gblclass.arr_qr_code objectAtIndex:6];
            if ([[gblclass.arr_qr_code objectAtIndex:4] length]>0)
            {
                lbl_comment_heading.text = @"Comment";
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"franchise_payment"])
        {
//            NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            
            lbl_comment_heading.text = gblclass.deposit_slip;
            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
            {
                lbl_comment_heading.text = gblclass.deposit_slip;
            }
            else
            {
                lbl_comment_heading.hidden = YES;
            }
            
        }

        else if([gblclass.check_review_acct_type isEqualToString:@"school_fee"])
        {
//            NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
            
            
//            lbl_name_to.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
//            lbl_name_To_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
//            lbl_name_frm.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
//            lbl_name_frm_acct_no.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
//            lbl_amnt.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
//            lbl_comment.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
//
//            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0)
//            {
//                lbl_comment_heading.text = @"Comment";
//            }
//            else
//            {
//                lbl_comment_heading.hidden = YES;
//            }
            
            
            
            _vw_school.hidden = NO;
            _fbrView.hidden = YES;
            _tezraftaarView.hidden = YES;
            
            lbl_name_to_sch.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            lbl_name_To_acct_no_sch.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            lbl_name_frm_sch.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            lbl_name_frm_acct_no_sch.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            lbl_amnt_sch.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            lbl_comment_sch.text=[gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            lbl_std_id.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:6];
            lbl_voucher_id.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:7];
            
            if ([[gblclass.arr_review_withIn_myAcct objectAtIndex:5] length]>0) {
                lbl_comment_heading_sch.text = @"Comment";
            } else {
                lbl_comment_heading_sch.hidden = YES;
            }
            
        }
        else if([gblclass.check_review_acct_type isEqualToString:@"FBR"])

        {
            _fbrView.hidden = NO;
            _vw_school.hidden = YES;
            _tezraftaarView.hidden = YES;
            
            self.payToLabel.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            self.clientNameLabel.text = gblclass.fbrClientName;
            self.payFromLabel.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            selectedDepositAccountNumber = [gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            selectedDepositAccountId = [gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            
            self.amountLabel.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,gblclass.fbrAmount];
            self.dueDateLabel.text = gblclass.fbrDueDate;
            
            if ([gblclass.fbrOTPRequired isEqualToString:@"1"]) {
                self.otpView.hidden = NO;
            } else {
                self.otpView.hidden = YES;
            }
            
            self.descriptionTextView.text = [NSString stringWithFormat:@"%@\n%@",gblclass.fbrDescription, [self stringByStrippingHTML:gblclass.fbrTransctionAmount]];
            
        }
        else if ([gblclass.check_review_acct_type isEqualToString:@"tezraftaar"])
        {
            
            _fbrView.hidden = YES;
            _vw_school.hidden = YES;
            _tezraftaarView.hidden = NO;
            _tezraftaarDescriptionTextView.alwaysBounceVertical = NO;
            
//            NSLog(@"%@",gblclass.arr_review_withIn_myAcct);
//            NSLog(@"%d",[[[gblclass.tz_fees lastObject] objectForKey:@"MinFeeCharge"] intValue]);
            
//            NSLog(@"%@",[gblclass.arr_review_withIn_myAcct objectAtIndex:9]);
//            NSLog(@"%@",[gblclass.arr_review_withIn_myAcct objectAtIndex:4]);
            
//            float v = [[gblclass.arr_review_withIn_myAcct objectAtIndex:9]  floatValue]* [[gblclass.arr_review_withIn_myAcct objectAtIndex:4] floatValue];
            
//            NSLog(@"%f",[[gblclass.arr_review_withIn_myAcct objectAtIndex:4] floatValue]);
            NSString *str = [gblclass.arr_review_withIn_myAcct objectAtIndex:4];
            
            str = [str stringByReplacingOccurrencesOfString:@","
                                                 withString:@""];
//            NSLog(@"%@",str);
            
            CGFloat amnttt = [[gblclass.arr_review_withIn_myAcct objectAtIndex:9] floatValue];
//            CGFloat ratee = [[gblclass.arr_review_withIn_myAcct objectAtIndex:4] floatValue];
              CGFloat ratee = [str floatValue];
            
            CGFloat value = amnttt * ratee;
            
            NSString* g = [NSString stringWithFormat:@"%.02f", value];
            float theFloat = [g floatValue];
            int rounded_amnt = roundf(theFloat);
//            NSLog(@"%d",rounded_amnt);
            
            _amountInPkr.text = [NSString stringWithFormat:@"%d",rounded_amnt]; //[NSString stringWithFormat:@"PKR %.2f", value];
            
//            NSExpression *expression = [NSExpression expressionWithFormat:[NSString stringWithFormat:@"%@*%@",[gblclass.arr_review_withIn_myAcct objectAtIndex:9], [[gblclass.arr_review_withIn_myAcct objectAtIndex:4] stringByReplacingOccurrencesOfString:@"," withString:@""]]];
            
            
            _payToAccountName.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:0];
            _payToAccountNumber.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:1];
            _payFromAccountName.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:2];
            _payFromAccountNumber.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:3];
            _purposeLabel.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:6];
            _amountInAed.text = [NSString stringWithFormat:@"%@ %@", [gblclass.arr_review_withIn_myAcct objectAtIndex:11],[gblclass.arr_review_withIn_myAcct objectAtIndex:4]];
            _conversionRate.text = [NSString stringWithFormat:@"%@ %@", @"PKR", [gblclass.arr_review_withIn_myAcct objectAtIndex:9]];
//            NSLog(@"%@",gblclass.base_currency);
//           NSString* sty = [NSString stringWithFormat:@"%@ %@", gblclass.base_currency, [expression expressionValueWithObject:nil context:nil]];
            // _amountInPkr.text
           // _amountInPkr.text= [NSString stringWithFormat:@"%.2f", sty];
            
             serviceFeeLabel.text = [NSString stringWithFormat:@"%@ %@", [gblclass.arr_review_withIn_myAcct objectAtIndex:11], [gblclass.arr_review_withIn_myAcct objectAtIndex:8]];
            _date.text = lbl_datt.text;
            _comentsLabel.text = [gblclass.arr_review_withIn_myAcct objectAtIndex:5];
            
            if ([gblclass.tezraftaarOTPRequired isEqualToString:@"1"]) {
                _tezraftaarOtpHeading.hidden = NO;
                _tezraftaarOpt.hidden = NO;
            } else {
                _tezraftaarOtpHeading.hidden = YES;
                _tezraftaarOpt.hidden = YES;
            }
            
            if ([[[gblclass.arr_review_withIn_myAcct objectAtIndex:4] stringByReplacingOccurrencesOfString:@"," withString:@""] intValue] >= [[[gblclass.tz_fees lastObject] objectForKey:@"MinFeeCharge"] intValue])
            {
              
// saki 19 jun 2019               _conversionRate.hidden = YES;
//                _conversionRateLabel.hidden = YES;
                
                serviceFeeLabel.hidden = YES;
                _serviceFee.hidden = YES;
               // conversionRateLabel.hidden = YES;
                
                _lbl_service_fee_txt.hidden = YES;
                
            }
            else
            {
                _conversionRate.hidden = NO;
                _serviceFee.hidden = NO;
                 conversionRateLabel.hidden = NO;
                
            }
            
            _tezraftaarDescriptionTextView.text = gblclass.tezraftaarReturnMessage;
        }
        
        
        //        [APIdleManager sharedInstance].onTimeout = ^(void){
        //            //  [self.timeoutLabel  setText:@"YES"];
        //
        //
        //            //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        //
        //            storyboard = [UIStoryboard storyboardWithName:
        //                          gblclass.story_board bundle:[NSBundle mainBundle]];
        //            vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        //
        //
        //            [self presentViewController:vc animated:YES completion:nil];
        //
        //            APIdleManager * cc1=[[APIdleManager alloc] init];
        //            // cc.createTimer;
        //
        //            [cc1 timme_invaletedd];
        //
        //        };
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Try again later." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    //NSLog(@"First viewWillAppear");
//
//}

-(IBAction)btn_back:(id)sender {
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(IBAction)btn_close:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
    //    [self.presentingViewController.presentingViewController
    //     dismissViewControllerAnimated:YES completion:nil];
    
    
    //    [self dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
    
    
    //   [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)dealloc
{
    //NSLog(@"First Dealloc");
}

- (void)dismissViewControllerAnimated:(BOOL)flag
                           completion:(void (^ _Nullable)(void))completion
{
    //NSLog(@"%s ", __PRETTY_FUNCTION__);
    [super dismissViewControllerAnimated:flag completion:completion];
    //    [self dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
}


-(IBAction)btn_pay:(id)sender
{
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    
    [txt_otp resignFirstResponder];
    [txt_otp_sch resignFirstResponder];
    [_tezraftaarOpt resignFirstResponder];
    
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
}


-(void)QR_Transaction:(NSString *)strIndustry
{
    
    
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //NSLog(@"%@",gblclass.arr_qr_code);
    
    
    //    string a2 = gnbweb.QRTransaction(539152,                             //userID,
    //                                     "3710916",                            //strSessionId
    //                                     "1.1.1.1",                            //string IP,
    //                                     "QRCODE",                                 //string strAccessKey,
    //                                     968174,                              //Int64 payAnyOneFromAccountID,
    //                                     "Syed Bashir-Ul-Has",             //string cmbPayFromSelectedItemText,
    //                                     "060510947958",                       //string cmbPayFromSelectedItemValue,
    //                                     "588974",                             //string payAnyOneFromBankImd,
    //                                     "1",                                  //string strTxtAmount,
    //                                     "QRCODE testing ",                                  //string strTxtComments,
    //                                     "PKR",                               //string strCCY,
    //                                     "5295970000000031",                            //string QRCODEPAN,
    //                                     "",                                //string merchantMobile,
    //                                     "MErchnatNAme",                       //string MErchnatNAme,
    //                                     "UBL",                                //string strBankCode,
    //                                     out strReturnMessage);
    
    
    
    
    //    [gblclass.arr_qr_code addObject:str_Mobile];
    //    [gblclass.arr_qr_code addObject:str_Accout_no];
    //    [gblclass.arr_qr_code addObject:str_merchant_name];
    //    [gblclass.arr_qr_code addObject:str_category_code];
    //    [gblclass.arr_qr_code addObject:str_city];
    //    [gblclass.arr_qr_code addObject:str_country_code];
    //    [gblclass.arr_qr_code addObject:str_curr_code];
    
    
    //NSLog(@"%@",gblclass.is_default_acct_id);
    //NSLog(@"%@",gblclass.is_default_acct_id_name);
    //NSLog(@"%@",gblclass.is_default_acct_no);
    
//    NSLog(@"%@",gblclass.arr_qr_code);
    
    //pan code 5295970000000031
    
    //   5295970000000031   gblclass.is_default_acct_id   gblclass.is_default_acct_id_name
    NSString* amounts = [[gblclass.arr_qr_code objectAtIndex:9] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"QRCODE"],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:12]],
                                [encrypt encrypt_Data:@"588974"],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:3]],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code  objectAtIndex:14]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code  objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_qr_code  objectAtIndex:8]],
                                nil]
                               
                                                          forKeys: [NSArray arrayWithObjects:@"userID",
                                                                    @"strSessionId",
                                                                    @"IP",
                                                                    @"strAccessKey",
                                                                    @"payAnyOneFromAccountID",
                                                                    @"cmbPayFromSelectedItemText",
                                                                    @"cmbPayFromSelectedItemValue",
                                                                    @"payAnyOneFromBankImd",
                                                                    @"strTxtAmount",
                                                                    @"strTxtComments",
                                                                    @"strCCY",
                                                                    @"scanQRCodePan",
                                                                    @"merchantMobileNo",
                                                                    @"merchantTitle",
                                                                    @"strBankCode",
                                                                    @"strOTPPIN",
                                                                    @"QRPIN",
                                                                    @"Device_ID",
                                                                    @"Token",
                                                                    @"strQRCodeCRC",
                                                                    @"strMPQRCode",nil]];
    
    
    
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"QRTransaction_MPNew" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //                  NSError *error;
              //                  NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  txt_otp.hidden=YES;
                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
              }
              
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              }
              else if([[dic objectForKey:@"Response"] isEqualToString:@"-10"])
              {
                  [self clearOTP];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* aletAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                      [self slide_left];
                      [self dismissViewControllerAnimated:NO completion:nil];
                  }];
                  
                  [alert addAction:aletAction];
                  [self presentViewController:alert animated:YES completion:nil];
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
              //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Request Time Out." preferredStyle:UIAlertControllerStyleAlert];
              //
              //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //                  [alert addAction:ok];
              //                  [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}





-(void)P2P_Transaction:(NSString *)strIndustry
{
    @try {
        
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
//        NSLog(@"%@",gblclass.arr_qr_code);
        
        //pan code 5295970000000031
        
        // [gblclass.arr_qr_code objectAtIndex:8]  gblclass.instant_qr_pin
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
        //                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
        //                                    [encrypt encrypt_Data:gblclass.M3sessionid],
        //                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
        //                                    [encrypt encrypt_Data:@"QR_P2P"],
        //                                    [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:6]],
        //                                    [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:2]],
        //                                    [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:3]],
        //                                    [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:4]],
        //                                    [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]],
        //                                    [encrypt encrypt_Data:txt_otp.text],
        //                                    [encrypt encrypt_Data:gblclass.instant_qr_pin],
        //                                    [encrypt encrypt_Data:gblclass.Udid],
        //                                    [encrypt encrypt_Data:gblclass.token], nil]
        //
        //                                                              forKeys: [NSArray arrayWithObjects:@"userID",
        //                                                                        @"strSessionId",
        //                                                                        @"IP",
        //                                                                        @"strAccessKey",
        //                                                                        @"payAnyOneFromAccount",
        //                                                                        @"payAnyOneToAccount",
        //                                                                        @"strTxtAmount",
        //                                                                        @"strTxtComments",
        //                                                                        @"QR_ReferenceString",
        //                                                                        @"strOTPPIN",
        //                                                                        @"QRPIN",
        //                                                                        @"Device_ID",
        //                                                                        @"Token", nil]];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"QR_P2P"],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]],[encrypt encrypt_Data:txt_otp.text],[encrypt encrypt_Data:gblclass.instant_qr_pin],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token], nil] forKeys:
                                   [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccount",@"payAnyOneToAccount",@"strTxtAmount",@"strTxtComments",@"QR_ReferenceString",@"strOTPPIN",@"QRPIN",@"Device_ID",@"Token", nil]];
        
        
//        NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"P2PTransactionLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      //txt_otp.enabled=NO;
                      txt_otp.hidden=YES;
                      txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                  }
                  
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-1"]) {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      //                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      UIAlertController *alertView=[UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      UIAlertAction *defaultAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                          [self slide_right];
                          [self dismissViewControllerAnimated:NO completion:nil];
                          
                      }];
                      
                      [alertView addAction:defaultAction];
                      [self presentViewController:alertView animated:YES completion:nil];
                      
                      //                      return ;
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      //                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  [hud hideAnimated:YES];
                  
                  
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Request Time Out." preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}







-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    //    NSRange r;
    //    NSString *s = txt; //[self copy];
    //    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
    //        s = [s stringByReplacingCharactersInRange:r withString:@""];
    //    }
    //
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithData:[txt dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    NSString *s = attributedStr.string;
    
    return s;
    
}




-(void) Pay_Bill_Onlineshopping:(NSString *)strIndustry
{
    
    @try {
        
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSString *uuid = [[NSUUID UUID] UUIDString];
        
        
        //NSLog(@"%@",[gblclass.arr_values objectAtIndex:10]);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                       [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                       [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                    @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                    @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"tcAccessKey",@"M3Key", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"PayBill" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //               NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
                  transaction_type=[dic objectForKey:@"lblTransactionType"];
                  
                  
                  
                  //  [hud hideAnimated:YES];
                  
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      //                      [hud hideAnimated:YES];
                      //
                      //                      [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                      
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                      [alert addAction:ok];
                      //
                      //                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      
                      if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                      {
                          lbl_otp.enabled=YES;
                          txt_otp.enabled=YES;
                      }
                      else
                      {
                          lbl_otp.enabled=NO;
                          //txt_otp.enabled=NO;
                          txt_otp.hidden=YES;
                          txt_otp.text=@"";
                      }
                      
                      [self pay_Bill_Confirm_Submit:@""];
                      
                      
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  //   [hud hideAnimated:YES];
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception.name  preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}


-(void) pay_Bill_Confirm_Submit:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:@"1111"],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                [encrypt encrypt_Data:lbl_comment.text],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:@"dfg"],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys: [NSArray arrayWithObjects:@"strUserId",
                                                                    @"strSessionId",
                                                                    @"IP",
                                                                    @"strtxtPin",
                                                                    @"strTTAccessKey",
                                                                    @"strTCAccessKey",
                                                                    @"strTT_ID",
                                                                    @"strRegisteredConsumersId",
                                                                    @"strBillId",
                                                                    @"strlblCustomerID",
                                                                    @"strlblConsumerNick",
                                                                    @"payFromAccountID",
                                                                    @"cmbPayFromSelectedItemText",
                                                                    @"cmbPayFromSelectedItemValue",
                                                                    @"strTxtAmount",
                                                                    @"strTxtComments",
                                                                    @"strCCY",
                                                                    @"lblTransactionType",
                                                                    @"strGuid",
                                                                    @"strOTPPIN",
                                                                    @"Device_ID",
                                                                    @"Token",
                                                                    @"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"payBillConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  gblclass.Is_Otp_Required=@"0";
                  
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  txt_otp.hidden=YES;
                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  //  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
    {
        return;
    }
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
    }
    else if(buttonIndex == 0)
    {
        if ( [gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
        {
            gblclass.direct_pay_frm_Acctsummary=@"0";
            [self slide_right];
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else
        {
            gblclass.payment_login_chck=@"0";
            if ([chk_ssl isEqualToString:@"logout"])
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"logout";
                    [self SSL_Call];
                }
            }
            else
            {
                [self slide_right];
              //  [self dismissModalStack];
                
                [alertView show];
                [self slide_right];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                [self presentViewController:vc animated:NO completion:nil];
            }
        }
    }
}


-(void) pay_Bill_Confirm_Submit_Utility:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
//    NSLog(@"%@",gblclass.arr_values);
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:@"1111"],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:lbl_comment.text],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:gblclass.rev_transaction_type],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys: [NSArray arrayWithObjects:@"strUserId",
                                                                    @"strSessionId",
                                                                    @"IP",
                                                                    @"strtxtPin",
                                                                    @"strTTAccessKey",
                                                                    @"strTCAccessKey",
                                                                    @"strTT_ID",
                                                                    @"strRegisteredConsumersId",
                                                                    @"strBillId",
                                                                    @"strlblCustomerID",
                                                                    @"strlblConsumerNick",
                                                                    @"payFromAccountID",
                                                                    @"cmbPayFromSelectedItemText",
                                                                    @"cmbPayFromSelectedItemValue",
                                                                    @"strTxtAmount",
                                                                    @"strTxtComments",
                                                                    @"strCCY",
                                                                    @"lblTransactionType",
                                                                    @"strGuid",
                                                                    @"strOTPPIN",
                                                                    @"Device_ID",
                                                                    @"Token",
                                                                    @"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"payAllBillConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
              // gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  gblclass.Is_Otp_Required=@"0";
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  txt_otp.hidden=YES;
                  //txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  txt_ft_msg.text = [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  
                  _tezraftaarOtpHeading.hidden = YES;
                  lbl_otp_sch.hidden = YES;
                  lbl_otp.hidden = YES;
                  
                  //  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  txt_otp.text=@"";
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                  
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
         {
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:Call_center_msg :@"0"];
//              [self custom_alert:@"Retry" :@"0"];
          }];
}
 

-(void) pay_All_Bill_Confirm_Submit:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    //  //NSLog(@"%@",[gblclass.arr_values objectAtIndex:20]);
    
    
    NSString *amounts;
    if([gblclass.check_review_acct_type isEqualToString:@"Prepaid_voucher"])
    {
        amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        amounts=[NSString stringWithFormat:@"%@|%@",amounts,gblclass.ppv_domination_id];
    }
    else
    {
        amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    }
    
    
    //saki
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:11]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:12]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:13]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:14]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:15]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:16]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:17]);
    //    NSLog(@"%@",[gblclass.arr_values objectAtIndex:18]);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:@"1111"],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:lbl_comment.text],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:gblclass.rev_transaction_type],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strtxtPin",
                                                                   @"strTTAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strTT_ID",
                                                                   @"strRegisteredConsumersId",
                                                                   @"strBillId",
                                                                   @"strlblCustomerID",
                                                                   @"strlblConsumerNick",
                                                                   @"payFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strTxtComments",
                                                                   @"strCCY",
                                                                   @"lblTransactionType",
                                                                   @"strGuid",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"payAllBillConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  gblclass.Is_Otp_Required=@"0";
                  
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  lbl_otp.hidden=YES;
                  txt_otp.hidden=YES;
                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  
                  //[self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}





-(void) Pay_Confirm_Submit_CC:(NSString *)strIndustry
{
    
    
    @try {
        
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSString *uuid = [[NSUUID UUID] UUIDString];
        //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        
        
        NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                    [encrypt encrypt_Data:gblclass.direct_pay_frm_Acctsummary_customer_id],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]] ,
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                    [encrypt encrypt_Data:gblclass.direct_pay_frm_Acctsummary_registed_customer_id] ,
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                    [encrypt encrypt_Data:amounts],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:uuid],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                    [encrypt encrypt_Data:txt_otp.text],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                   
                                                              forKeys: [NSArray arrayWithObjects:@"strUserId",
                                                                        @"strSessionId",
                                                                        @"IP",
                                                                        @"strAccountNumber",
                                                                        @"strTxtTPin",
                                                                        @"PayCCFromAccountID",
                                                                        @"PayCCFromCCAccountID",
                                                                        @"cmbPayFromSelectedItemValue",
                                                                        @"strTxtAmount",
                                                                        @"strTxtComments",
                                                                        @"strCCY",
                                                                        @"strGuid",
                                                                        @"strAccessKey",
                                                                        @"strOTPPIN",
                                                                        @"Device_ID",
                                                                        @"Token",
                                                                        @"M3Key", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PayConfirmSubmitCC" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      gblclass.Is_Otp_Required=@"0";
                      
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      //txt_otp.enabled=NO;
                      txt_otp.hidden= YES;
                      lbl_otp.hidden = YES;
                      
//                      NSLog(@"%@",[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]);
                                   
                      txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                     
                      
                      txt_ft_msg.text = [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                      //[self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                      
                      //
                      //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                  [alert addAction:ok];
                      //                  [self presentViewController:alert animated:YES completion:nil];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}





-(void) pay_All_Bill_Confirm_Submit_UBLBP:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:@"1111"],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:lbl_comment.text],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:gblclass.rev_transaction_type],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys: [NSArray arrayWithObjects:@"strUserId",
                                                                    @"strSessionId",
                                                                    @"IP",
                                                                    @"strtxtPin",
                                                                    @"strTTAccessKey",
                                                                    @"strTCAccessKey",
                                                                    @"strTT_ID",
                                                                    @"strRegisteredConsumersId",
                                                                    @"strBillId",
                                                                    @"strlblCustomerID",
                                                                    @"strlblConsumerNick",
                                                                    @"payFromAccountID",
                                                                    @"cmbPayFromSelectedItemText",
                                                                    @"cmbPayFromSelectedItemValue",
                                                                    @"strTxtAmount",
                                                                    @"strTxtComments",
                                                                    @"strCCY",
                                                                    @"lblTransactionType",
                                                                    @"strGuid",
                                                                    @"strOTPPIN",
                                                                    @"Device_ID",
                                                                    @"Token",
                                                                    @"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"payAllBillConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  gblclass.Is_Otp_Required=@"0";
                  
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  txt_otp.hidden=YES;
                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  
                  //    [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
                  
                  //
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}





-(void) pay_All_Bill_Confirm_Submit_Wiz:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:7] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:0]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:1]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:2]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:3]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:4]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:5]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:6]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:7]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:8]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:9]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:10]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:12]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:13]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:14]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:15]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:16]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:17]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:18]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:19]);
    //    //NSLog(@"%@",[gblclass.arr_values objectAtIndex:20]);
    
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[gblclass.arr_values objectAtIndex:0],[gblclass.arr_values objectAtIndex:1],[gblclass.arr_values objectAtIndex:2],@"1111",[gblclass.arr_values objectAtIndex:3],[gblclass.arr_values objectAtIndex:13],[gblclass.arr_values objectAtIndex:14],[gblclass.arr_values objectAtIndex:15],[gblclass.arr_values objectAtIndex:16],[gblclass.arr_values objectAtIndex:17],[gblclass.arr_values objectAtIndex:18],[gblclass.arr_values objectAtIndex:4],[gblclass.arr_values objectAtIndex:5],[gblclass.arr_values objectAtIndex:6],amounts,lbl_comment.text,[gblclass.arr_values objectAtIndex:19],gblclass.rev_transaction_type,uuid,txt_otp.text,@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strtxtPin",@"strTTAccessKey",@"strTCAccessKey",@"strTT_ID",@"strRegisteredConsumersId",@"strBillId",@"strlblCustomerID",@"strlblConsumerNick",@"payFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strTxtAmount",@"strTxtComments",@"strCCY",@"lblTransactionType",@"strGuid",@"strOTPPIN",@"M3Key", nil]];
    
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:@"1111"],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:lbl_comment.text],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:@"dfg"],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:txt_otp.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strtxtPin",
                                                                   @"strTTAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strTT_ID",
                                                                   @"strRegisteredConsumersId",
                                                                   @"strBillId",
                                                                   @"strlblCustomerID",
                                                                   @"strlblConsumerNick",
                                                                   @"payFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strTxtComments",
                                                                   @"strCCY",
                                                                   @"lblTransactionType",
                                                                   @"strGuid",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"payAllBillConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  gblclass.Is_Otp_Required=@"0";
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  txt_otp.hidden=YES;
                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  //[self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
                  
                  //
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}




//-(void) Pay_Account_Within:(NSString *)strIndustry
//{
//
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
//                                                                                   ]];//baseURL];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
//
//    NSString *uuid = [[NSUUID UUID] UUIDString];
//    NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
//
//
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[gblclass.arr_values objectAtIndex:0],[gblclass.arr_values objectAtIndex:1],[gblclass.arr_values objectAtIndex:2],[gblclass.arr_values objectAtIndex:3],[gblclass.arr_values objectAtIndex:4],[gblclass.arr_values objectAtIndex:5],[gblclass.arr_values objectAtIndex:6],[gblclass.arr_values objectAtIndex:7],[gblclass.arr_values objectAtIndex:8],[gblclass.arr_values objectAtIndex:9],[gblclass.arr_values objectAtIndex:10],[gblclass.arr_values objectAtIndex:11],[gblclass.arr_values objectAtIndex:12],[gblclass.arr_values objectAtIndex:13],[gblclass.arr_values objectAtIndex:14],[gblclass.arr_values objectAtIndex:15],[gblclass.arr_values objectAtIndex:16],[gblclass.arr_values objectAtIndex:13],@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil] forKeys:
//                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"payAnyOneAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",@"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",@"strEmail",@"strIBFTRelation",@"strIBFTPOT",@"tcAccessKey",@"M3Key", nil]];
//
//    //NSLog(@"%@",dictparam);
//
//    [manager.requestSerializer setTimeoutInterval:time_out];
//
//    [manager POST:@"PayAccount" parameters:dictparam
//
//          success:^(NSURLSessionDataTask *task, id responseObject) {
//              NSError *error;
//              NSArray *arr = (NSArray *)responseObject;
//              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
//
//              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
//              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
//              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
//              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
//              transaction_type=[dic objectForKey:@"lblTransactionType"];
//              Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
//              Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
//
//            //  [hud hideAnimated:YES];
//
//              if ([[dic objectForKey:@"Response"] integerValue]==0)
//              {
//
//                  //[hud hideAnimated:YES];
//
//                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
//
//                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                  [alert addAction:ok];
//
//                  [self presentViewController:alert animated:YES completion:nil];
//
//
//
//                  if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
//                  {
//                      lbl_otp.enabled=YES;
//                      txt_otp.enabled=YES;
//                  }
//                  else
//                  {
//                      lbl_otp.enabled=NO;
//                      txt_otp.enabled=NO;
//                  }
//
//                  [self pay_Confirm_Submit:@""];
//
//
//
//
//             //     [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
//
//
//              }
//              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
//              {
//                   [hud hideAnimated:YES];
//
//                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
//
//                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                  [alert addAction:ok];
//
//                  [self presentViewController:alert animated:YES completion:nil];
//
//              }
//              else
//              {
//
//                  [hud hideAnimated:YES];
//
//                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
//
//                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                  [alert addAction:ok];
//
//                  [self presentViewController:alert animated:YES completion:nil];
//              }
//
//             //  [hud hideAnimated:YES];
//
//          }
//
//          failure:^(NSURLSessionDataTask *task, NSError *error) {
//              // [mine myfaildata];
//
//
//              [hud hideAnimated:YES];
//
//              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
//
//              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//              [alert addAction:ok];
//              [self presentViewController:alert animated:YES completion:nil];
//
//          }];
//}
//


-(void) pay_Confirm_Submit:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    //  NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
//    NSLog(@"%@",gblclass.arr_values);
    
//    NSLog(@"%@",gblclass.rev_Str_IBFT_Relation);
//    NSLog(@"%@",gblclass.rev_Str_IBFT_POT);
   // NSLog(@"%@",[gblclass.arr_values objectAtIndex:23]);
//    NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    
//    NSLog(@"%@",[gblclass.arr_values objectAtIndex:11]);
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                                                   [encrypt encrypt_Data:amounts],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]], //gblclass.rev_transaction_type
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                                                   [encrypt encrypt_Data:lbl_comment.text],
                                                                   [encrypt encrypt_Data:gblclass.rev_Str_IBFT_Relation],
                                                                   [encrypt encrypt_Data:gblclass.rev_Str_IBFT_POT],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:23]],//23
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                                                   [encrypt encrypt_Data:uuid],
                                                                   [encrypt encrypt_Data:txt_otp.text],
                                                                   [encrypt encrypt_Data:gblclass.Udid],
                                                                   [encrypt encrypt_Data:gblclass.token],
                                                                   [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userID",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strCCY",
                                                                   @"payAnyOneFromAccountID",
                                                                   @"payAnyOneFromBankImd",
                                                                   @"payAnyOneToAccountID",
                                                                   @"payAnyOneToAccountNo",
                                                                   @"strBankCode",
                                                                   @"payAnyOneToBranchCode",
                                                                   @"lblTransactionType",
                                                                   @"strAccessKey",
                                                                   @"strTxtComments",
                                                                   @"strIBFTRelation",
                                                                   @"strIBFTPOT",
                                                                   @"Ibft_Int_Purpose_Codes",
                                                                   @"strtxtPin",
                                                                   @"strGuid",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"payConfirmSubmit" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              // NSError *error;
              // NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [gblclass.arr_payee_list removeAllObjects];
                  
                  btn_ok.hidden=NO;
                  btn_pay.hidden=YES;
                  btn_cancel.hidden=YES;
                  //txt_otp.enabled=NO;
                  lbl_otp.hidden=YES;
                  txt_otp.hidden=YES;
//                  txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  txt_ft_msg.text = [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  //  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}


-(void) Pay_CC_Bill:(NSString *)strIndustry
{
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *amount = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    double amounts_roundup = ceil([amount doubleValue]);
    NSString* amounts = [NSString stringWithFormat:@"%.2f",amounts_roundup];
  
//    NSLog(@"%@",gblclass.direct_pay_frm_Acctsummary_customer_id);
//    NSLog(@"%@",gblclass.arr_values);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:gblclass.direct_pay_frm_Acctsummary_customer_id],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccountNumber",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strTxtAmount",
                                                                   @"strTxtComments",
                                                                   @"strCCY",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayCCBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
 
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  chk_ssl=@"1";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [hud hideAnimated:YES];
                  
                  UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                   message:[self stringByStrippingHTML:[dic objectForKey:@"lblReturnConfirmMessage"]]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert1 show];
                  
//                  CATransition *transition = [CATransition animation];
//                  transition.duration = 0.3;
//                  transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                  transition.type = kCATransitionPush;
//                  transition.subtype = kCATransitionFromRight;
//                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"act_summary"];
//                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"lblReturnConfirmMessage"]] :@"0"];
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              {
                  [hud hideAnimated:YES];
                  UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                   message:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert1 show];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"act_summary"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription] :@"0"];
              
          }];
}


-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}


-(void) clearOTP
{
    txt_otp.text=@"";
}



//-(void) showAlert:(NSString *)exception : (NSString *) Title{
//
//    dispatch_async(dispatch_get_main_queue(),
//                   ^{
//                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
//                                                                        message:exception
//                                                                       delegate:self
//                                                              cancelButtonTitle:@"OK"
//                                                              otherButtonTitles:nil];
//                       [alert1 show];
//                   });
//
//}


-(void) Pay_Account_Other:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    //
    //    NSDictionary *dictparam1 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[gblclass.arr_values objectAtIndex:0],[gblclass.arr_values objectAtIndex:1],[gblclass.arr_values objectAtIndex:2],[gblclass.arr_values objectAtIndex:3],[gblclass.arr_values objectAtIndex:4],[gblclass.arr_values objectAtIndex:5],[gblclass.arr_values objectAtIndex:6],[gblclass.arr_values objectAtIndex:7],[gblclass.arr_values objectAtIndex:8],[gblclass.arr_values objectAtIndex:9],[gblclass.arr_values objectAtIndex:10],[gblclass.arr_values objectAtIndex:11],[gblclass.arr_values objectAtIndex:12],[gblclass.arr_values objectAtIndex:13],[gblclass.arr_values objectAtIndex:14],[gblclass.arr_values objectAtIndex:15],uuid,@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil] forKeys:
    //                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",
    //                                @"payAnyOneAccountID",@"cmbPayFromSelectedItemText",
    //                                @"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",
    //@"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",
    //                                @"strEmail",@"strIBFTRelation",@"strIBFTPOT",@"tcAccessKey",@"M3Key", nil]];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"payAnyOneAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",@"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",@"strEmail",@"strIBFTRelation",@"strIBFTPOT",@"tcAccessKey",@"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              transaction_type=[dic objectForKey:@"lblTransactionType"];
              Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
              Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
              
              //  [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
                  
                  if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                  {
                      lbl_otp.enabled=YES;
                      txt_otp.enabled=YES;
                  }
                  else
                  {
                      lbl_otp.enabled=NO;
                      //txt_otp.enabled=NO;
                      txt_otp.hidden=YES;
                  }
                  
                  [self pay_Confirm_Submit:@""];
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self.connection cancel];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self.connection cancel];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
          }];
}



-(void) Pay_Bill_Mobile_Topup:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"tcAccessKey",@"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayPrepaidBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              transaction_type=[dic objectForKey:@"lblTransactionType"];
              ;
              
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
          }];
}

-(void) Pay_Bill_Prepaid_voucher:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[gblclass.arr_values objectAtIndex:0],[gblclass.arr_values objectAtIndex:1],
    //                                                                   [gblclass.arr_values objectAtIndex:2],[gblclass.arr_values objectAtIndex:3],[gblclass.arr_values objectAtIndex:4],[gblclass.arr_values objectAtIndex:5],[gblclass.arr_values objectAtIndex:6],[gblclass.arr_values objectAtIndex:7],
    //                                                                   [gblclass.arr_values objectAtIndex:8],[gblclass.arr_values objectAtIndex:9],[gblclass.arr_values objectAtIndex:10],[gblclass.arr_values objectAtIndex:11],[gblclass.arr_values objectAtIndex:12],[gblclass.arr_values objectAtIndex:13],[gblclass.arr_values objectAtIndex:14],[gblclass.arr_values objectAtIndex:15],[gblclass.arr_values objectAtIndex:16],
    //                                                                   [gblclass.arr_values objectAtIndex:17],[gblclass.arr_values objectAtIndex:18],[gblclass.arr_values objectAtIndex:19],[gblclass.arr_values objectAtIndex:20],uuid, nil] forKeys:
    //
    //                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
    //                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
    //                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"strtxtPin",
    //                                @"strTTAccessKey",@"strTT_ID",@"strRegisteredConsumersId",@"strBillId",@"strlbCustomerId",@"strlblConsumerNick",@"strGuid", nil]];
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"tcAccessKey",@"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayPrepaidBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]) ;
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}


-(void) Pay_Bill_Wiz:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]]
                                                                   ,[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strTTAccessKey",@"strConsumersNo",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strTxtAmount",@"strTxtComments",@"strCCY",@"strRegisteredAccount",@"strCompanyName",@"tcAccessKey",@"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayWizTopup" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.direct_pay_frm_Acctsummary=@"0";
                      
                      UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Successfull"
                                                                       message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert2 show];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  }
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}




-(void) Pay_Bill_Utility:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],uuid, nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"strtxtPin",
                                @"strTTAccessKey",@"strTT_ID",@"strRegisteredConsumersId",@"strBillId",@"strlbCustomerId",@"strlblConsumerNick",@"strGuid", nil]];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}


-(void) Pay_Bill_Mobile:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],uuid, nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",@"strlblTransactionType",@"strCCY",@"strtxtPin",
                                @"strTTAccessKey",@"strTT_ID",@"strRegisteredConsumersId",@"strBillId",@"strlbCustomerId",@"strlblConsumerNick",@"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}



-(void) Pay_Bill_ISP:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:18]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:19]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],
                                                                   [encrypt encrypt_Data:uuid], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",
                                @"strSessionId",
                                @"IP",
                                @"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",
                                @"cmbPayFromSelectedItemValue",
                                @"strtxtAmount",
                                @"strlblCustomerID",
                                @"strlblCompanyName",
                                @"strtxtComments",
                                @"strAccessKey",
                                @"strTCAccessKey",
                                @"strlblTransactionType",
                                @"strCCY",
                                @"strtxtPin",
                                @"strTTAccessKey",
                                @"strTT_ID",
                                @"strRegisteredConsumersId",
                                @"strBillId",
                                @"strlbCustomerId",
                                @"strlblConsumerNick",
                                @"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                  
                  
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [self clearOTP];
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:[error localizedDescription]  :@"0"];
          }];
}


-(void)PayBillSubmitClick:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
//        Method name: PayBillSubmitClick
//        Method signature:
//        public string PayBillSubmitClick(Int64 strUserId, string strSessionId, string IP, Int64 PayFromAccountID, string cmbPayFromSelectedItemText, string cmbPayFromSelectedItemValue, string strtxtAmount, string strlblCustomerID, string strlblCompanyName, string strtxtComments, string strAccessKey, string strTCAccessKey, string strlblTransactionType, string strCCY, string tcAccessKey, string Device_ID, string Token, out string lblTransactionType, out string lblTransAmount, out string strReturnMessage, out string IsOtpRequired, out string OtpMessage)
//
//
//
//
//        Parameter Detail:
//
//    strUserId: UserID
//    strSessionId: User sessionID
//    IP: User IP address
//    strtxtComments: FBR PSID of user +
//    strTTAccessKey: provided in dataset from GetBillData +
//    strTxtAmount:amount fetched from GetFBRInquiry in dtFBR DataTable +
//    strCCY:PKR
//    strLblCompanyName: provided in dataset from GetBillData in column company_name +
//    cmbPayFromSelectedItemText: User selected payment debit account name +
//    cmbPayFromSelectedItemValue : User selected payment debit account no. +
        
//    strRegisteredAccount: registered debit account ID from debit account selected from combo +
//    Device_ID: system device ID +
//    Token: system generated token provided against device ID +
//
//        Sample Data:
//        string status22 = gnbweb.PayBillSubmitClick(539152, "12345678910", "69.96.99.66", 968174, "Syed Bashir-Ul-Has", "060510947958", "1000", "50", "FBR Payments", "test", "FBR", "TP", "FBR Payment", "PKR", "TP","","", out lblTransactionType, out lblTransAmount, out strReturnMessage, out isOtpReq, out OtpMessage);
//        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.fbrPSID],
                                    [encrypt encrypt_Data:gblclass.fbrTcAccessKey],
                                    [encrypt encrypt_Data:gblclass.fbrAmount],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:gblclass.fbrCompanyName],
                                    [encrypt encrypt_Data:[gblclass.arr_review_withIn_myAcct objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_review_withIn_myAcct objectAtIndex:2]],
                                    [encrypt encrypt_Data:gblclass.registd_acct_id_trans],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strtxtComments",
                                                                       @"strTTAccessKey",
                                                                       @"strTxtAmount",
                                                                       @"strCCY",
                                                                       @"strLblCompanyName",
                                                                       @"cmbPayFromSelectedItemText",
                                                                       @"cmbPayFromSelectedItemValue",
                                                                       @"strRegisteredAccount",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PayBillSubmitClick" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  [hud hideAnimated:YES];
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      [self pay_Bill_Confirm_Submit:@""];
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
        
    }
}

-(void)PayAllBillConfirmSubmit:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
//        Method name: payAllBillConfirmSubmit
//
//        Method signature:
//        public string payAllBillConfirmSubmit(Int64 strUserId, string strSessionId, string IP, string strtxtPin, string strTTAccessKey, string strTCAccessKey, string strTT_ID, string strRegisteredConsumersId, string strBillId, string strlblCustomerID, string strlblConsumerNick, Int64 payFromAccountID, string cmbPayFromSelectedItemText, string cmbPayFromSelectedItemValue, string strTxtAmount, string strTxtComments, string strCCY, string lblTransactionType, string strGuid, string strOTPPIN, string Device_ID, string Token, string strSourceEnvironment, string strSourceEnvironmentDetail, out string strReturnMessage)
//
//
//        Parameter Detail:
//    strUserId: UserID
//    strTcAccessKey:FP
//    strSessionId: User sessionID
//    IP: User IP address
//    strTT_ID: provided in dataset from GetBillData in column TT_ID
//    strRegisteredConsumersId: provided in dataset from GetBillData in column                                               registered_consumer_id
//    strTxtAmount:amount fetched from GetFBRInquiry in dtFBR DataTable
//    payFromAccountID:   user accountID
        
//    cmbPayFromSelectedItemText: User selected payment debit account name
//    cmbPayFromSelectedItemValue: User selected payment debit account no.
        
//    strTTAccessKey: provided in dataset from GetBillData
//    strlblConsumerNick: dtProduct.Rows[0]["ConsumerName"].ToString() + '|' + dtProduct.Rows[0]["DueDate"].ToString() from  GetFBRInquiry
//    Device_ID: system device ID
//    Token: system generated token provided against device ID
//
//
//        Sample Data:
//        status = gnbweb.payAllBillConfirmSubmit(539152, "", "", "", "FBR", "TP", "343", "1212131313", "", "", dtProduct.Rows[0]["ConsumerName"].ToString() + '|' + dtProduct.Rows[0]["DueDate"].ToString(), 968174, "", "", "1000", "12691308", "586", "", "", "", "", "", "", "", out RetMsg);
//
        
//    @"strUserId",
//    @"strSessionId",
//    @"IP",
//    @"strtxtPin",
//    @"strTTAccessKey", @"FBR"
//    @"strTCAccessKey",
//    @"strTT_ID",
//    @"strRegisteredConsumersId",
//    @"strBillId",
//    @"strlblCustomerID",
//    @"strlblConsumerNick",
//    @"payFromAccountID",
//    @"cmbPayFromSelectedItemText",
//    @"cmbPayFromSelectedItemValue",
//    @"strTxtAmount",
//    @"strTxtComments",
//    @"strCCY",
//    @"lblTransactionType",
//    @"strGuid",
//    @"strOTPPIN",
//    @"Device_ID",
//    @"Token",
//    @"M3Key",
//
        
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
//                                    [encrypt encrypt_Data:@"FP"],
//                                    [encrypt encrypt_Data:gblclass.M3sessionid],
//                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                    [encrypt encrypt_Data:gblclass.fbrTtId],
//                                    [encrypt encrypt_Data:gblclass.fbrRegisteredConsumerId],
//                                    [encrypt encrypt_Data:gblclass.fbrAmount],
//                                    [encrypt encrypt_Data:gblclass.registd_acct_id_trans],
//                                    [encrypt encrypt_Data:selectedDepositAccountNumber],
//                                    [encrypt encrypt_Data:selectedDepositAccountId],
//                                    [encrypt encrypt_Data:@"TP"],
//                                    [encrypt encrypt_Data:@"nickName"],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:gblclass.token], nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
//                                                                       @"strTcAccessKey",
//                                                                       @"strSessionId",
//                                                                       @"IP",
//                                                                       @"strTT_ID",
//                                                                       @"strRegisteredConsumersId",
//                                                                       @"strTxtAmount",
//                                                                       @"payFromAccountID",
//                                                                       @"cmbPayFromSelectedItemText",
//                                                                       @"cmbPayFromSelectedItemValue",
//                                                                       @"strTTAccessKey",
//                                                                       @"strlblConsumerNick",
//                                                                       @"Device_ID",
//                                                                       @"Token", nil]];
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"1111"],
                                    [encrypt encrypt_Data:gblclass.fbrAccessKey],
                                    [encrypt encrypt_Data:gblclass.fbrTcAccessKey],
                                    [encrypt encrypt_Data:gblclass.fbrTtId],
                                    [encrypt encrypt_Data:gblclass.fbrRegisteredConsumerId],
                                    [encrypt encrypt_Data:gblclass.fbrBillId],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@|%@",gblclass.fbrClientName,gblclass.fbrDueDate]],
                                    [encrypt encrypt_Data:gblclass.registd_acct_id_trans],
                                    [encrypt encrypt_Data:self.payFromLabel.text],
                                    [encrypt encrypt_Data:selectedDepositAccountNumber],
                                    [encrypt encrypt_Data:gblclass.fbrAmount],
                                    [encrypt encrypt_Data:gblclass.fbrPSID],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:@""],
                                    [encrypt encrypt_Data:uuid],
                                    [encrypt encrypt_Data:self.BusinessOtp.text],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strtxtPin",
                                                                       @"strTTAccessKey",
                                                                       @"strTCAccessKey",
                                                                       @"strTT_ID",
                                                                       @"strRegisteredConsumersId",
                                                                       @"strBillId",
                                                                       @"strlblCustomerID",
                                                                       @"strlblConsumerNick",
                                                                       @"payFromAccountID",
                                                                       @"cmbPayFromSelectedItemText",
                                                                       @"cmbPayFromSelectedItemValue",
                                                                       @"strTxtAmount",
                                                                       @"strTxtComments",
                                                                       @"strCCY",
                                                                       @"lblTransactionType",
                                                                       @"strGuid",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"M3Key", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"payAllBillConfirmSubmit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  [hud hideAnimated:YES];
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
//                  {
//                      Response = 0;
//                      strReturnMessage = "Transaction is processed successfully. Please check its status in e-Transaction History.";
//                  }
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                       [hud hideAnimated:YES];
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      self.descriptionTextView.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                     
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-48"]) {
                       [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                       [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                       [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                   [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception) {
         [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
        
    }
}

-(void)PayBill:(NSString *)strIndustry {
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        //        Method name: PayBillSubmitClick
        //        Method signature:
        //        public string PayBillSubmitClick(Int64 strUserId, string strSessionId, string IP, Int64 PayFromAccountID, string cmbPayFromSelectedItemText, string cmbPayFromSelectedItemValue, string strtxtAmount, string strlblCustomerID, string strlblCompanyName, string strtxtComments, string strAccessKey, string strTCAccessKey, string strlblTransactionType, string strCCY, string tcAccessKey, string Device_ID, string Token, out string lblTransactionType, out string lblTransAmount, out string strReturnMessage, out string IsOtpRequired, out string OtpMessage)
        //
        //
        //
        //
        //        Parameter Detail:
        //
        //    strUserId: UserID
        //    strSessionId: User sessionID
        //    IP: User IP address
        //    strtxtComments: FBR PSID of user +
        //    strTTAccessKey: provided in dataset from GetBillData +
        //    strTxtAmount:amount fetched from GetFBRInquiry in dtFBR DataTable +
        //    strCCY:PKR
        //    strLblCompanyName: provided in dataset from GetBillData in column company_name +
        //    cmbPayFromSelectedItemText: User selected payment debit account name +
        //    cmbPayFromSelectedItemValue : User selected payment debit account no. +
        
        //    strRegisteredAccount: registered debit account ID from debit account selected from combo +
        //    Device_ID: system device ID +
        //    Token: system generated token provided against device ID +
        //
        //        Sample Data:
        //        string status22 = gnbweb.PayBillSubmitClick(539152, "12345678910", "69.96.99.66", 968174, "Syed Bashir-Ul-Has", "060510947958", "1000", "50", "FBR Payments", "test", "FBR", "TP", "FBR Payment", "PKR", "TP","","", out lblTransactionType, out lblTransAmount, out strReturnMessage, out isOtpReq, out OtpMessage);
        //
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:selectedDepositAccountId],
                                                                       [encrypt encrypt_Data:self.payFromLabel.text],
                                                                       [encrypt encrypt_Data:selectedDepositAccountNumber],
                                                                       [encrypt encrypt_Data:gblclass.fbrAmount],
                                                                       [encrypt encrypt_Data:gblclass.fbrRegisteredConsumerId],
                                                                       [encrypt encrypt_Data:gblclass.fbrCompanyName],
                                                                       [encrypt encrypt_Data:gblclass.fbrPSID],
                                                                       [encrypt encrypt_Data:@"FBR"],
                                                                       [encrypt encrypt_Data:@"TP"],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:gblclass.base_currency],
                                                                       [encrypt encrypt_Data:@"TP"],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                   
                                                              forKeys: [NSArray arrayWithObjects: @"strUserId",
                                                                        @"strSessionId",
                                                                        @"IP",
                                                                        @"PayFromAccountID",
                                                                        @"cmbPayFromSelectedItemText",
                                                                        @"cmbPayFromSelectedItemValue",
                                                                        @"strtxtAmount",
                                                                        @"strlblCustomerID",
                                                                        @"strlblCompanyName",
                                                                        @"strtxtComments",
                                                                        @"strAccessKey",
                                                                        @"strTCAccessKey",
                                                                        @"strlblTransactionType",
                                                                        @"strCCY",
                                                                        @"tcAccessKey",
                                                                        @"Device_ID",
                                                                        @"Token",
                                                                        @"M3Key", nil]];
        
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PayBill" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
//                  {
//                      IsOtpRequired = 0;
//                      OtpMessage = "";
//                      Response = 0;
//                      lblTransAmount = "<br  />  The Transaction charges shall be PKR 10.00 plus FED";
//                      lblTransactionType = "";
//                      strReturnMessage = " You have requested to transfer PKR 1,000.00 from your A/C No. 060510238092 (FAISAL UR REHMAN) to ";
//                  }
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [hud hideAnimated:YES];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      [self clearOTP];
                      [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                      
                      
                  } else {
                      [self clearOTP];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                      
                  }
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [self custom_alert:[error localizedDescription]  :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Retry"  :@"0"];
    }
}

-(void) payTezraftaarConfirmSubmit:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
//        PayTezraftaarConfirmSubmit
//        names = {ArrayList@7416}  size = 16
//        0 = "strUserId"
//        1 = "strSessionId"
//        2 = "strAccessKey"
//        3 = "Device_ID"
//        4 = "Token"
//        5 = "AccountId"
//        6 = "AccountNumber"
//        7 = "AccountName"
//        8 = "Amount"
//        9 = "OTPPin"
//        10 = "Comments"
//        11 = "BeneficiaryId"
//        12 = "PurposeofRemittance"
//        13 = "PurposeofRemitId"
//        14 = "Currency"
//        15 = "strGuid"
//        values = {ArrayList@7417}  size = 16
//        0 = "711404"
//        1 = "10984686"
//        2 = "UAE_3PC"
//        3 = "5bf69f8c1737cced"
//        4 = "a2793422e1d26ab65ed85535bd1526198878a241ece365a5fd8b2756c25d5de3"
//        5 = "1241777"
//        6 = "090101057236"
//        7 = "SYED%20MUBASHIR%20ABBA%20%5B105723-6%5D"
//        8 = "10"
//        9 = "175689"
//        10 = ""
//        11 = "1050-0078-003557-017"
//        12 = "EDUCATIONAL%20PAYMENT"
//        13 = "ED"
//        14 = "AED"
//        15 = "467064"
        
//        NSLog(@"%@",[gblclass.arr_review_withIn_myAcct objectAtIndex:4]);
        
        NSString* amounts = [[gblclass.arr_review_withIn_myAcct objectAtIndex:4] stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:@"UAE_3PC"],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                    [encrypt encrypt_Data:_payFromAccountNumber.text],
                                    [encrypt encrypt_Data:_payFromAccountName.text],
                                    [encrypt encrypt_Data:amounts],
                                    [encrypt encrypt_Data:_tezraftaarOpt.text],
                                    [encrypt encrypt_Data:_comentsLabel.text],
                                    [encrypt encrypt_Data:_payToAccountNumber.text],
                                    [encrypt encrypt_Data:_purposeLabel.text],
                                    [encrypt encrypt_Data:[gblclass.arr_review_withIn_myAcct objectAtIndex:7]], //arr_values
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:uuid], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"strAccessKey",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"AccountId",
                                                                       @"AccountNumber",
                                                                       @"AccountName",
                                                                       @"Amount",
                                                                       @"OTPPin",
                                                                       @"Comments",
                                                                       @"BeneficiaryId",
                                                                       @"PurposeofRemittance",
                                                                       @"PurposeofRemitId",
                                                                       @"Currency",
                                                                       @"strGuid", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"payTezraftaarConfirmSubmit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  [hud hideAnimated:YES];
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      [hud hideAnimated:YES];
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      
                      _tezraftaarOpt.hidden = YES;
                      _tezraftaarOtpHeading.hidden = YES;
                      
                      gblclass.tz_accounts = [[NSArray alloc] init];
                      self.tezraftaarDescriptionTextView.text=[self stringByStrippingHTML:[dic objectForKey:@"ThanksMessage"]];
//                      self.descriptionTextView.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-48"]) {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
              failure:^(NSURLSessionDataTask *task, NSError *error)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Try again later." :@"0"];
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    //[hud show:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        @try {
            
            gblclass.ssl_pin_check=@"1";
            
            //NSLog(@"%@",gblclass.arr_values);
            
            NSString* chk_acct_type;
            NSString* acct_type;
            NSString* ran;
            
            if ([chk_ssl isEqualToString:@"logout"])
            {
                chk_ssl=@"";
                [self mob_App_Logout:@""];
                
                return;
            }
            
            
            if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
            {
                if ([txt_otp.text isEqualToString:@""])
                {
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter OTP" :@"0"];
                    
                    return;
                }
                else if (txt_otp.text.length<6)
                {
                    [hud hideAnimated:YES];
                    
                    [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
                    
                    return;
                }
                
            }
            
            
            gblclass.Is_Otp_Required=@"0";
            
            if ([gblclass.check_review_acct_type isEqualToString:@"My_acct"])
            {
                NSArray* split = [[gblclass.arr_transfer_within_acct_to objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
                
                //          NSArray* arr=[[split objectAtIndex:3] componentsSeparatedByString: @""];
                ran=[split objectAtIndex:3];
                // privalages=[ran substringWithRange:NSMakeRange(2,1)];
                acct_type=[split objectAtIndex:3];
                
                
                chk_acct_type=@"0";
                // CHeck Account Type SY,CM,O
                
                
                if ([acct_type isEqualToString:@"1"] && [chk_acct_type isEqual:@"0"])
                {
                    
                    [self pay_Confirm_Submit:@""];
                    
                    
                    return;
                }
                
                else
                {
                    
                    
                    [self custom_alert:@"Unable to deposit, please try later" :@"0"];
                    
                    
                    //                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
                    //
                    //                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    //                    [alert addAction:ok];
                    //
                    //                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                    
                    [hud hideAnimated:YES];
                    return;
                }
                
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Other_acct"])
            {
                
                [self pay_Confirm_Submit:@""];
                
                
                
                
                //                NSString* num;
                //                num=gblclass.indexxx;
                //                if (!([num length]>0))
                //                {
                //                    num=@"0";
                //                }
                
                
                
                //                NSArray* split = [[gblclass.arr_transfer_within_acct objectAtIndex:[num integerValue]] componentsSeparatedByString: @"|"];
                //
                //                //  NSArray* arr=[[split objectAtIndex:3] componentsSeparatedByString: @""];
                //                ran=[split objectAtIndex:3];
                //                // privalages=[ran substringWithRange:NSMakeRange(2,1)];
                //                acct_type=[split objectAtIndex:5];
                //
                
                
                
                //                if ([acct_type isEqualToString:@"SY"] && [chk_acct_type isEqual:@"0"])
                //                {
                //                     [self.connection cancel];
                //                     [self pay_Confirm_Submit:@""];
                //
                //                    return;
                //                }
                //                else if ([acct_type isEqualToString:@"CM"] && [chk_acct_type isEqual:@"0"])
                //                {
                //                     [self.connection cancel];
                //                     [self pay_Confirm_Submit:@""];
                //
                //                    return;
                //                }
                //                else if ([acct_type isEqualToString:@"OR"] && [chk_acct_type isEqual:@"0"])
                //                {
                //                    [self.connection cancel];
                //                    [self pay_Confirm_Submit:@""];
                //
                //                    return;
                //                }
                //                if ([acct_type isEqualToString:@"O"] && [chk_acct_type isEqual:@"0"])
                //                {
                //                     [self.connection cancel];
                //                     [self pay_Confirm_Submit:@""];
                //
                //                    return;
                //                }
                
                //                else
                //                {
                //                    [self.connection cancel];
                //                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
                //
                //                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                //                    [alert addAction:ok];
                //
                //                    [self presentViewController:alert animated:YES completion:nil];
                //                    [hud hideAnimated:YES];
                //                    return;
                //                }
                
                
                
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Mobile_Topup"])
            {
                //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                //                {
                //                    if ([txt_otp.text isEqualToString:@""])
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //
                //                        return;
                //                    }
                //                    else if (txt_otp.text.length<6)
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter 6 Digits OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //                        return;
                //                    }
                //                }
                //
                //                gblclass.Is_Otp_Required=@"0";
                
                [self pay_All_Bill_Confirm_Submit:@""];
                
                
                // [self Pay_Bill_Mobile_Topup:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Prepaid_voucher"])
            {
                
                [self pay_All_Bill_Confirm_Submit:@""];
                
                
                // [self Pay_Bill_Prepaid_voucher:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Wiz"])
            {
                [self pay_All_Bill_Confirm_Submit_Wiz:@""];
                
                //[self Pay_Bill_Wiz:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Online_shopping"])
            {
                // [self Pay_Bill_Onlineshopping:@""];
                
                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                {
                    if ([txt_otp.text isEqualToString:@""])
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter OTP"  :@"0"];
                        
                        //                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                        //                                                                 message:@"Enter OTP"
                        //                                                                delegate:self
                        //                                                       cancelButtonTitle:@"Ok"
                        //                                                       otherButtonTitles:nil, nil];
                        //
                        //                [alert1 show];
                        
                        return;
                    }
                    else if (txt_otp.text.length<6)
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter 6 Digits OTP"  :@"0"];
                        
                        //                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                        //                                                                 message:@"Enter 6 Digits OTP"
                        //                                                                delegate:self
                        //                                                       cancelButtonTitle:@"Ok"
                        //                                                       otherButtonTitles:nil, nil];
                        //
                        //                [alert2 show];
                        
                        return;
                    }
                }
                
                [self pay_Bill_Confirm_Submit:@""];
                
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"Utility_bill"])
            {
                
                //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                //                {
                //                    if ([txt_otp.text isEqualToString:@""])
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //
                //                        return;
                //                    }
                //                    else if (txt_otp.text.length<6)
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter 6 Digits OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //                        return;
                //                    }
                //
                //                }
                
                
                [self pay_Bill_Confirm_Submit_Utility:@""];
                
                // [self Pay_Bill_Utility:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"OB_bill"])
            {
                
                //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                //                {
                //                    if ([txt_otp.text isEqualToString:@""])
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //
                //                        return;
                //                    }
                //                    else if (txt_otp.text.length<6)
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter 6 Digits OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //                        return;
                //                    }
                //
                //                }
                
                [self pay_Bill_Confirm_Submit_Utility:@""];
                
                // [self Pay_Bill_Mobile:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"isp"])
            {
                
                //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
                //                {
                //                    if ([txt_otp.text isEqualToString:@""])
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //
                //                        return;
                //                    }
                //                    else if (txt_otp.text.length<6)
                //                    {
                //                        [hud hideAnimated:YES];
                //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                        message:@"Enter 6 Digits OTP"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //
                //                        [alert show];
                //                        return;
                //                    }
                //                }
                //
                //                gblclass.Is_Otp_Required=@"0";
                
                [self pay_Bill_Confirm_Submit_Utility:@""];
                
                //[self Pay_Bill_ISP:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"UBLBP"])
            {
                
                if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
                {
                    [self Pay_Confirm_Submit_CC:@"" ];
                    
                }
                else
                {
                    [self pay_All_Bill_Confirm_Submit_UBLBP:@""];
                    
                }
                
                //[self Pay_Bill:@""];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code"])
            {
                [self QR_Transaction:@""];
                
                [self.connection cancel];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code_p2p"])
            {
                //  [self QR_Transaction:@""];
                
                [self P2P_Transaction:@""];
                
                [self.connection cancel];
            }
            else if([gblclass.check_review_acct_type isEqualToString:@"franchise_payment"])
            {
                
                [self Franchise_payment:@""];
                
            }

            else if([gblclass.check_review_acct_type isEqualToString:@"school_fee"])
            {
                [self pay_Bill_ConfirmSubmit_school:@""];
            }

            else if([gblclass.check_review_acct_type isEqualToString:@"FBR"]) {

                
                if ([gblclass.fbrOTPRequired isEqualToString:@"1"]) {
                    if ([self.BusinessOtp.text isEqualToString:@""]) {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter OTP" :@"0"];
                    }
                    
                    else if (self.BusinessOtp.text.length<6) {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
                    } else {
                        [self PayAllBillConfirmSubmit:@""];
                    }
                    
                } else {
                    [self PayAllBillConfirmSubmit:@""];
                }
                
                
            } if ([gblclass.check_review_acct_type isEqualToString:@"tezraftaar"]) {
                
                if ([gblclass.tezraftaarOTPRequired isEqualToString:@"1"]) {
                    if ([_tezraftaarOpt.text isEqualToString:@""]) {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter OTP" :@"0"];
                    }
                    
                    else if (_tezraftaarOpt.text.length<6) {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
                    } else {
                        [self payTezraftaarConfirmSubmit:@""];
                    }
                    
                } else {
                    [self payTezraftaarConfirmSubmit:@""];
                }
            }
              
        } @catch (NSException *exception)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Try again later." :@"0"];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    //NSLog(@"%@",gblclass.arr_values);
    
    NSString* chk_acct_type;
    NSString* acct_type;
    NSString* ran;
    
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
        
        //[self.connection cancel];
        
        return;
    }
    
//    NSLog(@"%@",gblclass.Is_Otp_Required);
    if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
    {
//        NSLog(@"%@",gblclass.check_review_acct_type);//tezraftaar
        if([gblclass.check_review_acct_type isEqualToString:@"school_fee"])
        {
            if ([txt_otp_sch.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Enter OTP" :@"0"];
                return;
            }
            else if (txt_otp_sch.text.length<6)
            {
                [hud hideAnimated:YES]; 
                [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
                return;
            }
            
            txt_otp.text = txt_otp_sch.text;
        }
        else
        {
            if ([txt_otp.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
               //[self.connection cancel];
                [self custom_alert:@"Enter OTP" :@"0"];
                return;
            }
            else if (txt_otp.text.length<6)
            {
                [hud hideAnimated:YES];
                //[self.connection cancel];
                [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
                return;
            }
        }
        
    }
    
    
    gblclass.Is_Otp_Required=@"0";
    
    if ([gblclass.check_review_acct_type isEqualToString:@"My_acct"])
    {
        
        NSArray* split = [[gblclass.arr_transfer_within_acct_to objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
        
        //          NSArray* arr=[[split objectAtIndex:3] componentsSeparatedByString: @""];
        ran=[split objectAtIndex:3];
        // privalages=[ran substringWithRange:NSMakeRange(2,1)];
        acct_type=[split objectAtIndex:3];
        
        
        chk_acct_type=@"0";
        // CHeck Account Type SY,CM,O
        
        
        [self pay_Confirm_Submit:@""];
        
        //        [self.connection cancel];
        
        //        ************ 19 Oct 2017  ************
        //        if ([acct_type isEqualToString:@"1"] && [chk_acct_type isEqual:@"0"])
        //        {
        //
        //            [self pay_Confirm_Submit:@""];
        //            [self.connection cancel];
        //
        //            return;
        //        }
        //
        //        else
        //        {
        //
        //            [self.connection cancel];
        //            [self custom_alert:@"Do Not Deposit" :@"0"];
        //
        //
        //            //                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
        //            //
        //            //                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //            //                    [alert addAction:ok];
        //            //
        //            //                    [self presentViewController:alert animated:YES completion:nil];
        //
        //
        //
        //            [hud hideAnimated:YES];
        //            return;
        //        }
        //        ************ 19 Oct 2017  ************
        
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Other_acct"])
    {
       
//        NSLog(@"%@",gblclass.acct_summary_section);
        
//        if ([gblclass.acct_summary_section isEqualToString:@"Credit Card Account"])
//        {
//            [self Pay_CC_Bill:@""];
//        }
//        else
//        {
            [self pay_Confirm_Submit:@""];
//        }
        
        
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Mobile_Topup"])
    {
        //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        //                {
        //                    if ([txt_otp.text isEqualToString:@""])
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //
        //                        return;
        //                    }
        //                    else if (txt_otp.text.length<6)
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter 6 Digits OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //                        return;
        //                    }
        //                }
        //
        //                gblclass.Is_Otp_Required=@"0";
        
        [self pay_All_Bill_Confirm_Submit:@""];
        //        [self.connection cancel];
        
        // [self Pay_Bill_Mobile_Topup:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Prepaid_voucher"])
    {
        
        [self pay_All_Bill_Confirm_Submit:@""];
        //        [self.connection cancel];
        
        // [self Pay_Bill_Prepaid_voucher:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Wiz"])
    {
        [self pay_All_Bill_Confirm_Submit_Wiz:@""];
        //        [self.connection cancel];
        //[self Pay_Bill_Wiz:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Online_shopping"])
    {
        // [self Pay_Bill_Onlineshopping:@""];
        
        if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        {
            if ([txt_otp.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                //                [self.connection cancel];
                [self custom_alert:@"Enter OTP"  :@"0"];
                
                //                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                 message:@"Enter OTP"
                //                                                                delegate:self
                //                                                       cancelButtonTitle:@"Ok"
                //                                                       otherButtonTitles:nil, nil];
                //
                //                [alert1 show];
                
                return;
            }
            else if (txt_otp.text.length<6)
            {
                [hud hideAnimated:YES];
                //                [self.connection cancel];
                [self custom_alert:@"Enter 6 Digits OTP"  :@"0"];
                
                //                UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                //                                                                 message:@"Enter 6 Digits OTP"
                //                                                                delegate:self
                //                                                       cancelButtonTitle:@"Ok"
                //                                                       otherButtonTitles:nil, nil];
                //
                //                [alert2 show];
                
                return;
            }
        }
        
        [self pay_Bill_Confirm_Submit:@""];
        //        [self.connection cancel];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"Utility_bill"])
    {
        
        //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        //                {
        //                    if ([txt_otp.text isEqualToString:@""])
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //
        //                        return;
        //                    }
        //                    else if (txt_otp.text.length<6)
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter 6 Digits OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //                        return;
        //                    }
        //
        //                }
        
        
        [self pay_Bill_Confirm_Submit_Utility:@""];
        //        [self.connection cancel];
        // [self Pay_Bill_Utility:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"OB_bill"])
    {
        
        //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        //                {
        //                    if ([txt_otp.text isEqualToString:@""])
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //
        //                        return;
        //                    }
        //                    else if (txt_otp.text.length<6)
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter 6 Digits OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //                        return;
        //                    }
        //
        //                }
        
        [self pay_Bill_Confirm_Submit_Utility:@""];
        //        [self.connection cancel];
        // [self Pay_Bill_Mobile:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"isp"])
    {
        
        //                if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
        //                {
        //                    if ([txt_otp.text isEqualToString:@""])
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //
        //                        return;
        //                    }
        //                    else if (txt_otp.text.length<6)
        //                    {
        //                        [hud hideAnimated:YES];
        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                                        message:@"Enter 6 Digits OTP"
        //                                                                       delegate:self
        //                                                              cancelButtonTitle:@"Ok"
        //                                                              otherButtonTitles:nil, nil];
        //
        //                        [alert show];
        //                        return;
        //                    }
        //                }
        //
        //                gblclass.Is_Otp_Required=@"0";
        
        [self pay_Bill_Confirm_Submit_Utility:@""];
        //        [self.connection cancel];
        //[self Pay_Bill_ISP:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"UBLBP"])
    {
        
        if ([gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
        {
            
           // [self Pay_CC_Bill:@""];
            
            [self Pay_Confirm_Submit_CC:@"" ];
            //            [self.connection cancel];
        }
        else
        {
            [self pay_All_Bill_Confirm_Submit_UBLBP:@""];
        }
        
        //[self Pay_Bill:@""];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code"])
    {
        [self QR_Transaction:@""];
        
        //        [self.connection cancel];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"QR_Code_p2p"])
    {
        //  [self QR_Transaction:@""];
        
        [self P2P_Transaction:@""];
        
        //        [self.connection cancel];
    }
    else if([gblclass.check_review_acct_type isEqualToString:@"franchise_payment"])
    {
        //        [self.connection cancel];
        [self Franchise_payment:@""];
        
    }

    else if([gblclass.check_review_acct_type isEqualToString:@"school_fee"])
    {
        [self pay_Bill_ConfirmSubmit_school:@""];
    }

    else if([gblclass.check_review_acct_type isEqualToString:@"FBR"]) {
        [self PayAllBillConfirmSubmit:@""];
        
    }
//    NSLog(@"%@",gblclass.check_review_acct_type);
    if ([gblclass.check_review_acct_type isEqualToString:@"tezraftaar"]) {
        
    if ([gblclass.tezraftaarOTPRequired isEqualToString:@"1"])
    {
        if ([_tezraftaarOpt.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter OTP" :@"0"];
        }
        else if (_tezraftaarOpt.text.length<6)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter 6 Digits OTP" :@"0"];
        }
        else
        {
            [self payTezraftaarConfirmSubmit:@""];
        }
    }
    else
    {
        [self payTezraftaarConfirmSubmit:@""];
    }
        
}
//    else
//    {
//        [self payTezraftaarConfirmSubmit:@""];
//    }
    
    
    //    [self.connection cancel];
     
}


-(void) Pay_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"strlblCustomerID",
                                @"strlblCompanyName",@"strtxtComments",@"strAccessKey",@"strTCAccessKey",
                                @"strlblTransactionType",@"strCCY",@"tcAccessKey",@"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayUBLBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  //                  [hud hideAnimated:YES];
                  //                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfull"
                  //                                                                  message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                  //                                                                 delegate:self
                  //                                                        cancelButtonTitle:@"Ok"
                  //                                                        otherButtonTitles:nil, nil];
                  //
                  //                  [alert show];
                  
                  
                  if ( [gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
                  {
                      gblclass.direct_pay_frm_Acctsummary=@"0";
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Successfull"
                                                                       message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert1 show];
                      
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self showAlertwithcancel:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"Attention"];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      //
                      //                       [self presentViewController:vc animated:NO completion:nil];
                  }
                  
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}






-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil,nil];
                       [alert1 show];
                   });
    
}



-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

-(void)dismissModalStack {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:nil];
    
    
    //    UIViewController *vc2 = self.presentingViewController;
    //    while ([vc2 isKindOfClass:[Payee_VC class]]) {
    //        vc2 = vc2.presentingViewController;
    //    }
    //    [vc dismissViewControllerAnimated:YES completion:NULL];
    //    UIViewController *vc2 = self.presentingViewController;
    //
    //    while ([vc2 isKindOfClass:[Payee_VC class]]) {
    //
    //
    //        [vc2 dismissViewControllerAnimated:NO completion:nil];
    //    }
    //
    
    //    while (vc2.presentingViewController) {
    //        vc2 = vc2.presentingViewController;
    //    }
    //    [vc dismissViewControllerAnimated:YES completion:NULL];
    //}
    
    
    
}










- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    BOOL stringIsValid;
    NSInteger MAX_DIGITS=6;
     BOOL stringIsValid;
    
    if ([theTextField isEqual:_tezraftaarOpt]) {

        
//        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
//
//        [theTextField addTarget:self
//                         action:@selector(textFieldDidChange:)
//               forControlEvents:UIControlEventEditingChanged];
//
//        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    else if ([theTextField isEqual:txt_otp])
    {
        MAX_DIGITS=6;
        
//        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
//
//        [theTextField addTarget:self
//                      action:@selector(textFieldDidChange:)
//            forControlEvents:UIControlEventEditingChanged];
//
//        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;

        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    else if ([theTextField isEqual:txt_otp_sch]) {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    else if ([theTextField isEqual:self.BusinessOtp]) {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    NSString *str;
    
     if ([theTextField isEqual:txt_otp])
     {
         NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
         //        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
         //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
         //        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
         
         NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
         NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
         nf.numberStyle = NSNumberFormatterDecimalStyle;
//         NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
         
         str = [nf stringFromNumber:myNumber];
         
         str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
         
         txt_otp.text = str;
         
//         txt_otp.text=[nf stringFromNumber:myNumber];
     }
   if ([theTextField isEqual:_tezraftaarOpt]) {
       
       NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"" withString:@""];
       //        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
       //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
       //        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
       
       NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
       NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
       nf.numberStyle = NSNumberFormatterNoStyle;
//       NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
       
//       _tezraftaarOpt.text=[nf stringFromNumber:myNumber];
       
       str = [nf stringFromNumber:myNumber];
       
       str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
       
       _tezraftaarOpt.text = str;
   }
    
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    vw_down_chck = @"1";
    [txt_otp resignFirstResponder];
    [txt_otp_sch resignFirstResponder];
    [_tezraftaarOpt resignFirstResponder];
    [_tezraftaarDescriptionTextView resignFirstResponder];
    [txt_ft_msg resignFirstResponder];
    [txt_ft_tax resignFirstResponder];
    [lbl_ft_msg resignFirstResponder];
    
    
    [self.view endEditing:YES];
    
//    if ([vw_down_chck isEqualToString:@"1"])
//    {
//        [self keyboardWillHide];
//        vw_down_chck = @"0";
//    }
    
  //  [self.fbrView endEditing:YES];
}


#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)keyboardWillHide
{
    
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_ok:(id)sender
{
    
    if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
    {
        return;
    }
    
    //    //NSLog(@"%ld",(long)buttonIndex);
    //    if (buttonIndex == 1)
    //    {
    //        [self checkinternet];
    //        if (netAvailable)
    //        {
    //            chk_ssl=@"logout";
    //            [self SSL_Call];
    //        }
    //
    //        //[self mob_App_Logout:@""];
    //
    //        //Do something
    //        //NSLog(@"1");
    //    }
    //    else if(buttonIndex == 0)
    //    {
    
    
    if ( [gblclass.direct_pay_frm_Acctsummary isEqualToString:@"1"])
    {
        gblclass.direct_pay_frm_Acctsummary=@"0";
        [self slide_right];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        
        gblclass.payment_login_chck=@"0";
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
        }
        else
        {
            [self slide_right];
            [self dismissModalStack];
        }
    }
    //  }
    
}




-(void) pay_Bill_ConfirmSubmit_school:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];  //baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //payBillConfirmSubmit(string strUserId, string strSessionId, string IP, string strtxtPin, string strTTAccessKey, string strTCAccessKey, string strTT_ID, string strRegisteredConsumersId, string strBillId, string strlblCustomerID, string strlblConsumerNick, string payFromAccountID, string cmbPayFromSelectedItemText, string cmbPayFromSelectedItemValue, string strTxtAmount, string strTxtComments, string strCCY, string lblTransactionType, string strGuid, string strOTPPIN, string Device_ID, string Token, string M3Key)
        
        NSString *uuid = [[NSUUID UUID] UUIDString];
 
//          NSLog(@"%@",gblclass.arr_values);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"1111"],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],//@"BCR"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],//@"BCR"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],//@"226"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],//@"21127"
                                    [encrypt encrypt_Data:@"0"],//bill id
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],//@"111301"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:20]],//20
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],//@"2279625"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],//@"SHOAIB"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],//@"212549568"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],//@"34630"
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],//@"34630"],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],//@"PKR"
                                    [encrypt encrypt_Data:@""],
                                    [encrypt encrypt_Data:uuid],
                                    [encrypt encrypt_Data:txt_otp.text],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="],nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strtxtPin",
                                                                       @"strTTAccessKey",
                                                                       @"strTCAccessKey",
                                                                       @"strTT_ID",
                                                                       @"strRegisteredConsumersId",
                                                                       @"strBillId",
                                                                       @"strlblCustomerID",
                                                                       @"strlblConsumerNick",
                                                                       @"payFromAccountID",
                                                                       @"cmbPayFromSelectedItemText",
                                                                       @"cmbPayFromSelectedItemValue",
                                                                       @"strTxtAmount",
                                                                       @"strTxtComments",
                                                                       @"strCCY",
                                                                       @"lblTransactionType",
                                                                       @"strGuid",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"M3Key", nil]];
        //GetBillPayment GetFCPBillsDetails
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"payBillConfirmSubmit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* a;
                  a = [[NSMutableArray alloc] init];
                  
                  if ([[dic2 objectForKey:@"Response"] integerValue]==0)
                  {
                      [hud hideAnimated:YES];
                      
                      lbl_heading.text = @"PAYMENT CONFIRMATION";
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      //txt_otp.enabled=NO;
                      txt_otp_sch.hidden=YES;
                      txt_msg_sch.text=[self stringByStrippingHTML:[dic2 objectForKey:@"strReturnMessage"]];
                      
                  }
                  else if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      txt_otp_sch.text = @"";
                      
                      [self custom_alert:[self stringByStrippingHTML:[dic2 objectForKey:@"strReturnMessage"]] :@"0"];
                      
                  }

                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             // [mine myfaildata];
             [hud hideAnimated:YES];
             [self custom_alert:@"Please try again later." :@"0"];
             
         }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void)Franchise_payment:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //         manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSArray *split_1 = [[gblclass.arr_franchise_data objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
//        NSLog(@"%@",gblclass.arr_franchise_data_selected);
        NSArray*  split_2 = [[gblclass.arr_franchise_data_selected objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        NSString* str_ocl=[NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",
                           [gblclass.arr_franchise_data_OCL objectAtIndex:2],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:0],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:1],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:7],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:4],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:3],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:6],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:10],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:5],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:9],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:8],
                           [gblclass.arr_franchise_data_OCL objectAtIndex:11]];
        
        
//        NSLog(@"%@",[gblclass.arr_values objectAtIndex:15]);
//        NSLog(@"%@",[gblclass.arr_values objectAtIndex:16]);
//        NSLog(@"%@",[gblclass.arr_values objectAtIndex:10]);
//        NSLog(@"%@",gblclass.arr_values);
        
        //[gblclass.arr_values objectAtIndex:14]
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                    [encrypt encrypt_Data:uuid],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                    [encrypt encrypt_Data:str_ocl],
                                    [encrypt encrypt_Data:txt_otp.text],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strtxtPin",
                                                                       @"strTTAccessKey",
                                                                       @"strTCAccessKey",
                                                                       @"strTT_ID",
                                                                       @"strRegisteredConsumersId",
                                                                       @"strBillId",
                                                                       @"strlblCustomerID",
                                                                       @"strlblConsumerNick",
                                                                       @"payFromAccountID",
                                                                       @"cmbPayFromSelectedItemText",
                                                                       @"cmbPayFromSelectedItemValue",
                                                                       @"strTxtAmount",
                                                                       @"strTxtComments",
                                                                       @"strCCY",
                                                                       @"lblTransactionType",
                                                                       @"strGuid",
                                                                       @"strMandatoryFieldId",
                                                                       @"strTxtMandatoryFieldValue",
                                                                       @"outOCL",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"M3Key", nil]];
        
//        NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"PayConfirmSubmitFranchisePayment" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      btn_ok.hidden=NO;
                      btn_pay.hidden=YES;
                      btn_cancel.hidden=YES;
                      //txt_otp.enabled=NO;
                      txt_otp.hidden=YES;
                      txt_msg.text=[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      
                      
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                      
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:exception.reason :@"0"];
    }
}

- (void)textViewTapped:(UITapGestureRecognizer *)tap {
     //DO SOMTHING
     
//     NSLog(@"TAPPP");
//     NSLog(@"%@",tap);
     vw_down_chck = @"1";
 }

 #pragma mark - Gesture recognizer delegate

 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
 }



@end

