 //
//  GlobalStaticClass.h
//  ubltestbanking
//
//  Created by ammar on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GlobalStaticClass  : NSObject
{
    NSArray *actsummaryarr;
}

-(void)SSL_name_set:(NSString *)txt;
-(void)SSL_name_get;

//For Certificate ::
@property(nonatomic,strong)NSString* SSL_name;
@property(nonatomic,strong)NSString* SSL_type;
@property(nonatomic,strong)NSString* SSL_Certificate_name;

@property(nonatomic,strong)NSString* nic_mobile_no;
@property(nonatomic,strong)NSString* nic_account_no;
@property(nonatomic,strong)NSString* nic_email;
@property(nonatomic,strong)NSString* trans_amt;
@property(nonatomic,strong)NSString* ft_msg;
@property(nonatomic,strong)NSString* ft_tax;
@property(nonatomic,strong)NSString* gbl_curr;
@property(nonatomic,strong)NSString* landing;
@property(nonatomic,strong)NSString* bill_type_chck;

@property(nonatomic,strong)NSMutableArray* arr_resend_otp_cnic;
@property(nonatomic,strong)NSString* lati_pekabo;
@property(nonatomic,strong)NSString* longi_pekabo;
@property(nonatomic,strong)NSString* txt_cnic_signup;
@property(nonatomic,strong)NSString* txt_atmCard_signup;
@property(nonatomic,strong)NSString* txt_atmPin_signup;

@property(nonatomic,strong)NSString* device_name;
@property(nonatomic,strong)NSString* signUp_through_cnic;
@property(nonatomic,strong)NSString* jb_chk_login_back;
@property(nonatomic,strong)NSString* wiz_card_chck;
@property(nonatomic,strong)NSString* register_acct_back;
@property(nonatomic,strong)NSString* chk_device_jb;
@property(nonatomic,strong)NSString* M3sessionid;
@property(nonatomic,retain)NSString* tab_bar_login_pw_check;
@property(nonatomic,retain)NSString* user_login_name;
@property(nonatomic,retain)NSString* user_id;
@property(nonatomic,retain)NSString* isEmail_Exist;
@property(nonatomic,retain)NSString* btn_reg_selected;
@property(nonatomic,retain)NSString* dob;
@property(nonatomic,retain)NSString* screen_size;
@property(nonatomic,retain)NSString* welcome_msg;
@property(nonatomic,retain)NSString* T_Pin;
@property(nonatomic,retain)NSString* daily_limit;
@property(nonatomic,retain)NSString* monthly_limit;
@property(nonatomic,retain)NSString* pin_pattern;
@property(nonatomic,retain)NSArray *actsummaryarr;
@property(nonatomic,retain)NSMutableArray *atmcardlist;
@property(nonatomic,retain)NSMutableArray *transaction_history;
@property(nonatomic,retain)NSMutableArray *arr_acct_statement1;
@property(nonatomic,retain)NSMutableArray *arr_acct_statement_global;
@property(nonatomic,strong)NSString* acct_statmnt_indexpath;
@property(nonatomic,strong)NSString* defaultaccountno;
@property(nonatomic,strong)NSString* defaultaccountname;
@property(nonatomic,retain)NSString* storyboardidentifer;
@property(nonatomic,retain)NSMutableArray* arr_get_Forgot_pass;
@property(nonatomic,retain)NSMutableArray* arr_send_Forgot_pass;
@property(nonatomic,retain)NSMutableArray* arr_transaction_history;
@property(nonatomic,retain)NSString* transaction_id;
@property(nonatomic,strong)NSMutableArray* arr_act_statmnt_name;
@property(nonatomic,strong)NSString* is_default_acct_id;
@property(nonatomic,strong)NSString* profile_pic_name;
@property(nonatomic,strong)NSString* is_default_m3_balc;
@property(nonatomic,strong)NSString* is_touch_chk;
@property(nonatomic,strong)NSString* chck_qr_send;
@property(nonatomic,strong)NSString* scan_qr_amnt;
@property(nonatomic,strong)NSString* chk_qr_demo_login;
@property(nonatomic,strong)NSString* acct_statement_name;
@property(nonatomic,strong)NSString* acct_statement_id;
@property(nonatomic,strong)NSString* qr_code_pin;
@property(nonatomic,strong)NSString* summary_currency;
@property(nonatomic,strong)NSString* sign_up_token;
@property(nonatomic,strong)NSString* Chk_instant_qr_payment;
@property(nonatomic,strong)NSString* instant_credit_chk;
@property(nonatomic,strong)NSString* instant_debit_chk;
@property(nonatomic,strong)NSMutableArray* arr_instant_qr;
@property(nonatomic,strong)NSString* arr_qr_index;
@property(nonatomic,strong)NSString* M3Session_ID_instant;
@property(nonatomic,strong)NSString* token_instant;
@property(nonatomic,strong)NSString* chk_term_condition;
@property(nonatomic,strong)NSString* amt_mp_limit;
@property(nonatomic,strong)NSString* deposit_slip;
@property(nonatomic,strong)NSString* chk_payee_management_select;
@property(nonatomic,strong)NSString* is_default_acct_type;
@property(nonatomic,strong)NSString* tc_access_key;
@property(nonatomic,strong)NSString* is_default_br_code;
@property(nonatomic,strong)NSString* forget_pw_btn_click;
@property(nonatomic,strong)NSString* onlineShopingRefFieldName;
@property(nonatomic,strong)NSString* device_chck;
@property(nonatomic,strong)NSString* term_conditn_chk_place;
@property(nonatomic,strong)NSString* TnC_calling_source;
@property(nonatomic,strong)NSString* proced_login_t_n_c;
@property(nonatomic,strong)NSString* change_pw_txt;
@property(nonatomic,strong)NSString* forgot_pw_click;
@property(nonatomic,strong)NSString* camera_back;
@property(nonatomic,strong)NSString* login_tch_btn_txt;
@property(nonatomic,strong)NSString* isDeviceJailBreaked;
@property(nonatomic,strong)NSString* reviewCommentFieldText;
@property(nonatomic, strong) NSString *otp_chck;
@property(nonatomic, strong) NSString *termsAndconditions;
@property(nonatomic, strong) NSMutableArray *descriptionDetailModel;
@property(nonatomic, strong) NSString *arr_school_name;
@property(nonatomic, strong) NSString *chck_103;
@property(nonatomic, strong) NSMutableArray *school_fee_data;
@property(nonatomic, strong) NSMutableArray *arr_school_edit;
@property(nonatomic, strong) NSMutableArray *arr_school_amnt;
@property(nonatomic, strong) NSMutableArray *Registered_customer_id;
@property(nonatomic,strong)NSString* chk_termNCondition;
@property(nonatomic,strong)NSString* TnC_chck_On;
@property(nonatomic,strong)NSString* peekabo_kfc_userid;
@property(nonatomic,strong)NSString* story_board;

@property(nonatomic,strong)NSMutableArray* arr_resend_with_atm_signup_email;
@property(nonatomic,strong)NSMutableArray* arr_resend_with_atm_signup_dob;
@property(nonatomic,strong)NSString* str_processed_login_chk;


@property(nonatomic,strong)NSMutableArray* arr_transaction_detail;
@property(nonatomic,strong)NSString* chk_acct_statement;
@property(nonatomic,strong)NSString* user_email;
@property(nonatomic,strong)NSString* is_default_acct_id_name;
@property(nonatomic,strong)NSString* is_default_acct_no;
@property(nonatomic,strong)NSString* is_default_currency;
@property(nonatomic,strong)NSString* is_qr_payment;
@property(nonatomic,strong)NSString* is_debit_lock;
@property(nonatomic,strong)NSString* setting_chck_Tch_ID;


@property(nonatomic,strong)NSMutableArray* arr_transfer_within_acct;
@property(nonatomic,strong)NSMutableArray* arr_transfer_within_acct_deposit_chck;
@property(nonatomic,strong)NSMutableArray* arr_user_pw_table2;
@property(nonatomic,strong)NSMutableArray* arr_transfer_within_acct_table2;
@property(nonatomic,strong)NSMutableArray* arr_get_pay_details;
@property(nonatomic,strong)NSMutableArray* arr_bill_elements;
@property(nonatomic,strong)NSString* tot_balc;
@property(nonatomic,strong)NSString* atm_req_gender_flag;
@property(nonatomic,strong)NSString* atm_req_gender_flag_value;
@property(nonatomic,strong)NSMutableArray* arr_review_withIn_myAcct;
@property(nonatomic,strong)NSString* check_review_acct_type;
@property(nonatomic,strong)NSString* strAccessKey_for_add;
@property(nonatomic,strong)NSString* acct_add_type;
@property(nonatomic,strong)NSString* acct_add_header_type;
@property(nonatomic,strong)NSString* add_bill_type;
@property(nonatomic,strong)NSString* signup_mobile;
@property(nonatomic,strong)NSString* signup_login_name;
@property(nonatomic,strong)NSString* signup_pw;
@property(nonatomic,strong)NSString* Chck_Mpin_login;
@property(nonatomic,strong)NSString* first_time_login_chck;
@property(nonatomic,strong)NSString* add_payee_email;
@property(nonatomic,strong)NSString* sign_in_username;
@property(nonatomic,strong)NSString* sign_in_pw;
@property(nonatomic,strong)NSString* touch_id_frm_menu;
@property(nonatomic,strong)NSMutableArray* arr_re_genrte_OTP_additn;
@property(nonatomic,strong)NSString* registd_acct_id_trans;
@property(nonatomic,strong)NSString* sign_Up_chck;
@property(nonatomic,strong)NSString* Is_Otp_Required;
@property(nonatomic,strong)NSString* rev_transaction_type;
@property(nonatomic,strong)NSString* rev_Str_IBFT_POT;
@property(nonatomic,strong)NSString* rev_Str_IBFT_Relation;
@property(nonatomic,strong)NSString* relation_id;
@property(nonatomic,strong)NSMutableArray* arr_instant_p2p;
@property(nonatomic,strong)NSMutableArray* arr_franchise_data;
@property(nonatomic,strong)NSMutableArray* arr_franchise_data_selected;
@property(nonatomic,strong)NSString* franchise_index;
@property(nonatomic,strong)NSMutableArray* arr_franchise_data_OCL;
@property(nonatomic,strong)NSString* franchise_btn_chck;
@property(nonatomic,strong)NSString* acct_summary_section;
@property(nonatomic,strong)NSString* acct_summary_curr;
@property(nonatomic,strong)NSString* is_isntant_allow;
@property(nonatomic,strong)NSString* instant_qr_pin;
@property(nonatomic,strong)NSString* qr_prompt;
@property(nonatomic,strong)NSMutableArray* arr_new_signup;
@property(nonatomic,strong)NSString* cnic_no;
@property(nonatomic,strong)NSString* frist_last_debt_no;
@property(nonatomic,strong)NSString* sign_up_user;
@property(nonatomic,strong)NSString* chk_new_signup_back;
@property(nonatomic,strong)NSString* isFromOnlineShoping;
@property(nonatomic) BOOL isLogedIn;
@property(nonatomic) BOOL isIphoneX;
@property(nonatomic,strong) NSString* is_iPhoneX;
@property(nonatomic,strong)NSString* map_call_service;
@property(nonatomic) int noOfAvailableAttempts;
@property(nonatomic) int noOfPerformedAttempts;


@property(nonatomic,strong)NSString* touch_id_enable;
@property(nonatomic,strong)NSString* touch_id_Mpin;
@property(nonatomic,strong)NSString* Mpin_login;

@property(nonatomic,strong)NSString* From_Exis_login;
@property(nonatomic,strong)NSString* Exis_relation_id;

@property(nonatomic,strong)NSString* Otp_mobile_verifi;
@property(nonatomic,strong)NSString* conti_session_id;
 
@property(nonatomic,strong)NSMutableArray* arr_internet;
@property(nonatomic,strong)NSMutableArray* arr_postpaid;
@property(nonatomic,strong)NSMutableArray* arr_mobiletopup;
@property(nonatomic,strong)NSMutableArray* arr_bill_load_all;
@property(nonatomic,strong)NSMutableArray* arr_bill_ob;
@property(nonatomic,strong)NSMutableArray* arr_bill_isp;
@property(nonatomic,strong)NSMutableArray* arr_bill_ublbp;
@property(nonatomic,strong)NSString* indexxx;
@property(nonatomic,strong)NSMutableArray* arr_values;
@property(nonatomic,strong)NSMutableArray* arr_other_pay_list;
@property(nonatomic,strong)NSMutableArray* arr_payee_list;
@property(nonatomic,strong)NSString* chck_UBLBP_Bill_type;
@property(nonatomic,strong)NSString* bill_type;
@property(nonatomic,strong)NSString* bill_click;
@property(nonatomic,strong)NSString* payment_login_chck;
@property(nonatomic,strong)NSString* custom_alert_msg;
@property(nonatomic,strong)NSString* custom_alert_img;
@property(nonatomic,strong)NSString* chk_tezraftar;

@property(nonatomic,strong)NSMutableArray* arr_qr_code;
@property(nonatomic,strong)NSString* Udid;
@property(nonatomic,strong)NSString* Edit_bill_type;
@property(nonatomic,strong)NSString* Offer_id;
@property(nonatomic,strong)NSMutableArray* arr_Get_Detail_Desc;
@property(nonatomic,strong)NSString* ssl_pin_check;
@property(nonatomic,strong)NSMutableArray* arr_ph_address;
@property(nonatomic,strong)NSMutableArray* arr_add_fav;
@property(nonatomic,strong)NSMutableArray* arr_Get_add_fav;
@property(nonatomic,strong)NSString* offer_name;
@property(nonatomic,strong)NSString* arr_search_offer_chck;
@property(nonatomic,strong)NSMutableArray* arr_search_offer;
@property(nonatomic,strong)NSMutableArray* arr_prepaid;
@property(nonatomic,strong)NSMutableArray* arr_prepaid_data;
@property(nonatomic,strong)NSString* ppv_domination_id;
@property(nonatomic,strong)NSString* frm_acct_default_chck;
@property(nonatomic,strong)NSString* token;

@property(nonatomic,strong)NSMutableArray* arr_graph_name;
@property(nonatomic,strong)NSMutableArray* arr_get_direction;
@property(nonatomic,strong)NSMutableArray* arr_current_location;
@property(nonatomic,strong)NSString* exis_user_mobile_pin;
@property(nonatomic,strong)NSString* faq_index;
@property(nonatomic,strong)NSString* app_features;
@property(nonatomic,strong)NSMutableArray* locationDataArray;
@property(nonatomic,strong)NSMutableArray* arr_transfer_within_acct_to;
@property(nonatomic,strong)NSString* chck_transfer_other_acct_frm;
@property(nonatomic,strong)NSMutableArray* chck_arr_transfer_other_acct_frm;
@property(nonatomic,strong)NSMutableArray* arr_daily_limit;

@property(nonatomic, strong) NSString *vc;

//Pay Button click from Summary
@property(nonatomic,strong)NSString* direct_pay_frm_Acctsummary;
@property(nonatomic,strong)NSString* direct_pay_frm_Acctsummary_nick;
@property(nonatomic,strong)NSString* direct_pay_frm_Acctsummary_customer_id;
@property(nonatomic,strong)NSString* direct_pay_frm_Acctsummary_registed_customer_id;
@property(nonatomic,strong)NSString* ibft_relation;
@property CGPoint centrePoint;


@property(nonatomic,strong)NSString* summary_acct_no;

//Login table2 items

@property(nonatomic,strong)NSString* Mobile_No;
@property(nonatomic,strong)NSString* UserType;
@property(nonatomic,strong)NSString* base_currency;

// AddPayee for Fund Transfer
@property(nonatomic,strong)NSString*acc_type;
@property(nonatomic,strong)NSString*bankname;
@property(nonatomic,strong)NSString*bankimd;
@property(nonatomic,strong)NSString*fetchtitlebranchcode;
@property(nonatomic,strong)NSString*acc_number;
@property(nonatomic,strong)NSString* acctitle;
@property(nonatomic,strong)NSString* acct_nick;
// Settings
@property(nonatomic,strong)NSString* settingclick;


// @signup
@property(nonatomic,strong)NSString* signup_countrycode;

@property(nonatomic,strong)NSString* signup_acctype;
@property(nonatomic,strong)NSString* signup_relationshipid;
@property(nonatomic,strong)NSString* signup_activationcode;
@property(nonatomic,strong)NSString* signup_userid;
//@property(nonatomic,strong)NSString* signin_userid;
@property(nonatomic,strong)NSString* signup_username;


// @ accout creation
@property(nonatomic, strong) NSString *user_name_onbording;
@property(nonatomic, strong) NSString *cnic_onbording;
@property(nonatomic, strong) NSString *mobile_number_onbording;
@property(nonatomic, strong) NSString *branch_name_onbording;
@property(nonatomic, strong) NSString *selcted_branch_address;
@property(nonatomic, strong) NSString *selcted_branch_contact;
@property(nonatomic, strong) NSString *email_onbording;
@property(nonatomic, strong) NSString *nationality_onbording;
@property(nonatomic, strong) NSString *branch_code_onbording;
@property(nonatomic, strong) NSString *debitcard_name_onbording;
@property(nonatomic, strong) NSString *tracking_pin_onbording;
@property(nonatomic, strong) NSString *mode_of_meeting;
@property(nonatomic, strong) NSString *preferred_time;
@property(nonatomic, strong) NSString *request_id_onbording;
@property(nonatomic, strong) NSString *step_no_onbording;
@property(nonatomic, strong) NSString *step_no_help_onbording;
@property(nonatomic, strong) NSMutableArray *selected_services_onbording;
@property(nonatomic, strong) NSString *selected_account_onbording;
@property(nonatomic, strong) NSString *parent_vc_onbording;
@property(nonatomic ,strong) CLLocation *current_location_onbording;
@property(nonatomic, strong) NSMutableDictionary *getOnBoardProducts;
@property(nonatomic, strong) NSString *helpidentifier;
@property(nonatomic, strong) NSDictionary *accountStatus;
@property(nonatomic, strong) NSString *otpPin;
@property(nonatomic, strong) NSString *cnic;
@property(nonatomic) BOOL isAccountSelected;
@property(nonatomic, strong) NSMutableArray *arr_reset_otp_onbarding;
@property(nonatomic, strong) NSMutableArray *arr_get_On_Board_Additional;
@property(nonatomic, strong) NSString *sr_number;
@property(nonatomic) BOOL isHideAdditionalServices;
@property(nonatomic, strong) NSArray *rm_avalibilityTimings;
@property(nonatomic) int timmerLimitOnBording;
@property(nonatomic) int remainingTimeOnBording;
@property(nonatomic,strong)  NSMutableArray* arr_school_list;

@property BOOL isLocationSelected;
@property(nonatomic,strong)NSString* selctedLatitude;
@property(nonatomic,strong)NSString* selectedLongitude;

@property(nonatomic,strong)NSString* selcted_marker_title;

@property(nonatomic,strong)NSString* selected_city;
@property(nonatomic,strong)NSString* selcted_country;
@property(nonatomic,strong)NSString* selcted_branch_code;
@property(nonatomic,strong)NSString* retValue;
@property(nonatomic,strong)NSString* secureCode;


//@editpayee

@property(nonatomic,strong)NSString*  selectaccount;


//@Credit card from Summary
@property(nonatomic,strong)NSString*  direct_payccfrm_acct_id;
@property(nonatomic,strong)NSString*  direct_payccfrmCC_acct_id;
@property(nonatomic,strong)NSString*  direct_strCCY;


@property(nonatomic,strong)NSString*  summary_acct_name;
@property(nonatomic,strong)NSString*  login_session;


@property(nonatomic,strong)NSString*  pay_combobill_name;
@property(nonatomic,strong)NSString* locationCity;

@property(nonatomic,strong)NSString* mainurl1;
@property(nonatomic,strong)NSString* header_user;
@property(nonatomic,strong)NSString* header_pw;
@property(nonatomic,strong)NSString* ssl_pinning_url1;
@property(class)int requestDomain;


@property(nonatomic,strong)NSMutableArray *controllers_Refrence_Stack;
@property BOOL isRedirected;


// FBRPayment
@property(nonatomic, strong) NSString *fbrTcAccessKey;
@property(nonatomic, strong) NSString *fbrCompanyName;
@property(nonatomic, strong) NSString *fbrTypeName;
@property(nonatomic, strong) NSString *fbrAccessKey;
@property(nonatomic, strong) NSString *fbrPSID;
@property(nonatomic, strong) NSString *fbrAmount;
@property(nonatomic, strong) NSString *fbrTtId;
@property(nonatomic, strong) NSString *fbrRegisteredConsumerId;
@property(nonatomic, strong) NSString *fbrDueDate;
@property(nonatomic, strong) NSString *fbrRegisteredAccountId;
@property(nonatomic, strong) NSString *fbrConsumerNo;
@property(nonatomic, strong) NSString *fbrNick;
@property(nonatomic, strong) NSString *fbrBillId;
@property(nonatomic, strong) NSString *fbrOTPRequired;
@property(nonatomic, strong) NSString *fbrDescription;
@property(nonatomic, strong) NSString *fbrTransctionAmount;
@property(nonatomic, strong) NSString *fbrClientName;

@property(nonatomic, strong) NSString *strReturnMessage;

// VirtualWiz
@property(nonatomic, strong) NSString *wizLoadAmount;
@property(nonatomic, strong) NSString *wizRequestProfileNo;
@property(nonatomic, strong) NSString *wizTrasnctionType;
@property(nonatomic, strong) NSString *wizExistingCardDetails;
@property(nonatomic, strong) NSString *wizTransctionAmount;

// Tezraftaar
@property(nonatomic, strong) NSString *chk_tezraftaar_transfer;
@property(nonatomic, strong) NSArray *tz_accounts;
@property(nonatomic, strong) NSArray *tz_fees;
@property(nonatomic, strong) NSArray *tz_purpose;
@property(nonatomic, strong) NSString *tz_note;
@property(nonatomic, strong) NSMutableArray* tz_purposeOfTransfer;
@property(nonatomic, strong) NSString *tz_selectedAccountNo;
@property(nonatomic, strong) NSString* tezraftaarOTPRequired;
@property(nonatomic, strong) NSString* tezraftaarReturnMessage;
@property(nonatomic, strong) NSString* Offer_msg1;
@property(nonatomic, strong) NSString* setting_ver;
@property(nonatomic, strong) NSMutableArray *arr_tezraftar_relation;
@property(nonatomic, strong) NSMutableArray *arr_tezrftar_bene_additn;
@property(nonatomic, strong) NSString* default_acct_num;
@property(nonatomic, strong) NSMutableArray *arr_tezrftar_city;
@property(nonatomic, strong) NSMutableArray *arr_tezrftar_branch;
@property(nonatomic, strong) NSMutableArray *arr_tezrftar_bank;
@property(nonatomic, strong) NSString *chck_tezraftr_bene_type;
@property(nonatomic, strong) NSString *tz_pay_type;
@property(nonatomic, strong) NSString *tz_city;
@property(nonatomic, strong) NSString *beneficiary_Bank;

// Named pasteBoard
@property(nonatomic, strong) UIPasteboard *pasteBoard;

+(GlobalStaticClass *) getInstance;
+(NSString *) stringByStrippingHTML:(NSString*)txt;
+(NSString *) getPublicIp;
-(void) Pekaboo;

// TabBar Controls //
+(void) btn_offer;
+(void) btn_faq;
+(void) btn_findUs;
+(void) btn_feature;

@end
