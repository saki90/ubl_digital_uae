//
//  Transaction_History_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 19/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Transaction_History_VC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchDisplayDelegate,UISearchDisplayDelegate>
{
    IBOutlet UITableView* table;
    IBOutlet UIButton* btn_refresh;
    IBOutlet UIButton* btn_load_more;
    IBOutlet UIDatePicker* datt_picker;
    IBOutlet UILabel* lbl_dat_frm;
    IBOutlet UILabel* lbl_dat_to;
    
    IBOutlet UIView* vw_datt;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (nonatomic, strong) NSMutableArray *searchResult;

-(IBAction)btn_load_more:(id)sender;
-(IBAction)btn_refresh:(id)sender;
-(IBAction)btn_logout:(id)sender;
-(IBAction)btn_account:(id)sender;
-(IBAction)btn_more:(id)sender;
-(IBAction)btn_back:(id)sender;

-(IBAction)btn_datt_frm:(id)sender;
-(IBAction)btn_datt_to:(id)sender;
-(IBAction)btn_datt_cancel:(id)sender;
-(IBAction)btn_datt_done:(id)sender;
-(IBAction)btn_datt_click:(id)sender;

@end

