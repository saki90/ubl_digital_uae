//
//  Receive_qr_list.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 04/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"


@interface Receive_qr_list : UIViewController<UITableViewDelegate>
{
    IBOutlet UITableView* table;
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

