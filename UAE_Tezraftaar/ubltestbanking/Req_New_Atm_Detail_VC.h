//
//  Req_New_Atm_Detail_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 06/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Req_New_ATM_VC.h"
#import "Reachability.h"


@interface Req_New_Atm_Detail_VC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_gender;
    IBOutlet UIView* vw_marital;
    IBOutlet UIButton* btn_check_gender;
    IBOutlet UITableView* table;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}


-(IBAction)btn_gender:(id)sender;

@end
