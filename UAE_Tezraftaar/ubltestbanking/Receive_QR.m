//
//  Receive_QR.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Receive_QR.h"
#import "UIImage+QRCodeGenerator.h"
#import "Globals.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Receive_QR ()
{
    UIStoryboard *storyboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    NSArray* split;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    NSString* qr_txt;
    CGFloat heights;
    UIImageView *imgView;
    Encrypt *encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) IBOutlet UIView *barcodeView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *accounttitle;
@property (strong, nonatomic) IBOutlet UILabel *accountNumber;


@end



@implementation Receive_QR
@synthesize transitionController;
@synthesize imageView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
    
    ssl_count = @"0";
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    heights = 0.0;
    
    
    if (height == 568)
    {
        heights=118;
    }
    
    
    //http://www.oscarsanderson.com
    
    
    
    //    if ([gblclass.chk_qr_demo_login isEqualToString:@"1"])
    //    {
    //         qr_txt=@"#m303332392279m3#|1056880|Syed Bashir";//[NSString stringWithFormat:@"#m3%@m3#|%@|%@",gblclass.is_default_acct_no,gblclass.is_default_acct_id,gblclass.is_default_acct_id_name];
    //
    //        gblclass.chk_qr_demo_login=@"0";
    //    }
    //    else
    //    {
    //        qr_txt =[NSString stringWithFormat:@"#m3%@m3#|%@|%@",gblclass.is_default_acct_no,gblclass.is_default_acct_id,gblclass.is_default_acct_id_name];
    //    }
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
    }
    
    
    
    
    
    //    UIGraphicsBeginImageContext(self.view.frame.size);
    //    [[UIImage imageNamed:@"qr_img_bg.png"] drawInRect:self.view.bounds];
    //    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //    self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
    
    
    
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame: self.view.bounds];
    //    [imageView setAutoresizingMask: (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)];
    //    [imageView setImage: [UIImage imageNamed: @"qr_img_bg.png"]];
    //  //  [self.view addSubview: imageView];
    //      self.view.backgroundColor = [UIColor colorWithPatternImage:imageView];
    
    
    
    
    
    
    
    
    // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_img_bg.png"]];
    
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_back:(id)sender {
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    //    [self dismissModalStack];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}



-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:YES completion:nil];
}



-(void) QRCODE_GENERATE:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSLog(@"%@",gblclass.arr_transfer_within_acct);
        
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:[gblclass.arr_qr_index integerValue]] componentsSeparatedByString: @"|"];
        
        NSString* br_code;
        if ([[split objectAtIndex:9] isEqualToString:@""] || [[split objectAtIndex:9] isEqualToString:@" "])
        {
            br_code=@"0";
        }
        else
        {
            br_code=[split objectAtIndex:9];
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:br_code],
                                    [encrypt encrypt_Data:[split objectAtIndex:1]],
                                    [encrypt encrypt_Data:@"after"], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strDevice_Id",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strBranchCode",
                                                                       @"strAccountNumber",
                                                                       @"place", nil]];
        
        NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GenerateQRCode" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  if([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      //#m3p2p#|" + account_name + "|" + account_no + "|#m3p2p#
                      //    qr_txt=[NSString stringWithFormat:@"#m3p2p#|%@|#m3p2p#",[dic2 objectForKey:@"QRCode"]];
                      
                      
                      qr_txt=[NSString stringWithFormat:@".%@",[dic2 objectForKey:@"QRCode"]];
                      imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 ,heights ,300,300)];
                      
                      
                      imgView.image = [UIImage QRCodeGenerator:qr_txt
                                                andLightColour:[UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0]
                                                 andDarkColour:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
                                                  andQuietZone:1
                                                       andSize:300];
                      
                      
                      
                      NSString* ab = [NSString stringWithFormat:@"%lu", (unsigned long)[[split objectAtIndex:1] length]];
                      
                      NSString*  subStr = [[split objectAtIndex:1] substringWithRange:NSMakeRange([ab integerValue]-4,4)];
                      
                      
                      self.imageView.image = imgView.image;
                      self.accounttitle.text = [NSString stringWithFormat:@"UBL Account Title. %@",[[split objectAtIndex:0] uppercaseString]];
                      self.accountNumber.text = [NSString stringWithFormat:@"UBL Account No. *******%@",subStr];
                      self.accounttitle.hidden=NO;
                      self.accountNumber.hidden=NO;
                      
                      //                    [self.view addSubview:imgView];
                      //                    [self.view addSubview:img_logo];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic2 objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  NSLog(@"%@",error);
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        
        NSLog(@"%@",exception.reason);
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            //
            
            [self custom_alert:statusString :@"0"];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
////    if ([self isSSLPinning])
////    {
////        [self printMessage:@"Making pinned request"];
////    }
////    else
////    {
////        [self printMessage:@"Making non-pinned request"];
////    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self.connection cancel];
            [self QRCODE_GENERATE:@""];
        }
        
        
        chk_ssl=@"";    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self.connection cancel];
        [self QRCODE_GENERATE:@""];
    }
    
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark -UIActivityController Sharing -

- (IBAction)shareButton:(UIButton *)sender
{
    UIImage *barCodeImage = self.imageView.image;
    
    NSArray *objectsToShare = @[barCodeImage];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


- (IBAction)saveButton:(id)sender
{
    
    
    UIImage *barcodeImage = [self captureView];
    UIImageWriteToSavedPhotosAlbum(barcodeImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    //        NSArray *dirPath = NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES);
    //        NSString *dir = [[dirPath objectAtIndex:0] stringByAppendingFormat:@"/UBL/%@.jgp", gblclass.user_login_name];
    //        NSData *imageData = UIImageJPEGRepresentation(imgView.image, 1.0);
    //        NSError *writeError = nil;
    //        [imageData writeToFile:dir atomically:YES];
    
    
    //        if(![imageData writeToFile:dir options:NSDataWritingAtomic error:&writeError])
    //        {
    //            NSLog(@"%@: Error saving image: %@", [self class], [writeError localizedDescription]);
    
    //            [self custom_alert:@"QR Code image save successfully" :@"1"];
    //        }
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    if (error == nil) {
        [self custom_alert:@"QR Code image saved successfully" :@"1"];
    } else {
        [self custom_alert:@"QR Code image save unsuccessful" :@"0"];
    }
    
}

- (UIImage *)captureView {
    
    //hide controls if needed
    CGRect rect = [self.barcodeView bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.barcodeView.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

@end

