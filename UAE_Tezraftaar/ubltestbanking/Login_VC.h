//
//  Login_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import <AVFoundation/AVFoundation.h>
 
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>

@interface Login_VC : UIViewController<UITextFieldDelegate,UIPageViewControllerDataSource,CLLocationManagerDelegate>
{
    //IBOutlet UITextField* txt_login;
    IBOutlet UIButton* btn_login_color;
    IBOutlet UIButton* btn_signup_round;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UITextField* txt_Mpin;
    IBOutlet UITextField* txt_pw1;
    IBOutlet UIButton* btn_check_mail;
    IBOutlet UIButton* btn_agree;
    IBOutlet UIScrollView* vw_term;
    IBOutlet UIView* vw_apply_acct;
    IBOutlet UIView* vw_tracking;
   
    IBOutlet UIView* vw_apply_for_acct_line;
    IBOutlet UIView* vw_login_line;
    IBOutlet UIView *activateApplicationView;
    IBOutlet UIButton* btn_tracking;
    IBOutlet UIView* vw_btn_tracking_line;
    IBOutlet UIButton* btn_continue;
    IBOutlet UIView* vw_btn_continue_line;
    IBOutlet UIButton* btn_cancel;
    IBOutlet UIView* vw_btn_cancel_line;
    
    
    NSMutableData* responseData;
    
    __weak IBOutlet UITextField *emailOutlet;
    __weak IBOutlet UIView *emailView;
    
    __weak IBOutlet UITextField *passOutlet;
    __weak IBOutlet UIView *passView;
    
    __weak IBOutlet UIButton *forgetPass;
    __weak IBOutlet UIButton *nxt_outlet;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    IBOutlet UIButton* btn_resend_otp;
    
}




- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;



@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (nonatomic, strong) TransitionDelegate *transitionController;


@property(nonatomic,strong) IBOutlet UITextField* txt_login;
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton1;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) NSString *photoFilename;





-(IBAction)btn_next:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Amounttextfield;
@property (weak, nonatomic) IBOutlet UITextField *fundtextfield;

-(IBAction)btn_menu:(id)sender;
-(IBAction)btn_Signup:(id)sender;
-(IBAction)btn_feature:(id)sender;
-(IBAction)btn_offer:(id)sender;
-(IBAction)btn_find_us:(id)sender;
-(IBAction)btn_faq:(id)sender;

@end

