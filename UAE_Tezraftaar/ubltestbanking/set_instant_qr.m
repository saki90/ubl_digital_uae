//
//  set_instant_qr.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 13/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "set_instant_qr.h"
#import "GlobalClass.h"
#import "Globals.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"


@interface set_instant_qr ()
{
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UISwitch *mySwitch;
    NSString* chk_switch;
    NSString* chk_ssl;
    NSString* str_chk;
    Encrypt* encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation set_instant_qr
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    
    //   chk_switch = [[NSUserDefaults standardUserDefaults] stringForKey:@"ui_switch"];
    
    ssl_count = @"0";
    str_chk=@"";
    if([gblclass.is_isntant_allow isEqualToString:@"1"])
    {
        NSLog(@"Switch is ON");
        str_chk=@"1";
        lbl_switch.text=@"Turn my Instant QR Payment On";
        [myswitch setOn:YES];
        
    }
    else if([gblclass.is_isntant_allow isEqualToString:@"0"])
    {
        NSLog(@"Switch is OFF");
        [myswitch setOn:NO];
        str_chk=@"0";
        lbl_switch.text=@"Turn my Instant QR Payment Off";
    }
    else
    {
        str_chk = @"0";
    }
    
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
//                                    gblclass.story_board bundle:[NSBundle mainBundle]];
//        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:myController animated:YES completion:nil];
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    [myswitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_submit:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"submit";
        [self SSL_Call];
    }
}


- (void)switchChanged:(UISwitch *)sender
{
    // Do something
    // BOOL value = sender.on;
    
    mySwitch = (UISwitch *)sender;
    
    
    if ([mySwitch isOn])
    {
        str_chk=@"1";
        gblclass.Chk_instant_qr_payment=@"on";
        [mySwitch setOn:YES];
        lbl_switch.text=@"Turn my Instant QR Payment On";
        
    } else
    {
        str_chk=@"0";
        gblclass.Chk_instant_qr_payment=@"off";
        [mySwitch setOn:NO];
        lbl_switch.text=@"Turn my Instant QR Payment Off";
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"submit"])
        {
            chk_ssl=@"";
            [self set_QR_Instant:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
        [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"submit"])
    {
        chk_ssl=@"";
        [self set_QR_Instant:@""];
        [self.connection cancel];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) set_QR_Instant:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token],[encrypt encrypt_Data:str_chk], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"userid",@"strSessionId",@"IP",@"strDeviceid",@"Token",@"isEnableQR", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    //  generate_QRCODE_PIN
    [manager POST:@"setQRInstant" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              NSDictionary*   dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [hud hideAnimated:YES];
                  
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  gblclass.is_isntant_allow=str_chk;
                  
                  [self slide_right];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              [self custom_alert:@"Retry" :@"0"];
              
          }];
    
}




-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"abcd"],[encrypt encrypt_Data:gblclass.token], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strDeviceID",@"strSessionId",@"IP",@"hCode",@"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
    }
    else if(buttonIndex == 0)
    {
        [self slide_right];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
        
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else
    {
        
    }
}


-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


- (IBAction)changeSwitch:(id)sender
{
    
    [self switchToggled:sender];
    
    //    if([sender isOn])
    //    {
    //        NSLog(@"Switch is ON");
    //        gblclass.Chk_instant_qr_payment=@"on";
    //        lbl_switch.text=@"Turn my Instant QR Payment On";
    //    }
    //    else
    //    {
    //        NSLog(@"Switch is OFF");
    //        gblclass.Chk_instant_qr_payment=@"off";
    //        lbl_switch.text=@"Turn my Instant QR Payment Off";
    //    }
    
}


- (void) switchToggled:(id)sender
{
    mySwitch = (UISwitch *)sender;
    
    
    if ([mySwitch isOn])
    {
        str_chk=@"1";
        gblclass.Chk_instant_qr_payment=@"on";
        [mySwitch setOn:YES];
        lbl_switch.text=@"Turn my Instant QR Payment On";
        
    } else
    {
        str_chk=@"0";
        gblclass.Chk_instant_qr_payment=@"off";
        [mySwitch setOn:NO];
        lbl_switch.text=@"Turn my Instant QR Payment Off";
    }
    
    //    [[NSUserDefaults standardUserDefaults] setObject:str_chk forKey:@"ui_switch"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
}

-(IBAction)btn_back:(id)sender
{
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //    [self slide_right];
    //
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    //    [self presentViewController:vc animated:NO completion:nil];
}


@end

