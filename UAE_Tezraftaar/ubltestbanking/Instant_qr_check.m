//
//  Instant_qr_check.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 31/03/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Instant_qr_check.h"
#import "GlobalStaticClass.h"
#import "Globals.h"
#import "GlobalClass.h"
#import "UIImage+QRCodeGenerator.h"
#import "MBProgressHUD.h"

@interface Instant_qr_check ()
{
    UIStoryboard *storyboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    NSArray* split;
    MBProgressHUD *hud;
    NSString* qr_txt;
    NSString* qr_img_name;
    UIImageView *imgView;
    
}

@property (strong, nonatomic) IBOutlet UIView *barcodeView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *accounttitle;
@property (strong, nonatomic) IBOutlet UILabel *accountNumber;

@end

@implementation Instant_qr_check
//@synthesize imgView;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    @try {
        
        
        gblclass=[GlobalStaticClass getInstance];
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        [hud showAnimated:YES];
        
        split = [[NSArray alloc] init];
        
        NSLog(@"%@",gblclass.instant_credit_chk);
        NSLog(@"%@",gblclass.instant_debit_chk);
        NSLog(@"%@",gblclass.arr_instant_qr);
        
        
        vw_social.hidden=NO;
        vw_normal.hidden=YES;
        
        lbl_heading.text=@"RECEIVE MONEY";
        
        lbl_name.hidden=NO;
        
        if ([gblclass.instant_credit_chk isEqualToString:@"1"] && [gblclass.instant_debit_chk isEqualToString:@"1"])
        {
            btn_receive.enabled=YES;
            btn_send.enabled=YES;
        }
        else if ([gblclass.instant_credit_chk isEqualToString:@"1"] &&  [gblclass.instant_debit_chk isEqualToString:@"0"])
        {
            btn_receive.enabled=YES;
            btn_send.enabled=NO;
        }
        else if ([gblclass.instant_credit_chk isEqualToString:@"0"] &&  [gblclass.instant_debit_chk isEqualToString:@"1"])
        {
            btn_receive.enabled=NO;
            btn_send.enabled=YES;
        }
        else if ([gblclass.instant_credit_chk isEqualToString:@"0"] &&  [gblclass.instant_debit_chk isEqualToString:@"0"])
        {
            btn_receive.enabled=NO;
            btn_send.enabled=NO;
        }
        else
        {
            btn_receive.enabled=NO;
            btn_send.enabled=NO;
        }
        
        
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        CGFloat heights = 0.0;
        
        
        if (height == 568)
        {
            heights=118;
        }
        
        
        NSLog(@"%@",gblclass.arr_instant_qr);
        
        
        split = [[gblclass.arr_instant_qr objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        qr_img_name = [split objectAtIndex:1];
        
        //  qr_txt=@"#m303332392279m3#|1056880|Bashirsyed [03332392279]";//[NSString stringWithFormat:@"#m3%@m3#|%@|%@",gblclass.is_default_acct_no,gblclass.is_default_acct_id,gblclass.is_default_acct_id_name];
        
        
        //  qr_txt=[NSString stringWithFormat:@"#m3p2p#|%@|#m3p2p#",[split objectAtIndex:2]];
        
        
        qr_txt=[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
        
        
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 ,heights ,300,300)];
        
        
        imgView.image = [UIImage QRCodeGenerator:qr_txt
                                  andLightColour:[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0]
                                   andDarkColour:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
                                    andQuietZone:1
                                         andSize:300];
        
        
        NSString* ab = [NSString stringWithFormat:@"%lu", (unsigned long)[[split objectAtIndex:3] length]];
        
        NSString*  subStr = [[split objectAtIndex:3] substringWithRange:NSMakeRange([ab integerValue]-4,4)];
        
        
        //   NSString*  subStr = [[split objectAtIndex:4] substringWithRange:NSMakeRange(8,3)];
        
        self.imageView.image = imgView.image;
        self.accounttitle.text = [NSString stringWithFormat:@"UBL Account Title. %@",[[split objectAtIndex:1] uppercaseString]];
        self.accountNumber.text = [NSString stringWithFormat:@"UBL Account No. *******%@",subStr];
        self.accounttitle.hidden=NO;
        self.accountNumber.hidden=NO;
        
        
        //  [self.view addSubview:imgView];
        //  [self.view addSubview:img_logo];
        
        //  [self.view addSubview:img_logo];
        
        
        vw_black.backgroundColor=[UIColor colorWithRed:19/255.0 green:20/255.0 blue:20/255.0 alpha:0.6];
        
        self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    //    [self dismissModalStack];
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_send_money:(id)sender
{
    
    gblclass.chck_qr_send=@"0";
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr_login"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(IBAction)btn_receive_money:(id)sender
{
    
    vw_social.hidden=NO;
    vw_normal.hidden=YES;
    
    lbl_heading.text=@"QR CODE";
    gblclass.chck_qr_send=@"0";
    
    lbl_name.hidden=NO;
    
    
    imgView.image = [UIImage QRCodeGenerator:qr_txt
                              andLightColour:[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0]
                               andDarkColour:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
                                andQuietZone:1
                                     andSize:300];
    
    NSString* ab = [NSString stringWithFormat:@"%lu", (unsigned long)[[split objectAtIndex:3] length]];
    
    NSString*  subStr = [[split objectAtIndex:3] substringWithRange:NSMakeRange([ab integerValue]-4,4)];
    
    self.imageView.image = imgView.image;
    self.accounttitle.text = [NSString stringWithFormat:@"UBL Account Title. %@",[[split objectAtIndex:1] uppercaseString]];
    self.accountNumber.text = [NSString stringWithFormat:@"UBL Account No. *******%@",subStr];
    self.accounttitle.hidden=NO;
    self.accountNumber.hidden=NO;
    
    
    //    imgView.image = [UIImage QRCodeGenerator:qr_txt
    //                              andLightColour:[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0]
    //                               andDarkColour:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]
    //                                andQuietZone:1
    //                                     andSize:300];
    //
    //    [self.view addSubview:imgView];
    
    
    vw_black.backgroundColor=[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0];
    
    
    
    //    CATransition *transition = [ CATransition animation];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromRight;
    //    [self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"receive_qr_login"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
    
    
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


#pragma mark -UIActivityController Sharing -

- (IBAction)shareButton:(UIButton *)sender
{
    @try {
        
        UIImage *barCodeImage = self.imageView.image;
        
        NSArray *objectsToShare = @[barCodeImage];
        
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        [self presentViewController:activityVC animated:YES completion:nil];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


- (IBAction)saveButton:(id)sender
{
    
    UIImage *barcodeImage = [self captureView];
    UIImageWriteToSavedPhotosAlbum(barcodeImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    
    //        NSArray *dirPath = NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES);
    //        NSString *dir = [[dirPath objectAtIndex:0] stringByAppendingFormat:@"/UBL/%@.jgp", gblclass.user_login_name];
    //        NSData *imageData = UIImageJPEGRepresentation(imgView.image, 1.0);
    //        NSError *writeError = nil;
    //        [imageData writeToFile:dir atomically:YES];
    
    
    //        if(![imageData writeToFile:dir options:NSDataWritingAtomic error:&writeError])
    //        {
    //            NSLog(@"%@: Error saving image: %@", [self class], [writeError localizedDescription]);
    
    //            [self custom_alert:@"QR Code image save successfully" :@"1"];
    //        }
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    if (error == nil)
    {
        [self custom_alert:@"QR Code image saved successfully" :@"1"];
    }
    else
    {
        [self custom_alert:@"QR Code image save unsuccessful" :@"0"];
    }
    
}

- (UIImage *)captureView
{
    
    //hide controls if needed
    CGRect rect = [self.barcodeView bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.barcodeView.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}



//- (IBAction)saveButton:(id)sender
//{
//    UIImageWriteToSavedPhotosAlbum(imgView.image, nil, nil, nil);
//
//        NSArray *dirPath = NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES);
//        NSString *dir = [[dirPath objectAtIndex:0] stringByAppendingFormat:@"/UBL/%@.jgp", qr_img_name];
//        NSData *imageData = UIImageJPEGRepresentation(imgView.image, 1.0);
//        NSError *writeError = nil;
//        [imageData writeToFile:dir atomically:YES];
//
//
//        if(![imageData writeToFile:dir options:NSDataWritingAtomic error:&writeError])
//        {
//            NSLog(@"%@: Error saving image: %@", [self class], [writeError localizedDescription]);
//            [self custom_alert:@"QR Code image save successfully" :@"1"];
//        }
//}

@end

