//
//  Login_New.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 27/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Login_New.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
 
#import "SWRevealViewController.h"
#import "Slide_menu_VC.h"
#import "TransitionDelegate.h"
#import <UIKit/UIKit.h>
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "FBEncryptorAES.h"
#import <Security/Security.h>
#import <CoreLocation/CoreLocation.h>
#import "Broken.h"
#import <sys/utsname.h>
#import <PeekabooConnect/PeekabooConnect.h>
#import "Peekaboo.h"

#import "Globals.h"
#import "Encrypt.h"

#define btn @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

@interface Login_New ()<CLLocationManagerDelegate>
{
    UIAlertController *alert;
    MBProgressHUD* hud;
    NSString* login_status_chck;
    GlobalStaticClass* gblclass;
    NSProgress *mine;
   // Password_VC* txt_pw;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSDictionary *dic;
    NSString *login_name;
    NSString *login_pw;
    NSInteger randomNumber ;
    NSString* btn_forgot_chck;
    NSMutableArray*  a;
    NSString* Pw_status_chck;
    NSString* touch_id;
    NSString* user,* pass;
    CLLocationManager *myLcationManager;
    NSString* btn_next_press;
    UIStoryboard *storyboard;
    NSString* tch_chk;
    NSString* chk_ssl;
    NSString* device_jail;
    UIImage *btn_chck_Image;
    NSString* t_n_c;
    NSString* device_jb_chk;
    NSString* btn_TnC_cancel_press;
    NSString* debit_lock_chck;
    Encrypt* encrypt;
    NSString* req_id,*apply_acct;
    NSUserDefaults *defaults;
    Broken* brk_jb;
    NSString* btn_chk;
    NSString* ssl_count;
    NSString *myLocalizedReasonString_1;
    NSString* session_id;
    NSString* lati_pekaboo,*longi_pekaboo;
    NSString* get_udid;
    NSString* chk_pekaboo_service_hit;
    NSString* login_chk;
    NSUserDefaults *userDefaults;
    NSString* chck_pekaboo_service_call;
}



- (IBAction)MakeHTTPRequestTapped;
-(IBAction)btn_tch:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UIButton *btn_touchID;


@end


@implementation Login_New
static UIView* loadingView = nil;


//BOOL isIphoneX (void) {
//    static BOOL isIphoneX = NO;
//    static dispatch_once_t onceToken;
//
//    dispatch_once(&onceToken, ^{
//
//#if TARGET_IPHONE_SIMULATOR
//        NSString *model = NSProcessInfo.processInfo.environment[@"SIMULATOR_MODEL_IDENTIFIER"];
//#else
//
//        struct utsname systemInfo;
//        uname(&systemInfo);
//
//        NSString *model = [NSString stringWithCString:systemInfo.machine
//                                             encoding:NSUTF8StringEncoding];
//#endif
//        isIphoneX = [model isEqualToString:@"iPhone10,3"] || [model isEqualToString:@"iPhone10,6"];
//        NSLog(@"%@", model);
//    });
//
//    return isIphoneX;
//}



- (void)viewDidLoad
{
    
    [super viewDidLoad];
    //    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    @try {
        
        gblclass=[GlobalStaticClass getInstance];
      
        
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        // [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self.view endEditing:YES];
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        defaults = [NSUserDefaults standardUserDefaults];
        [[UITextField appearance] setTintColor:[UIColor whiteColor]];
        userDefaults  = NSUserDefaults.standardUserDefaults;
        
        //brk_jb = [[Broken alloc] init];
        
        session_id = @"";
        encrypt = [[Encrypt alloc] init];
        debit_lock_chck = @"no";
        chck_pekaboo_service_call =@"";
        vw_tch_view.hidden=YES;
        vw_without_tch_view.hidden=NO;
        gblclass.isLogedIn=NO;
//      gblclass.vc = @"";
        ssl_count = @"0";
        gblclass.landing = @"";
        btn_changeto_tch_ID.layer.cornerRadius = 5;
        
//        [self.view endEditing:YES];
        
        // [self encrypt_Data:@"sa"]; OK SAQIB
        // NSLog(@"%@",brk_jb.isDeviceJailbroken);
        
        login_chk = @"";
        chk_ssl=@"";
        tch_chk=@"";
        gblclass.is_debit_lock = @"0";
        gblclass.setting_chck_Tch_ID = @"0";
        
        btn_next_press=@"0";
        
        txt_pw1.delegate=self;
        txt_login.delegate=self;
        txt_tch_pw.delegate=self;
        txt_tch_login.delegate=self;
        
        a=[[NSMutableArray alloc] init];
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageNamed:@"login-bg.png"] drawInRect:self.view.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        
        
        vw_login_line.layer.borderColor = [UIColor whiteColor].CGColor;
        vw_login_line.layer.cornerRadius = 8;
        vw_login_line.layer.borderWidth = 1.0f;
        
        emailView.layer.borderColor = [UIColor whiteColor].CGColor;
        emailView.layer.cornerRadius = 8;
        emailView.layer.borderWidth = 1.0f;
        
        passView.layer.borderColor = [UIColor whiteColor].CGColor;
        passView.layer.cornerRadius = 8;
        passView.layer.borderWidth = 1.0f;
        
        nxt_outlet.layer.cornerRadius=5;
        
        
        
        gblclass.TnC_calling_source = @"login";
        
        //   UIFont *font = [UIFont fontWithName:@"Aspira-Light" size:14];
        lbl_heading.text=@"Please enter your netbanking username and password.";
        
        
        //  Enable/Disable touch login 17 march 2017
        //saki disable 18 jan    [self getCredentialsForServer_touch:@"finger_print"];
        
        
        //        btn_apply_for_acct.backgroundColor = [UIColor whiteColor];
        //        btn_apply_for_acct.layer.cornerRadius=9;
        
        
        NSString* first_time_chk = [[NSUserDefaults standardUserDefaults] stringForKey:@"enable_touch"];
        
        if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
        {
            btn_apply_for_acct.hidden = YES;
            vw_apply_for_acct_line.hidden = YES;
        }
        else
        {
            btn_apply_for_acct.hidden = NO;
            vw_apply_for_acct_line.hidden = NO;
        }
        
//        NSString* deviceName() {
//            struct utsname systemInfo;
//            uname(&systemInfo);
//            return [NSString stringWithCString:systemInfo.machine
//                                      encoding:NSUTF8StringEncoding];
//        }
        
       // NSLog(@"%@",gblclass.is_iPhoneX);
        
//        if ([gblclass.is_iPhoneX isEqualToString:@"iPhone X"])
//        {
//           // NSLog(@"Iphonex");
//        }

        
        //gblclass.is_iPhoneX = @"iPhone X";
       // if ([gblclass.is_iPhoneX isEqualToString:@"iPhone X"])
        if ([gblclass.story_board isEqualToString:@"iPhone_X"] || [gblclass.story_board isEqualToString:@"iPhone_Xr"])
        {
            myLocalizedReasonString_1 = @"Face ID";
            [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"faceID"] forState:UIControlStateNormal];
            //Set a new (x,y) point for the button's center
            
            if ([gblclass.story_board isEqualToString:@"iPhone_X"])
            {
              //  self.btn_touchID.center = CGPointMake(375/2, 6);
            }
            else  if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
            {
              //  self.btn_touchID.center = CGPointMake(414/2, 330);
            }
            else
            {
                self.btn_touchID.center = CGPointMake(320/2, 296);
            }
            
            
          //  [self.view addSubview:self.btn_touchID];
            [btn_changeto_tch_ID setTitle:@"Change to Face ID" forState:UIControlStateNormal];
            
        }
        else
        {
            myLocalizedReasonString_1 = @"Touch ID";
            [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"touchID"] forState:UIControlStateNormal];
            [btn_changeto_tch_ID setTitle:@"Change to Touch ID" forState:UIControlStateNormal];
        }
        
//        if (gblclass.isIphoneX) {
//            myLocalizedReasonString = @"Face ID";
//            [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"faceID"] forState:UIControlStateNormal];
//            [btn_changeto_tch_ID setTitle:@"Change to Face ID" forState:UIControlStateNormal];
//
//        } else {
//            myLocalizedReasonString = @"Touch ID";
//            [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"touchID"] forState:UIControlStateNormal];
//            [btn_changeto_tch_ID setTitle:@"Change to Touch ID" forState:UIControlStateNormal];
//        }
        
        
        
//        if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
//            NSLog(@"%i",(int)[[UIScreen mainScreen] nativeBounds].size.height);
//
//            switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
//
//                case 2436: //2436 // 1704
//                    printf("iPhone X");
//                    myLocalizedReasonString = @"Face ID";
//                    [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"faceID"] forState:UIControlStateNormal];
//                    [btn_changeto_tch_ID setTitle:@"Change to Face ID" forState:UIControlStateNormal];
//                    break;
//                default:
//                    myLocalizedReasonString = @"Touch ID";
//                    [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"touchID"] forState:UIControlStateNormal];
//                    [btn_changeto_tch_ID setTitle:@"Change to Touch ID" forState:UIControlStateNormal];
//                    printf("unknown");
//            }
//        }
        
        [self getCredentialsForServer_touch:@"finger_print"];
        
        
        //        [self getCredentialsForServer:@"login"];
        //        [self getCredentialsForServer:@"touch_enable"];
        //     // [self getCredentialsForServer_finger_print:@"touch_enable"];
        //        [self getCredentialsForServer_touch:@"touch_enable"];
        
        //if ([touch_id isEqualToString:@"1"])
        
        
      //  defaults = [NSUserDefaults standardUserDefaults];
//        get_udid = [defaults valueForKey:@"get_udid"];
//
//        if ([get_udid intValue]< 2)
//        {
//            [hud showAnimated:YES];
//            [self.view addSubview:hud];
//
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl = @"get_udid";
//                [self SSL_Call];
//            }
//        }
        
        if ([tch_chk isEqualToString:@"1"])
        {
            
            btn_changeto_tch_ID.hidden = NO;
            
            // Get Credential from Keychain ::
            [self getCredentialsForServer:@"login"];
            //[self getCredentialsForServer:@"touch_enable"];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            get_udid = [defaults valueForKey:@"get_udid"];

            if ([get_udid intValue]< 3)
            {
                [self.view addSubview:hud];
                [hud showAnimated:YES];

                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl = @"get_udid";
                    [self SSL_Call];
                }
            }
            
        }
        else
        {
            btn_changeto_tch_ID.hidden = YES;
        }
        

        //NSLog(@"%@",gblclass.device_chck);
      //defaults = [NSUserDefaults standardUserDefaults];
        
        t_n_c = [defaults valueForKey:@"t&c"];
        
        NSUserDefaults *default_jb = [NSUserDefaults standardUserDefaults];
        device_jb_chk = [default_jb valueForKey:@"isDeviceJailBreaked"];
        
        if ([tch_chk isEqualToString:@"1"])
        {
            
          //  NSLog(@"%@",gblclass.device_chck);
            if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
            {
                device_jail = @"yes";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
            }
            else if ([device_jb_chk isEqualToString:@"0"] && [t_n_c isEqualToString:@"1"])
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
            {
                device_jail = @"yes";
                vw_JB.hidden = NO;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else if ([t_n_c isEqualToString:@"0"])
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = NO;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else if ([t_n_c isEqualToString:@"1"])
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = YES;
                nxt_outlet.enabled=YES;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = YES;
                
                btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden=YES;
                btn_next.enabled = YES;
                nxt_outlet.enabled=YES;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = YES;
                
                //btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
                //[btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            
        }
        else
        {
           // NSLog(@"%@",device_jb_chk);
            //            if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
            //            {
            //                device_jail = @"yes";
            //                vw_JB.hidden=YES;
            //                vw_tNc.hidden=YES;
            //                btn_next.enabled=NO;
            //                btn_next_jb.enabled=YES;
            //                btn_resend_otp.enabled=NO;
            //
            //                btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
            //                [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
            //
            //                [btn_check_JB_rooted setImage:btn_chck_Image forState:UIControlStateNormal];
            //            }
            if ([device_jb_chk isEqualToString:@"0"] && [t_n_c isEqualToString:@"1"])
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                btn_next.enabled = NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                nxt_outlet.enabled=NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
            }
            else if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
            {
                device_jail = @"yes";
                vw_JB.hidden = NO;
                vw_tNc.hidden = YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
            {
                device_jail = @"yes";
                vw_JB.hidden = NO;
                vw_tNc.hidden = YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = NO;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            else if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@""])
            {
                device_jail = @"yes";
                vw_JB.hidden = NO;
                vw_tNc.hidden = YES;
                btn_next.enabled = NO;
                nxt_outlet.enabled=NO;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = NO;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setImage:btn_chck_Image forState:UIControlStateNormal];
                
            }
            //            else if ([device_jb_chk isEqualToString:@"1"])
            //            {
            //                device_jail = @"yes";
            //                vw_JB.hidden = NO;
            //                vw_tNc.hidden = YES;
            //                btn_next.enabled = NO;
            //                btn_next_jb.enabled = YES;
            //                btn_resend_otp.enabled = NO;
            //
            //                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
            //                [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
            //
            //                [btn_check_JB_rooted setImage:btn_chck_Image forState:UIControlStateNormal];
            //
            //            }
            //            else if ([t_n_c isEqualToString:@"0"])
            //            {
            //                device_jail = @"no";
            //                vw_JB.hidden = YES;
            //                vw_tNc.hidden = NO;
            //                btn_next.enabled = NO;
            //                btn_next_jb.enabled = NO;
            //                btn_resend_otp.enabled = NO;
            //
            //                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
            //                [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
            //
            //                [btn_check_JB_rooted setImage:btn_chck_Image forState:UIControlStateNormal];
            //
            //            }
            else if ([t_n_c isEqualToString:@"1"])
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                btn_next.enabled = YES;
                nxt_outlet.enabled=YES;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = YES;
                
                btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
                [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
                
                [btn_check_JB_rooted setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
            }
            else
            {
                device_jail = @"no";
                vw_JB.hidden = YES;
                vw_tNc.hidden = YES;
                vw_tNc_tch.hidden = YES;
                btn_next.enabled = YES;
                nxt_outlet.enabled=YES;
                btn_next_jb.enabled = YES;
                btn_resend_otp.enabled = YES;
                
                //btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
                //[btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
            }
        }
        
        //Animation
        
        //      lbl_heading.frame = CGRectMake(self.view.frame.size.width + lbl_heading.frame.size.width, lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        
        
        vw_tch_view.frame = CGRectMake(self.view.frame.size.width + vw_tch_view.frame.size.width, vw_tch_view.frame.origin.y, vw_tch_view.frame.size.width, vw_tch_view.frame.size.height);
        
         vw_without_tch_view.frame = CGRectMake(self.view.frame.size.width + vw_without_tch_view.frame.size.width, vw_without_tch_view.frame.origin.y, vw_without_tch_view.frame.size.width, vw_without_tch_view.frame.size.height);
        
        vw_apply_acct.frame = CGRectMake(self.view.frame.size.width + vw_apply_acct.frame.size.width, vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
        
        vw_tracking.frame = CGRectMake(self.view.frame.size.width + vw_tracking.frame.size.width, vw_tracking.frame.origin.y, vw_tracking.frame.size.width, vw_tracking.frame.size.height);
        
        [NSTimer scheduledTimerWithTimeInterval:0.2
                                         target:self
                                       selector:@selector(showAnimation)
                                       userInfo:nil
                                        repeats:NO];
        
        
//        [self Apply_acct_buttons];
        [self Play_bg_video];
        
        [self playerStartPlaying];
        
        
        
        UIColor *color = [UIColor lightTextColor];
           txt_login.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Login" attributes:@{NSForegroundColorAttributeName: color}];
           
           txt_pw1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
        
        
//                if ([CLLocationManager locationServicesEnabled])
//                {
//
//                    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
//
//                        myLcationManager = [[CLLocationManager alloc] init];
//                        [myLcationManager requestAlwaysAuthorization];
//                        myLcationManager.distanceFilter = kCLDistanceFilterNone;
//                        myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//                        myLcationManager.delegate = self;
//                        [myLcationManager startUpdatingLocation];
//                    }
//
//                    else
//                    {
//
//                        myLcationManager = [[CLLocationManager alloc] init];
//                        myLcationManager.distanceFilter = kCLDistanceFilterNone;
//                        myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//                        myLcationManager.delegate = self;
//                        [myLcationManager startUpdatingLocation];
//
//                    }
//                }
////                else
////                {
////
////                    [self custom_alert:@"Please enable your location services in your settings" :@"0"];
////
////                }
//
//
//        myLcationManager = [[CLLocationManager alloc] init];
//        myLcationManager.delegate = self;
//        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
//        if ([myLcationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
//        {
//            [myLcationManager requestWhenInUseAuthorization];
//        }
//
//        [myLcationManager startUpdatingLocation];
        
        
        
        
        if ([CLLocationManager locationServicesEnabled])
        {
            
            //NSLog(@"Location Services Enabled");
            
            if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
                
                myLcationManager = [[CLLocationManager alloc] init];
                [myLcationManager requestAlwaysAuthorization];
                myLcationManager.distanceFilter = kCLDistanceFilterNone;
                myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
                myLcationManager.delegate = self;
                [myLcationManager startUpdatingLocation];
            }
            else
            {
                
                myLcationManager = [[CLLocationManager alloc] init];
                myLcationManager.distanceFilter = kCLDistanceFilterNone;
                myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
                myLcationManager.delegate = self;
                [myLcationManager startUpdatingLocation];
                
            }
        }
        
        
        // ** Don't forget to add NSLocationWhenInUseUsageDescription in MyApp-Info.plist and give it a string
        
        myLcationManager = [[CLLocationManager alloc] init];
        myLcationManager.delegate = self;
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
        if ([myLcationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [myLcationManager requestWhenInUseAuthorization];
        }
        [myLcationManager startUpdatingLocation];
        
        
        
        //Hidden Apply Account buttons ::
        
        vw_apply_acct.hidden = YES;
        vw_tracking.hidden = YES;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
        
        
 //9 july 2018
//        [self checkinternet];
//        if (netAvailable)
//        {
//            [self pekaboo];
//        }
        
      //saki 22 nov
     //    [self Apply_acct_buttons];

        
        //saki 29 nov 2019
//        [NSTimer scheduledTimerWithTimeInterval:0.3
//                                         target:self
//                                       selector:@selector(pekaboo_apply_acct)
//                                       userInfo:nil
//                                        repeats:NO];
//
//    }
//    @catch (NSException *exception)
//    {
//        //NSLog(@"%@",exception);
//    }
        
        
        
//        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
//        [self.view addSubview:hud];
//        NSString *info = [NSString stringWithFormat:@"Loading %@ gauges", self.stateIdentifier];
//        [hud setLabelText:info];
//        [hud setDetailsLabelText:@"Please wait..."];
//        [hud setDimBackground:YES];
//        [hud setOpacity:0.5f];
//        [hud show:YES];
//        [hud hide:YES afterDelay:10.0];
        
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
 
    }
}

//- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [[self view] endEditing:YES];
//}

-(void)App_version
{
   // NSLog(@"saki Appversion");
    //[self getCredentialsForServer_touch:@"finger_print"];
  //  NSLog(@"%@",tch_chk);
    
    NSString *App_Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *Build_Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    NSString* chck_app = [NSString stringWithFormat:@"%@%@",App_Version,Build_Version];
    
    //CFBundleVersion
    //CFBundleShortVersionString
    
    NSString *previousVersion = [userDefaults objectForKey:@"buildVersion"];
    
    if (!previousVersion)
    {
        login_chk = @"1";
        // first launch
        [userDefaults setObject:chck_app forKey:@"buildVersion"];
        [userDefaults synchronize];
    }
    else if ([previousVersion isEqualToString:chck_app])
    {
        // same version
        login_chk = @"0";
       // NSLog(@"SAME VERSION.!");
    }
    else
    {
        // other version
        
        login_chk = @"1";
        [userDefaults setObject:chck_app forKey:@"buildVersion"];
        [userDefaults synchronize];
    }
    
}



-(void) pekaboo_apply_acct
{
    
    [self checkinternet];
    if (netAvailable)
    {
        [self pekaboo];
    }
    
    //        [self checkinternet];
    //        if (netAvailable)
    //        {
    //            [hud showAnimated:YES];
    //            [self.view addSubview:hud];
    //            [self pekaboo];
    //        }
    
}

-(void)Apply_acct_buttons
{
    req_id = [encrypt de_crypt_url:[defaults valueForKey:@"req_id"]];
    apply_acct = [defaults valueForKey:@"apply_acct"];
    
    
   // apply_acct = @"1";
    //req_id = @"234";
    if ([req_id isEqualToString:@""])
    {
        vw_apply_acct.hidden = NO;
        vw_tracking.hidden = YES;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    else if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"1"]) //Tracking
    {
        vw_apply_acct.hidden = YES;
        vw_tracking.hidden = NO;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = NO;
        vw_btn_tracking_line.hidden = NO;
    }
    else if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"0"]) //Conti & Cancel
    {
        vw_apply_acct.hidden=YES;
        vw_tracking.hidden=NO;
        
        btn_continue.hidden = NO;
        vw_btn_continue_line.hidden = NO;
        btn_cancel.hidden = NO;
        vw_btn_cancel_line.hidden = NO;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    else
    {
        vw_apply_acct.hidden=NO;
        vw_tracking.hidden=YES;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // [self getCredentialsForServer_touch:@"finger_print"];
    
    txt_login.text = @"";
    txt_pw1.text = @"";
    
    //    if ([gblclass.parent_vc_onbording containsString:@"mismatched"]) {
    //        [self custom_alert:gblclass.parent_vc_onbording :@"0"];
    //    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    //    [[UITextField appearance] setTintColor:[UIColor blackColor]];
}

-(void) showAnimation {
    
//    [UIView animateWithDuration:0.4 animations:^{
//        lbl_heading.frame = CGRectMake((self.view.frame.size.width/2) - (lbl_heading.frame.size.width/2), lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
//
//    }];
    
    
    [UIView animateWithDuration:0.6 animations:^{
        vw_tch_view.frame = CGRectMake((self.view.frame.size.width/2) - (vw_tch_view.frame.size.width/2), vw_tch_view.frame.origin.y, vw_tch_view.frame.size.width, vw_tch_view.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        vw_without_tch_view.frame = CGRectMake((self.view.frame.size.width/2) - (vw_without_tch_view.frame.size.width/2), vw_without_tch_view.frame.origin.y, vw_without_tch_view.frame.size.width, vw_without_tch_view.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        vw_apply_acct.frame = CGRectMake((self.view.frame.size.width/2) - (vw_apply_acct.frame.size.width/2), vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
        
    }];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        vw_tracking.frame = CGRectMake((self.view.frame.size.width/2) - (vw_tracking.frame.size.width/2), vw_tracking.frame.origin.y, vw_tracking.frame.size.width, vw_tracking.frame.size.height);
        
    }];

    
}


- (NSString *)obfuscate:(NSString *)string withKey:(NSString *)key
{
    // Create data object from the string
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get pointer to data to obfuscate
    char *dataPtr = (char *) [data bytes];
    
    // Get pointer to key data
    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    // Points to each char in sequence in the key
    char *keyPtr = keyData;
    int keyIndex = 0;
    
    // For each character in data, xor with current value in key
    for (int x = 0; x < [data length]; x++)
    {
        // Replace current character in data with
        // current character xor'd with current key value.
        // Bump each pointer to the next character
        *dataPtr = *dataPtr++ ^ *keyPtr++;
        
        // If at end of key data, reset count and
        // set key pointer back to start of key value
        if (++keyIndex == [key length])
            keyIndex = 0, keyPtr = keyData;
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}



- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [myLcationManager requestAlwaysAuthorization];
    }
}

//-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"ATTENTION" message:@"Please enable your location to use new feature in app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [errorAlert show];
//    NSLog(@"Error: %@",error.description);
//}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *crnLoc = [locations lastObject];
    //    latitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.latitude];
    //    longitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.longitude];
    //    altitude.text = [NSString stringWithFormat:@"%.0f m",crnLoc.altitude];
    //    speed.text = [NSString stringWithFormat:@"%.1f m/s", crnLoc.speed];
    
    gblclass.arr_current_location = [[NSMutableArray alloc] init];
    float latitude = myLcationManager.location.coordinate.latitude;
    float longitude = myLcationManager.location.coordinate.longitude;
    
    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
    
//
//    NSLog(@"%@",locations.lastObject);
//
//    NSLog(@"%f",crnLoc.coordinate.latitude);
//    NSLog(@"%f",crnLoc.coordinate.longitude);
    
    
    lati_pekaboo = lati;
    longi_pekaboo = longi;
    
    gblclass.lati_pekabo = lati;
    gblclass.longi_pekabo = longi;
    
    //NSLog(@"lati %@",lati);
    //NSLog(@"lati %@",longi);
    
    [gblclass.arr_current_location addObject:lati];
    [gblclass.arr_current_location addObject:longi];
    
    [manager stopUpdatingLocation];
    
    if ([chck_pekaboo_service_call isEqualToString:@""])
    {
            chck_pekaboo_service_call = @"1";
            [self checkinternet];
            if (netAvailable)
            {
                [self pekaboo];
            }
    }
    
}



//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
//{
//
//    gblclass.arr_current_location = [[NSMutableArray alloc] init];
//    float latitude = myLcationManager.location.coordinate.latitude;
//    float longitude = myLcationManager.location.coordinate.longitude;
//
//    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
//    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
//    [gblclass.arr_current_location addObject:lati];
//    [gblclass.arr_current_location addObject:longi];
//
//    lati_pekaboo = lati;
//    longi_pekaboo = longi;
//
//    [myLcationManager stopUpdatingLocation];
//
//
//
//    CLGeocoder *geocoder2 = [[CLGeocoder alloc] init] ;
//    [geocoder2 reverseGeocodeLocation:myLcationManager.location
//                    completionHandler:^(NSArray *placemarks, NSError *error) {
//                        //NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
//
//                        if (error){
//                            //NSLog(@"Geocode failed with error: %@", error);
//                            return;
//
//                        }
//
//
//                        CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                        //  NSString * location = ([placemarks count] > 0) ? [[placemarks objectAtIndex:0] locality] : @"Not Found";
//
//
//
//                        //NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
//                        //NSLog(@"placemark.country %@",placemark.country);
//                        //NSLog(@"placemark.postalCode %@",placemark.postalCode);
//                        //NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
//                        //NSLog(@"placemark.locality %@",placemark.locality);
//
//
//                        gblclass.locationCity = placemark.locality;
//                        //[self getBranchesInfo:placemark.locality];
//
//
//                        //NSLog(@"placemark.subLocality %@",placemark.subLocality);
//                        //NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
//
//                    }];
//
//    //NSLog(@"%f",latitude);
//    //NSLog(@"%f",longitude);
//
//}



//*********************** Login credential Start ******************************



-(void) saveUsername:(NSString*)user withPassword:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, user, kSecAttrAccount, nil];
    
    
    
    dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, user, kSecAttrAccount, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


-(void) removeAllCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
}



-(void) getCredentialsForServer_finger_print:(NSString*)server
{
    @try {
        
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
}


-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try
    {
        
     // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
        
//        // Create dictionary of search parameters
//        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
//
//        // Look up server in the keychain
//        NSDictionary* found = nil;
//        CFDictionaryRef foundCF;
//        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
//
//        // Check if found
//        found = (__bridge NSDictionary*)(foundCF);
//        if (!found)
//            return;
//
//        // Found
//        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
//
//        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
//
//        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
//
//        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}






-(void) getCredentialsForServer:(NSString*)server
{
    @try {
        
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //27 march 2017
        //    user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        user=@"";
        
        pass = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}



-(void) getCredentialsForServer_header:(NSString*)server
{
    @try {
        
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        gblclass.header_user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        gblclass.header_pw = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}




//*********************** Login credential Start ******************************







-(void)Play_bg_video {
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [self.avplayer play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (BOOL) isTouchIDAvailable
{
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
        //NSLog(@"%@", [authError localizedDescription]);
        return NO;
    }
    
    return YES;
}


-(void)viewDidAppear:(BOOL)animated
{
  
  //NSLog(@"%@",gblclass.payment_login_chck);
    [self.view endEditing:YES];
    
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    //   if ([gblclass.payment_login_chck isEqualToString:@"1"])
    //   {
    
    //touch_id
    
    [self App_version];
    
    if ([tch_chk isEqualToString:@"2"] && [login_chk isEqualToString:@"1"])
    {
        
        //first_time
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"enable_touch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else if ([tch_chk isEqualToString:@"0"] && [login_chk isEqualToString:@"1"])
    {
        //mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
       // vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        //[self presentViewController:vc animated:NO completion:nil];
    }
    
    
    @try {
        
        if ([tch_chk isEqualToString:@"1"])
        {
            //  btn_touch_id.hidden=NO;
 
            
            vw_tch_view.hidden = NO;
            vw_without_tch_view.hidden = YES;
            
//            if ([gblclass.story_board isEqualToString:@"iPhone_X"] || [gblclass.story_board isEqualToString:@"iPhone_Xr"])
//            {
//                myLocalizedReasonString_1 = @"Face ID";
//                [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"faceID"] forState:UIControlStateNormal];
//                //Set a new (x,y) point for the button's center
//
//                if ([gblclass.story_board isEqualToString:@"iPhone_X"])
//                {
//                    //  self.btn_touchID.center = CGPointMake(375/2, 6);
//                }
//                else  if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
//                {
//                    //  self.btn_touchID.center = CGPointMake(414/2, 330);
//                }
//                else
//                {
//                    self.btn_touchID.center = CGPointMake(320/2, 296);
//                }
//
//
//                //  [self.view addSubview:self.btn_touchID];
//                [btn_changeto_tch_ID setTitle:@"Change to Face ID" forState:UIControlStateNormal];
//
//            }
//            else
//            {
//                myLocalizedReasonString_1 = @"Touch ID";
//                [self.btn_touchID setBackgroundImage:[UIImage imageNamed:@"touchID"] forState:UIControlStateNormal];
//                [btn_changeto_tch_ID setTitle:@"Change to Touch ID" forState:UIControlStateNormal];
//            }
//
            
            
            
            
            
            
            //        LAContext *myContext = [[LAContext alloc] init];
            //        NSError *authError = nil;
            //        NSString *myLocalizedReasonString = @"Touch ID ";
            //
            //        if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
            //        {
            //            [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
            //                      localizedReason:myLocalizedReasonString
            //                                reply:^(BOOL success, NSError *error) {
            //
            //                                    if (success) {
            //                                        dispatch_async(dispatch_get_main_queue(), ^{
            //                                            // [self performSegueWithIdentifier:@"Success" sender:nil];
            //
            //
            //                                            [self.view addSubview:hud];
            //                                           [hud showAnimated:YES];
            //
            //                                            //                                                [self checkinternet];
            //                                            //                                                if (netAvailable)
            //                                            //                                                {
            //
            //                                            btn_next_press=@"0";
            //
            //                                            chk_ssl=@"login";
            //                                            [self SSL_Call];
            //                                            //                                                }
            //
            //                                        });
            //
            //                                    } else {
            //                                        dispatch_async(dispatch_get_main_queue(), ^{
            ////                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
            ////                                                                                                message:error.localizedDescription
            ////                                                                                               delegate:self
            ////                                                                                      cancelButtonTitle:@"OK"
            ////                                                                                      otherButtonTitles:nil, nil];
            //                                            //  [alertView show];
            //                                            //NSLog(@"Switch to fall back authentication - ie, display a keypad or password entry box");
            //                                        });
            //                                    }
            //                                }];
            //        }
            //        else
            //        {
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //
            //
            //                [self custom_alert:authError.localizedDescription :@"0"];
            //
            ////                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
            ////                                                                    message:authError.localizedDescription
            ////                                                                   delegate:self
            ////                                                          cancelButtonTitle:@"OK"
            ////                                                          otherButtonTitles:nil, nil];
            ////                [alertView show];
            //
            //
            //                // btn_touch_id.hidden=YES;
            //
            //            });
            //        }
            
            
        } 
    } @catch (NSException *exception) {
        [self custom_alert:exception.reason :@"0"];
    }
    
    
    
    //} //payment login check..
    
    
    [hud hideAnimated:YES];
    
}

///********************** SIGN IN NEW METHOD 27 OCT 2016 ********************************



-(IBAction)BTN_SIGN_IN_NEW:(id)sender
{
    [self.view endEditing:YES];

    //saki 19 Nov 2018
//    if ([btn_TnC_cancel_press isEqualToString:@"yes"])
//    {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Please Accept Term & Condition" :@"0"];
//        return;
//    }
    
    btn_next_press=@"1";
    
    if ([tch_chk isEqualToString:@"1"])
    {
        
        //        NSString *Str_user = [txt_tch_login.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        //        NSString *Str_pw = [txt_tch_pw.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        
        NSString *Str_user = [txt_login.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        NSString *Str_pw = [txt_pw1.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        
        if ([Str_user isEqualToString:@""] || [Str_user isEqualToString:@" "])
        {
            //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter User Name" preferredStyle:UIAlertControllerStyleAlert];
            //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //        [alert addAction:ok];
            //
            //        [self presentViewController:alert animated:YES completion:nil];
            
            [self custom_alert:@"Please enter valid Login Name" :@"0"];
            
            return ;
            
        }
        else if([Str_pw isEqualToString:@""] || [Str_pw isEqualToString:@" "])
        {
            //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Password" preferredStyle:UIAlertControllerStyleAlert];
            //
            //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //        [alert addAction:ok];
            //
            //        [self presentViewController:alert animated:YES completion:nil];
            
            
            [self custom_alert:@"Enter Password" :@"0"];

            return ;
        }
        else
        {
            [hud showAnimated:YES];
            [hud hideAnimated:YES afterDelay:130];
            
            //  [self checkking_test];
            //  [self mob_Sign_In:@""];
            
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"login";
                [self SSL_Call];
            }
        }
        
    }
    else
    {
        
        NSString *Str_user = [txt_login.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        NSString *Str_pw = [txt_pw1.text stringByReplacingOccurrencesOfString:@" " withString: @""];
        
        if ([Str_user isEqualToString:@""] || [Str_user isEqualToString:@" "])
        {
            //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter User Name" preferredStyle:UIAlertControllerStyleAlert];
            //
            //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //        [alert addAction:ok];
            //
            //        [self presentViewController:alert animated:YES completion:nil];
            
            [self custom_alert:@"Please enter valid Login Name" :@"0"];
            
            return ;
            
        }
        else if([Str_pw isEqualToString:@""] || [Str_pw isEqualToString:@" "])
        {
            //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Password" preferredStyle:UIAlertControllerStyleAlert];
            //
            //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //        [alert addAction:ok];
            //
            //        [self presentViewController:alert animated:YES completion:nil];
            
            
            [self custom_alert:@"Please enter valid Password" :@"0"];
            
            return ;
        }
        else
        {
            [hud showAnimated:YES];
            [hud hideAnimated:YES afterDelay:130];
            
//            [hud setDetailsLabelText:@"Please wait..."];
//            [hud setDimBackground:YES];
//            [hud setOpacity:0.5f];
//            [hud show:YES];
//            [hud hide:YES afterDelay:3.0];
            
            //  [self checkking_test];
            //  [self mob_Sign_In:@""];
            
          //  NSLog(@"%@",gblclass.TnC_chck_On);
            
            t_n_c = @"1"; //saki 9 march
            
            if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"login";
                    [self SSL_Call];
                }
            }
            else if ([t_n_c isEqualToString:@"0"])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Accept Term & Condition" :@"0"];
            }
            else
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"login";
                    [self SSL_Call];
                }
            }
            
            
        }
    }
}



-(void) mob_Sign_In:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    ///   manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    // manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    gblclass.arr_transfer_within_acct_deposit_chck = [[NSMutableArray alloc] init];
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    gblclass.arr_transfer_within_acct_to=[[NSMutableArray alloc] init];
    
    NSDictionary *dictparam;
    
    gblclass.sign_in_username = txt_login.text;
    gblclass.sign_in_pw = txt_pw1.text;
    
    //NSLog(@"%@",gblclass.Udid);
    
    
    //touch_id
    if ([tch_chk isEqualToString:@"1"])
    {
        //NSLog(@"%lu",(unsigned long)user.length);
        
        //        txt_login.text = txt_tch_login.text;
        //        txt_pw1.text = txt_tch_pw.text;
        
        
        //27 march 2017
        //        if ([user isEqualToString:@""] || user.length==0)
        //        {
        //            [hud hideAnimated:YES];
        //
        //            [self custom_alert:@"Touch Id not working properly, Please try again later." :@"0"];
        //
        ////            alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Touch Id not working properly, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        ////            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        ////            [alert addAction:ok];
        ////            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        
        
        
        if ([pass isEqualToString:@""] || pass.length==0)
        {
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Touch Id not working properly, Please try again later." :@"0"];
            
            return;
        }
        
        
        //public void mobSignInRooted(string LoginName, string strPassword, string strDeviceID, string strSessionId, string IP, string hCode, string isTouchLogin, Int16 isRooted, string isTnCAccepted)
        
        
        //        t_n_c = @"0";
        //        gblclass.device_chck = @"0";
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        if ([btn_next_press isEqualToString:@"1"])
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:txt_login.text],
                          [encrypt encrypt_Data:txt_pw1.text],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:@"1234"],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:gblclass.setting_ver],
                          [encrypt encrypt_Data:@"0"],
                          [encrypt encrypt_Data:gblclass.device_chck],
                          [encrypt encrypt_Data:t_n_c], nil]
                                                    forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                             @"strPassword",
                                                             @"strDeviceID",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"hCode",
                                                             @"isTouchLogin",
                                                             @"isRooted",
                                                             @"isTnCAccepted", nil]];
        }
        else
        {
            //frm finger :: user
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:@""],
                          [encrypt encrypt_Data:pass],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:@"1234"],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:gblclass.setting_ver],
                          [encrypt encrypt_Data:@"1"],
                          [encrypt encrypt_Data:gblclass.device_chck],
                          [encrypt encrypt_Data:t_n_c], nil]
                                                    forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                             @"strPassword",
                                                             @"strDeviceID",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"hCode",
                                                             @"isTouchLogin",
                                                             @"isRooted",
                                                             @"isTnCAccepted", nil]];
        }
        
    }
    else
    {
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:txt_login.text],
                      [encrypt encrypt_Data:txt_pw1.text],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:@"1234"],
                      [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                      [encrypt encrypt_Data:gblclass.setting_ver],
                      [encrypt encrypt_Data:@"0"],
                      [encrypt encrypt_Data:gblclass.device_chck],
                      [encrypt encrypt_Data:t_n_c], nil]
                                                forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                         @"strPassword",
                                                         @"strDeviceID",
                                                         @"strSessionId",
                                                         @"IP",
                                                         @"hCode",
                                                         @"isTouchLogin",
                                                         @"isRooted",
                                                         @"isTnCAccepted", nil]];
    }
    
    // NSLog(@"%@",dictparam);
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"mobSignInRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         @try {
             
             //NSError *error;
             //NSArray *arr = (NSArray *)responseObject;
             
             //             NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             //             NSString *jsonString = [encrypt de_crypt_Data:response];
             //             NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
             //             id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             
             dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             NSDictionary *dic1 = dic;
             
             NSMutableArray*  aa = [[NSMutableArray alloc] init];
             //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
             
             gblclass.sign_Up_chck = @"no";
             gblclass.From_Exis_login = @"0"; // signup check
             
             //NSLog(@"%@",gblclass.actsummaryarr);
             if ([[dic objectForKey:@"Response"] integerValue]==-2)
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
             {
                 [hud hideAnimated:YES];
                 
                 gblclass.forget_pw_btn_click=@"1"; //0 saki 12 april 2019
                 gblclass.forgot_pw_click = @"1";
                 
                 alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                 [alert addAction:ok];
                 
                 //777   [self presentViewController:alert animated:YES completion:nil];
                  
                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                 
                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //change_pw
                 //777                   [self presentViewController:vc animated:NO completion:nil];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
             {
                 
                 [hud hideAnimated:YES];
             // saki 6 aug 2019   [self custom_alert:@"forgot pass" :@"0"];
                 
                 gblclass.forget_pw_btn_click=@"1"; //0 saki 12 april 2019
                 gblclass.forgot_pw_click = @"1";
                 
                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
                 [self presentViewController:vc animated:NO completion:nil];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
             {
                 [hud hideAnimated:YES];
                 txt_pw1.text = @"";
                 [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                 
                 return ;
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-50)
             {
                 [hud hideAnimated:YES];
                 [self showAlert:@"Your login password has been changed you have to signup again." :@"Attention"];
                 
                 
                 gblclass.token=@"";
                 Pw_status_chck=@"";
                 gblclass.T_Pin=@"";
                 gblclass.welcome_msg=@"";
                 gblclass.user_login_name=@"";
                 gblclass.daily_limit=@"";
                 gblclass.monthly_limit=@"";
                 gblclass.user_id=@"";
                 gblclass.is_default_acct_id=@"";
                 gblclass.is_default_acct_id_name=@"";
                 gblclass.is_default_acct_no=@"";
                 gblclass.is_default_m3_balc=@"";
                 gblclass.registd_acct_id_trans=@"";
                 
                 [gblclass.arr_transfer_within_acct_deposit_chck removeAllObjects];
                 [gblclass.arr_transfer_within_acct removeAllObjects];
                 [gblclass.arr_transfer_within_acct_to removeAllObjects];
                 [gblclass.arr_transaction_history removeAllObjects];
                 
                 NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
                 [removeUD removeObjectForKey:@"Touch_ID"];
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 NSUserDefaults * first_1 = [NSUserDefaults standardUserDefaults];
                 [first_1 removeObjectForKey:@"enable_touch"]; //first_time
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 NSUserDefaults * first_2 = [NSUserDefaults standardUserDefaults];
                 [first_2 removeObjectForKey:@"second_time"];
                 [[NSUserDefaults standardUserDefaults]synchronize ];
                 
                 storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                 [self presentViewController:vc animated:YES completion:nil];
                 
                 return ;
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==101)
             {
                 [a removeAllObjects];
                 
                 [self.avplayer pause];
                 
                 gblclass.token = [dic objectForKey:@"Token"];
                 Pw_status_chck = [dic objectForKey:@"Response"];
                 gblclass.T_Pin = [dic objectForKey:@"pinMessage"];
                 gblclass.welcome_msg = [dic objectForKey:@"strWelcomeMesage"];
                 gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                 gblclass.daily_limit = [dic objectForKey:@"strDailyLimit"];
                 gblclass.monthly_limit = [dic objectForKey:@"strMonthlyLimit"];
               //saki  gblclass.user_id=[dic objectForKey:@"strUserId"];
                 gblclass.is_qr_payment = [dic objectForKey:@"is_QRPayment"];
                 gblclass.is_isntant_allow = [dic objectForKey:@"isInstantAllow"];
                 
                 
                 //NSLog(@"%@",[[dic objectForKey:@"strMonthlyLimit"] objectForKey:@"objdtUserInfo"]);
                 
                // NSLog(@"%@",gblclass.daily_limit);
                // NSLog(@"%@",gblclass.monthly_limit);
                 
                 //NSLog(@"%@",[dic objectForKey:@"userId"]);
                // gblclass.user_id=[dic objectForKey:@"userId"];
                 
                 gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                 
                 //gblclass.actsummaryarr =
                 NSArray* array= [[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                 gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                 gblclass.relation_id=[[array objectAtIndex:0]objectForKey:@"RelationshipID"];
                 gblclass.franchise_btn_chck=[[array objectAtIndex:0]objectForKey:@"isFA"];
                 
                // NSLog(@"%@",gblclass.franchise_btn_chck);
                 
                 gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                 //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                 gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                 
                 //                 NSString *mobileno_ =[[array objectAtIndex:0]objectForKey:@"userId"];
                 
                 NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 if(mobileno==(NSString *)[NSNull null])
                 {
                     gblclass.Mobile_No =@"";
                 }
                 else
                 {
                     gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                 }
                 
                 gblclass.arr_user_pw_table2 = [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                 gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];

                 NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                 
                 if (Email ==(NSString *) [NSNull null])
                 {
                     Email=@"N/A";
                     [a addObject:Email];
                 }
                 else
                 {
                     // [a addObject:Email];
                     gblclass.user_email=Email;
                 }
                 
                 NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                 
                 if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                 {
                     FundsTransferEnabled=@"N/A";
                     [a addObject:FundsTransferEnabled];
                 }
                 else
                 {
                     // [a addObject:FundsTransferEnabled];
                 }
                 
                 NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                 
                 if (Mobile_No == (NSString *)[NSNull null])
                 {
                     Mobile_No=@"N/A";
                     [a addObject:Mobile_No];
                 }
                 else
                 {
                     // [a addObject:Mobile_No];
                 }
                 
                 //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                 //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                 //              [a removeAllObjects];
                 //              bbb=@"";
                 
                 
                 //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                 
                 gblclass.actsummaryarr =
                 [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                 
                 //NSLog(@"%@",gblclass.actsummaryarr);
                 
                 
                 //     NSUInteger *set;
                 for (dic in gblclass.actsummaryarr)
                 {
                     
                     NSString* acct_name=[dic objectForKey:@"account_name"];
                     
                     if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                     {
                         acct_name=@"N/A";
                         [a addObject:acct_name];
                     }
                     else
                     {
                         [a addObject:acct_name];
                     }
                     
                     
                     NSString* acct_no=[dic objectForKey:@"account_no"];
                     if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                     {
                         acct_no=@"N/A";
                         [a addObject:acct_no];
                     }
                     else
                     {
                         [a addObject:acct_no];
                     }
                     
                     
                     NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                     if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                     {
                         acct_id=@"N/A";
                         [a addObject:acct_id];
                     }
                     else
                     {
                         [a addObject:acct_id];
                     }
                     
                     NSString* is_default=[dic objectForKey:@"is_default"];
                     NSString* is_default_curr=[dic objectForKey:@"ccy"];
                     NSString* is_default_name=[dic objectForKey:@"account_name"];
                     NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                     NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                     NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                     NSString* is_account_type=[dic objectForKey:@"account_type"];
                     NSString* is_br_code=[dic objectForKey:@"br_code"];
                     
                     
                     //privileges
                     NSString* privileges=[dic objectForKey:@"privileges"];
                     //777         NSString* pri=[dic objectForKey:@"privileges"];
                     privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //(1,1) //Second bit 1 for withDraw ..
                     
                     
                     if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"1"])
                     {
                         
                         gblclass.is_default_acct_id=acct_id;
                         gblclass.is_default_acct_id_name=is_default_name;
                         gblclass.is_default_acct_no=is_default_acct_no;
                         gblclass.is_default_m3_balc=is_default_balc;
                         gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                         gblclass.is_default_currency=is_default_curr;
                         gblclass.is_default_acct_type=is_account_type;
                         gblclass.is_default_br_code=is_br_code;
                         
//                         NSLog (@"%@",acct_id);
//                         NSLog(@"%@",is_default_name);
//                         NSLog(@"%@",is_default_acct_no);
//                         NSLog(@"%@",is_default_balc);
//                         NSLog(@"%@",is_default_regstd_acct_id);
//                         NSLog(@"%@",is_default_curr);
//                         NSLog(@"%@",is_account_type);
//                         NSLog(@"%@",is_br_code);
                         
                     }
                     else if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"0"])
                     {
                         debit_lock_chck = @"yes";
                         
                         gblclass.is_debit_lock = @"1";
                         
                         gblclass.is_default_acct_id=acct_id;
                         gblclass.is_default_acct_id_name=is_default_name;
                         gblclass.is_default_acct_no=is_default_acct_no;
                         gblclass.is_default_m3_balc=is_default_balc;
                         gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                         gblclass.is_default_currency=is_default_curr;
                         gblclass.is_default_acct_type=is_account_type;
                         gblclass.is_default_br_code=is_br_code;
                         
//                         NSLog(@"%@",acct_id);
//                         NSLog(@"%@",is_default_name);
//                         NSLog(@"%@",is_default_acct_no);
//                         NSLog(@"%@",is_default_balc);
//                         NSLog(@"%@",is_default_regstd_acct_id);
//                         NSLog(@"%@",is_default_curr);
//                         NSLog(@"%@",is_account_type);
//                         NSLog(@"%@",is_br_code);
                     }
                     
                     
                     if ([privileges isEqualToString:@"0"])
                     {
                         privileges=@"0";
                         [a addObject:privileges];
                     }
                     else
                     {
                         [a addObject:privileges];
                     }
                     
                     NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                     if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                     {
                         account_type_desc=@"N/A";
                         [a addObject:account_type_desc];
                     }
                     else
                     {
                         [a addObject:account_type_desc];
                     }
                     //account_type
                     
                     NSString* account_type=[dic objectForKey:@"account_type"];
                     if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                     {
                         account_type=@"N/A";
                         [a addObject:account_type];
                     }
                     else
                     {
                         [a addObject:account_type];
                     }
                     
                     
                     NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                     
                     
                     //((NSString *)[NSNull null] == m3_balc)
                     //[m3_balc isEqualToString:@" "] ||
                     
                     
                     //27 OCt New Sign IN Method :::
                     
                     if (m3_balc ==(NSString *) [NSNull null])
                     {
                         m3_balc=@"0";
                         [a addObject:m3_balc];
                     }
                     else
                     {
                         [a addObject:m3_balc];
                     }
                     
                     
                     NSString* branch_name=[dic objectForKey:@"branch_name"];
                     if ( branch_name == (NSString *)[NSNull null])
                     {
                         branch_name=@"N/A";
                         [a addObject:branch_name];
                     }
                     else
                     {
                         [a addObject:branch_name];
                     }
                     
                     NSString* available_balance=[dic objectForKey:@"available_balance"];
                     if ( available_balance == (NSString *)[NSNull null])
                     {
                         available_balance=@"0";
                         [a addObject:available_balance];
                     }
                     else
                     {
                         [a addObject:available_balance];
                     }
                     
                     NSString* br_code=[dic objectForKey:@"br_code"];
                     if ( br_code == (NSString *)[NSNull null])
                     {
                         br_code=@"0";
                         [a addObject:br_code];
                     }
                     else
                     {
                         [a addObject:br_code];
                     }
                     
                     NSString* ccy=[dic objectForKey:@"ccy"];
                     if ( ccy == (NSString *)[NSNull null])
                     {
                         ccy=@"0";
                         [a addObject:ccy];
                     }
                     else
                     {
                         [a addObject:ccy];
                     }
                     
                     
                     NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                     [gblclass.arr_act_statmnt_name addObject:bbb];
                     
                     if ([privileges isEqualToString:@"1"])
                     {
                         if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                         {
                             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                         }
                     }
                     
                     
                     if ([account_type_desc isEqualToString:@"Deposit Account"])
                     {
                         [gblclass.arr_transfer_within_acct_deposit_chck addObject:bbb];
                     }
                     
                     NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
                     privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
                     
                     
                     if ([privileges_to isEqualToString:@"1"])
                     {
                         [gblclass.arr_transfer_within_acct_to addObject:bbb];
                     }
                      
                    // NSLog(@"%@",gblclass.arr_transfer_within_acct_deposit_chck);
                     //NSLog(@"%@", bbb);
                     [a removeAllObjects];
                     [aa removeAllObjects];
                     
                 }
                 
                 //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                 //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                 //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                 
                 
                 if ([debit_lock_chck isEqualToString:@"yes"])
                 {
                     
                    // NSLog(@"%lu", (unsigned long)[gblclass.arr_transfer_within_acct count]);
                    // NSLog(@"%@",gblclass.arr_transfer_within_acct);
                     
                     if ([gblclass.arr_transfer_within_acct count]>0)
                     {
                         NSArray*  split_debit = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
                         
                       //  NSLog(@"%@",[split_debit objectAtIndex:0]);
                         
                         gblclass.is_default_acct_id = [split_debit objectAtIndex:2];
                         gblclass.is_default_acct_id_name = [split_debit objectAtIndex:0];
                         gblclass.is_default_acct_no = [split_debit objectAtIndex:1];
                         gblclass.is_default_m3_balc = [split_debit objectAtIndex:6];
                         gblclass.registd_acct_id_trans = [split_debit objectAtIndex:2];
                         gblclass.is_default_currency = [split_debit objectAtIndex:10];
                         gblclass.is_default_acct_type = [split_debit objectAtIndex:5];
                         gblclass.is_default_br_code = [split_debit objectAtIndex:9];
                         
//                         NSLog(@"%@",gblclass.is_default_acct_id);
//                         NSLog(@"%@",gblclass.is_default_acct_id_name);
//                         NSLog(@"%@",gblclass.is_default_acct_no);
//                         NSLog(@"%@",gblclass.is_default_m3_balc);
//                         NSLog(@"%@",gblclass.registd_acct_id_trans);
//                         NSLog(@"%@",gblclass.is_default_currency);
//                         NSLog(@"%@",gblclass.is_default_acct_type);
//                         NSLog(@"%@",gblclass.is_default_br_code);
                     }
 
                 }
                 
                NSString* pekaboo = [defaults valueForKey:@"peekabo_kfc"];
                 
                 if (pekaboo.length ==0 )
                 {
                     [self Get_MobUser_Login_CreateDate:@""];
                 }
                 else
                 {
                    // [hud hideAnimated:YES];
                     [self segue1];
                 }
                //10 oct [hud hideAnimated:YES];
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==-54)
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
             }
             else
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[dic objectForKey:@"ReturnMessage"] :@"0"];
             }
             
         }
         @catch (NSException *exception)
         {
             [hud hideAnimated:YES];
            // NSLog(@"%@",exception.description);
             //NSLog(@"%@",exception);
             
             [self custom_alert:Call_center_msg :@"0"];
         }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
               [hud hideAnimated:YES];
               [self custom_alert:Call_center_msg :@"0"];
              
          }];
}

-(void) Get_MobUser_Login_CreateDate:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"1"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"ChannelId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetMobUserLoginCreateDate" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                  
                  NSString* msg = [dic2 objectForKey:@"strEncryptUserId"];
                  [[NSUserDefaults standardUserDefaults] setObject:msg forKey:@"peekabo_kfc"];
                  [[NSUserDefaults standardUserDefaults] synchronize];
                  
                  [self segue1];
                      
                  }
                  else
                  {
                       [hud hideAnimated:YES];
                       [self segue1];
                  }
                      
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                    [self segue1]; //saki 8 oct 2018
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
          [self segue1]; //saki 8 oct 2018
      //  [self custom_alert:@"Please try again later." :@"0"];
    }
    
}


-(void)segue1
{
    [self getCredentialsForServer_touch:@"touch_enable"];
    
    
    if ([tch_chk isEqualToString:@"1"])
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"touch_id"];  //act_summary pie_chart
        [self presentViewController:vc animated:NO completion:nil];
        
        return;
    }
    
    if ([Pw_status_chck isEqualToString:@"101"] || [Pw_status_chck isEqualToString:@"0"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        gblclass.isLogedIn=YES;
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
}

-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                      
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  
//                  gblclass.custom_alert_msg=@"Please try again later.";
//                  gblclass.custom_alert_img=@"0";
//
//
//                  CATransition *transition = [ CATransition animation ];
//                  transition.duration = 0.3;
//                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                  transition.type = kCATransitionPush;
//                  transition.subtype = kCATransitionFromRight;
//                  [ self.view.window. layer addAnimation:transition forKey:nil];
//
//
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void) Is_Rooted_Device:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //  public void IsRootedDevice(string Device_ID, string IP, Int16 isRooted, string isTncAccepted, string callingSource)
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:@"login"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"IP",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"callingSource", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsRootedDevice" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      vw_JB.hidden=YES;
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      vw_JB.hidden=YES;
                      [self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void)GetOnBoardData:(NSString *)strIndustry{
    
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //            NSUserDefaults *cnic = [NSUserDefaults standardUserDefaults];
        //            cnic =[cnic valueForKey:@"cnic"];
        
        gblclass.cnic_onbording = (NSString*) [encrypt de_crypt_url:[defaults valueForKey:@"cnic"]];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data: gblclass.cnic_onbording],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token",
                                                                       @"strCNIC",  nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [[dic objectForKey:@"Accounts"] enumerateObjectsUsingBlock:^(NSArray *item, NSUInteger index, BOOL *stop) {
                          [gblclass.selected_services_onbording addObject:item];
                      }];
                      
                      [[dic objectForKey:@"SelectedProducts"] enumerateObjectsUsingBlock:^(NSArray *item, NSUInteger index, BOOL *stop) {
                          [gblclass.selected_services_onbording addObject:item];
                      }];
                      
                      
                      NSDictionary *otherInfo = [[dic objectForKey:@"OtherInfo"] firstObject];
                      
                      gblclass.branch_code_onbording = [otherInfo objectForKey:@"strBranchCode"];
                      gblclass.mode_of_meeting = [otherInfo objectForKey:@"ModeOfMeeting"];
                      gblclass.preferred_time = [otherInfo objectForKey:@"PreferredTime"];
                      gblclass.debitcard_name_onbording = [otherInfo objectForKey:@"DebitCardName"];
                      //                          gblclass.user_name_onbording = [otherInfo objectForKey:@"fullname"];
                      //                          gblclass.cnic_onbording = [otherInfo objectForKey:@"cnic"];
                      //                          gblclass.email_onbording = [otherInfo objectForKey:@"emailaddress"];
                      //                          gblclass.mobile_number_onbording = [otherInfo objectForKey:@"mobileno"];
                      gblclass.step_no_onbording = [otherInfo objectForKey:@"StepNo"];
                      
                      
                      //                      All the data provided by customer till the stage including stepno as follows:
                      //                      {
                      //                          "Accounts":[
                      //                          {"id":"1","Name":"Current","Description":"Current Account","Document":"NIC^Passport Size Picture"}],
                      //                          "selectedproducts":[
                      //                          {"id":"1","Name":"Credit Card","Description":"Credit Card","Document":"NIC^Passport Size Picture"},
                      //                          {"id":"10","Name":"Personal Loan","Description":"Personal Loan","Document":"NIC^Passport Size Picture"}],
                      //                          "otherinfo":
                      //                          [{"strBranchCode":"08090",
                      //                              "modeofmeeting":"R",
                      //                              "preferredtime":"M",
                      //                              "debitcardname":"Customer Name",
                      //                              "stepno":"5"
                      //                              "fullname":"Customer Name",
                      //                              "cnic":"4201019874561",
                      //                              "emailaddress":"a@b.com",
                      //                              "mobileno":"03213343344"
                      //                          }]
                      //                      }
                      
                      
                      NSString *stepedVC = @"";
                      
                      switch ([gblclass.step_no_onbording integerValue]) {
                          case 1:
                              stepedVC = @"SelectAccountTypeVC";
                              break;
                          case 2:
                              stepedVC = @"FindingLocationVC";
                              break;
                          case 3:
                              stepedVC = @"GoogleMapVC";
                              break;
                          case 4:
                              stepedVC = @"CustomizeDebitCardVC";
                              break;
                          default:
                              //  stepedVC = @"SelectAccountTypeVC";
                              stepedVC = @"SelectedServicesVC";
                              break;
                      }
                      
                      // [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];

//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}





-(void)ApplyForAccount:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"name"],
                                                                       [defaults valueForKey:@"mobile_num"],
                                                                       [defaults valueForKey:@"cnic"],
                                                                       [defaults valueForKey:@"email"],
                                                                       [defaults valueForKey:@"nationality"],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:session_id],
                                                                       [encrypt encrypt_Data:@"0"],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"FullName",
                                                                       @"strMobileNo",
                                                                       @"strCNIC",
                                                                       @"strEmailAddress",
                                                                       @"IsPakistani",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"strSessionId",
                                                                       @"ChannelId", nil]];
        
        
//        NSLog(@"%@\n %@\n %@\n %@\n %@\n",[encrypt de_crypt_url:[defaults valueForKey:@"name"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"cnic"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"email"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"nationality"]]);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ApplyForAccount" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==1)
                  {
                      
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
                      gblclass.request_id_onbording = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Temp_RequestId"]];
                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
                      
                      [self Generate_OTP_For_DA_Request:@""];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                   [self custom_alert:Call_center_msg :@"0"];
                  
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) Generate_OTP_For_DA_Request:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [defaults valueForKey:@"mobile_num"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [hud hideAnimated:YES];
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      //                      {
                      //                          Response = 0;
                      //                          strReturnMessage = "OTP Generated Successfully";
                      //                      }
                      
                      gblclass.user_name_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"name"]];
                      gblclass.cnic_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"cnic"]];
                      gblclass.email_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"email"]];
                      gblclass.mobile_number_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]];
                      gblclass.nationality_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"nationality"]];
                      
                      // [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyOtpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                   [self custom_alert:Call_center_msg :@"0"];
                  
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




-(void)GetTokenRequest:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        gblclass.request_id_onbording = req_id;
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                    [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:req_id],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",   nil]];
        
        //NSLog(@"%@",dictparam);
        
//        NSLog(@"%@",[encrypt de_crypt_url:[defaults valueForKey:@"cnic"]]);
//        NSLog(@"%@",[encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]]);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetTokenRequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                //  [hud hideAnimated:YES];
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"3"]){
                      
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:@"Your request has been successfuly completed!"
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
                      
                      
                      //[self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                      [self removeUserdata];
                      [defaults removeObjectForKey:@"apply_acct"];
                      [defaults synchronize];
                      
                      [self Apply_acct_buttons];
                      
                      [hud hideAnimated:YES];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"]){
                      
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
                      
                      if ([btn_chk isEqualToString:@"track"]) {
                          [hud hideAnimated:YES];
                          [self slide_right];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else if ([[dic objectForKey:@"Response"] isEqualToString:@"3"])
                      {
                          [defaults setValue:@"" forKey:@"req_id"];
                          [defaults setValue:@"" forKey:@"apply_acct"];
                          [defaults synchronize];
                          
                          [self Apply_acct_buttons];
                      }
                      else
                      {
                          [self On_Boarding_Cancle:@""];
                      }
                      
                    //  [hud hideAnimated:YES];
                      
                  } else {
                      [hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }

              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  //[self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void)On_Boarding_Cancle:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //        // CNIC Load ::
        //        NSUserDefaults *cnic = [NSUserDefaults standardUserDefaults];
        //        cnic =[cnic valueForKey:@"cnic"];
        //        gblclass.cnic_onbording = (NSString*) [encrypt de_crypt_url:cnic];
        //        NSLog(@"%@",gblclass.cnic_onbording);
        
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:req_id],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",   nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"CancelOnBoarding" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      [hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      
                      [defaults setValue:@"" forKey:@"req_id"];
                      [defaults setValue:@"" forKey:@"apply_acct"];
                      [defaults synchronize];
                      
                      [self Apply_acct_buttons];
                      
                  } else {
                      [hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  //[self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
    }
    
}


-(void) get_udid:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //  public void IsRootedDevice(string Device_ID, string IP, Int16 isRooted, string isTncAccepted, string callingSource)
        
        
      //  NSLog(@"%@",gblclass.Udid);
       // NSLog(@"%@",gblclass.user_id);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:@"1"],
                                    [encrypt encrypt_Data:pass], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Is_Touch",
                                                                       @"TouchPassword", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"getDeviceIdViaTouchPassword" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      vw_JB.hidden=YES;
                      gblclass.Udid = [dic2 objectForKey:@"OldDeviceID"];
                      
                      [self Guid_delete_Item_Pressed];
                      [self Guid_key_chain_method_make];
                      [self Guid_key_chain_method_load];
                      
                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                      [defaults setValue:@"3" forKey:@"get_udid"];
                      [defaults synchronize];
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      vw_JB.hidden=YES;
                      [self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                //  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        //[self custom_alert:@"Try again later." :@"0"];
        
    }
    
}



// ******************* GUID ********************************

-(void) Guid_key_chain_method_make
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
  //  NSLog(@"%@",gblclass.Udid);
    
    NSString *username = @"Guid1";
    NSString *GUID = gblclass.Udid; // [NSString stringWithFormat:@"%@",UDID1];
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
     //   NSLog(@"Guid Already Exists");
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Already Exists", nil)
        //                                                        message:NSLocalizedString(@"Please update it instead.",)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
    else
    {
        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        
       // NSLog(@"Guid create");
        keychainItem[(__bridge id)kSecValueData] = [GUID dataUsingEncoding:NSUTF8StringEncoding]; //Our password
        
        OSStatus sts = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
       // NSLog(@"Error Code: %d", (int)sts);
    }
    
}


-(void) Guid_key_chain_method_load
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFDictionaryRef result = nil;
    
    OSStatus sts = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    
//    NSLog(@"Error Code: %d", (int)sts);
//    NSLog(@"%d",noErr);
    
    if(sts == noErr)
    {
       // NSLog(@"guid load");
        NSDictionary *resultDict = (__bridge NSDictionary *)result;
        NSData *pswd = resultDict[(__bridge id)kSecValueData];
        NSString *guid = [[NSString alloc] initWithData:pswd encoding:NSUTF8StringEncoding];
        
        
        //NSLog(@"%@",guid);
        gblclass.Udid = guid;
        
    }
    else
    {
        //NSLog(@"Guid Not found, Old load guid");
    }
}


- (void) Guid_delete_Item_Pressed
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //    @"<a class="vglnk" href="http://www.myawesomeservice.com" rel="nofollow"><span>http</span><span>://</span><span>www</span><span>.</span><span>myawesomeservice</span><span>.</span><span>com</span></a>";
    
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
        OSStatus sts = SecItemDelete((__bridge CFDictionaryRef)keychainItem);
        //NSLog(@"Error Code: %d", (int)sts);
        
    }
    else
    {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Doesn't Exist.", nil)
        //                                                        message:NSLocalizedString(@"The item doesn't exist. It may have already been deleted.", nil)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
    }
}


//************************** END GUID ******************************



-(void) fadeInAnimate:(UIView *)view {
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.4;
    [view.layer addAnimation:animation forKey:nil];
}



-(void) slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_pw1 resignFirstResponder];
    [txt_login resignFirstResponder];
    
    [txt_tch_pw resignFirstResponder];
    [txt_tch_login resignFirstResponder];
    
    [self resignFirstResponder];
    
//    - (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//    {
//        [[self view] endEditing:YES];
//    }
}

-(IBAction)btn_login:(id)sender
{
    NSString *Str_user = [txt_login.text stringByReplacingOccurrencesOfString:@" " withString: @""];
    NSString *Str_pw = [txt_pw1.text stringByReplacingOccurrencesOfString:@" " withString: @""];
    randomNumber = arc4random() % 10+1;
    
    //NSLog(@"%ld",(long)randomNumber);
    
    
    //    [hud showAnimated:YES];
    
    if ([Str_user isEqualToString:@""] || [Str_user isEqualToString:@" "])
    {
        
        [self custom_alert:@"Please enter valid Login Name" :@"0"];
        
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Login" message:@"Please Enter User Name" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
        
    }
    else if([Str_pw isEqualToString:@""] || [Str_pw isEqualToString:@" "])
    {
        
        [self custom_alert:@"Please Enter User Password" :@"0"];
        
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Password" message:@"Please Enter User Password" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    else
    {
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        gblclass.user_login_name=txt_login.text;
        //   [self Existing_UserLogin:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            
            chk_ssl=@"login";
            [self SSL_Call];
        }
        
        //NSLog(@"%@",login_status_chck);
        
        // [self performSegueWithIdentifier:@"login" sender:nil];
    }
    
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"login"])
        {
            chk_ssl=@"";
            [self mob_Sign_In:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        else if ([chk_ssl isEqualToString:@"jb_cancel"])
        {
            chk_ssl=@"";
            [self Is_Rooted_Device:@""];
        }
        else if ([chk_ssl isEqualToString:@"applyForAccount"])
        {
            chk_ssl = @"";
            [self ApplyForAccount:@""];
            //  [self GetOnBoardData:@""];
        }
        else if ([chk_ssl isEqualToString:@"getTokenRequest"])
        {
            chk_ssl = @"";
            [self GetTokenRequest:@""];
        }
        else if ([chk_ssl isEqualToString:@"get_udid"])
        {
            chk_ssl=@"";
            [self get_udid:@""];
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"login"])
    {
        chk_ssl=@"";
        [self mob_Sign_In:@""];
     //   [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
      //  [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"jb_cancel"])
    {
        chk_ssl=@"";
        [self Is_Rooted_Device:@""];
       // [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"applyForAccount"])
    {
        chk_ssl = @"";
        [self ApplyForAccount:@""];
        //        [self GetOnBoardData:@""];
    }
    else if ([chk_ssl isEqualToString:@"getTokenRequest"])
    {
        chk_ssl = @"";
        //        [self On_Boarding_Cancle:@""];
        [self GetTokenRequest:@""];
    }
    else if ([chk_ssl isEqualToString:@"get_udid"])
    {
        chk_ssl=@"";
        [self get_udid:@""];
    }
    
   // [self.connection cancel];
    
    
    
    //    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    //    {
    //
    //        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
    //        {
    //            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
    //
    //            gblclass.ssl_pin_check=@"1";
    //
    //            [self mob_Sign_In:@""];
    //
    //            [self.connection cancel];
    //        }
    //        else
    //        {
    //            [self.connection cancel];
    //
    //             [self custom_alert:@"Authentication Failed." :@"0"];
    //
    //
    ////            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
    ////                                                                message:@"Authentication Failed."
    ////                                                               delegate:self
    ////                                                      cancelButtonTitle:@"OK"
    ////                                                      otherButtonTitles:nil, nil];
    ////            [alertView show];
    //
    //
    //            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
    //
    //            gblclass.ssl_pin_check=@"0";
    //
    //            [hud hideAnimated:YES];
    //
    //            return ;
    //        }
    //
    //        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
    //        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    //    }
    //    else
    //    {
    //        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
    //        [[challenge sender] cancelAuthenticationChallenge:challenge];
    //
    //        gblclass.ssl_pin_check=@"0";
    //        [hud hideAnimated:YES];
    //
    //        return;
    //    }
    
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
//    NSLog(@"%ld",(long)error.code);
//    NSLog(@"%@",error.userInfo);
//    NSLog(@"%@",error.localizedFailureReason);
//    NSLog(@"%@",error.localizedDescription);
//    NSLog(@"%@",error.localizedRecoverySuggestion);
//    NSLog(@"%@",error.domain);
    if(error.code == NSURLErrorTimedOut)
    {
        txt_pw1.text = @"";
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    }
    //NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //NSLog(@"%@", gblclass.SSL_name);
    //NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}

- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
   // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}




///////////************************** SSL PINNING END ******************************////////////////





-(IBAction)btn_exist_forgot_pw:(id)sender
{
     gblclass.forgot_pw_click = @"1";
     gblclass.forget_pw_btn_click = @"1";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    
    //new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertController * alert=   [UIAlertController
                                                     alertControllerWithTitle:Title
                                                     message:exception
                                                     preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:UIAlertActionStyleDefault
                                                                        handler:nil]; //You can use a block here to handle a press on this button
                       [alert addAction:actionOk];
                       
                       [self presentViewController:alert animated:YES completion:nil];
                   });
    
}

-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert1 show];
                   });
    //[alert release];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([btn_forgot_chck isEqualToString:@"1"])
    {
        //NSLog(@"%ld",(long)buttonIndex);
        if (buttonIndex == 1)
        {
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 0)
        {
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"existing_forgot"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        
        btn_forgot_chck=@"0";
    }
    
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //777    BOOL stringIsValid;
    NSInteger MAX_DIGITS=20;
    
    
    if ([theTextField isEqual:txt_pw1])
    {
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._- "];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    
    if ([theTextField isEqual:txt_login])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._-"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)btn_feature:(id)sender
{
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"qr";
                [self SSL_Call];
            }
        }
//        else if ([t_n_c isEqualToString:@"0"])
//        {
//            [hud hideAnimated:YES];
//            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
//        }
        else
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"qr";
                [self SSL_Call];
            }
        }
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
}

-(IBAction)btn_offer:(id)sender
{

     [self custom_alert:Offer_msg :@""];
    
//    if ( gblclass.peekabo_kfc_userid.length == 0)
//    {
//        [self peekabooSDK:@"deals"];
//    }
//    else
//    {
//         [self peekabooSDK:@"deals"];
    //       [self peekabooSDK:@{@"type": @"deals",}];
    
//    }
    
}


-(IBAction)btn_find_us:(id)sender
{
    gblclass.forget_pw_btn_click = @"2";
//    [self peekabooSDK:@"locator"];
//      [self peekabooSDK:@{@"type": @"locator",}];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
////
////
//////    new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    // [self.avplayer pause];
    
    [self checkinternet];
    if (netAvailable)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            [hud hideAnimated:YES];
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) removeUserdata {
    [defaults removeObjectForKey:@"name"];
    [defaults removeObjectForKey:@"cnic"];
    [defaults removeObjectForKey:@"mobile_num"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"nationality"];
    [defaults removeObjectForKey:@"req_id"];
    [defaults synchronize];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_tch:(id)sender
{
    
    //    if ([gblclass.payment_login_chck isEqualToString:@"1"])
    //    {
    
    //touch_id
    
    if ([tch_chk isEqualToString:@"1"]) {
        //  btn_touch_id.hidden=NO;
        
        
        LAContext *myContext = [[LAContext alloc] init];
        NSError *authError = nil;
        NSString *myLocalizedReasonString = myLocalizedReasonString_1;
        
        if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
        {
            [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                      localizedReason:myLocalizedReasonString
                                reply:^(BOOL success, NSError *error) {
                                    
                                    if (success) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            // [self performSegueWithIdentifier:@"Success" sender:nil];
                                            
                                            
                                            [self.view addSubview:hud];
                                            [hud showAnimated:YES];
                                            
                                            //                                                [self checkinternet];
                                            //                                                if (netAvailable)
                                            //                                                {
                                            
                                            btn_next_press=@"0";
                                            
                                            chk_ssl=@"login";
                                            [self SSL_Call];
                                            //                                                }
                                            
                                        });
                                        
                                    } else {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            //                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                            //                                                                                                message:error.localizedDescription
                                            //                                                                                               delegate:self
                                            //                                                                                      cancelButtonTitle:@"OK"
                                            //                                                                                      otherButtonTitles:nil, nil];
                                            //  [alertView show];
                                            //NSLog(@"Switch to fall back authentication - ie, display a keypad or password entry box");
                                        });
                                    }
                                }];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [self custom_alert:authError.localizedDescription :@"0"];
                
                //                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
                //                                                                    message:authError.localizedDescription
                //                                                                   delegate:self
                //                                                          cancelButtonTitle:@"OK"
                //                                                          otherButtonTitles:nil, nil];
                //                [alertView show];
                
                
                // btn_touch_id.hidden=YES;
                
            });
        }
    }
    
    //   } //payment login check..
    
    
    [hud hideAnimated:YES];
    
    
    
}


-(IBAction)btn_termNCondition:(id)sender
{
    //vw_tNc.hidden=NO;
    gblclass.term_conditn_chk_place = @"login_new";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
    [self presentViewController:vc animated:NO completion:nil];
    [self slide_right];
    
}

-(IBAction)btn_jb_next:(id)sender {
    vw_JB.hidden = YES;
    vw_tNc.hidden=NO;
}

-(IBAction)btn_jb_cancel:(id)sender {
    
    [hud showAnimated:YES];
    exit(0);
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    //        chk_ssl=@"jb_cancel";
    //        [self SSL_Call];
    //
    //        btn_TnC_cancel_press=@"yes";
    //        //vw_JB.hidden = YES;
    //    }
    
}

-(IBAction)btn_activate_ubl_acct:(id)sender
{
//    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup"]; // new_exist
//    [self presentViewController:vc animated:NO completion:nil];
//    [self slide_right];
    gblclass.forgot_pw_click = @"0";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"SecureCodeOrProfileInfoViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    [self slide_right];
    
}

-(IBAction)btn_apply_for_acct:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"NewApplicationVC"]; //NewApplicationVC RegisterAccountVC
    [self presentViewController:vc animated:NO completion:nil];
    [self slide_right];    
}

-(IBAction)btn_change_tch_ID:(id)sender
{
    UIButton *button = (UIButton *)sender;

    if ([button.titleLabel.text isEqualToString:@"Change to Touch ID"] || [button.titleLabel.text isEqualToString:@"Change to Face ID"]) {
        [self fadeInAnimate:vw_tch_view];
        vw_without_tch_view.hidden = YES;
         _btn_touchID.hidden = NO;
        vw_tch_view.hidden = NO;

    } else if ([button.titleLabel.text isEqualToString:@"Change to Login Password"]) {
        [self fadeInAnimate:vw_without_tch_view];
        vw_tch_view.hidden = YES;
        _btn_touchID.hidden = YES;
        vw_without_tch_view.hidden = NO;
    }
}

//- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
//{
//    if(event.type == UIEventSubtypeMotionShake)
//    {
//        NSLog(@"called");
//        [self.view setBackgroundColor:[UIColor greenColor]];
//    }
//}
//
//- (BOOL)canBecomeFirstResponder
//{
//    return YES;
//}

-(IBAction)btn_track:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    btn_chk = @"track";
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"getTokenRequest";
        [self SSL_Call];
    }
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
    //    [self presentViewController:vc animated:NO completion:nil];
    //
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromRight;
    //    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_continue:(id)sender
{
    btn_chk = @"continue";
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    gblclass.conti_session_id = @"c";
    session_id = @"1234|n";
    chk_ssl = @"applyForAccount";
    [self SSL_Call];
}

-(IBAction)btn_cancel:(id)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to cancel your current request!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertActionOk = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        btn_chk = @"cancel";
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        chk_ssl = @"getTokenRequest";
        [self SSL_Call];
        
    }];
    
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:alertActionOk];
    [alertController addAction:alertActionCancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
    //    btn_chk = @"cancel";
    //    [hud showAnimated:YES];
    //    chk_ssl = @"getTokenRequest";
    //    [self SSL_Call];
    
}


-(void)pekaboo
{
    @try
    {
        
        chk_pekaboo_service_hit = @"1";
       // NSLog(@"%@",gblclass.lati_pekabo);
       // NSLog(@"%@",gblclass.longi_pekabo);
        
//        gblclass.lati_pekabo =@"31.5033532" ;
//        gblclass.longi_pekabo =@"74.3491567" ;
        
        NSMutableString* requestURL = [[NSMutableString alloc] init];
        [requestURL appendString:@"https://secure-sdk.peekaboo.guru/jRavhbSijxdc2raUB0wM"];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [NSString stringWithString:requestURL]]];
        
        NSError *error = nil;
        NSData *json;
        [request setHTTPMethod: @"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"7db159dc932ec461c1a6b9c1778bb2b0" forHTTPHeaderField:@"ownerkey"];
        
        NSDictionary *parameter = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:gblclass.lati_pekabo ,@"js6nwf", gblclass.longi_pekabo, @"pan3ba", nil], @"asdxa", nil];
        
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:&error];
        
        
        [request setHTTPBody: json];
        [request  setURL:[NSURL URLWithString:requestURL]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            if (data.length > 0 && connectionError == nil)
            {
                
                NSDictionary *rest_data = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                
                [hud hideAnimated:YES];
             //   NSLog(@"%@",[rest_data objectForKey:@"isWhitelisted"]);
                //  NSLog(@"%@",[rest_data objectForKey:@"city"]);
                
                NSString* is_Whitelisted_chck = [NSString stringWithFormat:@"%@", [rest_data objectForKey:@"isWhitelisted"]];
                
                [hud hideAnimated:YES];
                
                // is_Whitelisted_chck = @"1";
                if ([is_Whitelisted_chck isEqualToString:@"0"])
                {
                    vw_apply_acct.hidden = YES;
                   // vw_tracking.hidden = YES;
                    
                    req_id = [encrypt de_crypt_url:[defaults valueForKey:@"req_id"]];
                    apply_acct = [defaults valueForKey:@"apply_acct"];
                    
                    if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"1"]) //Tracking
                    {
                        vw_apply_acct.hidden = YES;
                        vw_tracking.hidden = NO;
                        
                        btn_continue.hidden = YES;
                        vw_btn_continue_line.hidden = YES;
                        btn_cancel.hidden = YES;
                        vw_btn_cancel_line.hidden = YES;
                        btn_tracking.hidden = NO;
                        vw_btn_tracking_line.hidden = NO;
                    }
                }
                else
                {
                    vw_apply_acct.hidden = NO;
                    [self Apply_acct_buttons];
                }
                
              //  NSLog(@"%@",rest_data);
                
            }
        }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


//- (void)peekabooSDK:(NSString *)type
//{
//    NSLog(@"%@",gblclass.peekabo_kfc_userid);
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
//}

- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    //saki 
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}
 

@end


