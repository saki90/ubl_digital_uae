

//
//  Payee_list_update.m
//  ubltestbanking
//
//  Created by Mehmood on 27/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Payee_list_update.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Payee_list_update ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    UILabel* label;
    UIStoryboard *storyboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    
    UIAlertController  *alert;
    NSDictionary* dic;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* session_timeout;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Payee_list_update
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    txt_nick.delegate=self;
    ssl_count = @"0";
    
   
    
    session_timeout = @"";
//    NSLog(@"%@",gblclass.arr_payee_list);
    
    if ([gblclass.arr_payee_list count]>0)
    {
        txt_nick.text=[gblclass.arr_payee_list objectAtIndex:0];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load_payee_list";
        [self SSL_Call];
    }
    
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if ([textField isEqual:txt_nick])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;
            }
            
        }
    }
    
    return YES;
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(IBAction)btn_save:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    [txt_email resignFirstResponder];
    [txt_nick resignFirstResponder];
    
    
    if ([txt_nick.text isEqualToString:@""])
    {
        [self custom_alert:@"Enter Nick" :@"0"];
        [hud hideAnimated:YES];
        return;
        
    } else if ([txt_nick.text length] < 5) {
        
        [self custom_alert:@"Nick should be between 5 to 30 characters" :@"0"];
        [hud hideAnimated:YES];
        return;
    }
    //    else  if([txt_email.text isEqualToString:@""])
    //    {
    //
    //
    //        txt_email.text=@"";
    //
    //       // [hud hideAnimated:YES];
    //
    //       //  [self custom_alert:@"Enter Email Address" :@"0"];
    //
    //        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Email Address" preferredStyle:UIAlertControllerStyleAlert];
    ////
    ////        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    ////        [alert addAction:ok];
    ////
    ////        [self presentViewController:alert animated:YES completion:nil];
    //
    //         return;
    //    }
    else if (![self validateEmail:[txt_email text]] && ![txt_email.text isEqualToString:@""])
    {
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention :" message:@"Invalid Email Address" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Invalid Email Address" :@"0"];
        
        return;
    }
    
    else
    {
        //        [self PayeeList_Update:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"save";
            [self SSL_Call];
        }
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_nick resignFirstResponder];
    [txt_email resignFirstResponder];
}

-(void)load_pay_list:(NSString *)strIndustry
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //str_acct_id_to
    
    
    
//    NSLog(@"%@",gblclass.user_id);
//    NSLog(@"%@",gblclass.M3sessionid);
//    NSLog(@"%@",gblclass.Udid);
//    NSLog(@"%@",gblclass.token);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:strIndustry],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"payAnyOneAccountID",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"LoadPayeeList" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSMutableArray* arr_load_payee_list=[[NSMutableArray alloc] init];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  session_timeout = @"1";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }

              
              if ([[dic objectForKey:@"Response"] integerValue]==0) {
                  [hud hideAnimated:YES];
                  arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
                  
                  for (dic in arr_load_payee_list) {
                      NSString* str_email=[dic objectForKey:@"EMAIL"];
                      NSString*  ACCOUNT_NAME = [dic objectForKey:@"ACCOUNT_NAME"];
                      NSString*  TT_NAME = [dic objectForKey:@"TT_NAME"];
                      
                      if (![TT_NAME isEqual:[NSNull null]]) {
                          if ([TT_NAME isEqualToString:@"CNIC"]) {
                              txt_mobile.hidden = NO;
                              txt_mobile.text = [dic objectForKey:@"BENEFICIARYMOBILE"];
                              txt_nick.enabled = NO;
                          }
                      }
                      
                      if (![ACCOUNT_NAME isEqual:[NSNull null]]) {
                          txt_nick.text = ACCOUNT_NAME;
                      } else {
                          txt_nick.text = @"";
                      }
                      
                      if (str_email==(NSString *)[NSNull null]) {
                          txt_email.text=@"";
                      } else {
                          txt_email.text=[dic objectForKey:@"EMAIL"];
                      }
                  }
                  
              } else {
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void)PayeeList_Update:(NSString *)strIndustry
{
    
    @try {
        
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //NSLog(@"%@",[gblclass.arr_payee_list objectAtIndex:1]);
        
        //NSLog(@"%@",gblclass.user_email);
        
        if([gblclass.user_email length]>0)
        {
            
        }
        else
        {
            gblclass.user_email=@"";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:[gblclass.arr_payee_list objectAtIndex:1]],
                                    [encrypt encrypt_Data:txt_nick.text],
                                    [encrypt encrypt_Data:txt_email.text],
                                    [encrypt encrypt_Data:txt_mobile.text],
                                    [encrypt encrypt_Data:@""],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys: [NSArray arrayWithObjects:@"strUserId",
                                                                        @"strSessionId",
                                                                        @"IP",
                                                                        @"payAnyOneTOAccountID",
                                                                        @"strtxtNick",
                                                                        @"strAccountEmail",
                                                                        @"strBeneficiarymobile",
                                                                        @"strIBFTRelation",
                                                                        @"Device_ID",
                                                                        @"Token", nil]];
        
//    strUserId:
//    strSessionId:
//    IP:
//    payAnyOneTOAccountID:
//    strtxtNick:
//    strAccountEmail:
//    strIBFTRelation:
//    strBeneficiarymobile:
//    Device_ID:
//    Token:
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PayeeListUpdate" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      session_timeout = @"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      
                      //                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"1"];
                      
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert1 show];
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"payee_list"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]   :@""];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  [self custom_alert:@"Retry"  :@""];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([session_timeout isEqualToString:@"1"])
    {
    
        if (buttonIndex == 0)
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
            //[self mob_App_Logout:@""];
        }
        else if(buttonIndex ==1)
        {
            //        CATransition *transition = [ CATransition animation];
            //        transition.duration = 0.3;
            //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
            //        transition.type = kCATransitionPush;
            //        transition.subtype = kCATransitionFromRight;
            //        [self.view.window. layer addAnimation:transition forKey:nil];
            //
            //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            //        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
            //        [self presentViewController:vc animated:NO completion:nil];
        }
        
    }
    else if ([session_timeout isEqualToString:@"2"])
    {
        if (buttonIndex == 0)
        {
            [hud hideAnimated:YES];
        }
        else if(buttonIndex ==1)
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl = @"delete";
                [self SSL_Call];
            }
        }
    }
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
    
}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    
    [gblclass.arr_payee_list removeAllObjects];
    
    
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    //
    //    [self presentViewController:vc animated:NO completion:nil];
    
    
    
    
    //    [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


-(IBAction)btn_delete:(id)sender
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([txt_nick.text isEqualToString:@""])
    {
        [hud hideAnimated:YES];
        alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Enter Nick" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    else
    {
         
        UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                         message:@"Are your sure, you want to delete the payee?"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert3 show];
        
        session_timeout = @"2";
        
        
//        [self checkinternet];
//        if (netAvailable)
//        {
//            chk_ssl = @"delete";
//            [self SSL_Call];
//            //[self PayeeList_Delete:@""];
//        }
    }
}


-(void) PayeeList_Delete:(NSString *)strIndustry
{
    
    @try {
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //NSLog(@"%@",gblclass.arr_payee_list);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:[gblclass.arr_payee_list objectAtIndex:1]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"strUserId",
                                    @"strSessionId",
                                    @"IP",
                                    @"payAnyOneTOAccountID",
                                    @"Device_ID",
                                    @"Token", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"PayeeListDelete" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      session_timeout = @"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      
                      //    [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@""];
                      
                      session_timeout = @"";
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert1 show];
                      
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"payee_list"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  //                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"])
                  //                  {
                  //
                  //                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //                      //
                  //                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                      //
                  //                      //                      [alert addAction:ok];
                  //                      //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                      [hud hideAnimated:YES];
                  //
                  //                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  //
                  //                      return ;
                  //
                  //                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@""];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  [self custom_alert:[error localizedDescription]  :@""];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@""];
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ( [chk_ssl isEqualToString:@"save"])
        {
            chk_ssl=@"";
            [self PayeeList_Update:@""];
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            [self  load_pay_list:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        else if([chk_ssl isEqualToString:@"delete"])
        {
            [self PayeeList_Delete:@""];
            chk_ssl=@"";
        }
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ( [chk_ssl isEqualToString:@"save"])
    {
        chk_ssl=@"";
        [self PayeeList_Update:@""];
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
        [self  load_pay_list:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"delete"])
    {
        [self PayeeList_Delete:@""];
        chk_ssl=@"";
    }
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    [hud hideAnimated:YES];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES]; 
            [self custom_alert:statusString :@""];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

@end

