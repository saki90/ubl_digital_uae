//
//  offerDetailTableViewCell.h
//  ubltestbanking
//
//  Created by Mehmood on 08/11/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface offerDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageTag1;
@property (weak, nonatomic) IBOutlet UILabel *labeltag0;
@property (weak, nonatomic) IBOutlet UILabel *labeltag2;
@property (weak, nonatomic) IBOutlet UILabel *labeltag3;
@property (weak, nonatomic) IBOutlet UILabel *labeltag4;
@property (weak, nonatomic) IBOutlet UILabel *labeltag9;
@property (weak, nonatomic) IBOutlet UILabel *labelTag8;
@property (weak, nonatomic) IBOutlet UIButton *favOutlet;

@end
