//
//  Login_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Login_VC.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
 
#import "SWRevealViewController.h"
#import "Slide_menu_VC.h"
#import "TransitionDelegate.h"
#import <UIKit/UIKit.h>
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
 
#import "Login_New.h"
#import "Login_VC.h"
#import "Encrypt.h"
#import <QuartzCore/QuartzCore.h>
#import "Globals.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "Peekaboo.h"


#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."


@interface Login_VC ()<NSURLConnectionDataDelegate,CLLocationManagerDelegate>
{
    UIAlertController *alert;
    MBProgressHUD* hud;
    NSString* login_status_chck;
    GlobalStaticClass* gblclass;
    NSProgress *mine;
//    Password_VC* txt_pw;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSDictionary *dic;
    NSString *login_name;
    NSString *login_pw;
    NSInteger randomNumber ;
    NSString* btn_forgot_chck;
    NSMutableArray*  a;
    NSString* Pw_status_chck;
    CLLocationManager *myLcationManager;
    BOOL runOnce;
    UIStoryboard *storyboard;
    
    UIImage *btnImage;
    NSString* btn_flag_email;
    
    UIImage *btn_chck_Image;
    NSString* chk_termNCondition;
    NSString* device_jail;
    NSString* termNcondi_txt;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSUserDefaults *defaults,*default_jb;
    NSString* device_jb_chk;
    NSString* t_n_c;
    NSString* ssl_count;
    NSString* btn_chk;
    NSString* req_id,*apply_acct;
    NSString* session_id;
    NSString* lati_pekaboo,*longi_pekaboo;
    NSString* chk_pekaboo_service_hit;
    NSString* chck_pekaboo_service_call;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UIButton *btn_jb_cancel;

@property (strong,nonatomic) UIViewController *modal;

@end


@implementation Login_VC
static UIView* loadingView = nil;

@synthesize photoFilename;
@synthesize transitionController;
@synthesize txt_login;

//@synthesize traitCollection;


- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    //NSLog(@"%@",notification);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    encrypt = [[Encrypt alloc] init];
    timer_class = [[APIdleManager alloc] init];
    
     responseData = [NSMutableData new];
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    a = [[NSMutableArray alloc] init];
    
    
    //Dark Mode ::
    
//    if #available(iOS 13.0, *)
//    {
//        overrideUserInterfaceStyle = .light
//    }
    
//    you can override the interface style of UIViewController by
//    1: overrideUserInterfaceStyle = .dark //For dark mode
//    2: overrideUserInterfaceStyle = .light //For light mode
//    class ViewController: UIViewController {
//        override func viewDidLoad() {
//            super.viewDidLoad()
//            overrideUserInterfaceStyle = .light
//        }
//    }
    
    
    
    
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    [self.btn_jb_cancel.layer setCornerRadius:35.0];
    self.btn_jb_cancel.layer.borderWidth = 1.0;
    self.btn_jb_cancel.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.btn_jb_cancel.layer setMasksToBounds:YES];
    session_id = @"";
    btn_signup_round.layer.cornerRadius = 20;
    btn_signup_round.clipsToBounds = YES;
    
    chck_pekaboo_service_call = @"";
    chk_pekaboo_service_hit = @"";
    ssl_count = @"0";
    Login_New* loginNew=[[Login_New alloc] init];
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    login_name=txt_login.text;
    
    gblclass.lati_pekabo = @"";
    gblclass.longi_pekabo = @"";
    
    
    
    txt_pw1.delegate=self;
    txt_login.delegate=self;
    
    txt_pw1.text=@"";
    txt_login.text=@"";
    gblclass.isLogedIn=NO;
    
//    NSLog(@"%@",gblclass.chk_term_condition);
    
    // Shows the terms and condition view.
    
    gblclass.chk_term_condition=0;
    vw_term.hidden=YES;
    
    Encrypt* encyr = [[Encrypt alloc] init];
   
    gblclass.TnC_calling_source = @"signup";
    
    
    if ([gblclass.str_processed_login_chk isEqualToString:@"1"])
    {
        txt_login.text =  gblclass.sign_up_user;
        txt_login.enabled=NO;
        forgetPass.hidden=YES;
    }
    else
    {
        txt_login.enabled=YES;
        forgetPass.hidden=NO;
    }
    
//    NSLog(@"%@",gblclass.TnC_chck_On);
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    
    default_jb = [NSUserDefaults standardUserDefaults];
    device_jb_chk = [default_jb valueForKey:@"isDeviceJailBreaked"];
    
    gblclass.jb_chk_login_back = @"0";
    if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
    {
        device_jail = @"1";
        vw_JB.hidden=NO;
        vw_tNc.hidden=YES;
        btn_next.enabled=NO;
        btn_resend_otp.enabled=NO;
        gblclass.jb_chk_login_back = @"1";
        
        btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
        [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
    }
    else if ([device_jb_chk isEqualToString:@"2"] && [t_n_c isEqualToString:@"2"])
    {
        device_jail = @"1";
        vw_JB.hidden = YES;
        vw_tNc.hidden = YES;
        btn_next.enabled = YES;
        btn_resend_otp.enabled = NO;
        
        //        btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
        //        [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
        
        
        btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
        [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
    }
    else if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
    {
        device_jail = @"1";
        vw_JB.hidden=NO;
        vw_tNc.hidden=YES;
        btn_next.enabled=YES;
        btn_resend_otp.enabled=NO;
        
        btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
        [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
    }
    else
    {
        device_jail = @"0";
        
        vw_JB.hidden=YES;
        vw_tNc.hidden=YES;
        btn_next.enabled=YES;
        btn_resend_otp.enabled=YES;
        
        //        btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
        //        [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    }
    
    

    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"login-bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    vw_login_line.layer.borderColor = [UIColor whiteColor].CGColor;
    vw_login_line.layer.cornerRadius = 8;
    vw_login_line.layer.borderWidth = 1.0f;
    
    emailView.layer.borderColor = [UIColor whiteColor].CGColor;
    emailView.layer.cornerRadius = 8;
    emailView.layer.borderWidth = 1.0f;
    
    passView.layer.borderColor = [UIColor whiteColor].CGColor;
    passView.layer.cornerRadius = 8;
    passView.layer.borderWidth = 1.0f;
    
    nxt_outlet.layer.cornerRadius=5;
    
    
    //  UIFont *font = [UIFont fontWithName:@"Aspira-Light" size:14];
    
    lbl_heading.text=@"Please enter your netbanking username and password.";
    
    
    //   [lbl_heading setFont:font];
    
    
    runOnce = NO;
    
    
    lbl_heading.frame = CGRectMake(self.view.frame.size.width + lbl_heading.frame.size.width, lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
    
    //    emailOutlet.frame = CGRectMake(self.view.frame.size.width + emailOutlet.frame.size.width, emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
    //    emailView.frame = CGRectMake(self.view.frame.size.width + emailView.frame.size.width, emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
    //
    //    passOutlet.frame = CGRectMake(self.view.frame.size.width + passOutlet.frame.size.width, passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
    //    passView.frame = CGRectMake(self.view.frame.size.width + passView.frame.size.width, passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
    //
    //    forgetPass.frame = CGRectMake(self.view.frame.size.width + forgetPass.frame.size.width, forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
    //
    activateApplicationView.frame = CGRectMake(self.view.frame.size.width + activateApplicationView.frame.size.width, activateApplicationView.frame.origin.y, activateApplicationView.frame.size.width, activateApplicationView.frame.size.height);
    
    
    vw_apply_acct.frame = CGRectMake(self.view.frame.size.width + vw_apply_acct.frame.size.width, vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
    
    
    vw_tracking.frame = CGRectMake(self.view.frame.size.width + vw_tracking.frame.size.width, vw_tracking.frame.origin.y, vw_tracking.frame.size.width, vw_tracking.frame.size.height);
    
    vw_login_line.frame = CGRectMake(self.view.frame.size.width + vw_login_line.frame.size.width, vw_login_line.frame.origin.y, vw_login_line.frame.size.width, vw_login_line.frame.size.height);
    
    
    
    //     btn_next.frame = CGRectMake(self.view.frame.size.width + btn_next.frame.size.width, btn_next.frame.origin.y, btn_next.frame.size.width, btn_next.frame.size.height);
    //
    
    [NSTimer scheduledTimerWithTimeInterval:0.4
                                     target:self
                                   selector:@selector(showAnimation)
                                   userInfo:nil
                                    repeats:NO];
    
    
    
    //*** 18 May  lcoation update close ....

    
 
    
    
        if ([CLLocationManager locationServicesEnabled])
        {

            //NSLog(@"Location Services Enabled");

            if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){

                myLcationManager = [[CLLocationManager alloc] init];
                [myLcationManager requestAlwaysAuthorization];
                myLcationManager.distanceFilter = kCLDistanceFilterNone;
                myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
                myLcationManager.delegate = self;
                [myLcationManager startUpdatingLocation];
            }
            else
            {

                myLcationManager = [[CLLocationManager alloc] init];
                myLcationManager.distanceFilter = kCLDistanceFilterNone;
                myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
                myLcationManager.delegate = self;
                [myLcationManager startUpdatingLocation];

            }
        }

    
    // ** Don't forget to add NSLocationWhenInUseUsageDescription in MyApp-Info.plist and give it a string
    
    myLcationManager = [[CLLocationManager alloc] init];
    myLcationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([myLcationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [myLcationManager requestWhenInUseAuthorization];
    }
    [myLcationManager startUpdatingLocation];
    
    
    
    
    //    [self removeAllCredentialsForServer:@"header_2"];
    //    [self saveUsername:@"" withPassword:@"" forServer:@"header_2"];
    //    [self getCredentialsForServer:@"header_2"];
    
    
    
    //  Obfuscator *o = [Obfuscator newWithSalt:[AppDelegate class],[NSString class], nil];
    //
    //             Obfuscator *o = [Obfuscator newWithSalt:[Login_VC class],[NSObject class], nil];
    //
    //
    //
    //            //Step 1: Get obfuscated code from console and then copy it to Globals.m.
    //            NSString *origParseKey = user_saki;     //  @"q/xpnYRQveuGy5YofkLCug==-ITrP+agrSVtE0eenudFGwg==-
    //
    //            [o hexByObfuscatingString:origParseKey];
    //
    //            //Step 2: Comment out: [o hexByObfuscatingString:@"JEG3i8R9LAXIDW0kXGHGjauak0G2mAjPacv1QfkO"];
    //
    //            //Step 3: When you want to reveal the original:
    //            NSString *decoded = [o reveal:key];
    //            //NSLog(@"\n\ndecoded: %@", decoded);
    
    
    
    //    NSString *unencryptedKey = [pw_saki unobfuscatedString];
    //
    //    //NSLog(@"%@",unencryptedKey);
    //
    //
    //    gblclass.header_user=[user_saki unobfuscatedString];
    //    gblclass.header_pw=[pw_saki unobfuscatedString];
    //
    
    
//    txt_login.placeholder = [UIColor whiteColor];
    
    UIColor *color = [UIColor lightTextColor];
    txt_login.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Login" attributes:@{NSForegroundColorAttributeName: color}];
    
    txt_pw1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    [APIdleManager sharedInstance].onTimeout = ^(void){
        //  [self.timeoutLabel  setText:@"YES"];
        
        
        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        
        storyboard = [UIStoryboard storyboardWithName:
                      gblclass.story_board bundle:[NSBundle mainBundle]];
        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        [self presentViewController:vc animated:YES completion:nil];
        
        APIdleManager * cc1=[[APIdleManager alloc] init];
        // cc.createTimer;
        
        [cc1 timme_invaletedd];
        
    };
    
    
    //vw_JB = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    //Hidden Apply Account buttons ::
    
    vw_apply_acct.hidden = YES;
    vw_tracking.hidden = YES;
    
    btn_continue.hidden = YES;
    vw_btn_continue_line.hidden = YES;
    btn_cancel.hidden = YES;
    vw_btn_cancel_line.hidden = YES;
    btn_tracking.hidden = YES;
    vw_btn_tracking_line.hidden = YES;
    
    [self Play_bg_video];
    
  //9 july 2018
//    [self checkinternet];
//    if (netAvailable)
//    {
//        [self pekaboo];
//    }
    
//    saki 29 nov
//    [NSTimer scheduledTimerWithTimeInterval:0.8
//                                     target:self
//                                   selector:@selector(pekaboo_apply_acct)
//                                   userInfo:nil
//                                    repeats:NO];
    
    
    
//    NSLog(@"%@",gblclass.str_processed_login_chk);
//    if (![gblclass.str_processed_login_chk isEqualToString:@"1"])
//    {
//        [self Apply_acct_buttons];
//    }
//    else
//    {
        vw_apply_acct.hidden = YES;
        vw_tracking.hidden = YES;
        
        activateApplicationView.hidden = NO;
      
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
  //  }
    
    //saki 29 nov
   // [self Apply_acct_buttons];
}



//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    if ((UITextField *)sender == txt_login)
//        return NO;
//
//    return [super canPerformAction:action withSender:self];
//}


- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [myLcationManager requestAlwaysAuthorization];
    }
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1) {
//        // Send the user to the Settings for this app
//        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//        [[UIApplication sharedApplication] openURL:settingsURL];
//    }
//}


-(void) pekaboo_apply_acct
{
    
        [self checkinternet];
        if (netAvailable)
        {
            [self pekaboo];
        }
    
//        [self checkinternet];
//        if (netAvailable)
//        {
//            [hud showAnimated:YES];
//            [self.view addSubview:hud];
//            [self pekaboo];
//        }
    
}



//-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"ATTENTION" message:@"Please enable your location to use new feature in app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [errorAlert show];
//    NSLog(@"Error: %@",error.description);
//}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *crnLoc = [locations lastObject];
//    latitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.latitude];
//    longitude.text = [NSString stringWithFormat:@"%.8f",crnLoc.coordinate.longitude];
//    altitude.text = [NSString stringWithFormat:@"%.0f m",crnLoc.altitude];
//    speed.text = [NSString stringWithFormat:@"%.1f m/s", crnLoc.speed];

        gblclass.arr_current_location = [[NSMutableArray alloc] init];
        float latitude = myLcationManager.location.coordinate.latitude;
        float longitude = myLcationManager.location.coordinate.longitude;
    
        NSString *lati=[NSString stringWithFormat:@"%f",latitude];
        NSString *longi=[NSString stringWithFormat:@"%f",longitude];
    
    
//    NSLog(@"%@",locations.lastObject);
//
//    NSLog(@"%f",crnLoc.coordinate.latitude);
//    NSLog(@"%f",crnLoc.coordinate.longitude);
    
    
        lati_pekaboo = lati;
        longi_pekaboo = longi;
    
        gblclass.lati_pekabo = lati;
        gblclass.longi_pekabo = longi;
    
//        NSLog(@"lati %@",lati);
//        NSLog(@"lati %@",longi);
    
        [gblclass.arr_current_location addObject:lati];
        [gblclass.arr_current_location addObject:longi];
    
        [manager stopUpdatingLocation];

    
    if ([chck_pekaboo_service_call isEqualToString:@""])
    {
        chck_pekaboo_service_call = @"1";
        [self checkinternet];
        if (netAvailable)
        {
            [self pekaboo];
        }
    }
    
//
       // [self peekaboo_geo];


}


//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
//{
//    
//  
//    
//    gblclass.arr_current_location = [[NSMutableArray alloc] init];
//    float latitude = myLcationManager.location.coordinate.latitude;
//    float longitude = myLcationManager.location.coordinate.longitude;
//    
//    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
//    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
//    
//    lati_pekaboo = lati;
//    longi_pekaboo = longi;
//    
//    gblclass.lati_pekabo = lati;
//    gblclass.longi_pekabo = longi;
//    
//    NSLog(@"lati %@",lati);
//    NSLog(@"lati %@",longi);
//    
//    [gblclass.arr_current_location addObject:lati];
//    [gblclass.arr_current_location addObject:longi];
//    
//    
//      [manager stopUpdatingLocation];
//    
//    [self checkinternet];
//    if (netAvailable)
//    {
//        [self pekaboo];
//    }
//    
//   // [self peekaboo_geo];
//    
//}

-(void)Apply_acct_buttons
{
    req_id = [encrypt de_crypt_url:[defaults valueForKey:@"req_id"]];
    apply_acct = [defaults valueForKey:@"apply_acct"];
    
    
    //apply_acct = @"1";
    
    if ([req_id isEqualToString:@""])
    {
        vw_apply_acct.hidden = NO;
        vw_tracking.hidden = YES;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    else if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"1"]) //Tracking
    {
        vw_apply_acct.hidden = YES;
        vw_tracking.hidden = NO;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = NO;
        vw_btn_tracking_line.hidden = NO;
    }
    else if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"0"]) //Conti & Cancel
    {
        vw_apply_acct.hidden=YES;
        vw_tracking.hidden=NO;
        
        btn_continue.hidden = NO;
        vw_btn_continue_line.hidden = NO;
        btn_cancel.hidden = NO;
        vw_btn_cancel_line.hidden = NO;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    else
    {
        vw_apply_acct.hidden=NO;
        vw_tracking.hidden=YES;
        
        btn_continue.hidden = YES;
        vw_btn_continue_line.hidden = YES;
        btn_cancel.hidden = YES;
        vw_btn_cancel_line.hidden = YES;
        btn_tracking.hidden = YES;
        vw_btn_tracking_line.hidden = YES;
    }
    
}


////*********************** Login credential Start ******************************
//
//
//
//-(void) saveUsername:(NSString*)user withPassword:(NSString*)pass forServer:(NSString*)server
//{
//
//    // Create dictionary of search parameters
//    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
//
//    // Remove any old values from the keychain
//    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
//
//    // Create dictionary of parameters to add
//    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
//    dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, user, kSecAttrAccount, nil];
//
//    // Try to save to keychain
//    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
//
//}
//
//
//-(void) removeAllCredentialsForServer:(NSString*)server
//{
//
//    // Create dictionary of search parameters
//    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
//
//    // Remove any old values from the keychain
//    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
//
//}
//
//
//-(void) getCredentialsForServer:(NSString*)server
//{
//
//    // Create dictionary of search parameters
//    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
//
//    // Look up server in the keychain
//    NSDictionary* found = nil;
//    CFDictionaryRef foundCF;
//    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
//
//    // Check if found
//    found = (__bridge NSDictionary*)(foundCF);
//    if (!found)
//        return;
//
//    // Found
//    gblclass.header_user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
//    gblclass.header_pw = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
//
//
//    //NSLog(@"%@",gblclass.header_user);
//    //NSLog(@"%@",gblclass.header_pw);
//}
//
//
////*********************** Login credential End ******************************














 




-(void) showAnimation
{
    
    [UIView animateWithDuration:0.4 animations:^{
        
        lbl_heading.frame = CGRectMake((self.view.frame.size.width/2) - (lbl_heading.frame.size.width/2), lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        
    }];
    
    //    [UIView animateWithDuration:0.6 animations:^{
    //
    //        emailOutlet.frame = CGRectMake((self.view.frame.size.width/2) - (emailOutlet.frame.size.width/2), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
    //        emailView.frame = CGRectMake((self.view.frame.size.width/2) - (emailView.frame.size.width/2), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
    //
    //    }];
    //
    //    [UIView animateWithDuration:0.8 animations:^{
    //
    //        passOutlet.frame = CGRectMake((self.view.frame.size.width/2) - (passOutlet.frame.size.width/2), passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
    //        passView.frame = CGRectMake((self.view.frame.size.width/2) - (passView.frame.size.width/2), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
    //
    //    }];
    
    
    //    [UIView animateWithDuration:1.0 animations:^{
    //
    //        emailOutlet.frame = CGRectMake((self.view.frame.size.width/6) - (emailOutlet.frame.size.width/6), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
    //
    //    }];
    
    //    [UIView animateWithDuration:1.0 animations:^{
    //
    //        passOutlet.frame = CGRectMake((self.view.frame.size.width/6) - (passOutlet.frame.size.width/6), passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
    //
    //    }];
    
    
    
    //    [UIView animateWithDuration:1.0 animations:^{
    //
    //        forgetPass.frame = CGRectMake((self.view.frame.size.width/2) - (forgetPass.frame.size.width/2), forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
    //
    //    }];
    //
    
    [UIView animateWithDuration:0.6 animations:^{
        vw_login_line.frame = CGRectMake((self.view.frame.size.width/2) - (vw_login_line.frame.size.width/2), vw_login_line.frame.origin.y, vw_login_line.frame.size.width, vw_login_line.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        activateApplicationView.frame = CGRectMake((self.view.frame.size.width/2) - (activateApplicationView.frame.size.width/2), activateApplicationView.frame.origin.y, activateApplicationView.frame.size.width, activateApplicationView.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:1.0 animations:^{
        vw_apply_acct.frame = CGRectMake((self.view.frame.size.width/2) - (vw_apply_acct.frame.size.width/2), vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
        
    }];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        vw_tracking.frame = CGRectMake((self.view.frame.size.width/2) - (vw_tracking.frame.size.width/2), vw_tracking.frame.origin.y, vw_tracking.frame.size.width, vw_tracking.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:1.0 animations:^{
        
        vw_login_line.frame = CGRectMake((self.view.frame.size.width/2) - (vw_login_line.frame.size.width/2), vw_login_line.frame.origin.y, vw_login_line.frame.size.width, vw_login_line.frame.size.height);
        
    }];
    
    
    
}





-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    [self.view endEditing:YES];
    
  
    
    // vw_JB.hidden=YES;
    
    txt_pw1.text=@"";
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults] stringForKey:@"enable_touch"];
    
    
    //    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    //    {
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //    }
    //    else
    //    {
    //
    //        //        if ([gblclass.TnC_chck_On isEqualToString:@"1"])
    //        //        {
    //        //            btn_chck_Image = [UIImage imageNamed:@"check.png"];
    //        //            [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    //        //        }
    //    }
    
    
    
    if (runOnce == YES) {
        
        
        lbl_heading.frame = CGRectMake(-(self.view.frame.size.width + lbl_heading.frame.size.width) ,lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        
        //        emailOutlet.frame = CGRectMake(-(self.view.frame.size.width + emailOutlet.frame.size.width), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //        emailView.frame = CGRectMake(-(self.view.frame.size.width + emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        passOutlet.frame = CGRectMake(-(self.view.frame.size.width + passOutlet.frame.size.width), passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //        passView.frame = CGRectMake(-(self.view.frame.size.width + passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        forgetPass.frame = CGRectMake(-(self.view.frame.size.width + forgetPass.frame.size.width), forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //        btn_login_color.frame = CGRectMake(-(self.view.frame.size.width + btn_login_color.frame.size.width), btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        //
        [self showAnimation];
        
        
    }
    
    
    
    //    [UIView animateWithDuration:10.f animations:^{
    //        self.view.frame = CGRectMake(0.f, 0.f, 320.f, 568.f);
    //    }];
    
    [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //BOOL stringIsValid;
    NSInteger MAX_DIGITS=20;
    
    if ([theTextField isEqual:txt_pw1])
    {
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._- "];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    
    
    if ([theTextField isEqual:txt_login])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._-"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}


-(IBAction)btn_JB_Cancel:(id)sender
{
    vw_JB.hidden=YES;
}

-(IBAction)btn_JB_Next:(id)sender
{
    [vw_JB addSubview:hud];
    [hud showAnimated:YES];
    
    chk_ssl=@"signup";
    [self SSL_Call];
}

-(IBAction)btn_next:(id)sender
{
    
    //    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNetworkChnageNotification:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
    [self.view endEditing:YES];
    
    [txt_login resignFirstResponder];
    [txt_pw1 resignFirstResponder];
    
    gblclass.term_conditn_chk_place=@"";
    
    NSString *Str_user = [txt_login.text stringByReplacingOccurrencesOfString:@" " withString: @""];
    NSString *Str_pw = [txt_pw1.text stringByReplacingOccurrencesOfString:@" " withString: @""];
    
    login_name = [txt_login.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    login_pw = [txt_pw1.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    randomNumber = arc4random() % 10+1;
    
    if ([Str_user isEqualToString:@""] || [Str_user isEqualToString:@" "])
    {
        [self custom_alert:@"Please enter valid Login Name" :@"0"];
        return ;
        
    }
    else if([Str_pw isEqualToString:@""] || [Str_pw isEqualToString:@" "])
    {
        [self custom_alert:@"Please enter valid Password" :@"0"];
        return ;
        
    }
    else
    {
        
        gblclass.user_login_name=txt_login.text;
        //   [self Existing_UserLogin:@""];
        
        gblclass.signup_login_name=login_name;
        gblclass.signup_pw=login_pw;
        
        
        NSString* t_n_c = [defaults valueForKey:@"t&c"];
        NSString* device_jb_chk = [defaults valueForKey:@"isDeviceJailBreaked"];
        
        device_jb_chk = nil;
        t_n_c = nil;
        if ([device_jb_chk isEqualToString:@"1"] || [device_jb_chk isEqualToString:@"2"])
        {
            
            if ([t_n_c isEqualToString:@"1"] || [t_n_c isEqualToString:@"2"])
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"signup";
                    [self SSL_Call];
                }
                
//                [hud hideAnimated:YES];
//                [self slide_right];
//
//                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
//                [self presentViewController:vc animated:NO completion:nil];
            }
            else
            {
//
//                NSLog(@"%@",gblclass.str_processed_login_chk);
//                NSLog(@"%@",gblclass.str_processed_login_chk);
                
                if ([gblclass.str_processed_login_chk isEqualToString:@"1"] && [gblclass.forgot_pw_click isEqualToString:@"1"])
                {
                    [hud showAnimated:YES];
                    [self.view addSubview:hud];
                    [hud showAnimated:YES];
                    
                    [self checkinternet];
                    if (netAvailable)
                    {
                        chk_ssl=@"signup";
                        [self SSL_Call];
                    }
                }
                else
                {
                    [hud hideAnimated:YES];
                    [self slide_right];
                    gblclass.signUp_through_cnic = @"0";
                    
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
                    [self presentViewController:vc animated:NO completion:nil];
                }
                
               
            }
           
        }
        else
        {
            
//            NSLog(@"%@",gblclass.str_processed_login_chk);
//            NSLog(@"%@",gblclass.str_processed_login_chk);
            
            if ([gblclass.str_processed_login_chk isEqualToString:@"1"] && [gblclass.forgot_pw_click isEqualToString:@"1"])
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"signup";
                    [self SSL_Call];
                }
            }
            else
            {
                            [hud hideAnimated:YES];
                            [self slide_right];
                            gblclass.signUp_through_cnic = @"0";
            
                            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
                            [self presentViewController:vc animated:NO completion:nil];
            }
            
//                        [hud showAnimated:YES];
//                        [self.view addSubview:hud];
//                        [hud showAnimated:YES];
//
//
//                        [self checkinternet];
//                        if (netAvailable)
//                        {
//                            chk_ssl=@"signup";
//                            [self SSL_Call];
//                        }
        }
        
        
       
        //saki 29 june 2018

//        if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && ![t_n_c isEqualToString:@"2"])
//        {
//            gblclass.TnC_chck_On=@"1";
//            device_jail = @"1";
//            vw_JB.hidden=NO;
//            vw_tNc.hidden=YES;
//            btn_next.enabled=NO;
//            btn_resend_otp.enabled=NO;
//            btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
//            [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
//
//
//        }
//        else
//        {
        
  
//            [hud showAnimated:YES];
//            [self.view addSubview:hud];
//            [hud showAnimated:YES];
            
           
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl=@"signup";
//                [self SSL_Call];
//            }
            
 
 //       }
    }
    
}


-(void) Existing_UserLogin:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        UIDevice *deviceInfo = [UIDevice currentDevice];
        
        //NSLog(@"Device name:  %@", deviceInfo.name);
        
        login_name = [txt_login.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        login_pw = [txt_pw1.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        randomNumber = arc4random() % 10+1;
        
        NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
        
        gblclass.login_session=random;
        
     
        
        //    gblclass.signup_username=login_name;
        //    gblclass.signup_pw=login_pw;
        
        NSString* device_value;
        if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"])
        {
            device_value=@"No";
            gblclass.chk_termNCondition = @"0";
        }
        else if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"])
        {
            device_value=@"Yes";
        }
        else
        {
            device_value=@"No";
            gblclass.chk_termNCondition = @"0";
        }
        
        
        gblclass.sign_in_username = gblclass.signup_login_name;
        gblclass.sign_in_pw = gblclass.signup_pw;
     
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"asdf"],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:gblclass.signup_login_name],
                                    [encrypt encrypt_Data:gblclass.signup_pw],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb],
                                    [encrypt encrypt_Data:gblclass.chk_termNCondition], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"LogBrowserInfo",
                                                                       @"hCode",
                                                                       @"strLoginName",
                                                                       @"strPassword",
                                                                       @"strDeviceID",
                                                                       @"strIsTouchLogin",
                                                                       @"isRooted",
                                                                       @"isTnCAccepted", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"ExistingUserMobRegRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject)
         {
             NSError *error1=nil;
             //NSArray *arr = (NSArray *)responseObject;
             dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             login_status_chck=[dic objectForKey:@"Response"];
             
             //   if ([login_status_chck isEqualToString:@"0"])
             
             if ([[dic objectForKey:@"Response"] integerValue]==0)
             {
                 
                 [self.avplayer pause];
                 
                 gblclass.From_Exis_login = @"1";
                 gblclass.sign_Up_chck = @"yes";
                 //gblclass.signup_login_name = txt_login.text;
                 //gblclass.signup_pw = txt_pw1.text;
                 gblclass.exis_user_mobile_pin = [dic objectForKey:@"strMobileNum"];
                 gblclass.M3sessionid = [dic objectForKey:@"strRetSessionId"];
                 gblclass.Exis_relation_id = [dic objectForKey:@"strRelationshipId"];
                 gblclass.user_id = [dic objectForKey:@"struserId"];
                 gblclass.sign_up_token = [dic objectForKey:@"Token"];
                 
                 [self segue];
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==103)
             {
                 
                 if ([gblclass.str_processed_login_chk isEqualToString:@"1"])
                 {
//                     gblclass.str_processed_login_chk = @"0";
                     mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                     vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"create_option"];
                     [self presentViewController:vc animated:NO completion:nil];
                 }
                 else
                 {
                     mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                     vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup"];
                     [self presentViewController:vc animated:NO completion:nil];
                 }
                 
             }
             else
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
             }
             
         }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) Is_Rooted_Device:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //public void IsRootedDevice(string Device_ID, string IP, Int16 isRooted, string isTncAccepted, string callingSource)
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:@"login"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"IP",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"callingSource", nil]];
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsRootedDevice" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      vw_JB.hidden=YES;
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      vw_JB.hidden=YES;
                      [self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  
//                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Try again later." :@"0"];
        
    }
    
}



-(void)ApplyForAccount:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"name"],
                                                                       [defaults valueForKey:@"mobile_num"],
                                                                       [defaults valueForKey:@"cnic"],
                                                                       [defaults valueForKey:@"email"],
                                                                       [defaults valueForKey:@"nationality"],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:session_id],
                                                                       [encrypt encrypt_Data:@"0"],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"FullName",
                                                                       @"strMobileNo",
                                                                       @"strCNIC",
                                                                       @"strEmailAddress",
                                                                       @"IsPakistani",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"strSessionId",
                                                                       @"ChannelId", nil]];
        
        
//        NSLog(@"%@\n %@\n %@\n %@\n %@\n",[encrypt de_crypt_url:[defaults valueForKey:@"name"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"cnic"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"email"]],
//              [encrypt de_crypt_url:[defaults valueForKey:@"nationality"]]);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ApplyForAccount" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
                      gblclass.request_id_onbording = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Temp_RequestId"]];
                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
                      
                      [self Generate_OTP_For_DA_Request:@""];
                      
                  }  else {
                      
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) Generate_OTP_For_DA_Request:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [defaults valueForKey:@"mobile_num"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [hud hideAnimated:YES];
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      //                      {
                      //                          Response = 0;
                      //                          strReturnMessage = "OTP Generated Successfully";
                      //                      }
                      
                      gblclass.user_name_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"name"]];
                      gblclass.cnic_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"cnic"]];
                      gblclass.email_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"email"]];
                      gblclass.mobile_number_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]];
                      gblclass.nationality_onbording = [encrypt de_crypt_url:[defaults valueForKey:@"nationality"]];
                      
                      // [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyOtpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void)GetTokenRequest:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        gblclass.request_id_onbording = req_id;
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                    [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:req_id],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",   nil]];
        
        
        //NSLog(@"%@",[encrypt de_crypt_url:[defaults valueForKey:@"cnic"]]);
        //NSLog(@"%@",[encrypt de_crypt_url:[defaults valueForKey:@"mobile_num"]]);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetTokenRequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                 // [hud hideAnimated:YES];
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"3"]){
                      
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:@"Your request has been successfuly completed!"
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
                      
                    
                      [self removeUserdata];
                      [defaults removeObjectForKey:@"apply_acct"];
                      [defaults synchronize];
                      
                     [self Apply_acct_buttons];
                      
                       [hud hideAnimated:YES];
                  
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"])
                  {
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
                      
                      if ([btn_chk isEqualToString:@"track"])
                      {
                          [hud hideAnimated:YES];
                          [self slide_right];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else
                      {
                          [self On_Boarding_Cancle:@""];
                      }
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  // [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  
//                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




-(void)On_Boarding_Cancle:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //        // CNIC Load ::
        //        NSUserDefaults *cnic = [NSUserDefaults standardUserDefaults];
        //        cnic =[cnic valueForKey:@"cnic"];
        //        gblclass.cnic_onbording = (NSString*) [encrypt de_crypt_url:cnic];
        //        NSLog(@"%@",gblclass.cnic_onbording);
        
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:req_id],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",   nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"CancelOnBoarding" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                     
                      // vw_JB.hidden=YES;
                      
                      [defaults setValue:@"" forKey:@"req_id"];
                      [defaults setValue:@"" forKey:@"apply_acct"];
                      [defaults synchronize];
                      
                      [self Apply_acct_buttons];
                      [hud hideAnimated:YES];
                      
                  } else {
                      [hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
//                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
        
    }
    
}




-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}



- (NSString *)platformRawString
{
    size_t size = 0;
    //sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    //   sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

- (NSString *)platformNiceString
{
    
    NSString *platform = [self platformRawString];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (4G,2)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (4G,3)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

-(void) UserLogin:(NSString *)strIndustry
{
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    UIDevice *deviceInfo = [UIDevice currentDevice];
    
    //NSLog(@"Device name:  %@", deviceInfo.name);
    
    
    
    login_name = [txt_login.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    randomNumber = arc4random() % 10+1;
    
    NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
    
    gblclass.login_session=random;
    
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:login_name,random,@"1234",[GlobalStaticClass getPublicIp],@"osdf",nil] forKeys:[NSArray
                                                                                                                                                                            arrayWithObjects:@"strLoginName",@"strSerialNumber",@"strSessionId",@"IP",@"LogBrowserInfo",nil]];
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"UserLogin" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //      NSError *error;
              //      NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)responseObject;
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              // //NSLog(@"%@",[dic objectForKey:@"response"]);
              login_status_chck=[dic objectForKey:@"Response"];
              gblclass.pin_pattern=[dic objectForKey:@"PinPattern"];
              gblclass.user_id=[dic objectForKey:@"UserId"];
              gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                  
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
                  [self presentViewController:vc animated:NO completion:nil];
                  [hud hideAnimated:YES];
                  
                  return ;
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
              {
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                  
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                  [self presentViewController:vc animated:NO completion:nil];
                  [hud hideAnimated:YES];
                  
                  return ;
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==-96)
              {
                  [self segue];
              }
              
              [hud hideAnimated:YES];
              
              [self segue];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              [self custom_alert:[error localizedDescription] :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void)segue
{
    if ([login_status_chck isEqualToString:@"0"] || [login_status_chck isEqualToString:@"-96"])
    {
        //NSLog(@"%@",txt_login.text);
        gblclass.user_login_name=login_name;
        [timer_class createTimer];
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        
        
        //        lbl_heading.frame = CGRectMake(self.view.frame.size.width + lbl_heading.frame.size.width, lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        //
        //        emailOutlet.frame = CGRectMake(self.view.frame.size.width + emailOutlet.frame.size.width, emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //        emailView.frame = CGRectMake(self.view.frame.size.width + emailView.frame.size.width, emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        passOutlet.frame = CGRectMake(self.view.frame.size.width + passOutlet.frame.size.width, passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //        passView.frame = CGRectMake(self.view.frame.size.width + passView.frame.size.width, passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        forgetPass.frame = CGRectMake(self.view.frame.size.width + forgetPass.frame.size.width, forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //        btn_login_color.frame = CGRectMake(self.view.frame.size.width + btn_login_color.frame.size.width, btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        
        runOnce = YES;
        
        
        
        //        [UIView animateWithDuration:0.4 animations:^{
        //            lbl_heading.frame = CGRectMake(-(self.view.frame.size.width+lbl_heading.frame.size.width), lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        //
        //        }];
        
        //        [UIView animateWithDuration:0.6 animations:^{
        //
        //            emailOutlet.frame = CGRectMake(-(self.view.frame.size.width+emailOutlet.frame.size.width), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:0.8 animations:^{
        //
        //            emailOutlet.frame = CGRectMake(-(self.view.frame.size.width+emailOutlet.frame.size.width), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:0.8 animations:^{
        //
        //            passOutlet.frame = CGRectMake(-(self.view.frame.size.width+passOutlet.frame.size.width), passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //            passView.frame = CGRectMake(-(self.view.frame.size.width+passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:1 animations:^{
        //
        //            forgetPass.frame = CGRectMake(-(self.view.frame.size.width+forgetPass.frame.size.width), forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //
        //        }];
        //
        //        [UIView animateWithDuration:1.1 animations:^{
        //
        //            btn_login_color.frame = CGRectMake(-(self.view.frame.size.width+btn_login_color.frame.size.width), btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        //
        //
        //        }];
        
        //        [UIView animateWithDuration:1.1 animations:^{
        //
        //            vw_apply_acct.frame = CGRectMake(-(self.view.frame.size.width+vw_apply_acct.frame.size.width), vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
        //
        //        }];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.8
                                         target:self
                                       selector:@selector(present_Controller)
                                       userInfo:nil
                                        repeats:NO];
        
        
        
        
        
        
        //        CATransition *transition = [ CATransition animation ];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        
        //[self performSegueWithIdentifier:@"question" sender:nil];
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Retry" :@"0"];
        
    }
}

-(void)present_Controller
{
    [hud hideAnimated:YES];
    [self slide_right];
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(void) UserMobReg:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    //  NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    
    NSString*strCountry,*strTxtEmail,*strTxtKeyword,*strBranchCode,*strTxtAccountNumber,*strCardPin,*strAccType,*strUserId,*strDeviceID,*strSessionId;
 
    //    IP = "1.1.1.1";
    //    hCode = abv;
    strAccType = @"WalletCard";
    strBranchCode = @"0";
    strCardPin = @"1111";
    strCountry = @"1";
    strDeviceID = gblclass.Udid;
    strSessionId = @"1234";
    strTxtAccountNumber = @"5327090949000026";
    strTxtEmail = @"";
    strTxtKeyword = @"";
    strUserId = @"0";
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"0",@"1234",[GlobalStaticClass getPublicIp],@"abv",@"1",strTxtEmail,strTxtKeyword,strBranchCode,strTxtAccountNumber,strCardPin,strAccType,gblclass.Udid, nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"hCode",@"strCountry",@"strTxtEmail",@"strTxtKeyword",@"strBranchCode",@"strTxtAccountNumber",@"strCardPin",@"strAccType",@"strDeviceID", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"UserMobReg" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //      NSError *error;
              //      NSDictionary *dic = (NSDictionary *)responseObject;
              
              NSString* responsecode =[dic objectForKey:@"Response"];
              if ([responsecode isEqualToString:@"0" ]){
                  
                  gblclass.signup_relationshipid=[dic objectForKey:@"strRelationShipId"];
                  gblclass.signup_mobile=[dic objectForKey:@"strMobileNumber"];
                  
                  
                  
                  [self slide_right];
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
                  //777   [self presentModalViewController:vc animated:NO];
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  NSString* exe=[GlobalStaticClass stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  [self showAlert:exe:@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:[error localizedDescription] :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertController * alert=   [UIAlertController
                                                     alertControllerWithTitle:Title
                                                     message:exception
                                                     preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:UIAlertActionStyleDefault
                                                                        handler:nil]; //You can use a block here to handle a press on this button
                       [alert addAction:actionOk];
                       
                       [self presentViewController:alert animated:YES completion:nil];
                   });
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_pw1 resignFirstResponder];
    [txt_login resignFirstResponder];
}

#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //  //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view resignFirstResponder];
    
    // emailOutlet.text = @"";
    // txt_login.text = @"";
    passOutlet.text = @"";
    txt_pw1.text = @"";
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    //    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(IBAction)btn_meni1:(id)sender
{
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"more_slide"];
    
    
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_menu:(id)sender
{
    [self performSegueWithIdentifier:@"showPhoto" sender:self];
    self.childViewControllers.self;
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}


-(IBAction)btn_Signup:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"sign_up"];
    //  [self presentViewController:vc animated:NO completion:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_feature:(id)sender
{
    
    //[hud showAnimated:YES];
    //
    //
    [self custom_alert:@"Please register your User." :@"0"];
    
    
    
    //  [self checkinternet];
    //   if (netAvailable)
    //   {
    
    //       chk_ssl=@"qr";
    //       [self SSL_Call];
    
    //        gblclass.chk_qr_demo_login=@"1";
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [self.view.window.layer addAnimation:transition forKey:nil];
    //
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //   receive_qr_login  feature
    //         [self presentViewController:vc animated:NO completion:nil];
    //  }
    
    
    
    //    [self slide_right];
    //
    //    gblclass.chk_qr_demo_login=@"1";
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //  receive_qr_login   feature
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_offer:(id)sender
{
//  gblclass.tab_bar_login_pw_check=@"login";
//  [self peekabooSDK:@"deals"];
    
//    [self peekabooSDK:@{@"type": @"deals",}];
    
    [self custom_alert:Offer_msg :@""];
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
//  [self peekabooSDK:@"locator"];
//    [self peekabooSDK:@{@"type": @"locator",}];
    
    gblclass.forget_pw_btn_click = @"2";
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
////
////    new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_Mpin:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Mpin_login"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_back:(id)sender
{
    // gblclass.user_login_name=@"";
    
    [self slide_left];
    
    //saki comment on 4 april 2018
    //    if ( [gblclass.str_processed_login_chk isEqualToString:@"1"])
    //    {
    //        [self dismissViewControllerAnimated:NO completion:nil];
    //    }
    //    else
    //    {
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"]; //next new_exist
    [self presentViewController:vc animated:NO completion:nil];
    //    }
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"signup"])
        {
            chk_ssl=@"";
            [self Existing_UserLogin:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        else if ([chk_ssl isEqualToString:@"jb_cancel"])
        {
            chk_ssl=@"";
            [self Is_Rooted_Device:@""];
        }
        else if ([chk_ssl isEqualToString:@"applyForAccount"])
        {
            chk_ssl = @"";
            [self ApplyForAccount:@""];
            //  [self GetOnBoardData:@""];
        }
        else if ([chk_ssl isEqualToString:@"getTokenRequest"])
        {
            chk_ssl = @"";
            [self GetTokenRequest:@""];
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData] && remoteCertificateData != nil)
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            //[self Existing_UserLogin:@""];
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"signup"])
    {
        chk_ssl=@"";
        chk_pekaboo_service_hit = @"0";
        [self Existing_UserLogin:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        chk_pekaboo_service_hit = @"0";
        [self Is_Instant_Pay_Allow:@""];
    }
    else if ([chk_ssl isEqualToString:@"jb_cancel"])
    {
        chk_ssl=@"";
        chk_pekaboo_service_hit = @"0";
        [self Is_Rooted_Device:@""];
    }
    else if ([chk_ssl isEqualToString:@"applyForAccount"])
    {
        chk_ssl = @"";
        chk_pekaboo_service_hit = @"0";
        [self ApplyForAccount:@""];
        //  [self GetOnBoardData:@""];
    }
    else if ([chk_ssl isEqualToString:@"getTokenRequest"])
    {
        chk_ssl = @"";
        chk_pekaboo_service_hit = @"0";
        [self GetTokenRequest:@""];
    }
    
    //    [self.connection cancel];
    
}


-(void)pekaboo
{
    @try
    {
    
       chk_pekaboo_service_hit = @"1";
     //   NSLog(@"%@",gblclass.arr_current_location);
      //  NSLog(@"%@",[gblclass.arr_current_location objectAtIndex:0]);
       // NSLog(@"%@",gblclass.lati_pekabo);
        //NSLog(@"%@",gblclass.longi_pekabo);
        
        if ([gblclass.arr_current_location count] > 1)
        {
            gblclass.lati_pekabo = [gblclass.arr_current_location objectAtIndex:0];
            gblclass.longi_pekabo = [gblclass.arr_current_location objectAtIndex:1];
        }
        
        NSMutableString* requestURL = [[NSMutableString alloc] init];
        [requestURL appendString:@"https://secure-sdk.peekaboo.guru/jRavhbSijxdc2raUB0wM"];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [NSString stringWithString:requestURL]]];
        
        NSError *error = nil;
        NSData *json;
            [request setHTTPMethod: @"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:@"7db159dc932ec461c1a6b9c1778bb2b0" forHTTPHeaderField:@"ownerkey"];
        
        NSDictionary *parameter = [NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:gblclass.lati_pekabo ,@"js6nwf", gblclass.longi_pekabo, @"pan3ba", nil], @"asdxa", nil];
        
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:&error];
        
        
        [request setHTTPBody: json];
         [request  setURL:[NSURL URLWithString:requestURL]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            if (data.length > 0 && connectionError == nil)
            {
                
                NSDictionary *rest_data = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                
                [hud hideAnimated:YES];
               // NSLog(@"%@",[rest_data objectForKey:@"isWhitelisted"]);
              //  NSLog(@"%@",[rest_data objectForKey:@"city"]);
                
                NSString* is_Whitelisted_chck = [NSString stringWithFormat:@"%@", [rest_data objectForKey:@"isWhitelisted"]];
                
                        [hud hideAnimated:YES];
                
                           //  is_Whitelisted_chck = @"1";
                        if ([is_Whitelisted_chck isEqualToString:@"0"])
                        {
                            vw_apply_acct.hidden = YES;
                          //  vw_tracking.hidden = YES;
                            
                            req_id = [encrypt de_crypt_url:[defaults valueForKey:@"req_id"]];
                            apply_acct = [defaults valueForKey:@"apply_acct"];
                            
                            if (![req_id isEqualToString:@""] && [apply_acct isEqualToString:@"1"]) //Tracking
                            {
                                vw_apply_acct.hidden = YES;
                                vw_tracking.hidden = NO;
                                
                                btn_continue.hidden = YES;
                                vw_btn_continue_line.hidden = YES;
                                btn_cancel.hidden = YES;
                                vw_btn_cancel_line.hidden = YES;
                                btn_tracking.hidden = NO;
                                vw_btn_tracking_line.hidden = NO;
                            }
                            
                        }
                        else
                        {
                            vw_apply_acct.hidden = NO;
                            [self Apply_acct_buttons];
                        }
                
               // NSLog(@"%@",rest_data);
                
            }
        }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
        if (self.responseData == nil)
        {
            [hud hideAnimated:YES];
            self.responseData = [NSMutableData dataWithData:data];
        }
        else
        {
            [self.responseData appendData:data];
        }
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        [self printMessage:response];
        self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];

    if(error.code == NSURLErrorTimedOut)
    {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    }


}


- (NSData *)skabberCert
{
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
   // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


-(IBAction)btn_exist_forgot_pw:(id)sender
{
    //    btn_forgot_chck=@"1";
    //    [self showAlertwithcancel:@"Are you sure, you want to reset your Existing Password?" :@"Attention"];
    
    gblclass.forget_pw_btn_click=@"1"; //0 saki 12 april 2019
    gblclass.forgot_pw_click = @"1";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    
    //new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert1 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([btn_forgot_chck isEqualToString:@"1"])
    {
        //NSLog(@"%ld",(long)buttonIndex);
        if (buttonIndex == 1)
        {
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 0)
        {
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"existing_forgot"];
            //   [self presentViewController:vc animated:NO completion:nil];
            
            [self presentViewController:vc animated:YES completion:nil];
            
        }
        btn_forgot_chck=@"0";
    }
}


///********************** SIGN IN NEW METHOD 27 OCT 2016 ********************************



-(IBAction)BTN_SIGN_IN_NEW:(id)sender
{
    [self mob_Sign_In:@""];
}



-(void) mob_Sign_In:(NSString *)strIndustry
{
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl]];   //baseURL];
    
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:@"" password:@""];
    
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    
    //NSLog(@"%@",gblclass.user_login_name);
    
    NSDictionary *dictparam;
    
    //    NSString *msg = [[NSUserDefaults standardUserDefaults]
    //                     stringForKey:@"Mpin-pw"];
    
    dictparam = [NSDictionary dictionaryWithObjects:
                 [NSArray arrayWithObjects:[encrypt encrypt_Data:txt_login.text],
                  [encrypt encrypt_Data:txt_pw1.text],
                  [encrypt encrypt_Data:gblclass.Udid],
                  [encrypt encrypt_Data:@"1234"],
                  [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                  [encrypt encrypt_Data:@"0"], nil]
                                            forKeys:[NSArray arrayWithObjects:@"LoginName",
                                                     @"strPassword",
                                                     @"strDeviceID",
                                                     @"strSessionId",
                                                     @"IP",
                                                     @"hCode", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"mobSignIn" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              @try {
                  
                  //NSError *error;
                  //NSArray *arr = (NSArray *)responseObject;
                  //  NSDictionary *dic = (NSDictionary *)responseObject;
                  NSDictionary *dic1 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  
                  NSMutableArray*  aa=[[NSMutableArray alloc] init];
                  //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  
                  Pw_status_chck=[dic objectForKey:@"Response"];
                  gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                  gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  // gblclass.user_id=[dic objectForKey:@"strUserId"];
                  gblclass.M3sessionid=@"1234";
                  gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  
                  NSArray* array=    [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  
                  gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                  
                  gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                  gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                  
                  NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  if(mobileno==(NSString *)[NSNull null])
                  {
                      gblclass.Mobile_No =@"";
                  }
                  else
                  {
                      gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  }
                  
                  gblclass.arr_user_pw_table2 =
                  [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  
                  gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                  
                  NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                  
                  if (Email == (NSString *)[NSNull null])
                  {
                      Email=@"N/A";
                      [a addObject:Email];
                  }
                  else
                  {
                      // [a addObject:Email];
                      gblclass.user_email=Email;
                  }
                  
                  NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                  
                  if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                  {
                      FundsTransferEnabled=@"N/A";
                      [a addObject:FundsTransferEnabled];
                  }
                  else
                  {
                      // [a addObject:FundsTransferEnabled];
                  }
                  
                  NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                  
                  if (Mobile_No ==(NSString *) [NSNull null])
                  {
                      Mobile_No=@"N/A";
                      [a addObject:Mobile_No];
                  }
                  else
                  {
                      // [a addObject:Mobile_No];
                  }
                  
                  //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                  //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //              [a removeAllObjects];
                  //              bbb=@"";
                  
                  
                  //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                  
                  gblclass.actsummaryarr =
                  [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                  
                  
                  //NSLog(@"%@",gblclass.actsummaryarr);
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
                  {
                      
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
                      //  [self presentViewController:vc animated:NO completion:nil];
                      
                      [self presentViewController:vc animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
                  {
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"existing_forgot"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      //                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                      //                       [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
                  {
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                      [alert addAction:ok];
                      //
                      //                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==101)
                  {
                      [a removeAllObjects];
                      
                      //NSLog(@"%@",gblclass.actsummaryarr);
                      
                      
                      //                NSUInteger *set;
                      for (dic in gblclass.actsummaryarr)
                      {
                          
                          NSString* acct_name=[dic objectForKey:@"account_name"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          
                          NSString* acct_no=[dic objectForKey:@"account_no"];
                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                          {
                              acct_no=@"N/A";
                              [a addObject:acct_no];
                          }
                          else
                          {
                              [a addObject:acct_no];
                          }
                          
                          
                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                          {
                              acct_id=@"N/A";
                              [a addObject:acct_id];
                          }
                          else
                          {
                              [a addObject:acct_id];
                          }
                          
                          NSString* is_default=[dic objectForKey:@"is_default"];
                          NSString* is_default_curr=[dic objectForKey:@"ccy"];
                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                          
                          if ([is_default isEqualToString:@"1"])
                          {
                              
                              gblclass.is_default_acct_id=acct_id;
                              gblclass.is_default_acct_id_name=is_default_name;
                              gblclass.is_default_acct_no=is_default_acct_no;
                              gblclass.is_default_m3_balc=is_default_balc;
                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                              gblclass.is_default_currency=is_default_curr;
                          }
                          
                          //privileges
                          NSString* privileges=[dic objectForKey:@"privileges"];
                          //777                    NSString* pri=[dic objectForKey:@"privileges"];
                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //Second bit 1 for withDraw ..
                          
                          
                          if ([privileges isEqualToString:@"0"])
                          {
                              privileges=@"0";
                              [a addObject:privileges];
                          }
                          else
                          {
                              [a addObject:privileges];
                          }
                          
                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                          {
                              account_type_desc=@"N/A";
                              [a addObject:account_type_desc];
                          }
                          else
                          {
                              [a addObject:account_type_desc];
                          }
                          //account_type
                          
                          NSString* account_type=[dic objectForKey:@"account_type"];
                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                          {
                              account_type=@"N/A";
                              [a addObject:account_type];
                          }
                          else
                          {
                              [a addObject:account_type];
                          }
                          
                          
                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                          
                          
                          //((NSString *)[NSNull null] == m3_balc)
                          //[m3_balc isEqualToString:@" "] ||
                          
                          
                          //27 OCt New Sign IN Method :::
                          
                          //                          if (m3_balc == [NSNull null])
                          //                          {
                          //                              m3_balc=@"0";
                          //                              [a addObject:m3_balc];
                          //                          }
                          //                          else
                          //                          {
                          //                              [a addObject:m3_balc];
                          //                          }
                          
                          
                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                          if ( branch_name == (NSString *)[NSNull null])
                          {
                              branch_name=@"N/A";
                              [a addObject:branch_name];
                          }
                          else
                          {
                              [a addObject:branch_name];
                          }
                          
                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                          if ( available_balance ==(NSString *) [NSNull null])
                          {
                              available_balance=@"N/A";
                              [a addObject:available_balance];
                          }
                          else
                          {
                              [a addObject:available_balance];
                          }
                          
                          NSString* br_code=[dic objectForKey:@"br_code"];
                          if ( br_code == (NSString *)[NSNull null])
                          {
                              br_code=@"0";
                              [a addObject:br_code];
                          }
                          else
                          {
                              [a addObject:br_code];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_act_statmnt_name addObject:bbb];
                          
                          if ([privileges isEqualToString:@"1"])
                          {
                              [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                          }
                          
                          //                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                          
                          
                          
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                          [aa removeAllObjects];
                          
                      }
                      
                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      
                      
                      [hud hideAnimated:YES];
                      [self segue1];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      
                      [self custom_alert:[dic objectForKey:@"ReturnMessage"] :@"0"];
                      
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                      [alert addAction:ok];
                      //
                      //                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
              }
              @catch (NSException *exception)
              {
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Unable to Signup, Please try again later." :@"0"];
                  
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to login, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",[error localizedDescription]);
              //NSLog(@"%@",[error localizedFailureReason]);
              //NSLog(@"%@",[error localizedRecoverySuggestion]);
              //NSLog(@"%@",task);
              
              
              [self custom_alert:@"Unable to Signup, Please try again later." :@"0"];
              
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to login, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}


-(void)segue1
{
    
    
    //NSLog(@"%@",gblclass.ssl_pin_check);
    
    
    
    if ([Pw_status_chck isEqualToString:@"101"] || [Pw_status_chck isEqualToString:@"0"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        gblclass.isLogedIn=YES;
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    
}

-(void) removeUserdata {
    [defaults removeObjectForKey:@"name"];
    [defaults removeObjectForKey:@"cnic"];
    [defaults removeObjectForKey:@"mobile_num"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"nationality"];
    [defaults removeObjectForKey:@"req_id"];
    [defaults synchronize];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            [hud hideAnimated:YES];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        ////////// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllow" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:gblclass.Udid];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      gblclass.chk_qr_demo_login=@"1";
                      gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                      gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                      
                      gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                      
                      
                      [gblclass.arr_instant_qr removeAllObjects];
                      
                      NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                      
                      if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                      {
                          acct_name=@"N/A";
                          [a addObject:acct_name];
                      }
                      else
                      {
                          [a addObject:acct_name];
                      }
                      
                      NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                      
                      if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                      {
                          strAccountTitle=@"N/A";
                          [a addObject:strAccountTitle];
                      }
                      else
                      {
                          [a addObject:strAccountTitle];
                      }
                      
                      NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                      
                      if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                      {
                          strDefaultAccQRCode=@"N/A";
                          [a addObject:strDefaultAccQRCode];
                      }
                      else
                      {
                          [a addObject:strDefaultAccQRCode];
                      }
                      
                      NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                      
                      if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                      {
                          strDefaultAccountMask=@"N/A";
                          [a addObject:strDefaultAccountMask];
                      }
                      else
                      {
                          [a addObject:strDefaultAccountMask];
                      }
                      
                      
                      NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      [gblclass.arr_instant_qr addObject:bbb];
                      
                      bbb=@"";
                      
                      
                      
                      
                      
                      
                      CATransition *transition = [CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_instant_qr"]; //   receive_qr_login login_instant_qr
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}


-(IBAction)btn_TnC:(id)sender
{
    [self slide_right];
    
    gblclass.term_conditn_chk_place=@"login";
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_email_chck:(id)sender
{
    
    if ([btn_flag_email isEqualToString:@"0"])
    {
        btnImage = [UIImage imageNamed:@"check.png"];
        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
        btn_flag_email=@"1";
        gblclass.chk_term_condition=@"1";
        btn_agree.enabled=YES;
    }
    else
    {
        btnImage = [UIImage imageNamed:@"uncheck.png"];
        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
        btn_flag_email=@"0";
        gblclass.chk_term_condition=@"0";
        btn_agree.enabled=NO;
    }
}

-(IBAction)btn_agree:(id)sender
{
    vw_term.hidden=YES;
}

-(IBAction)btn_activate_ubl_acct:(id)sender
{
    gblclass.forgot_pw_click = @"0";
 
    //http://www.ubldirect.com/Corporate/ebank.aspx
     
    gblclass.forget_pw_btn_click=@"1";  //0 saki 12 april 2019
    gblclass.forgot_pw_click = @"1";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    
    //new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];
    
    
//    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    vc = [storyboard instantiateViewControllerWithIdentifier:@"SecureCodeOrProfileInfoViewController"];
//    [self presentViewController:vc animated:NO completion:nil];
//    [self slide_right];
    
}

-(IBAction)btn_apply_for_acct:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"NewApplicationVC"]; //NewApplicationVC RegisterAccountVC
    [self presentViewController:vc animated:NO completion:nil];
    [self slide_right];
}

-(IBAction)btn_track:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    btn_chk = @"track";
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"getTokenRequest";
        [self SSL_Call];
    }
}

-(IBAction)btn_continue:(id)sender
{
    btn_chk = @"continue";
    [self checkinternet];
    if (netAvailable)
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        
        gblclass.conti_session_id = @"c";
        session_id = @"1234|c";
        chk_ssl = @"applyForAccount";
        [self SSL_Call];
    }
}

-(IBAction)btn_cancel:(id)sender
{
    [hud showAnimated:YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to cancel your current request!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertActionOk = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        btn_chk = @"cancel";
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        chk_ssl = @"getTokenRequest";
        [self SSL_Call];
    }];
    
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:alertActionOk];
    [alertController addAction:alertActionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}



//-(void)peekaboo_geo
//{
//
//    //    NSURL *url = [NSURL URLWithString:@"https://secure-sdk.peekaboo.guru/njxklawnxioakwmskoajsan"];
//    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
//    //
//    //  //  NSData *requestData = [@"name=testname&suggestion=testing123" dataUsingEncoding:NSUTF8StringEncoding];
//    //
//    //   // NSDictionary* headers = [NSHTTPCookie requestHeaderFieldsWithCookies:authCookies];
//    //
//    //    [request setHTTPMethod:@"POST"];
//    //   // [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
//    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    //  //  [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
//    //  //  [request setHTTPBody: requestData];
//    // //   [request setAllHTTPHeaderFields:headers];
//    //
//    //    [[NSURLConnection alloc] initWithRequest:request delegate:self];
//
//
//
//    NSURL *httpsURL = [NSURL URLWithString:@"https://secure-sdk.peekaboo.guru/njxklawnxioakwmskoajsan"];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:lati_pekaboo forHTTPHeaderField:@"js6nwf"];
//    [request setValue:longi_pekaboo forHTTPHeaderField:@"pan3ba"];
//    [request setHTTPMethod:@"POST"];
//
//    [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    // [self.connection start];
//}

//- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    [responseData setLength:0];
//}
//
////- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
////{
////    [responseData appendData:data];
////}
//
////- (void) connectionDidFinishLoading:(NSURLConnection *)connection
////{
////    //[connection release];
////
////    NSString* responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
////
////
////    NSLog(@"%@",responseString);
////    //  [viewController returnHtmlFromPost:responseString];
////
////    //  [responseString release];
////}


//- (void)peekabooSDK:(NSString *)type {
//    NSLog(@"%@",gblclass.peekabo_kfc_userid);
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"United Arab Emirates",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
//}

- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    //saki 
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}

@end

