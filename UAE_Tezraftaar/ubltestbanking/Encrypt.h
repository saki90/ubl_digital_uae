//
//  Encrypt.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 21/09/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>


#define FBENCRYPT_ALGORITHM     kCCAlgorithmAES
#define FBENCRYPT_BLOCK_SIZE    kCCBlockSizeAES
#define FBENCRYPT_KEY_SIZE      kCCKeySizeAES

@interface Encrypt : NSObject

//-----------------
// API (raw data)
//-----------------

- (NSString *)de_crypt_url:(NSString*)data;
- (NSString *)encrypt_Data:(NSString*)data;
- (NSDictionary *)de_crypt_Data:(NSData*)data;

- (NSString *)encrypt_IP:(NSString*)IP;
- (NSString *)externalIPAddress;

//+ (NSData*)generateIv;
//+ (NSData*)encryptData:(NSData*)data key:(NSData*)key iv:(NSData*)iv;
//+ (NSData*)decryptData:(NSData*)data key:(NSData*)key iv:(NSData*)iv;
//
////-----------------
//// API (base64)
////-----------------
//// the return value of encrypteMessage: and 'encryptedMessage' are encoded with base64.
////
//
//+ (NSString*)encryptBase64String:(NSString*)string keyString:(NSString*)keyString separateLines:(BOOL)separateLines;
//+ (NSString*)decryptBase64String:(NSString*)encryptedBase64String keyString:(NSString*)keyString;
//
////-----------------
//// API (utilities)
////-----------------
//
//+ (NSString*)hexStringForData:(NSData*)data;
//+ (NSData*)dataForHexString:(NSString*)hexString;

@end
