//
//  Trnsfer_Other_Acct_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 17/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Trnsfer_Other_Acct_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Get_Pay_Detail_Model.h"
#import "AFNetworking.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Trnsfer_Other_Acct_VC ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    MBProgressHUD* hud;
    NSArray* split;
    NSString* str_acct_name_frm;
    NSString* str_acct_id_frm;
    NSString* str_acct_name_to;
    NSString* str_acct_id_to;
    NSDictionary* dic;
    UIAlertController *alert;
    NSMutableArray* a;
    NSMutableArray* arr_other_pay_list;
    
    NSString* str_acct_name_frm_O;
    NSString* str_acct_id_frm_O;
    NSString* str_acct_no_frm_O;
    NSString* str_acct_name_to_O;
    NSString* str_acct_no_to_O;
    NSString* str_acct_id_to_O;
    
    NSString* branch_code;
    NSString* bank_name;
    NSString* bank_imd;
    NSString* access_key;
    NSString* base_ccy;
    NSString* email;
    
    //    NSString* branch_code_to;
    int branch_code_to;
    NSString* bank_name_to;
    NSString* bank_imd_to;
    NSString* access_key_to,* access_key_to_1;
    
    NSString* ran;
    NSString* privalages;
    NSString* acct_type;
    NSString* chk_acct_type;
    NSMutableArray* purpose;
    NSDictionary* dic_purpose;
    NSMutableArray* arr_select_purpose;
    UIImage *btnImage;
    NSString* btn_flag_email;
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    NSMutableArray* search;
    
    NSMutableArray* arr_relationship;
    
    NSString* purpose_chck;
    NSString* relation_chck;
    NSString* search_flag;
    NSArray *table_IndexTitles;
    UILabel* label;
    UIImageView* img;
    NSString* chck_review;
    
    NSString* str_IBFT_Relation,*str_IBFT_POT,*intt_purpose_code;
    NSString* ibft_relation_chck;
    
    NSNumber *number;
    NSString* chk_ssl;
    NSNumberFormatter *numberFormatter;
    NSArray*   split_from;
    NSString *numberString;
    NSDecimalNumber *decimalNumber;
    NSArray* split1_ibft;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString * vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Trnsfer_Other_Acct_VC
@synthesize txt_acct_from;
@synthesize txt_acct_to;
@synthesize txt_amnt_with;
@synthesize txt_comment_with;
@synthesize txt_amnt_without;
@synthesize txt_comment_without;
@synthesize lbl_bank_name;
@synthesize txt_purpose;
@synthesize txt_t_pin_with;
@synthesize txt_t_pin_without;
@synthesize lbl_t_pin_with;
@synthesize lbl_t_pin_without;
@synthesize lbl_email_with;
@synthesize lbl_email_without;
//@synthesize lbl_t_pin;
//@synthesize lbl_email;
//@synthesize txt_purpose;
@synthesize transitionController;
//Relationship
@synthesize txt_relationship_amount;
@synthesize txt_relationship_comment;
@synthesize txt_relationship_T_pin;
@synthesize lbl_relationship_T_pin;
@synthesize lbl_balance;
@synthesize lbl_acct_frm;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass=[GlobalStaticClass getInstance];
    arr_select_purpose=[[NSMutableArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    search=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    encrypt = [[Encrypt alloc] init];
    ssl_count = @"0";
    
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    intt_purpose_code = @"";
    table_Purpose_relationship.hidden=YES;
    table_relationship.hidden=YES;
    vw_relationship.hidden=YES;
    relationship_view.hidden=YES;
    vw_relationship_purpose.hidden=YES;
    search_flag=@"0";
    gblclass.ft_tax = @"";
    gblclass.ft_msg = @"";
    
    branch_code_to=0;
    vw_down_chck = @"";
    //NSLog(@"%@",gblclass.arr_payee_list);
    
    vw_from.hidden=YES;
    vw_to.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    vw_purpose.hidden=YES;
    table_purpose.hidden=YES;
    lbl_t_pin_with.hidden=YES;
    txt_t_pin_with.hidden=YES;
    lbl_t_pin_without.hidden=YES;
    txt_t_pin_without.hidden=YES;
    txt_acct_to.text=@"";
    relationship_view.hidden=YES;
    txt_email.hidden=YES;
    
    txt_acct_to.delegate=self;
    txt_amnt_with.text=@"";
    txt_amnt_without.text=@"";
    txt_comment_with.text=@"";
    txt_comment_without.text=@"";
    txt_t_pin_without.text=@"";
    txt_t_pin_with.text=@"";
    
    
    txt_comment_with.delegate=self;
    txt_amnt_with.delegate=self;
    txt_comment_without.delegate=self;
    txt_amnt_without.delegate=self;
    txt_t_pin_with.delegate=self;
    txt_t_pin_without.delegate=self;
    
    txt_relationship_amount.delegate=self;
    txt_relationship_comment.delegate=self;
    txt_relationship_T_pin.delegate=self;
    lbl_relationship_T_pin.hidden=YES;
    txt_relationship_T_pin.hidden=YES;
//    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_email.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_comment_with.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_comment_without.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_relationship_comment.autocorrectionType = UITextAutocorrectionTypeNo;
    
    btnImage = [UIImage imageNamed:@"uncheck.png"];
    [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
    btn_flag_email=@"0";
    //  btn_check_mail.hidden=YES;
    txt_email_with.hidden=YES;
    
    
    
    if ([gblclass.chck_transfer_other_acct_frm isEqualToString:@"1"])
    {
        //NSLog(@"%@",gblclass.chck_arr_transfer_other_acct_frm);
        
        txt_acct_from.text=[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:0];
        str_acct_name_frm=[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:0];
        str_acct_no_frm_O=[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:1];
        str_acct_id_frm_O=[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:2];
        lbl_acct_frm.text=[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:1];
        lbl_balance.text = [gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:3];
    }
    else
    {
        
        if ([gblclass.is_debit_lock isEqualToString:@"0"])
        {
            txt_acct_from.text=gblclass.is_default_acct_id_name;
            str_acct_name_frm=gblclass.is_default_acct_id_name;
            str_acct_no_frm_O=gblclass.is_default_acct_no;
            str_acct_id_frm_O=gblclass.is_default_acct_id;
            lbl_acct_frm.text=gblclass.is_default_acct_no;
            
             btn_submit.enabled = YES;
        }
        else
        {
            txt_acct_from.text = @"-";
            str_acct_name_frm = @"-";
            str_acct_no_frm_O = @"-";
            str_acct_id_frm_O = @"-";
            lbl_acct_frm.text = @"-";
            
            btn_submit.enabled = NO;
        }
    }
    
    // [btn_submit setBackgroundColor: [UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    
    
    purpose_with_view.hidden=YES;
    
    [hud showAnimated:YES];
    
    
    
    //    [self Get_Pay_Details:@""];
    
    
    
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    //        [self SSL_Call];
    //    }
    
    
    
    
    //**   [self load_pay_list:@""];
    
    
    txt_Utility_bill_name.text= gblclass.pay_combobill_name;
    
    gblclass.arr_review_withIn_myAcct=[[NSMutableArray alloc] init];
    arr_bill_names=[[NSMutableArray alloc] init];
    
    
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//
//
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:vc animated:YES completion:nil];
//
//
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//
//        [cc1 timme_invaletedd];
//
//    };
    
    
    //WITH IMAGE ::
    
    //    UILabel * Label = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 200, 60)];
    //    [self.view addSubview:Label];
    
//    NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    if ([gblclass.arr_transfer_within_acct count]>0)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0 || [[split objectAtIndex:6] isEqualToString:@""])
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        NSString *mystring =[NSString stringWithFormat:@"%@",balc];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        [mutableAttriStr1 appendAttributedString:imageStr1];
        
        //   lbl_balance.attributedText = mutableAttriStr1;
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[numberFormatter stringFromNumber:number]];
        
        if ([gblclass.chck_transfer_other_acct_frm isEqualToString:@"1"])
            
        {
            
            NSString *mystring =[NSString stringWithFormat:@"%@",[gblclass.chck_arr_transfer_other_acct_frm objectAtIndex:3]];
            number = [NSDecimalNumber decimalNumberWithString:mystring];
            NSNumberFormatter *formatter = [NSNumberFormatter new];
            [formatter setMinimumFractionDigits:2];
            [formatter setMaximumFractionDigits:2];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            
            NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
            NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
            [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
            
            [mutableAttriStr1 appendAttributedString:imageStr1];
            //     lbl_balance.attributedText = mutableAttriStr1;
            
            lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];        }
        
    }
    else
    {
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        
        NSString *mystring =[NSString stringWithFormat:@"%@",@"0"];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[numberFormatter stringFromNumber:number]];
        
    }
    
    txt_amnt_without.placeholder = [NSString stringWithFormat:@"Amount in %@",gblclass.base_currency];
    txt_relationship_amount.placeholder = [NSString stringWithFormat:@"Amount in %@",gblclass.base_currency];
    
    vw_table.hidden=YES;
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    table_IndexTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
}


- (void)viewDidAppear:(BOOL)animated
{
    //NSLog(@"back");
    // txt_amnt_without.text=gblclass.indexxx;
    
    gblclass.ft_tax = @"";
    gblclass.ft_msg = @"";
    
    if ([gblclass.arr_payee_list count]>0)
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        //NSLog(@"%@",gblclass.arr_payee_list);
        
        txt_acct_to.text=[gblclass.arr_payee_list objectAtIndex:0];
        
        txt_acct_to.text=[gblclass.arr_payee_list objectAtIndex:0];
        str_acct_name_to=[gblclass.arr_payee_list objectAtIndex:0];
        str_acct_no_to_O=[gblclass.arr_payee_list objectAtIndex:2];
        str_acct_id_to=[gblclass.arr_payee_list objectAtIndex:1];
        lbl_bank_name.text=[gblclass.arr_payee_list objectAtIndex:3];
        access_key_to_1=[gblclass.arr_payee_list objectAtIndex:4];
        branch_code_to=[[gblclass.arr_payee_list objectAtIndex:5] intValue];
        
        
        //        if (branch_code_to == nil || branch_code_to == (id)[NSNull null] || [branch_code_to isEqualToString:@"<null>"])
        //        {
        
        //        }
        
        
        chk_ssl=@"load_payee_list";
        
        [self SSL_Call];
        
        //      [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        
        // [self load_pay_list:@""];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    //GetLimits
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//       //count of section
//
////    if (tableView==table_to)
////    {
//        return [table_IndexTitles count];
////    }
////    else
////    {
////         return 1;
////    }
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];
    }
    else if(tableView == table_to)
    {
        //        if ([search_flag isEqual:@"1"])
        //        {
        //            return [search count];
        //        }
        //        else
        //        {
        
        return [arr_other_pay_list count];
        
        
        //        }
        
    }
    //    else if(tableView==table_purpose)
    //    {
    //        return [arr_select_purpose count];
    //    }
    //    else
    //    {
    //        return [arr_bill_names count];
    //    }
    else if (tableView==table_purpose)
    {
        return [arr_select_purpose count];
    }
    else if(tableView==table_Purpose_relationship)
    {
        return [arr_select_purpose count];
    }
    else if(tableView==table_relationship)
    {
        return[arr_relationship count];
    }
    else
    {
        return 0;
    }
    
    
    //count number of row from counting array hear cataGorry is An Array
}


//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    //  return animalSectionTitles;
////    if (tableView==table_to)
////    {
//        return gblclass.arr_transfer_within_acct;  //table_IndexTitles
////    }
////    else
////    {
////        return 0;
////    }
//
//}

//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
//{
//    return [table_IndexTitles indexOfObject:title];
//}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%@",tableView);
    
    if (tableView==table_from)
    {
        split_from = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_from objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split_from objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        numberString = [NSString stringWithFormat:@"%@", [split_from objectAtIndex:6]];
        decimalNumber = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [numberFormatter stringFromNumber:decimalNumber];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqual:@"1"])
        {
            NSArray*   split_to = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [split_to objectAtIndex:1];
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        else
        {
            NSArray*   split_to = [[arr_other_pay_list objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            //Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_to objectAtIndex:0];
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            
            //Acct. No ::
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_to objectAtIndex:3];
            label.textColor=[UIColor whiteColor];
            label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            
            img=(UIImageView*)[cell viewWithTag:1];
            img.image=[UIImage imageNamed:@"Non.png"];
            [cell.contentView addSubview:img];
            
        }
    }
    else if(tableView==table_purpose)
    {
        split1_ibft = [[arr_select_purpose objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        cell.textLabel.text = [split1_ibft objectAtIndex:0];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    else if (tableView==table_Purpose_relationship)
    {
        
        NSArray* split_purpose = [[arr_select_purpose objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        cell.textLabel.text = [split_purpose objectAtIndex:0];//[arr_select_purpose objectAtIndex:indexPath.row];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    else if(tableView==table_relationship) //  table_Purpose_relationship
    {
        
        NSArray* split_relationship = [[arr_relationship objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        cell.textLabel.text =[split_relationship objectAtIndex:0]; //[arr_relationship objectAtIndex:indexPath.row];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    else
    {
        cell.textLabel.text = [arr_bill_names objectAtIndex:indexPath.row];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    table_to.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_Purpose_relationship.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_relationship.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_purpose.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self checkinternet];
    if (netAvailable)
    {
        
        [table_from reloadData];
        if (tableView==table_from)
        {
            
            gblclass.chck_arr_transfer_other_acct_frm=[[NSMutableArray alloc] init];
            [gblclass.chck_arr_transfer_other_acct_frm removeAllObjects];
            
            gblclass.chck_transfer_other_acct_frm=@"1";
            
            gblclass.indexxx=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            
            split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            [gblclass.chck_arr_transfer_other_acct_frm addObject:[split objectAtIndex:0]];
            [gblclass.chck_arr_transfer_other_acct_frm addObject:[split objectAtIndex:1]];
            [gblclass.chck_arr_transfer_other_acct_frm addObject:[split objectAtIndex:2]];
            [gblclass.chck_arr_transfer_other_acct_frm addObject:[split objectAtIndex:8]];
            
            
            txt_acct_from.text=[split objectAtIndex:0];
            str_acct_name_frm=[split objectAtIndex:0];
            str_acct_no_frm_O=[split objectAtIndex:1];
            str_acct_id_frm_O=[split objectAtIndex:2];
            lbl_acct_frm.text=[split objectAtIndex:1];
            
            
            NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
            attach1.image = [UIImage imageNamed:@"Pkr.png"];
            attach1.bounds = CGRectMake(10, 8, 20, 10);
            NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
            
            NSString* balc;
            
            if ([split objectAtIndex:6]==0)
            {
                balc=@"0";
            }
            else
            {
                balc=[split objectAtIndex:6];
            }
            
            NSString *mystring =[NSString stringWithFormat:@"%@",balc];
            number = [NSDecimalNumber decimalNumberWithString:mystring];
            NSNumberFormatter *formatter = [NSNumberFormatter new];
            [formatter setMinimumFractionDigits:2];
            [formatter setMaximumFractionDigits:2];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            
            NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
            NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
            [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
            
            [mutableAttriStr1 appendAttributedString:imageStr1];
            //     lbl_balance.attributedText = mutableAttriStr1;
            
            lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[numberFormatter stringFromNumber:number]];
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
            myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
            
            table_from.hidden=YES;
            vw_from.hidden=YES;
            vw_table.hidden=YES;
            
        }
        else if(tableView==table_to)
        {
            
            if ([search_flag isEqual:@"1"])
            {
                NSArray*   split_to = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                txt_acct_to.text=[split_to objectAtIndex:1];
                str_acct_name_to=[split_to objectAtIndex:1];
                str_acct_no_to_O=[split_to objectAtIndex:3];
                str_acct_id_to=[split_to objectAtIndex:1];
                lbl_bank_name.text=[split_to objectAtIndex:4];
                
                table_to.hidden=YES;
                vw_to.hidden=YES;
                vw_table.hidden=YES;
                
                [hud showAnimated:YES];
                [self load_pay_list_to:@""];
                
                search_flag=@"0";
                [table_to reloadData];
            }
            else
            {
                NSArray*   split_to = [[arr_other_pay_list objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                txt_acct_to.text=[split_to objectAtIndex:0];
                str_acct_name_to=[split_to objectAtIndex:1];
                str_acct_no_to_O=[split_to objectAtIndex:3];
                str_acct_id_to=[split_to objectAtIndex:1];
                lbl_bank_name.text=[split_to objectAtIndex:4];
                
                table_to.hidden=YES;
                vw_to.hidden=YES;
                
                vw_table.hidden=YES;
                
                [hud showAnimated:YES];
                [self load_pay_list_to:[split_to objectAtIndex:1]];
                
            }
        }
        else if(tableView==table_purpose)
        {
            // NSArray*   split_to = [[arr_other_pay_list objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            //**    str_IBFT_Relation=[arr_select_purpose objectAtIndex:indexPath.row];
            //**      str_IBFT_POT=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            
            str_IBFT_Relation=ibft_relation_chck;
            
            //txt_purpose.text=[arr_select_purpose objectAtIndex:indexPath.row];
            
            NSArray* split1_ibft = [[arr_select_purpose objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            txt_purpose.text=[split1_ibft objectAtIndex:0];
            str_IBFT_POT=[split1_ibft objectAtIndex:1];
            
            table_purpose.hidden=YES;
            vw_purpose.hidden=YES;
            vw_table.hidden=YES;
            
//            [self Get_Sub_Category:@""];
        
        }
        else if (tableView==table_Purpose_relationship)
        {
            
            NSArray* split_purpose = [[arr_select_purpose objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            _txt_relationship_purpose.text=[split_purpose objectAtIndex:0];//[arr_select_purpose objectAtIndex:indexPath.row];
            
            
            
            str_IBFT_POT=[split_purpose objectAtIndex:1]; //[arr_select_purpose objectAtIndex:indexPath.row];
            
            intt_purpose_code = [split_purpose objectAtIndex:2];
            
            table_Purpose_relationship.hidden=YES;
            vw_relationship_purpose.hidden=YES;
            vw_table.hidden=YES;
        }
        else if (tableView==table_relationship)
        {
             NSArray* split_relationship = [[arr_relationship objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            _txt_relationship.text=[split_relationship objectAtIndex:0];
            str_IBFT_Relation=[split_relationship objectAtIndex:1];
            
            table_relationship.hidden=YES;
            table_relationship.hidden=YES;
            vw_table.hidden=YES;
            
            
            [hud showAnimated:YES];
            [self Get_Sub_Category:@""];
        }
        
        else
        {
            _txt_relationship.text=[arr_relationship objectAtIndex:indexPath.row];
            
            table_relationship.hidden=YES;
            vw_relationship.hidden=YES;
            vw_table.hidden=YES;
            
            NSArray* split_bill;
            split_bill=[[NSMutableArray alloc] init];
            split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            gblclass.pay_combobill_name=[split_bill objectAtIndex:0];
            
            switch (indexPath.row)
            {
                case 0:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"]; //utility_bill   //bill_all
                    //    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                case 1:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_voucher"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 2:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"online_shopping"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 3:
                    
                    
                    
                    break;
                    
                case 4:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_within_acct"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 5:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_other_acct"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 6:
                    
                    //                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    //                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                    //    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                default: break;
                    
            }
        }
    }
    
    //    else
    //    {
    //        txt_Utility_bill_name.text=[arr_bill_names objectAtIndex:indexPath.row];
    //    }
    
}

-(IBAction)btn_combo_frm:(id)sender
{
    lbl_vw_heading.text = @"Select Your Account";
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    
    txt_amnt_with.text=@"";
    txt_amnt_without.text=@"";
    txt_comment_with.text=@"";
    txt_comment_without.text=@"";
    txt_t_pin_without.text=@"";
    txt_t_pin_with.text=@"";
    
    if (table_from.isHidden==YES)
    {
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        table_purpose.hidden=YES;
        vw_purpose.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
}

-(IBAction)btn_combo_to:(id)sender
{
    [self.view endEditing:YES];
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee_list"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
    //    [table_to reloadData];
    //    if (table_to.isHidden==YES)
    //    {
    //        vw_to.hidden=NO;
    //        table_to.hidden=NO;
    //        table_from.hidden=YES;
    //        vw_from.hidden=YES;
    //        table_purpose.hidden=YES;
    //        vw_purpose.hidden=YES;
    //        [table_to bringSubviewToFront:self.view];
    //        vw_table.hidden=NO;
    //    }
    //    else
    //    {
    //        search_flag=@"0";
    //        table_to.hidden=YES;
    //        vw_to.hidden=YES;
    //        [table_to reloadData];
    //        vw_table.hidden=YES;
    //    }
    
}

-(void) Get_Pay_Details:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    gblclass.arr_get_pay_details=[[NSMutableArray alloc] init];
    arr_other_pay_list=[[NSMutableArray alloc] init];
    a=[[NSMutableArray alloc] init];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],@"abcd", nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetPayDetails" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              gblclass.arr_other_pay_list=[[NSMutableArray alloc] init];
              
              [gblclass.arr_other_pay_list removeAllObjects];
              
              gblclass.arr_get_pay_details=
              [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbPayAnyOneList"];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
              
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  NSUInteger *set;
                  for (dic in gblclass.arr_get_pay_details)
                  {
                      
                      set = [gblclass.arr_get_pay_details indexOfObject:dic];
                      
                      
                      
                      
                      
                      NSString* ACCOUNT_NAME=[dic objectForKey:@"ACCOUNT_NAME"];
                      //                      if (ACCOUNT_NAME.length==0 || [ACCOUNT_NAME isEqualToString:@" "] || [ACCOUNT_NAME isEqualToString:nil])
                      //                      {
                      //                          ACCOUNT_NAME=@"N/A";
                      //                          [a addObject:ACCOUNT_NAME];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:ACCOUNT_NAME];
                      //                      }
                      
                      
                      NSString* USER_ACCOUNT_ID=[dic objectForKey:@"USER_ACCOUNT_ID"];
                      
                      
                      //                      if (USER_ACCOUNT_ID==@"")
                      //                      {
                      //                          USER_ACCOUNT_ID=@"N/A";
                      //                          [a addObject:USER_ACCOUNT_ID];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:USER_ACCOUNT_ID];
                      //                      }
                      
                      
                      
                      NSString* TT_ACCESS_KEY=[dic objectForKey:@"TT_ACCESS_KEY"];
                      //                      if (TT_ACCESS_KEY.length==0 || [TT_ACCESS_KEY isEqualToString:@" "] || [TT_ACCESS_KEY isEqualToString:nil])
                      //                      {
                      //                          TT_ACCESS_KEY=@"N/A";
                      //                          [a addObject:TT_ACCESS_KEY];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:TT_ACCESS_KEY];
                      //                      }
                      
                      NSString* ACCOUNT_NO=[dic objectForKey:@"ACCOUNT_NO"];
                      //                      if ([ACCOUNT_NO isEqualToString:@""])
                      //                      {
                      //                          ACCOUNT_NO=@"N/A";
                      //                          [a addObject:ACCOUNT_NO];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:ACCOUNT_NO];
                      //                      }
                      
                      //BANK_CODE
                      
                      NSString* BANK_CODE=[dic objectForKey:@"BANK_CODE"];
                      //                      if ([ACCOUNT_NO isEqualToString:@""])
                      //                      {
                      //                          ACCOUNT_NO=@"N/A";
                      //                          [a addObject:ACCOUNT_NO];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:BANK_CODE];
                      //                      }
                      
                      
                      NSString* BRANCH_CODE=[dic objectForKey:@"BRANCH_CODE"];
                      //                      if ([BRANCH_CODE isEqual:@""])
                      //                      {
                      //                          BRANCH_CODE=@"N/A";
                      //                          [a addObject:BRANCH_CODE];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:BRANCH_CODE];
                      //                      }
                      
                      NSString* BANK_IMD=[dic objectForKey:@"BANK_IMD"];
                      //                      if ([BANK_IMD isEqual:@""])
                      //                      {
                      //                          BANK_IMD=@"N/A";
                      //                          [a addObject:BANK_IMD];
                      //                      }
                      //                      else
                      //                      {
                      [a addObject:BANK_IMD];
                      //                      }
                      
                      NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      [arr_other_pay_list addObject:bbb];
                      [gblclass.arr_other_pay_list addObject:bbb];
                      
                      //NSLog(@"%@", bbb);
                      [a removeAllObjects];
                      
                  }
                  
                  [table_to reloadData];
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  
                  return ;
              }
              else
              {
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert2 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
    if (buttonIndex == 0)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
 
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}




-(void) Get_Sub_Category:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"3P"],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:_txt_relationship.text], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strAccessKey",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strPurposeCode", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetSubCategory" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* ab;
                  ab = [[NSMutableArray alloc] init];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                  
                  purpose=[[NSMutableArray alloc] init];
                  purpose  = [(NSDictionary *)[dic2 objectForKey:@"dsSubCatg"] objectForKey:@"Table"];
                  
                  [arr_select_purpose removeAllObjects];
                  for (dic2 in purpose)
                  {
                      
                      NSString* select_purpose;
                      NSString* BFT_POT_ID;
                      NSString* purpose_code;
                      
                      if ([gblclass.arr_payee_list count] >0)
                      {
                          if ([[gblclass.arr_payee_list objectAtIndex:6] isEqualToString:@"Transfer Funds to CNIC"])
                          {
                              select_purpose = [dic2 objectForKey:@"CNIC_POT_DESC"];
                              BFT_POT_ID=[dic2 objectForKey:@"CNIC_POT_ID"];
                          }
                          else
                          {
                              select_purpose = [dic2 objectForKey:@"IBFT_INT_PUR_CODE_DESC"];
                              BFT_POT_ID=[dic2 objectForKey:@"IBFT_INT_PCSC_ID"];
                              purpose_code =[dic2 objectForKey:@"IBFT_INT_PURPOSE_CODES"];
                          }
                      }
                      
                      
                      
                      
                      if ([gblclass.arr_payee_list count] >0)
                      {
                          if ([[gblclass.arr_payee_list objectAtIndex:6] isEqualToString:@"Transfer Funds to CNIC"])
                          {
                              if ( select_purpose == (NSString *)[NSNull null])
                              {
                                  select_purpose=@"0";
                                  [ab addObject:select_purpose];
                              }
                              else
                              {
                                  [ab addObject:select_purpose];
                              }
                              
                              
                              if ( BFT_POT_ID == (NSString *)[NSNull null])
                              {
                                  BFT_POT_ID=@"0";
                                  [ab addObject:BFT_POT_ID];
                              }
                              else
                              {
                                  [ab addObject:BFT_POT_ID];
                              }
                              
                          }
                          else
                          {
                              if ( select_purpose == (NSString *)[NSNull null])
                              {
                                  select_purpose=@"0";
                                  [ab addObject:select_purpose];
                              }
                              else
                              {
                                  [ab addObject:select_purpose];
                              }
                              
                              
                              if ( BFT_POT_ID == (NSString *)[NSNull null])
                              {
                                  BFT_POT_ID=@"0";
                                  [ab addObject:BFT_POT_ID];
                              }
                              else
                              {
                                  [ab addObject:BFT_POT_ID];
                              }
                              
                              if ( purpose_code == (NSString *)[NSNull null])
                              {
                                  purpose_code=@"0";
                                  [ab addObject:purpose_code];
                              }
                              else
                              {
                                  [ab addObject:purpose_code];
                              }
                              
                          }
                      }
                      
//                      if ( select_purpose == (NSString *)[NSNull null])
//                      {
//                          select_purpose=@"0";
//                          [ab addObject:select_purpose];
//                      }
//                      else
//                      {
//                          [ab addObject:select_purpose];
//                      }
//
//
//                      if ( BFT_POT_ID == (NSString *)[NSNull null])
//                      {
//                          BFT_POT_ID=@"0";
//                          [ab addObject:BFT_POT_ID];
//                      }
//                      else
//                      {
//                          [ab addObject:BFT_POT_ID];
//                      }
//
//                      if ( purpose_code == (NSString *)[NSNull null])
//                      {
//                          purpose_code=@"0";
//                          [ab addObject:purpose_code];
//                      }
//                      else
//                      {
//                          [ab addObject:purpose_code];
//                      }
                      
                      NSString *bbb = [ab componentsJoinedByString:@"|"];
                      [arr_select_purpose addObject:bbb];
                      
                      [ab removeAllObjects];
                      bbb=@"";
                      
                      //NSLog(@"%@",[dic objectForKey:@"IBFT_POT_DESC"]);
                      //IBFT_POT_ID
                      //IS_ACTIVE
                      
                    //  purpose_with_view.hidden=NO;
                      
                      purpose_without_view.hidden=YES;
                      purpose_chck=@"1";
                      relation_chck=@"0";
                  }
                      
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
            
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
//        NSLog(@"%@",exception.reason);
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
    }
}





-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) Pay_Account:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSArray* split1 = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    base_ccy=[split1 objectAtIndex:0];
    
    
    
    //gblclass.arr_user_pw_table2
    
    //    NSArray* split1 = [[gblclass.arr_user_pw_table2 objectAtIndex:0] componentsSeparatedByString: @"|"];
    //@"bashir.hasan@ubl.com.pk";//[split1 objectAtIndex:1];
    
    NSString* amnt,*comments;
    
    if (purpose_with_view.isHidden==YES)
    {
        amnt=txt_amnt_without.text;
        comments=txt_comment_without.text;
        email=txt_email_without.text;
    }
    else
    {
        amnt=txt_amnt_with.text;
        comments=txt_comment_with.text;
        email=txt_email_with.text;
    }
    
    
    if ([btn_flag_email isEqualToString:@"0"])
    {
        email=@"";
    }
    else
    {
        email=txt_email_with.text;
    }
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,
                                                                   gblclass.M3sessionid,
                                                                   [GlobalStaticClass getPublicIp],
                                                                   str_acct_id_frm_O,
                                                                   str_acct_name_frm,
                                                                   str_acct_no_frm_O,
                                                                   amnt,
                                                                   gblclass.base_currency,
                                                                   str_acct_id_to,
                                                                   str_acct_name_to,
                                                                   str_acct_no_to_O,
                                                                   bank_name,
                                                                   @"",
                                                                   access_key,email,
                                                                   @"",
                                                                   @"",
                                                                   @"1111",
                                                                   @"588974",
                                                                   comments,
                                                                   @"",
                                                                   str_acct_no_to_O,
                                                                   bank_name,
                                                                   uuid, nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"payAnyOneAccountID",
                                @"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",
                                @"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",@"strEmail",
                                @"strIBFTRelation",@"strIBFTPOT",@"strtxtPin",@"payAnyOneFromBankImd",@"strTxtComments",@"payAnyOneToBranchCode",
                                @"payAnyOneToAccountNo",@"strBankCode",@"strGuid", nil]];
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,@"1234",[GlobalStaticClass getPublicIp],str_acct_id_frm,str_acct_name_frm,str_acct_no_frm_O,txt_amnt.text,base_ccy,str_acct_id_to,str_acct_name_to,str_acct_no_to_O,bank_name,@"",access_key,email,@"",@"",txt_t_pin.text,bank_imd,txt_comment.text,@"",str_acct_no_to_O,@"UBL",uuid, nil]
    //                                                          forKeys:
    //                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"payAnyOneAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"strtxtAmount",@"StrCCY",@"payAnyOneToAccountID",@"lblPayTo",@"lblVAccNo",@"lblBank",@"strAccType",@"ttAccessKey",@"strEmail",@"strIBFTRelation",@"strIBFTPOT",@"strtxtPin",@"payAnyOneFromBankImd",@"strTxtComments",@"payAnyOneToBranchCode",@"payAnyOneToAccountNo",@"strBankCode",@"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              //NSLog(@"Status %@",[dic objectForKey:@"lblTransactionType"]);
              //NSLog(@"ReturnMessage %@",[dic objectForKey:@"strReturnMessage"]);
              
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Successfull" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
                  UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Successfull"
                                                                   message:[dic objectForKey:@"strReturnMessage"]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert1 show];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==-1)
              {
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              else
              {
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void)load_pay_list:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //gblclass.arr_payee_list
    //gblclass.is_default_acct_id
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],gblclass.arr_payee_list, nil] forKeys:
                               [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"payAnyOneAccountID", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"LoadPayeeList" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSMutableArray* arr_load_payee_list;
              
              arr_load_payee_list=[[NSMutableArray alloc] init];
              arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              
              NSUInteger *set;
              for (dic in arr_load_payee_list)
              {
                  set = [arr_load_payee_list indexOfObject:dic];
                  
                  branch_code=[dic objectForKey:@"BRANCH_CODE"];
                  access_key=[dic objectForKey:@"TT_NAME"];
                  bank_name=[dic objectForKey:@"BANK_NAME"];
                  bank_imd=[dic objectForKey:@"BANK_IMD"];
                  //lbl_email.text=[dic objectForKey:@"EMAIL"];
                  
                  //                 NSString* str_email=[dic objectForKey:@"EMAIL"];
                  
                  //                  if (str_email==[NSNull null])
                  //                  {
                  //                      lbl_email_with.text=@"";
                  //                      btn_check_mail.hidden=YES;
                  //                  }
                  //                  else
                  //                  {
                  //                      btn_check_mail.hidden=NO;
                  //
                  //                      lbl_email_with.text=[dic objectForKey:@"EMAIL"];
                  //                  }
                  //
                  //NSLog(@"%@",[dic objectForKey:@"EMAIL"]);
              }
              
              
              
              [hud hideAnimated:YES];
              
              //
              //              if ([[dic objectForKey:@"Response"] integerValue]==0)
              //              {
              //                  alert  = [UIAlertController alertControllerWithTitle:@"Successfull" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
              //
              //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //                  [alert addAction:ok];
              //
              //                  [self presentViewController:alert animated:YES completion:nil];
              //
              //              }
              //              else
              //              {
              //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
              //
              //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //                  [alert addAction:ok];
              //
              //                  [self presentViewController:alert animated:YES completion:nil];
              //              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}

-(void)load_pay_list_to:(NSString *)strIndustry
{
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
 
    @try {
    
    //str_acct_id_to
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:strIndustry],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"payAnyOneAccountID",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"LoadPayeeList" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_load_payee_list_to;
                  NSMutableArray* arr_relationship_load,*arr_msg;
                  arr_relationship=[[NSMutableArray alloc] init];
                  arr_msg=[[NSMutableArray alloc] init];
                  
                  arr_relationship_load=[[NSMutableArray alloc] init];
                  arr_load_payee_list_to=[[NSMutableArray alloc] init];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      arr_load_payee_list_to =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
                      
                      //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                      
                      purpose=[[NSMutableArray alloc] init];
                    //**  purpose  = [(NSDictionary *)[dic objectForKey:@"outdsIBFTPOT"] objectForKey:@"Table1"];
                      
                      //outdsIBFTPOT
                     // arr_relationship_load  = [(NSDictionary *)[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
                      
                      arr_relationship_load  = [(NSDictionary *)[dic objectForKey:@"outdsIBFTPOT"] objectForKey:@"Table1"];
                      
                      arr_msg = [(NSDictionary *)[dic objectForKey:@"outdsIBFTPOT"] objectForKey:@"Table3"];
                      
                      
                      ibft_relation_chck=[dic objectForKey:@"outStrIBFTRelation"];
                      if ([[dic objectForKey:@"Response"] integerValue]==0)
                      {
                          
                          //NSLog(@"%@",[dic_purpose objectForKey:@"IBFT_POT_DESC"]);
                          
                          lbl_bank_name.text=@"";
                          
                          purpose_with_view.hidden=YES;
                          //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                          
                          
                          for (dic in arr_msg)
                          {
                              lbl_note.text = [dic objectForKey:@"NOTE"];
                              lbl_msg.text = [dic objectForKey:@"MSG"];
                              
                              [lbl_note sizeToFit];
                              [lbl_msg sizeToFit];
                          }
                          
                          NSMutableArray* ab1;
                          ab1=[[NSMutableArray alloc] init];
                          
                          
                          for (dic in arr_relationship_load) //outdsIBFTRelation
                          {
                              
                           //   NSString* select_relationship=[dic objectForKey:@"IBFT_RELATION_DESC"];
                              // [ab addObject:select_purpose];
                              
//                              [arr_relationship addObject:select_relationship];
                              
                              //NSLog(@"%@",[dic objectForKey:@"IBFT_POT_DESC"]);
                              
                              NSString* select_purpose;
                              NSString* BFT_POT_ID;
                              
                              if ([gblclass.arr_payee_list count] >0)
                              {
                                  if ([[gblclass.arr_payee_list objectAtIndex:6] isEqualToString:@"Transfer Funds to CNIC"])
                                  {
                                      select_purpose = [dic objectForKey:@"CNIC_POT_DESC"];
                                      BFT_POT_ID=[dic objectForKey:@"CNIC_POT_ID"];
                                  }
                                  else
                                  {
                                      select_purpose = [dic objectForKey:@"IBFT_INT_PUR_CODE_DESC"];
                                      BFT_POT_ID=[dic objectForKey:@"IBFT_INT_PUR_CODE_ID"];
                                  }
                              }
                              
                              if ( select_purpose == (NSString *)[NSNull null])
                              {
                                  select_purpose=@"0";
                                  [ab1 addObject:select_purpose];
                              }
                              else
                              {
                                  [ab1 addObject:select_purpose];
                              }
                              
                              
                              if ( BFT_POT_ID == (NSString *)[NSNull null])
                              {
                                  BFT_POT_ID=@"0";
                                  [ab1 addObject:BFT_POT_ID];
                              }
                              else
                              {
                                  [ab1 addObject:BFT_POT_ID];
                              }
                              
                              
                              NSString *bbb1 = [ab1 componentsJoinedByString:@"|"];
                             // [arr_select_purpose addObject:bbb1];
                              [arr_relationship addObject:bbb1];
                              
                              [ab1 removeAllObjects];
                              bbb1=@"";
                              
                              
                              
                              // vw_relationship.hidden=NO;
                              vw_purpose.hidden=YES;
                              //chck_review=@"vw_purpose";
                          }
                          
                          purpose_with_view.hidden=YES;
                          //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                          
                          
                          for (dic in arr_load_payee_list_to)
                          {
                              
                              //  branch_code_to=[dic objectForKey:@"BRANCH_CODE"];
                              access_key_to=[dic objectForKey:@"TT_NAME"];
                              bank_name_to=[dic objectForKey:@"BANK_NAME"];
                              bank_imd_to=[dic objectForKey:@"BANK_IMD"];
                              lbl_bank_name.text=bank_name_to;
                              NSString* str_email=[dic objectForKey:@"EMAIL"];
                              
                              if (str_email==(NSString *)[NSNull null])
                              {
                                  //saki 4 aprl 2019
//                                  lbl_email_with.text=@"";
//                                  txt_email_with.text=@"";
//                                  txt_email.text=@"";
                                  
                              }
                              else
                              {
                                  lbl_email_with.text=[dic objectForKey:@"EMAIL"];
                                  txt_email_with.text=[dic objectForKey:@"EMAIL"];
                                  txt_email.text=[dic objectForKey:@"EMAIL"];
                                  
                              }
                              
                              //                  if ([branch_code_to isEqualToString:@"<null>"])
                              //                  {
                              //branch_code_to=@"";
                              //                  }
                              
                              //NSLog(@"%@",[dic objectForKey:@"EMAIL"]);
                              
                              // chck_review=@"purpose_without_view";
                              purpose_with_view.hidden=YES;
                              purpose_without_view.hidden=NO;
                              
                          }
                          
                          
                          NSMutableArray* ab;
                          ab=[[NSMutableArray alloc] init];
                          
                          
                          [arr_select_purpose removeAllObjects];
                          NSUInteger *set1;
                          for (dic in purpose) //outdsIBFTPOT
                          {
                              set1 = [purpose indexOfObject:dic_purpose];
                              
                              NSString* select_purpose;
                              NSString* BFT_POT_ID;
                              
                              if ([gblclass.arr_payee_list count] >0)
                              {
                                  if ([[gblclass.arr_payee_list objectAtIndex:6] isEqualToString:@"Transfer Funds to CNIC"])
                                  {
                                      select_purpose = [dic objectForKey:@"CNIC_POT_DESC"];
                                      BFT_POT_ID=[dic objectForKey:@"CNIC_POT_ID"];
                                  }
                                  else
                                  {
                                      select_purpose = [dic objectForKey:@"IBFT_INT_PUR_CODE_DESC"];
                                      BFT_POT_ID=[dic objectForKey:@"IBFT_INT_PUR_CODE_ID"];
                                  }
                              }
                              
                              if ( select_purpose == (NSString *)[NSNull null])
                              {
                                  select_purpose=@"0";
                                  [ab addObject:select_purpose];
                              }
                              else
                              {
                                  [ab addObject:select_purpose];
                              }
                              
                              
                              if ( BFT_POT_ID == (NSString *)[NSNull null])
                              {
                                  BFT_POT_ID=@"0";
                                  [ab addObject:BFT_POT_ID];
                              }
                              else
                              {
                                  [ab addObject:BFT_POT_ID];
                              }
                              
                              
                              NSString *bbb = [ab componentsJoinedByString:@"|"];
                              [arr_select_purpose addObject:bbb];
                              
                              [ab removeAllObjects];
                              bbb=@"";
                              
                              //NSLog(@"%@",[dic objectForKey:@"IBFT_POT_DESC"]);
                              //IBFT_POT_ID
                              //IS_ACTIVE
                              
                              purpose_with_view.hidden=NO;
                              purpose_without_view.hidden=YES;
                              purpose_chck=@"1";
                              relation_chck=@"0";
                              
                          }
                          
                          
                          //NSLog(@"%@", arr_select_purpose);
                          
                          [hud hideAnimated:YES];
                          
                          
                          if ([arr_relationship count]==0)
                          {
                              
                              relationship_view.hidden=YES;
                              // purpose_with_view.hidden=YES;
                              
                          }
                          else
                          {
                              purpose_chck=@"0";
                              relation_chck=@"1";
                              relationship_view.hidden=NO;
                              purpose_with_view.hidden=YES;
                              // chck_review=@"relationship_view";
                          }
                          
                          
                          //                  if ([purpose count]>0)
                          //                  {
                          //                      purpose_without_view.hidden=YES;
                          //                      purpose_with_view.hidden=NO;
                          //                      relationship_view.hidden=YES;
                          //                  }
                          //                  else if([arr_relationship_load count]>0)
                          //                  {
                          //                      purpose_without_view.hidden=YES;
                          //                      purpose_with_view.hidden=YES;
                          //                      relationship_view.hidden=NO;
                          //                  }
                          //                  else
                          //                  {
                          //                      purpose_without_view.hidden=NO;
                          //                      purpose_with_view.hidden=YES;
                          //                      relationship_view.hidden=YES;
                          //                  }
                          
                      }
                      else
                      {
                          alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Record Not Found" preferredStyle:UIAlertControllerStyleAlert];
                          
                          UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                          [alert addAction:ok];
                          
                          [self presentViewController:alert animated:YES completion:nil];
                          [hud hideAnimated:YES];
                      }
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  if ([arr_relationship_load count]>0  && [arr_relationship_load count]>0)
                  {
                      chck_review=@"ibft relation & pot";
                      purpose_without_view.hidden=YES;
                      purpose_with_view.hidden=YES;
                      relationship_view.hidden=NO;
                  }
                  else if ([arr_relationship_load count]>0 && [purpose count]==0)
                  {
                      chck_review=@"ibft_relation";
                      purpose_without_view.hidden=YES;
                      purpose_with_view.hidden=NO;
                      relationship_view.hidden=YES;
                  }
                  else if ([purpose count]>0)    // && [arr_relationship_load count]==0
                  {
                      chck_review=@"ibft_pot";
                      purpose_without_view.hidden=YES;
                      purpose_with_view.hidden=NO;
                      relationship_view.hidden=YES;
                  }
                  else
                  {
                      chck_review=@"Purpose";
                      purpose_without_view.hidden=NO;
                      purpose_with_view.hidden=YES;
                      relationship_view.hidden=YES;
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:[error localizedDescription] :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        [self custom_alert:@"Retry"  :@"0"];
    }
}
 

-(IBAction)btn_submit:(id)sender
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSArray* split1 = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    //    NSArray* arr=[[split1 objectAtIndex:3] componentsSeparatedByString: @""];
    
    ran=[split1 objectAtIndex:3];
    // privalages=[ran substringWithRange:NSMakeRange(2,1)];
    acct_type=[split1 objectAtIndex:5];
     
    chk_acct_type=@"0";
    // CHeck Account Type SY,CM,O
    
    if ([acct_type isEqualToString:@"SY"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    else if ([acct_type isEqualToString:@"CM"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    if ([acct_type isEqualToString:@"O"] && [chk_acct_type isEqual:@"0"])
    {
        chk_acct_type=@"1";
        [self Pay_Account];
        
        return;
    }
    else
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Error" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    // [self Pay_Account:@""];
}



-(void)Pay_Account
{
    // Check Privalages 111000
    
    if ([privalages isEqualToString:@"0"])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    if ([txt_acct_from.text isEqualToString:@" "] || txt_acct_from.text.length==0)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select From Account" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
        
    }
    else if ([txt_acct_to.text isEqualToString:@" "] || txt_acct_to.text.length==0)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select To Account" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if ([txt_acct_from.text isEqualToString:txt_acct_to.text])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Different Account" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if ([txt_amnt_without.text isEqualToString:@" "] || txt_amnt_without.text.length==0)
    {
        
        if ([purpose_chck isEqualToString:@"1"]) //purpose_without_view
        {
            
            if ([txt_purpose.text isEqualToString:@" "] || txt_purpose.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Purpose" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([txt_amnt_with.text isEqualToString:@" "] || txt_amnt_with.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([txt_comment_with.text isEqualToString:@" "] || txt_comment_with.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                
                return;
            }
            else if(lbl_t_pin_with.hidden==YES && txt_t_pin_with.hidden==YES)
            {
                lbl_t_pin_with.hidden=NO;
                txt_t_pin_with.hidden=NO;
                
                [hud hideAnimated:YES];
                
                return ;
            }
            else if([txt_t_pin_with.text isEqualToString:@""] || [txt_t_pin_with.text isEqualToString:@" "])
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                
                return;
            }
            else if (![txt_email_with.text isEqualToString:@""]) //if (![self validateEmail:[txt_email_with text]])
            {
                
                if (![self validateEmail:[txt_email_with text]])
                {
                    alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [hud hideAnimated:YES];
                    return;
                }
                else
                {
                    // [self load_pay_list:@""];
                    [self Pay_Account:@""];
                }
            }
            
            else
            {
                // [self load_pay_list:@""];
                [self Pay_Account:@""];
            }
            
        }
        else if ([relation_chck isEqualToString:@"1"]) //purpose_with_view
        {
            if ([_txt_relationship.text isEqualToString:@" "] || _txt_relationship.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Relationship" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([_txt_relationship_purpose.text isEqualToString:@" "] || _txt_relationship_purpose.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Purpose" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([txt_relationship_amount.text isEqualToString:@" "] || txt_relationship_amount.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([txt_relationship_comment.text isEqualToString:@" "] || txt_relationship_comment.text.length==0)
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
            else if ([btn_flag_email isEqualToString:@"1"])//(![self validateEmail:[txt_email_with text]])
            {
                if (![self validateEmail:[txt_email_with text]])
                {
                    alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [hud hideAnimated:YES];
                    return;
                }
            }
            
            else if (lbl_relationship_T_pin.hidden==YES && txt_relationship_T_pin.hidden==YES)
            {
                lbl_relationship_T_pin.hidden=NO;
                txt_relationship_T_pin.hidden=NO;
                
                [hud hideAnimated:YES];
            }
            else if ([txt_relationship_T_pin.text isEqualToString:@""])
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
            }
            else
            {
                //[self load_pay_list:@""];
                [self Pay_Account:@""];
            }
            
            
        }
        
        else
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            return;
            
        }
        
    }
    else
    {
        
        if ([txt_comment_without.text isEqualToString:@" "] || txt_comment_without.text.length==0)
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        
        else if ([btn_flag_email isEqualToString:@"1"])//(![self validateEmail:[txt_email_with text]])
        {
            if (![self validateEmail:[txt_email_with text]])
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
            }
        }
        
        else if (lbl_t_pin_without.hidden==YES && txt_t_pin_without.hidden==YES)
        {
            lbl_t_pin_without.hidden=NO;
            txt_t_pin_without.hidden=NO;
            
            [hud hideAnimated:YES];
        }
        else if ([txt_t_pin_without.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
        }
        else
        {
            
            // [self load_pay_list:@""];
            [self Pay_Account:@""];
        }
    }
    
}




//
//
//-(void)Pay_Account
//{
//    // Check Privalages 111000
//
//    if ([privalages isEqualToString:@"0"])
//    {
//        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Do Not Deposit" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:ok];
//
//        [self presentViewController:alert animated:YES completion:nil];
//        [hud hideAnimated:YES];
//        return;
//    }
//
//    if ([txt_acct_from.text isEqualToString:@" "] || txt_acct_from.text.length==0)
//    {
//        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select From Account" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:ok];
//
//        [self presentViewController:alert animated:YES completion:nil];
//        [hud hideAnimated:YES];
//        return;
//
//    }
//    else if ([txt_acct_to.text isEqualToString:@" "] || txt_acct_to.text.length==0)
//    {
//        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select To Account" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:ok];
//
//        [self presentViewController:alert animated:YES completion:nil];
//        [hud hideAnimated:YES];
//
//        return;
//    }
//    else if ([txt_acct_from.text isEqualToString:txt_acct_to.text])
//    {
//        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Different Account" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        [alert addAction:ok];
//
//        [self presentViewController:alert animated:YES completion:nil];
//        [hud hideAnimated:YES];
//
//        return;
//    }
//    else if ([txt_amnt_without.text isEqualToString:@" "] || txt_amnt_without.text.length==0)
//    {
//
//        if (purpose_without_view.isHidden==YES)
//        {
//            if ([txt_purpose.text isEqualToString:@" "] || txt_purpose.text.length==0)
//            {
//                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Purpose" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alert addAction:ok];
//
//                [self presentViewController:alert animated:YES completion:nil];
//                [hud hideAnimated:YES];
//                return;
//            }
//            else if ([txt_amnt_with.text isEqualToString:@" "] || txt_amnt_with.text.length==0)
//            {
//                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alert addAction:ok];
//
//                [self presentViewController:alert animated:YES completion:nil];
//                [hud hideAnimated:YES];
//                return;
//            }
//            else if ([txt_comment_with.text isEqualToString:@" "] || txt_comment_with.text.length==0)
//            {
//                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alert addAction:ok];
//
//                [self presentViewController:alert animated:YES completion:nil];
//                [hud hideAnimated:YES];
//
//                return;
//            }
//            else if(lbl_t_pin_with.hidden==YES && txt_t_pin_with.hidden==YES)
//            {
//                lbl_t_pin_with.hidden=NO;
//                txt_t_pin_with.hidden=NO;
//
//                [hud hideAnimated:YES];
//            }
//            else if([txt_t_pin_with.text isEqualToString:@""] || [txt_t_pin_with.text isEqualToString:@" "])
//            {
//                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alert addAction:ok];
//
//                [self presentViewController:alert animated:YES completion:nil];
//                [hud hideAnimated:YES];
//
//                return;
//            }
//            else if ([btn_flag_email isEqualToString:@"1"]) //if (![self validateEmail:[txt_email_with text]])
//            {
//
//                if ([txt_email_with.text isEqualToString:@""])
//                {
//                    alert  = [UIAlertController alertControllerWithTitle:@"Attention :" message:@"Enter Email Address" preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                    [alert addAction:ok];
//
//                    [self presentViewController:alert animated:YES completion:nil];
//                    [hud hideAnimated:YES];
//                    return;
//                }
//                else if (![self validateEmail:[txt_email_with text]])
//                {
//                    alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                    [alert addAction:ok];
//
//                    [self presentViewController:alert animated:YES completion:nil];
//                    [hud hideAnimated:YES];
//                    return;
//                }
//                else
//                {
//                    // [self load_pay_list:@""];
//                    [self Pay_Account:@""];
//                }
//
//            }
//
//            else
//            {
//                // [self load_pay_list:@""];
//                [self Pay_Account:@""];
//            }
//
//        }
//        else
//        {
//            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alert addAction:ok];
//
//            [self presentViewController:alert animated:YES completion:nil];
//            [hud hideAnimated:YES];
//            return;
//
//        }
//
//    }
//    else
//    {
//
//        if ([txt_comment_without.text isEqualToString:@" "] || txt_comment_without.text.length==0)
//        {
//            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alert addAction:ok];
//
//            [self presentViewController:alert animated:YES completion:nil];
//            [hud hideAnimated:YES];
//
//            return;
//        }
//        else if (lbl_t_pin_without.hidden==YES && txt_t_pin_without.hidden==YES)
//        {
//            lbl_t_pin_without.hidden=NO;
//            txt_t_pin_without.hidden=NO;
//
//            [hud hideAnimated:YES];
//        }
//        else if ([txt_t_pin_without.text isEqualToString:@""])
//        {
//            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            [alert addAction:ok];
//
//            [self presentViewController:alert animated:YES completion:nil];
//            [hud hideAnimated:YES];
//        }
//        else if ([btn_flag_email isEqualToString:@"1"] && [txt_email_with.text isEqualToString:@""])//(![self validateEmail:[txt_email_with text]])
//        {
//            //            if ([txt_email_with.text isEqualToString:@""])
//            //            {
//            //                alert  = [UIAlertController alertControllerWithTitle:@"Attention:" message:@"Enter Email Address" preferredStyle:UIAlertControllerStyleAlert];
//            //
//            //                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//            //                [alert addAction:ok];
//            //
//            //                [self presentViewController:alert animated:YES completion:nil];
//            //                [hud hideAnimated:YES];
//            //                return;
//            //            }
//            if (![self validateEmail:[txt_email_with text]])
//            {
//                alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                [alert addAction:ok];
//
//                [self presentViewController:alert animated:YES completion:nil];
//                [hud hideAnimated:YES];
//                return;
//            }
//            else
//            {
//
//                // [self load_pay_list:@""];
//                [self Pay_Account:@""];
//            }
//        }
//        else
//        {
//
//            // [self load_pay_list:@""];
//            [self Pay_Account:@""];
//        }
//    }
//
//}








//-(void)Load_pay_list
//{
//
//        //allCompany = [[NSMutableArray alloc] init];
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
//                                                                                       ]];//baseURL];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//
//
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:login_name,@"4",@"1234",[GlobalStaticClass getPublicIp],@"iphone", nil] forKeys:[NSArray arrayWithObjects:@"strLoginName",@"strSerialNumber",@"strSessionId",@"IP",@"LogBrowserInfo", nil]];
//
//        [manager POST:@"UserLogin" parameters:dictparam
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//                  NSError *error;
//                  NSArray *arr = (NSArray *)responseObject;
//                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
//
//
//                  login_status_chck=[dic objectForKey:@"Response"];
//                  gblclass.pin_pattern=[dic objectForKey:@"PinPattern"];
//                  gblclass.user_id=[dic objectForKey:@"UserId"];
//                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
//                  //NSLog(@"Status %@",[dic objectForKey:@"Status"]);
//                  //NSLog(@"UserId %@",[dic objectForKey:@"UserId"]);
//                  //NSLog(@"PinPattern %@",[dic objectForKey:@"PinPattern"]);
//                  //NSLog(@"ReturnMessage %@",[dic objectForKey:@"ReturnMessage"]);
//
//
//                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
//                  {
//
//                      [hud hideAnimated:YES];
//
//                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
//
//                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                      [alert addAction:ok];
//
//                      [self presentViewController:alert animated:YES completion:nil];
//
//                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
//
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
//                      [self presentModalViewController:vc animated:YES];
//                      [hud hideAnimated:YES];
//
//                      return ;
//                  }
//                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
//                  {
//
//                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
//
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
//                      [self presentModalViewController:vc animated:YES];
//                      [hud hideAnimated:YES];
//
//                      return ;
//                  }
//                  else if ([[dic objectForKey:@"Response"] integerValue]==0)
//                  {
//                      [self segue];
//                  }
//
//                  [hud hideAnimated:YES];
//
//
//              }
//
//              failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  // [mine myfaildata];
// [hud hideAnimated:YES];
//              }];
//
//
//
//}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
  //  [self dismissModalStack];
    
//    [self slideRight];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:NO completion:nil];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}

#define kOFFSET_FOR_KEYBOARD 138.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //   //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [hud hideAnimated:YES];
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [self keyboardWillHide];
    
    vw_bill.hidden=YES;
    //  table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    vw_purpose.hidden=YES;
    vw_down_chck = @"1";
    
    [txt_amnt_with resignFirstResponder];
    [txt_comment_with resignFirstResponder];
    [txt_amnt_without resignFirstResponder];
    [txt_comment_without resignFirstResponder];
    [txt_t_pin_without resignFirstResponder];
    [txt_t_pin_with resignFirstResponder];
    [txt_email_with resignFirstResponder];
    [txt_relationship_T_pin resignFirstResponder];
    [txt_relationship_comment resignFirstResponder];
    [txt_relationship_amount resignFirstResponder];
    [txt_acct_to resignFirstResponder];
    [txt_email resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)btn_purpose:(id)sender
{
    [table_purpose reloadData];
    
    lbl_vw_heading.text = @"Select Purpose";
    
    if (table_purpose.isHidden==YES)
    {
        table_purpose.hidden=NO;
        vw_purpose.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_purpose.hidden=YES;
        vw_purpose.hidden=YES;
        vw_table.hidden=YES;
    }
}

-(IBAction)btn_email_chck:(id)sender
{
    
    if ([btn_flag_email isEqualToString:@"0"])
    {
        btnImage = [UIImage imageNamed:@"check.png"];
        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
        btn_flag_email=@"1";
        txt_email_with.hidden=NO;
        txt_email.hidden=NO;
        
    }
    else
    {
        btnImage = [UIImage imageNamed:@"uncheck.png"];
        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
        btn_flag_email=@"0";
        txt_email_with.hidden=YES;
        txt_email.hidden=YES;
    }
    
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [search  removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", substring];
    
    search= [NSMutableArray arrayWithArray: [arr_other_pay_list filteredArrayUsingPredicate:resultPredicate]];
    
    //    }
    
    
    table_to.hidden=NO;
    [table_to reloadData];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
    //    if(string.length > 0)
    //    {
    
    //NSLog(@"%@",textField);
    if ([textField isEqual:txt_amnt_without])
    {
        MAX_DIGITS=13;
        
        //NSLog(@"%lu",txt_amnt_without.text.length);
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else  if ([textField isEqual:txt_amnt_with])
    {
        MAX_DIGITS=13;
        
        //NSLog(@"%lu",txt_amnt_with.text.length);
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([textField isEqual:txt_t_pin_without])
    {
        MAX_DIGITS=4;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([textField isEqual:txt_t_pin_with])
    {
        MAX_DIGITS=4;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([textField isEqual:txt_acct_to])
    {
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            
            search_flag=@"1";
            [self   searchAutocompleteEntriesWithSubstring:txt_acct_to.text];
            
            //return [textField.text stringByReplacingCharactersInRange:range withString:set];
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    
    else if ([textField isEqual:txt_comment_without])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
    }
    else if ([textField isEqual:txt_comment_with])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    else if ([textField isEqual:txt_relationship_amount])
    {
        MAX_DIGITS=12;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    
    
    
    //NSLog(@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS);
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    
}



-(void)textFieldDidChange:(UITextField *)theTextField
{
    if ([theTextField isEqual:txt_amnt_without])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];

        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);

        txt_amnt_without.text=[nf stringFromNumber:myNumber];
    }
    else if ([theTextField isEqual:txt_amnt_with])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
        
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_amnt_with.text=[nf stringFromNumber:myNumber];
    }
    else if([theTextField isEqual:txt_t_pin_without])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"" withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        //  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];

        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_t_pin_without.text=[nf stringFromNumber:myNumber];
    }
    else if([theTextField isEqual:txt_t_pin_with])
    {
        NSString *textFieldText= [theTextField.text stringByReplacingOccurrencesOfString:@"" withString:@""];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        //  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
        
        txt_t_pin_with.text=formattedOutput;
    }
    else if ([theTextField isEqual:txt_relationship_amount])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];

        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_relationship_amount.text=[nf stringFromNumber:myNumber];
    }
    
}

-(IBAction)btn_Utility_bill_names:(id)sender
{
    if (table_bill.isHidden==YES)
    {
        vw_bill.hidden=NO;
        table_bill.hidden=NO;
        vw_from.hidden=YES;
        table_from.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        table_bill.hidden=YES;
        vw_bill.hidden=YES;
    }
    
}


-(IBAction)btn_relationship:(id)sender
{
    [self.view endEditing:YES];
    [table_relationship reloadData];
    lbl_vw_heading.text = @"Select Category";
    if (table_relationship.isHidden==YES)
    {
        table_relationship.hidden=NO;
        vw_relationship.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_relationship.hidden=YES;
        vw_relationship.hidden=YES;
        vw_table.hidden=YES;
    }
}


-(IBAction)btn_relationship_purpose:(id)sender
{
    lbl_vw_heading.text = @"Select Purpose";
    [self.view endEditing:YES];
    [table_Purpose_relationship reloadData];
    
    if (table_Purpose_relationship.isHidden==YES)
    {
        table_Purpose_relationship.hidden=NO;
        table_relationship.hidden=YES;
        vw_relationship_purpose.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_relationship.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        // table_purpose.hidden=YES;
        //vw_purpose.hidden=YES;
        
        vw_relationship_purpose.hidden=YES;
        table_Purpose_relationship.hidden=YES;
        vw_table.hidden=NO;
    }
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
    
    table_from.hidden=YES;
    table_to.hidden=YES;
    table_purpose.hidden=YES;
    table_relationship.hidden=YES;
    table_Purpose_relationship.hidden=YES;
}

-(IBAction)btn_review:(id)sender
{
    //  NSError* err1;
    
    @try {
        
        //    [gblclass.arr_review_withIn_myAcct removeAllObjects];
        //
        //    [gblclass.arr_review_withIn_myAcct addObject:txt_acct_to.text];
        //    [gblclass.arr_review_withIn_myAcct addObject:str_acct_no_to];
        //    [gblclass.arr_review_withIn_myAcct addObject:txt_acct_from.text];
        //    [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
        //    [gblclass.arr_review_withIn_myAcct addObject:txt_amnt.text];
        //    [gblclass.arr_review_withIn_myAcct addObject:txt_comment.text];
        
        
        [txt_amnt_with resignFirstResponder];
        [txt_comment_with resignFirstResponder];
        [txt_amnt_without resignFirstResponder];
        [txt_comment_without resignFirstResponder];
        [txt_t_pin_without resignFirstResponder];
        [txt_t_pin_with resignFirstResponder];
        [txt_email_with resignFirstResponder];
        [txt_email resignFirstResponder];
        [txt_relationship_T_pin resignFirstResponder];
        [txt_relationship_comment resignFirstResponder];
        [txt_relationship_amount resignFirstResponder];
        [txt_acct_to resignFirstResponder];
        
       
        
        [hud showAnimated:YES];
        
        //NSLog(@"%d",vw_relationship.isHidden==YES);
        //NSLog(@"%d",vw_relationship.isHidden==NO);
        //NSLog(@"%d",vw_purpose.isHidden==NO);
        
        [gblclass.arr_review_withIn_myAcct removeAllObjects];
        [gblclass.arr_values removeAllObjects];
        
        
        if (txt_acct_to.text.length==0 || [txt_acct_to.text isEqualToString:@""] ) {
            [hud hideAnimated:YES];
            [self custom_alert:@"Select To Account"  :@"0"];
            return;
        }
        
        
        if ([chck_review isEqualToString:@"ibft_pot"])
        {
            [gblclass.arr_review_withIn_myAcct addObject:@"ibft_pot"];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_to.text];
            [gblclass.arr_review_withIn_myAcct addObject:str_acct_no_to_O];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_from.text];
            [gblclass.arr_review_withIn_myAcct addObject:lbl_acct_frm.text];
            
            // [gblclass.arr_review_withIn_myAcct addObject:txt_purpose.text];
            [gblclass.arr_review_withIn_myAcct addObject:txt_amnt_with.text];
            // [gblclass.arr_review_withIn_myAcct addObject:txt_comment_with.text];
            
            gblclass.reviewCommentFieldText = txt_comment_with.placeholder;
            
        }
        else if ([chck_review isEqualToString:@"ibft_relation"]) {
            //NSLog(@"ibft_relation");
            gblclass.reviewCommentFieldText = txt_comment_with.placeholder;
        }
        else if ([chck_review isEqualToString:@"ibft relation & pot"])
        {
            if ([_txt_relationship.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Select Category"  :@"0"];
                
                return;
            }
            else if ([_txt_relationship_purpose.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Select Purpose"  :@"0"];
                
                return;
            }
            
            [gblclass.arr_review_withIn_myAcct addObject:@"ibft relation & pot"];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_to.text];
            [gblclass.arr_review_withIn_myAcct addObject:str_acct_no_to_O];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_from.text];
            [gblclass.arr_review_withIn_myAcct addObject:lbl_acct_frm.text];
            
            
          //[gblclass.arr_review_withIn_myAcct addObject:_txt_relationship.text];
          //[gblclass.arr_review_withIn_myAcct addObject:_txt_relationship_purpose.text];
            [gblclass.arr_review_withIn_myAcct addObject:txt_relationship_amount.text];
        //  [gblclass.arr_review_withIn_myAcct addObject:txt_relationship_comment.text];
            
            gblclass.reviewCommentFieldText = txt_relationship_comment.placeholder;
        }
        else
        {
            [gblclass.arr_review_withIn_myAcct addObject:@"purpose"];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_to.text];
            [gblclass.arr_review_withIn_myAcct addObject:str_acct_no_to_O];
            [gblclass.arr_review_withIn_myAcct addObject:txt_acct_from.text];
            [gblclass.arr_review_withIn_myAcct addObject:lbl_acct_frm.text];
            [gblclass.arr_review_withIn_myAcct addObject:txt_comment_with.text];
            
            str_IBFT_Relation=@"";
            str_IBFT_POT=@"";
            
            //NSLog(@"%@",txt_amnt_without.text);
            
            [gblclass.arr_review_withIn_myAcct addObject:txt_amnt_without.text];
            // [gblclass.arr_review_withIn_myAcct addObject:txt_comment_without.text];
            
            gblclass.reviewCommentFieldText = txt_comment_without.placeholder;
        }
        
        
        gblclass.check_review_acct_type=@"Other_acct";
        
        
        //    if ([purpose count]>0  && [arr_relationship_load count]==0)
        //    {
        //        chck_review=@"ibft_pot";
        //    }
        //    else if ([arr_relationship_load count]>0 && [purpose count]==0)
        //    {
        //        chck_review=@"ibft_relation";
        //    }
        //    else if ([arr_relationship_load count]>0  && [arr_relationship_load count]>0)
        //    {
        //        chck_review=@"ibft relation & pot";
        //    }
        //    else
        //    {
        //        chck_review=@"Purpose";
        //    }
        
        
        
        base_ccy=gblclass.base_currency;
        email=gblclass.user_email;
        
        if (email.length==0)
        {
            email=@"";
        }
        
        NSString* amnt,*comments;
        
        if (relationship_view.isHidden==NO)
        {
            //NSLog(@"hsdf");
        }
        
        if (purpose_with_view.isHidden==NO)
        {
            if (txt_purpose.text.length==0)
            {
                [hud hideAnimated:YES];
                
                [self custom_alert:@"Select Purpose" :@"0"];
                
                return;
            }
            
            
            amnt=txt_amnt_with.text;   //  txt_amnt_without
            comments=[txt_comment_with.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            //txt_comment_without.text;
            email=txt_email_without.text;
        }
        else if (relationship_view.isHidden==NO)
        {
            amnt=txt_relationship_amount.text;
            comments=[txt_relationship_comment.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            //txt_relationship_comment.text;
            email=@"";  //txt_email_without.text;
        }
        else
        {
            //NSLog(@"%@",txt_amnt_without.text);
            amnt=txt_amnt_without.text;
            comments=[txt_comment_without.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            // txt_comment_without.text;
            email=txt_email_with.text;
        }
        
        
        if ([btn_flag_email isEqualToString:@"0"])
        {
            email=@"";
        }
        else
        {
            
            if (![self validateEmail:[txt_email text]])
            {
                //            alert  = [UIAlertController alertControllerWithTitle:@"Email Invalid :" message:@"Please enter valid email address" preferredStyle:UIAlertControllerStyleAlert];
                //
                //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                //            [alert addAction:ok];
                //
                //            [self presentViewController:alert animated:YES completion:nil];
                
                [self custom_alert:@"Invalid Email Address" :@"0"];
                
                [hud hideAnimated:YES];
                return;
            }
            else
            {
                email=txt_email.text;          //txt_email_with
            }
            
        }
        
        
        double frm_balc,to_balc;
        
        NSNumber* lbl_balc = number;    //[lbl_balance.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSString* txt_amnt2 = [amnt stringByReplacingOccurrencesOfString:@"," withString:@""];
        frm_balc=[lbl_balc doubleValue];
        to_balc=[txt_amnt2 doubleValue];
        
        
        if ([amnt isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Enter Amount"  :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        else if ([amnt isEqualToString:@"0"])
        {
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter a valid amount"  :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter a Valid Amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        if (to_balc>frm_balc)
        {
            [hud hideAnimated:YES];

            [self custom_alert:@"Available balance is less than the transaction amount"  :@"0"];

            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Available balance is less then bill amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];

            return;
        }
        
        
        //        if ([comments isEqualToString:@""])
        //        {
        //            [hud hideAnimated:YES];
        //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
        //
        //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //            [alert addAction:ok];
        //            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        
        NSString* branch_COde=[NSString stringWithFormat:@"%d",branch_code_to];
        
        
        //NSLog(@"%@",[gblclass.arr_payee_list objectAtIndex:3]);
//        NSLog(@"%@",gblclass.arr_payee_list);
        
        
        [gblclass.arr_review_withIn_myAcct addObject:comments];
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:str_acct_id_frm_O];
        [gblclass.arr_values addObject:str_acct_name_frm];
        [gblclass.arr_values addObject:str_acct_no_frm_O];
        [gblclass.arr_values addObject:amnt];
        [gblclass.arr_values addObject:base_ccy];
        [gblclass.arr_values addObject:str_acct_id_to];
        [gblclass.arr_values addObject:str_acct_name_to];
        [gblclass.arr_values addObject:[str_acct_no_to_O uppercaseString]];
        [gblclass.arr_values addObject:[gblclass.arr_payee_list objectAtIndex:3]]; // bank_name_to
        [gblclass.arr_values addObject:@""];   //str Acct type
        [gblclass.arr_values addObject:access_key_to_1];
        [gblclass.arr_values addObject:email];
        [gblclass.arr_values addObject:str_IBFT_Relation];
        [gblclass.arr_values addObject:str_IBFT_POT]; // saki 6 feb 19
        [gblclass.arr_values addObject:@"1111"];
        [gblclass.arr_values addObject:bank_imd_to];
        [gblclass.arr_values addObject:comments];
        [gblclass.arr_values addObject:branch_COde];   //branch_code_to
        [gblclass.arr_values addObject:[str_acct_no_to_O uppercaseString]];
        [gblclass.arr_values addObject:bank_name_to];
        [gblclass.arr_values addObject:intt_purpose_code]; //_txt_relationship.text
        
//        NSLog(@"%@",gblclass.arr_values);
        
        if ([vw_down_chck isEqualToString:@""])
        {
            [self keyboardWillHide];
        }
        
        if ([[gblclass.arr_payee_list objectAtIndex:6] isEqualToString:@"Transfer Funds to CNIC"])
        {
            chk_ssl = @"payAccountOverload";
        }
        else
        {
            chk_ssl = @"review";
        }
        
        
        [self checkinternet];
        if (netAvailable)
        {
            [self SSL_Call];
        }
        
        
        // [self SSL_Call];
        
        //  [self Pay_Account_Other:@""];
        
        //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //    vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
        //    [self presentModalViewController:vc animated:YES];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES]; 
        [self custom_alert:@"Try again later."  :@"0"];
    }
    
}



- (void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) Pay_Account_Other:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //   NSString *uuid = [[NSUUID UUID] UUIDString];
    //
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
   
    
    //[gblclass.arr_values objectAtIndex:14]        Email ..
//    NSLog(@"%@",gblclass.arr_values);
    
    NSString* lbl_acct_no = [[gblclass.arr_values objectAtIndex:10] uppercaseString];
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:lbl_acct_no],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userID",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"payAnyOneAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"StrCCY",
                                                                   @"payAnyOneToAccountID",
                                                                   @"lblPayTo",
                                                                   @"lblVAccNo",
                                                                   @"lblBank",
                                                                   @"strAccType",
                                                                   @"ttAccessKey",
                                                                   @"strEmail",
                                                                   @"strIBFTRelation",
                                                                   @"strIBFTPOT",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    ////NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
 
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
               if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
                  
              }
              else if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
 
                  gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
                  gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
                  gblclass.rev_Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
                  gblclass.rev_Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
                  
                  gblclass.ft_tax = [self stringByStrippingHTML:[dic objectForKey:@"lblTransAmount"]];
                  gblclass.ft_msg = [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  [hud hideAnimated:YES];
                  [self slideRight];
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
     
              }
//              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
//              {
//                  [hud hideAnimated:YES];
//
//                  CATransition *transition = [ CATransition animation];
//                  transition.duration = 0.3;
//                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                  transition.type = kCATransitionPush;
//                  transition.subtype = kCATransitionFromRight;
//                  [self.view.window.layer addAnimation:transition forKey:nil];
//
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
//                  [self presentViewController:vc animated:NO completion:nil];
//
//              }
//              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
//              {
//                  [hud hideAnimated:YES];
//                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
//
//              }
              else
              {
                  [hud hideAnimated:YES];
                  [self.connection cancel];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
  
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
    
}

-(void)payAccount:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
   
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    
    
    
    //    0 = "userID"
    //    1 = "strSessionId"
    //    2 = "IP"
    //    3 = "payAnyOneAccountID"
    //    4 = "cmbPayFromSelectedItemText"
    //    5 = "cmbPayFromSelectedItemValue"
    //    6 = "strtxtAmount"
    //    7 = "StrCCY"
    //    8 = "payAnyOneToAccountID"
    //    9 = "lblPayTo"
    //    10 = "lblVAccNo"
    //    11 = "lblBank"
    //    12 = "strAccType"
    //    13 = "ttAccessKey"
    //    14 = "strEmail"
    //    15 = "tcAccessKey"
    //    16 = "strIBFTRelation"
    //    17 = "strIBFTPOT"
    //    18 = "strtxtPin"
    //    19 = "strGuid"
    //    20 = "Device_ID"
    //    21 = "Token"
    //    22 = "M3Key"
    
    //    0 = "1663489"
    //    1 = "10969169"
    //    2 = "45.116.233.47"
    //    3 = "2279625"
    //    4 = "SHOAIB%20IQBAL%20NICK"
    //    5 = "212549568"
    //    6 = "1000"
    //    7 = "PKR"
    //    8 = "7565760"
    //    9 = "Abdul%20Qadir"
    //    10 = "4130759109965"
    //    11 = "United%20Bank%20Limited"
    //    12 = ""
    //    13 = "3P"
    //    14 = ""
    //    15 = "3P"
    //    16 = ""
    //    17 = "3"
    //    18 = "1111"
    //    19 = "435598"
    //    20 = "835af48567013e70"
    //    21 = "185114b81b9cc416b20029a5eb4c1dd3776765d4a86666d2dea6dcf75e83b598"
    //    22 = "JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo%3D"
     
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSString* lbl_acct_no = [[gblclass.arr_values objectAtIndex:10] uppercaseString];
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:amounts],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:lbl_acct_no],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:14]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:16]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:17]],
                                [encrypt encrypt_Data:uuid],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userID",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"payAnyOneAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"StrCCY",
                                                                   @"payAnyOneToAccountID",
                                                                   @"lblPayTo",
                                                                   @"lblVAccNo",
                                                                   @"lblBank",
                                                                   @"strAccType",
                                                                   @"ttAccessKey",
                                                                   @"strEmail",
                                                                   @"tcAccessKey",
                                                                   @"strIBFTRelation",
                                                                   @"strIBFTPOT",
                                                                   @"strtxtPin",
                                                                   @"strGuid",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    ////NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              
              
//              {
//                  IsOtpRequired = 0;
//                  OtpMessage = "";
//                  Response = 0;
//                  lblTransAmount = "<br  />The Transaction charges shall be PKR 250.00 plus FED";
//                  lblTransactionType = "Direct Deposit payee";
//                  outStrIBFTPOT = 3;
//                  outStrIBFTRelation = "";
//                  strReturnMessage = " You have requested to transfer PKR 1,000.00 from your A/C No. 060510238092 (FAISAL UR REHMAN) to  Asims  with CNIC 4240169592915<br />";
//              }
              
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0) {
                  
                  gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
                  gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
                  gblclass.rev_Str_IBFT_POT=[dic objectForKey:@"outStrIBFTPOT"];
                  gblclass.rev_Str_IBFT_Relation=[dic objectForKey:@"outStrIBFTRelation"];
                  gblclass.trans_amt = [self stringByStrippingHTML:[dic objectForKey:@"lblTransAmount"]];
                  
                  gblclass.ft_tax = [self stringByStrippingHTML:[dic objectForKey:@"lblTransAmount"]];
                  gblclass.ft_msg = [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  [hud hideAnimated:YES];
                  [self slideRight];
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              //              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              //              {
              //                  [hud hideAnimated:YES];
              //
              //                  CATransition *transition = [ CATransition animation];
              //                  transition.duration = 0.3;
              //                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
              //                  transition.type = kCATransitionPush;
              //                  transition.subtype = kCATransitionFromRight;
              //                  [self.view.window.layer addAnimation:transition forKey:nil];
              //
              //                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
              //                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
              //                  [self presentViewController:vc animated:NO completion:nil];
              //
              //              }
              //              else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              //              {
              //                  [hud hideAnimated:YES];
              //                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
              //
              //              }
              else
              {
                  [hud hideAnimated:YES];
                  [self.connection cancel];
                  
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                  
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}




-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"review"])
        {
            [self Pay_Account_Other:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        } else if ([chk_ssl isEqualToString:@"payAccountOverload"]) {
            chk_ssl=@"";
            [self payAccount:@""];
        }
        else if ([chk_ssl isEqualToString:@"category"])
        {
            chk_ssl = @"";
            [self Get_Sub_Category:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"review"])
    {
        [self Pay_Account_Other:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
//        NSLog(@"%@",gblclass.arr_payee_list);
        
        [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    } else if ([chk_ssl isEqualToString:@"payAccountOverload"])
    {
        chk_ssl=@"";
        [self payAccount:@""];
    }
    else if ([chk_ssl isEqualToString:@"category"])
    {
        chk_ssl = @"";
        [self Get_Sub_Category:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

 

@end

