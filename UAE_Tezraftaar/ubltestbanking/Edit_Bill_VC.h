//
//  Edit_Bill_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 02/08/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Edit_Bill_VC : UIViewController
{
    IBOutlet UITextField* txt_bill_nick;
    IBOutlet UITextField* txt_bill_type;
    IBOutlet UITextField* txt_bill_customer_id;
    IBOutlet UILabel* lbl_header;
    IBOutlet UILabel *lbl_title;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


@end

