//
//  Tz_add_otp.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 02/07/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tz_add_otp : UIViewController<UITextFieldDelegate>
{
    //**********Reachability*********
        Reachability* internetReach;
        BOOL netAvailable;
    //**********Reachability*********
    
    IBOutlet UILabel* lbl_heading;
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

NS_ASSUME_NONNULL_END
