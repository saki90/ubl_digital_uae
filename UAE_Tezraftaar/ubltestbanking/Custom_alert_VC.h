//
//  Custom_alert_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 14/12/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Custom_alert_VC : UIViewController
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UILabel* lbl_desc;
    IBOutlet UIImageView* img;
    IBOutlet UIButton* btn_close;
    
}

@end
