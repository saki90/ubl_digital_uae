//
//  Transaction_detail_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 19/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Transaction_detail_VC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchDisplayDelegate>
{
    
    IBOutlet UILabel* lbl_transaction_id;
    IBOutlet UILabel* lbl_transaction_id_label;
    IBOutlet UILabel* lbl_frm_acct;
    IBOutlet UILabel* lbl_frm_acct_no;
    IBOutlet UILabel* lbl_frm_acct_no1;
    IBOutlet UILabel* lbl_amount;
    IBOutlet UILabel* lbl_amount_label;
    IBOutlet UILabel* lbl_status;
    IBOutlet UILabel* lbl_date;
    IBOutlet UILabel* lbl_app_id;
    IBOutlet UILabel* lbl_app_id_label;
    IBOutlet UILabel* lbl_transaction_type;
    IBOutlet UILabel* lbl_transaction_type_label;
    IBOutlet UILabel* lbl_product_nick;
    IBOutlet UIImageView* img_cr;
    IBOutlet UILabel *lbl_expireDate_title;
    IBOutlet UILabel *lbl_expireDate_label;
    IBOutlet UILabel *lbl_amount_QR_stan;
    IBOutlet UILabel *lbl_amount_label_QR_stan;
    
    IBOutlet UILabel *lbl_to_acc_number_label;
    IBOutlet UILabel *lbl_to_acc_number;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_logout:(id)sender;
-(IBAction)btn_more:(id)sender;
-(IBAction)btn_account:(id)sender;
-(IBAction)btn_screenshot:(id)sender;


@end

