//
//  Landing_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 03/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface Landing_VC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UITableView* table1;
    IBOutlet UITableView* table2;
    IBOutlet UIButton* btn_back;
    IBOutlet UIImageView* img_back;
    IBOutlet UILabel* welcome_name;
    IBOutlet UILabel* welcome_day;
    IBOutlet UILabel* welcome_date;
    IBOutlet UILabel* lbl_num_to_word;
    IBOutlet UIView* container;
    
    IBOutlet UILabel* lbl_date_dr;
    IBOutlet UILabel* lbl_tran_amount_dr;
    IBOutlet UILabel* lbl_type_dr;
    
    
    IBOutlet UILabel* lbl_date_cr;
    IBOutlet UILabel* lbl_tran_amount_cr;
    IBOutlet UILabel* lbl_type_cr;
    IBOutlet UIButton* btn_summary;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}

 

@property (weak, nonatomic) IBOutlet UIImageView *navigationBar;
@property (weak, nonatomic) IBOutlet UIButton *profileImg;
@property (weak, nonatomic) IBOutlet UIButton *accountOutlet;
@property (weak, nonatomic) IBOutlet UIButton *payOutlet;


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong)IBOutlet UITableView* table1;
//@property(nonatomic,strong)IBOutlet UITableView* table2;

@property (strong, nonatomic) IBOutlet UIImageView *Profile_imageView;
@property (nonatomic, strong) IBOutlet UIView *profile_view;

@property (nonatomic, strong) IBOutlet UILabel *ddText;
//@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

/*@property (nonatomic, strong) IBOutlet UILabel *act_name;
 @property (nonatomic, strong) IBOutlet UILabel *act_num;
 @property (nonatomic, strong) IBOutlet UILabel *act_branch;
 @property (nonatomic, strong) IBOutlet UILabel *act_balcdate;
 @property (nonatomic, strong) IBOutlet UILabel *act_avabal;*/

@property (nonatomic, strong) IBOutlet UILabel *upper_currency;
@property (nonatomic, strong) IBOutlet UILabel *upper_amount;
@property (nonatomic, strong) IBOutlet UIView *pie_view;
@property (nonatomic, strong) IBOutlet UIImageView *pie_img;

@property (weak, nonatomic) IBOutlet UIImageView *circleImg;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sarcle;


//@property (nonatomic, strong) IBOutlet UILabel *ddText;

-(IBAction)ddMenuShow:(UIButton *)sender;
-(IBAction)ddMenuSelectionMade:(UIButton *)sender;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_ref:(id)sender;

- (IBAction)takePhoto:  (UIButton *)sender;
- (IBAction)selectPhoto:(UIButton *)sender;
- (IBAction)btn_profile:(UIButton *)sender;
- (IBAction)btn_acct_summary:(UIButton *)sender;


- (void) myInstanceMethod1;
+ (void) myClassMethod1;
@property (strong) IBOutlet UIImageView *imageView_saki;

@end
