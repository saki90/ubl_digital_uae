//
//  BranchesObject.m
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "BranchesObject.h"

@implementation BranchesObject
@synthesize filePath,city_name,branch_id,branch_name,latitude,longitude,Address,Contact_no,Fax_no,ATM,Ameen_window,Lockers_Available;

- (void) encodeWithCoder: (NSCoder *) encoder
{
    [encoder encodeObject:self.city_name forKey:@"city_name"];
    [encoder encodeObject:self.branch_id forKey:@"branch_id"];
    [encoder encodeObject:self.branch_name forKey:@"branch_name"];
    [encoder encodeObject:self.latitude forKey:@"latitude"];
    [encoder encodeObject:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.Address forKey:@"Address"];
    [encoder encodeObject:self.Contact_no forKey:@"Contact_no"];
    [encoder encodeObject:self.Fax_no forKey:@"Fax_no"];
    [encoder encodeObject:self.ATM forKey:@"ATM"];
    [encoder encodeObject:self.Ameen_window forKey:@"Ameen_window"];
    [encoder encodeObject:self.Lockers_Available forKey:@"Lockers_available"];
    [encoder encodeObject:self.branchType forKey:@"branchType"];
    
    
    
    
    
}

- (id) initWithCoder: (NSCoder *) decoder
{
    self = [super init];
    if (self != nil)
    {
        self.city_name = [decoder decodeObjectForKey: @"city_name"];
        self.branch_id = [decoder decodeObjectForKey: @"branch_id"];
        self.branch_name =[decoder decodeObjectForKey: @"branch_name"];
        self.latitude =[decoder decodeObjectForKey: @"latitude"];
        self.longitude =[decoder decodeObjectForKey: @"longitude"];
        self.Address =[decoder decodeObjectForKey: @"Address"];
        self.Contact_no =[decoder decodeObjectForKey: @"Contact_no"];
        self.Fax_no =[decoder decodeObjectForKey: @"Fax_no"];
        self.ATM =[decoder decodeObjectForKey: @"ATM"];
        self.Ameen_window = [decoder decodeObjectForKey:@"Ameen_window"];
        self.Lockers_Available = [decoder decodeObjectForKey:@"Lockers_available"];
        self.branchType = [decoder decodeObjectForKey:@"branchType"];
        
    }
    return self;
}

-(BOOL)loadFile
{
    //create a temp directory
    //NSString* documentsDir = [filePath stringByDeletingLastPathComponent];
    
    BOOL result = NO;
    
    NSString* textFile = filePath;
    // //NSLog(@"text file path passed = %@",filePath);
    
    NSDictionary* texts = [NSDictionary dictionaryWithContentsOfFile:textFile];
    
    self.city_name =  (NSString*)[texts objectForKey:@"city_name"];
    self.branch_id =  (NSString*)[texts objectForKey:@"branch_id"];
    self.branch_name =  (NSString*)[texts objectForKey:@"branch_name"];
    self.latitude =  (NSString*)[texts objectForKey:@"latitude"];
    
    self.longitude =  (NSString*)[texts objectForKey:@"longitude"];
    self.Address = (NSString*)[texts objectForKey:@"Address"];
    self.Contact_no = (NSString*)[texts objectForKey:@"Contact_no"];
    self.Fax_no = (NSString*)[texts objectForKey:@"Fax_no"];
    self.ATM = (NSString*)[texts objectForKey:@"ATM"];
    self.ATM = (NSString*)[texts objectForKey:@"Ameen_window"];
    self.ATM = (NSString*)[texts objectForKey:@"Lockers_available"];
    
    return result;
}


@end
