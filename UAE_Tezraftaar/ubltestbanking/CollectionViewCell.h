//
//  CollectionViewCell.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/11/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *offerImage;
@property (weak, nonatomic) IBOutlet UILabel *offerCount;

@end
