//
//  AppDelegate+AppDelegate.h
//  UblLocationSwift
//
//  Created by Basit on 01/01/2018.
//  Copyright © 2018 M3Tech. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (AppDelegate)



-(void)writeToPlist : (NSDictionary*) data  plistName : (NSString*) plistName;
-(NSDictionary*)readingFromPlist : (NSString*) plistName;
-(void)appLaunchStatus;

@property NSDictionary *globarVariableForAdditionalServices;

@end
