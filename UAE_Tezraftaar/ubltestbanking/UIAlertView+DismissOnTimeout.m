//
//  UIAlertView+DismissOnTimeout.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 08/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "UIAlertView+DismissOnTimeout.h"

@implementation UIAlertView (DismissOnTimeout)

//[[NSNotificationCenter defaultCenter] addObserver:alertView selector:@selector(hide) name:@"UIApplicationWillResignActiveNotification" object:nil];

- (id) init {
    self = [super init];
    if (!self) return nil;
    
    // Add this instance of TestClass as an observer of the TestNotification.
    // We tell the notification center to inform us of "TestNotification"
    // notifications using the receiveTestNotification: selector. By
    // specifying object:nil, we tell the notification center that we are not
    // interested in who posted the notification. If you provided an actual
    // object rather than nil, the notification center will only notify you
    // when the notification was posted by that particular object.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTimeoutNotification:) name:@"TimeoutNotification" object:nil];
    return self;
}

- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void) receiveTimeoutNotification:(NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"TimeoutNotification"])
    {
        [self dismissWithClickedButtonIndex:0 animated:YES];
        NSLog (@"Successfully received the test notification!");
    }
}


@end
