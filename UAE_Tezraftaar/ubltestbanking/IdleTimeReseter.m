//
//  IdleTimeReseter.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 11/05/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "IdleTimeReseter.h"
#import "AppDelegate.h"
#import "Login_VC.h"
#import "GlobalStaticClass.h"

@implementation IdleTimeReseter  {
    NSTimer *idleTimer;
    AppDelegate *delegate;
    UIWindow *window;
    UIStoryboard *storyboard;
    GlobalStaticClass *gblclass;
}


- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    
    // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
    NSSet *allTouches = [event allTouches];
    
    if ([allTouches count] > 0) {
        // allTouches count only ever seems to be 1, so anyObject works here.
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
            [self resetIdleTimer];
    }
}

- (void)resetIdleTimer
{
    
     NSLog(@"idle time not akireset");
    if (idleTimer)
    {
        [idleTimer invalidate];
    }
    
    NSLog(@"idle time reset");
    idleTimer = [NSTimer scheduledTimerWithTimeInterval:300 target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO]; //900
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}


- (void)idleTimerExceeded
{

    delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    gblclass = [GlobalStaticClass getInstance];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:[NSBundle mainBundle]];
    window = delegate.window;
    
    NSLog(@"IDLE Timmer Exceded");
    
    // Fired to dissmiss the alert viewas.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimeoutNotification" object:self];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController.view endEditing:YES];
    gblclass = [GlobalStaticClass getInstance];
    NSString*  enable_touch = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"enable_touch"];
    
    NSLog(@"%lu", (unsigned long)[enable_touch length]);
    NSLog(@"%lu", (unsigned long)[gblclass.vc length]);
    
    if (!([enable_touch length] == 0))
    { // && !(gblclass.vc.length == 0) //([enable_touch isEqualToString:@"0"] || [enable_touch isEqualToString:@"1"])
      
        
      [self slideRight];
        
      NSString* stepedVC;
      NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"enable_touch"];

        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        NSLog(@"------------ TIMEOUT------------- %@",stepedVC);
        
//        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
//        [self presentViewController:vc animated:NO completion:nil];
        
        UIViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:stepedVC];
        window.rootViewController = loginViewController;
        
    }
    else
    {
        [self resetIdleTimer];
        NSLog(@"TIME RESET");
    }
    
    
//    if (([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"]) && gblclass.isLogedIn) {
//        UIViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self slideRight];
//        window.rootViewController = loginViewController;
//
//    }
//    else if (!([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"]) && !gblclass.isLogedIn) {
//        UIViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"next"];
//        [self slideRight];
//        window.rootViewController = loginViewController;
//
//    } else {
//        [self resetIdleTimer];
//    }
    
}

-(void)slideLeft {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [window.layer addAnimation:transition forKey:nil];
    
}

-(void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [window.layer addAnimation:transition forKey:nil];
}

@end

