//
//  Check_Managmnt_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Check_Managmnt_VC.h"

@interface Check_Managmnt_VC ()
{
    UIAlertController * alrt;
    NSArray* arr_table2;
    NSArray* arr_table1;
    NSString* table_row;
    UILabel* label;
    NSString* flash_btn;
    
}
@end

@implementation Check_Managmnt_VC
@synthesize ddMenu;
@synthesize txt_check_name;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    table1.delegate=self;
    ddMenu.hidden=YES;
    
    
    arr_table1=@[@"AHMED SHAHZAD-2387898",@"AHMED-2387898",@"SHAHZAD-2387898",@"AHMED SHAHZAD-2387898",];
    arr_table2=@[@"Account Summary",@"Account Statement",@"Cheque Management",@"ATM Card Management"];

    
    flash_btn=@"0";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)ddMenuShow:(UIButton *)sender
{
    if ([flash_btn isEqualToString:@"0"]) //sender.tag
    {
        flash_btn = @"1";  //sender.tag = 1;
        self.ddMenu.hidden = NO;
        [sender setTitle:@"▲" forState:UIControlStateNormal];
    } else {
        
        flash_btn = @"0"; //sender.tag = 0;
        self.ddMenu.hidden = YES;
        [sender setTitle:@"▼" forState:UIControlStateNormal];
    }
}


- (IBAction)ddMenuSelectionMade:(UIButton *)sender
{
    self.ddText.text = sender.titleLabel.text;
    [self.ddMenuShowButton setTitle:@"▼" forState:UIControlStateNormal];
    self.ddMenuShowButton.tag = 0;
    self.ddMenu.hidden = YES;
    switch (sender.tag)
    {
        case 1:
            self.view.backgroundColor = [UIColor redColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor blueColor];
            break;
        case 3:
            self.view.backgroundColor = [UIColor greenColor];
            break;
            
        default:
            break;
    }
    
}


- (IBAction)indexChanged:(UISegmentedControl *)sender
{
    switch (self.segmentedControl.selectedSegmentIndex)
    {
        case 0:
            [table2 reloadData];
            break;
        case 1:
            [table2 reloadData];
            break;
        default:
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==table1)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    table_row =[arr_table2 objectAtIndex:indexPath.row];
    //NSLog(@"Selected item %@ ",arr_table2);
    
    
    
    if (tableView==table1)
    {
         //NSLog(@"%lu",(unsigned long)[arr_table1 objectAtIndex:indexPath.row]);
         txt_check_name.text=[arr_table1 objectAtIndex:indexPath.row];
         ddMenu.hidden=YES;
        
        
        if ([flash_btn isEqualToString:@"0"]) //sender.tag
        {
            flash_btn = @"1";  //sender.tag = 1;
            self.ddMenu.hidden = NO;
           // [sender setTitle:@"▲" forState:UIControlStateNormal];
        } else {
            
            flash_btn = @"0"; //sender.tag = 0;
            self.ddMenu.hidden = YES;
           // [sender setTitle:@"▼" forState:UIControlStateNormal];
        }
        
    }
    else
    {
         //NSLog(@"%lu",(unsigned long)[arr_table1 objectAtIndex:indexPath.row]);
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==table1)
    {
        //NSLog(@"%lu",(unsigned long)[arr_table1 count]);
        
        return [arr_table1 count];
    }
    else
    {
        if (section==0)
        {
            return [arr_table2 count];
        }
        else
        {
            return [arr_table2 count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (tableView==table1)
    {
        cell.textLabel.text = [arr_table1 objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        
    }
    else
    {
        
        
        switch (self.segmentedControl.selectedSegmentIndex)
        {
            case 0:
                // For Account ::
                
                label=(UILabel*)[cell viewWithTag:1];
                label.text=@"Account no 2432423 cheque no: 856903 HABIB BANK LIMITED";
                label.font=[UIFont systemFontOfSize:14];
                [cell.contentView addSubview:label];
                
                // For Amount ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text=@"PKR 1,000.00 PAID";
                label.font=[UIFont systemFontOfSize:14];
                [cell.contentView addSubview:label];
                
                // For Date ::
                
                label=(UILabel*) [cell viewWithTag:3];
                label.text=@"27-Jul-2014";
                label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];

                break;
            case 1:
                // For Account ::
                
                label=(UILabel*)[cell viewWithTag:1];
                label.text=@"Account no 8888888 cheque no: 67676767 UBL BANK LIMITED";
                label.font=[UIFont systemFontOfSize:14];
                [cell.contentView addSubview:label];
                
                // For Amount ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text=@"PKR 3,050.00 PAID";
                label.font=[UIFont systemFontOfSize:14];
                [cell.contentView addSubview:label];
                
                // For Date ::
                
                label=(UILabel*) [cell viewWithTag:3];
                label.text=@"30-Aug-2015";
                label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];

                
                break;
            default:
                break;
        }

    }
    return cell;
}

@end
