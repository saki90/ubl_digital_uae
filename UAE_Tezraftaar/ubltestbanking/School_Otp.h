//
//  School_Otp.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 27/08/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface School_Otp : UIViewController<NSURLConnectionDataDelegate,UITextFieldDelegate>
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@end
