//
//  Login_New.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 27/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>

@interface Login_New : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate>
{
    IBOutlet UITextField* txt_login;
    IBOutlet UITextField* txt_pw1;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_tch_view;
    IBOutlet UIView* vw_without_tch_view;
    IBOutlet UIButton* btn_changeto_tch_ID;
    IBOutlet UIButton* btn_apply_for_acct;
    IBOutlet UIView* vw_apply_acct;
    IBOutlet UIView* vw_tracking;
    IBOutlet UIView* vw_apply_for_acct_line;
    
    IBOutlet UIButton* btn_tracking;
    IBOutlet UIView* vw_btn_tracking_line;
    IBOutlet UIButton* btn_continue;
    IBOutlet UIView* vw_btn_continue_line;
    IBOutlet UIButton* btn_cancel;
    IBOutlet UIView* vw_btn_cancel_line;
    
    
    IBOutlet UITextField* txt_tch_login;
    IBOutlet UITextField* txt_tch_pw;
    IBOutlet UIView* vw_login_line;
    
    
    __weak IBOutlet UITextField *emailOutlet;
    __weak IBOutlet UIView *emailView;
    __weak IBOutlet UITextField *passOutlet;
    __weak IBOutlet UIView *passView;
    __weak IBOutlet UIButton *forgetPass;
    __weak IBOutlet UIButton *nxt_outlet;
    
    
    
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc_tch;
    IBOutlet UIButton* btn_check_JB_tch;
    IBOutlet UIButton* btn_check_JB_rooted;
    IBOutlet UIButton* btn_next_tch;
    IBOutlet UIButton* btn_next_jb;
    IBOutlet UIButton* btn_resend_otp;
    
    
    
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}


@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) AVPlayer *avplayer;


@property(nonatomic,retain) CLLocationManager *locationManager2;
-(IBAction)btn_feature:(id)sender;
-(IBAction)btn_offer:(id)sender;
-(IBAction)btn_find_us:(id)sender;
-(IBAction)btn_faq:(id)sender;


@end


