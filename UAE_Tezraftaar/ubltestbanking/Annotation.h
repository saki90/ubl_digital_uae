//
//  Annotation.h
//  MKMapview
//
//  Created by INCISIVESOFT MM1 on 18/02/2014.
//  Copyright (c) 2014 INCISIVESOFT MM1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Annotation : NSObject<MKAnnotation>

@property(nonatomic,assign) CLLocationCoordinate2D coordinate;
@property(nonatomic,copy)  NSString * title;
@property(nonatomic,copy)  NSString * subtitle;

@end
