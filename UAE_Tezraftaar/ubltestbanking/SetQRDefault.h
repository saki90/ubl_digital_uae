//
//  setQRDefault.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 04/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface SetQRDefault : UIViewController<UITableViewDelegate>
{
    IBOutlet UITableView* table;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

