//
//  TermsAndConditionsViewController.h
//  ubltestbanking
//
//  Created by ammar on 24/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import <WebKit/WebKit.h>

@interface TermsAndConditionsViewController : UIViewController
{
//    IBOutlet UIWebView* webView;
    
     IBOutlet WKWebView* webView1;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}
@property (nonatomic, strong) TransitionDelegate *transitionController;
//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end


