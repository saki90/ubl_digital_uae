//
//  Add_payee_TZ.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 03/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


NS_ASSUME_NONNULL_BEGIN

@interface Add_payee_TZ : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
    IBOutlet UILabel* lbl_bottom_txt;
    IBOutlet UIView* vw_from;
    IBOutlet UITableView* table_from;
    
//    txt_acct_from.text = gblclass.is_default_acct_id_name;
//    str_acct_name_frm = gblclass.is_default_acct_id_name;
//    str_acct_no_frm = gblclass.is_default_acct_no;
//    str_acct_id_frm = gblclass.is_default_acct_id;
//    _lbl_acct_frm.text
    
    IBOutlet UITextField* txt_acct_from;
    IBOutlet UILabel* lbl_acct_frm;
    IBOutlet UILabel* lbl_vw_header;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;




@end

NS_ASSUME_NONNULL_END
