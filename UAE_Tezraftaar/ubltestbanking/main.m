  //
//  main.m
//  ubltestbanking
//
//  Created by ammar on 21/01/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "IdleTimeReseter.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        //NSStringFromClass([IdleTimeReseter class])
        return UIApplicationMain(argc, argv, NSStringFromClass([IdleTimeReseter class]), NSStringFromClass([AppDelegate class]));
    }
}

