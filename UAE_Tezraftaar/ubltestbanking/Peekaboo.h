//
//  Header.h
//  Connect Sample
//
//  Created by Zain Sajjad on 20/03/2019.
//  Copyright © 2019 Fetch Sky. All rights reserved.
//

#ifndef Peekaboo_h
#define Peekaboo_h

@interface Peekaboo : NSObject

+(id)sharedManager;
- (UIViewController *) getPeekabooUIViewController:(NSDictionary *)type;

@end

#endif /* Peekaboo_h */
