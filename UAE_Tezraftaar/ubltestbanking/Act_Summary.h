//
//  Branch_Act_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Act_Summary : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UITableView* table1;
    IBOutlet UITableView* table2;
    IBOutlet UIButton* btn_back;
    IBOutlet UIImageView* img_back;
    IBOutlet UILabel* welcome_name;
    IBOutlet UILabel* welcome_day;
    IBOutlet UILabel* welcome_date;
    IBOutlet UILabel* lbl_num_to_word;
    IBOutlet UIView* container;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong)IBOutlet UITableView* table1;
//@property(nonatomic,strong)IBOutlet UITableView* table2;

@property (strong, nonatomic) IBOutlet UIImageView *Profile_imageView;
@property (nonatomic, strong) IBOutlet UIView *profile_view;

@property (nonatomic, strong) IBOutlet UILabel *ddText;
//@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

/*@property (nonatomic, strong) IBOutlet UILabel *act_name;
 @property (nonatomic, strong) IBOutlet UILabel *act_num;
 @property (nonatomic, strong) IBOutlet UILabel *act_branch;
 @property (nonatomic, strong) IBOutlet UILabel *act_balcdate;
 @property (nonatomic, strong) IBOutlet UILabel *act_avabal;*/

@property (nonatomic, strong) IBOutlet UILabel *upper_currency;
@property (nonatomic, strong) IBOutlet UILabel *upper_amount;
@property (nonatomic, strong) IBOutlet UIView *pie_view;
@property (nonatomic, strong) IBOutlet UIImageView *pie_img;


//@property (nonatomic, strong) IBOutlet UILabel *ddText;

-(IBAction)ddMenuShow:(UIButton *)sender;
-(IBAction)ddMenuSelectionMade:(UIButton *)sender;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_ref:(id)sender;

- (IBAction)takePhoto:  (UIButton *)sender;
- (IBAction)selectPhoto:(UIButton *)sender;
- (IBAction)btn_profile:(UIButton *)sender;



@end
