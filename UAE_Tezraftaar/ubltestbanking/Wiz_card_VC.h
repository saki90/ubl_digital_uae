//
//  Wiz_card_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Wiz_card_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
}



@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;

@property (nonatomic, strong) IBOutlet UILabel *cc_name;
@property (nonatomic, strong) IBOutlet UILabel *cc_num;
//@property (nonatomic, strong) IBOutlet UILabel *act_branch;
@property (nonatomic, strong) IBOutlet UILabel *cc_balcdate;
@property (nonatomic, strong) IBOutlet UILabel *cc_outstandingbal;


@end
