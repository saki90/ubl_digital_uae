//
//  set_instant_qr.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 13/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "TransitionDelegate.h"


@interface set_instant_qr : UIViewController
{
    
    IBOutlet UILabel* lbl_switch;
    IBOutlet UISwitch* myswitch;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

