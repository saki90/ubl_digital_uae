//
//  Bill_Toll_All_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 21/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Bill_Toll_All_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
//#import "GnbBillList.h"
//#import "Bill_management_Model_class.h"
//#import "OutdtDatasetISP.h"
//#import "OutdtDatasetOB.h"
//#import "OutdtDatasetUBLBP.h"
//#import "OutdtDatasetUBP.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"

@interface Bill_Toll_All_VC ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSArray* split;
    NSArray* split_bill;
    NSDictionary* dic;
    MBProgressHUD* hud;
    UIAlertController *alert;
    NSMutableArray* arr_get_bill;
    NSMutableArray* arr_get_bill_load;
    NSMutableArray* a;
    // UIAlertController *alert;
    
    NSString* payfrom_acct_id;
    NSString* Payfrm_selected_item_text;
    NSString* Payfrm_selected_item_value;
    NSString* str_amnt;
    NSString* lbl_customer_id;
    NSString* lbl_compny_name;
    NSString* str_comment;
    NSString* str_access_key;
    NSString* str_Tc_access_key;
    NSString* str_ccy;
    NSString* str_TT_accesskey;
    NSString* str_tt_id;
    NSString* str_tt_name;
    NSString* str_regt_consumer_id;
    NSString* str_build;
    NSString* lb_customer_id;
    NSString* lbl_customer_nick;
    NSString* status;
    NSString* str_bill_id;
    NSString* str_due_datt;
    NSString* PAYMENT_STATUS;
    NSNumber* IS_PAYBLE_1;
    
    NSMutableArray* arr_bill_load_all;
    
    NSMutableArray* arr_bill_ISP;
    NSMutableArray* arr_bill_OB;
    NSMutableArray* arr_bill_UBLBP;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    NSArray* animalIndexTitles;
    
    
    NSMutableArray* arr_isp;
    NSMutableArray* arr_ubl;
    NSMutableArray* arr_ublbp;
    NSMutableArray* arr_ob;
    NSArray* table_header;
    UILabel* label;
    NSString* img_name;
    NSNumberFormatter *numberFormatter;
    
    UIButton *button_header;
    double bill_price;
    NSMutableAttributedString *string;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Bill_Toll_All_VC
@synthesize transitionController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    numberFormatter = [NSNumberFormatter new];
    //    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    gblclass=[GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    table_from.hidden=YES;
    table_to.hidden=YES;
    
    arr_isp=[[NSMutableArray alloc] init];
    arr_ubl=[[NSMutableArray alloc] init];
    arr_ublbp=[[NSMutableArray alloc] init];
    arr_ob=[[NSMutableArray alloc] init];
    gblclass.arr_internet = [[NSMutableArray alloc] init];
    gblclass.arr_postpaid = [[NSMutableArray alloc] init];
    gblclass.arr_mobiletopup = [[NSMutableArray alloc] init];
    
    arr_bill_ISP=[[NSMutableArray alloc] init];
    arr_bill_OB=[[NSMutableArray alloc] init];
    arr_bill_UBLBP=[[NSMutableArray alloc] init];
    gblclass.arr_bill_elements=[[NSMutableArray alloc] init];
    
    arr_bill_load_all=[[NSMutableArray alloc] init];
    arr_get_bill=[[NSMutableArray alloc] init];
    arr_get_bill_load=[[NSMutableArray alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    //   table_header=@[@"Utility Bills",@"Mobile Bills",@"ISP Bill",@"UBL Bills"];
    
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    ssl_count = @"0";
    vw_from.hidden=YES;
    vw_to.hidden=YES;
    txt_t_pin.delegate=self;
    txt_comment.delegate=self;
    lbl_t_pin.hidden=YES;
    txt_t_pin.hidden=YES;
    
    // [btn_submit setBackgroundColor:  [UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    txt_combo_frm.text= gblclass.is_default_acct_id_name;
    
    Payfrm_selected_item_text=gblclass.is_default_acct_id_name;
    Payfrm_selected_item_value=gblclass.is_default_acct_no;
    payfrom_acct_id=gblclass.is_default_acct_id;
    
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    //     [self Get_Bill:@""];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
    }
    
    animalIndexTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_submit:(id)sender
{
    
}

-(void) Get_Bill:(NSString *)strIndustry
{
    
    @try {
        
   
//    NSLog(@"%@",gblclass.mainurl1);
        
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_get_bill=[[NSMutableArray alloc] init];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:@"ME_TELCO"],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abcd"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strTcAccessKey",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);    GetBill
    //GetBillData
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetBillData" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
            //NSArray *arr = (NSArray *)responseObject;
        
              NSDictionary* dic_OB,*dic_ISP,*dic_UBLBP;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              dic_ISP = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              dic_UBLBP = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              dic_OB = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSString* response = [dic objectForKey:@"Response"];
              NSArray* split_res = [[dic objectForKey:@"Response"] componentsSeparatedByString: @"|"];
              
              
              gblclass.arr_bill_load_all=[[NSMutableArray alloc] init];
              gblclass.arr_bill_ob=[[NSMutableArray alloc] init];
              gblclass.arr_bill_isp=[[NSMutableArray alloc] init];
              gblclass.arr_bill_ublbp=[[NSMutableArray alloc] init];
        
              gblclass.arr_internet = [[NSMutableArray alloc] init];
              gblclass.arr_postpaid = [[NSMutableArray alloc] init];
              gblclass.arr_mobiletopup = [[NSMutableArray alloc] init];
        
              
              //              arr_bill_load_all= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              //              arr_bill_OB= [(NSDictionary *)[dic objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
              //              arr_bill_ISP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
              //              arr_bill_UBLBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
               
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
//              table_header = @[@"Utility Bills",@"Mobile Bills",@"ISP Bill",@"UBL Bills"];
        
         table_header = @[@"Mobile Top Up",@"Internet & TV Services",@"Postpaid & Landline"];
        
              
//              arr_bill_OB= [(NSDictionary *)[dic objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
//              arr_bill_ISP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
//              arr_bill_UBLBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              
              a=[[NSMutableArray alloc] init];
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
            //if ([[dic objectForKey:@"resultUBLBP"] isEqualToString:@"-1"]) {
              //Utility bill ::
        
        
           //saki 29 jan 2020     if ([[split_res objectAtIndex:0] isEqualToString:@"0"]) { //1
  
              if ([response isEqualToString:@"0"])
              {
                  arr_bill_load_all = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbBillList"];
                  
              //          NSUInteger *set;
              for (dic in arr_bill_load_all)
              {
                  
                  //             set = [arr_bill_load_all indexOfObject:dic];
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || TT_NAME==(NSString *)[NSNull null])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "] || TT_ID==(NSString *)[NSNull null])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || CONSUMER_NO==(NSString *)[NSNull null])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || NICK==(NSString *)[NSNull null])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || DUE_DATE==(NSString *)[NSNull null])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || PAYABLE_AMOUNT==(NSString *)[NSNull null])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || PAYMENT_STATUS_DESC==(NSString *)[NSNull null])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || BILL_ID==(NSString *)[NSNull null])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || ACCESS_KEY==(NSString *)[NSNull null])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || REGISTERED_CONSUMERS_ID==(NSString *)[NSNull null])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || COMPANY_NAME==(NSString *)[NSNull null])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || REGISTERED_ACCOUNT_ID==(NSString *)[NSNull null])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || TC_ACCESS_KEY==(NSString *)[NSNull null])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS1=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS1.length==0 || [PAYMENT_STATUS1 isEqualToString:@" "] || PAYMENT_STATUS1==(NSString *)[NSNull null])
                  {
                      PAYMENT_STATUS1=@"N/A";
                      [a addObject:PAYMENT_STATUS1];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS1];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || IS_PAYBLE==(NSString *)[NSNull null])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  NSString* TYPE_NAME=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TYPE_NAME"]];
                  if (TYPE_NAME.length==0 || [TYPE_NAME isEqualToString:@" "] || TYPE_NAME==(NSString *)[NSNull null])
                  {
                      TYPE_NAME=@"N/A";
                      [a addObject:TYPE_NAME];
                  }
                  else
                  {
                      [a addObject:TYPE_NAME];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  
                  //For Internet & TV Services ::
                  if ([TT_NAME isEqualToString:@"Al Shamil Broadband"] || [TT_NAME isEqualToString:@"E-Life"] || [TT_NAME isEqualToString:@"E-Vision"] || [TT_NAME isEqualToString:@"Internet Dial-Up"] || [TT_NAME isEqualToString:@"al shamil broadband"] || [TT_NAME isEqualToString:@"e-life"] || [TT_NAME isEqualToString:@"e-vision"] || [TT_NAME isEqualToString:@"internet dial-up"])
                  {
                      [gblclass.arr_internet addObject:bbb];
                  }
                  else if ([TT_NAME isEqualToString:@"Al Wasel Top Up"] || [TT_NAME isEqualToString:@"Top Up"] || [TT_NAME isEqualToString:@"al wasel top up"])
                  {
                      [gblclass.arr_mobiletopup addObject:bbb];
                  }
                  else if ([TT_NAME isEqualToString:@"Landline"] || [TT_NAME isEqualToString:@"Mobile/Landline Number"] || [TT_NAME isEqualToString:@"Post Paid"] || [TT_NAME isEqualToString:@"landline"] || [TT_NAME isEqualToString:@"mobile/landline number"] || [TT_NAME isEqualToString:@"post paid"])
                  {
                      [gblclass.arr_postpaid addObject:bbb];
                  }
                  
                  //[gblclass.arr_bill_load_all addObject:bbb];
                  
                  [a removeAllObjects];
              }
        }
        else
        {
            [hud hideAnimated:YES];
            [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
        }
        
        
        
        
//
//              // if ([[dic_OB objectForKey:@"resultOB"] isEqualToString:@"0"]) {
//              if ([[split_res objectAtIndex:1] isEqualToString:@"0"]) {
//
//                arr_bill_OB= [(NSDictionary *)[dic_OB objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
//
//
//              // FOR OB Bill
//
//
//              for (dic_OB in arr_bill_OB)
//              {
//
//                  NSString* TT_NAME=[dic_OB objectForKey:@"TT_NAME"];
//                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
//                  {
//                      TT_NAME=@"N/A";
//                      [a addObject:TT_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:TT_NAME];
//                  }
//
//                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"TT_ID"]];
//                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
//                  {
//                      TT_ID=@"N/A";
//                      [a addObject:TT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:TT_ID];
//                  }
//
//                  NSString* CONSUMER_NO=[dic_OB objectForKey:@"CONSUMER_NO"];
//                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
//                  {
//                      CONSUMER_NO=@"N/A";
//                      [a addObject:CONSUMER_NO];
//                  }
//                  else
//                  {
//                      [a addObject:CONSUMER_NO];
//                  }
//
//                  NSString* NICK=[dic_OB objectForKey:@"NICK"];
//                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
//                  {
//                      NICK=@"N/A";
//                      [a addObject:NICK];
//                  }
//                  else
//                  {
//                      [a addObject:NICK];
//                  }
//
//                  NSString* DUE_DATE=[dic_OB objectForKey:@"DUE_DATE"];
//                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
//                  {
//                      DUE_DATE=@"N/A";
//                      [a addObject:DUE_DATE];
//                  }
//                  else
//                  {
//                      [a addObject:DUE_DATE];
//                  }
//
//                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"PAYABLE_AMOUNT"]];
//                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
//                  {
//                      PAYABLE_AMOUNT=@"N/A";
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//                  else
//                  {
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//
//                  NSString* PAYMENT_STATUS_DESC=[dic_OB objectForKey:@"PAYMENT_STATUS_DESC"];
//                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS_DESC=@"N/A";
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//
//                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"BILL_ID"]];
//                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
//                  {
//                      BILL_ID=@"N/A";
//                      [a addObject:BILL_ID];
//                  }
//                  else
//                  {
//                      [a addObject:BILL_ID];
//                  }
//
//                  NSString* ACCESS_KEY=[dic_OB objectForKey:@"ACCESS_KEY"];
//                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
//                  {
//                      ACCESS_KEY=@"N/A";
//                      [a addObject:ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:ACCESS_KEY];
//                  }
//
//                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"REGISTERED_CONSUMERS_ID"]];
//                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
//                  {
//                      REGISTERED_CONSUMERS_ID=@"N/A";
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//
//                  NSString* COMPANY_NAME=[dic_OB objectForKey:@"COMPANY_NAME"];
//                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
//                  {
//                      COMPANY_NAME=@"N/A";
//                      [a addObject:COMPANY_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:COMPANY_NAME];
//                  }
//
//                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"REGISTERED_ACCOUNT_ID"]];
//
//
//                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
//                  {
//                      REGISTERED_ACCOUNT_ID=@"N/A";
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//
//
//                  NSString* TC_ACCESS_KEY=[dic_OB objectForKey:@"TC_ACCESS_KEY"];
//                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
//                  {
//                      TC_ACCESS_KEY=@"N/A";
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//
//                  NSString* PAYMENT_STATUS2=[dic_OB objectForKey:@"PAYMENT_STATUS"];
//                  if (PAYMENT_STATUS2.length==0 || [PAYMENT_STATUS2 isEqualToString:@" "] || [PAYMENT_STATUS2 isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS2=@"N/A";
//                      [a addObject:PAYMENT_STATUS2];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS2];
//                  }
//
//                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic_OB objectForKey:@"IS_PAYBLE"]];
//                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
//                  {
//                      IS_PAYBLE=@"N/A";
//                      [a addObject:IS_PAYBLE];
//                  }
//                  else
//                  {
//                      [a addObject:IS_PAYBLE];
//                  }
//
//
//                  NSString* bbb = [a componentsJoinedByString:@"|"];
//                  //NSLog(@"%@", bbb);
//                  [gblclass.arr_bill_ob addObject:bbb];
//                  // [arr_ob addObject:bbb];
//                  [a removeAllObjects];
//
//              }
//               }
//
//              // OB End
//
//
//         //      if ([[dic_UBLBP objectForKey:@"resultUBLBP"] isEqualToString:@"0"]) {
//            if ([[split_res objectAtIndex:3] isEqualToString:@"0"]) {
//
//                        arr_bill_UBLBP= [(NSDictionary *)[dic_UBLBP objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
//
//              // FOR UBLBP Bill
//
//
//              for (dic_UBLBP in arr_bill_UBLBP)
//              {
//
//                  NSString* TT_NAME=[dic_UBLBP objectForKey:@"TT_NAME"];
//                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
//                  {
//                      TT_NAME=@"N/A";
//                      [a addObject:TT_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:TT_NAME];
//                  }
//
//                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"TT_ID"]];
//                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
//                  {
//                      TT_ID=@"N/A";
//                      [a addObject:TT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:TT_ID];
//                  }
//
//                  NSString* CONSUMER_NO=[dic_UBLBP objectForKey:@"CONSUMER_NO"];
//                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
//                  {
//                      CONSUMER_NO=@"N/A";
//                      [a addObject:CONSUMER_NO];
//                  }
//                  else
//                  {
//                      [a addObject:CONSUMER_NO];
//                  }
//
//                  NSString* NICK=[dic_UBLBP objectForKey:@"NICK"];
//                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
//                  {
//                      NICK=@"N/A";
//                      [a addObject:NICK];
//                  }
//                  else
//                  {
//                      [a addObject:NICK];
//                  }
//
//                  NSString* DUE_DATE=[dic_UBLBP objectForKey:@"DUE_DATE"];
//                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
//                  {
//                      DUE_DATE=@"N/A";
//                      [a addObject:DUE_DATE];
//                  }
//                  else
//                  {
//                      [a addObject:DUE_DATE];
//                  }
//
//                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"PAYABLE_AMOUNT"]];
//                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
//                  {
//                      PAYABLE_AMOUNT=@"N/A";
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//                  else
//                  {
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//
//                  NSString* PAYMENT_STATUS_DESC=[dic_UBLBP objectForKey:@"PAYMENT_STATUS_DESC"];
//                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS_DESC=@"N/A";
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//
//                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"BILL_ID"]];
//                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
//                  {
//                      BILL_ID=@"N/A";
//                      [a addObject:BILL_ID];
//                  }
//                  else
//                  {
//                      [a addObject:BILL_ID];
//                  }
//
//                  NSString* ACCESS_KEY=[dic_UBLBP objectForKey:@"ACCESS_KEY"];
//                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
//                  {
//                      ACCESS_KEY=@"N/A";
//                      [a addObject:ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:ACCESS_KEY];
//                  }
//
//                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"REGISTERED_CONSUMERS_ID"]];
//                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
//                  {
//                      REGISTERED_CONSUMERS_ID=@"N/A";
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//
//                  NSString* COMPANY_NAME=[dic_UBLBP objectForKey:@"COMPANY_NAME"];
//                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
//                  {
//                      COMPANY_NAME=@"N/A";
//                      [a addObject:COMPANY_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:COMPANY_NAME];
//                  }
//
//                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"REGISTERED_ACCOUNT_ID"]];
//
//
//                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
//                  {
//                      REGISTERED_ACCOUNT_ID=@"N/A";
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//
//
//                  NSString* TC_ACCESS_KEY=[dic_UBLBP objectForKey:@"TC_ACCESS_KEY"];
//                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
//                  {
//                      TC_ACCESS_KEY=@"N/A";
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//
//                  NSString* PAYMENT_STATUS3=[dic_UBLBP objectForKey:@"PAYMENT_STATUS"];
//                  if (PAYMENT_STATUS3.length==0 || [PAYMENT_STATUS3 isEqualToString:@" "] || [PAYMENT_STATUS3 isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS3=@"N/A";
//                      [a addObject:PAYMENT_STATUS3];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS3];
//                  }
//
//                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic_UBLBP objectForKey:@"IS_PAYBLE"]];
//                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
//                  {
//                      IS_PAYBLE=@"N/A";
//                      [a addObject:IS_PAYBLE];
//                  }
//                  else
//                  {
//                      [a addObject:IS_PAYBLE];
//                  }
//
//
//                  NSString* bbb = [a componentsJoinedByString:@"|"];
//                  //NSLog(@"%@", bbb);
//                  [gblclass.arr_bill_ublbp addObject:bbb];
//                  // [arr_ublbp addObject:bbb];
//                  [a removeAllObjects];
//
//              }
//               }
//
//              // UBLBP End
//
//
//         //     if ([[dic_ISP objectForKey:@"resultISP"] isEqualToString:@"0"]) {
//
//               if ([[split_res objectAtIndex:2] isEqualToString:@"0"]) {
//
//                    arr_bill_ISP= [(NSDictionary *)[dic_ISP objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
//
//              // FOR ISP Bill
//
//
//              for (dic_ISP in arr_bill_ISP)
//              {
//
//                  NSString* TT_NAME=[dic_ISP objectForKey:@"TT_NAME"];
//                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
//                  {
//                      TT_NAME=@"N/A";
//                      [a addObject:TT_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:TT_NAME];
//                  }
//
//                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"TT_ID"]];
//                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
//                  {
//                      TT_ID=@"N/A";
//                      [a addObject:TT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:TT_ID];
//                  }
//
//                  NSString* CONSUMER_NO=[dic_ISP objectForKey:@"CONSUMER_NO"];
//                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
//                  {
//                      CONSUMER_NO=@"N/A";
//                      [a addObject:CONSUMER_NO];
//                  }
//                  else
//                  {
//                      [a addObject:CONSUMER_NO];
//                  }
//
//                  NSString* NICK=[dic_ISP objectForKey:@"NICK"];
//                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
//                  {
//                      NICK=@"N/A";
//                      [a addObject:NICK];
//                  }
//                  else
//                  {
//                      [a addObject:NICK];
//                  }
//
//                  NSString* DUE_DATE=[dic_ISP objectForKey:@"DUE_DATE"];
//                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
//                  {
//                      DUE_DATE=@"N/A";
//                      [a addObject:DUE_DATE];
//                  }
//                  else
//                  {
//                      [a addObject:DUE_DATE];
//                  }
//
//                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"PAYABLE_AMOUNT"]];
//                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
//                  {
//                      PAYABLE_AMOUNT=@"N/A";
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//                  else
//                  {
//                      [a addObject:PAYABLE_AMOUNT];
//                  }
//
//                  NSString* PAYMENT_STATUS_DESC=[dic_ISP objectForKey:@"PAYMENT_STATUS_DESC"];
//                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS_DESC=@"N/A";
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS_DESC];
//                  }
//
//                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"BILL_ID"]];
//                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
//                  {
//                      BILL_ID=@"N/A";
//                      [a addObject:BILL_ID];
//                  }
//                  else
//                  {
//                      [a addObject:BILL_ID];
//                  }
//
//                  NSString* ACCESS_KEY=[dic_ISP objectForKey:@"ACCESS_KEY"];
//                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
//                  {
//                      ACCESS_KEY=@"N/A";
//                      [a addObject:ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:ACCESS_KEY];
//                  }
//
//                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"REGISTERED_CONSUMERS_ID"]];
//                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
//                  {
//                      REGISTERED_CONSUMERS_ID=@"N/A";
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_CONSUMERS_ID];
//                  }
//
//                  NSString* COMPANY_NAME=[dic_ISP objectForKey:@"COMPANY_NAME"];
//                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
//                  {
//                      COMPANY_NAME=@"N/A";
//                      [a addObject:COMPANY_NAME];
//                  }
//                  else
//                  {
//                      [a addObject:COMPANY_NAME];
//                  }
//
//                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"REGISTERED_ACCOUNT_ID"]];
//
//
//                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
//                  {
//                      REGISTERED_ACCOUNT_ID=@"N/A";
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//                  else
//                  {
//                      [a addObject:REGISTERED_ACCOUNT_ID];
//                  }
//
//
//                  NSString* TC_ACCESS_KEY=[dic_ISP objectForKey:@"TC_ACCESS_KEY"];
//                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
//                  {
//                      TC_ACCESS_KEY=@"N/A";
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//                  else
//                  {
//                      [a addObject:TC_ACCESS_KEY];
//                  }
//
//                  NSString* PAYMENT_STATUS4=[dic_ISP objectForKey:@"PAYMENT_STATUS"];
//                  if (PAYMENT_STATUS4.length==0 || [PAYMENT_STATUS4 isEqualToString:@" "] || [PAYMENT_STATUS4 isEqualToString:nil])
//                  {
//                      PAYMENT_STATUS4=@"N/A";
//                      [a addObject:PAYMENT_STATUS4];
//                  }
//                  else
//                  {
//                      [a addObject:PAYMENT_STATUS4];
//                  }
//
//                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic_ISP objectForKey:@"IS_PAYBLE"]];
//                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
//                  {
//                      IS_PAYBLE=@"N/A";
//                      [a addObject:IS_PAYBLE];
//                  }
//                  else
//                  {
//                      [a addObject:IS_PAYBLE];
//                  }
//
//
//                  NSString* bbb = [a componentsJoinedByString:@"|"];
//                  //NSLog(@"%@", bbb);
//                  [gblclass.arr_bill_isp addObject:bbb];
//                  //  [arr_isp addObject:bbb];
//                  [a removeAllObjects];
//
//              }
              
//              }
              // ISP End
              
              [hud hideAnimated:YES];
              [table reloadData];
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@""];
              
          }];
        }
        @catch (NSException *exception)
           {
               [hud hideAnimated:YES];
               [self custom_alert:@"Please try again later." :@"0"];
           }
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
        
    }
    
    
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        //[self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window. layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        
        gblclass.indexxx=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        CATransition *transition = [ CATransition animation];
        NSString* bbb;
        
        if (indexPath.section == 0)
        {
            
        }
        
        switch (indexPath.section)
        {
            case 0:
                
//                NSLog(@"%@",gblclass.arr_mobiletopup);
                split_bill = [[NSArray alloc] init];
                gblclass.arr_bill_load_all = [[NSMutableArray alloc] init];
                
                gblclass.bill_type_chck = @"0";
                
                split_bill = [[gblclass.arr_mobiletopup objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
            //    a = [[NSMutableArray alloc] init];
                 
                [gblclass.arr_bill_load_all removeAllObjects];
//
                [a addObject:[split_bill objectAtIndex:1]];
                [a addObject:[split_bill objectAtIndex:8]];
                [a addObject:[split_bill objectAtIndex:10]];
                [a addObject:[split_bill objectAtIndex:2]];
                [a addObject:[split_bill objectAtIndex:0]];
                [a addObject:[split_bill objectAtIndex:15]];
                [a addObject:[split_bill objectAtIndex:3]];
                [a addObject:[split_bill objectAtIndex:8]]; // TT access key
                [a addObject:[split_bill objectAtIndex:9]]; // registered id
//
                bbb = [a componentsJoinedByString:@"|"];
                [gblclass.arr_bill_load_all addObject:bbb];
                [a removeAllObjects];
                
//                NSLog(@"%@",gblclass.arr_bill_load_all);
                
//                [gblclass.arr_bill_load_all addObject:[split_bill objectAtIndex:1]]; //tt_id
//                [gblclass.arr_bill_load_all addObject:[split_bill objectAtIndex:8]]; //access key
//                [gblclass.arr_bill_load_all addObject:[split_bill objectAtIndex:10]]; //company name
//                [gblclass.arr_bill_load_all addObject:[split_bill objectAtIndex:2]]; //customer id
                
                
          //saki 31 jan 2020      split_bill = [[gblclass.arr_bill_load_all objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                 
                //                if ([[split_bill objectAtIndex:13] isEqualToString:@"U"] && [[split_bill objectAtIndex:14] isEqualToString:@"0"])
                //                {
                //
                //                }
                //                else
                //                {
                
//                utility_bill
                
                transition.duration = 0.3;
                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [self.view.window.layer addAnimation:transition forKey:nil];
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"]; //utility_bill   //bill_all
                [self presentViewController:vc animated:NO completion:nil];
                //                }
                
                break;
                
            case 1:
                //Mobile
                
                gblclass.bill_type_chck = @"1";
//                NSLog(@"%@",gblclass.arr_mobiletopup);
                split_bill = [[NSArray alloc] init];
                gblclass.arr_bill_load_all = [[NSMutableArray alloc] init];
                
                split_bill = [[gblclass.arr_internet objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                //    a = [[NSMutableArray alloc] init];
                
                [gblclass.arr_bill_load_all removeAllObjects];
                //
                [a addObject:[split_bill objectAtIndex:1]];
                [a addObject:[split_bill objectAtIndex:8]];
                [a addObject:[split_bill objectAtIndex:10]];
                [a addObject:[split_bill objectAtIndex:2]];
                [a addObject:[split_bill objectAtIndex:0]];
                [a addObject:[split_bill objectAtIndex:15]];
                [a addObject:[split_bill objectAtIndex:3]];
                [a addObject:[split_bill objectAtIndex:8]]; // TT access key
                [a addObject:[split_bill objectAtIndex:9]]; // registered id
                //
                bbb = [a componentsJoinedByString:@"|"];
                [gblclass.arr_bill_load_all addObject:bbb];
                [a removeAllObjects];
                
//                NSLog(@"%@",gblclass.arr_bill_load_all);
                
                transition.duration = 0.3;
                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [self.view.window.layer addAnimation:transition forKey:nil];
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 2:
                
                gblclass.bill_type_chck = @"1";
//                NSLog(@"%@",gblclass.arr_mobiletopup);
                split_bill = [[NSArray alloc] init];
                gblclass.arr_bill_load_all = [[NSMutableArray alloc] init];
                
                split_bill = [[gblclass.arr_postpaid objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                //    a = [[NSMutableArray alloc] init];
                
                [gblclass.arr_bill_load_all removeAllObjects];
                //
                [a addObject:[split_bill objectAtIndex:1]];
                [a addObject:[split_bill objectAtIndex:8]];
                [a addObject:[split_bill objectAtIndex:10]];
                [a addObject:[split_bill objectAtIndex:2]];
                [a addObject:[split_bill objectAtIndex:0]];
                [a addObject:[split_bill objectAtIndex:15]];
                [a addObject:[split_bill objectAtIndex:3]];
                [a addObject:[split_bill objectAtIndex:8]]; // TT access key
                [a addObject:[split_bill objectAtIndex:9]]; // registered id
                //
                bbb = [a componentsJoinedByString:@"|"];
                [gblclass.arr_bill_load_all addObject:bbb];
                [a removeAllObjects];
                
//                NSLog(@"%@",gblclass.arr_bill_load_all);
                
                transition.duration = 0.3;
                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [self.view.window.layer addAnimation:transition forKey:nil];
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 3:
                
                transition.duration = 0.3;
                transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromRight;
                [self.view.window.layer addAnimation:transition forKey:nil];
                
                gblclass.direct_pay_frm_Acctsummary=@"0";
                [[gblclass.arr_bill_ublbp objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"UBP_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            default: break;
        }
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//      return table_header;
//    //return animalIndexTitles;
//}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [table_header indexOfObject:title];  //[animalIndexTitles indexOfObject:title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:  (NSInteger)section
{
    switch (section)
    {
        case 0: //Mobile Topup
            return [gblclass.arr_mobiletopup count]; // [gblclass.arr_bill_load_all count];
            break;
            
        case 1: //Internet
            return [gblclass.arr_internet count];  //[gblclass.arr_bill_ob count];
            break;
            
        case 2: //Postpaid
            return  [gblclass.arr_postpaid count]; // [gblclass.arr_bill_isp count];
            break;
            
//        case 3: //UBL Bill
//            return [gblclass.arr_bill_ublbp count];
//            break;
//
        default:
            return 0;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [table_header count];//numberOfSections;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //  UIButton *addButton;
    
    switch (section)
    {
        case 0:
            
            return [table_header objectAtIndex:section];
            
            break;
        case 1:
            
            return [table_header objectAtIndex:section];
            
            break;
            
        case 2:
            
            return [table_header objectAtIndex:section];
            
            break;
            
//        case 3:
//
//            return [table_header objectAtIndex:section];
//
//            break;
            
        default:
            
            return 0;
    }
    
    //    CGRect frame = tableView.frame;
    //
    //    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-60, 10, 50, 30)];
    //    addButton.titleLabel.text = @"BUTTONS";
    //    addButton.backgroundColor = [UIColor redColor];
    //
    //    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
    //    title.text = @"Reminders";
    //
    //    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    //    [headerView addSubview:title];
    //    [headerView addSubview:addButton];
    //
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(320.0, 0.0, 320.0, 20.0)];
    //  myView.backgroundColor=[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0];
    
    myView.backgroundColor= [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    UIButton *button1 = [[UIButton alloc]init];
    //buttonWithType:UIButtonTypeContactAdd];
    
    //***** 28 aug [button1 setFrame:CGRectMake(230.0, 9.0, 70.0, 20.0)];
    
    button1.titleLabel.text=@"Add Bill";
    //   button.titleLabel.font=[UIImage imageNamed:@"Debit.png"];//[UIColor redColor];
    button1.tag = section;
    button1.hidden = NO;
    
   
//    UIImage *buttonimage;
//
//    if (section==0)
//    {
//        [button1 setFrame:CGRectMake(220.0, 9.0, 80.0, 20.0)];
//        buttonimage= [UIImage imageNamed:@"Add_Utility_Bill.png"];
//        [button1 setBackgroundImage:buttonimage forState:UIControlStateNormal];
//    }
//    else if(section==1)
//    {
//        [button1 setFrame:CGRectMake(220.0, 9.0, 80.0, 20.0)];
//        buttonimage = [UIImage imageNamed:@"Add_Mobile_Bill.png"];
//        [button1 setBackgroundImage:buttonimage forState:UIControlStateNormal];
//    }
//    else if(section==2)
//    {
//        [button1 setFrame:CGRectMake(230.0, 9.0, 70.0, 20.0)];
//        buttonimage = [UIImage imageNamed:@"Add_ISP_Bill.png"];
//        [button1 setBackgroundImage:buttonimage forState:UIControlStateNormal];
//    }
//    else if(section==3)
//    {
//        [button1 setFrame:CGRectMake(220.0, 9.0, 80.0, 20.0)];
//        buttonimage = [UIImage imageNamed:@"Add_ubl"];
//        [button1 setBackgroundImage:buttonimage forState:UIControlStateNormal];
//    }
//
//
//    [button1 addTarget:self action:@selector(Add_bill_click:) forControlEvents:UIControlEventTouchDown];
//    [myView addSubview:button1];
    
    
    
    
    //    UIImage *myImage = [UIImage imageNamed:@"Add-bill.png"];
    //    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    //    imageView.frame = CGRectMake(230.0, 9.0, 70.0, 20.0);
    
    
    //  [myView addSubview:imageView];
    
    
    
    UILabel *label1 = [[UILabel alloc] init];
    [label1 setFrame:CGRectMake(15.0, 5.0, 180.0, 30.0)];
    label1.text= [table_header objectAtIndex:section];
    label1.textColor=[UIColor blackColor];
    label1.font=[UIFont systemFontOfSize:14];
    label1.tag = section;
    label1.hidden = NO;
    [label1 setBackgroundColor:[UIColor clearColor]];
    // [button1 addTarget:self action:@selector(insertParameter:) forControlEvents:UIControlEventTouchDown];
    [myView addSubview:label1];
    
    return myView;
    //   }
    
    //    // create the parent view that will hold header Label
    //    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 44.0)];
    //
    //    // create the button object
    //    UIButton * headerBtn = [[UIButton alloc] initWithFrame:CGRectZero];
    //    headerBtn.backgroundColor = [UIColor clearColor];
    //    headerBtn.opaque = NO;
    //    headerBtn.frame = CGRectMake(10.0, 0.0, 100.0, 30.0);
    //    [headerBtn setTitle:@"<Put here whatever you want to display>" forState:UIControlEventTouchUpInside];
    //    [headerBtn addTarget:self action:@selector(ActionEventForButton:) forControlEvents:UIControlEventTouchUpInside];
    //    [customView addSubview:headerBtn];
    //
    //    return customView;
    
}


-(IBAction)btn_add_new_bill:(id)sender
{
    //billmanagegment  ublbillmanagement
    gblclass.bill_type = @"Add Bill";
    gblclass.bill_click = @"Utility Bills";
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"billmanagegment"];
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)Add_bill_click:(id)sender
{
    UIButton *allButton = (UIButton*)sender;
    allButton.selected = !allButton.selected;
    
    //NSLog(@"Button Click :: %ld", (long)[sender tag]);
    
    
    //NSLog(@"%@",[table_header objectAtIndex:[sender tag]]);
    
    
    //    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    [self presentModalViewController:vc animated:YES];
    
    gblclass.bill_click=[table_header objectAtIndex:[sender tag]];
    gblclass.bill_type=[NSString stringWithFormat:@"Add %@",[table_header objectAtIndex:[sender tag]]];
    
    if ([gblclass.bill_click isEqualToString:@"UBL Bills"])
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"billmanagegment"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (indexPath.section==0)
    {
      //  split_bill = [[gblclass.arr_bill_load_all objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        split_bill = [[gblclass.arr_mobiletopup objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        label=(UILabel*)[cell viewWithTag:1];
        label.text = [split_bill objectAtIndex:3]; //3
        //label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:2]; //3
        //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [NSString stringWithFormat:@"%@ - %@",[split_bill objectAtIndex:10],[split_bill objectAtIndex:0]]; //3
        //         label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        NSString *numberString = [split_bill objectAtIndex:5];
        NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //        //Balc ::
        //        label=(UILabel*)[cell viewWithTag:4];
        //        label.text = [numberFormatter stringFromNumber:number];
        //        label.textColor=[UIColor whiteColor];
        //        label.font=[UIFont systemFontOfSize:18];
        //        [cell.contentView addSubview:label];
        
        
//         Balance ::
//        bill_price=0.00;
//        label=(UILabel*)[cell viewWithTag:4];
//        //        bill_price=[[split_bill objectAtIndex:5] doubleValue];
//        label.text =[NSString stringWithFormat:@"PKR %@", [numberFormatter stringFromNumber:number]];//3
//        label.font=[UIFont systemFontOfSize:14];
//        [cell.contentView addSubview:label];
        
        
        //for Image ::
        
//        UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
//
//        if ([[split_bill objectAtIndex:6] isEqualToString:@"PAID"])
//        {
//            img_name=@"Paid.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"-"])
//        {
//            img_name=@"paid-.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"IN PROCESS"])
//        {
//            img_name=@"in_process.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:13] isEqualToString:@"U"] && [[split_bill objectAtIndex:14] isEqualToString:@"0"])
//        {
//            img_name=@"not_payble.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else
//        {
//            img_name=@"Unpaid.png";
//
//            //                        string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Due Date : %@",[split_bill objectAtIndex:4]]];
//            //                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,5)];
//            //                        [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5,15)];
//            //                        label.attributedText = string;
//
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor redColor];
//            [cell.contentView addSubview:label];
//        }
//
//        imv.image=[UIImage imageNamed:img_name];
//        [cell.contentView addSubview:imv];
        
        
    }
    else if (indexPath.section==1)
    {
     //   split_bill = [[gblclass.arr_bill_ob objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
       
        split_bill = [[gblclass.arr_internet objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        label=(UILabel*)[cell viewWithTag:1];
        label.text = [split_bill objectAtIndex:3]; //3
        //label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:2]; //3
        //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:3];
        label.text =[NSString stringWithFormat:@"%@ - %@",[split_bill objectAtIndex:10],[split_bill objectAtIndex:0]]; //3
        //  label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor blackColor];
        [cell.contentView addSubview:label];
        
//        bill_price=0.00;
//        label=(UILabel*)[cell viewWithTag:4];
//        bill_price=[[split_bill objectAtIndex:5] doubleValue];
//        label.text =@"";    //[NSString stringWithFormat:@"%.2f", bill_price]; //3
//        //label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
        
        
        //for Image ::
        
//        UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
//
//        if ([[split_bill objectAtIndex:6] isEqualToString:@"PAID"])
//        {
//            img_name=@"Paid.png";
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"-"])
//        {
//            img_name=@"paid-.png";
//
//
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"IN PROCESS"])
//        {
//            img_name=@"in_process.png";
//
//
//        }
//
//        else
//        {
//            img_name=@"Unpaid.png";
//        }
//        imv.image=[UIImage imageNamed:img_name];
//        [cell.contentView addSubview:imv];
        
    }
    else if (indexPath.section==2)
    {
     //   split_bill = [[gblclass.arr_bill_isp objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
       
        split_bill = [[gblclass.arr_postpaid objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        label=(UILabel*)[cell viewWithTag:1];
        label.text = [split_bill objectAtIndex:3]; //3
        //label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split_bill objectAtIndex:2]; //3
        //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [NSString stringWithFormat:@"%@ - %@",[split_bill objectAtIndex:10],[split_bill objectAtIndex:0]]; //3
        //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        // bill_price=@"";
//        label=(UILabel*)[cell viewWithTag:4];
//        bill_price=[[split_bill objectAtIndex:5] doubleValue];
//        label.text =@"";    //3
//        //label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
//
        
        //for Image ::
        
//        UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
//
//        if ([[split_bill objectAtIndex:6] isEqualToString:@"PAID"])
//        {
//            img_name=@"Paid.png";
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"-"])
//        {
//            img_name=@"paid-.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"IN PROCESS"])
//        {
//            img_name=@"in_process.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//
//        else
//        {
//            img_name=@"Unpaid.png";
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor redColor];
//            [cell.contentView addSubview:label];
//        }
//        imv.image=[UIImage imageNamed:img_name];
//        [cell.contentView addSubview:imv];
//
    }
        
        
        
//    else if (indexPath.section==3)
//    {
//        split_bill = [[gblclass.arr_bill_ublbp objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
//
//
//        label=(UILabel*)[cell viewWithTag:1];
//        label.text = [split_bill objectAtIndex:3]; //3
//        //label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
//
//        label=(UILabel*)[cell viewWithTag:2];
//        label.text = [split_bill objectAtIndex:10]; //3
//        //  label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
//
//        label=(UILabel*)[cell viewWithTag:3];
//        label.text = [NSString stringWithFormat:@"Loan No. %@",[split_bill objectAtIndex:2]]; //3
//        //  label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
//
//        bill_price=0.00;
//        label=(UILabel*)[cell viewWithTag:4];
//        bill_price=[[split_bill objectAtIndex:5] doubleValue];
//        label.text =@"";        //[NSString stringWithFormat:@"%.2f", bill_price]; //3
//        //label.font=[UIFont systemFontOfSize:12];
//        [cell.contentView addSubview:label];
//
//
//        //for Image ::
//
//        UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
//
//        if ([[split_bill objectAtIndex:6] isEqualToString:@"PAID"])
//        {
//            img_name=@"Paid.png";
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"-"])
//        {
//            img_name=@"paid-.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//        else if ([[split_bill objectAtIndex:6] isEqualToString:@"IN PROCESS"])
//        {
//            img_name=@"in_process.png";
//
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//
//        else
//        {
//            img_name=@"Unpaid.png";
//            label=(UILabel*)[cell viewWithTag:3];
//            label.textColor=[UIColor blackColor];
//            [cell.contentView addSubview:label];
//        }
//
//        imv.image=[UIImage imageNamed:img_name];
//        [cell.contentView addSubview:imv];
//
//    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(IBAction)btn_combo_frm:(id)sender
{
    if (table_from.isHidden==YES)
    {
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
    }
}

-(IBAction)btn_bill_names:(id)sender
{
    if (table_to.isHidden==YES)
    {
        vw_to.hidden=NO;
        table_to.hidden=NO;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        [table_to bringSubviewToFront:self.view];
    }
    else
    {
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];
    [self presentViewController:vc animated:NO completion:nil];
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}
 

-(IBAction)btn_account:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self Get_Bill:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self Get_Bill:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@""];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 :(NSString*)icons
{
    gblclass.custom_alert_msg = msg1;
    gblclass.custom_alert_img = icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end

