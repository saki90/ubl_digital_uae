//
//  ParkPlaceMark.h
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface ParkPlaceMark : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    NSNumber *companyID;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property(nonatomic, copy) NSNumber *companyID;
-(id)initWithCoordinate:(CLLocationCoordinate2D) c;



@end
