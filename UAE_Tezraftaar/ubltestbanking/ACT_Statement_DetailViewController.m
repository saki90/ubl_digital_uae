//
//  ACT_Statement_DetailViewController.m
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "ACT_Statement_DetailViewController.h"


@interface ACT_Statement_DetailViewController ()
{
    NSString *Yourstring;
    NSArray *arr_act_type;
}
@end

@implementation ACT_Statement_DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arr_act_type=[[NSArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    
    cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Yourstring=[arr_act_type objectAtIndex:indexPath.row];
    
    NSLog(@"Selected item %@ ",Yourstring);
    
    
    
    [self performSegueWithIdentifier:@"detail" sender:self];
    
    
}


@end
