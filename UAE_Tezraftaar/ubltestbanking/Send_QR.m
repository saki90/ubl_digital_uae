//
//  Send_QR.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Send_QR.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>



@interface Send_QR ()<AVCaptureMetadataOutputObjectsDelegate>
{
    UIStoryboard *storyboard;
    UIViewController *vc;
    
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    GlobalStaticClass* gblclass;
    
    APIdleManager* timer_class;
    
    NSString *detectionString;
    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code,*str_crc;
    
    NSString* optional_chck;
    UIImageView *bg_img;
    
}
@end

@implementation Send_QR
@synthesize transitionController;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    @try {
        
        
        gblclass=[GlobalStaticClass getInstance];
        detectionString = nil;
        gblclass.arr_qr_code =[[NSMutableArray alloc] init];
        
        
        str_crc = @"";
        gblclass.chck_qr_send=@"0";
        
        
        //    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
        //    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        //    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
        //    [_viewPreview.layer addSublayer:_videoPreviewLayer];
        
        gblclass.arr_qr_code=[[NSMutableArray alloc] init];
        
        _highlightView = [[UIView alloc] init];
        _highlightView.autoresizingMask =  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        
        // _highlightView.autoresizingMask=uiviewautosizingf
        _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
        //  _highlightView.frame  = CGRectMake(0, 50, 320, 360);
        _highlightView.layer.borderWidth = 3;
        [self.view addSubview:_highlightView];
        
        
        
        _label = [[UILabel alloc] init];
        _label.frame = CGRectMake(0, self.view.bounds.size.height - 80, self.view.bounds.size.width, 80);
        _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = NSTextAlignmentCenter;
        // _label.text = @"(none)";
        
        //**    [self.view addSubview:_label];
        
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        CGFloat heights = 0.0;
        
        
        bg_img =[[UIImageView alloc] initWithFrame:CGRectMake(0,60,width,height-140)];
        //    bg_img.frame = CGRectMake(0, self.view.bounds.size.height - 80, self.view.bounds.size.width, 80);
        bg_img.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
        bg_img.image=[UIImage imageNamed:@"Cemra_g1.png"];
        [self.view addSubview:bg_img];
        
        
        
        _session = [[AVCaptureSession alloc] init];
        _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSError *error = nil;
        _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
        if (_input)
        {
            [_session addInput:_input];
        }
        else
        {
            //NSLog(@"Error: %@", error);
            
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Allow UBL Digital App to access your Camera in your Phone Settings"
                                                            message: @"This will enable you to scan QR codes on the app"
                                                           delegate:nil                                             cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            [alert1 show];
            
            return;
        }
        
        _output = [[AVCaptureMetadataOutput alloc] init];
        [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [_session addOutput:_output];
        
        _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
        
        _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
        //    _prevLayer.frame = self.view.bounds;
        
        
        
        _prevLayer.frame = CGRectMake(0, 60, 320, 430);
        //_prevLayer.videoGravity=AVLayerVideoGravityResize;
        _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        [self.view.layer addSublayer:_prevLayer];
        
        [_session startRunning];
        
        [self.view bringSubviewToFront:_highlightView];
        [self.view bringSubviewToFront:bg_img];
        
        
//        [APIdleManager sharedInstance].onTimeout = ^(void){
//            //  [self.timeoutLabel  setText:@"YES"];
//            
//            
//            //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//            
//            storyboard = [UIStoryboard storyboardWithName:
//                          gblclass.story_board bundle:[NSBundle mainBundle]];
//            vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//            
//            
//            [self presentViewController:vc animated:YES completion:nil];
//            
//            
//            APIdleManager * cc1=[[APIdleManager alloc] init];
//            // cc.createTimer;
//            
//            [cc1 timme_invaletedd];
//            
//        };
        
    }
    @catch (NSException *exception)
    {
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention"
                                                        message: exception.reason
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert1 show];
        
    }
    
    self.transitionController = [[TransitionDelegate alloc] init];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    lbl_amnt.text=[NSString stringWithFormat:@"Scan the code to send Rs. %@ from your account.",gblclass.scan_qr_amnt];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([gblclass.camera_back isEqualToString: @"1"]) {
        
        detectionString = nil;
        gblclass.arr_qr_code =[[NSMutableArray alloc] init];
        
        str_crc = @"";
        gblclass.chck_qr_send=@"0";
        
        
        _session = [[AVCaptureSession alloc] init];
        _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSError *error = nil;
        
        _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
        if (_input)
        {
            [_session addInput:_input];
        }
        else
        {
            //NSLog(@"Error: %@", error);
            
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention"
                                                            message: error.localizedDescription
                                                           delegate:nil                                             cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            [alert1 show];
            return;
        }
        
        _output = [[AVCaptureMetadataOutput alloc] init];
        [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [_session addOutput:_output];
        
        _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
        
        _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
        //    _prevLayer.frame = self.view.bounds;
        
        
        
        _prevLayer.frame = CGRectMake(0, 60, 320, 430);
        //_prevLayer.videoGravity=AVLayerVideoGravityResize;
        _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        [self.view.layer addSublayer:_prevLayer];
        
        [_session startRunning];
        
        [self.view bringSubviewToFront:_highlightView];
        [self.view bringSubviewToFront:bg_img];
        
        gblclass.camera_back = @"0";
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
  //  [self dismissModalStack];
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    
    //    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
    //            AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
    //            AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    
    NSArray *barCodeTypes = @[AVMetadataObjectTypeQRCode];
    
    for (AVMetadataObject *metadata in metadataObjects)
    {
        for (NSString *type in barCodeTypes)
        {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                
                highlightViewRect = barCodeObject.bounds;
                
                
                
                //NSLog(@"Bounds: %@", NSStringFromCGRect(barCodeObject.bounds));
                //NSLog(@"Frame: %@", barCodeObject.corners);
                
                //Update Laser
                //***       _highlightView.frame = highlightViewRect;
                detectionString = nil;
                
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                
                
                //after login
                
                
                if ([@"000201" isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,6)]])
                {
                    [self Extract_Raw_Data_MasterPass_New];
                }
                else if ([@"00" isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,2)]])
                {
                    [self Extract_Raw_Data_MasterPass];
                }
                else
                {
                    [self Extract_Raw_Data];
                }
                
                
                
                //  [self Extract_Raw_Data];
                
                break;
            }
        }
        
        if (detectionString != nil)
        {
            _label.text = detectionString;
            
            break;
        }
        // else
        //  _label.text = @"(none)";
    }
    
    //****  _highlightView.frame = highlightViewRect;
    
    // Stop video capture and make the capture session object nil.
    //     [_session stopRunning];
    //     _session = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    //     [_prevLayer removeFromSuperlayer];
    //     _highlightView.hidden=YES;
}




-(void)Extract_Raw_Data_MasterPass
{
    
    //    NSError* error = nil;
    
    @try {
        
        NSString* SEP_PAN = @"00";
        NSString* SEP_MOBILE=@"01";
        NSString* SEP_ACCOUNT= @"08";
        NSString* SEP_MERCHANT_NAME=@"0A";
        NSString* SEP_MERCHANT_CATEGORY_CODE= @"0B";
        NSString* SEP_MERCHANT_CITY=@"0C";
        NSString* SEP_MERCHANT_COUNTRY_CODE=@"0D";
        NSString* SEP_MERCHANT_CURRENCY_CODE=@"0E";
        
        
        //    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code;
        
        
        
        
        //       00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
        
        
        //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
        //     PAN   account               Mobile#        name     code        city   cuntry cod   curr code
        
        
        
        
        int bb=0;
        if ([_label.text length]>0)
        {
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,2)]);
            if ([SEP_PAN isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,2)]])
            {
                //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(20,2)]); // For Mobile ide
                
                
                if ([SEP_ACCOUNT isEqualToString:[detectionString  substringWithRange:NSMakeRange(2,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(2,2)]); // FOr Account
                    
                    int calc;
                    int b = [[detectionString  substringWithRange:NSMakeRange(2,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    bb=2;
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    str_Accout_no=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // FOr Account
                    
                    bb=bb+calc;
                }
                
                if ([SEP_MOBILE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //                   NSNumber* num;
                    int calc;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // For Mobile
                    
                    bb=bb+2;
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                        
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);  // For Mobile
                    
                    str_Mobile=[detectionString  substringWithRange:NSMakeRange(bb,calc-1)];  // For Mobile
                    
                    
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    bb=bb+calc;
                    
                }
                
                
                if ([SEP_MERCHANT_NAME isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    bb=bb+2;
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,b)]); // FOr Name
                    
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    bb=bb+2;
                    str_merchant_name=[detectionString  substringWithRange:NSMakeRange(bb,b)]; // For Name
                    bb=bb+b;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                if ([SEP_MERCHANT_CATEGORY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);// FOr Category Code
                    //                    NSNumber* num;
                    int calc;
                    
                    bb=bb+2;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    
                    bb=bb+2;
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    str_category_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                    
                    bb=bb+calc;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                //00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
                
                if ([SEP_MERCHANT_CITY isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    //bb=bb+2;
                    int c = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    bb=bb+2;
                    str_city=[detectionString  substringWithRange:NSMakeRange(bb,c)]; // City
                    
                    bb=bb+c;
                }
                
                if ([SEP_MERCHANT_COUNTRY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Country Code
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int c = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    bb=bb+2;
                    str_country_code=[detectionString  substringWithRange:NSMakeRange(bb,c)]; // Country Code
                    
                    bb=bb+c;
                }
                if ([SEP_MERCHANT_CURRENCY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Currency Code
                    
                    //                   NSNumber* num;
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    bb=bb+2;
                    str_curr_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                }
                
                
                
                //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
                //       PAN   account               Mobile#        name     code        city   cuntry cod   curr code
                
            }
            
            //        NSString* SEP_PAN = @"00";
            //             NSString* SEP_MOBILE = @"01";
            //             NSString* SEP_ACCOUNT = @"08";
            //           //NSString* SEP_RFU = {"03","04","05","06","07","08","09"};
            //             NSString* SEP_MERCHANT_NAME = @"0A";
            //             NSString* SEP_MERCHANT_CATEGORY_CODE = @"0B";
            //             NSString* SEP_MERCHANT_CITY = @"0C";
            //             NSString* SEP_MERCHANT_COUNTRY_CODE = @"0D";
            //             NSString* SEP_MERCHANT_CURRENCY_CODE = @"0E";
            
            
            
            
            //NSLog(@"%@",str_Mobile);
            //NSLog(@"%@",str_Accout_no);
            //NSLog(@"%@",str_merchant_name);
            //NSLog(@"%@",str_category_code);
            //NSLog(@"%@",str_city);
            //NSLog(@"%@",str_country_code);
            //NSLog(@"%@",str_curr_code);
            
            
            [gblclass.arr_qr_code addObject:str_Mobile];
            [gblclass.arr_qr_code addObject:str_Accout_no];
            [gblclass.arr_qr_code addObject:str_merchant_name];
            [gblclass.arr_qr_code addObject:str_category_code];
            [gblclass.arr_qr_code addObject:str_city];
            [gblclass.arr_qr_code addObject:str_country_code];
            [gblclass.arr_qr_code addObject:str_curr_code];
            [gblclass.arr_qr_code addObject:str_crc];
            [gblclass.arr_qr_code addObject:detectionString];
            
            // Stop video capture and make the capture session object nil.
            [_session stopRunning];
            _session = nil;
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"QR_Code_Master"]; //QR_Code_Master  QR_Code_Detail_Instant1
            [self presentViewController:vc animated:NO completion:nil];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
        // NSError* err;
        
        // [hud hideAnimated:YES];
        
        //         //NSLog(@"%@", [error localizedDescription]);
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Incorrect BarCode"
        //                                                       delegate:self
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil, nil];
        //
        //        [alert show];
    }
    
}






-(void)Extract_Raw_Data_MasterPass_New
{
    
    //    NSError* error = nil;
    
    @try {
        
        //        NSString* SEP_PAN = @"00";
        //        NSString* SEP_MOBILE=@"01";
        //        NSString* SEP_ACCOUNT= @"08";
        //        NSString* SEP_MERCHANT_NAME=@"0A";
        //        NSString* SEP_MERCHANT_CATEGORY_CODE= @"0B";
        //        NSString* SEP_MERCHANT_CITY=@"0C";
        //        NSString* SEP_MERCHANT_COUNTRY_CODE=@"0D";
        //        NSString* SEP_MERCHANT_CURRENCY_CODE=@"0E";
        
        
        
        NSString* SEP_PAN = @"000201";
        NSString* SEP_MOBILE=@"010211";
        NSString* SEP_Virtual_Card_Number= @"04";
        NSString* SEP_MERCHANT_NAME=@"59";
        NSString* SEP_MERCHANT_CATEGORY_CODE= @"52";
        NSString* SEP_MERCHANT_CITY=@"60";
        NSString* SEP_MERCHANT_COUNTRY_CODE=@"58";
        NSString* SEP_MERCHANT_CURRENCY_CODE=@"53";
        NSString* SEP_CRC=@"63";
        
        //  QRString: 00020101021104155245210218002275204581253035865802PK5918S. Mansor Ul Hasan6006Lahore6211030710005836304CE38
        
        
        int bb=0;
        if ([_label.text length]>0)
        {
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,2)]);
            if ([SEP_PAN isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,6)]])
            {
                
                NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(12,2)]);
                if ([SEP_Virtual_Card_Number isEqualToString:[detectionString  substringWithRange:NSMakeRange(12,2)]])
                {
                    
                    int calc;
                    int b = [[detectionString  substringWithRange:NSMakeRange(14,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    bb=b;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(14,b)]);
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(14,b)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b+2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    
                    bb=bb+b;
                    calc = b;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    str_Accout_no=[detectionString  substringWithRange:NSMakeRange(16,calc)]; // FOr Account
                    
                    
                    if ([str_Accout_no length] == 15 )
                    {
                        str_Accout_no = [NSString stringWithFormat:@"%@%@",str_Accout_no,@"0"];
                    }
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    bb=16+calc;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                }
                
                
                if ([SEP_MERCHANT_CATEGORY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);// FOr Category Code
                    //                    NSNumber* num;
                    int calc;
                    
                    bb=bb+2;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc = b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    
                    bb=bb+2;
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    str_category_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                    
                    bb=bb+calc;
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                
                if ([SEP_MERCHANT_CURRENCY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Currency Code
                    
                    //                   NSNumber* num;
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    bb=bb+2;
                    str_curr_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                    
                    bb=bb+calc;
                }
                
                
                if ([SEP_MERCHANT_COUNTRY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Country Code
                    
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    str_country_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // Country Code
                    
                    bb=bb+calc;
                }
                
                if ([SEP_MERCHANT_NAME isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    bb=bb+2;
                    str_merchant_name=[detectionString  substringWithRange:NSMakeRange(bb,b)]; // For Name
                    
                    bb=bb+calc;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                if ([SEP_MERCHANT_CITY isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    str_city=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // City
                    
                    bb=bb+calc;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    optional_chck = [detectionString  substringWithRange:NSMakeRange(bb,2)];
                    
                }
                
                
                if ([optional_chck isEqualToString:@"57"])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    //    str_city=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // City
                    
                    bb=bb+calc;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    optional_chck = [detectionString  substringWithRange:NSMakeRange(bb,2)];
                }
                
                if ([optional_chck isEqualToString:@"61"])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    //  str_city=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // City
                    
                    bb=bb+calc;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    optional_chck = [detectionString  substringWithRange:NSMakeRange(bb,2)];
                }
                
                if ([optional_chck isEqualToString:@"62"])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    //  str_city=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // City
                    
                    bb=bb+calc;
                    
                    NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    optional_chck = [detectionString  substringWithRange:NSMakeRange(bb,2)];
                }
                
                
                
                if ([SEP_CRC isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        calc=b;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    bb=bb+2;
                    str_crc=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // City
                    
                    bb=bb+calc;
                }
                
                
                
                //                if ([SEP_MOBILE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                //                {
                //                    //                   NSNumber* num;
                //                    int calc;
                //
                //                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // For Mobile
                //
                //                    bb=bb+2;
                //                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                //                    //NSLog(@"%d",b);
                //
                //
                //                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                //                    {
                //                        // newString consists only of the digits 0 through 9
                //                        calc=b*2;
                //
                //                    }
                //                    else
                //                    {
                //                        //NSLog(@"Not Digit");
                //                    }
                //
                //                    //NSLog(@"%d",calc);
                //
                //                    bb=bb+2;
                //                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);  // For Mobile
                //
                //                    str_Mobile=[detectionString  substringWithRange:NSMakeRange(bb,calc-1)];  // For Mobile
                //
                //
                //
                //                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                //
                //                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                //
                //                    bb=bb+calc;
                //
                //                }
                
                
                
                
                //00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
                
                
                //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
                //       PAN   account               Mobile#        name     code        city   cuntry cod   curr code
                
            }
            
            //        NSString* SEP_PAN = @"00";
            //             NSString* SEP_MOBILE = @"01";
            //             NSString* SEP_ACCOUNT = @"08";
            //           //NSString* SEP_RFU = {"03","04","05","06","07","08","09"};
            //             NSString* SEP_MERCHANT_NAME = @"0A";
            //             NSString* SEP_MERCHANT_CATEGORY_CODE = @"0B";
            //             NSString* SEP_MERCHANT_CITY = @"0C";
            //             NSString* SEP_MERCHANT_COUNTRY_CODE = @"0D";
            //             NSString* SEP_MERCHANT_CURRENCY_CODE = @"0E";
            
            
            
            
            //NSLog(@"%@",str_Mobile);
            //NSLog(@"%@",str_Accout_no);
            //NSLog(@"%@",str_merchant_name);
            //NSLog(@"%@",str_category_code);
            //NSLog(@"%@",str_city);
            //NSLog(@"%@",str_country_code);
            //NSLog(@"%@",str_curr_code);
            
            
            //            [gblclass.arr_qr_code addObject:str_Mobile];
            //            [gblclass.arr_qr_code addObject:str_Accout_no];
            //            [gblclass.arr_qr_code addObject:str_merchant_name];
            //            [gblclass.arr_qr_code addObject:str_category_code];
            //            [gblclass.arr_qr_code addObject:str_city];
            //            [gblclass.arr_qr_code addObject:str_country_code];
            //            [gblclass.arr_qr_code addObject:str_curr_code];
            
            
            [gblclass.arr_qr_code addObject:@""]; //str_Mobile
            [gblclass.arr_qr_code addObject:str_Accout_no];
            [gblclass.arr_qr_code addObject:str_merchant_name];
            [gblclass.arr_qr_code addObject:str_category_code];
            [gblclass.arr_qr_code addObject:str_city];
            [gblclass.arr_qr_code addObject:str_country_code];
            [gblclass.arr_qr_code addObject:str_curr_code];
            [gblclass.arr_qr_code addObject:str_crc];
            [gblclass.arr_qr_code addObject:detectionString];
            
            NSLog(@"%@",gblclass.arr_qr_code);
            
            // Stop video capture and make the capture session object nil.
            [_session stopRunning];
            _session = nil;
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"QR_Code_Master"];
            [self presentViewController:vc animated:NO completion:nil];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
        // NSError* err;
        
        // [hud hideAnimated:YES];
        
        //         //NSLog(@"%@", [error localizedDescription]);
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Incorrect BarCode"
        //                                                       delegate:self
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil, nil];
        //
        //        [alert show];
    }
    
}







-(void)Extract_Raw_Data
{
    
    //    NSError* error = nil;
    
    @try {
        
        NSString* SEP_PAN = @"00";
        NSString* SEP_MOBILE=@"01";
        NSString* SEP_ACCOUNT= @"08";
        NSString* SEP_MERCHANT_NAME=@"0A";
        NSString* SEP_MERCHANT_CATEGORY_CODE= @"0B";
        NSString* SEP_MERCHANT_CITY=@"0C";
        NSString* SEP_MERCHANT_COUNTRY_CODE=@"0D";
        NSString* SEP_MERCHANT_CURRENCY_CODE=@"0E";
        
        
        //    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code;
        
        
        
        
        //       00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
        
        
        //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
        //     PAN   account               Mobile#        name     code        city   cuntry cod   curr code
        
        NSArray* split;
        
        split=[[NSArray alloc] init];
        
        int bb=0;
        if ([detectionString length]>0)
        {
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            detectionString = [detectionString stringByReplacingOccurrencesOfString:@"#m3" withString:@""];
            detectionString = [detectionString stringByReplacingOccurrencesOfString:@"m3#" withString:@""];
            
            
            
            split = [detectionString componentsSeparatedByString: @"|"];
            
            
            //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,2)]);
            
        }
        
        
        
        //        Bashirsyed[10235468],
        //        039510235468,
        //        1198896,
        //        1,
        //        Deposit Account,
        //        SY,
        //        10.28,
        //        NEW KARACHI TOWNSHIP KARACHI,
        //        10.28
        //        )
        
        
        //        txt_acct_to.text=[split_to objectAtIndex:0];
        //        str_acct_name_to=[split_to objectAtIndex:0];
        //        str_acct_no_to=[split_to objectAtIndex:1];
        //        str_acct_id_to=[split_to objectAtIndex:2];
        //
        //        acct_to_chck=[split_to objectAtIndex:1];
        
        
        gblclass.chck_qr_send=@"1";
        // [gblclass.arr_qr_code addObject:[split objectAtIndex:2]];
        [gblclass.arr_qr_code addObject:[split objectAtIndex:0]];
        // [gblclass.arr_qr_code addObject:[split objectAtIndex:1]];
        
        
        
        
        
        // Stop video capture and make the capture session object nil.
        [_session stopRunning];
        _session = nil;
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"send_money"]; //send_qr
        [self presentViewController:vc animated:NO completion:nil];
        
        
        
    }
    @catch (NSException *exception)
    {
        
        // NSError* err;
        
        // [hud hideAnimated:YES];
        
        //         //NSLog(@"%@", [error localizedDescription]);
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Incorrect BarCode"
        //                                                       delegate:self
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil, nil];
        //
        //        [alert show];
    }
    
}



@end

