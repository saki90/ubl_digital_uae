//
//  Pay_Load_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Pay_Load_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;
-(IBAction)btn_back:(id)sender;


-(IBAction)btn_addpayee:(id)sender;
@end

