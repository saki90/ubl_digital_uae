//
//  Tezraftr_payee_list.h
//  ubltestbanking
//
//  Created by Asim Khan on 2/12/19.
//  Copyright © 2019 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Tezraftr_payee_list : UIViewController
{
    IBOutlet UITableView* table;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
-(IBAction)btn_new_payee:(id)sender;

@end

