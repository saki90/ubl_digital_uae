//
//  QR_Code_Detail.m
//  ubltestbanking
//
//  Created by Mehmood on 28/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "QR_Code_Detail.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface QR_Code_Detail ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    UIAlertController  *alert;
    UILabel* label;
    NSArray* split;
    UIImageView* img;
    NSString* chk_ssl;
    NSString* responsecode;
    NSString* str_acct_id_frm;
    Encrypt* encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation QR_Code_Detail
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    
    self.transitionController = [[TransitionDelegate alloc] init];
    NSLog(@"%@",gblclass.arr_qr_code);
    
    //    [gblclass.arr_qr_code addObject:str_Mobile];
    //    [gblclass.arr_qr_code addObject:str_Accout_no];
    //    [gblclass.arr_qr_code addObject:str_merchant_name];
    //    [gblclass.arr_qr_code addObject:str_category_code];
    //    [gblclass.arr_qr_code addObject:str_city];
    //    [gblclass.arr_qr_code addObject:str_country_code];
    //    [gblclass.arr_qr_code addObject:str_curr_code];
    
    ssl_count = @"0";
    txt_name.text = [gblclass.arr_qr_code objectAtIndex:2];
    txt_name.enabled=NO;
    lbl_name.text=[gblclass.arr_qr_code objectAtIndex:2];
    txt_cell.text=[NSString stringWithFormat:@"Merchant Cell # %@",[gblclass.arr_qr_code objectAtIndex:0]];
    
    txt_acct_from.text=gblclass.is_default_acct_id_name;
    _lbl_acct_frm.text=gblclass.is_default_acct_no;
    str_acct_id_frm=gblclass.is_default_acct_id;
    
    
    if ([gblclass.arr_transfer_within_acct count]>0)
    {
        
        
    split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    attach1.bounds = CGRectMake(10, 8, 20, 10);
    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    
    NSString* balc;
    
    if ([split objectAtIndex:6]==0)
    {
        balc=@"0";
    }
    else
    {
        balc=[split objectAtIndex:6];
    }
    
    
    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    
    [mutableAttriStr1 appendAttributedString:imageStr1];
    lbl_balance.attributedText = mutableAttriStr1;
    
    }
    
    txt_pin.delegate=self;
    txt_amnt.delegate=self;
    txt_qr_pin.delegate=self;
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//    };
    
    vw_table.hidden=YES;
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_cancel:(id)sender
{
    gblclass.camera_back = @"1";
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_review:(id)sender
{
    @try {
        
        
        if ([txt_amnt.text isEqualToString:@""])
        {
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter amount" :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //
            //            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        else  if ([txt_amnt.text isEqualToString:@"0"])
        {
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Valid Amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //
            //            [self presentViewController:alert animated:YES completion:nil];
            
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter valid amount" :@"0"];
            
            
            return;
        }
        else  if ([txt_qr_pin.text isEqualToString:@""])
        {
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Valid Amount" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //
            //            [self presentViewController:alert animated:YES completion:nil];
            
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter 6 digits QR PIN" :@"0"];
            
            
            return;
        }
        else  if ([txt_qr_pin.text length]<6)
        {
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter 6 digits QR PIN" :@"0"];
            
            return;
        }
        else  if ([txt_comment.text isEqualToString:@""])
        {
            txt_comment.text=@"";
        }
        
        
        //        if ([txt_pin.text isEqualToString:@""])
        //        {
        //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter PIN" preferredStyle:UIAlertControllerStyleAlert];
        //
        //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //            [alert addAction:ok];
        //
        //            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        
        
        
        NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code  count]);
        NSLog(@"%@",gblclass.arr_qr_code);
        if ([gblclass.arr_qr_code  count]>9)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:9 withObject:txt_amnt.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_amnt.text];
        }
        
        if ([gblclass.arr_qr_code count]>10) //txt_comment
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:10 withObject:txt_comment.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_comment.text];
        }
        
        if ([gblclass.arr_qr_code count]>11)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:11 withObject:txt_acct_from.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_acct_from.text];
        }
        
        if ([gblclass.arr_qr_code count]>12)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:12 withObject:_lbl_acct_frm.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:_lbl_acct_frm.text];
        }
        
        if ([gblclass.arr_qr_code count]>13)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:13 withObject:str_acct_id_frm];
        }
        else
        {
            [gblclass.arr_qr_code addObject:str_acct_id_frm];
        }
        
        if ([gblclass.arr_qr_code count]>14)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:14 withObject:txt_qr_pin.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_qr_pin.text];
        }
        
        
        
        //         [gblclass.arr_qr_code addObject:_lbl_acct_frm.text];
        //         [gblclass.arr_qr_code addObject:str_acct_id_frm];
        
        
        
        //        [gblclass.arr_qr_code addObject:txt_amnt.text];
        //         [gblclass.arr_qr_code addObject:txt_comment.text];
        
        //NSLog(@"%@",gblclass.arr_qr_code);
        
        
        
        //  [self checkinternet];
        //  if (netAvailable)
        //  {
        chk_ssl=@"otp_check";
        [self SSL_Call];
        //  }
        
        
        // 20 march 2017
        //        gblclass.check_review_acct_type=@"QR_Code";
        //
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        //cannt pay
        //NSLog(@"cannt pay");
        
        [self custom_alert:@"Try again later." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}


-(void) slideLeft {
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(void) slideRight {
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    if ([theTextField isEqual:txt_amnt]) {
        
        NSInteger MAX_DIGITS=11;
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
    }
    
    if ([theTextField isEqual:txt_pin])
    {
        
        MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_qr_pin])
    {
        
        MAX_DIGITS=6;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_comment])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //    //NSLog(@"text changed: %@", theTextField.text);
    //    textFieldText =[theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    //    formatter = [[NSNumberFormatter alloc] init];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //    formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];  //textFieldText
    
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText intValue]]];

    NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
    
    if ([theTextField isEqual:txt_amnt])
    {
        txt_amnt.text = [nf stringFromNumber:myNumber];
    }
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [self keyboardWillHide];
    
    [txt_cell resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_pin resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_qr_pin resignFirstResponder];
    
}

-(IBAction)btn_back:(id)sender
{
    gblclass.camera_back = @"1";
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //         [self dismissModalStack];
    
}

-(void)dismissModalStack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    if(tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];
    }
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [split objectAtIndex:6];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    
    //     if(tableView==table_to)
    //    {
    //
    //        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
    //
    //        //Name ::
    //        label=(UILabel*)[cell viewWithTag:2];
    //        label.text = [split objectAtIndex:0];
    //        label.textColor=[UIColor whiteColor];
    //        label.font=[UIFont systemFontOfSize:12];
    //        [cell.contentView addSubview:label];
    //
    //
    //        //Acct. No ::
    //        label=(UILabel*)[cell viewWithTag:3];
    //        label.text = [split objectAtIndex:1];
    //        label.textColor=[UIColor whiteColor];
    //        label.font=[UIFont systemFontOfSize:12];
    //        [cell.contentView addSubview:label];
    //
    //
    //
    //        img=(UIImageView*)[cell viewWithTag:1];
    //        // [img setImage:@"accounts.png"];
    //
    //        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    //        {
    //            img.image=[UIImage imageNamed:@"Non.png"];
    //        }
    //        else
    //        {
    //            img.image=[UIImage imageNamed:@"Non.png"];
    //        }
    //
    //        [cell.contentView addSubview:img];
    //
    //
    //    }
    //    else
    //    {
    ////        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
    ////        cell.textLabel.text = [split_bill objectAtIndex:0];
    ////        cell.textLabel.font=[UIFont systemFontOfSize:12];
    //    }
    //
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [table_from reloadData];
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        txt_acct_from.text=[split objectAtIndex:0];
        //        str_acct_name_frm=[split objectAtIndex:0];
        //        _lbl_acct_frm.text=[split objectAtIndex:1];
        //        str_acct_no_frm=[split objectAtIndex:1];
        str_acct_id_frm=[split objectAtIndex:2];
        lbl_balance.text=[split objectAtIndex:6];
        //
        //
        //        acct_frm_chck=[split objectAtIndex:1];
        
        _lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        
        table_from.hidden=YES;
        vw_table.hidden=YES;
    }
}


-(IBAction)btn_combo_frm:(id)sender
{
    
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    
    if (table_from.isHidden==YES)
    {
        // vw_bill.hidden=YES;
        // table_bill.hidden=YES;
        //  vw_from.hidden=NO;
        table_from.hidden=NO;
        //  table_to.hidden=YES;
        //  vw_to.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        //    vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
    
}


-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
////    if ([self isSSLPinning])
////    {
////        [self printMessage:@"Making pinned request"];
////    }
////    else
////    {
////        [self printMessage:@"Making non-pinned request"];
////    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"otp_check"])
        {
            [self CheckOtp_Req_DailyLimit:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"otp_check"])
    {
        [self CheckOtp_Req_DailyLimit:@""];
    }
    
    chk_ssl=@"";
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        // [self checkinternet];
        // if (netAvailable)
        // {
        chk_ssl=@"logout";
        [self SSL_Call];
        // }
        
        // [self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}



-(void) CheckOtp_Req_DailyLimit:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSString *amounts = [txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:amounts],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@"after"],
                                                                       [encrypt encrypt_Data:@"QRCODE"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTxtAmount",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"place",
                                                                       @"strAccessKey", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"CheckOTPReqQRDailyLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic2 objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      chk_ssl=@"logout";
                      
                      [hud hideAnimated:YES];
                      
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      // [self QR_Transaction_Instant_MP:@""];
                      
                      gblclass.check_review_acct_type=@"QR_Code";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"]) {
                      
                      gblclass.check_review_acct_type=@"QR_Code";
                      [self GenerateOTP:@""];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      
                  }
                  else if([[dic2 objectForKey:@"Response"] isEqualToString:@"-2"]) // Otp not required
                  {
                      [hud hideAnimated:YES];
                      gblclass.check_review_acct_type=@"QR_Code";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert: [dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}



-(void)QR_Transaction_Instant_MP:(NSString *)strIndustry
{
    
    
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3Session_ID_instant,@"1.1.1.1",@"QR_INSTANT_MP",@"0",@"",@"",@"",[gblclass.arr_qr_code objectAtIndex:7],[gblclass.arr_qr_code objectAtIndex:8],[gblclass.arr_qr_code objectAtIndex:5],[gblclass.arr_qr_code objectAtIndex:1],[gblclass.arr_qr_code objectAtIndex:0],[gblclass.arr_qr_code objectAtIndex:2],@"UBL",@"",[gblclass.arr_qr_code objectAtIndex:9],gblclass.Udid,gblclass.token_instant, nil] forKeys:[NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"payAnyOneFromBankImd",@"strTxtAmount",@"strTxtComments",@"strCCY",@"scanQRCodePan",@"merchantMobileNo",@"merchantTitle",@"strBankCode",@"strOTPPIN",@"QRPIN",@"Device_ID",@"Token", nil]];
    
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    NSString *amounts = [[gblclass.arr_qr_code objectAtIndex:9] stringByReplacingOccurrencesOfString:@"," withString:@""];
    //[gblclass.arr_qr_code objectAtIndex:9]
    //@"UBL"
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3Session_ID_instant,@"1.1.1.1",@"QR_INSTANT_MP",@"0",@"",@"",@"",amounts,[gblclass.arr_qr_code objectAtIndex:10],[gblclass.arr_qr_code objectAtIndex:5],[gblclass.arr_qr_code objectAtIndex:1],[gblclass.arr_qr_code objectAtIndex:0],[gblclass.arr_qr_code objectAtIndex:2],[gblclass.arr_qr_code objectAtIndex:3],[gblclass.arr_qr_code objectAtIndex:11],@"",gblclass.Udid,gblclass.token_instant,[gblclass.arr_qr_code objectAtIndex:7],[gblclass.arr_qr_code objectAtIndex:8], nil] forKeys:[NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"payAnyOneFromBankImd",@"strTxtAmount",@"strTxtComments",@"strCCY",@"scanQRCodePan",@"merchantMobileNo",@"merchantTitle",@"strBankCode",@"QRPIN",@"strOTPPIN",@"Device_ID",@"Token",@"strQRCodeCRC",@"strMPQRCode", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"QRTransactionInstant_MPNew" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //                  NSError *error;
              //                  NSArray *arr = (NSArray *)responseObject;
              
              NSDictionary* dic = (NSDictionary *)responseObject;
              
              //                  NSMutableArray* arr_load_payee_list;
              
              //                  arr_load_payee_list=[[NSMutableArray alloc] init];
              //                  arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
              //
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         // [mine myfaildata];
         
         [hud hideAnimated:YES];
         
         [self custom_alert:@"Retry" :@"0"];
         
     }];
    
}


-(void) GenerateOTP:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"MOB", nil]];
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    //limit_QRCODE
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id], [encrypt encrypt_Data:gblclass.M3sessionid], [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], [encrypt encrypt_Data:@""], [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:1]], [encrypt encrypt_Data:@"limit_QRCODE"], [encrypt encrypt_Data:@"Addition"], [encrypt encrypt_Data:@"300"], [encrypt encrypt_Data:@"limit_QRCODE"], [encrypt encrypt_Data:@"MOB"], [encrypt encrypt_Data:@"QRPINTRAN"], [encrypt encrypt_Data:gblclass.Udid], [encrypt encrypt_Data:gblclass.token], [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"Device_ID",@"Token",@"M3Key",nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  gblclass.check_review_acct_type=@"QR_Code";
                  
                  gblclass.Is_Otp_Required=@"1";
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
              
              
          }];
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
    //[alert release];
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id], [encrypt encrypt_Data:gblclass.Udid], [encrypt encrypt_Data:gblclass.M3sessionid], [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], [encrypt encrypt_Data:@"abcd"], [encrypt encrypt_Data:gblclass.token], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strDeviceID",@"strSessionId",@"IP",@"hCode",@"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}




#define kOFFSET_FOR_KEYBOARD 100.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}





@end

