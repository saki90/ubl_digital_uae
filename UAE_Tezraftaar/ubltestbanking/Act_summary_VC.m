//
//  Act_summary_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//
#import "AFNetworking.h"
#import "Act_summary_VC.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"

//#import "GnbRefreshAcct.h"
//#import "GnbAccountTransactionDetail.h"
#import "GlobalStaticClass.h"

@interface Act_summary_VC ()
{
    NSArray* arr_act_type;
    NSString* table_row;
    GlobalStaticClass* gblclass;
    NSArray* arr_table_name;
    NSArray* arr_table_img;
    UILabel* label;
    UIImageView* img;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    MBProgressHUD *hud;
  //  UILabel* label;
  //  UIImageView* img;
}
@end

@implementation Act_summary_VC

@synthesize ddMenu, ddText;
@synthesize ddMenuShowButton;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    ddMenu.hidden=YES;
    arr_act_type=[[NSArray alloc] init];
    arr_table_name=[[NSArray alloc] init];
    
    table.delegate=self;
    arr_act_type=@[@"Branch Account",@"Term Deposit",@"Omni Account",@"Credit Card Account",@"UBL Funds Investment Portfolio",@"Wiz Card Account"];
    
    //arr_table_name=@[@"Account",@"Payments",@"Transfer",@"E-Transactions",@"Settings",@"Log Out"];

    arr_table_name=@[@"Accounts",@"Payments",@"Transfer",@"E-Transactions",@"Settings",@"Log Out"];

    arr_table_img=@[@"Account-ic.png",@"Payments-ic.png",@"Transfer-ic.png",@"E-Transactions-ic.png",@"Setting-ic.png",@"Log-out-ic.png"];
    
    lbl_welcome_user.text=gblclass.welcome_msg;
    lbl_daily_limit.text=gblclass.daily_limit;
    lbl_monthly_limit.text=gblclass.monthly_limit;
    lbl_T_Pin.text= [gblclass.T_Pin stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
   
    
     NSLog(@"%@",gblclass.screen_size);
//     hud = [[MBProgressHUD alloc] initWithView:self.view];
//    [self AccountSummary:@""];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.

}

- (IBAction)ddMenuShow:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        sender.tag = 1;
        self.ddMenu.hidden = NO;
        [sender setTitle:@"Account ▲" forState:UIControlStateNormal];
    }
    else
    {
        sender.tag = 0;
        self.ddMenu.hidden = YES;
        [sender setTitle:@"Account ▼" forState:UIControlStateNormal];
    }
}

- (IBAction)ddMenuSelectionMade:(UIButton *)sender
{
    
    self.ddText.text = sender.titleLabel.text;
    [self.ddMenuShowButton setTitle:@"Account ▼" forState:UIControlStateNormal];
    self.ddMenuShowButton.tag = 0;
    self.ddMenu.hidden = YES;
    
    switch (sender.tag)
    {
        case 1:
            self.view.backgroundColor = [UIColor redColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor blueColor];
            break;
        case 3:
            self.view.backgroundColor = [UIColor greenColor];
            break;
            
        default:
            break;
    }
    
}










//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;    //count of section
//}
//
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//    return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
//}
//
//
//- (UITableViewCell *)tableView:(UITableView *)tableView
//         cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *MyIdentifier = @"MyIdentifier";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                      reuseIdentifier:MyIdentifier];
//    }
//    
//    
//    
//    cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
//    cell.textLabel.font = [UIFont systemFontOfSize:14];
//    
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    
//    return cell;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    table_row =[arr_act_type objectAtIndex:indexPath.row];
//    
//    NSLog(@"Selected item %@ ",table_row);
//    
//    
//    switch (indexPath.row)
//    {
//        case 0:
//            
//            [self performSegueWithIdentifier:@"branch_act" sender:self];
//            break;
//        case 1:
//            
//            [self performSegueWithIdentifier:@"act_statement" sender:self];
//            break;
//            
//        case 3:
//            [self performSegueWithIdentifier:@"cheuqe_management" sender:self];
//            break;
//            
//        case 4:
//            [self performSegueWithIdentifier:@"atm_card_management" sender:self];
//            break;
//        default: break;
//    }
//}
















//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if (tableView==table1)
//    {
//        return 1;
//    }
//    else
//    {
//        return 2;
//    }
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==table1)
    {
        
         mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//         vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"act_summary"];
        
        
        switch (indexPath.row)
        {
            case 0:
                gblclass.storyboardidentifer=@"accounts";
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
               
                break;
            case 1:
                
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"payment"];
                break;
                
            case 2:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"transfer"];
                break;
                
            case 3:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"E-Transaction"];
                break;
            case 4:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"setting"];
              
                break;
            case 5:
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
                break;
                
            default: break;
        }
        
          [self presentModalViewController:vc animated:YES];
        
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                
                [self performSegueWithIdentifier:@"branch_act" sender:self];
                break;
            case 1:
                
                [self performSegueWithIdentifier:@"act_statement" sender:self];
                break;
                
            case 3:
                [self performSegueWithIdentifier:@"cheuqe_management" sender:self];
                break;
                
            case 4:
                [self performSegueWithIdentifier:@"atm_card_management" sender:self];
                break;
            default: break;
        }

        table_row =[arr_act_type objectAtIndex:indexPath.row];
        NSLog(@"Selected item %@ ",table_row);
        
        
        }
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==table1)
    {
        //  NSLog(@"%lu",(unsigned long)[account_name count]);
        
        return [arr_table_name count];
        
    }
    else
    {
//        if (section==0)
//        {
//            return 3;
//            //return [arr_act_type count];
//        }
//        else
//        {
            //return 3;
            return [arr_act_type count];
//        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (tableView==table1)
    {
        label=(UILabel*)[cell viewWithTag:1];
        label.text=[arr_table_name objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:14];
        [cell.contentView addSubview:label];
        
        img=(UIImageView*)[cell viewWithTag:2];
        // [img setImage:@"accounts.png"];
       // img.image=[UIImage imageNamed:@"accounts1"];
        img.image=[UIImage imageNamed:[arr_table_img objectAtIndex:indexPath.row]];
        
        //[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        cell.separatorInset = UIEdgeInsetsZero;
    
    }
    else
    {
        //cell for second table
        NSLog(@"%lu",[arr_act_type count]);
            cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
        
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}











-(void) AccountSummary:(NSString *)strIndustry{
    
    //  method = @"GetMarketCompany";
    //self.ddTextCompany.text = @"Select Company";
    
    // allCompany = [[NSMutableArray alloc] init];
    // [mine showmyhud];
     [self.view addSubview:hud];
      [hud show:YES];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //539152
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"77",@"12345",@"1.1.1.1", nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP", nil]];
    
    NSLog(@"%@",dictparam);
    
    [manager POST:@"AccountSummary" parameters:dictparam
     
     
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSError *error;
              NSDictionary *dic = (NSDictionary *)responseObject;
              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              gblclass.actsummaryarr =
              [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbRefreshAcct"];
              
              NSLog(@"Done");
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
          }];
}


-(IBAction)btn_slide_close:(id)sender
{
    user_view.hidden=YES;
}

@end
