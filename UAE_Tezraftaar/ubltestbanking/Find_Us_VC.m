//
//  Find_Us_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Find_Us_VC.h"
#import "GlobalStaticClass.h"
#import "ViewController.h"
#import "ParkPlaceMark.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TBXML.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
//saki #import <PeekabooConnect/PeekabooConnect.h>
#import "Encrypt.h"


#define METERS_PER_MILE 1609.344
#define centreLat 30.372875
#define centreLong 70.092773
#define CLCOORDINATES_EQUAL( coord1, coord2 ) ((coord1.latitude == coord2.latitude && coord1.longitude == coord2.longitude))


NSArray *arealist;
NSArray *citylist;
int overLayCount=0;
int tableHeight=0;



@interface Find_Us_VC ()


{
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    UIAlertController* alert;
    MBProgressHUD* hud;
    CLLocationCoordinate2D currentLocation1;
    CLLocationManager *myLcationManager;
    Encrypt *encrypt;
    
    
    
}
@end

@implementation Find_Us_VC
@synthesize map,locationManager,area,city,mapBar,searchBar;
@synthesize pastUrls,HUD,annotationArray;
@synthesize autocompleteUrls;
@synthesize autocompleteTableView,branchesTableView;
@synthesize netAvailable,isATM,objPolyline,rb0,rb1,rb2,rb3;
@synthesize indicatorView,lbl_Faxno,lbl_Phoneno,pinkAnotImage,bgImageView,autocompleteBranchesUrls;
@synthesize longitudeLabel;
@synthesize latitudeLabel;


BOOL firstLocationUpdate1;
Boolean noStartSets_5 = TRUE;
CLLocationCoordinate2D startPoints_5;



#pragma mark - View LifeCycle -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.backBarButtonItem.target = self;
        self.navigationItem.backBarButtonItem.action = @selector(backbp);
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self mbHudProgress];
    // [HUD showWhileExecuting:@selector(beforeViewDidLoad) onTarget:self withObject:nil animated:YES];
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    
    self.arrNeighboursData = [[NSMutableArray alloc] init];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    
    
    bnk_InfoPage.hidden=YES;
    
    btn_GetDirection.hidden=TRUE;
    // [self beforeViewDidLoad];
    [self setUpRadioButtons];
    [map addSubview:searchBar];
    [map insertSubview:mapBar aboveSubview:searchBar];
    searchBar.hidden=YES;
    isATM=@"both";
    //
    //
    //    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *backBtnImage = [UIImage imageNamed:@"Home.png"];
    //    //    UIImage *backBtnImagePressed = [UIImage imageNamed:@"Home.png"];
    //    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    //    //    [backBtn setBackgroundImage:backBtnImagePressed forState:UIControlStateHighlighted];
    //    [backBtn addTarget:self action:@selector(backbp) forControlEvents:UIControlEventTouchUpInside];
    //    backBtn.frame = CGRectMake(3, 2,75, 47.0);
    //    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 47.0)];
    //    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 25,0);
    //    [backButtonView addSubview:backBtn];
    //    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    //    self.navigationItem.leftBarButtonItem = backButton;
    //
    //    //[CLLocationManager regionMonitoringEnabled];
    //    self.locationManager = [[CLLocationManager alloc] init] ;
    //    self.locationManager.delegate = self;
    //    self.locationManager.desiredAccuracy =
    //    //kCLLocationAccuracyBest;
    //    kCLLocationAccuracyBestForNavigation;
    //    [self.locationManager startUpdatingLocation];
    //
    //    map.zoomEnabled = YES;
    //    map.showsUserLocation=YES;
    //    [self.view addSubview:self.map];
    //    [map setUserTrackingMode:MKUserTrackingModeNone animated:YES];
    //    // hide the prefs UI for user tracking mode - if MKMapView is not capable of it
    //
    //    //self.locationManager.distanceFilter = 500.0f;
    //    // [self performSelector:@selector(getCurrentLocation) withObject:nil afterDelay:0.5];
    //
    //    [self getCurrentLocation];
    //
    //
    //    //NSLog(@"mehmood lat:%f..long:%f",originValue.latitude,originValue.longitude);
    //    //map.layer.cornerRadius=6;
    //    map.layer.borderColor=[UIColor grayColor].CGColor;
    //    map.layer.borderWidth=1.5;
    //    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Locate-Us_hdr.png"] forBarMetrics:UIBarMetricsDefault];
    //    self.title=@"Locate Us";
    //    //[[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"Locate-Us.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forBarMetrics:UIBarMetricsDefault];
    //
    //    karachiCityArray=[[NSMutableArray alloc] initWithObjects:@"clifton",@"mehmoodabad",@"nazimabad",@"kalapul",@"defence",@"nipa", nil];
    //
    //    citiesBranches=[[NSMutableDictionary alloc] initWithObjectsAndKeys:karachiCityArray,@"karachi", nil];
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(searchBar.frame.origin.x+city.frame.origin.x, city.frame.origin.y+city.frame.size.height+searchBar.frame.origin.y, city.frame.size.width, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    autocompleteTableView.backgroundColor=[UIColor blackColor];
    autocompleteTableView.alpha=0.8;
    autocompleteTableView.tag=10;
    
    [self.view addSubview:autocompleteTableView];
    autocompleteTableView.hidden = YES;
    
    
    
    branchesTableView = [[UITableView alloc] initWithFrame:
                         CGRectMake(searchBar.frame.origin.x+area.frame.origin.x,area.frame.size.height+area.frame.origin.y+searchBar.frame.origin.y, area.frame.size.width, 120) style:UITableViewStylePlain];
    branchesTableView.delegate = self;
    branchesTableView.dataSource = self;
    branchesTableView.scrollEnabled = YES;
    branchesTableView.hidden = YES;
    branchesTableView.backgroundColor=[UIColor blackColor];
    branchesTableView.alpha=0.8;
    branchesTableView.tag=11;
    [self.view addSubview:branchesTableView];
    branchesTableView.hidden = YES;
    //
    self.autocompleteUrls = [[NSMutableArray alloc] init];
    //
    //    city.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //
    //    // BranchesObject *branchObj=[[BranchesObject alloc] init];
    annotationArray=[[NSMutableArray alloc] init];
    //    for(BranchesObject *branchObj in banksPlotingPoditions)
    //    {
    //        [self focusBankPosition:branchObj];
    //    }
    //
    //    [self setColor];
    //
    //    [rb2 handleButtonTap];
    //
    //    // [RadioButton buttonSelected:rb1];
    //    // [RadioButton getradiobtn:2 andgroup:@"fg1"];
    //    //[RadioButton buttonSelected:temprb];
    
    
    
    
    if ([CLLocationManager locationServicesEnabled]){
        
        //NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
            
            myLcationManager = [[CLLocationManager alloc] init];
            [myLcationManager requestAlwaysAuthorization];
            myLcationManager.distanceFilter = kCLDistanceFilterNone;
            myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            myLcationManager.delegate = self;
            [myLcationManager startUpdatingLocation];
        }
        
        else {
            
            myLcationManager = [[CLLocationManager alloc] init];
            myLcationManager.distanceFilter = kCLDistanceFilterNone;
            myLcationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            myLcationManager.delegate = self;
            [myLcationManager startUpdatingLocation];
            
        }
    }
    
    
    //  [self checkLocationServicesTurnedOn];
    [self checkApplicationHasLocationServicesPermission];
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(myLcationManager.location.coordinate, 800, 800);
    [self.map setRegion:[self.map regionThatFits:region] animated:YES];
    
    
    currentLocation = locationManager.location;
    
    
    [manager stopUpdatingLocation];
    
    gblclass.arr_current_location = [[NSMutableArray alloc] init];
    float latitude = myLcationManager.location.coordinate.latitude;
    float longitude = myLcationManager.location.coordinate.longitude;
    
    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
    [gblclass.arr_current_location addObject:lati];
    [gblclass.arr_current_location addObject:longi];
    
    
    //NSLog(@"%f",latitude);
    //NSLog(@"%f",longitude);
    
}


#pragma mark - CLLocationManagerDelegate

//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    //NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    //NSLog(@"didUpdateToLocation: %@", newLocation);
//    CLLocation *currentLocation = newLocation;
//
//    if (currentLocation != nil) {
//        longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//        latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//    }
//}
//
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    CLLocation *location = [locations lastObject];
//
//   // self.latitudeValue.text = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
//  //  self.longtitudeValue.text = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
//
//    //NSLog(@"%@",[NSString stringWithFormat:@"%f", location.coordinate.latitude]);
//    //NSLog(@"%@",[NSString stringWithFormat:@"%f", location.coordinate.longitude]);
//}


-(void)viewWillAppear:(BOOL)animated
{
    
    //***
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //
    //            [hud showAnimated:YES];
    //            [hud showWhileExecuting:@selector(get_branches) onTarget:self withObject:nil animated:nil];
    //
    //        });
    
}

-(void)get_branches
{
    
    //  bnk_InfoPage.hidden=YES;
    
    btn_GetDirection.hidden=TRUE;
    [self beforeViewDidLoad];
    [self setUpRadioButtons];
    [map addSubview:searchBar];
    [map insertSubview:mapBar aboveSubview:searchBar];
    searchBar.hidden=YES;
    isATM=@"both";
     
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backBtnImage = [UIImage imageNamed:@"Home.png"];
    //    UIImage *backBtnImagePressed = [UIImage imageNamed:@"Home.png"];
    [backBtn setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    //    [backBtn setBackgroundImage:backBtnImagePressed forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(backbp) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(3, 2,75, 47.0);
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 47.0)];
    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 25,0);
    [backButtonView addSubview:backBtn];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = backButton;
    
    //[CLLocationManager regionMonitoringEnabled];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy =
    //kCLLocationAccuracyBest;
    kCLLocationAccuracyBestForNavigation;
    [self.locationManager startUpdatingLocation];
    
    map.zoomEnabled = YES;
    map.showsUserLocation=YES;
    [self.view addSubview:self.map];
    [map setUserTrackingMode:MKUserTrackingModeNone animated:YES];
    // hide the prefs UI for user tracking mode - if MKMapView is not capable of it
    
    //self.locationManager.distanceFilter = 500.0f;
    // [self performSelector:@selector(getCurrentLocation) withObject:nil afterDelay:0.5];
    
    [self getCurrentLocation];
    
    
    //NSLog(@"mehmood lat:%f..long:%f",originValue.latitude,originValue.longitude);
    //map.layer.cornerRadius=6;
    map.layer.borderColor=[UIColor grayColor].CGColor;
    map.layer.borderWidth=1.5;
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"Locate-Us_hdr.png"] forBarMetrics:UIBarMetricsDefault];
    self.title=@"Locate Us";
    //[[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"Locate-Us.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forBarMetrics:UIBarMetricsDefault];
    
    karachiCityArray=[[NSMutableArray alloc] initWithObjects:@"clifton",@"mehmoodabad",@"nazimabad",@"kalapul",@"defence",@"nipa", nil];
    
    citiesBranches=[[NSMutableDictionary alloc] initWithObjectsAndKeys:karachiCityArray,@"karachi", nil];
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(searchBar.frame.origin.x+city.frame.origin.x, city.frame.origin.y+city.frame.size.height+searchBar.frame.origin.y, city.frame.size.width, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    autocompleteTableView.backgroundColor=[UIColor blackColor];
    autocompleteTableView.alpha=0.8;
    autocompleteTableView.tag=10;
    
    [self.view addSubview:autocompleteTableView];
    
    
    
    branchesTableView = [[UITableView alloc] initWithFrame:
                         CGRectMake(searchBar.frame.origin.x+area.frame.origin.x,area.frame.size.height+area.frame.origin.y+searchBar.frame.origin.y, area.frame.size.width, 120) style:UITableViewStylePlain];
    branchesTableView.delegate = self;
    branchesTableView.dataSource = self;
    branchesTableView.scrollEnabled = YES;
    branchesTableView.hidden = YES;
    branchesTableView.backgroundColor=[UIColor blackColor];
    branchesTableView.alpha=0.8;
    branchesTableView.tag=11;
    [self.view addSubview:branchesTableView];
    
    self.autocompleteUrls = [[NSMutableArray alloc] init];
    
    city.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    // BranchesObject *branchObj=[[BranchesObject alloc] init];
    annotationArray=[[NSMutableArray alloc] init];
    for(BranchesObject *branchObj in banksPlotingPoditions)
    {
        [self focusBankPosition:branchObj];
    }
    
    [self setColor];
    
    [rb2 handleButtonTap];
    
    // [RadioButton buttonSelected:rb1];
    // [RadioButton getradiobtn:2 andgroup:@"fg1"];
    //[RadioButton buttonSelected:temprb];
}


// Location Service Checking ::


- (void) checkLocationServicesTurnedOn
{
    
    if (![CLLocationManager locationServicesEnabled])
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Location Services' need to be on.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        
        //        UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
        //                                                        message:@"'Location Services' need to be on."
        //                                                       delegate:nil
        //                                              cancelButtonTitle:@"OK"
        //                                              otherButtonTitles:nil];
        //        [alert3 show];
    }
    
}


-(void) checkApplicationHasLocationServicesPermission
{
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"This application needs 'Location Services' to be turned on.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
        //                                                        message:@"This application needs 'Location Services' to be turned on."
        //                                                       delegate:nil
        //                                              cancelButtonTitle:@"OK"
        //                                              otherButtonTitles:nil];
        //        [alert1 show];
        
        
        
        [self getBranchesInfo:@""];
        
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [hud showAnimated:YES];
            [hud showWhileExecuting:@selector(get_branches) onTarget:self withObject:nil animated:nil];
            
        });
    }
    
}












- (void)updateCurrentLocation
{
    //  sleep(2);
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    CLLocation *location1 = [locationManager location];
    currentLocation1 = [location1 coordinate];
    
    if(noStartSets_5)
    {
        startPoints_5 = currentLocation1;
        noStartSets_5 = FALSE;
    }
    
    
    // [self.mapView animateToLocation:currentLocation];
}


- (IBAction)getCurrentLocation
{
    
    //    if(netAvailable){
    //        // [self setFocusedPoint:centreLat longt:centreLong andPosition:@"Pakistan Centre"];//:@"Pakistan Centre"];
    //        [self setFocusedPoint:originValue.latitude longt:originValue.longitude andPosition:@" "];
    //    }
    //    else{
    
    [myLcationManager startUpdatingLocation];
    CLLocationCoordinate2D orgCord=CLLocationCoordinate2DMake(myLcationManager.location.coordinate.latitude, myLcationManager.location.coordinate.longitude);
    
    if (orgCord.latitude!=0.0f && orgCord.longitude!=0.0f)
    {
        originValue=orgCord;
        //NSLog(@"Valid");
        
        [self setFocusedPoint:originValue.latitude longt:originValue.longitude andPosition:@" "];
    }
    else
    {
        // originValue=CLLocationCoordinate2DMake(24.856895f, 67.0423050f);
        //NSLog(@"NotValid");
        
        
        
        if (orgCord.latitude==0 && orgCord.longitude==0)
        {
            
            [self updateCurrentLocation];
            return;
        }
        
        
        
        if([CLLocationManager locationServicesEnabled] &&
           [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
        {
            //NSLog(@"%d",kCLAuthorizationStatusDenied);
        }
        else
        {
            //NSLog(@"asdf asdf");
        }
        
        
        
        
        
        
        
        
        
        
        //        if ([CLLocationManager locationServicesEnabled])
        //        {
        
        //NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            
            gblclass.custom_alert_msg=@"To re-enable, please go to Settings and turn on Location Service for this app.";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            //            UIAlertView *  alert2 = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
            //                                                              message:@"To re-enable, please go to Settings and turn on Location Service for this app."
            //                                                             delegate:nil
            //                                                    cancelButtonTitle:@"OK"
            //                                                    otherButtonTitles:nil];
            //            [alert2 show];
            
            
        }
        //      }
        
        
        
        //        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention"
        //                                                        message: @"Not Found Your Location Try Again !"
        //                                                       delegate:self
        //                                              cancelButtonTitle:nil
        //                                              otherButtonTitles:@"OK", nil];
        //        [alert1 show];
        
        return;
        
    }
    
    
    //    }
    
    
}
-(void)beforeViewDidLoad
{
    objPolyline=[[MKPolyline alloc] init];
    arrRoutePoints=[[NSArray alloc] init];
    netAvailable = NO;
    
    [self checkinternet];
    // Do any additional setup after loading the view from its nib.
    fileManager= [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    self->documentDirectory = [paths objectAtIndex:0];
    //NSLog(@"Path array:%@",paths);
    infoPath = [documentDirectory stringByAppendingPathComponent:@"Info"];
    NSError *error;
    [fileManager createDirectoryAtPath:infoPath withIntermediateDirectories:NO attributes:nil error:&error];
    //NSLog(@"do Dir wiew appear:%@",infoPath);
    
    
    // //NSLog(@"%@",[CLLocationManager locationServicesEnabled]);
    //    if ([CLLocationManager locationServicesEnabled])
    //    {
    
    //NSLog(@"Location Services Enabled");
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
    {
        
        gblclass.custom_alert_msg=@"To re-enable, please go to Settings and turn on Location Service for this app.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        UIAlertView* alert1 = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
        //                                                        message:@"To re-enable, please go to Settings and turn on Location Service for this app."
        //                                                       delegate:nil
        //                                              cancelButtonTitle:@"OK"
        //                                              otherButtonTitles:nil];
        //        [alert1 show];
        
        
    }
    
    //    }
    
    
    
    // [self mbHudProgress];
    if(netAvailable)
    {
        totalBranchesInfo=[[NSMutableDictionary alloc] init];
        
        //sakii     [self Branches_By_city:@""];
        
        
        gblclass.arr_current_location = [[NSMutableArray alloc] init];
        // 777     float latitude = myLcationManager.location.coordinate.latitude;
        //777      float longitude = myLcationManager.location.coordinate.longitude;
        
        //        CLLocation *locatioF = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        //        currentLocation = locatioF;
        //
        //        CLGeocoder *geocoder2 = [[CLGeocoder alloc] init] ;
        //        [geocoder2 reverseGeocodeLocation:currentLocation
        //                       completionHandler:^(NSArray *placemarks, NSError *error) {
        //                           //NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
        //
        //                           if (error){
        //                               //NSLog(@"Geocode failed with error: %@", error);
        //                               return;
        //
        //                           }
        //
        //
        //                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
        //                           //  NSString * location = ([placemarks count] > 0) ? [[placemarks objectAtIndex:0] locality] : @"Not Found";
        //
        //
        //
        //                           //NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
        //                           //NSLog(@"placemark.country %@",placemark.country);
        //                           //NSLog(@"placemark.postalCode %@",placemark.postalCode);
        //                           //NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
        //                           //NSLog(@"placemark.locality %@",placemark.locality);
        //
        //                           [self getBranchesInfo:placemark.locality];
        //
        //
        //                           //NSLog(@"placemark.subLocality %@",placemark.subLocality);
        //                           //NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
        //
        //                       }];
        
        
        
        
        if (![gblclass.locationCity isEqual:@""]|| gblclass.locationCity != nil) {
            
            [self getBranchesInfo:gblclass.locationCity];
            
        }
        
        else {
            
            
            [self getBranchesInfo:@""];
            
        }
        //        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
        //         {
        //             //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        //             if (error == nil && [placemarks count] > 0)
        //             {
        //                 CLPlacemark *placemark = [placemarks lastObject];
        //
        //                 // strAdd -> take bydefault value nil
        //                 NSString *strAdd = nil;
        //
        //                 if ([placemark.subThoroughfare length] != 0)
        //                     strAdd = placemark.subThoroughfare;
        //
        //                 if ([placemark.thoroughfare length] != 0)
        //                 {
        //                     // strAdd -> store value of current location
        //                     if ([strAdd length] != 0)
        //                         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
        //                     else
        //                     {
        //                         // strAdd -> store only this value,which is not null
        //                         strAdd = placemark.thoroughfare;
        //                     }
        //                 }
        //
        //                 if ([placemark.postalCode length] != 0)
        //                 {
        //                     if ([strAdd length] != 0)
        //                         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
        //                     else
        //                         strAdd = placemark.postalCode;
        //                 }
        //
        //                 if ([placemark.locality length] != 0)
        //                 {
        //                     if ([strAdd length] != 0)
        //                         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
        //                     else
        //                         strAdd = placemark.locality;
        //                 }
        //
        //                 if ([placemark.administrativeArea length] != 0)
        //                 {
        //                     if ([strAdd length] != 0)
        //                         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
        //                     else
        //                         strAdd = placemark.administrativeArea;
        //                 }
        //
        //                 if ([placemark.country length] != 0)
        //                 {
        //                     if ([strAdd length] != 0)
        //                         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
        //                     else
        //                         strAdd = placemark.country;
        //                 }}}
        //         ];
        //
        //
        //
        //
        //
        //
        //
        //
        //
        
        //[self getBranchesInfo: @"karachi"];
    }
    else
    {
        [self getinfo];
    }
}





//-(void)viewWillAppear:(BOOL)animated
//{
//    //bgImageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"UBL-Bg-%d.jpg",[userDefault integerForKey:@"BgView"]]];
//
//
//    //**  bgImageView.image=[UIImage imageWithData:[userDefault objectForKey:@"BgView"]];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//http://stackoverflow.com/questions/2834523/drawing-a-route-in-mapkit-in-iphone-sdk

#pragma mark - Check Internet -
-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            //            NSString *extension = @"png";
            //            //NSString *docDir = [NSString stringWithString:documentDirectory];
            //            //NSLog(@"imgdir:::%@",imgdirSmall);
            //            if(imageArray==nil){
            //                if([fileManager fileExistsAtPath:imgdirSmall]&&[fileManager fileExistsAtPath:imgdirLarge]){
            //                    NSArray *contents = [fileManager contentsOfDirectoryAtPath:imgdirSmall error:NULL];
            //                    NSArray *contents2 = [fileManager contentsOfDirectoryAtPath:imgdirLarge error:NULL];
            //
            //                    offlineImageArray = [NSMutableArray arrayWithCapacity: [contents count]];
            //                    offlineImageArrayLarge=[NSMutableArray arrayWithCapacity: [contents count]];
            //                    NSString *filename,*filename2;
            //                    for (filename in contents)
            //                    {
            //                        if ([[filename pathExtension] isEqualToString:extension])
            //                        {
            //                            [offlineImageArray addObject:[imgdirSmall stringByAppendingPathComponent:filename]];
            //
            //                        }
            //                    }
            //                    for (filename2 in contents2)
            //                    {
            //                        if ([[filename2 pathExtension] isEqualToString:extension])
            //                        {
            //                            [offlineImageArrayLarge addObject:[imgdirLarge stringByAppendingPathComponent:filename2]];
            //
            //                        }
            //                    }
            //                    //offlineImageArray=[[NSMutableArray alloc] initWithContentsOfFile:imgdir];
            //                    //NSLog(@"Offline Array:::%@,..%@",offlineImageArray,offlineImageArrayLarge);
            //                }
            //                else{
            //
            //                }
            //            }
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            UIAlertView *magalert33 = [[UIAlertView alloc] initWithTitle:@"Notification" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [magalert33 show];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring :(UITextField *)fieldName
{
    
    [autocompleteUrls removeAllObjects];
    
    if(fieldName==city)
    {
        
        for(NSString *curString in citiesArray)
        {
            // curString = [curString lowercaseString];
            NSRange substringRange = [[curString lowercaseString] rangeOfString:[substring lowercaseString]];
            
            if (substringRange.location != NSNotFound)
            {
                // String was found
                [autocompleteUrls addObject:curString];
                //NSLog(@"autocomplete Urls:%@",autocompleteUrls);
            }
            else
            {
                // String not found
            }
        }
        [autocompleteTableView reloadData];
    }
    else if (fieldName==area)
    {
        [autocompleteUrls arrayByAddingObjectsFromArray:selectedCityBranches];
        for(NSString *curString in selectedCityBranches)
        {
            // curString = [curString lowercaseString];
            NSRange substringRange = [[curString lowercaseString] rangeOfString:[substring lowercaseString]];
            if (substringRange.location != NSNotFound)
            {
                // String was found
                [autocompleteUrls addObject:curString];
                //NSLog(@"autocomplete Urls:%@",autocompleteUrls);
            }
            else
            {
                // String not found
            }
            
        }
        
        [branchesTableView reloadData];
    }
    
}
#pragma mark - MBHUDProgress Methods -

-(void)mbHudProgress
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    // HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Please Wait...";
    HUD.delegate=self;
    HUD.mode = MBProgressHUDModeIndeterminate;
    [HUD showAnimated:YES];
    [map addSubview:HUD];
}

- (void)hudWasHidden
{
    // Remove HUD from screen
    [HUD removeFromSuperview];
    // add here the code you may need
}

#pragma mark - UITextFieldDelegate methods -

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    tableHeight=0;
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    if(textField==city)
    {
        autocompleteTableView.hidden = NO;
        branchesTableView.hidden=YES;
        
        [self searchAutocompleteEntriesWithSubstring:substring:textField];
    }
    else
    {
        [self searchAutocompleteEntriesWithSubstring:substring:textField];
        //NSLog(@"Running Branches");
    }
    return YES;
}
- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
    if(textField.tag==10)
    {
        //************************//selectedCityBranches=[NSMutableArray arrayWithArray:[citiesBranches objectForKey:city.text]];
        //NSString *are = [NSString stringWithFormat:@"%@ Branch",city.text];
        //area.text = are ;
        [branchesTableView reloadData];
        // autocompleteTableView.hidden = TRUE;
    }
    else if(textField.tag==11)
    {
        // branchesTableView.hidden=YES;
    }
    
    [area resignFirstResponder];
    [city resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    tableHeight=0;
    [self animationOut];
    if(textField==area)
    {
        
        autocompleteTableView.hidden=YES;
        
        if ([city.text length]==0) {
            
            
            gblclass.custom_alert_msg=@"Please enter your city";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            UIAlertView *al = [[UIAlertView alloc]initWithTitle:@"City Empty" message:@"Please enter your city" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [al show];
            
            [self.city becomeFirstResponder];
        }
        else
        {
            branchesTableView.hidden=NO;
            //NSLog(@"Running");
            
        }
    }
    else if(textField==city){
        area.text=@"";
        branchesTableView.hidden=YES;
    }
}



#pragma mark - UITableView methods -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [autocompleteUrls count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
        cell.textLabel.font=[UIFont boldSystemFontOfSize:10.0];
        cell.textLabel.numberOfLines=3;
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.backgroundColor=[UIColor blackColor];
        cell.backgroundColor=[UIColor blackColor];
    }
    
    
    // set a font size
    UIFont *cellFont = [UIFont fontWithName:@"Helvetica" size:14.0];
    
    // get a constraint size - not sure how it works
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    
    
    
    if(tableView.tag==10){
        cell.textLabel.text = [autocompleteUrls objectAtIndex:indexPath.row];
        // calculate a label size - takes parameters including the font, a constraint and a specification for line mode
        // CGSize labelSize = [cell.textLabel.text sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        // tableHeight=tableHeight+labelSize.height + 20;
        // tableHeight=autocompleteTableView.rowHeight+tableHeight;
        // give it a little extra height
        // [autocompleteTableView setFrame:CGRectMake(autocompleteTableView.frame.origin.x, autocompleteTableView.frame.origin.y, autocompleteTableView.frame.size.width, tableHeight) ];
    }
    else{
        //        if(autocompleteUrls.count==0){
        //            cell.textLabel.text = [selectedCityBranches objectAtIndex:indexPath.row];
        //        }
        //        else{
        cell.textLabel.text = [autocompleteUrls objectAtIndex:indexPath.row];
        //        }
        
        // calculate a label size - takes parameters including the font, a constraint and a specification for line mode
        // CGSize labelSize = [cell.textLabel.text sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        // tableHeight=tableHeight+labelSize.height + 20;
        // tableHeight=autocompleteTableView.rowHeight+tableHeight;
        // [branchesTableView setFrame:CGRectMake(branchesTableView.frame.origin.x, branchesTableView.frame.origin.y, branchesTableView.frame.size.width, tableHeight)];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *branchArray=[[NSMutableArray alloc] init];
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    //NSLog(@"%@",selectedCell.textLabel.text);
    
    
    hud.mode = MBProgressHUDModeIndeterminate;
    [hud showAnimated:YES];
    
    if(tableView.tag==10)
    {
        
        selectedCityBranches=[[NSMutableArray alloc] init];
        autocompleteTableView.hidden = TRUE;
        branchesTableView.hidden=YES;
        
        [city resignFirstResponder];
        
        //[hud showAnimated:YES];
        //[self.view addSubview:hud];
        
        
        //[hud showAnimated:YES]
        
        NSString *urlStr = [NSString stringWithFormat:@"http://61.5.156.98/NetServiceRegionUBL/Service.asmx/All_RegionsByCityFinal?AccessKey=%@&City=%@",access_key_feedback,selectedCell.textLabel.text];
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                              returningResponse:&response
                                                          error:&error];
        
        
        
        if (error == nil)
        {
            // Parse data here
            
            [hud hideAnimated:YES];
            
            
            //NSLog(@"asd");
            //NSLog(@"asd");
            //NSLog(@"asd");
            //NSLog(@"asd");
            //NSLog(@"asd");
            
            self.xmlParser = [[NSXMLParser alloc] initWithData:data];
            self.xmlParser.delegate = self;
            
            // Initialize the mutable string that we'll use during parsing.
            self.foundValue = [[NSMutableString alloc] init];
            
            // Start parsing.
            [self.xmlParser parse];
            
            //self.arrNeighboursData = [[NSMutableArray alloc] init];
            
            for (int i =0; i<self.arrNeighboursData.count; i++) {
                
                bankInfo = [[NSMutableArray alloc] init];
                BranchesObject *branchObj=[[BranchesObject alloc] init];
                branchObj.branch_name = [self.arrNeighboursData[i] objectForKey:@"BranchName"];
                branchObj.Address = [self.arrNeighboursData[i] objectForKey:@"BranchAddress"];
                branchObj.branch_id = [self.arrNeighboursData[i] objectForKey:@"S_NO"];
                branchObj.Contact_no = [self.arrNeighboursData[i] objectForKey:@"Contact_No_1"];
                branchObj.Lockers_Available = [self.arrNeighboursData[i] objectForKey:@"Lockers_Available"];
                branchObj.Ameen_window = [self.arrNeighboursData[i] objectForKey:@"Ameen_Window"];
                branchObj.city_name = [self.arrNeighboursData[i] objectForKey:@"City"];
                branchObj.longitude = [self.arrNeighboursData[i] objectForKey:@"LONGITUDE"];
                branchObj.latitude = [self.arrNeighboursData[i] objectForKey:@"LATITUED"];
                branchObj.ATM = [self.arrNeighboursData[i] objectForKey:@"ATM_Available"];
                branchObj.branchType = [self.arrNeighboursData[i] objectForKey:@"TypeOfBranch"];
                
                [bankInfo addObject:branchObj];
                [banksPlotingPoditions addObject:branchObj];
                
                
                NSString *str = [branchObj.city_name stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                
                if (i != 0) {
                    NSArray *arr = [totalBranchesInfo objectForKey:[str lowercaseString]];
                    
                    [((NSMutableArray*)[totalBranchesInfo objectForKey:[str lowercaseString]]) addObject:branchObj];
                }
                
                
                else {
                    [totalBranchesInfo setValue:bankInfo forKey:[str lowercaseString]];
                }
                // [citiesArray addObject:str];
                
                
            }
            
            [self saveinfo];
            
        }
        else
        {
            
            [hud hideAnimated:YES];
            //            UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Internet not connected" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //            [alert2 show];
            
            gblclass.custom_alert_msg=@"Internet not connected";
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            return;
            
        }
        
        
        
        
        selectedCityBranches=[[NSMutableArray alloc] init];
        city.text = selectedCell.textLabel.text;
        
        autocompleteTableView.hidden = TRUE;
        branchesTableView.hidden=YES;
        NSArray *arry=[[NSArray alloc] initWithArray:[totalBranchesInfo objectForKey:[city.text lowercaseString]]];
        
        for(int i=0;i<arry.count;i++)
        {
            
            if([ isATM isEqualToString:@"atm"])
            {
                if([[[arry objectAtIndex:i] ATM] isEqualToString:@"YES"])
                {
                    [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
                }
            }
            
            
            // AMEEN - ISLAMIC
            
            
            if([isATM isEqualToString:@"ameen"])
                
                
            {
                if([[[arry objectAtIndex:i] Ameen_window] isEqualToString:@"YES"] || [[[arry objectAtIndex:i] branchType] isEqualToString:@"AMEEN - ISLAMIC"])
                {
                    [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
                }
            }
            else if([ isATM isEqualToString:@"notatm"])
            {
                if([[[arry objectAtIndex:i] ATM] isEqualToString:@"NO"])
                {
                    [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
                }
            }
            else if([ isATM isEqualToString:@"both"])
            {
                [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
            }
            
        }
        
        [autocompleteUrls removeAllObjects];
        [autocompleteUrls addObjectsFromArray:selectedCityBranches];
        tableHeight=tableHeight-1;
        //[branchesTableView reloadData];
        
    }
    else if(tableView.tag==11)
    {
        tableHeight=0;
        //area.text = selectedCell.textLabel.text;
        NSString * branchAddressIs=[NSString stringWithString:selectedCell.textLabel.text];
        NSString *are = [NSString stringWithFormat:@"%@ Branch",selectedCell.textLabel.text];
        area.text = are ;
        branchesTableView.hidden=YES;
        branchArray=[totalBranchesInfo objectForKey:city.text.lowercaseString];
        //NSLog(@"Branches arrayyyy:%@....%@",branchArray,totalBranchesInfo);
        
        for(BranchesObject *branchObj in branchArray)
        {
            
            if([branchObj.Address.lowercaseString isEqualToString:branchAddressIs.lowercaseString])
            {
                CLLocationCoordinate2D nextDestination=CLLocationCoordinate2DMake([branchObj.latitude doubleValue], [branchObj.longitude doubleValue]);
                if(CLCOORDINATES_EQUAL(nextDestination,destinationValue))
                {
                    
                }
                else
                {
                    destinationValue=nextDestination;
                    
                    if(netAvailable)
                    {
                        
                        //[HUD showWhileExecuting:@selector(setRoutPoints) onTarget:self withObject:nil animated:YES];
                        // [HUD showAnimated:YES whileExecutingBlock:^{[self setRoutPoints];}];
                        
                        [self mbHudProgress];
                        [self performSelector:@selector(hudWasHidden) withObject:nil afterDelay:0.5];
                        MKCoordinateRegion region;
                        region.center.latitude     =destinationValue.latitude;
                        region.center.longitude    =destinationValue.longitude;
                        [self setFocusedPoint:destinationValue.latitude longt:destinationValue.longitude andPosition:@" "];
                        
                        for (id currentAnnotation in map.annotations)
                        {
                            
                            if ([currentAnnotation isKindOfClass:[ParkPlaceMark class]])
                            {
                                [map deselectAnnotation:currentAnnotation animated:YES];
                            }
                        }
                        
                        //  [self setRoutPoints];
                        //[map addSubview:indicatorView];
                        // [indicatorView startAnimating];
                        //[self setRoutPoints];
                        //[self mbHudProgress];
                        //[self setRoutPoints];
                        //[self performSelectorOnMainThread:@selector(hudWasHidden) withObject:nil waitUntilDone:YES];
                        
                    }
                    else
                    {
                        [self setFocusedPoint:[branchObj.latitude doubleValue] longt:[branchObj.longitude doubleValue] andPosition:@" "];
                    }
                }
                break;
            }
        }
    }
    
    [city resignFirstResponder];
    [area resignFirstResponder];
    [self radioButtonSelectedAtIndex:2 inGroup:@""];
    
    [hud hideAnimated:YES];
    
    //NSLog(@"Selected Cities Branches:%@",selectedCityBranches);
    
}

// get the height of the row
//
//- (CGFloat)tableView:(UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath    {
//
//    NSString *thisCity = [autocompleteUrls objectAtIndex:indexPath.row];
//    NSString * cellText = thisCity;
//
//    // set a font size
//    UIFont *cellFont = [UIFont fontWithName:@"Helvetica" size:17.0];
//
//    // get a constraint size - not sure how it works
//    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
//
//    // calculate a label size - takes parameters including the font, a constraint and a specification for line mode
//    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
//
//    // give it a little extra height
//    return labelSize.height + 20;
//
//}

#pragma mark - My Methods -
-(void)setColor
{
    // UIView *firstLayer = [[UIView alloc] initWithFrame:CGRectMake(100, 150, 100, 100)];
    bnk_InfoPage.alpha = 0.80;
    //    bnk_InfoPage.layer.cornerRadius = 25;
    //    bnk_InfoPage.layer.backgroundColor=[UIColor blackColor].CGColor;//[UIColor colorWithRed:0.1f green:4/255.0f blue:32/255.0f alpha:1].CGColor;//[UIColor darkTextColor].CGColor;
    //    bnk_InfoPage.layer.borderColor = [UIColor blackColor].CGColor;
    //bnk_InfoPage.layer.borderWidth = 2;
    
    bnk_InfoPage.layer.rasterizationScale=[UIScreen mainScreen].scale;
    bnk_InfoPage.layer.shadowColor = [UIColor blackColor].CGColor;
    bnk_InfoPage.layer.shadowOpacity = 5.0;
    bnk_InfoPage.layer.shadowRadius = 10.0;
    bnk_InfoPage.layer.shadowOffset = CGSizeMake(0, 4);
    bnk_InfoPage.layer.shouldRasterize = YES;
}

-(void)backbp
{
    [self dismissModalViewControllerAnimated:YES];
    
    NSArray *pop = self.navigationController.viewControllers;
    UIViewController *tager = [pop objectAtIndex:0];
    [self.navigationController popToViewController:tager animated:YES];
    self.navigationController.navigationBarHidden = TRUE;
}

-(void)animationOut
{
    //  bnk_InfoPage.hidden=NO;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6f];
    [UIView setAnimationDelegate:self];
    //bnk_InfoPage.transform = CGAffineTransformIdentity;
    //[UIView commitAnimations];
    [bnk_InfoPage setFrame:CGRectMake(self.view.frame.size.width+10, map.frame.size.height/2-(bnk_InfoPage.frame.size.height/2), bnk_InfoPage.frame.size.width, bnk_InfoPage.frame.size.height)];
    [UIView commitAnimations];
}

#pragma mark - RadioButtons -
-(void)setUpRadioButtons
{
    rb3 = [[RadioButton alloc] initWithGroupId:@"fg1" index:3 andFrame:CGRectMake(94,8,15,15)];
    rb0 = [[RadioButton alloc] initWithGroupId:@"fg1" index:0 andFrame:CGRectMake(162,8,15,15)];
    //rb1 = [[RadioButton alloc] initWithGroupId:@"fg1" index:1 andFrame:CGRectMake(182,8,15,15)];
    rb2 = [[RadioButton alloc] initWithGroupId:@"fg1" index:2 andFrame:CGRectMake(240,8,15,15)];
    
    
    
    
    
    //  RadioButton *rb3 = [[RadioButton alloc] initWithGroupId:@"first group" index:2];
    
    //    rb0.frame = CGRectMake(94,80,15,15);
    //    rb1.frame = CGRectMake(184,80,15,15);
    //    rb2.frame = CGRectMake(274,80,15,15);
    
    [searchBar addSubview:rb0];
    //[searchBar addSubview:rb1];
    [searchBar addSubview:rb2];
    [searchBar addSubview:rb3];
    
    
    
    //    UILabel *label1 =[[UILabel alloc] initWithFrame:CGRectMake(110, 7, 50, 21)];
    //    label1.backgroundColor = [UIColor clearColor];
    //    label1.font=[UIFont fontWithName:@"Marion" size:14.0];
    //    label1.textColor=[UIColor blackColor];
    //    label1.text = @"ATM";
    //    [searchBar addSubview:label1];
    //
    //    UILabel *label2 =[[UILabel alloc] initWithFrame:CGRectMake(178, 7, 60, 21)];
    //    label2.backgroundColor = [UIColor clearColor];
    //    label2.font=[UIFont fontWithName:@"Marion" size:14.0];
    //    label2.textColor=[UIColor blackColor];
    //    label2.text = @"Branches";
    //    [searchBar addSubview:label2];
    //
    //
    //    UILabel *label3 =[[UILabel alloc] initWithFrame:CGRectMake(256, 7, 50, 21)];
    //    label3.backgroundColor = [UIColor clearColor];
    //    label3.font=[UIFont fontWithName:@"Marion" size:14.0];
    //    label3.textColor=[UIColor blackColor];
    //    label3.text = @"Both";
    //    [searchBar addSubview:label3];
    
    
    UILabel *label0 =[[UILabel alloc] initWithFrame:CGRectMake(110, 7, 50, 21)];
    label0.backgroundColor = [UIColor clearColor];
    label0.font=[UIFont fontWithName:@"Marion" size:14.0];
    label0.textColor=[UIColor blackColor];
    label0.text = @"Ameen";
    [searchBar addSubview:label0];
    
    
    
    
    UILabel *label1 =[[UILabel alloc] initWithFrame:CGRectMake(178, 7, 60, 21)];
    label1.backgroundColor = [UIColor clearColor];
    label1.font=[UIFont fontWithName:@"Marion" size:14.0];
    label1.textColor=[UIColor blackColor];
    label1.text = @"ATM";
    [searchBar addSubview:label1];
    
    //    UILabel *label2 =[[UILabel alloc] initWithFrame:CGRectMake(178, 7, 60, 21)];
    //    label2.backgroundColor = [UIColor clearColor];
    //    label2.font=[UIFont fontWithName:@"Marion" size:14.0];
    //    label2.textColor=[UIColor blackColor];
    //    label2.text = @"Branches";
    //    [searchBar addSubview:label2];
    
    
    UILabel *label3 =[[UILabel alloc] initWithFrame:CGRectMake(270, 7, 50, 21)];
    label3.backgroundColor = [UIColor clearColor];
    label3.font=[UIFont fontWithName:@"Marion" size:14.0];
    label3.textColor=[UIColor blackColor];
    label3.text = @"All";
    [searchBar addSubview:label3];
    
    
    [RadioButton addObserverForGroupId:@"fg1" observer:self];
}

-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString *)groupId
{
    
    //NSLog(@"changed to %lu in %@",(unsigned long)index,groupId);
    if(index==0)
    {
        isATM=@"atm";
        [map removeAnnotations:annotationArray];
        for(BranchesObject *branchObj in banksPlotingPoditions)
        {
            if([branchObj.ATM isEqualToString:@"YES"])
            {
                [self focusBankPosition:branchObj];
            }
        }
        
        // [map setNeedsDisplay];
    }
    
    
    if(index==3)
    {
        isATM=@"ameen";
        [map removeAnnotations:annotationArray];
        for(BranchesObject *branchObj in banksPlotingPoditions)
        {
            //NSLog(@"%@", branchObj.branchType);
            
            if([branchObj.Ameen_window isEqualToString:@"YES"]|| [branchObj.branchType isEqual:@"AMEEN - ISLAMIC"])
            {
                [self focusBankPosition:branchObj];
            }
        }
        
        // [map setNeedsDisplay];
    }
    
    else if (index==1)
    {
        isATM=@"notatm";
        [map removeAnnotations:annotationArray];
        for(BranchesObject *branchObj in banksPlotingPoditions)
        {
            if([branchObj.ATM isEqualToString:@"NO"])
            {
                [self focusBankPosition:branchObj];
            }
        }
        
        // [map setNeedsDisplay];
    }
    else if (index==2)
    {
        isATM=@"both";
        [map removeAnnotations:annotationArray];
        
        for(BranchesObject *branchObj in banksPlotingPoditions)
        {
            [self focusBankPosition:branchObj];
        }
        
        //[map setNeedsDisplay];
    }
    
    
    
    
    selectedCityBranches=[[NSMutableArray alloc] init];
    NSArray *arry=[[NSArray alloc] initWithArray:[totalBranchesInfo objectForKey:[city.text lowercaseString]]];
    for(int i=0;i<arry.count;i++)
    {
        if([ isATM isEqualToString:@"atm"])
        {
            if([[[arry objectAtIndex:i] ATM] isEqualToString:@"YES"])
            {
                [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
            }
        }
        
        
        // AMEEN - ISLAMIC
        
        
        if([isATM isEqualToString:@"ameen"])
            
            
        {
            if([[[arry objectAtIndex:i] Ameen_window] isEqualToString:@"YES"] || [[[arry objectAtIndex:i] branchType] isEqualToString:@"AMEEN - ISLAMIC"])
            {
                [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
            }
        }
        else if([ isATM isEqualToString:@"notatm"])
        {
            if([[[arry objectAtIndex:i] ATM] isEqualToString:@"NO"])
            {
                [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
            }
        }
        else if([ isATM isEqualToString:@"both"])
        {
            [selectedCityBranches addObject:[[arry objectAtIndex:i] Address]];
        }
        
        
        
    }
    [autocompleteUrls removeAllObjects];
    [autocompleteUrls addObjectsFromArray:selectedCityBranches];
    [branchesTableView reloadData];
}

#pragma mark - Methods For Maps -

-(void)setFocusedPoint:(double )lat longt:(double)longt andPosition:(NSString *)position
{
    MKCoordinateRegion viewRegion;
    //Pakistan's Centre Point
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = lat;
    zoomLocation.longitude= longt;
    
    if([position isEqualToString:@"Pakistan Centre"])
    {
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 800.0*METERS_PER_MILE, 800.0*METERS_PER_MILE);
    }
    else
    {
        viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 2.0*METERS_PER_MILE, 2.0*METERS_PER_MILE);
    }
    
    [map setRegion:viewRegion animated:YES];
}

-(void)focusBankPosition:(BranchesObject *)branchInfo
{
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [branchInfo.latitude floatValue];
    zoomLocation.longitude= [branchInfo.longitude floatValue];
    
    ParkPlaceMark *placemark=[[ParkPlaceMark alloc] initWithCoordinate:zoomLocation];
    placemark.coordinate = zoomLocation;
    placemark.title = [NSString stringWithFormat:@"%@",branchInfo.branch_name];
    //  [placemark setTitle:[NSString stringWithFormat:@"%@",branchInfo.branch_name]];
    //[placemark setSubtitle:[NSString stringWithFormat:@"%@ %@",branchInfo.Address,branchInfo.city_name.uppercaseString ]];
    //  placemark.subtitle = [NSString stringWithFormat:@"%@ %@",branchInfo.Address,branchInfo.city_name.uppercaseString ];
    placemark.companyID=(NSNumber*)branchInfo.branch_id;
    [annotationArray addObject:placemark];
    [map addAnnotation:placemark];
    //NSLog(@"lat:%f..Long:%f...Title:%@...Subtitle:%@",zoomLocation.latitude,zoomLocation.longitude,placemark.title,placemark.subtitle
    //      );
    
}

-(void)bgView:(MKPinAnnotationView *)view
{
    // view.backgroundColor=[UIColor clearColor];
    //view.pinColor=MKPinAnnotationColorRed;
    
    //    UIImage *img = [UIImage imageNamed:@"testAnnotation"];
    //    view.image = img;
    // view.image=pinkAnotImage;
    //   view.image=[UIImage imageNamed:@"Pin-Red.png"];
    //  view.frame=CGRectMake(view.frame.origin.x, view.frame.origin.y, 32, 44);
}

#pragma mark - MapKit -

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // [view setCanShowCallout:NO];
    // [view setSelected:NO animated:YES];
    for (id currentAnnotation in mapView.annotations) {
        if ([currentAnnotation isKindOfClass:[ParkPlaceMark class]]) {
            [map deselectAnnotation:currentAnnotation animated:YES];
        }
    }
    //NSLog(@"calloutAccessoryControlTapped:");
    ParkPlaceMark *tappedSite = [view annotation];
    NSNumber *companyIDThatWasClickedExample = [tappedSite companyID];
    for(BranchesObject *branchObj in banksPlotingPoditions){
        if((NSNumber*)branchObj.branch_id==companyIDThatWasClickedExample){
            [bnk_Branch setText:[NSString stringWithFormat:@"%@\n%@",branchObj.branch_name,branchObj.Address]];
            [bnk_City setText:branchObj.city_name.uppercaseString];
            if([branchObj.Fax_no isEqualToString:@""]){
                bnk_Faxno.hidden=TRUE;
                lbl_Faxno.hidden=TRUE;
            }
            else{
                bnk_Faxno.hidden=FALSE;
                lbl_Faxno.hidden=FALSE;
                [bnk_Faxno setText:branchObj.Fax_no];
            }
            if([branchObj.Contact_no isEqualToString:@""]){
                bnk_Phoneno.hidden=TRUE;
                lbl_Phoneno.hidden=TRUE;
            }
            else{
                bnk_Phoneno.hidden=FALSE;
                lbl_Phoneno.hidden=FALSE;
                [bnk_Phoneno setText:branchObj.Contact_no];
            }
            
            //  bnk_InfoPage.hidden=NO;
            
            [bnk_InfoPage setFrame:CGRectMake(self.view.frame.size.width,  map.frame.size.height/2-(bnk_InfoPage.frame.size.height/2), bnk_InfoPage.frame.size.width, bnk_InfoPage.frame.size.height)];
            // [bnk_InfoPage setCenter:CGPointMake(map.frame.origin.x+map.frame.size.width,  map.frame.size.height/2-(bnk_InfoPage.frame.size.height/2))];
            
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.6f];
            [UIView setAnimationDelegate:self];
            //CGAffineTransform t2 = CGAffineTransformMakeTranslation(map.center.x, 0);
            // CGAffineTransform t2 = CGAffineTransformMakeTranslation(-bnk_InfoPage.frame.size.width, 0);
            //bnk_InfoPage.transform = t2;
            [bnk_InfoPage setCenter:CGPointMake(map.frame.size.width/2,map.frame.size.height/2)];
            //bnk_InfoPage.alpha = 1;
            [UIView commitAnimations];
            [map addSubview:bnk_InfoPage];
            break;
        }
    }
    //NSLog(@"Id that is Clicked:%@",companyIDThatWasClickedExample);
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    // view.backgroundColor=[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.9];
    // view.layer.cornerRadius=5.0;
    //   view.image=[UIImage imageNamed:@"Pin-Green.png"];
    //    view.frame=CGRectMake(view.frame.origin.x, view.frame.origin.y, 32, 44);
    //    CGRect bgFrame=view.frame;
    //    bgFrame.size.width=30;
    //    bgFrame.size.height=30;
    //    view.frame=bgFrame;
    
    //    [UIView animateWithDuration:0.5
    //                          delay:0
    //                        options:UIViewAnimationOptionBeginFromCurrentState
    //                     animations:(void (^)(void)) ^{
    //                         view.transform=CGAffineTransformMakeScale(1.2, 1.2);
    //                     }
    //                     completion:^(BOOL finished){
    //                         //view.transform=CGAffineTransformIdentity;
    //                     }];
    
    NSNumber *companyIDThatWasClickedExample ;
    CLLocationCoordinate2D nextDestination;
    ParkPlaceMark *tappedSite = [view annotation];
    //NSLog(@"Tapped site%@",tappedSite);
    
    bnk_InfoPage.hidden=NO;
    
    if(![tappedSite.title isEqualToString:@"Current Location"])
    {
        companyIDThatWasClickedExample = [tappedSite companyID];
        MKAnnotationView *pinAnnotation = (MKAnnotationView *) view;
        
        
        // pinAnnotation.pinColor=MKPinAnnotationColorGreen;
        [self performSelector:@selector(bgView:) withObject:pinAnnotation afterDelay:5.0];
    }
    
    for(BranchesObject *branchObj in banksPlotingPoditions)
    {
        
        if((NSNumber*)branchObj.branch_id==companyIDThatWasClickedExample)
        {
            
            //NSLog(@"%@",branchObj.branch_id);
            //NSLog(@"%@",companyIDThatWasClickedExample);
            
            nextDestination=CLLocationCoordinate2DMake([branchObj.latitude doubleValue], [branchObj.longitude doubleValue]);
            //            if(CLCOORDINATES_EQUAL(nextDestination,destinationValue)){
            //
            //
            //            }
            //            else{
            destinationValue=nextDestination;
            if(netAvailable)
            {
                //[HUD showWhileExecuting:@selector(setRoutPoints) onTarget:self withObject:nil animated:YES];
                //[HUD showAnimated:YES whileExecutingBlock:^{[self setRoutPoints];}];
                [self mbHudProgress];
                [self performSelector:@selector(hudWasHidden) withObject:nil afterDelay:0.5];
                // [self setRoutPoints];
            }
            else{
                
            }
            [map addSubview:bnk_InfoPage];
            //                CGSize sizeText = [[NSString stringWithFormat:@"%@",branchObj.branch_name.uppercaseString] sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:12] constrainedToSize:CGSizeMake(150, CGRectGetHeight(bnk_Branch.frame))  lineBreakMode:UILineBreakModeTailTruncation];
            //            //    UILabel *lblTitolo = [[UILabel alloc] initWithFrame:CGRectMake(2,2,150,sizeText.height)];
            //                bnk_Branch.frame=CGRectMake(bnk_Branch.frame.origin.x, bnk_Branch.frame.origin.y, bnk_Branch.frame.size.width, sizeText.height);
            [bnk_Branch setText:[NSString stringWithFormat:@"%@",branchObj.branch_name]];
            
            //NSLog(@"%@",branchObj.branch_name);
            
            
            //                bnk_Branch.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            //                bnk_Branch.lineBreakMode = UILineBreakModeTailTruncation;
            //                bnk_Branch.numberOfLines = 0;
            //              CGSize sizeText2 = [[NSString stringWithFormat:@"%@",branchObj.city_name.uppercaseString] sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:12] constrainedToSize:CGSizeMake(150, CGRectGetHeight(bnk_City.frame))  lineBreakMode:UILineBreakModeTailTruncation];
            //                bnk_City.frame=CGRectMake(bnk_City.frame.origin.x, bnk_City.frame.origin.y, bnk_City.frame.size.width, sizeText2.height);
            
            [bnk_City setText:branchObj.city_name.uppercaseString];
            
            if([branchObj.Fax_no isEqualToString:@""])
            {
                bnk_Faxno.hidden=TRUE;
                lbl_Faxno.hidden=TRUE;
            }
            else
            {
                bnk_Faxno.hidden=FALSE;
                lbl_Faxno.hidden=FALSE;
                [bnk_Faxno setText:branchObj.ATM];
            }
            
            //                if([branchObj.Contact_no isEqualToString:@""]){
            //                    bnk_Phoneno.hidden=TRUE;
            //                    lbl_Phoneno.hidden=TRUE;
            //                }
            //                else{
            //                    bnk_Phoneno.hidden=FALSE;
            //                    lbl_Phoneno.hidden=FALSE;
            //                    [bnk_Phoneno setText:branchObj.Contact_no];
            //                }
            
            
            //  bnk_InfoPage.hidden=NO;
            
            [bnk_InfoPage setFrame:CGRectMake(self.view.frame.size.width,  map.frame.size.height/4,bnk_InfoPage.frame.size.width, bnk_InfoPage.frame.size.height)];
            
            
            //-(bnk_InfoPage.frame.size.height/4)
            
            
            
            
            //     [bnk_InfoPage setCenter:CGPointMake(map.frame.origin.x+map.frame.size.width,  map.frame.size.height/2-(bnk_InfoPage.frame.size.height/2))];
            
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.6];
            
            //CGAffineTransform t2 = CGAffineTransformMakeTranslation(map.center.x, 0);
            // CGAffineTransform t2 = CGAffineTransformMakeTranslation(-bnk_InfoPage.frame.size.width, 0);
            //bnk_InfoPage.transform = t2;
            
            [bnk_InfoPage setCenter:map.center];
            //bnk_InfoPage.alpha = 1;
            [UIView commitAnimations];
            
            break;
            
            //[self setRoutPoints];
            // if(overLayCount!=0){
            // [map removeOverlay:self->objPolyline];
            //}
            // overLayCount=overLayCount+1;
            // }
            break;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinAnnotation = nil;
    
    if(annotation != mV.userLocation)
    {
        static NSString *defaultPinID = @"myPin";
        pinAnnotation = (MKAnnotationView *)[mV dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinAnnotation == nil )
            pinAnnotation = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        
        pinAnnotation.canShowCallout = NO;
        pinAnnotation.enabled=YES;
        //NSLog(@"%@",annotation.title);
        
        
        if ([isATM isEqual:@"both"]) {
            
            
            for(BranchesObject *branchObj in banksPlotingPoditions)
            {
                if ([branchObj.branch_name isEqual:annotation.title]) {
                    
                    if ([branchObj.Ameen_window isEqual:@"YES"] ||[branchObj.branchType isEqual:@"AMEEN - ISLAMIC"] ) {
                        
                        pinAnnotation.image = [[UIImage alloc] init];
                        pinAnnotation.image = [UIImage imageNamed:@"ameen.png"];
                    }
                    
                    //AMEEN - ISLAMIC
                    
                    else{
                        pinAnnotation.image = [[UIImage alloc] init];
                        pinAnnotation.image = [UIImage imageNamed:@"testAnnotation.png"];
                        
                    }
                    
                    
                }
                
                
                
            }
        }
        
        else if ([isATM isEqual:@"atm"]){
            
            pinAnnotation.image = [[UIImage alloc] init];
            pinAnnotation.image = [UIImage imageNamed:@"cash.png"];
            
            
        }
        
        else if ([isATM isEqual:@"ameen"]) {
            
            
            pinAnnotation.image = [[UIImage alloc] init];
            pinAnnotation.image = [UIImage imageNamed:@"ameen.png"];
            
        }
        
        // pinAnnotation.pinColor=MKPinAnnotationColorGreen;
        // pinAnnotation.image=[UIImage imageNamed:@"pinGreen.png"];
        //   pinAnnotation.frame=CGRectMake(pinAnnotation.frame.origin.x, pinAnnotation.frame.origin.y, 32, 44);
        //instatiate a detail-disclosure button and set it to appear on right side of annotation
        // UIImage *image = [UIImage imageNamed:@"Inquiry.png"];
        //UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
        //pinAnnotation.leftCalloutAccessoryView = imgView;
        //777       UIImage* image = nil;
        //not retina
        // UIGraphicsBeginImageContext(pinAnnotation.frame.size);
        //retina
        //        UIGraphicsBeginImageContextWithOptions(pinAnnotation.frame.size, NO, 2.0);
        //        [pinAnnotation.layer renderInContext: UIGraphicsGetCurrentContext()];
        //        image = UIGraphicsGetImageFromCurrentImageContext();
        //        UIGraphicsEndImageContext();
        //       // pinAnnotation.image=image;
        //        NSData* imgData = UIImagePNGRepresentation(image);
        //        NSString* targetPath = [NSString stringWithFormat:@"%@/%@", [self writablePath], @"thisismypin.png" ];
        //        [imgData writeToFile:targetPath atomically:YES];
        
        CGSize sizeText = [annotation.title sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:12] constrainedToSize:CGSizeMake(150, CGRectGetHeight(pinAnnotation.frame))                                 lineBreakMode:UILineBreakModeTailTruncation];
        UILabel *lblTitolo = [[UILabel alloc] initWithFrame:CGRectMake(2,2,150,sizeText.height)];
        lblTitolo.text = [NSString stringWithString:annotation.title];
        lblTitolo.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        lblTitolo.lineBreakMode = UILineBreakModeTailTruncation;
        lblTitolo.numberOfLines = 0;
        // pinView.leftCalloutAccessoryView = lblTitolo;
        // [lblTitolo release];
        //        annotation.title=@"";
        //        ParkPlaceMark *placemark=[[ParkPlaceMark alloc] init];
        //        placemark=(ParkPlaceMark *)annotation;
        //        [placemark setTitle:@""];
        //        annotation=(id <MKAnnotation> )placemark;
        // [(ParkPlaceMark *)annotation setTitle:@""];
        
        
        //        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        //        infoButton.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
        //        infoButton.lineBreakMode=UILineBreakModeCharacterWrap;
        //        infoButton.frame=lblTitolo.frame;
        //        [infoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        [infoButton setTitle:lblTitolo.text forState:UIControlStateNormal];
        // infoButton.imageView.image=[UIImage imageNamed:@"Inquiry.png"];
        
        
        //pinAnnotation.canShowCallout = YES;
        
        
        //        UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"testAnnotation.png"]];
        //        [pinAnnotation addSubview:imgV];
        
        //        UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //        [rightButton addTarget:self action:@selector(writeSomething:) forControlEvents:UIControlEventTouchUpInside];
        //        [rightButton setTitle:annotation.title forState:UIControlStateNormal];
        //        pinAnnotation.rightCalloutAccessoryView = rightButton;
        //        pinAnnotation.canShowCallout = YES;
        //        pinAnnotation.draggable = YES;
        
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinAnnotation.rightCalloutAccessoryView = infoButton;
        
        
    }
    
    pinkAnotImage=pinAnnotation.image;
    return pinAnnotation;
}
-(NSString*) writablePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}
/* MKMapViewDelegate Meth0d -- for viewForOverlay*/
//- (MKOverlayView*)mapView:(MKMapView*)theMapView viewForOverlay:(id <MKOverlay>)overlay
//{
//    MKPolylineView *view = [[MKPolylineView alloc] initWithPolyline:objPolyline];
//    view.fillColor = [UIColor blueColor];
//    view.strokeColor = [UIColor blueColor];
//    view.lineWidth = 4;
//    return view;
//}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        // MKPolyline *route = overlay;
        MKPolylineRenderer *routeRenderer = [[MKPolylineRenderer alloc] initWithPolyline:objPolyline ];
        routeRenderer.strokeColor = [UIColor blueColor];
        routeRenderer.lineWidth = 2;
        return routeRenderer;
    }
    else return nil;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"locationManager:%@ didFailWithError:%@", manager, error);
    //   UIAlertView *errorAlert = [[UIAlertView alloc]
    //                              initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //   [errorAlert show];
    //    originValue=CLLocationCoordinate2DMake(24.856895f, 67.0423050f);
    //    [self setFocusedPoint:originValue.latitude longt:originValue.longitude andPosition:@" "];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    originValue=currentLocation.coordinate;
    
    
    
    //        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    //        [geocoder reverseGeocodeLocation:self.locationManager.location
    //                       completionHandler:^(NSArray *placemarks, NSError *error) {
    //                           //NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
    //
    //                           if (error){
    //                               //NSLog(@"Geocode failed with error: %@", error);
    //                               return;
    //
    //                           }
    //                            CLPlacemark *placemark = [placemarks objectAtIndex:0];
    //                         //  NSString * location = ([placemarks count] > 0) ? [[placemarks objectAtIndex:0] locality] : @"Not Found";
    //
    //
    //
    //                           //NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
    //                           //NSLog(@"placemark.country %@",placemark.country);
    //                           //NSLog(@"placemark.postalCode %@",placemark.postalCode);
    //                           //NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
    //                           //NSLog(@"placemark.locality %@",placemark.locality);
    //                           //NSLog(@"placemark.subLocality %@",placemark.subLocality);
    //                           //NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
    //
    //                       }];
    //Stop Location Manager
    [myLcationManager stopUpdatingLocation];
}
#pragma mark - IBActions -
-(IBAction)search:(UIButton*)seder
{
    if(selectedCityBranches.count<=0 && branchesTableView.hidden==YES){
        branchesTableView.hidden=NO;
    }
    else if(selectedCityBranches.count>0 && branchesTableView.hidden==NO){
        branchesTableView.hidden=YES;
    }
}

-(IBAction)slideView:(UIButton *)sender
{
    autocompleteTableView.hidden=YES;
    branchesTableView.hidden=YES;
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionTransitionCurlUp;
    
    [UIView transitionWithView:self.searchBar
                      duration:1.0
                       options:options
                    animations:^{ searchBar.hidden = YES;
                        
                    }
                    completion:NULL];
}

-(IBAction)slideViewDown:(UIButton *)sender
{
    searchBar.hidden = NO;
    
    //    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear;
    //
    //    [UIView transitionWithView:self.searchBar
    //                      duration:2.0
    //                       options:options
    //                    animations:^{ searchBar.hidden = NO;
    //
    //                    }
    //                    completion:NULL];
    
    searchBar.frame=CGRectMake(searchBar.frame.origin.x, searchBar.frame.origin.y-searchBar.frame.size.height, searchBar.frame.size.width, searchBar.frame.size.height);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    //    [UIView setAnimationDelay:1.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    searchBar.frame=CGRectMake(searchBar.frame.origin.x, mapBar.frame.size.height, searchBar.frame.size.width, searchBar.frame.size.height);
    
    [UIView commitAnimations];
    
}

-(IBAction)bnk_Close:(UIButton*)seder
{
    
    [self animationOut];
    for (id currentAnnotation in map.annotations)
    {
        
        if ([currentAnnotation isKindOfClass:[ParkPlaceMark class]])
        {
            
            [map deselectAnnotation:currentAnnotation animated:YES];
        }
    }
    
    [bnk_InfoPage removeFromSuperview];
}


-(IBAction)btn_back:(id)sender
{
    [self slide_right];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction) infoButtonPressed:(id) sender
{
    //    DetailViewController *dvc = [[DetailViewController alloc] init];
    //    [self.navigationController pushViewController:dvc animated:YES];
}

-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}



-(IBAction)getDirectionBtnTapped:(UIButton *)sender
{
    [self checkinternet];
    
    if(netAvailable)
    {
        [self mbHudProgress];
        [self performSelector:@selector(hudWasHidden) withObject:nil afterDelay:1.0];
        [self setRoutPoints];
    }
    else
    {
        
    }
}

-(IBAction)clearContext:(UIButton *)sender
{
    [map removeOverlays:map.overlays];
}

#pragma mark - Direction Code -
-(void)setRoutPoints{
    // Origin Location.
    //    CLLocationCoordinate2D loc1;
    //    loc1.latitude =24.856895f;
    //    loc1.longitude =67.0423050f;
    ParkPlaceMark *origin = [[ParkPlaceMark alloc] initWithCoordinate:originValue];
    //[map addAnnotation:origin];
    
    // Destination Location.
    //    CLLocationCoordinate2D loc2;
    //    loc2.latitude =31.488407;
    //    loc2.longitude =74.358215;
    ParkPlaceMark *destination = [[ParkPlaceMark alloc] initWithCoordinate:destinationValue];
    //    [map addAnnotation:destination];
    
    //    if(arrRoutePoints) // Remove all annotations
    //    {
    //        //arrRoutePoints=[[NSArray alloc] init];
    //       // [map removeAnnotations:[map annotations]];
    //    }
    arrRoutePoints = [self getRoutePointFrom:origin to:destination];
    [self drawRoute];
    //[self performSelector:@selector(drawRoute) withObject:nil];
    //[NSThread detachNewThreadSelector:@selector(drawRoute) toTarget:self withObject:nil];
    // [self centerMap];
    return;
}

/* This will get the route coordinates from the google api. */

- (NSArray*)getRoutePointFrom:(ParkPlaceMark *)origin to:(ParkPlaceMark *)destination
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError *error;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSUTF8StringEncoding error:&error];
    // NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"points:\\\"([^\\\"]*)\\\"" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:apiResponse options:0 range:NSMakeRange(0, [apiResponse length])];
    NSString *encodedPoints = [apiResponse substringWithRange:[match rangeAtIndex:1]];
    return [self decodePolyLine:[encodedPoints mutableCopy]];
}

- (NSMutableArray *)decodePolyLine:(NSMutableString *)encodedString
{
    [encodedString replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [encodedString length])];
    NSInteger len = [encodedString length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encodedString characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encodedString characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

- (void)drawRoute
{
    
    int numPoints = [arrRoutePoints count];
    if (numPoints > 1)
    {
        CLLocationCoordinate2D* coords = malloc(numPoints * sizeof(CLLocationCoordinate2D));
        for (int i = 0; i < numPoints; i++)
        {
            CLLocation* current = [arrRoutePoints objectAtIndex:i];
            coords[i] = current.coordinate;
        }
        
        self.objPolyline = [MKPolyline polylineWithCoordinates:coords count:numPoints];
        //  previousPolyline=objPolyline;
        
        //[self.map addOverlays:(__bridge NSArray *)(coords)];
        //[self performSelectorInBackground:@selector(overlayMethod) withObject:nil];
        [map addOverlay:self.objPolyline];
        
        [map setNeedsDisplay];
        free(coords);
    }
    
    return;
}

-(void)overlayMethod
{
    [map addOverlay:objPolyline];
}

- (void)centerMap
{
    //[indicatorView removeFromSuperview];
    [indicatorView stopAnimating];
    MKCoordinateRegion region;
    
    CLLocationDegrees maxLat = -90;
    CLLocationDegrees maxLon = -180;
    CLLocationDegrees minLat = 90;
    CLLocationDegrees minLon = 180;
    
    for(int idx = 0; idx < arrRoutePoints.count; idx++)
    {
        CLLocation* currentLocations = [arrRoutePoints objectAtIndex:idx];
        
        if(currentLocations.coordinate.latitude > maxLat)
            maxLat = currentLocations.coordinate.latitude;
        if(currentLocations.coordinate.latitude < minLat)
            minLat = currentLocations.coordinate.latitude;
        if(currentLocations.coordinate.longitude > maxLon)
            maxLon = currentLocations.coordinate.longitude;
        if(currentLocations.coordinate.longitude < minLon)
            minLon = currentLocations.coordinate.longitude;
    }
    
    region.center.latitude     = (maxLat + minLat) / 2;
    region.center.longitude    = (maxLon + minLon) / 2;
    region.span.latitudeDelta  = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
    [map setRegion:region animated:YES];
    
    return;
}



-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    // If the current element name is equal to "geoname" then initialize the temporary dictionary.
    
    //NSLog(@"%@", elementName);
    
    if ([elementName isEqualToString:@"ArrayOfFeatureClass3"]) {
        self.arrNeighboursData = [[NSMutableArray alloc] init];
        totalBranchesInfo=[[NSMutableDictionary alloc] init];
        banksPlotingPoditions = [[NSMutableArray alloc] init];
        
    }
    if ([elementName isEqualToString:@"FeatureClass3"]) {
        self.dictTempDataStorage = [[NSMutableDictionary alloc] init];
        
    }
    
    if ([elementName isEqualToString:@"FeatureClass2"]) {
        self.dictTempDataStorage = [[NSMutableDictionary alloc] init];
    }
    
    
    
    
    
    // Keep the current element.
    self.currentElement = elementName;
}




-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    //NSLog(@"%@", elementName);
    if ([elementName isEqualToString:@"FeatureClass3"]) {
        // If the closing element equals to "geoname" then the all the data of a neighbour country has been parsed and the dictionary should be added to the neighbours data array.
        [self.arrNeighboursData addObject:[[NSDictionary alloc] initWithDictionary:self.dictTempDataStorage]];
    }
    if ([elementName isEqualToString:@"FeatureClass2"]) {
        // If the closing element equals to "geoname" then the all the data of a neighbour country has been parsed and the dictionary should be added to the neighbours data array.
        [citiesArray addObject:[self.dictTempDataStorage objectForKey:@"City"]];
    }
    
    else if ([elementName isEqualToString:@"S_NO"]){
        // If the country name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"S_NO"];
    }
    else if ([elementName isEqualToString:@"BranchCode"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"BranchCode"];
    }
    else if ([elementName isEqualToString:@"BranchName"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"BranchName"];
    }
    
    else if ([elementName isEqualToString:@"BranchAddress"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"BranchAddress"];
    }
    
    else if ([elementName isEqualToString:@"City"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"City"];
    }
    
    else if ([elementName isEqualToString:@"Region"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Region"];
    }
    
    else if ([elementName isEqualToString:@"TypeOfBranch"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"TypeOfBranch"];
    }
    else if ([elementName isEqualToString:@"Ameen_Window"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Ameen_Window"];
    }
    
    
    else if ([elementName isEqualToString:@"Saturday_Opening"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Saturday_Opening"];
    }
    
    else if ([elementName isEqualToString:@"ATM_Available"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"ATM_Available"];
    }
    else if ([elementName isEqualToString:@"Lockers_Available"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Lockers_Available"];
    }
    
    else if ([elementName isEqualToString:@"Branch_Timing"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Branch_Timing"];
    }
    
    else if ([elementName isEqualToString:@"LONGITUDE"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"LONGITUDE"];
    }
    
    else if ([elementName isEqualToString:@"LATITUED"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"LATITUED"];
    }
    
    else if ([elementName isEqualToString:@"Contact_No_1"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Contact_No_1"];
    }
    
    else if ([elementName isEqualToString:@"Contact_No_1"]){
        // If the toponym name element was found then store it.
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"Contact_No_1"];
    }
    
    
    
    
    // Clear the mutable string.
    [self.foundValue setString:@""];
}

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    //NSLog(@"%@", [parseError localizedDescription]);
}


//<FeatureClass3>
//<S_NO>744</S_NO>
//<BranchCode>434</BranchCode>
//<BranchName>BOULTON MARKET</BranchName>
//<BranchAddress>SR - 3- II DILKUSHA HOTEL , NICOL ROAD, KARACHI</BranchAddress>
//<City>KARACHI</City>
//<Region>KARACHI CENTRAL</Region>
//<TypeOfBranch>AMEEN - ISLAMIC</TypeOfBranch>
//<Ameen_Window>NO</Ameen_Window>
//<Saturday_Opening>YES</Saturday_Opening>
//<ATM_Available>NO</ATM_Available>
//<Lockers_Available>NO</Lockers_Available>
//<Branch_Timing>
//09:00 AM to 5:30 PM (Monday to Thrusday) 09:00 AM to 06:00 PM (Friday) 09:00 AM to 01:30 PM (Saturday)
//</Branch_Timing>
//<LONGITUDE>67.02176</LONGITUDE>
//<LATITUED>24.86539</LATITUED>
//<Contact_No_1>0321-2104963</Contact_No_1>
//<Contact_No_2/>
//</FeatureClass3>

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    // Store the found characters if only we're interested in the current element.
    
    //NSLog(@"%@", self.currentElement);
    if ([self.currentElement isEqualToString:@"S_NO"] ||
        [self.currentElement isEqualToString:@"BranchCode"] ||
        [self.currentElement isEqualToString:@"BranchName"] ||
        [self.currentElement isEqualToString:@"BranchAddress"] ||
        [self.currentElement isEqualToString:@"City"] ||
        [self.currentElement isEqualToString:@"Region"] ||
        [self.currentElement isEqualToString:@"TypeOfBranch"] ||
        [self.currentElement isEqualToString:@"Ameen_Window"] ||
        [self.currentElement isEqualToString:@"Saturday_Opening"] ||
        [self.currentElement isEqualToString:@"ATM_Available"] ||
        [self.currentElement isEqualToString:@"Lockers_Available"] ||
        [self.currentElement isEqualToString:@"Branch_Timing"] ||
        [self.currentElement isEqualToString:@"LONGITUDE"] ||
        [self.currentElement isEqualToString:@"LATITUED"] ||
        [self.currentElement isEqualToString:@"Contact_No_1"]
        
        ) {
        
        
        
        if (![string isEqual:@"\n    "] && ![string isEqual:@"\n  "] && ![string isEqual:@"\n "] && ![string isEqual:@"\n"]) {
            [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            [self.foundValue appendString:string];
        }
    }
}


-(void) Branches_By_city:(NSString *)strIndustry
{
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://61.5.156.98/NetServiceRegionUBL/Service.asmx"]];        //baseURL];
        
        
        manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
        
        //    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:access_key_feedback,@"karachi",nil] forKeys:[NSArray arrayWithObjects:@"AccessKey",@"City", nil]];
        
        //NSLog(@"%@",dictparam);
        
        //     [manager.requestSerializer setTimeoutInterval:time_out];
        
        
        
        [manager POST:@"All_RegionsByCityFinal" parameters:dictparam
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  NSDictionary* dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSString* response;
                  
                  
                  
                  
                  self.xmlParser = [[NSXMLParser alloc] initWithData:responseObject];
                  self.xmlParser.delegate = self;
                  
                  // Initialize the mutable string that we'll use during parsing.
                  self.foundValue = [[NSMutableString alloc] init];
                  
                  // Start parsing.
                  [self.xmlParser parse];
                  
                  
                  
                  
                  
                  
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  gblclass.arr_acct_statement1=[[NSMutableArray alloc] init];
                  
                  NSMutableArray* arr2;
                  
                  arr2=[[NSMutableArray alloc] init];
                  arr2=[(NSDictionary *) dic objectForKey:@"BranchCode"];
                  //NSLog(@"%@",arr2);
                  
                  
                  for (dic in gblclass.arr_acct_statement1)
                  {
                      response=[dic objectForKey:@"ResponseCode"];
                      //NSLog(@"Response %@",[dic objectForKey:@"ResponseCode"]);
                  }
                  
                  [hud hideAnimated:YES];
                  
                  if ([response isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=@"Comment Seccessfully";
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Comment Seccessfully" preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                      [alert addAction:ok];
                      //
                      //                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic objectForKey:@"ResponseCode"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      //                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ResponseCode"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                      [alert addAction:ok];
                      //
                      //                      [self presentViewController:alert animated:YES completion:nil];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             // [mine myfaildata];
             [hud hideAnimated:YES];
             
             gblclass.custom_alert_msg=[error localizedDescription];
             gblclass.custom_alert_img=@"0";
             
             mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
             vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
             vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
             vc.view.alpha = alpha1;
             [self presentViewController:vc animated:NO completion:nil];
             
             
             
             //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
             //
             //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
             //                  [alert addAction:ok];
             //                  [self presentViewController:alert animated:YES completion:nil];
             
             
         }];
        
    } @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        //NSLog(@"%@",exception.reason);
    }
    
}




#pragma mark - Consume XML -
-(void)getBranchesInfo:(NSString*) cityLocation

{
    
    if ([cityLocation isEqual:@""] || cityLocation == nil) {
        
        
        cityLocation = @"karachi";
    }
    
    banksPlotingPoditions=[[NSMutableArray alloc] init];
    citiesArray=[[NSMutableArray alloc] init];
    
    NSString *urlStr2 = [NSString stringWithFormat:@"http://61.5.156.98/NetServiceRegionUBL/Service.asmx/All_DistinctCitiesFinal?AccessKey=%@",access_key_feedback];
    NSURLRequest * urlRequest2 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr2]];
    NSURLResponse * response2 = nil;
    NSError * error2 = nil;
    NSData * data2 = [NSURLConnection sendSynchronousRequest:urlRequest2
                                           returningResponse:&response2
                                                       error:&error2];
    
    if (error2== nil)
    {
        // Parse data here
        
        
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        
        self.xmlParser = [[NSXMLParser alloc] initWithData:data2];
        self.xmlParser.delegate = self;
        
        // Initialize the mutable string that we'll use during parsing.
        self.foundValue = [[NSMutableString alloc] init];
        
        // Start parsing.
        [self.xmlParser parse];
        
    }
    else {
        
        gblclass.custom_alert_msg=@"Internet not connected";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        
        //        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Internet not connected" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alert2 show];
        
    }
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"http://61.5.156.98/NetServiceRegionUBL/Service.asmx/All_RegionsByCityFinal?AccessKey=%@&City=%@",access_key_feedback,@"karachi"];
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];
    
    if (error == nil)
    {
        // Parse data here
        
        
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        //NSLog(@"asd");
        
        self.xmlParser = [[NSXMLParser alloc] initWithData:data];
        self.xmlParser.delegate = self;
        
        // Initialize the mutable string that we'll use during parsing.
        self.foundValue = [[NSMutableString alloc] init];
        
        // Start parsing.
        [self.xmlParser parse];
        
    }
    else {
        
        gblclass.custom_alert_msg=@"Internet not connected";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Internet not connected" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alert2 show];
        
    }
    
    
    
    
    
    
    for (int i =0; i<self.arrNeighboursData.count; i++) {
        
        bankInfo = [[NSMutableArray alloc] init];
        BranchesObject *branchObj=[[BranchesObject alloc] init];
        branchObj.branch_name = [self.arrNeighboursData[i] objectForKey:@"BranchName"];
        branchObj.Address = [self.arrNeighboursData[i] objectForKey:@"BranchAddress"];
        branchObj.branch_id = [self.arrNeighboursData[i] objectForKey:@"S_NO"];
        branchObj.Contact_no = [self.arrNeighboursData[i] objectForKey:@"Contact_No_1"];
        branchObj.Lockers_Available = [self.arrNeighboursData[i] objectForKey:@"Lockers_Available"];
        branchObj.Ameen_window = [self.arrNeighboursData[i] objectForKey:@"Ameen_Window"];
        branchObj.city_name = [self.arrNeighboursData[i] objectForKey:@"City"];
        branchObj.longitude = [self.arrNeighboursData[i] objectForKey:@"LONGITUDE"];
        branchObj.latitude = [self.arrNeighboursData[i] objectForKey:@"LATITUED"];
        branchObj.ATM = [self.arrNeighboursData[i] objectForKey:@"ATM_Available"];
        branchObj.branchType = [self.arrNeighboursData[i] objectForKey:@"TypeOfBranch"];
        
        [bankInfo addObject:branchObj];
        [banksPlotingPoditions addObject:branchObj];
        
        
        NSString *str = [branchObj.city_name stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        if (i != 0) {
            NSArray *arr = [totalBranchesInfo objectForKey:[str lowercaseString]];
            [((NSMutableArray*)[totalBranchesInfo objectForKey:[str lowercaseString]]) addObject:branchObj];
        }
        
        
        else {
            [totalBranchesInfo setValue:bankInfo forKey:[str lowercaseString]];
        }
        // [citiesArray addObject:str];
        
        
    }
    
    [self saveinfo];
    //
    //     TBXML *tbxml3 = [[TBXML alloc] initWithURL:[NSURL URLWithString:@"http://61.5.156.104/ublimages/UBLImg.asp?rType=1"]];
    //
    //
    //   // [self Branches_By_city:@""];
    //
    //
    //    //adbannerarr = [[NSMutableArray alloc] init];
    //
    //    // Obtain root element
    //    TBXMLElement * root = tbxml3.rootXMLElement;
    //
    //    int count=0;
    //
    //    // if root element is valid
    //    if (root) {
    //        //, [TBXML attributeValue:attribute]
    //        TBXMLElement * cityName =[TBXML childElementNamed:@"city" parentElement:root];
    //        //TBXMLAttribute * attribute = root->firstAttribute;
    //
    //
    //        //    //NSLog(@"%@",cityName);
    //
    //
    ////        if ([self.currentElement isEqualToString:@"S_NO"] ||
    ////            [self.currentElement isEqualToString:@"BranchCode"] ||
    ////            [self.currentElement isEqualToString:@"BranchName"] ||
    ////            [self.currentElement isEqualToString:@"BranchAddress"] ||
    ////            [self.currentElement isEqualToString:@"City"] ||
    ////            [self.currentElement isEqualToString:@"Region"] ||
    ////            [self.currentElement isEqualToString:@"TypeOfBranch"] ||
    ////            [self.currentElement isEqualToString:@"Ameen_Window"] ||
    ////            [self.currentElement isEqualToString:@"Saturday_Opening"] ||
    ////            [self.currentElement isEqualToString:@"ATM_Available"] ||
    ////            [self.currentElement isEqualToString:@"Lockers_Available"] ||
    ////            [self.currentElement isEqualToString:@"Branch_Timing"] ||
    ////            [self.currentElement isEqualToString:@"LONGITUDE"] ||
    ////            [self.currentElement isEqualToString:@"LATITUED"] ||
    ////            [self.currentElement isEqualToString:@"Contact_No_1"]
    //
    //
    //
    //
    //        while(cityName)
    //        {
    //            bankInfo = [[NSMutableArray alloc] init];
    //            TBXMLAttribute * attribute = cityName->firstAttribute;
    //
    //            TBXMLElement * author3 = [TBXML childElementNamed:@"city_Detail" parentElement:cityName];
    //
    //
    //            while (author3 != nil) {
    //                //  NSDictionary *branchInfoDic = [[NSMutableDictionary alloc] init];
    //                BranchesObject *branchObj=[[BranchesObject alloc] init];
    //                TBXMLElement * cityNameStr = [TBXML childElementNamed:@"city_name" parentElement:author3];
    //                TBXMLElement * branchIdStr = [TBXML childElementNamed:@"branch_id" parentElement:author3];
    //                TBXMLElement * branchNameStr = [TBXML childElementNamed:@"branch_name" parentElement:author3];
    //                TBXMLElement * latitudeStr = [TBXML childElementNamed:@"latitude" parentElement:author3];
    //                TBXMLElement * longitudeStr = [TBXML childElementNamed:@"longitude" parentElement:author3];
    //                TBXMLElement * addressStr = [TBXML childElementNamed:@"Address" parentElement:author3];
    //                //   TBXMLElement * contactNoStr = [TBXML childElementNamed:@"Contact_no" parentElement:author3];
    //                //   TBXMLElement * faxNoStr = [TBXML childElementNamed:@"Fax_no" parentElement:author3];
    //                TBXMLElement * atm = [TBXML childElementNamed:@"ATM" parentElement:author3];
    //
    //                branchObj.city_name=[TBXML textForElement:cityNameStr];
    //                branchObj.branch_id=[TBXML textForElement:branchIdStr];
    //                branchObj.branch_name=[TBXML textForElement:branchNameStr];
    //                branchObj.latitude=[TBXML textForElement:latitudeStr];
    //                branchObj.longitude=[TBXML textForElement:longitudeStr];
    //                branchObj.Address=[TBXML textForElement:addressStr];
    //                // branchObj.Contact_no=[TBXML textForElement:contactNoStr];
    //                // branchObj.Fax_no=[TBXML textForElement:faxNoStr];
    //                branchObj.ATM=[TBXML textForElement:atm];
    //
    //                [bankInfo addObject:branchObj];
    //                [banksPlotingPoditions addObject:branchObj];
    //                author3 = [TBXML nextSiblingNamed:@"city_Detail" searchFromElement:author3];
    //
    //            }
    //
    //            [totalBranchesInfo setValue:bankInfo forKey:[[TBXML attributeValue:attribute]lowercaseString]];
    //
    //            [citiesArray addObject:[TBXML attributeValue:attribute]];
    //            cityName = [TBXML nextSiblingNamed:@"city" searchFromElement:cityName];
    //        }
    //
    //
    //        //NSLog(@"BankInfo Dictionary:%@",totalBranchesInfo);
    //        [self saveinfo];
    //    }
}

-(void)saveinfo
{
    
    NSString *imgdirinside2,*imgdirinside,*imgdirinside3;
    
    imgdirinside = [infoPath  stringByAppendingPathComponent:@"plotingPositions.archive"];
    imgdirinside2 = [infoPath  stringByAppendingPathComponent:@"totalInfo.archive"];
    imgdirinside3 = [infoPath  stringByAppendingPathComponent:@"citiesInfo.archive"];
    
    //
    //    BOOL success = [NSKeyedArchiver archiveRootObject:totalBranchesInfo  toFile: imgdirinside2];
    //    BOOL positionSuccess=[NSKeyedArchiver archiveRootObject:banksPlotingPoditions  toFile: imgdirinside];
    //    BOOL citiesSuccess=[NSKeyedArchiver archiveRootObject:citiesArray  toFile: imgdirinside3];
    
    //    if (success==TRUE&&positionSuccess==TRUE&&citiesSuccess==TRUE)
    //    {
    //        //NSLog(@"Successful write");
    //    }
    //    else
    //    {
    //        //NSLog(@"Failed write");
    //    }
}

-(void)getinfo
{
    
    NSString *imgdirinside2,*imgdirinside,*imgdirinside3;
    imgdirinside = [infoPath  stringByAppendingPathComponent:@"plotingPositions.archive"];
    imgdirinside2 = [infoPath  stringByAppendingPathComponent:@"totalInfo.archive"];
    imgdirinside3 = [infoPath  stringByAppendingPathComponent:@"citiesInfo.archive"];
    
    
    NSMutableDictionary *newDict=[[NSMutableDictionary alloc] init];
    NSMutableArray *newArray=[[NSMutableArray alloc] init];
    NSMutableArray *newcitiesArray=[[NSMutableArray alloc] init];
    
    newDict= [NSKeyedUnarchiver unarchiveObjectWithFile: imgdirinside2];
    newArray=[NSKeyedUnarchiver unarchiveObjectWithFile: imgdirinside];
    newcitiesArray=[NSKeyedUnarchiver unarchiveObjectWithFile: imgdirinside3];
    if(newDict&&newArray&&newcitiesArray){
        //NSLog(@"Successful get");
        totalBranchesInfo=[NSMutableDictionary dictionaryWithDictionary:newDict];
        banksPlotingPoditions=[NSMutableArray arrayWithArray:newArray];
        citiesArray=[NSMutableArray arrayWithArray:newcitiesArray];
        //NSLog(@"Get  array:%@...Ploting Array:%@,...Cities:%@",newDict,newArray,newcitiesArray);
    }
    else{
        //NSLog(@"Failed to get");
    }
    
}


-(IBAction)btn_home:(id)sender
{
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Mpin_login"];
    //
    //    [self presentModalViewController:vc animated:YES];
    
}



-(IBAction)btn_feature:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        gblclass.chk_qr_demo_login=@"1";
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"]; //   receive_qr_login
        [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_offer:(id)sender
{
    
//    NSURL *jsCodeLocation;
//    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
//    RCTRootView *rootView =
//    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
//                         moduleName        : @"peekaboo"
//                         initialProperties :
//     @{
//       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
//       @"peekabooEnvironment" : @"production",
//       @"peekabooSdkId" : @"UBL_SDK",
//       @"peekabooTitle" : @"UBL Bank Offers",
//       @"peekabooSplashImage" : @"true",
//       @"peekabooWelcomeMessage" : @"Please select your City to view the list of discounted facilities being offered by UBL",
//       @"peekabooGooglePlacesApiKey" : @"AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
//       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
//       @"peekabooPoweredByFooter" : @"false",
//       @"peekabooColorPrimary" : @"#007DC0",
//       @"peekabooColorPrimaryDark" : @"#005481",
//       @"peekabooColorStatus" : @"#2d2d2d",
//       @"peekabooColorSplash" : @"#FFFFFF",
//       @"peekabooType": @"deals",
//       
//       }
//                          launchOptions    : nil];
//    UIViewController *vc = [[UIViewController alloc] init];
//    vc.view = rootView;
//    [self presentViewController:vc animated:YES completion:nil];
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
    
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    //        // gblclass.tab_bar_login_pw_check=@"login";
    //
    //        CATransition *transition = [ CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [self.view.window.layer addAnimation:transition forKey:nil];
    //
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //    }
    
}

-(IBAction)btn_find_us:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        // gblclass.tab_bar_login_pw_check=@"login";
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

-(IBAction)btn_faq:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}


- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
}



@end

