//
//  Branch_Act_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Ubl_fund_invstment_VC.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"

@interface Ubl_fund_invstment_VC()
{
    NSMutableArray* account_name;
    
    NSMutableArray* account_number;
    
    NSMutableArray* type;
    NSMutableArray *account_typedesc;
    NSMutableArray *account_currencydesc;
    
    NSMutableArray* balc_date;
    NSMutableArray* avail_bal;
    
    GlobalStaticClass *gblclass;
    GnbRefreshAcct *classObj ;
    NSString *flag;
    //int *dictionaryindex;
    //  NSNumber *number;
    NSDictionary *dic;
    UIButton *exesender;
    
}
@end

@implementation Ubl_fund_invstment_VC
@synthesize ddMenu;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //account_name=[[NSArray alloc] init];
    flag=@"1";
    // dictionaryindex =0;
    gblclass =  [GlobalStaticClass getInstance];
    account_name = [[NSMutableArray alloc]init];
    
    account_number= [[NSMutableArray alloc]init];
    account_currencydesc = [[NSMutableArray alloc]init];
    account_typedesc = [[NSMutableArray alloc]init];
    type= [[NSMutableArray alloc]init];
    
    balc_date= [[NSMutableArray alloc]init];
    avail_bal= [[NSMutableArray alloc]init];
    
    //int value = [number intValue];
    NSUInteger *set;
    for (dic in gblclass.actsummaryarr)
    {
        set = [gblclass.actsummaryarr indexOfObject:dic];
        NSLog(@"%d",set);
        classObj = [[GnbRefreshAcct alloc] initWithDictionary:dic];
        [account_typedesc addObject:classObj.accountTypeDesc];
        
        //    for(int i = 0;i<[account_typedesc count];i++  ){
        //        NSDictionary *dic =[gblclass.actsummaryarr objectAtIndex:i];
        //        GnbRefreshAcct *classObj = [[GnbRefreshAcct alloc] initWithDictionary:dic];
        //
        // NSLog(@"%d",dictionaryindex);
        NSLog(@"ACount des %@",[account_typedesc objectAtIndex:set] );
        if([[account_typedesc objectAtIndex:set] isEqual:@"UBL FUNDS"]){
            
            [account_name addObject:classObj.accountName];
            [account_number addObject:classObj.accountNo];
            
            if([classObj.ccy isEqualToString:nil] || [classObj.ccy isEqualToString:@" "] || [classObj.ccy isEqualToString:@"null"] || [classObj.ccy isEqualToString:@"NULL"] || [classObj.ccy length]==0){
                [account_currencydesc addObject:@" "];
            }else{
                [account_currencydesc addObject:classObj.ccy];
            }
            //NSLog(classObj.accountTypeDesc);
            ///type name
            if([classObj.accountType isEqualToString:nil] || [classObj.accountType isEqualToString:@" "] || [classObj.accountType isEqualToString:@"null"] || [classObj.accountType isEqualToString:@"NULL"] || [classObj.accountType length]==0){
                [type addObject:@"N/A"];
            }else{
                [type addObject:classObj.accountType];
            }
            ////balance date
            if([classObj.availableBalanceTime isEqualToString:nil] || [classObj.availableBalanceTime isEqualToString:@" "] || [classObj.availableBalanceTime isEqualToString:@"null"] || [classObj.availableBalanceTime isEqualToString:@"NULL"] || [classObj.availableBalanceTime length]==0){
                [balc_date addObject:@"N/A"];
            }else{
                [balc_date addObject:classObj.availableBalanceTime];//  [branch addObject:classObj.branchName];
            }
            
            ///avail balance
            if([classObj.balance isEqualToString:nil] || [classObj.balance isEqualToString:@" "] || [classObj.balance isEqualToString:@"null"] || [classObj.balance isEqualToString:@"NULL"] || [classObj.balance length]==0){
                [avail_bal addObject:@"N/A"];
            }else{
                [avail_bal addObject:classObj.balance];
            }
            //  number = [NSNumber numberWithInt:dictionaryindex];
            // int value = [number intValue];
            //number = [NSNumber numberWithInt:value + 1];
            //           dictionaryindex = [number intValue];
            //            number = [NSNumber numberWithInt:dictionaryindex];
            //            number = [NSNumber numberWithInt:value + 1];
            
            //  dictionaryindex = [number intValue];
            
        }
        else{
        }
    }
    ddMenu.hidden=YES;
    
    //    table1.delegate=self;
    //    table2.delegate=self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)ddMenuShow:(UIButton *)sender
{
    if ([flag isEqualToString:@"1"]) {
        flag=@"0";
        // sender.tag = 1;
        self.ddMenu.hidden = NO;
        [sender setTitle:@"Account ▲" forState:UIControlStateNormal];
    } else {
        sender.tag = 0;
        flag=@"1";
        exesender =sender;
        self.ddMenu.hidden = YES;
        [sender setTitle:@"Account ▼" forState:UIControlStateNormal];
    }
}

- (IBAction)ddMenuSelectionMade:(UIButton *)sender
{
    self.ddText.text = sender.titleLabel.text;
    [self.ddMenuShowButton setTitle:@"Account ▼" forState:UIControlStateNormal];
    self.ddMenuShowButton.tag = 0;
    self.ddMenu.hidden = YES;
    switch (sender.tag) {
        case 1:
            self.view.backgroundColor = [UIColor redColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor blueColor];
            break;
        case 3:
            self.view.backgroundColor = [UIColor greenColor];
            break;
            
        default:
            break;
    }
}



//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if (tableView==table1)
//    {
//        return 1;
//    }
//    else
//    {
//        return 2;
//    }
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    self.ddMenu.hidden=true;
    [exesender setTitle:@"Account ▼" forState:UIControlStateNormal];
    flag=@"1";
    // cell.textLabel.text=[account_name objectAtIndex:indexPath.row];
    self.act_name.text =[account_name objectAtIndex:indexPath.row];
    self.act_num.text=[account_number objectAtIndex:indexPath.row];
    self.act_type.text=[type objectAtIndex:indexPath.row];
    self.act_balcdate.text=[balc_date objectAtIndex:indexPath.row];
    self.act_portfoliobal.text=[avail_bal objectAtIndex:indexPath.row];
    self.upper_amount.text=[avail_bal objectAtIndex:indexPath.row];
    self.upper_currency.text=[account_currencydesc objectAtIndex:indexPath.row];
    
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (tableView==table1)
    //    {
    //  NSLog(@"%lu",(unsigned long)[account_name count]);
    
    return [account_name count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (tableView==table)
    {
        
        cell.textLabel.text=[account_name objectAtIndex:indexPath.row];
        self.act_name.text =[account_name objectAtIndex:0];
        self.act_num.text=[account_number objectAtIndex:0];
        self.act_type.text=[type objectAtIndex:0];
        self.act_balcdate.text=[balc_date objectAtIndex:0];
        self.act_portfoliobal.text=[avail_bal objectAtIndex:0];
        self.upper_amount.text=[avail_bal objectAtIndex:0];
        self.upper_currency.text=[account_currencydesc objectAtIndex:0];
        
        
    }
    else
    {
        //cell for second table
    }
    return cell;
}

@end
