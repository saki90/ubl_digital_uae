//
//  UBLBP_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 25/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface UBLBP_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_combo_frm;
    IBOutlet UITextField* txt_combo_bill_names;
    IBOutlet UILabel* lbl_bill_mangmnt;
    IBOutlet UILabel* lbl_due_dat;
    IBOutlet UILabel* lbl_statement_due_dat;
    IBOutlet UILabel* lbl_amnts;
    IBOutlet UITextField* txt_comment;
    IBOutlet UILabel* lbl_t_pin;
    IBOutlet UITextField* txt_t_pin;
    IBOutlet UIButton* btn_submit;
    IBOutlet UILabel* lbl_status;
    IBOutlet UILabel* lbl_bill_type;
    IBOutlet UILabel* lbl_loan_acct;
    IBOutlet UITextField* txt_amounts;
    IBOutlet UILabel* lbl_t_pin_drive;
    IBOutlet UITextField* txt_t_pin_drive;
    IBOutlet UILabel* lbl_mini_B4_due_dat;
    IBOutlet UILabel* lbl_mini_after_due_dat;
    IBOutlet UITextField* txt_comment_credit;
    IBOutlet UITextField* txt_combo_payment;
    IBOutlet UITextField* lbl_credit_amount;
    IBOutlet UITextField* txt_credit_amount;
    IBOutlet UILabel* lbl_t_pin_credit;
    IBOutlet UITextField* txt_t_pin_credit;
    IBOutlet UITextField* txt_Utility_bill_name;
    IBOutlet UILabel* lbl_credit_card_heading;
    IBOutlet UILabel* lbl_credit_card_no;
    IBOutlet UILabel* lbl_bill_type_heading;
    IBOutlet UIButton* btn_review;
    IBOutlet UIButton* btn_cashline_combo;
    
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_payment;
    IBOutlet UITableView* table_bill;
    
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    IBOutlet UIView* vw_payment;
    IBOutlet UIView* vw_credit_card;
    IBOutlet UIView* vw_bill;
    IBOutlet UIView* vw_table;
    IBOutlet UIView* vw_acct;
    
    IBOutlet UIButton* btn_edit;
    IBOutlet UIButton* btn_del;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong) IBOutlet UILabel* lbl_balance;
@property(nonatomic,strong) IBOutlet UILabel* lbl_acct_frm;


-(IBAction)btn_back:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_bill_names:(id)sender;
-(IBAction)btn_credit_card:(id)sender;


@end

