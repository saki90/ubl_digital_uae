//
//  QR_Code_Detail.h
//  ubltestbanking
//
//  Created by Mehmood on 28/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"

@interface QR_Code_Detail : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_amnt;
    IBOutlet UITextField* txt_pin;
    IBOutlet UITextField* txt_cell;
    IBOutlet UILabel* lbl_name;
    IBOutlet UITextField* txt_comment;
    IBOutlet UITableView* table_from;
    IBOutlet UIView* vw_table;
    IBOutlet UILabel* _lbl_acct_frm;
    IBOutlet UITextField* txt_acct_from;
    IBOutlet UILabel* lbl_balance;
    IBOutlet UITextField* txt_qr_pin;
    IBOutlet UITextField* txt_name;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


@end

