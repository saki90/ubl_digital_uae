//
//  Faq_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"



@interface Faq_VC : UIViewController
{
    
    IBOutlet UITableView* table;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}
//@property (weak, nonatomic) IBOutlet UIWebView *webView;

-(IBAction)btn_back:(id)sender;

@end

