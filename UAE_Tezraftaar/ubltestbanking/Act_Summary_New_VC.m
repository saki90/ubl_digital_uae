//
//  Act_Summary_New_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 07/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Act_Summary_New_VC.h"
#import "Table_de.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Act_Summary_New_VC ()
{
    UILabel* label;
    NSArray* split_bill;
    NSMutableArray* table_header;
    double bill_price;
    UIImageView* img_name;
    GlobalStaticClass *gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSDictionary* dic;
    NSMutableArray* a,*b;
    NSMutableArray* summary_load;
    NSString* str_act_name;
    NSArray * header_main;
    NSMutableArray* arr_omni_acct,*arr_deposit_acct,*arr_credit_card_acct,*arr_ubl_wiz_act,*arr_ubl_fund,*arr_other_acct;
    UIImageView* img;
    UIButton* paybtn;
    NSString* cell_index;
    
    NSNumberFormatter *format;
    NSString *temp;
    float number2;
    MBProgressHUD *hud;
    UIAlertController* alert;
    NSString* touch_id;
    NSString* Pw_status_chck;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Act_Summary_New_VC
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass =  [GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    
    //NSLog(@"%@", gblclass.arr_act_statmnt_name);
    
    self.transitionController = [[TransitionDelegate alloc] init];
    
    gblclass.arr_graph_name=[[NSMutableArray alloc] init];
    arr_omni_acct=[[NSMutableArray alloc] init];
    arr_deposit_acct=[[NSMutableArray alloc] init];
    arr_credit_card_acct=[[NSMutableArray alloc] init];
    arr_ubl_wiz_act=[[NSMutableArray alloc] init];
    arr_ubl_fund=[[NSMutableArray alloc] init];
    arr_other_acct=[[NSMutableArray alloc] init];
    table_header=[[NSMutableArray alloc] init];
    summary_load=[[NSMutableArray alloc] init];
    a=[[NSMutableArray alloc] init];
    b=[[NSMutableArray alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    ssl_count = @"0";
    
    touch_id = [[NSUserDefaults standardUserDefaults] stringForKey:@"Touch_ID"];
    
    
    format = [[NSNumberFormatter alloc]init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    [format setRoundingMode:NSNumberFormatterRoundHalfUp];
    [format setMaximumFractionDigits:2];
    
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
    }
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [table1 reloadData];
}



-(void)parse_header
{
    
    BOOL result = false;
    for (int i=0; i<[gblclass.arr_act_statmnt_name count]; i++)
    {
        
        split_bill = [[gblclass.arr_act_statmnt_name objectAtIndex:i] componentsSeparatedByString: @"|"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF IN %@",[split_bill objectAtIndex:4]];
        
        if ([table_header count]==0)
        {
            [table_header addObject:[split_bill objectAtIndex:4]];
        }
        
        
        for (int j=0; j<[table_header count]; j++)
        {
            result = [predicate evaluateWithObject:[table_header objectAtIndex:j]];
            
            if (!result)
            {
                break;
            }
        }
        
        
        if (!result)
        {
            [table_header addObject:[split_bill objectAtIndex:4]];
        }
    }
    
    //[table_header setArray:[[NSSet setWithArray:header_main] allObjects]];
    
    //    NSOrderedSet *orderedSet1 = [NSOrderedSet orderedSetWithArray:table_header];
    //    NSArray *arrayWithoutDuplicates = [orderedSet1 array];
    
    //NSLog(@"%@",table_header);
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:table_header];
    header_main = [orderedSet array];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath
{
    
    @try {
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Omni Account"])
        {
            split_bill = [[arr_omni_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //   label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"PKR %@",temp];
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = [NSString stringWithFormat:@"IBAN %@",[split_bill objectAtIndex:9]];
            [cell.contentView addSubview:label];
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            
            [paybtn setTitle:@"" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            paybtn.hidden=YES;
            [cell.contentView addSubview:paybtn];
            
            
        }
        else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Deposit Account"])
            //else if (indexPath.section==1)
        {
            split_bill = [[arr_deposit_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //   label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:8],temp];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = [NSString stringWithFormat:@"IBAN %@",[split_bill objectAtIndex:9]];
            [cell.contentView addSubview:label];
            
            
            //for Image ::
            
            UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
            
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            [paybtn setTitle:@"" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            paybtn.hidden=YES;
            [cell.contentView addSubview:paybtn];
            
            
            
        }
        else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Other Account"])
            //else if (indexPath.section==1)
        {
            split_bill = [[arr_other_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //   label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:8],temp];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = [NSString stringWithFormat:@"IBAN %@",[split_bill objectAtIndex:9]];
            [cell.contentView addSubview:label];
            
            
            //for Image ::
            
            UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
            
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            [paybtn setTitle:@"" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            paybtn.hidden=YES;
            [cell.contentView addSubview:paybtn];
            
        }
        else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL UAE CC"])
        {
            split_bill = [[arr_credit_card_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //    label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:8],temp];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = @"";
            [cell.contentView addSubview:label];
            
            //for Image ::
            
            UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            [paybtn setTitle:@"Pay" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [paybtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            paybtn.hidden=NO;
            [cell.contentView addSubview:paybtn];
            
            
        }
        else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL WIZ Card"])
        {
            split_bill = [[arr_ubl_wiz_act objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //    label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:8],temp];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = @"";
            [cell.contentView addSubview:label];
            
            //for Image ::
            
            UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
            
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            
            [paybtn setTitle:@"Load" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            [paybtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            paybtn.hidden=NO;
            [cell.contentView addSubview:paybtn];
            
        }
        else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL FUNDS"])
            //else if (indexPath.section==1)
        {
            split_bill = [[arr_ubl_fund objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            
            label=(UILabel*)[cell viewWithTag:1];
            label.text = [split_bill objectAtIndex:1]; //3
            //      label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split_bill objectAtIndex:2]; //3
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split_bill objectAtIndex:3]; //3
            [cell.contentView addSubview:label];
            
            
            label=(UILabel*)[cell viewWithTag:4];
            number2=[[split_bill objectAtIndex:4] floatValue];
            temp = [format stringFromNumber:[NSNumber numberWithFloat:number2]];
            label.text=[NSString stringWithFormat:@"%@ %@",[split_bill objectAtIndex:8],temp];
            [cell.contentView addSubview:label];
            
            label=(UILabel*)[cell viewWithTag:5];
            label.text = @"";
            [cell.contentView addSubview:label];
            
            //for Image ::
            
            UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 40, 72)];
            
            
            if ([[split_bill objectAtIndex:6] isEqualToString:@"1"])
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Check_acct.png"];
                [cell.contentView addSubview:img];
            }
            else
            {
                img=(UIImageView*)[cell viewWithTag:11];
                img.image=[UIImage imageNamed:@"Non.png"];
                [cell.contentView addSubview:img];
            }
            
            paybtn=(UIButton*)[cell viewWithTag:10];
            [paybtn setTitle:@"" forState:UIControlStateNormal];
            cell_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
            paybtn.hidden=YES;
            [cell.contentView addSubview:paybtn];
            
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        
        return cell;
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
}



- (void)yourButtonClicked:(UIButton *)sender
{
    
    UITableViewCell *cell = (UITableViewCell *)sender.superview;
    NSIndexPath *indexPath = [table1 indexPathForCell:cell];
    
    NSIndexPath *indexPath2 = [NSIndexPath indexPathWithIndex:[sender tag]];
    //NSLog(@"%ld",(long)indexPath2);
    
    int rowNum = [(UIButton*)sender tag];
    
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *clickedButtonIndexPath = [table1 indexPathForCell:clickedCell];
    
    
    CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:table1];
    NSIndexPath *indexPath1 = [table1 indexPathForRowAtPoint:buttonPosition1];
    //NSLog(@"%lu",indexPath1.row);
    
    int section = clickedButtonIndexPath.section;
    int section2 = clickedButtonIndexPath.row;
    
    NSIndexPath *selectedIndexPath = [table1 indexPathForSelectedRow];
    
    
    NSIndexPath *indexPaths = [NSIndexPath indexPathForRow:[sender tag] inSection:
                               [[sender superview] tag]];
    
    
    //NSLog(@"%@",[header_main objectAtIndex:indexPath1.section]);
    
    // if (section==2)    //credit card
    
    if ([[header_main objectAtIndex:indexPath1.section] isEqualToString:@"UBL UAE CC"] || [[header_main objectAtIndex:indexPath1.section] isEqualToString:@"UBL UAE CC"])
    {
        split_bill = [[arr_credit_card_acct objectAtIndex:section2] componentsSeparatedByString: @"|"];
        
        gblclass.direct_pay_frm_Acctsummary_nick=[split_bill objectAtIndex:1];
        gblclass.direct_pay_frm_Acctsummary_customer_id=[split_bill objectAtIndex:2];
        gblclass.direct_pay_frm_Acctsummary_registed_customer_id=[split_bill objectAtIndex:0];
        
        [gblclass.arr_bill_ublbp removeAllObjects];
        gblclass.direct_pay_frm_Acctsummary=@"1";
        
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"UBP_bill"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    // if (section==3)     //ubl wiz card
    if ([[header_main objectAtIndex:indexPath1.section] isEqualToString:@"ubl wiz card"] || [[header_main objectAtIndex:indexPath1.section] isEqualToString:@"UBL WIZ Card"])
    {
        
        split_bill = [[arr_ubl_wiz_act objectAtIndex:section2] componentsSeparatedByString: @"|"];
        
        gblclass.direct_pay_frm_Acctsummary_nick=[split_bill objectAtIndex:1];
        gblclass.direct_pay_frm_Acctsummary_customer_id=[split_bill objectAtIndex:2];
        gblclass.direct_pay_frm_Acctsummary_registed_customer_id=[split_bill objectAtIndex:0];
        
        
        gblclass.direct_pay_frm_Acctsummary=@"1";
        
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"wiz_bill"];
        
        [self presentViewController:vc animated:NO completion:nil];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    gblclass.indexxx=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    [gblclass.arr_graph_name removeAllObjects];
    
    
    gblclass.frm_acct_default_chck=@"0";
    gblclass.wiz_card_chck = @"0";
    
    if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Omni Account"])
    {
        split_bill = [[arr_omni_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.acct_summary_section=@"Omni Account";
        gblclass.chk_acct_statement=@"0";
        [gblclass.arr_graph_name addObject:@"1"];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        
        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Deposit Account"])
    {
        split_bill = [[arr_deposit_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.acct_summary_section=@"Deposit Account";
        gblclass.chk_acct_statement=@"0";
        [gblclass.arr_graph_name addObject:@"1"];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        
       // NSLog(@"%@",gblclass.arr_graph_name);
        
        
        
        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"Other Account"])
    {
        split_bill = [[arr_other_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.acct_summary_section=@"Other Account";
        gblclass.chk_acct_statement=@"0";
        [gblclass.arr_graph_name addObject:@"1"];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        
        // NSLog(@"%@",gblclass.arr_graph_name);
        
        
        
        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL UAE CC"])
    {
        split_bill = [[arr_credit_card_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        gblclass.acct_summary_section=@"Credit Card Account";
        gblclass.chk_acct_statement=@"0";
        [gblclass.arr_graph_name addObject:@"1"];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:7]];
        
        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        
    }
    else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL WIZ Card"])
    {
        split_bill = [[arr_ubl_wiz_act objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.wiz_card_chck = @"1";
        gblclass.acct_summary_section=@"UBL WIZ Card";
        gblclass.chk_acct_statement=@"0";
        [gblclass.arr_graph_name addObject:@"1"];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:10]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:11]];
        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:12]];
        
        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    else if ([[header_main objectAtIndex:indexPath.section] isEqualToString:@"UBL FUNDS"])
    {
        
        //        split_bill = [[arr_ubl_fund objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        //
        //        gblclass.acct_summary_section=@"UBL FUNDS";
        //        gblclass.chk_acct_statement=@"0";
        //        [gblclass.arr_graph_name addObject:@"1"];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:1]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:0]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:4]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:2]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:6]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:5]];
        //        [gblclass.arr_graph_name addObject:[split_bill objectAtIndex:8]];
        //
        //        gblclass.summary_acct_name=[split_bill objectAtIndex:1];
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [header_main indexOfObject:title];  //[animalIndexTitles indexOfObject:title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:  (NSInteger)section
{
    //for (int i=0; i<[header_main count]; i++)
    //{
    
    //NSLog(@"*************************  JAHANGIR");
    
    if ([[header_main objectAtIndex:section] isEqualToString:@"Omni Account"])
    {
        return [arr_omni_acct count];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"Deposit Account"])
    {
        return [arr_deposit_acct count];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"UBL UAE CC"])
    {
        return [arr_credit_card_acct count];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"UBL WIZ Card"])
    {
        return [arr_ubl_wiz_act count];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"UBL FUNDS"])
    {
        return [arr_ubl_fund count];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"Other Account"])
    {
        return [arr_other_acct count];
    }
    else
    {
        return 0;
    }
    //}
    
    
    return 0;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [header_main count];//numberOfSections;         //header_main
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    
    if ([[header_main objectAtIndex:section] isEqualToString:@"Omni Account"])
    {
        
        return [header_main objectAtIndex:section];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"Deposit Account"])
    {
        return [header_main objectAtIndex:section];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"Credit Card Account"])
    {
        return [header_main objectAtIndex:section];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"UBL WIZ Card"])
    {
        return [header_main objectAtIndex:section];
    }
    else if ([[header_main objectAtIndex:section] isEqualToString:@"UBL FUNDS"])
    {
        return [header_main objectAtIndex:section];
    }
    else
    {
        return 0;
    }
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(320.0, 0.0, 320.0, 20.0)];
    //  myView.backgroundColor=[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0];
    
    myView.backgroundColor= [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    [label1 setFrame:CGRectMake(15.0, 5.0, 180.0, 30.0)];
    label1.text= [header_main objectAtIndex:section];
    label1.textColor=[UIColor blackColor];
    label1.font=[UIFont systemFontOfSize:12];
    label1.tag = section;
    label1.hidden = NO;
    [label1 setBackgroundColor:[UIColor clearColor]];
    [myView addSubview:label1];
    
    return myView;
    
}


- (void)Add_bill_click:(id)sender
{
    
    UIButton *allButton = (UIButton*)sender;
    allButton.selected = !allButton.selected;
    
    gblclass.bill_click=[table_header objectAtIndex:[sender tag]];
    gblclass.bill_type=[NSString stringWithFormat:@"Add %@",[table_header objectAtIndex:[sender tag]]];
    
    
    if ([gblclass.bill_click isEqualToString:@"UBL Bills"])
    {
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"billmanagegment"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
    }
}


-(IBAction)btn_back:(id)sender
{
    gblclass.landing = @"1";
 
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
//    CATransition *transition = [ CATransition animation ];
//    transition.duration = 0.3;
//    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromLeft;
//    [ self.view.window. layer addAnimation:transition forKey:nil];
//
//    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)Parse_summary
{
    
    @try {
        
        [gblclass.arr_transfer_within_acct_to removeAllObjects];
        [gblclass.arr_transfer_within_acct removeAllObjects];
        
        dic=(NSDictionary *)gblclass.arr_act_statmnt_name;
        
        //NSLog(@"%@",gblclass.actsummaryarr);
        
        for (dic in gblclass.actsummaryarr)
        {
            
            NSString* registered_account_id=[dic objectForKey:@"registered_account_id"];
            if (registered_account_id.length==0 || [registered_account_id isEqualToString:@" "] || [registered_account_id isEqualToString:nil])
            {
                registered_account_id=@"-";
                [a addObject:registered_account_id];
            }
            else
            {
                [a addObject:registered_account_id];
            }
            
            NSString* account_name=[dic objectForKey:@"account_name"];
            if (account_name.length==0 || [account_name isEqualToString:@" "])
            {
                account_name=@"-";
                [a addObject:account_name];
            }
            else
            {
                [a addObject:account_name];
            }
            
            NSString* account_no=[dic objectForKey:@"account_no"];
            if (account_no.length==0 || [account_no isEqualToString:@" "] || [account_no isEqualToString:nil])
            {
                account_no=@"-";
                [a addObject:account_no];
            }
            else
            {
                [a addObject:account_no];
            }
            
            NSString* branch_name=[dic objectForKey:@"branch_name"];
            if (branch_name==(NSString *)[NSNull null])
            {
                branch_name=@"-";
                [a addObject:branch_name];
            }
            else
            {
                [a addObject:branch_name];
            }
            
            NSString* available_balance=[dic objectForKey:@"m3_balance"];
            if (available_balance.length==0 || [available_balance isEqualToString:@" "] || [available_balance isEqualToString:@"0"] || [available_balance isEqualToString:nil])
            {
                available_balance=@"0";
                //17 nov 2017  gblclass.is_default_m3_balc=available_balance;
                // [a addObject:available_balance];
            }
            else
            {
                [a addObject:available_balance];
                //17 nov 2017  gblclass.is_default_m3_balc=available_balance;
            }
            
            
            if ([available_balance isEqualToString:@"0"])
            {
                NSString* cc_balance=[dic objectForKey:@"cc_balance"];
                if (cc_balance==(NSString *)[NSNull null])
                {
                    cc_balance=@"0";
                    [a addObject:cc_balance];
                }
                else
                {
                    [a addObject:cc_balance];
                    
                }
            }
            
            
            NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
            if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
            {
                account_type_desc=@"-";
                [a addObject:account_type_desc];
            }
            else
            {
                [a addObject:account_type_desc];
            }
            
            NSString* is_default=[dic objectForKey:@"is_default"];
            if (is_default.length==0 || [is_default isEqualToString:@" "] || [is_default isEqualToString:nil])
            {
                is_default=@"-";
                [a addObject:is_default];
            }
            else
            {
                [a addObject:is_default];
            }
            
            NSString* credit_limit=[dic objectForKey:@"credit_limit"];
            
            if (credit_limit ==(NSString *)[NSNull null]) {
                
                
                //        if (credit_limit.length==0 || [credit_limit isEqualToString:@" "] || credit_limit==[NSNull null] || [credit_limit isEqualToString:nil])
                //       {
                credit_limit=@"-";
                [a addObject:credit_limit];
            }
            else
            {
                [a addObject:credit_limit];
            }
            
            
            NSString* currency_Descr=[dic objectForKey:@"ccy"];
            
            if (currency_Descr ==(NSString *)[NSNull null])
            {
                
                
                //            NSString* ccy_currency=[dic objectForKey:@"ccy"];
                //
                //            if (ccy_currency ==(NSString *)[NSNull null])
                //            {
                //                ccy_currency=@"N/A";
                //                [a addObject:ccy_currency];
                //            }
                //            else
                //            {
                //                [a addObject:ccy_currency];
                //            }
                
                //            NSLog(@"%@",gblclass.base_currency);
                
                //            currency_Descr=@"N/A";
                [a addObject:gblclass.base_currency];
            }
            else
            {
                [a addObject:currency_Descr];
            }
            
            NSString* iban_with_space=[dic objectForKey:@"ibanwithspace"];
            
            if (iban_with_space ==(NSString *)[NSNull null])
            {
                iban_with_space=@"";
                [a addObject:iban_with_space];
            }
            else
            {
                [a addObject:iban_with_space];
            }
            
            NSString* Cust_No=[dic objectForKey:@"Cust_No"];
            
            if (Cust_No ==(NSString *)[NSNull null])
            {
                Cust_No=@"";
                [a addObject:Cust_No];
            }
            else
            {
                [a addObject:Cust_No];
            }
            
            NSString* br_code1=[dic objectForKey:@"br_code"];
            
            if (br_code1 ==(NSString *)[NSNull null])
            {
                br_code1=@"";
                [a addObject:br_code1];
            }
            else
            {
                [a addObject:br_code1];
            }
            
            NSString* account_type1=[dic objectForKey:@"account_type"];
            
            if (account_type1 ==(NSString *)[NSNull null])
            {
                account_type1=@"";
                [a addObject:account_type1];
            }
            else
            {
                [a addObject:account_type1];
            }
            
            NSString* bbb = [a componentsJoinedByString:@"|"];
            //NSLog(@"%@", bbb);
            
            if ([account_type_desc isEqualToString:@"Omni Account"])
            {
                [arr_omni_acct addObject:bbb];
            }
            else if ([account_type_desc isEqualToString:@"Deposit Account"])
            {
                [arr_deposit_acct addObject:bbb];
            }
            else if ([account_type_desc isEqualToString:@"UBL UAE CC"])
            {
                [arr_credit_card_acct addObject:bbb];
            }
            else if ([account_type_desc isEqualToString:@"UBL WIZ Card"])
            {
                [arr_ubl_wiz_act addObject:bbb];
            }
            else if ([account_type_desc isEqualToString:@"UBL FUNDS"])
            {
                [arr_ubl_fund addObject:bbb];
            }
            else if ([account_type_desc isEqualToString:@"Other Account"])
            {
                [arr_other_acct addObject:bbb];
            }
            
            
            //**************************** For From Account Update ****************************
            
            [b addObject:account_name];
            [b addObject:account_no];
            
            
            
            
            [b addObject:registered_account_id];
            
            
            NSString* is_default1=[dic objectForKey:@"is_default"];
            NSString* is_default_name=[dic objectForKey:@"account_name"];
            NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
            NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
            NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
            
            if ([is_default isEqualToString:@"1"])
            {
                
                gblclass.is_default_acct_id=registered_account_id;
                gblclass.is_default_acct_id_name=is_default_name;
                gblclass.is_default_acct_no=is_default_acct_no;
                gblclass.is_default_m3_balc=is_default_balc;
                gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                
            }
            
            //privileges
            NSString* privileges=[dic objectForKey:@"privileges"];
            NSString* pri=[dic objectForKey:@"privileges"];
            privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //Second bit 1 for withDraw ..
            
            
            if ([privileges isEqualToString:@"0"])
            {
                privileges=@"0";
                [b addObject:privileges];
            }
            else
            {
                [b addObject:privileges];
            }
            
            
            [b addObject:account_type_desc];
            
            
            
            NSString* account_type=[dic objectForKey:@"account_type"];
            if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
            {
                account_type=@"N/A";
                [b addObject:account_type];
            }
            else
            {
                [b addObject:account_type];
            }
            
            
            //m3_balance
            [b addObject:available_balance];
            
            
            [b addObject:branch_name];
            
            
            NSString* available_balance1=[dic objectForKey:@"available_balance"];
            if ( available_balance1 == (NSString *)[NSNull null])
            {
                available_balance1=@"N/A";
                [b addObject:available_balance1];
            }
            else
            {
                [b addObject:available_balance1];
            }
            
            NSString* br_code=[dic objectForKey:@"br_code"];
            if ( br_code == (NSString *)[NSNull null])
            {
                br_code=@"0";
                [b addObject:br_code];
            }
            else
            {
                [b addObject:br_code];
            }
            
            
            NSString *bbb1 = [b componentsJoinedByString:@"|"];
            
            
            //        if ([privileges isEqualToString:@"1"])
            //        {
            //            [gblclass.arr_transfer_within_acct addObject:bbb1];
            //        }
            //
            
            if ([privileges isEqualToString:@"1"])
            {
                if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                {
                    
                    [gblclass.arr_transfer_within_acct addObject:bbb1]; ////Arry transfer within my acct
                    
                }
            }
            
            
            NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
            privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
            
            
            if ([privileges_to isEqualToString:@"1"])
            {
                [gblclass.arr_transfer_within_acct_to addObject:bbb1];
            }
            
            
            
            
            //**************************** For From Account Update End ****************************
            
            
            //[summary_load addObject:bbb];
            
            //NSLog(@"%lu",(unsigned long)[a count]);
            //NSLog(@"%lu",(unsigned long)[b count]);
            
            [a removeAllObjects];
            [b removeAllObjects];
            
        }
        
        //NSLog(@"%@",gblclass.arr_transfer_within_acct);
        
        
        [table1 reloadData];
   
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        //NSLog(@"%@",gblclass.arr_transfer_within_acct);
        //NSLog(@"%@",exception);
        
        gblclass.custom_alert_msg=@"Try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay // bill_all
    
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(void) AccountSummary:(NSString *)strIndustry{
    
    @try {
        
        
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //  // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AccountSummaryV2" parameters:dictparam progress:nil
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  gblclass.actsummaryarr = [[NSMutableArray alloc] init];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      gblclass.actsummaryarr =[[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbRefreshAcct"];

                      [self parse_header];
                      [self Parse_summary];
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:@"Try again later." :@"0"];
                  }

                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  gblclass.custom_alert_msg=@"Retry";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                       
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        // [self mob_App_Logout:@""];
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //      // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString  :@"0"];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self AccountSummary:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
  
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self AccountSummary:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    } 
    
}


///////////************************** SSL PINNING END ******************************////////////////

@end

