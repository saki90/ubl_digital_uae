//
//  LocationData.h
//  ubltestbanking
//
//  Created by Mehmood on 11/11/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationData : NSObject

@property (nonatomic,strong)NSString* longitude;
@property (nonatomic,strong)NSString* latitude;
@property (nonatomic ,strong) NSArray* DetailInfo;

@end
