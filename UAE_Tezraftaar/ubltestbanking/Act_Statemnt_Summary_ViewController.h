//
//  Act_Statemnt_Summary_ViewController.h
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"

@interface Act_Statemnt_Summary_ViewController : UIViewController
{
    IBOutlet UILabel* lbl_transaction_date;
    IBOutlet UILabel* lbl_desc;
    IBOutlet UILabel* lbl_debit;
    IBOutlet UILabel* lbl_balc;
    IBOutlet UILabel* lbl_debit_name;
    IBOutlet UILabel* lbl_default_acct_name;
    IBOutlet UILabel* lbl_default_acct_id;
    IBOutlet UIImageView* img_cr;
    
    IBOutlet UILabel* lbl_acct_txt;
    IBOutlet UILabel* lbl_credit_amt_txt;
    IBOutlet UILabel* lbl_balc_amt_txt;
    IBOutlet UILabel* lbl_balc_amt_name;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,retain)NSString* indexx;

@property(nonatomic,strong) NSString* arr_;

-(IBAction)btn_screenshot:(id)sender;
-(IBAction)btn_back:(id)sender;

@end
