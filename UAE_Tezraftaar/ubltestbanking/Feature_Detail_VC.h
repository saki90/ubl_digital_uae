//
//  Feature_Detail_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 29/12/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Feature_Detail_VC : UIViewController
{
    
    
    
    
}

@property (strong, nonatomic) IBOutlet UILabel* lbl_heading;
@property (strong, nonatomic) IBOutlet UITextView* txt_desc;
@property (strong, nonatomic) IBOutlet UIScrollView* vw_scrl;
@property (strong, nonatomic) IBOutlet UIImageView* pro_img;

//@property (weak, nonatomic) IBOutlet UIWebView *webView;

-(IBAction)btn_back:(id)sender;


@end

