



//
//  Receive_QR_login.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 14/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Receive_QR_login.h"
#import "Globals.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "UIImage+QRCodeGenerator.h"


@interface Receive_QR_login ()
{
    UIStoryboard *storyboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    NSArray* split;
}
@end

@implementation Receive_QR_login

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @try {
        
        
        gblclass=[GlobalStaticClass getInstance];
        
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        CGFloat heights = 0.0;
        
        
        if (height == 568)
        {
            heights=118;
        }
        
        
        NSLog(@"%@",gblclass.arr_instant_qr);
        
        
        
        split = [[gblclass.arr_instant_qr objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        NSString* qr_txt;
        
        //  qr_txt=@"#m303332392279m3#|1056880|Bashirsyed [03332392279]";//[NSString stringWithFormat:@"#m3%@m3#|%@|%@",gblclass.is_default_acct_no,gblclass.is_default_acct_id,gblclass.is_default_acct_id_name];
        
        
        //  qr_txt=[NSString stringWithFormat:@"#m3p2p#|%@|#m3p2p#",[split objectAtIndex:2]];
        
        
        qr_txt=[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
        
        
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10 ,heights ,300,300)];
        
        
        imgView.image = [UIImage QRCodeGenerator:qr_txt
                                  andLightColour:[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0]
                                   andDarkColour:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5]
                                    andQuietZone:1
                                         andSize:300];
        
        [self.view addSubview:imgView];
        [self.view addSubview:img_logo];
        
        self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
        
    }
    @catch (NSException *exception)
    {
        [self custom_alert:@"Try again later" :@"0"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    //    [self dismissModalStack];
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_send_money:(id)sender
{
    
    gblclass.chck_qr_send=@"0";
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr_login"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark -UIActivityController Sharing -

- (IBAction)shareButton:(UIButton *)sender
{
    UIImage *barCodeImage = self.imgView.image;
    
    NSArray *objectsToShare = @[barCodeImage];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


@end

