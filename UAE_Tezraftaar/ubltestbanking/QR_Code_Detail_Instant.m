//
//  QR_Code_Detail_Instant.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "QR_Code_Detail_Instant.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface QR_Code_Detail_Instant ()
{
    GlobalStaticClass* gblclass;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    UIAlertController  *alert;
    UILabel* label;
    NSArray* split;
    UIImageView* img;
    NSString* chk_ssl;
    NSString* responsecode;
    NSString* str_acct_id_frm;
    Encrypt* encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation QR_Code_Detail_Instant

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    //    [gblclass.arr_qr_code addObject:str_Mobile];
    //    [gblclass.arr_qr_code addObject:str_Accout_no];
    //    [gblclass.arr_qr_code addObject:str_merchant_name];
    //    [gblclass.arr_qr_code addObject:str_category_code];
    //    [gblclass.arr_qr_code addObject:str_city];
    //    [gblclass.arr_qr_code addObject:str_country_code];
    //    [gblclass.arr_qr_code addObject:str_curr_code];
    
    
    
    ssl_count = @"0";
    //    lbl_name.text=[gblclass.arr_qr_code objectAtIndex:2];
    txt_name.text = [gblclass.arr_qr_code objectAtIndex:2];
    txt_name.enabled=NO;
    txt_cell.text = [gblclass.arr_qr_code objectAtIndex:2];;
    
    //      txt_cell.text=[NSString stringWithFormat:@"Merchant Cell # %@",[gblclass.arr_qr_code objectAtIndex:0]];
    
    //    txt_acct_from.text=gblclass.is_default_acct_id_name;
    //    _lbl_acct_frm.text=gblclass.is_default_acct_no;
    //    str_acct_id_frm=gblclass.is_default_acct_id;
    
    // split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    //    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    //    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    //    attach1.bounds = CGRectMake(10, 8, 20, 10);
    //    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    //
    //    NSString* balc;
    //
    //    if ([split objectAtIndex:6]==0)
    //    {
    //        balc=@"0";
    //    }
    //    else
    //    {
    //        balc=[split objectAtIndex:6];
    //    }
    
    
    //    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
    //    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    //    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    //
    //    [mutableAttriStr1 appendAttributedString:imageStr1];
    //    lbl_balance.attributedText = mutableAttriStr1;
    
    
    txt_pin.delegate=self;
    txt_amnt.delegate=self;
    txt_qr_pin.delegate=self;
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//    };
    
    vw_table.hidden=YES;
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_cancel:(id)sender
{
    gblclass.camera_back = @"1";
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(IBAction)btn_review:(id)sender
{
    @try {
        
        NSLog(@"%@",txt_qr_pin.text);
        
        if ([txt_amnt.text isEqualToString:@""])
        {
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter amount" :@"0"];
            
            return;
        }
        else  if ([txt_amnt.text isEqualToString:@"0"])
        {
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter valid amount" :@"0"];
            
            
            return;
        }
        else  if ([txt_qr_pin.text isEqualToString:@""])
        {
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter 6 digits QR PIN" :@"0"];
            
            
            return;
        }
        else  if ([txt_qr_pin.text length]<6)
        {
            
            [hud hideAnimated:YES];
            
            [self custom_alert:@"Please enter 6 digits QR PIN" :@"0"];
            
            
            return;
        }
        else  if ([txt_comment.text isEqualToString:@""])
        {
            txt_comment.text=@"";
            
        }
        
        
        
        //        if ([txt_pin.text isEqualToString:@""])
        //        {
        //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter PIN" preferredStyle:UIAlertControllerStyleAlert];
        //
        //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //            [alert addAction:ok];
        //
        //            [self presentViewController:alert animated:YES completion:nil];
        //
        //            return;
        //        }
        
        
        
        //        NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code  count]);
        if ([gblclass.arr_qr_code  count]>9)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:9 withObject:txt_amnt.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_amnt.text];
        }
        
        if ([gblclass.arr_qr_code count]>10) //Comments
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:10 withObject:@""];
        }
        else
        {
            [gblclass.arr_qr_code addObject:@""];
        }
        
        
        
        
        
        
        //        if ([gblclass.arr_qr_code count]>9)
        //        {
        //            [gblclass.arr_qr_code replaceObjectAtIndex:9 withObject:txt_acct_from.text];
        //        }
        //        else
        //        {
        //            [gblclass.arr_qr_code addObject:txt_acct_from.text];
        //        }
        
        //        if ([gblclass.arr_qr_code count]>10)
        //        {
        //            [gblclass.arr_qr_code replaceObjectAtIndex:10 withObject:_lbl_acct_frm.text];
        //        }
        //        else
        //        {
        //            [gblclass.arr_qr_code addObject:_lbl_acct_frm.text];
        //        }
        
        //        if ([gblclass.arr_qr_code count]>11)
        //        {
        //            [gblclass.arr_qr_code replaceObjectAtIndex:11 withObject:str_acct_id_frm];
        //        }
        //        else
        //        {
        //            [gblclass.arr_qr_code addObject:str_acct_id_frm];
        //        }
        //
        if ([gblclass.arr_qr_code count]>11)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:11 withObject:txt_qr_pin.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_qr_pin.text];
        }
        
        
        
        //         [gblclass.arr_qr_code addObject:_lbl_acct_frm.text];
        //         [gblclass.arr_qr_code addObject:str_acct_id_frm];
        
        
        
        //        [gblclass.arr_qr_code addObject:txt_amnt.text];
        //         [gblclass.arr_qr_code addObject:txt_comment.text];
        
        //NSLog(@"%@",gblclass.arr_qr_code);
        
        
        
        //  [self checkinternet];
        //  if (netAvailable)
        //  {
        chk_ssl=@"otp_check";
        [self SSL_Call];
        //  }
        
        
        // 20 march 2017
        //        gblclass.check_review_acct_type=@"QR_Code";
        //
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        //cannt pay
        //NSLog(@"cannt pay");
        
        [self custom_alert:@"Try again later." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void) slideLeft {
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    if ([theTextField isEqual:txt_amnt])
    {
        NSInteger MAX_DIGITS=12;
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
    }
    
    if ([theTextField isEqual:txt_pin])
    {
        
        MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_qr_pin])
    {
        
        MAX_DIGITS=6;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_comment])
    {
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    
    return YES;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //    //NSLog(@"text changed: %@", theTextField.text);
    //    textFieldText =[theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    //    formatter = [[NSNumberFormatter alloc] init];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //    formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];  //textFieldText
    
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText intValue]]];

    NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
    
    if ([theTextField isEqual:txt_amnt])
    {
        txt_amnt.text = [nf stringFromNumber:myNumber];
    }
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [self keyboardWillHide];
    
    [txt_cell resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_pin resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_qr_pin resignFirstResponder];
    
}

-(IBAction)btn_back:(id)sender
{
    gblclass.camera_back = @"1";
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //  [self dismissModalStack];
    
}

-(void)dismissModalStack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
////    if ([self isSSLPinning])
////    {
////        [self printMessage:@"Making pinned request"];
////    }
////    else
////    {
////        [self printMessage:@"Making non-pinned request"];
////    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            //        [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"otp_check"])
        {
            [self CheckOtp_Req_DailyLimit:@""];
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        //        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"otp_check"])
    {
        [self CheckOtp_Req_DailyLimit:@""];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        // [self checkinternet];
        // if (netAvailable)
        // {
        //    chk_ssl=@"logout";
        //    [self SSL_Call];
        // }
        
        // [self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}



-(void) CheckOtp_Req_DailyLimit:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3Session_ID_instant,@"1.1.1.1",txt_amnt.text,gblclass.Udid,gblclass.token_instant,@"before", nil] forKeys:[NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strTxtAmount",@"Device_ID",@"Token",@"Place", nil]];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],
                                                                        [encrypt encrypt_Data:gblclass.M3Session_ID_instant],
                                                                        [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                        [encrypt encrypt_Data:[txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""]],
                                                                        [encrypt encrypt_Data:gblclass.Udid],
                                                                        [encrypt encrypt_Data:gblclass.token_instant],
                                                                        [encrypt encrypt_Data:@"before"],
                                                                        [encrypt encrypt_Data:@"QR_INSTANT_MP"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTxtAmount",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"Place",@"strAccessKey", nil]];
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"CheckOTPReqQRDailyLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      gblclass.check_review_acct_type=@"QR_Code";
                      [self QR_Transaction_Instant_MP:@""];
                      
                      //10 Nov 2017
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      
                      NSLog(@"%@",gblclass.arr_qr_code);
                      NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                      
                      gblclass.check_review_acct_type=@"QR_Code";
                      
                      
                      [self GenerateOTP:@""];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if([[dic2 objectForKey:@"Response"] isEqualToString:@"-2"]) // Otp not required
                  {
                      // [hud hideAnimated:YES];
                      
                      [self QR_Transaction_Instant_MP:@""];
                      
                      //                      gblclass.check_review_acct_type=@"QR_Code";
                      //
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert: [dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
                  // [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}


-(void) GenerateOTP:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    gblclass.arr_re_genrte_OTP_additn = [[NSMutableArray alloc] init];
    
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.user_id];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.M3Session_ID_instant];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"1.1.1.1"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@""];
    [gblclass.arr_re_genrte_OTP_additn addObject:[gblclass.arr_qr_code objectAtIndex:1]];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"limit_QR_INSTANT_MP"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"Addition"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"300"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"QR_INSTANT_MP"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"SMS"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"QRPINTRAN"];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.Udid];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.token_instant];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="];
    
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                   [encrypt encrypt_Data:gblclass.M3Session_ID_instant],
                                                                   [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                   [encrypt encrypt_Data:@""],
                                                                   [encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:@"limit_QR_INSTANT_MP"],
                                                                   [encrypt encrypt_Data:@"Addition"],
                                                                   [encrypt encrypt_Data:@"300"],
                                                                   [encrypt encrypt_Data:@"QR_INSTANT_MP"],
                                                                   [encrypt encrypt_Data:@"SMS"],
                                                                   [encrypt encrypt_Data:@"QRPINTRAN"],
                                                                   [encrypt encrypt_Data:gblclass.Udid],
                                                                   [encrypt encrypt_Data:gblclass.token_instant],
                                                                   [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"Device_ID",@"Token",@"M3Key",nil]];
    
    
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  
                  gblclass.check_review_acct_type=@"QR_Code_MP";
                  //
                  //               gblclass.Is_Otp_Required=@"1";
                  
                  
                  gblclass.arr_instant_p2p=[[NSMutableArray alloc] init];
                  
                  NSLog(@"%@",gblclass.arr_qr_code);
                  NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                  
                  
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.user_id];
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.M3Session_ID_instant];
                  //                  [gblclass.arr_instant_p2p addObject:@"1.1.1.1"];
                  //                  [gblclass.arr_instant_p2p addObject:@"QR_INSTANT_MP"];
                  //                  [gblclass.arr_instant_p2p addObject:@"0"];
                  //                  [gblclass.arr_instant_p2p addObject:@""];
                  //                  [gblclass.arr_instant_p2p addObject:@""];
                  //                  [gblclass.arr_instant_p2p addObject:@""];
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:7]];
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:8]];
                  //                  [gblclass.arr_instant_p2p addObject:@"PKR"]; //[gblclass.arr_qr_code objectAtIndex:5]
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:1]];
                  //                  [gblclass.arr_instant_p2p addObject:@"03332258303"];
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:2]];
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:3]];
                  //                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:9]];
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.Udid];
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
                  //                  [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
                  
                  
                  [gblclass.arr_instant_p2p addObject:gblclass.user_id];
                  [gblclass.arr_instant_p2p addObject:gblclass.M3Session_ID_instant];
                  [gblclass.arr_instant_p2p addObject:@"1.1.1.1"];
                  [gblclass.arr_instant_p2p addObject:@"QR_INSTANT_MP"];
                  [gblclass.arr_instant_p2p addObject:@"0"];
                  [gblclass.arr_instant_p2p addObject:@""];
                  [gblclass.arr_instant_p2p addObject:@""];
                  [gblclass.arr_instant_p2p addObject:@""];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:9]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:10]];
                  [gblclass.arr_instant_p2p addObject:gblclass.base_currency]; //[gblclass.arr_qr_code objectAtIndex:5]
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:1]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:0]]; //mobile num
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:2]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:3]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:11]];
                  [gblclass.arr_instant_p2p addObject:gblclass.Udid];
                  [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:7]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:8]];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"Instant_qr_otp1"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:[error localizedDescription]  :@"0"];
              
              
              
          }];
    
}



-(void)QR_Transaction_Instant_MP:(NSString *)strIndustry
{
    
    
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3Session_ID_instant,@"1.1.1.1",@"QR_INSTANT_MP",@"0",@"",@"",@"",[gblclass.arr_qr_code objectAtIndex:7],[gblclass.arr_qr_code objectAtIndex:8],[gblclass.arr_qr_code objectAtIndex:5],[gblclass.arr_qr_code objectAtIndex:1],[gblclass.arr_qr_code objectAtIndex:0],[gblclass.arr_qr_code objectAtIndex:2],@"UBL",@"",[gblclass.arr_qr_code objectAtIndex:9],gblclass.Udid,gblclass.token_instant, nil] forKeys:[NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"payAnyOneFromBankImd",@"strTxtAmount",@"strTxtComments",@"strCCY",@"scanQRCodePan",@"merchantMobileNo",@"merchantTitle",@"strBankCode",@"strOTPPIN",@"QRPIN",@"Device_ID",@"Token", nil]];
    
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    NSString *amounts = [[gblclass.arr_qr_code objectAtIndex:9] stringByReplacingOccurrencesOfString:@"," withString:@""];
    //[gblclass.arr_qr_code objectAtIndex:9]
    //@"UBL"
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],[encrypt encrypt_Data:gblclass.M3Session_ID_instant],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"QR_INSTANT_MP"],[encrypt encrypt_Data:@"0"],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:amounts],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:10]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:1]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:11]],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token_instant],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:7]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:8]], nil] forKeys:[NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"payAnyOneFromBankImd",@"strTxtAmount",@"strTxtComments",@"strCCY",@"scanQRCodePan",@"merchantMobileNo",@"merchantTitle",@"strBankCode",@"QRPIN",@"strOTPPIN",@"Device_ID",@"Token",@"strQRCodeCRC",@"strMPQRCode", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"QRTransactionInstant_MPNew" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //                  NSError *error;
              //                  NSArray *arr = (NSArray *)responseObject;
              
              NSDictionary* dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //                  NSMutableArray* arr_load_payee_list;
              
              //                  arr_load_payee_list=[[NSMutableArray alloc] init];
              //                  arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
              //
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         // [mine myfaildata];
         
         [hud hideAnimated:YES];
         [self custom_alert:@"Retry" :@"0"];
         
     }];
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
    //[alert release];
}




#define kOFFSET_FOR_KEYBOARD 70.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}





@end

