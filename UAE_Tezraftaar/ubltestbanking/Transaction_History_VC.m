//
//  Transaction_History_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 19/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Transaction_History_VC.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "GnbTranansactionHistory.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Transaction_History_VC ()<NSURLConnectionDataDelegate>
{
    NSMutableArray* transaction_detail;
    UILabel* label;
    UIImageView* img;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
    GnbTranansactionHistory *classObj ;
    NSMutableArray *cc_number,*account_typedesc;
    NSArray* row;
    NSArray* value;
    NSMutableArray* trans_display;
    NSDictionary *dic ;
    NSArray* tableData;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIStoryboard *storyboard;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSMutableArray* arr_data;
    NSString* chck_datt;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    
    NSString *dateFormatter_to;
    NSString *dateFormatter_frm;
    NSDate *to_date,*frm_date;
    NSDateFormatter *dateFormatter;
    NSDate  *dt_frm;
    NSDate* dt_to;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Transaction_History_VC
@synthesize searchResult;
@synthesize transitionController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%@",@"saki transaction history load1");
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    encrypt = [[Encrypt alloc] init];
    
    NSLog(@"%@",@"saki transaction history load2");
    
    
    classObj=[[GnbTranansactionHistory alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    arr_data=[[NSMutableArray alloc] init];
    vw_datt.hidden=YES;
    chck_datt=@"";
    ssl_count = @"0";
    dateFormatter_to = @"";
    dateFormatter_frm = @"";
    transaction_detail=[[NSMutableArray alloc] init];
    searchResult = [NSMutableArray arrayWithCapacity:[transaction_detail count]];
    
    [btn_refresh setBackgroundColor: [UIColor colorWithRed:1/255.0f green:112/255.0f blue:181/255.0f alpha:1.0f]];
    [btn_load_more setBackgroundColor: [UIColor colorWithRed:183/255.0f green:183/255.0f blue:183/255.0f alpha:1.0f]];
    
    self.searchResult=[[NSMutableArray alloc] init];
    
    NSLog(@"%@",@"saki transaction history date format start");

    NSDate *currentDate = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:-90];
    NSDate *sevenDaysAgo = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];

    //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, sevenDaysAgo);


    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"]; //dd-MMM-yyyy
    dateFormatter_frm = [dateFormatter stringFromDate:sevenDaysAgo];

    dateFormatter_to = [dateFormatter stringFromDate:currentDate];
    NSLog(@"%@",@"saki transaction history date format End");
    
    [self checkinternet];
    if (netAvailable)
    {
        NSLog(@"%@",@"saki transaction history date service start");
        chk_ssl=@"trans";
        [self SSL_Call];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [table reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    self.searchResult = [NSMutableArray arrayWithArray: [tableData filteredArrayUsingPredicate:resultPredicate]];
    //NSLog(@"Search ::  %@",self.searchResult);
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSArray* split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.transaction_id=[split objectAtIndex:4];
        gblclass.tc_access_key=[split objectAtIndex:6];
        
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        
        [self performSegueWithIdentifier:@"history_detail" sender:self];
        
    }
    else
    {
        
        NSArray* split = [[trans_display objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        gblclass.transaction_id=[split objectAtIndex:4];
        gblclass.tc_access_key=[split objectAtIndex:6];
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        
        [self performSegueWithIdentifier:@"history_detail" sender:self];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        //NSLog(@"%lu",(unsigned long)[self.searchResult count]);
        return [self.searchResult count]; // self.searchResult
    }
    else
    {
        return [trans_display count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        //  cell.textLabel.text = [self.searchResult objectAtIndex:indexPath.row];
        //  //NSLog(@"%@",[self.searchResult objectAtIndex:indexPath.row]);
        
        NSArray* split_search = [[self.searchResult objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        //NSLog(@"%@",[split_search objectAtIndex:0]);
        //NSLog(@"%@",[split_search objectAtIndex:1]);
        //NSLog(@"%@",[split_search objectAtIndex:2]);
        //NSLog(@"%@",[split_search objectAtIndex:3]);
        
        cell.textLabel.text=[split_search objectAtIndex:1];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        
        NSString* img_check;
        
        // For Image::
        
        //     //   img_check=classObj.status;
        //
        //        img_check=[split objectAtIndex:5];
        //
        //        img=(UIImageView*)[cell viewWithTag:1];
        //        // [img setImage:@"accounts.png"];
        //
        //        if ([img_check isEqualToString:@"Rejected"])
        //        {
        //            img.image=[UIImage imageNamed:@"Rejected.png"];
        //        }
        //        else if ([img_check isEqualToString:@"Successful"])
        //        {
        //            img.image=[UIImage imageNamed:@"succseful.png"];
        //        }
        //        else if ([img_check isEqualToString:@"Pending Rejected"])
        //        {
        //            img.image=[UIImage imageNamed:@"Rejected-pending-reversal.png"];
        //        }
        //        else if ([img_check isEqualToString:@"Pending"])
        //        {
        //            img.image=[UIImage imageNamed:@"pending.png"];
        //        }
        //        [cell.contentView addSubview:img];
        
        
        //For Header ::
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text=[split_search objectAtIndex:1];      //classObj.ttName;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:14];
        [cell.contentView addSubview:label];
        
        
        //For Date ::
        
        label=(UILabel*)[cell viewWithTag:3];
        label.text=[split_search objectAtIndex:3];      //classObj.transactionDate;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:8];
        [cell.contentView addSubview:label];
        
        
        //For Amount ::
        
        label=(UILabel*)[cell viewWithTag:4];
        label.text=[split_search objectAtIndex:2];     //classObj.amount;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:14];
        [cell.contentView addSubview:label];
        
        
        //For Display ID ::
        
        label=(UILabel*)[cell viewWithTag:5];
        label.text=[split_search objectAtIndex:0];     //classObj.trandisplayId;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
    }
    else
    {
        
        //NSLog(@"%lu",(unsigned long)[trans_display count]);
        
        NSArray* split = [[trans_display objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        NSString* img_check;
        
        // For Image::
        
        //img_check=classObj.status;
        
        img_check=[split objectAtIndex:5];
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        
        if ([img_check isEqualToString:@"Rejected"])
        {
            img.image=[UIImage imageNamed:@"reject_11.png"];
            cell.backgroundColor=[UIColor colorWithRed:253/255.0 green:86/255.0 blue:72/255.0 alpha:1.0];
        }
        else if ([img_check isEqualToString:@"Successful"] || [img_check isEqualToString:@"Revoked"])
        {
            img.image=[UIImage imageNamed:@"successfull_1.png"];
            cell.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        }
        else if ([img_check isEqualToString:@"REJECTED-PENDING REVERSAL"])
        {
            img.image=[UIImage imageNamed:@"pending_3.png"];//  Rejected-pending-reversal.png
            cell.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        }
        else if ([img_check isEqualToString:@"Pending Rejected"])
        {
            img.image=[UIImage imageNamed:@"Rejected-pending-reversal.png"];
            cell.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        }
        //        else if ([img_check isEqualToString:@"REJECTED-PENDING REVERSAL"])
        //        {
        //            img.image=[UIImage imageNamed:@"Rejected-pending-reversal.png"];
        //            cell.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        //        }
        
        else if ([img_check isEqualToString:@"Pending"])
        {
            img.image=[UIImage imageNamed:@"pending_3.png"]; //pending_3  253 ,86, 72
            cell.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
            
        }
        
        [cell.contentView addSubview:img];
        
        //For Header ::
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text=[split objectAtIndex:1];      //classObj.ttName;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //NSLog(@"%@",classObj.status );
        
        //For Date ::
        
        label=(UILabel*)[cell viewWithTag:3];
        label.text=[split objectAtIndex:3];      //classObj.transactionDate;//[transaction_detail objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:8];
        [cell.contentView addSubview:label];
        
        
        //For Amount ::
        
        //        NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
        //        NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
        //        NSNumberFormatter *formatter = [NSNumberFormatter new];
        //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        
        NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
        NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        
        
        
        label=(UILabel*)[cell viewWithTag:4];
        label.text= [NSString stringWithFormat:@" %@ %2@ ",gblclass.base_currency, [formatter stringFromNumber:number]];
        label.font=[UIFont systemFontOfSize:11];
        [cell.contentView addSubview:label];
        
        
        //For Display ID ::
        
        label=(UILabel*)[cell viewWithTag:5];
        label.text=[split objectAtIndex:0];
        label.font=[UIFont systemFontOfSize:10];
        [cell.contentView addSubview:label];
        
        
    }// search if close
    
    
    //    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];/// change size as you need.
    //
    //    separatorLineView.backgroundColor = [UIColor lightGrayColor];// you can also put image here
    //    [cell.contentView addSubview:separatorLineView];
    
    
    // [table setSeparatorInset:UIEdgeInsetsZero];
    
    return cell;
}

-(void) Trasanction_history:(NSString *)strid
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
//    NSDate *currentDate = [NSDate date];
//    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
//    [dateComponents setDay:-90];
//    NSDate *sevenDaysAgo = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
//
//    //NSLog(@"\ncurrentDate: %@\nseven days ago: %@", currentDate, sevenDaysAgo);
//
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
//     dateFormatter_frm = [dateFormatter stringFromDate:sevenDaysAgo];
//
//     dateFormatter_to = [dateFormatter stringFromDate:currentDate];
    
    //NSLog(@"%@",dateFormatter_to);
    
    
    NSDictionary *dictparam;
    if ([chck_datt isEqualToString:@""])
    {
//         Oct 26, 2019
//         Jan 24, 2020
        
        lbl_dat_frm.text= [NSString stringWithFormat:@"%@",dateFormatter_frm];
        lbl_dat_to.text= [NSString stringWithFormat:@"%@",dateFormatter_to];
        
//        dateFormatter_frm = @"Oct 26, 2019";
//        dateFormatter_to = @"Jan 24, 2020";
        
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                      [encrypt encrypt_Data:gblclass.M3sessionid],
                      [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                      [encrypt encrypt_Data:dateFormatter_frm],
                      [encrypt encrypt_Data:dateFormatter_to],
                      [encrypt encrypt_Data:@"sadf"],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:gblclass.token], nil]
                     
                                                forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                         @"strSessionId",
                                                         @"IP",
                                                         @"fromDate",
                                                         @"toDate",
                                                         @"ReturnMessage",
                                                         @"Device_ID",
                                                         @"Token", nil]];
    }
    else
    {
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                      [encrypt encrypt_Data:gblclass.M3sessionid],
                      [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                      [encrypt encrypt_Data:lbl_dat_frm.text],
                      [encrypt encrypt_Data:lbl_dat_to.text],
                      [encrypt encrypt_Data:@"sadf"],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:gblclass.token], nil]
                     
                                                forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                         @"strSessionId",
                                                         @"IP",
                                                         @"fromDate",
                                                         @"toDate",
                                                         @"ReturnMessage",
                                                         @"Device_ID",
                                                         @"Token", nil]];
    }
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"DisplayTransactions" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              //            NSError *error;
              
              dic=(NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              trans_display=[[NSMutableArray alloc] init];
              gblclass.arr_transaction_history=[[NSMutableArray alloc] init];
              
              
              //Condition -78
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else if([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  gblclass.arr_transaction_history =
                  [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbTranansactionHistory"];
                  
                  arr_data= [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbTranansactionHistory"];
                  
                  //NSLog(@"Done");
                  
                  //NSLog(@" transaction_id %@",[dic objectForKey:@"transaction_id"]);
                  
                  //NSLog(@"%lu",(unsigned long)[gblclass.arr_transaction_history count]);
                  
                  account_typedesc=[[NSMutableArray alloc] init];
                  classObj = [[GnbTranansactionHistory alloc] initWithDictionary:dic];
                  
                  NSMutableArray  *a ;//= [NSArray arrayWithObjects:@"1", @"2", @"3", nil];//returns a pointer to NSArray
                  a=[[NSMutableArray alloc] init];
                  
                  //NSLog(@"%@", gblclass.arr_transaction_history);
                  
                  NSUInteger *set;
                  
                  if ([gblclass.arr_transaction_history count]>0)
                  {
                      
                      
                      for (dic in gblclass.arr_transaction_history)
                      {
                          
                          set = [gblclass.arr_transaction_history indexOfObject:dic];
                          
                          NSString* tt=[dic objectForKey:@"transaction_date"];
                          
                          //NSLog(@"%@",[dic objectForKey:@"trandisplay_id"]);
                          //NSLog(@"%@",[dic objectForKey:@"tt_name"]);
                          //NSLog(@"%@",[dic objectForKey:@"status"]);
                          //NSLog(@"%@",[dic objectForKey:@"Amount"]);
                          //NSLog(@"%@",[dic objectForKey:@"transaction_date"]);
                          
                          
                          NSString* display_id=[dic objectForKey:@"trandisplay_id"];
                          [a addObject:display_id];
                          //
                          NSString* tt_name=[dic objectForKey:@"tt_name"];
                          [a addObject:tt_name];
                          
                          
                          NSString* amount=[dic objectForKey:@"Amount"];
                          [a addObject:amount];
                          
                          NSString* dat=[dic objectForKey:@"transaction_date"];
                          
                          
                          
                          NSString *str = dat;
                          NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                          [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                          
                          NSDate *date = [dateFormat dateFromString:str];
                          
                          //NSLog(@"%@",date);
                          [dateFormat setDateFormat:@"dd-MMM-yyyy"];// MMM dd, yyyy this match the one you want to be
                          NSString *converted_datt = [dateFormat stringFromDate:date];
                          //NSLog(@"%@", converted_datt);
                          
                          
                          [a addObject:converted_datt];
                          
                          
                          NSString* trnsction_id=[dic objectForKey:@"transaction_id"];
                          [a addObject:trnsction_id];
                          
                          
                          NSString* status=[dic objectForKey:@"status"];
                          [a addObject:status];
                          
                          NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                          [a addObject:TC_ACCESS_KEY];
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [trans_display addObject:bbb];
                          //NSLog(@"%@", bbb);
                          
                          [a removeAllObjects];
                          
                          //  //NSLog(@"%d",set);
                      }
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:@"Record Not Found"  :@"0"];
                  }
                  
                //NSLog(@"%@",[dic objectForKey:classObj.trandisplayId]);
                  tableData =[[NSArray alloc] initWithArray:trans_display];
                  
                  [table reloadData];
                  [hud hideAnimated:YES];
                  searchResult = [NSMutableArray arrayWithCapacity:[trans_display count]];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"]  :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
            //[mine myfaildata];
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry"  :@"0"];
              
          }];
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}




-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert2 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        
        //        [self mob_App_Logout:@""];
        
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



-(IBAction)btn_refresh:(id)sender
{
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    [self Trasanction_history:@""];
}

-(IBAction)btn_account:(id)sender
{
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_back:(id)sender
{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    gblclass.landing = @"1";
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
   // [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:NO completion:nil];
}



-(IBAction)btn_datt_frm:(id)sender
{
    if (vw_datt.isHidden==YES)
    {
        vw_datt.hidden=NO;
        chck_datt=@"from";
    }
    else
    {
        vw_datt.hidden=YES;
    }
}

-(IBAction)btn_datt_to:(id)sender
{
    if (vw_datt.isHidden==YES)
    {
        //        [datt_picker setMaximumDate:[NSDate date]];
        //        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //        NSDate *eventDate = [NSDate date];
        //        [dateFormat setDateFormat:@"MMM dd, YYYY"];
        //
        //        NSString *dateString = [dateFormat stringFromDate:eventDate];
        //        //lbl_dat_to.text = [NSString stringWithFormat:@"%@",dateString];
        //        [datt_picker setDate:eventDate];
        
        vw_datt.hidden=NO;
        chck_datt=@"to";
    }
    else
    {
        vw_datt.hidden=YES;
    }
}

-(IBAction)btn_datt_cancel:(id)sender
{
    vw_datt.hidden=YES;
}

-(IBAction)btn_datt_done:(id)sender
{
    // UIDatePicker *picker = (UIDatePicker*)txtFieldBranchYear.inputView;
    
    if ([chck_datt isEqualToString:@"from"])
    {
        
        [datt_picker setMaximumDate:[NSDate date]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *eventDate = datt_picker.date;
        [dateFormat setDateFormat:@"MMM dd, yyyy"];//   MMM dd, YYYY
        
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        lbl_dat_frm.text = [NSString stringWithFormat:@"%@",dateString];
        
        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
        
        vw_datt.hidden=YES;
        
        return ;
    }
    else
    {
        [datt_picker setMaximumDate:[NSDate date]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *eventDate = datt_picker.date;
        [dateFormat setDateFormat:@"MMM dd, yyyy"];//MMM dd, YYYY
        
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        lbl_dat_to.text = [NSString stringWithFormat:@"%@",dateString];
        
        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
        
        vw_datt.hidden=YES;
        
        
//        NSString *dateString1 = [dateFormat stringFromDate:eventDate];
//        lbl_dat_frm.text = [NSString stringWithFormat:@"%@",dateString1];
        
        
        dt_frm = [dateFormat dateFromString:lbl_dat_frm.text];
        dt_to = [dateFormat dateFromString:lbl_dat_to.text];
        
        if ([dt_frm compare:dt_to] == NSOrderedDescending)
        {
            //NSLog(@"date1 is later than date2");
            //NSLog(@"To date is less then From date.");
            
            gblclass.custom_alert_msg=@"To Date is Less than From Date ";
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            return;
        }
        else if ([dt_frm compare:dt_to] == NSOrderedAscending)
        {
            //NSLog(@"date1 is earlier than date2");
            
        }
        else
        {
            //NSLog(@"dates are the same");
        }
    }
    
}


-(IBAction)btn_datt_click:(id)sender
{
    
//    if ([chck_datt isEqualToString:@"from"])
//    {
//
//        [datt_picker setMaximumDate:[NSDate date]];
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        NSDate *eventDate = datt_picker.date;
//        [dateFormat setDateFormat:@"dd-MMM-yyyy"];//   MMM dd, YYYY
//
//        NSString *dateString = [dateFormat stringFromDate:eventDate];
//        lbl_dat_frm.text = [NSString stringWithFormat:@"%@",dateString];
//
//        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
//
//        vw_datt.hidden=YES;
//
//       // return ;
//    }
//    else
//    {
//        [datt_picker setMaximumDate:[NSDate date]];
//        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        NSDate *eventDate = datt_picker.date;
//        [dateFormat setDateFormat:@"dd-MMM-yyyy"];//MMM dd, YYYY
//
//        NSString *dateString = [dateFormat stringFromDate:eventDate];
//        lbl_dat_to.text = [NSString stringWithFormat:@"%@",dateString];
//
//        //NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
//
//        vw_datt.hidden=YES;
//
//
//        //        NSString *dateString1 = [dateFormat stringFromDate:eventDate];
//        //        lbl_dat_frm.text = [NSString stringWithFormat:@"%@",dateString1];
    
        
        dt_frm = [dateFormatter dateFromString:lbl_dat_frm.text];
        dt_to = [dateFormatter dateFromString:lbl_dat_to.text];
        
        if ([dt_frm compare:dt_to] == NSOrderedDescending)
        {
            //NSLog(@"date1 is later than date2");
            //NSLog(@"To date is less then From date.");
            
            gblclass.custom_alert_msg=@"To Date is Less than From Date ";
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            return;
        }
        else if ([dt_frm compare:dt_to] == NSOrderedAscending)
        {
            //NSLog(@"date1 is earlier than date2");
            
        }
        else
        {
            //NSLog(@"dates are the same");
        }
  //  }
    
    
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"trans";
        [self SSL_Call];
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////


- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if([chk_ssl isEqualToString:@"trans"])
        {
            chk_ssl=@"";
            [self Trasanction_history:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if([chk_ssl isEqualToString:@"trans"])
    {
        chk_ssl=@"";
        [self Trasanction_history:@""];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    NSLog(@"%@", error.localizedDescription);
}

- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
   // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end

