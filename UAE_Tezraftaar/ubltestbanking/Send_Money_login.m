//
//  Send_Money_login.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 14/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Send_Money_login.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import "Encrypt.h"

@interface Send_Money_login ()
{
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    GlobalStaticClass* gblclass;
    APIdleManager* timer_class;
    NSString *detectionString;
    
    NSString* acct_to_chck;
    NSString* str_acct_name_to;
    NSString* str_acct_no_to;
    NSString* str_acct_id_to;
    MBProgressHUD* hud;
    UILabel* label;
    NSArray* split;
    UIImageView* img;
    NSNumber *number;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pin;
    NSString* str_pw;
    int txt_nam;
    NSString* chk_ssl;
    NSString* mask_acct_no;
    Encrypt* encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@property(nonatomic,strong) IBOutlet UITextField* txt_acct_to;

@end

@implementation Send_Money_login

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    
    ssl_count = @"0";
    txt_acct_from.text=gblclass.is_default_acct_id_name;
    str_acct_name_to=gblclass.is_default_acct_id_name;
    str_acct_no_to=gblclass.is_default_acct_no;
    str_acct_id_to=gblclass.is_default_acct_id;
    lbl_acct_frm.text=gblclass.is_default_acct_no;
    acct_to_chck=gblclass.is_default_acct_no;
    
    //    split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    //
    //    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    //    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    //    attach1.bounds = CGRectMake(10, 8, 20, 10);
    //    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    //
    //    NSString* balc;
    //
    //    if ([split objectAtIndex:6]==0 || [[split objectAtIndex:6] isEqualToString:@""])
    //    {
    //        balc=@"0";
    //    }
    //    else
    //    {
    //        balc=[split objectAtIndex:6];
    //
    //    }
    //
    //    NSString *mystring =[NSString stringWithFormat:@"%@",balc];
    //    number = [NSDecimalNumber decimalNumberWithString:mystring];
    //    NSNumberFormatter *formatter = [NSNumberFormatter new];
    //    [formatter setMinimumFractionDigits:2];
    //    [formatter setMaximumFractionDigits:2];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //
    //    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
    //    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    //    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    //
    //    [mutableAttriStr1 appendAttributedString:imageStr1];
    //
    //    //lbl_balance.attributedText = mutableAttriStr1;
    //
    //
    //    lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,number];
    //
    
    txt_desp.text=@"";
    txt_amnt.text=@"";
    
    txt_comment.delegate=self;
    txt_qr_pin.delegate=self;
    
    //NSLog(@"%@",gblclass.chck_qr_send);
    if ([gblclass.chck_qr_send isEqualToString:@"1"])
    {
        
        //NSLog(@"%@",gblclass.arr_qr_code);
        
        //  split = [[gblclass.arr_qr_code objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        
        //        txt_name_frm.text=[gblclass.arr_qr_code objectAtIndex:0];
        //        txt_amnt.text=gblclass.scan_qr_amnt;
        //        str_acct_name_to=[gblclass.arr_qr_code objectAtIndex:0];
        //        str_acct_no_to=[gblclass.arr_qr_code objectAtIndex:1];
        //        str_acct_id_to=[gblclass.arr_qr_code objectAtIndex:2];
        //        acct_to_chck=[gblclass.arr_qr_code objectAtIndex:1];
        //
        //        txt_desp.text=[gblclass.arr_qr_code objectAtIndex:1];
        
        
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"load";
            [self SSL_Call];
        }
        
        btn_qr.hidden=YES;
        btn_cancl.hidden=NO;
        btn_pay.hidden=NO;
        
    }
    else
    {
        btn_qr.hidden=NO;
        btn_cancl.hidden=YES;
        btn_pay.hidden=YES;
    }
    
    vw_table.hidden=YES;
    
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    txt1.delegate=self;
    txt2.delegate=self;
    txt3.delegate=self;
    txt4.delegate=self;
    txt5.delegate=self;
    txt6.delegate=self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_scan_qr:(id)sender
{
    
    
    //    if ([txt_amnt.text isEqualToString:@""])
    //    {
    //        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
    //                                                         message:@"Enter Amount"
    //                                                        delegate:self
    //                                               cancelButtonTitle:@"Ok"
    //                                               otherButtonTitles:nil, nil];
    //
    //        [alert1 show];
    //
    //        return;
    //    }
    
    gblclass.scan_qr_amnt=txt_amnt.text;
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr_login"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(IBAction)btn_back:(id)sender
{
    
    gblclass.camera_back = @"1";
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_pay:(id)sender
{
    
    [txt1 resignFirstResponder];
    [txt2 resignFirstResponder];
    [txt3 resignFirstResponder];
    [txt4 resignFirstResponder];
    [txt5 resignFirstResponder];
    [txt6 resignFirstResponder];
    
    [txt_amnt resignFirstResponder];
    
    if ([txt_amnt.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter amount" :@"0"];
        return;
    }
    else if ([txt_amnt.text isEqualToString:@"0"])
    {
        [self custom_alert:@"Please enter valid amount" :@"0"];
        return;
    }
    else if ([txt_qr_pin.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter 6 digits QR Pin" :@"0"];
        return;
    }
    else if ([txt_qr_pin.text length]<6)
    {
        [self custom_alert:@"Please enter 6 digits QR Pin" :@"0"];
        return;
    }
    
    //    else if ([txt1.text isEqualToString:@""] || [txt2.text isEqualToString:@""] || [txt3.text isEqualToString:@""] || [txt4.text isEqualToString:@""] || [txt5.text isEqualToString:@""] || [txt6.text isEqualToString:@""])
    //    {
    //
    //
    //        [self custom_alert:@"Enter All Pin" :@"0"];
    //
    //        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter All Pin" preferredStyle:UIAlertControllerStyleAlert];
    //        //
    //        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //        //        [alert addAction:ok];
    //        //
    //        //        [self presentViewController:alert animated:YES completion:nil];
    //
    //        return;
    //    }
    
    
    
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"review";
        [self SSL_Call];
    }
    
    
    
    
    //    str_Otp=@"";
    //    str_Otp = [str_Otp stringByAppendingString:txt1.text];
    //    str_Otp = [str_Otp stringByAppendingString:txt2.text];
    //    str_Otp = [str_Otp stringByAppendingString:txt3.text];
    //    str_Otp = [str_Otp stringByAppendingString:txt4.text];
    //    str_Otp = [str_Otp stringByAppendingString:txt5.text];
    //    str_Otp = [str_Otp stringByAppendingString:txt6.text];
    
    
    
    //       [self showAlertwithcancel:@"Transaction Processed Successfully" :@"Attention"];
    
    
}


-(void) slideLeft
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(void) slideRight
{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil,nil];
                       [alert1 show];
                   });
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    
    
    
    if (buttonIndex == 1)
    {
        
        //        [self checkinternet];
        //        if (netAvailable)
        //        {
        //            chk_ssl=@"logout";
        //            [self SSL_Call];
        //        }
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    else if(buttonIndex == 0)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//
//
//        NSString *first_time_chk = [[NSUserDefaults standardUserDefaults]
//                                    stringForKey:@"first_time"];
//
//        //NSLog(@"%@",first_time_chk);
//
//
//        if ([first_time_chk isEqualToString:@"first_time"] || [first_time_chk isEqualToString:@"second_time"])
//        {
//            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//            [self presentViewController:vc animated:NO completion:nil];
//
//        }
//        else
//        {
//            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//            [self presentViewController:vc animated:NO completion:nil];
//
//        }
//}



-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //   BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    
    if ([theTextField isEqual:txt_desp])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    if ([theTextField isEqual:txt_comment])
    {
        MAX_DIGITS=30;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    if ([theTextField isEqual:txt_qr_pin])
    {
        
        MAX_DIGITS=6;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    
    if ([theTextField isEqual:txt_amnt])
    {
        
        MAX_DIGITS=10;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    
    
    
    //
    //    NSString *str = [theTextField.text stringByReplacingCharactersInRange:range withString:string];
    //    //NSLog(@"Responder :: %@",str);
    //
    //    if ([arr_pin_pattern count]>1)
    //    {
    //
    //        str_pw =[NSString stringWithFormat:@"%d",[theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
    //
    //        //NSLog(@"sre pw :: %@",str_pw);
    //        theTextField.text = string;
    //
    //        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    //
    //        if ([theTextField.text rangeOfCharacterFromSet:set].location == NSNotFound)
    //        {
    //            //NSLog(@"NO SPECIAL CHARACTER");
    //        }
    //        else
    //        {
    //            //NSLog(@"HAS SPECIAL CHARACTER");
    //            theTextField.text=@"";
    //
    //            return 0;
    //        }
    //
    //        if( [str length] > 0 )
    //        {
    //            //   [arr_pin_pattern removeObjectAtIndex:0];
    //
    //            int j;
    //            int i;
    //            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
    //            {
    //                j=[[arr_pw_pin objectAtIndex:i] integerValue];
    //
    //                if (j>theTextField.tag)
    //                {
    //                    j=i;
    //                    break;
    //                }
    //
    //            }
    //
    //
    //            theTextField.text = string;
    //            UIResponder* nextResponder = [theTextField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
    //
    //            //NSLog(@"Responder :: %@",str);
    //            //NSLog(@"tag %ld",(long)theTextField.tag);
    //
    //            if (nextResponder)
    //            {
    //                [theTextField resignFirstResponder];
    //                [nextResponder becomeFirstResponder];
    //            }
    //
    //
    //            if (theTextField.tag == 6)
    //            {
    //                [hud showAnimated:YES];
    //                [self.view addSubview:hud];
    //                [hud showAnimated:YES];
    //
    //                txt6.text=string;
    //
    //                str_pin=@"";
    //                str_pin = [str_pin stringByAppendingString:txt1.text];
    //                str_pin = [str_pin stringByAppendingString:txt2.text];
    //                str_pin = [str_pin stringByAppendingString:txt3.text];
    //                str_pin = [str_pin stringByAppendingString:txt4.text];
    //                str_pin = [str_pin stringByAppendingString:txt5.text];
    //                str_pin = [str_pin stringByAppendingString:txt6.text];
    //
    //       //       [self SSL_Call];
    //
    //
    //               //   [self showAlertwithcancel:@"Transaction Processed Successfully" :@"Attention"];
    //
    //
    //               // return 0;
    //            }
    //
    //            return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //        }
    //
    //    }
    //
    //    //NSLog(@"%ld",(long)theTextField.tag);
    //
    //    if( [str length] > 0 )
    //    {
    //        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //    }
    //    else
    //    {
    //        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
    //
    //        txt_nam=theTextField.tag;
    //        [self txt_field_back_move];
    //        UIResponder* nextResponder = [theTextField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
    //
    //        //NSLog(@"Responder :: %@",nextResponder);
    //
    //        if (nextResponder)
    //        {
    //            [theTextField resignFirstResponder];
    //            [nextResponder becomeFirstResponder];
    //        }
    //
    //        //textField.text=@"";
    //        return 0;
    //    }
    //
    
    
    
    
    return YES;
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [txt_comment resignFirstResponder];
    
    [txt_name_frm resignFirstResponder];
    
    [txt_desp resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_qr_pin resignFirstResponder];
    
    
    [txt1 resignFirstResponder];
    [txt2 resignFirstResponder];
    [txt3 resignFirstResponder];
    [txt4 resignFirstResponder];
    [txt5 resignFirstResponder];
    [txt6 resignFirstResponder];
    
    // [self keyboardWillHide];
}





///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        
        if ([chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            [self Check_OTP_ReqDaily_Limit:@""];
            
            // [self QR_Instant_Transaction:@""];
        }
        else if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self Get_QR_Account_Info:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        [self Check_OTP_ReqDaily_Limit:@""];
        
        // [self QR_Instant_Transaction:@""];
    }
    else if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self Get_QR_Account_Info:@""];
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////





#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString  :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void) Get_QR_Account_Info:(NSString *)strIndustry
{
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSLog(@"%@",gblclass.arr_qr_code);
        NSLog(@"%@",gblclass.user_id);
        
          NSLog(@"%@",gblclass.user_id);
          NSLog(@"%@",gblclass.M3Session_ID_instant);
          NSLog(@"%@",gblclass.Udid);
          NSLog(@"%@",[gblclass.arr_qr_code objectAtIndex:0]);
        
       // NSNumber* session =[NSNumber number] gblclass.M3Session_ID_instant;
        
        
//        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//        f.numberStyle = NSNumberFormatterDecimalStyle;
//        NSNumber *myNumber = [f numberFromString:gblclass.M3Session_ID_instant];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],[encrypt encrypt_Data:gblclass.M3Session_ID_instant],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]], nil] forKeys:

                                   [NSArray arrayWithObjects:@"userid",@"strSessionId",@"IP",@"strDeviceid",@"strQR_String", nil]];
        

//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],[encrypt encrypt_Data:@"123456"],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]], nil] forKeys:
//
//                                   [NSArray arrayWithObjects:@"userid",@"strSessionId",@"IP",@"strDeviceid",@"strQR_String", nil]];
        
      
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"GetQRAccountInfo" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
                  NSDictionary*   dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      
                      
                      //     [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      [self showAlertwithcancel:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      NSMutableArray* arr_display_trans;
                      
                      arr_display_trans=[[NSMutableArray alloc] init];
                      arr_display_trans=[dic objectForKey:@"dtAccInfo"];
                      
                      
                      for (dic in arr_display_trans)
                      {
                          
                          txt_name_frm.text=[dic objectForKey:@"AccountTitle"];
                          mask_acct_no=[dic objectForKey:@"Account_No"];
                          
                          NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                          
                          if ([gblclass.arr_qr_code count]>13)
                          {
                              [gblclass.arr_qr_code replaceObjectAtIndex:13 withObject:txt_name_frm.text];
                          }
                          else
                          {
                              [gblclass.arr_qr_code addObject:txt_name_frm.text];
                          }
                          
                          
                          NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                          
                          if ([gblclass.arr_qr_code count]>14)
                          {
                              [gblclass.arr_qr_code replaceObjectAtIndex:13 withObject:[dic objectForKey:@"Account_No"]]; //060510947958
                          }
                          else
                          {
                              [gblclass.arr_qr_code addObject:[dic objectForKey:@"Account_No"]];// 060510947958
                          }
                          
                      }
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self slideLeft];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      gblclass.camera_back = @"1";
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:[dic objectForKey:@"strReturnMessage"]
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                      [alert1 show];
                      [self dismissViewControllerAnimated:NO completion:nil];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        
        NSLog(@"%@",exception.reason);
        NSLog(@"%@",exception.name);
        NSLog(@"%@",exception.observationInfo);
        NSLog(@"%@",exception.userInfo);
        NSLog(@"%@",exception.description);
        NSLog(@"%@",exception.description);
        NSLog(@"%@",exception.debugDescription);
        
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




-(void) Check_OTP_ReqDaily_Limit:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3Session_ID_instant,@"1.1.1.1",txt_amnt.text,gblclass.Udid,gblclass.token_instant,@"before", nil] forKeys:
        //
        //                                   [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strTxtAmount",@"Device_ID",@"Token",@"place", nil]];
        
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],
                                    [encrypt encrypt_Data:gblclass.M3Session_ID_instant],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:txt_amnt.text],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token_instant],
                                    [encrypt encrypt_Data:@"before"],[encrypt encrypt_Data:@"QR_INSTANT_P2P"], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"userID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTxtAmount",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"place",
                                                                       @"strAccessKey", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"CheckOTPReqQRDailyLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
                  
                  NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      
                      //    [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                      
                      
                      
                      [self showAlertwithcancel:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      
                      
                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      
                      
                      [self GenerateOTP:@""];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      
                  }
                  else if([[dic objectForKey:@"Response"] isEqualToString:@"-2"]) // Otp not required
                  {
                      //[hud hideAnimated:YES];
                      
                      
                      
                      [self P2P_Instant_Transaction:@""];
                      
                      //                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      //
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert: [dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  
                  // [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



-(void) P2P_Instant_Transaction:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        
        split = [[gblclass.arr_instant_qr objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        //    NSArray* split_to=[gblclass.arr_qr_code ];
        
        
        
        //        [gblclass.arr_instant_p2p addObject:gblclass.user_id];
        //        [gblclass.arr_instant_p2p addObject:gblclass.M3Session_ID_instant];
        //        [gblclass.arr_instant_p2p addObject:@"1.1.1.1"];
        //        [gblclass.arr_instant_p2p addObject:@"QR_INSTANT_P2P"];
        //        [gblclass.arr_instant_p2p addObject:[split objectAtIndex:3]];
        //        [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:2]];
        //        [gblclass.arr_instant_p2p addObject:txt_amnt.text];
        //        [gblclass.arr_instant_p2p addObject:txt_comment.text];
        //        [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:0]];
        //        [gblclass.arr_instant_p2p addObject:txt_qr_pin.text];
        //        [gblclass.arr_instant_p2p addObject:gblclass.Udid];
        //        [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],[encrypt encrypt_Data:gblclass.M3Session_ID_instant],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"QR_INSTANT_P2P"],[encrypt encrypt_Data:[split objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:2]],[encrypt encrypt_Data:txt_amnt.text],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:txt_qr_pin.text],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token_instant], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccount",@"payAnyOneToAccount",@"strTxtAmount",@"strTxtComments",@"QR_ReferenceString",@"strOTPPIN",@"QRPIN",@"Device_ID",@"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        //  P2PInstantTransaction
        [manager POST:@"P2PInstantTransactionLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  
                  NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      //      [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      //
                      [hud hideAnimated:YES];
                      
                      //   [self showAlertwithcancel:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      gblclass.qr_prompt=@"1";
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                      
                      
                      
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert: [dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
                  // [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}





-(void) GenerateOTP:(NSString *)strIndustry
{
    
    @try {
         
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    gblclass.arr_re_genrte_OTP_additn = [[NSMutableArray alloc] init];
    
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.user_id];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.M3Session_ID_instant];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"1.1.1.1"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@""];
    [gblclass.arr_re_genrte_OTP_additn addObject:mask_acct_no];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"limit_QR_INSTANT_P2P"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"Addition"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"300"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"QR_INSTANT_P2P"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"SMS"];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"QRPINTRAN"];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.Udid];
    [gblclass.arr_re_genrte_OTP_additn addObject:gblclass.token_instant];
    [gblclass.arr_re_genrte_OTP_additn addObject:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="];
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    NSLog(@"%@",gblclass.M3Session_ID_instant);
        
        
    
    // [gblclass.arr_qr_code objectAtIndex:1]  lbl_acct_frm
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                                   [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.user_id]],
                                                                   [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.M3Session_ID_instant]],
                                                                   [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                   [encrypt encrypt_Data:@""],
                                                                   [encrypt encrypt_Data:mask_acct_no],
                                                                   [encrypt encrypt_Data:@"limit_QR_INSTANT_P2P"],
                                                                   [encrypt encrypt_Data:@"Addition"],
                                                                   [encrypt encrypt_Data:@"300"],
                                                                   [encrypt encrypt_Data:@"QR_INSTANT_P2P"],
                                                                   [encrypt encrypt_Data:@"SMS"],
                                                                   [encrypt encrypt_Data:@"QRPINTRAN"],
                                                                   [encrypt encrypt_Data:gblclass.Udid],
                                                                   [encrypt encrypt_Data:gblclass.token_instant],
                                                                   [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"Device_ID",@"Token",@"M3Key",nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic1 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              
              NSString*   responsecode = [dic1 objectForKey:@"Response"];
              NSString* responsemsg = [dic1 objectForKey:@"strReturnMessage"];
              
              
              if ([[dic1 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic1 objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[dic1 objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //   [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  gblclass.check_review_acct_type=@"QR_Code_p2p";
                  
                  //   gblclass.Is_Otp_Required=@"1";
                  
                  
                  
                  gblclass.arr_instant_p2p=[[NSMutableArray alloc] init];
                  
                  
                  split = [[gblclass.arr_instant_qr objectAtIndex:0] componentsSeparatedByString: @"|"];
                  
                  //    NSArray* split_to=[gblclass.arr_qr_code ];
                  
                  
                  
                  [gblclass.arr_instant_p2p addObject:gblclass.user_id];
                  [gblclass.arr_instant_p2p addObject:gblclass.M3Session_ID_instant];
                  [gblclass.arr_instant_p2p addObject:@"1.1.1.1"];
                  [gblclass.arr_instant_p2p addObject:@"QR_INSTANT_P2P"];
                  [gblclass.arr_instant_p2p addObject:[split objectAtIndex:3]];
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:2]];
                  [gblclass.arr_instant_p2p addObject:txt_amnt.text];
                  [gblclass.arr_instant_p2p addObject:@""];//comments
                  [gblclass.arr_instant_p2p addObject:[gblclass.arr_qr_code objectAtIndex:0]];
                  [gblclass.arr_instant_p2p addObject:txt_qr_pin.text];
                  [gblclass.arr_instant_p2p addObject:gblclass.Udid];
                  [gblclass.arr_instant_p2p addObject:gblclass.token_instant];
                  
                  
                  
                  
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"Instant_qr_otp1"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
            
          }];
    
    }
    @catch (NSException *exception)
    {
        
        
        NSLog(@"%@",exception.reason);
        NSLog(@"%@",exception.description);
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
        
    }
    
}






@end

