//
//  Req_New_ATM_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Req_New_Atm_Detail_VC.h"
#import "Reachability.h"



@interface Req_New_ATM_VC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    IBOutlet UITableView* typetableview;
    IBOutlet UITableView* gendertableview;
    IBOutlet UITableView* statustableview;
    IBOutlet UIButton* btn_check;
    IBOutlet UIButton* btn_check1;
    IBOutlet UIButton* btn_virtualWiz_check;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UILabel* lbl_cardHolder;
    IBOutlet UIView* top_view;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}





@property (nonatomic, strong) TransitionDelegate *transitionController;

@property (nonatomic, strong) IBOutlet UITextField *txtfieldcardtype;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldprimarycardname;

@property (nonatomic, strong) IBOutlet UIButton *cardtypeddbtn;
@property (nonatomic, strong) IBOutlet UIButton *genderbtn;
@property (nonatomic, strong) IBOutlet UIButton *statusbtn;

@property (nonatomic, strong) IBOutlet UIView *cardtypeddview;
@property (nonatomic, strong) IBOutlet UIView *genderview;
@property (nonatomic, strong) IBOutlet UIView *statusview;
@property (nonatomic, strong) IBOutlet UIView *primaryview;
@property (nonatomic, strong) IBOutlet UIView *supplementaryview;
@property (nonatomic, strong) IBOutlet UIView *dateview;

//@property (nonatomic, strong) IBOutlet UILabel *lblcardtype;
@property (nonatomic, strong) IBOutlet UITextField *lblgender;
@property (nonatomic, strong) IBOutlet UITextField *lblstatus;


@property (nonatomic, strong) IBOutlet UIScrollView *scrollview;
@property (nonatomic, strong) IBOutlet UIDatePicker *datePicker;
@property(nonatomic,strong) IBOutlet UIView* vw_table;


//- (IBAction)cardtypeddclickbtn:(UIButton *)sender;
//- (IBAction)genderclickbtn:(UIButton *)sender;
//- (IBAction)statusclickbtn:(UIButton *)sender;


@property (nonatomic, strong) IBOutlet UITextField *txtfieldsupcardname;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldfirstname;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldsurname;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldemail;
@property (nonatomic, strong) IBOutlet UITextField *txtfielddob;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldkeyowrd;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldpoc;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldmailingaddress;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldrelationship;
@property (nonatomic, strong) IBOutlet UITextField *txtfieldcnic;


-(IBAction)btn_chck:(id)sender;
-(IBAction)btn_cancel:(id)sender;

@end
