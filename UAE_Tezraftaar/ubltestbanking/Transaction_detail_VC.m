//
//  Transaction_detail_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 19/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Transaction_detail_VC.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "GnbTransactionHistoryDetail.h"
#import "AFNetworking.h"
#import "Slide_menu_VC.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Transaction_detail_VC ()<NSURLConnectionDataDelegate>
{
    UIAlertController *alert;
    MBProgressHUD* hud;
    GlobalStaticClass* gblclass;
    GnbTransactionHistoryDetail *classObj;
    NSDictionary *dic;
    NSMutableArray* trans_display;
    NSMutableArray *cc_number,*account_typedesc;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIStoryboard *storyboard;
    NSNumberFormatter *formatter;
    APIdleManager* timer_class;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Transaction_detail_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    encrypt = [[Encrypt alloc] init];
    ssl_count = @"0";
    
    classObj=[[GnbTransactionHistoryDetail alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    
    lbl_app_id.hidden=YES;
    lbl_app_id_label.hidden=YES;
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"trans";
        [self SSL_Call];
    }
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        
//        
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    img_cr.image=[UIImage imageNamed:@"succsefull_1.png"];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"inner-bg2.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) Trasanction_detail:(NSString *)strid
{
    
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.transaction_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strTransactionID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"DisplayTransactionsDetails" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  //            NSError *error;
                  dic=(NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  trans_display=[[NSMutableArray alloc] init];
                  gblclass.arr_transaction_detail=[[NSMutableArray alloc] init];
                  GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
                  
                  
                  //Condition -78
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  gblclass.arr_transaction_detail = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbTransactionHistoryDetail"];
                  
                  //NSLog(@"Done");
                  
                  //NSLog(@" transaction_id %@",[dic objectForKey:@"dlblTranID"]);
                  
                  //            //NSLog(@"%lu",(unsigned long)[gblclass.arr_transaction_detail count]);
                  
                  account_typedesc=[[NSMutableArray alloc] init];
                  classObj = [[GnbTransactionHistoryDetail alloc] initWithDictionary:dic];
                  
                  NSMutableArray  *a;
                  a=[[NSMutableArray alloc] init];
                  
                  
                  //NSLog(@"%@",[dic objectForKey:@"Response"]);
                  NSString* resp=[dic objectForKey:@"Response"];
                  
                  if (![resp isEqualToString:@"0"])
                  {
                      //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                  [alert addAction:ok];
                      //
                      //                  [self presentViewController:alert animated:YES completion:nil];
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      [hud hideAnimated:YES];
                      
                      return ;
                  }
                  
                  
                  //              NSUInteger *set;
                  for (dic in gblclass.arr_transaction_detail)
                  {
                      
                      //                  NSString* tt=[dic objectForKey:@"dlblTranID"];
                      //
                      //                  //NSLog(@"%@",[dic objectForKey:@"dlblTranType"]);
                      //                  //NSLog(@"%@",[dic objectForKey:@"dlblAmount"]);
                      //                  //NSLog(@"%@",[dic objectForKey:@"dlblToAcc"]);
                      //                  //NSLog(@"%@",[dic objectForKey:@"dlblStatus"]);
                      //                  //NSLog(@"%@",[dic objectForKey:@"dlblInitByCustomer"]);
                      //
                      //                  lbl_transaction_id.text=[dic objectForKey:@"dlblTranID"];
                      //                  lbl_frm_acct.text=[dic objectForKey:@"dlblFromAcc"];
                      //                  lbl_amount.text=[dic objectForKey:@"dlblAmount"];
                      //                  lbl_status.text=[dic objectForKey:@"dlblStatus"];
                      //                  lbl_date.text=[dic objectForKey:@"dlblInitByCustomer"];
                      //                  lbl_app_id.text=[dic objectForKey:@""];
                      //                  lbl_transaction_type.text=[dic objectForKey:@"dlblTranType"];
                      //                  lbl_product_nick.text=[dic objectForKey:@"dlblToAccNick"];
                      //                  lbl_frm_acct_no.text=[dic objectForKey:@"dlblToAcc"];
                      
                      
                      //                 NSString* tt=[dic objectForKey:@"dlblTranID"];
                      
                      //NSLog(@"%@",[dic objectForKey:@"dlblTranType"]);
                      //NSLog(@"%@",[dic objectForKey:@"dlblAmount"]);
                      //NSLog(@"%@",[dic objectForKey:@"dlblToAcc"]);
                      //NSLog(@"%@",[dic objectForKey:@"dlblStatus"]);
                      //NSLog(@"%@",[dic objectForKey:@"dlblInitByCustomer"]);
                      
                      if ([gblclass.tc_access_key isEqualToString:@"PPV"])
                      {
                          
                          lbl_transaction_id_label.text = @"Voucher No.";
                          lbl_transaction_id.text=[dic objectForKey:@"dlblToAcc"];
                          lbl_amount_label.text = @"Transaction Amount";
                          lbl_frm_acct_no1.hidden=YES;
                          lbl_expireDate_title.hidden=NO;
                          lbl_expireDate_label.hidden=NO;
                          lbl_expireDate_label.text = [dic objectForKey:@"dlblVoucherExpDate"];
                          
                      } else if ([gblclass.tc_access_key isEqualToString:@"TP"])
                      {
                          
                          lbl_frm_acct_no1.hidden=NO;
                          lbl_expireDate_title.text=[dic objectForKey:@"lblComments"];
                          lbl_expireDate_title.hidden=NO;
                          lbl_expireDate_label.text=[dic objectForKey:@"dlblComments"];
                          lbl_expireDate_label.hidden=NO;
                          
                          lbl_transaction_id.text=[dic objectForKey:@"dlblTranID"];
                          
                      }
                      else
                      {
                          lbl_frm_acct_no1.hidden=NO;
                          lbl_expireDate_title.hidden=YES;
                          lbl_expireDate_label.hidden=YES;
                          lbl_transaction_id.text=[dic objectForKey:@"dlblTranID"];
                      }
                      
                      
                      lbl_frm_acct.text=[dic objectForKey:@"dlblFromAcc"];
                      lbl_status.text=[dic objectForKey:@"dlblStatus"];
                      
                      
                      NSString* name = [dic objectForKey:@"lblVoucherExpDate"];
                      
                      if (name ==(NSString*) [NSNull null])
                      {
                          lbl_amount_label_QR_stan.text = [dic objectForKey:@"lblAmount"];
                          lbl_amount_QR_stan.text = [dic objectForKey:@"dlblAmount"];
                          lbl_amount_QR_stan.adjustsFontSizeToFitWidth = YES;
                          
                          lbl_to_acc_number_label.text = [dic objectForKey:@"lblToAcc"];
                          lbl_to_acc_number.text = [dic objectForKey:@"dlblToAcc"];
                          lbl_to_acc_number.adjustsFontSizeToFitWidth = YES;
                          //[lbl_to_acc_number adjustFontSizeToFillItsContents];
                          lbl_to_acc_number.minimumScaleFactor = 0.5;
                          
                          lbl_amount.hidden = YES;
                          lbl_amount_label.hidden = YES;
                      }
                      else
                      {
                          lbl_amount_label_QR_stan.text = [dic objectForKey:@"lblVoucherExpDate"];
                          lbl_amount_QR_stan.text = [dic objectForKey:@"dlblVoucherExpDate"];
                          lbl_amount_QR_stan.adjustsFontSizeToFitWidth = YES;
                          
                          lbl_to_acc_number_label.text = [dic objectForKey:@"lblToAcc"];
                          lbl_to_acc_number.text = [dic objectForKey:@"dlblToAcc"];
                          lbl_to_acc_number.adjustsFontSizeToFitWidth = YES;
                          lbl_to_acc_number.minimumScaleFactor = 0.5;
                          
                          lbl_amount.text = [dic objectForKey:@"dlblAmount"];
                          lbl_amount_label.text = [dic objectForKey:@"lblAmount"];
                      }
                     
                      
                      NSString *str = [dic objectForKey:@"dlblInitByCustomer"];
                      NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                      [dateFormat setDateFormat:@"dd-MMM-yyyy"];
                      
                      NSDate *date = [dateFormat dateFromString:str];
                      
                      //NSLog(@"%@",date);
                      [dateFormat setDateFormat:@"dd-MMM-yyyy"];// //dd-MMM-yyyy  this match the one you want to be
                      NSString *converted_datt = [dateFormat stringFromDate:date];
                      //NSLog(@"%@", converted_datt);
                      
                      
                      lbl_date.text=converted_datt;
                      [a addObject:converted_datt];
                      lbl_app_id.text=[dic objectForKey:@""];
                      
                      lbl_transaction_type.text = [NSString stringWithFormat:@"%@ Performed\r%@",[dic objectForKey:@"dlblFromAcc"],[dic objectForKey:@"dlblTranType"]];
                      //lbl_transaction_type.text=[dic objectForKey:@"dlblTranType"];
                      lbl_product_nick.text=[dic objectForKey:@"dlblToAccNick"];
                      lbl_frm_acct_no1.text=[dic objectForKey:@"dlblToAcc"];
                      lbl_frm_acct_no1.text=[dic objectForKey:@"dlblToAcc"];
                      
                      
                      
                      if ([[dic objectForKey:@"dlblFromAcc"] length]>0)
                      {
                          
                      }
                      else
                      {
                          lbl_frm_acct.text=@"N/A";
                      }
                      
                      if ([[dic objectForKey:@"dlblStatus"] isEqualToString:@"Rejected"])
                      {
                          
                          //                      UIColor *headingColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          //                      UIColor *textColor = [UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          
                          lbl_transaction_id.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          lbl_frm_acct.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_to_acc_number.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_status.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          lbl_date.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_transaction_type.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_frm_acct_no1.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          
                          lbl_amount_label.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                         lbl_to_acc_number_label.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          
                          lbl_amount_label_QR_stan.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                          lbl_amount_QR_stan.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          
                          lbl_expireDate_title.textColor=[UIColor colorWithRed:123/255.0 green:11/255.0 blue:0/255.0 alpha:1.0];
                           lbl_expireDate_label.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          
                          img_cr.image=[UIImage imageNamed:@"reject_11_d.png"];
                          self.view.backgroundColor=[UIColor colorWithRed:253/255.0 green:86/255.0 blue:72/255.0 alpha:1.0];
                      }
                      else if ([[dic objectForKey:@"dlblStatus"] isEqualToString:@"Successful"] || [[dic objectForKey:@"dlblStatus"] isEqualToString:@"Revoked"])
                      {
                          
                          lbl_transaction_id.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_frm_acct.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_to_acc_number.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_status.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_date.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_transaction_type.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_frm_acct_no1.textColor=[UIColor colorWithRed:165/255.0 green:218/255.0 blue:255/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
                          lbl_amount_label.textColor=[UIColor colorWithRed:165/255.0 green:218/255.0 blue:255/255.0 alpha:1.0];
                           lbl_to_acc_number_label.textColor=[UIColor colorWithRed:165/255.0 green:218/255.0 blue:255/255.0 alpha:1.0];
                          
                          lbl_expireDate_label.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          
                          lbl_amount_label_QR_stan.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          lbl_amount_QR_stan.textColor=[UIColor colorWithRed:170/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
                          
                          //NSLog(@"%@",lbl_frm_acct_no1.text);
                          
                          img_cr.image=[UIImage imageNamed:@"succsefull_1.png"];
                          
                          UIGraphicsBeginImageContext(self.view.frame.size);
                          [[UIImage imageNamed:@"inner-bg2.png"] drawInRect:self.view.bounds];
                          UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                          UIGraphicsEndImageContext();
                          
                          self.view.backgroundColor = [UIColor colorWithPatternImage:image];
                          
                          
                          //  self.view.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
                      }
                      else if ([[dic objectForKey:@"dlblStatus"] isEqualToString:@"Pending Rejected"])
                      {
                          img_cr.image=[UIImage imageNamed:@"Rejected-pending-reversal.png"];
                      }
                      else if ([[dic objectForKey:@"dlblStatus"] isEqualToString:@"Pending"])
                      {//Rejected-Pending Reversal
                          img_cr.image=[UIImage imageNamed:@"pending_3_d.png"]; //pending_3  253 ,86, 72
                          
                          //[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          
                          
                          lbl_frm_acct.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_to_acc_number.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_status.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_date.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_type.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_transaction_type_label.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_id_label.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_id.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_amount_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_frm_acct_no1.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_expireDate_title.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_expireDate_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_to_acc_number_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_amount_label_QR_stan.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_amount_QR_stan.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          self.view.backgroundColor=[UIColor colorWithRed:255/255.0 green:211/255.0 blue:63/255.0 alpha:1.0];
                          
                          
                          //                      NSTextAttachment * attach = [[NSTextAttachment alloc] init];
                          //                      attach.image = [UIImage imageNamed:@"Pkr_yellow.png"];
                          //                      attach.bounds = CGRectMake(0, 0, 20, 12);
                          //                      NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
                          //
                          //                      NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [dic objectForKey:@"dlblAmount"]]];
                          //                      NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0]};
                          //                      [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
                          //
                          //                      [mutableAttriStr appendAttributedString:imageStr];
                          //                      lbl_amount.attributedText = mutableAttriStr;
                          
                      }
                      else if ([[dic objectForKey:@"dlblStatus"] isEqualToString:@"Rejected-Pending Reversal"])
                      {
                          img_cr.image=[UIImage imageNamed:@"pending_3_d.png"]; //pending_3  253 ,86, 72
                          
                          //[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          
                          
                          lbl_frm_acct.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_amount.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                           lbl_to_acc_number.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_status.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_date.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_type.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_transaction_type_label.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_id_label.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_transaction_id.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_amount_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                           lbl_to_acc_number_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_amount.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_amount_label_QR_stan.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_amount_QR_stan.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          lbl_frm_acct_no1.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          lbl_expireDate_title.textColor=[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0];
                          lbl_expireDate_label.textColor=[UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
                          
                          
                          
                          self.view.backgroundColor=[UIColor colorWithRed:255/255.0 green:211/255.0 blue:63/255.0 alpha:1.0];
                          
                          
                          //                      NSTextAttachment * attach = [[NSTextAttachment alloc] init];
                          //                      attach.image = [UIImage imageNamed:@"Pkr_yellow.png"];
                          //                      attach.bounds = CGRectMake(0, 0, 20, 12);
                          //                      NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
                          //
                          //                      NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [dic objectForKey:@"dlblAmount"]]];
                          //                      NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0/255.0 green:3/255.0 blue:0/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0]};
                          //                      [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
                          //
                          //                      [mutableAttriStr appendAttributedString:imageStr];
                          //                      lbl_amount.attributedText = mutableAttriStr;
                          
                      }
                      [hud hideAnimated:YES];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Retry" :@"0"];
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Retry" :@"0"];
    }
    
    
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert2 show];
                   });
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        [self mob_App_Logout:@""];
        
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_screenshot:(id)sender
{
    [self takeAScreenShot];
    
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.bounds];
    flashView.backgroundColor = [UIColor whiteColor];
    flashView.alpha = 1.0;
    
    // Add the flash view to the window
    
    [self.view addSubview:flashView];
    
    // Fade it out and remove after animation.
    
    [UIView animateWithDuration:0.20 animations:^{
        flashView.alpha = 0.0;
    }
                     completion:^(BOOL finished) {
                         [flashView removeFromSuperview];
                     }];
    
    
    // For Sound Play ::
    
    
    //    /* Use this code to play an audio file */
    //    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"test"  ofType:@"m4a"];
    //    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    //
    //    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    //    player.numberOfLoops = -1; //Infinite
    //
    //    [player play];
    
    
}

-(UIImage *) takeAScreenShot
{
    
    @try {
        
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {// checking for Retina display
        UIGraphicsBeginImageContextWithOptions(keyWindow.bounds.size, YES, [UIScreen mainScreen].scale);
        //if this method is not used for Retina device, image will be blurr.
    }
    else
    {
        UIGraphicsBeginImageContext(keyWindow.bounds.size);
    }
    
    [keyWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // now storing captured image in Photo Library of device
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    /*** if you want to save captured image locally in your app's document directory
     NSData * data = UIImagePNGRepresentation(image);
     
     NSString *imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"testImage.png"];
     //NSLog(@"Path for Image : %@",imagePath);
     [data writeToFile:imagePath atomically:YES];
     
     *******************************/
    return image;
        
    }
    @catch (NSException *exception)
    {
        
    }
}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"trans"])
        {
            chk_ssl=@"";
            [self Trasanction_detail:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"trans"])
    {
        chk_ssl=@"";
        [self Trasanction_detail:@""];
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

