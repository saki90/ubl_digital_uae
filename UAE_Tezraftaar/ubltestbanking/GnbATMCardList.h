//
//  GnbATMCardList.h
//
//  Created by   on 26/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbATMCardList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *aTMFLAG1;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *aTMTYPE;
@property (nonatomic, assign) id pENDINGATM;
@property (nonatomic, assign) id pENDINGSTATUS;
@property (nonatomic, strong) NSString *aTMMAINSEQ;
@property (nonatomic, assign) id aTMREPLSEQ;
@property (nonatomic, assign) id aTMCARDTYPE;
@property (nonatomic, strong) NSString *aTMCUSTNAME;
@property (nonatomic, strong) NSString *aTMCARDNO;
@property (nonatomic, assign) id aTMNATURE;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
