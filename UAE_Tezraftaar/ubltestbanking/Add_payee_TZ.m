//
//  Add_payee_TZ.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 03/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import "Add_payee_TZ.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"


@interface Add_payee_TZ ()
{
    NSMutableArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* chk_ssl;
    NSString* ssl_count;
    Encrypt* encrypt;
    NSArray* arr_pay_type;
    NSMutableArray* a,*arr_relation;
    NSMutableArray* arr_city,*arr_branch,*arr_bank;
    NSArray* split;
    NSString* str_acct_name_frm;
    NSString* str_acct_id_frm;
    NSString* str_acct_no_frm;
    NSString* acct_frm_chck, *acct_to_chck;
    UILabel* label;
    UIImageView* img;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Add_payee_TZ
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

     NSLog(@"%@",@"saki add payee 2 option load");
            
            self.transitionController = [[TransitionDelegate alloc] init];
            //arr_act_type=[[NSArray alloc] init];
            table.delegate=self;
            hud = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:hud];
            
            encrypt = [[Encrypt alloc] init];
            arr_pay_type = [[NSArray alloc] init];
            arr_relation = [[NSMutableArray alloc] init];
            
            chk_ssl=@"";
            ssl_count = @"0";
            vw_from.hidden = YES;
            table_from.hidden = YES;
            gblclass = [GlobalStaticClass getInstance];
    
            
            gblclass.tz_pay_type = @"";
            gblclass.beneficiary_Bank = @"";
            gblclass.tz_city = @"";
            gblclass.chck_tezraftr_bene_type = @"";
            
            NSLog(@"%@",gblclass.UserType);
//            if([gblclass.UserType isEqualToString:@"1000"])
//            {
//                //arr_act_type=@[@"Add through IBAN",@"UBL Branch Account",@"UBL Omni Account",@"Other Bank Account (IBFT)", @"Transfer Funds to CNIC"];
//
//                arr_act_type=@[@"Add through IBAN",@"UBL Branch Account",@"Other Bank Account (IBFT)"];
//            }
//            else if([gblclass.UserType isEqualToString:@"4000"]||[gblclass.UserType isEqualToString:@"4096"])
//            {
//                arr_act_type=@[@"Cash Over Counter/CNIC",@"Other Bank Account",@"UBL Branch Account"]; //,@"Other Bank Account (IBFT)
//            }
    
            
    //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissAllViewControllers:) name:@"YourDismissAllViewControllersIdentifier" object:nil];
    //
    [self.view endEditing:YES];
    
    if ([gblclass.is_debit_lock isEqualToString:@"0"])
    {
        txt_acct_from.text = gblclass.is_default_acct_id_name;
        str_acct_name_frm = gblclass.is_default_acct_id_name;
        str_acct_no_frm = gblclass.is_default_acct_no;
        str_acct_id_frm = gblclass.is_default_acct_id;
        lbl_acct_frm.text = gblclass.is_default_acct_no;
        
        acct_frm_chck=gblclass.is_default_acct_no;
        
        gblclass.default_acct_num = gblclass.is_default_acct_no;
        
        //btn_submit.enabled = YES;
    }
    else
    {
        txt_acct_from.text =  @"-";
        str_acct_name_frm = @"-";
        str_acct_no_frm = @"-";
        str_acct_id_frm = @"-";
        lbl_acct_frm.text = @"-";
        
        acct_frm_chck = @"-";
        gblclass.default_acct_num = @"0";
        
       // btn_submit.enabled = NO;
    }
        
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"GetTezRaftarList";
        [self SSL_Call];
    }
     vw_from.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    }

    - (void)dismissAllViewControllers:(NSNotification *)notification
    {
        // dismiss all view controllers in the navigation stack
        [self dismissViewControllerAnimated:YES completion:^{}];
    }


    - (void)didReceiveMemoryWarning
    {
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
    }

    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return 1;    //count of section
    }

    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
        if (tableView==table_from)
        {
            return [gblclass.arr_transfer_within_acct count];    //count number of row from counting array hear cataGorry is An Array
        }
        else
        {
            return [arr_act_type count];
        }
           //count number of row from counting array hear cataGorry is An Array
    }

    - (UITableViewCell *)tableView:(UITableView *)tableView
             cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        static NSString *MyIdentifier = @"MyIdentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
        }
        
        if (tableView==table_from)
        {
            split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            //Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split objectAtIndex:0];
            label.textColor=[UIColor whiteColor];
          //  label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            
            //Acct. No ::
            label=(UILabel*)[cell viewWithTag:3];
            label.text = [split objectAtIndex:1];
            label.textColor=[UIColor whiteColor];
           // label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
            
            
//            NSString *numberString =[NSString stringWithFormat:@"%@",[split objectAtIndex:6]];
//            NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithString:numberString];
            
//            //Balc ::
//            label=(UILabel*)[cell viewWithTag:4];
//           // label.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:decimalNumber]];
//            //label.textColor=[UIColor whiteColor];
//            label.font=[UIFont systemFontOfSize:18];
//            [cell.contentView addSubview:label];
            
            
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];
            
            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            
            [cell.contentView addSubview:img];
        }
        else
        {
            split = [[arr_act_type objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            cell.textLabel.text = [split objectAtIndex:1];//[arr_act_type objectAtIndex:indexPath.row];
            
            if ([gblclass.story_board isEqualToString:@"iPhone_X"])
            {
                cell.textLabel.font=[UIFont systemFontOfSize:13];
            }
            else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
            {
                cell.textLabel.font=[UIFont systemFontOfSize:14];
            }
            else
            {
                cell.textLabel.font=[UIFont systemFontOfSize:12];
            }
        }
       
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return cell;
    }

    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        Yourstring = [arr_act_type objectAtIndex:indexPath.row];
        
        //NSLog(@"Selected item %ld ",(long)indexPath.row);
        //    UIStoryboard *mainStoryboard;
        //    UIViewController *vc;
        
              NSLog(@"%@",@"saki Add paye 2 option did select first");
        
              [table_from reloadData];
           if (tableView==table_from)
           {
               split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
               
               txt_acct_from.text=[split objectAtIndex:0];
               //str_acct_name_frm=[split objectAtIndex:0];
               lbl_acct_frm.text=[split objectAtIndex:1];
               gblclass.default_acct_num = [split objectAtIndex:1];
               
               UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
               UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
               myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
               
               table_from.hidden=YES;
               vw_from.hidden=YES;
           }
           else
           {
               
               
               NSArray* splitt = [arr_act_type objectAtIndex:indexPath.row];
               
               splitt = [[arr_act_type objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
               
               if ([[splitt objectAtIndex:1] isEqualToString:@"UBL BRANCHES"])
               {
                   if([gblclass.UserType isEqualToString:@"4000"] || [gblclass.UserType isEqualToString:@"4096"])
                   {
//                       if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                       {
//
//                           [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                       }
//                       else
//                       {
                           //cash
                           gblclass.beneficiary_Bank = @"";
                           gblclass.tz_pay_type = [splitt objectAtIndex:0];
                           gblclass.acct_add_type=@"UBL Branch Account";
                           gblclass.acct_add_header_type = @"ADD PAYEE TZ";
                           CATransition *transition = [ CATransition animation];
                           transition.duration = 0.3;
                           transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                           transition.type = kCATransitionPush;
                           transition.subtype = kCATransitionFromRight;
                           [self.view.window.layer addAnimation:transition forKey:nil];
                           
                           storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                           vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_ubl_branch"];
                           [self presentViewController:vc animated:NO completion:nil];
//                       }
                   }
               }
               else if ([[splitt objectAtIndex:1] isEqualToString:@"OTHER BANK" ])
               {
                   if([gblclass.UserType isEqualToString:@"4000"] || [gblclass.UserType isEqualToString:@"4096"])
                   {
//                       if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                       {
//
//                           [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                       }
//                       else
//                       {
                           gblclass.beneficiary_Bank = @"";
                           gblclass.tz_pay_type = [splitt objectAtIndex:0];
                           gblclass.acct_add_type=@"Other Bank";
                           gblclass.acct_add_header_type = @"ADD PAYEE TZ";
                           CATransition *transition = [ CATransition animation];
                           transition.duration = 0.3;
                           transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                           transition.type = kCATransitionPush;
                           transition.subtype = kCATransitionFromRight;
                           [self.view.window.layer addAnimation:transition forKey:nil];
                           
                           storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                           vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_ubl_branch"];
                           [self presentViewController:vc animated:NO completion:nil];
//                       }
                   }
               }
               else if ([[splitt objectAtIndex:1] isEqualToString:@"CASH OVER COUNTER"])
               {
                   if([gblclass.UserType isEqualToString:@"4000"]||[gblclass.UserType isEqualToString:@"4096"])
                   {
                       
//                       if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                       {
//
//                           [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                       }
//                       else
//                       {
                           gblclass.beneficiary_Bank = @"UBL";
                           gblclass.tz_pay_type = [splitt objectAtIndex:0];
                           gblclass.acct_add_type=@"cash"; //UBL Branch Account
                           gblclass.acct_add_header_type = @"ADD PAYEE TZ";
                           CATransition *transition = [ CATransition animation];
                           transition.duration = 0.3;
                           transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                           transition.type = kCATransitionPush;
                           transition.subtype = kCATransitionFromRight;
                           [self.view.window.layer addAnimation:transition forKey:nil];
                           
                           storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                           vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_COC_Cnic"];
                           [self presentViewController:vc animated:NO completion:nil];
                           //Tz_COC_Cnic Tz_ubl_branch
//                       }
                       
                   }
               }
               else
               {
                   
               }
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
               
//               [self checkinternet];
//               if (netAvailable)
//               {
//
//                   switch (indexPath.row)
//                   {
//
//                       case 0:
//                           //NSLog(@"%@",gblclass.UserType);
//
//                           //NSLog(@"%@",gblclass.Mobile_No);
//
//                           if([gblclass.UserType isEqualToString:@"4000"] || [gblclass.UserType isEqualToString:@"4096"])
//                           {
//                               if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                               {
//
//                                   [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                               }
//                               else
//                               {
//                                   //cash
//                                   gblclass.acct_add_type=@"UBL Branch Account";
//                                   gblclass.acct_add_header_type = @"ADD PAYEE TZ";
//                                   CATransition *transition = [ CATransition animation];
//                                   transition.duration = 0.3;
//                                   transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                                   transition.type = kCATransitionPush;
//                                   transition.subtype = kCATransitionFromRight;
//                                   [self.view.window.layer addAnimation:transition forKey:nil];
//
//                                   storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                                   vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_ubl_branch"];
//                                   [self presentViewController:vc animated:NO completion:nil];
//                               }
//                           }
//
//                           break;
//
//                       case 1:
//                           //NSLog(@"%@",gblclass.UserType);
//
//                           //NSLog(@"%@",gblclass.Mobile_No);
//
//                           if([gblclass.UserType isEqualToString:@"4000"] || [gblclass.UserType isEqualToString:@"4096"])
//                           {
//                               if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                               {
//
//                                   [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                               }
//                               else
//                               {
//                                   gblclass.acct_add_type=@"Other Bank";
//                                   gblclass.acct_add_header_type = @"ADD PAYEE TZ";
//                                   CATransition *transition = [ CATransition animation];
//                                   transition.duration = 0.3;
//                                   transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                                   transition.type = kCATransitionPush;
//                                   transition.subtype = kCATransitionFromRight;
//                                   [self.view.window.layer addAnimation:transition forKey:nil];
//
//                                   storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                                   vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_ubl_branch"];
//                                   [self presentViewController:vc animated:NO completion:nil];
//                               }
//                           }
//
//                           break;
//
//
//                       case 2:
//
//                           break;
//
//                       case 3:
//
//                           break;
//
//                       case 4:
//
//                           if([gblclass.UserType isEqualToString:@"4000"]||[gblclass.UserType isEqualToString:@"4096"])
//                           {
//
//                               if([gblclass.Mobile_No length]==0 ||[gblclass.Mobile_No length]<11)
//                               {
//
//                                   [self showAlert:@"You dont have valid registered mobile number.Please register your mobile number" :@"Attention"];
//                               }
//                               else
//                               {
//                                   gblclass.acct_add_type=@"cash"; //UBL Branch Account
//                                   gblclass.acct_add_header_type = @"ADD PAYEE TZ";
//                                   CATransition *transition = [ CATransition animation];
//                                   transition.duration = 0.3;
//                                   transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                                   transition.type = kCATransitionPush;
//                                   transition.subtype = kCATransitionFromRight;
//                                   [self.view.window.layer addAnimation:transition forKey:nil];
//
//                                   storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                                   vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_COC_Cnic"];
//                                   [self presentViewController:vc animated:NO completion:nil];
//                                   //Tz_COC_Cnic Tz_ubl_branch
//                               }
//
//                           }
//
//                           break;
//
//                       default: break;
//                   }
//
//               }

           }

         
         NSLog(@"%@",@"saki Add paye 2 option did select End");
    }


    -(void) showAlert:(NSString *)exception : (NSString *) Title{
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           UIAlertController * alert=   [UIAlertController
                                                         alertControllerWithTitle:Title
                                                         message:exception
                                                         preferredStyle:UIAlertControllerStyleAlert];
                           UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                              style:UIAlertActionStyleDefault
                                                                            handler:nil]; //You can use a block here to handle a press on this button
                           [alert addAction:actionOk];
                           
                           [self presentViewController:alert animated:YES completion:nil];
                       });
        //[alert release];
    }

    - (void)slideLeft {
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [ self.view.window.layer addAnimation:transition forKey:nil];
    }


-(IBAction)btn_combo_frm:(id)sender
{
    [self.view endEditing:YES];
    lbl_vw_header.text = @"Select Your Account";
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        
        gblclass.custom_alert_msg=@"Only One Account Exist.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        [hud hideAnimated:YES];
        return;
    }
    
    if (table_from.isHidden==YES)
    {
//        vw_bill.hidden=YES;
//        table_bill.hidden=YES;
//        vw_to.hidden=YES;
        vw_from.hidden=YES;
        vw_from.hidden=NO;
        table_from.hidden=NO;
//        table_to.hidden=YES;
//        vw_to.hidden=YES;
//        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        //vw_table.hidden=YES;
    }
    
}

-(IBAction)btn_close:(id)sender
{
    vw_from.hidden = YES;
    table_from.hidden = YES;
    
}

    -(IBAction)btn_back:(id)sender
    {
        [self slideLeft];
    //    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
    }

    -(IBAction)btn_Pay:(id)sender
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
        
        [self presentViewController:vc animated:NO completion:nil];
    }

    -(IBAction)btn_more:(id)sender
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
        vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
        [vc setTransitioningDelegate:transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
        
    }

    -(IBAction)btn_logout:(id)sender
    {
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                         message:Logout
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        
        [alert2 show];
    }

    - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
    {
        //NSLog(@"%ld",(long)buttonIndex);
        if (buttonIndex == 0)
        {
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 1)
        {
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        
    }

    -(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
    {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
    -(IBAction)btn_settings:(id)sender
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
        
        [self presentViewController:vc animated:NO completion:nil];
    }
    -(IBAction)btn_account:(id)sender
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        [self presentViewController:vc animated:NO completion:nil];
    }

    //- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
    //{
    //    [txtbillservice resignFirstResponder];;
    //    [_txtbilltype resignFirstResponder];;
    //    [_txtcompany resignFirstResponder];;
    //    [ _txtphonenumber resignFirstResponder];;
    //    [_txtnick resignFirstResponder];
    //}


    #define kOFFSET_FOR_KEYBOARD 65.0

    -(void)keyboardWillShow
    {
        // Animate the current view out of the way
        if (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
        else if (self.view.frame.origin.y < 0)
        {
            [self setViewMovedUp:NO];
        }
    }

    -(void)keyboardWillHide
    {
        if (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
        else if (self.view.frame.origin.y < 0)
        {
            [self setViewMovedUp:NO];
        }
    }

    -(void)textFieldDidBeginEditing:(UITextField *)sender
    {
        if ([sender isEqual:sender.text])
        {
            //move the main view, so that the keyboard does not hide it.
            if  (self.view.frame.origin.y >= 0)
            {
                [self setViewMovedUp:YES];
            }
        }
    }

    //method to move the view up/down whenever the keyboard is shown/dismissed
    -(void)setViewMovedUp:(BOOL)movedUp
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3]; // if you want to slide up the view
        
        CGRect rect = self.view.frame;
        
        //NSLog(@"%d",rect );
        
        if (movedUp)
        {
            // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
            // 2. increase the size of the view so that the area behind the keyboard is covered up.
            
            
            //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
            //NSLog(@"%f", rect.origin.y );
            //NSLog(@"%f",rect.size.height);
            
            rect.origin.y -= kOFFSET_FOR_KEYBOARD;
            rect.size.height += kOFFSET_FOR_KEYBOARD;
            
            
            //NSLog(@"%f", rect.origin.y );
            //NSLog(@"%f",rect.size.height);
            
            
        }
        else
        {
            // revert back to the normal state.
            rect.origin.y += kOFFSET_FOR_KEYBOARD;
            rect.size.height -= kOFFSET_FOR_KEYBOARD;
            [self.view endEditing:YES];
        }
        self.view.frame = rect;
        
        [UIView commitAnimations];
    }


    - (void)viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        // register for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           //  self.fundtextfield.delegate = self;
                           
                           
                           //         [mine showmyhud];
                           //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                           
                           //                       [self getFolionumber];
                       });
        
        
    }

    - (void)viewWillDisappear:(BOOL)animated
    {
        [super viewWillDisappear:animated];
        // unregister for keyboard notifications while not visible.
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillShowNotification
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
    }

    -(void)custom_alert :(NSString* )msg1 : (NSString*)icons
    {
        gblclass.custom_alert_msg=msg1;
        gblclass.custom_alert_img=icons;
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }


    -(void) mob_App_Logout:(NSString *)strIndustry
    {
        
        @try {
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                           ]];//baseURL];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
            
            NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                       [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                        [encrypt encrypt_Data:gblclass.Udid],
                                        [encrypt encrypt_Data:gblclass.M3sessionid],
                                        [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                        [encrypt encrypt_Data:@"abcd"],
                                        [encrypt encrypt_Data:gblclass.token], nil]
                                                                  forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                           @"strDeviceID",
                                                                           @"strSessionId",
                                                                           @"IP",
                                                                           @"hCode",
                                                                           @"Token", nil]];
            
            //NSLog(@"%@",dictparam);
            
            [manager.requestSerializer setTimeoutInterval:time_out];
            [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
             
                  success:^(NSURLSessionDataTask *task, id responseObject) {
                      //                  NSError *error;
                      
                      NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                      NSMutableArray* arr_display_trans;
                      
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:YES completion:nil];
                      
                      [hud hideAnimated:YES];
                      
                  }
             
                  failure:^(NSURLSessionDataTask *task, NSError *error) {
                      // [mine myfaildata];
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=@"Please try again later.";
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }];
            
        }
        @catch (NSException *exception)
        {
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=@"Please try again later.";
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
        }
        
    }



///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:130];
//    [self.view addSubview:hud];
   // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"review"])
        {
          //  [self payTezraftaarSubmit:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            //[self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
           // [self mob_App_Logout:@""];
        } else if ([chk_ssl isEqualToString:@"payAccountOverload"]) {
            chk_ssl=@"";
           // [self payAccount:@""];
        }
        else if ([chk_ssl isEqualToString:@"category"])
        {
            chk_ssl = @"";
          //  [self Get_Sub_Category:@""];
            
        } else if ([chk_ssl isEqualToString:@"GetTezRaftarList"]) {
            chk_ssl=@"";
            [self GetTezRaftarList:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"review"])
    {
        //[self payTezraftaarSubmit:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
        NSLog(@"%@",gblclass.arr_payee_list);
        
       // [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
       // [self mob_App_Logout:@""];
    } else if ([chk_ssl isEqualToString:@"payAccountOverload"])
    {
        chk_ssl=@"";
       // [self payAccount:@""];
    }
    else if ([chk_ssl isEqualToString:@"category"])
    {
        chk_ssl = @"";
      //  [self Get_Sub_Category:@""];
        
    } else if ([chk_ssl isEqualToString:@"GetTezRaftarList"]) {
        chk_ssl=@"";
        [self GetTezRaftarList:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}

- (void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void) GetTezRaftarList:(NSString *)strIndustry {

    @try {
        
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:@""], nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"date", nil]];

        //2020-08-07 00:00:00
        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetTezRaftarList" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      [hud hideAnimated:YES];
                      gblclass.arr_tezraftar_relation = [[NSMutableArray alloc] init];
                      arr_relation = [[NSMutableArray alloc] init];
                      gblclass.arr_tezrftar_branch = [[NSMutableArray alloc] init];
                      gblclass.arr_tezrftar_city = [[NSMutableArray alloc] init];
                      gblclass.arr_tezrftar_bank = [[NSMutableArray alloc] init];
                      
                      arr_pay_type = [[dic objectForKey:@"beneficiaryList"] objectForKey:@"BeneficiaryPayType"];
                      
                      arr_relation = [[dic objectForKey:@"beneficiaryList"] objectForKey:@"BeneficiaryRelation"];
                      
                      arr_city = [[dic objectForKey:@"beneficiaryList"] objectForKey:@"BeneficiaryCity"];
                      
                      arr_bank = [[dic objectForKey:@"beneficiaryList"] objectForKey:@"BeneficiaryBank"];
                      
                      arr_branch = [[dic objectForKey:@"beneficiaryList"] objectForKey:@"BeneficiaryBankBranch"];
                      
                      a = [[NSMutableArray alloc] init];
                      arr_act_type = [[NSMutableArray alloc] init];
                      split = [[NSArray alloc] init];
                      gblclass.arr_tezrftar_branch = [[NSMutableArray alloc] init];
                      gblclass.arr_tezrftar_city = [[NSMutableArray alloc] init];
                      gblclass.arr_tezrftar_bank = [[NSMutableArray alloc] init];
                      
                      
                      // For Pay type
                      for (dic in arr_pay_type)
                      {
                      
                      NSString* PAY_TYPE_CODE=[dic objectForKey:@"PAY_TYPE_CODE"]; //BeneAccountNo
                      
                      if (![PAY_TYPE_CODE isEqual:[NSNull null]])
                      {
                          [a addObject:PAY_TYPE_CODE];
                      }
                      else
                      {
                          [a addObject:@"N/A"];
                      }
                       
                          NSString* PAY_TYPE_DESC=[dic objectForKey:@"PAY_TYPE_DESC"]; //BeneAccountNo
                          
                          if (![PAY_TYPE_DESC isEqual:[NSNull null]])
                          {
                              [a addObject:PAY_TYPE_DESC];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [arr_act_type addObject:bbb];
                          
                          [a removeAllObjects];
                          
                    }
                      
                      // For Relation
                      
                      for (dic in arr_relation)
                      {
                          
                          NSString* LOV_CODE=[dic objectForKey:@"LOV_CODE"]; //BeneAccountNo
                          
                          if (![LOV_CODE isEqual:[NSNull null]])
                          {
                              [a addObject:LOV_CODE];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString* LOV_SHORT_DESC=[dic objectForKey:@"LOV_SHORT_DESC"]; //BeneAccountNo
                          
                          if (![LOV_SHORT_DESC isEqual:[NSNull null]])
                          {
                              [a addObject:LOV_SHORT_DESC];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [gblclass.arr_tezraftar_relation addObject:bbb];
                          
                          [a removeAllObjects];
                      }

                      
                      //For City
                      for (dic in arr_city)
                      {
                          
                          NSString* CITY_CODE=[dic objectForKey:@"CITY_CODE"];
                          if (![CITY_CODE isEqual:[NSNull null]])
                          {
                              [a addObject:CITY_CODE];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString* CITY_NAME=[dic objectForKey:@"CITY_NAME"];
                          if (![CITY_NAME isEqual:[NSNull null]])
                          {
                              [a addObject:CITY_NAME];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [gblclass.arr_tezrftar_city addObject:bbb];
                          
                          [a removeAllObjects];
                      }
                      
                      //For bank
                      for (dic in arr_bank)
                      {
                          
                          NSString* BANK_CODE=[dic objectForKey:@"BANK_CODE"];
                          if (![BANK_CODE isEqual:[NSNull null]])
                          {
                              [a addObject:BANK_CODE];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString* BANK_NAME=[dic objectForKey:@"BANK_NAME"];
                          if (![BANK_NAME isEqual:[NSNull null]])
                          {
                              [a addObject:BANK_NAME];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [gblclass.arr_tezrftar_bank addObject:bbb];
                          
                          [a removeAllObjects];
                      }
                      
                      //For branch
                      for (dic in arr_branch)
                      {
                          
                          NSString* BNK_BR_CODE=[dic objectForKey:@"BNK_BR_CODE"];
                          if (![BNK_BR_CODE isEqual:[NSNull null]])
                          {
                              [a addObject:BNK_BR_CODE];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString* BNK_BR_NAME=[dic objectForKey:@"BNK_BR_NAME"];
                          if (![BNK_BR_NAME isEqual:[NSNull null]])
                          {
                              [a addObject:BNK_BR_NAME];
                          }
                          else
                          {
                              [a addObject:@"N/A"];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [gblclass.arr_tezrftar_branch addObject:bbb];
                          
                          [a removeAllObjects];
                      }
                      
                      
                      NSLog(@"%@",gblclass.arr_tezrftar_branch);
                      NSLog(@"%@",gblclass.arr_tezrftar_city);
                      NSLog(@"%@",gblclass.arr_tezrftar_bank);
                      
                      NSLog(@"%@",arr_act_type);
                      [table reloadData];
                  }
                  else
                  {
                      gblclass.tz_accounts = [[NSArray alloc] init];
                      [self custom_alert:[NSString stringWithFormat:@"%@",[dic objectForKey:@"strReturnMessage"]] :@"0"];
                  }
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];

    }

}


@end
