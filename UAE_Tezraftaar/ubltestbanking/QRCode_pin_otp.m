//
//  QRCode_pin_otp.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 02/03/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "QRCode_pin_otp.h"
#import "GlobalClass.h"
#import "Globals.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "UIAlertView+DismissOnTimeout.h"
#import "Encrypt.h"


@interface QRCode_pin_otp ()
{
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    UIAlertController* alert;
    NSString* str_Otp;
    NSString* alert_chck;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pw;
    int txt_nam;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    NSString* logout_chck;
    UISwitch *mySwitch;
    NSString* chk_switch;
    NSString* str_chk;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* count_ssl;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation QRCode_pin_otp
@synthesize transitionController;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    
    
    chk_switch = [[NSUserDefaults standardUserDefaults] stringForKey:@"ui_switch"];
    
    
    
    //gblclass.Mobile_No
    
    count_ssl = @"0";
    ssl_count = @"0";
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    NSString *subStr ;//= [str substringWithRange:NSMakeRange(0, 20)];
    
    //NSLog(@"%@", gblclass.Mobile_No);
    alert_chck=@"";
    
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below.",subStr];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    NSLog(@"%@",gblclass.is_qr_payment);
    
    
    if ([gblclass.is_qr_payment isEqualToString:@"0"])
    {
        vw_switch.hidden=NO;
        
        
        if([chk_switch isEqualToString:@"1"])
        {
            NSLog(@"Switch is ON");
            lbl_switch.text=@"Turn my Instant QR Payment On";
            [myswitch setOn:YES];
            
        }
        else if([chk_switch isEqualToString:@"0"])
        {
            NSLog(@"Switch is OFF");
            [myswitch setOn:NO];
            lbl_switch.text=@"Turn my Instant QR Payment Off";
        }
        else
        {
            str_chk=@"0";
        }
    }
    else
    {
        vw_switch.hidden=YES;
    }
    
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
//                                    gblclass.story_board bundle:[NSBundle mainBundle]];
//        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:myController animated:YES completion:nil];
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(IBAction)btn_submit:(id)sender
{
    
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        
        [self custom_alert:@"Please enter 6 Digits OTP received via SMS" :@"0"];
        return ;
        
    }
    
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
    
    
    //NSLog(@"%@",gblclass.add_bill_type);
    
    //@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"submit";
        [self SSL_Call];
    }
    
}


-(void) Generate_QR_CODE_PIN:(NSString *)strIndustry
{
    
    @try
    {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:gblclass.is_default_acct_no],[encrypt encrypt_Data:str_Otp],[encrypt encrypt_Data:gblclass.qr_code_pin], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"Device_ID",@"Token",@"strBranchCode",@"strAccountNumber",@"strOTPPin",@"QRPin", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        //  generate_QRCODE_PIN
        [manager POST:@"GenerateQRCodePIN" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  
                  NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  count_ssl = @"0";
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [hud hideAnimated:YES];
                      
                      chk_ssl=@"logout";
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      NSLog(@"%@",gblclass.is_qr_payment);
                      if ([gblclass.is_qr_payment isEqualToString:@"0"])
                      {
                          if (vw_switch.isHidden==NO)
                          {
                              [self checkinternet];
                              if (netAvailable)
                              {
                                  chk_ssl=@"is_instant_pay";
                                  [self SSL_Call];
                              }
                          }
                          
                      }
                      
                      //                  CATransition *transition = [ CATransition animation];
                      //                  transition.duration = 0.3;
                      //                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      //                  transition.type = kCATransitionPush;
                      //                  transition.subtype = kCATransitionFromRight;
                      //                  [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      //                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                  vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
                      //                  [self presentViewController:vc animated:NO completion:nil];
                      
                      logout_chck=@"2";
                      gblclass.is_qr_payment=@"1";
                      
                      [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                  }
                  //              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"])
                  //              {
                  //
                  //                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //                  //
                  //                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  //
                  //                  //                      [alert addAction:ok];
                  //                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                  [hud hideAnimated:YES];
                  //
                  //                  logout_chck=@"1";
                  //                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  //
                  //                  return ;
                  //
                  //              }
                  
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  count_ssl = @"0";
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        count_ssl = @"0";
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later" :@"0"];
    }
    
}



-(void) clearOTP {
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
    
    [txt_1 becomeFirstResponder];
}


-(void) set_QR_Instant:(NSString *)strIndustry
{
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:str_chk], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userid",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strDeviceid",
                                                                       @"Token",
                                                                       @"isEnableQR", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        //  generate_QRCODE_PIN
        [manager POST:@"setQRInstant" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;
                  
                  NSDictionary*   dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [hud hideAnimated:YES];
                      
                      chk_ssl=@"logout";
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      
                      [self slide_right];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later" :@"0"];
    }
    
}







-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                       
                   });
    
    //**   [self performSelector:@selector(hideAlertView) withObject:nil afterDelay:50.40];
    
}


-(void)hideAlertView
{
    //[alert dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:alert completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 1)
    {
        
        if ([logout_chck isEqualToString:@"2"])
        {
            [self slide_right];
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else if([logout_chck isEqualToString:@"1"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
        }
        
        
        //[self mob_App_Logout:@""];
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 0)
    {
        
        if ([logout_chck isEqualToString:@"2"])
        {
            [self slide_right];
            
            logout_chck=@"";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
            
            [self presentViewController:vc animated:NO completion:nil];
        }
        else if([logout_chck isEqualToString:@"1"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                logout_chck=@"";
                chk_ssl=@"logout";
                [self SSL_Call];
            }
        }
        
        
    }
    else
    {
        
        
        
        // [self slide_right];
        
        //       [self dismissModalStack];
        
    }
}



///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"submit"])
        {
            chk_ssl=@"";
            [self Generate_QR_CODE_PIN:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"otp"])
        {
            chk_ssl=@"";
            [self Generate_OTP_For_Addition:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"is_instant_pay"])
        {
            chk_ssl=@"";
            [self set_QR_Instant:@""];
            
        }
        
        chk_ssl=@"";
    }
    else
    {
//        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//        [request setHTTPMethod:@"POST"];
//        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//        [self.connection start];
        
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
        
        
//        [NSTimer scheduledTimerWithTimeInterval:200.0
//                                         target:self
//                                       selector:@selector(targetMethod:)
//                                       userInfo:nil
//                                        repeats:NO];
    }
    
    
}

-(void)targetMethod:(NSTimer *)timer
{
    //do something
    
    [self clearOTP];

    [hud hideAnimated:YES];
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"submit"])
    {
        chk_ssl=@"";
        [self Generate_QR_CODE_PIN:@""];
       // [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
      //  [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"otp"])
    {
        chk_ssl=@"";
        [self Generate_OTP_For_Addition:@""];
      //  [self.connection cancel];
    }
    else if ([chk_ssl isEqualToString:@"is_instant_pay"])
    {
        chk_ssl=@"";
        [self set_QR_Instant:@""];
     //  [self.connection cancel];
    }
    
    chk_ssl=@"";
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        
        [self clearOTP];
        [self.connection cancel];
        
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger MAX_DIGITS = 1;
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            if (textField.tag == 6)
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                if ([count_ssl isEqualToString:@"0"])
                {
                    
                [self checkinternet];
                if (netAvailable)
                {
                    count_ssl = @"1";
                    [hud showAnimated:YES];
                     [self.view addSubview:hud];
                    chk_ssl=@"submit";
                    //[self SSL_Call];
                    [self Generate_QR_CODE_PIN:@""];
                }
                }
                
                return 0;
            }
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
    return YES;
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


-(IBAction)btn_resend_otp:(id)sender
{
    
    [hud showAnimated:YES];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"otp";
        [self SSL_Call];
    }
    
    // [self Generate_OTP_For_Addition:@""];
    
}

-(void) Generate_OTP_For_Addition:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:gblclass.is_default_acct_no],[encrypt encrypt_Data:@"QRCODEPIN"],[encrypt encrypt_Data:@"Generate OTP"],[encrypt encrypt_Data:@"2"],[encrypt encrypt_Data:@"QRCODEPIN"],[encrypt encrypt_Data:@"SMS"],[encrypt encrypt_Data:@"QRPIN"],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token],[encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"Device_ID",@"Token",@"M3key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
              NSDictionary*   dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  logout_chck=@"1";
                  
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  //   22 march 2017
                  //     gblclass.qr_code_pin=txt_qr_pin.text;
                  
                  //                  CATransition *transition = [ CATransition animation];
                  //                  transition.duration = 0.3;
                  //                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  //                  transition.type = kCATransitionPush;
                  //                  transition.subtype = kCATransitionFromRight;
                  //                  [self.view.window.layer addAnimation:transition forKey:nil];
                  //
                  //                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //                  vc = [storyboard instantiateViewControllerWithIdentifier:@"qrcode_pin_otp"];
                  //                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  //  [self showAlertwithcancel:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                  
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}


- (IBAction)changeSwitch:(id)sender
{
    
    [self switchToggled:sender];
    
    //    if([sender isOn])
    //    {
    //        NSLog(@"Switch is ON");
    //        gblclass.Chk_instant_qr_payment=@"on";
    //        lbl_switch.text=@"Turn my Instant QR Payment On";
    //    }
    //    else
    //    {
    //        NSLog(@"Switch is OFF");
    //        gblclass.Chk_instant_qr_payment=@"off";
    //        lbl_switch.text=@"Turn my Instant QR Payment Off";
    //    }
    
}


- (void) switchToggled:(id)sender
{
    mySwitch = (UISwitch *)sender;
    
    
    if ([mySwitch isOn])
    {
        str_chk=@"1";
        gblclass.Chk_instant_qr_payment=@"on";
        [mySwitch setOn:YES];
        lbl_switch.text=@"Turn my Instant QR Payment On";
        
    } else
    {
        str_chk=@"0";
        gblclass.Chk_instant_qr_payment=@"off";
        [mySwitch setOn:NO];
        lbl_switch.text=@"Turn my Instant QR Payment Off";
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:str_chk forKey:@"ui_switch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end

