//
//  Bill_Management_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 22/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Bill_Management_VC.h"
#import "TransitionDelegate.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"


@interface Bill_Management_VC ()
{
    NSMutableArray* arr_bill_names;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UIAlertView* alert;
    APIdleManager* timer_class;
    UIAlertController *alert_1;
}

@end

@implementation Bill_Management_VC
@synthesize transitionController;



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.transitionController = [[TransitionDelegate alloc] init];
    arr_bill_names=[[NSMutableArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    //   arr_bill_names=@[@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"];
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_bill_names count];    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    cell.textLabel.text = [arr_bill_names objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:12];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self checkinternet];
    if (netAvailable)
    {
        
        //  Yourstring=[arr_load_names objectAtIndex:indexPath.row];
        
        //NSLog(@"Selected item %ld ",(long)indexPath.row);
        //    UIStoryboard *mainStoryboard;
        //    UIViewController *vc;
        
        switch (indexPath.row)
        {
            case 0:
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"]; //utility_bill   //bill_all
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
            case 1:
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"OB_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 2:
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"UBP_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 3:
                
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            default: break;
        }
        
    }
    
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:Nil];
}
-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            //    [hud hideAnimated:YES];
            alert_1  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert_1 addAction:ok];
            [self presentViewController:alert_1 animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



@end

