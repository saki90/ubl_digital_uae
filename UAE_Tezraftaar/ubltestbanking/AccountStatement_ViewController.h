//
//  AccountStatement_ViewController.h
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SecondViewController;

@interface AccountStatement_ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
}


@property(strong,nonatomic)SecondViewController *secondViewController;


@end




 