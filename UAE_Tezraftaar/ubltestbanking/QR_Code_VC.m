//
//  QR_Code_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 21/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "QR_Code_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>


@interface QR_Code_VC ()<AVCaptureMetadataOutputObjectsDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    GlobalStaticClass* gblclass;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    
    NSString *detectionString;
    
    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code;
    
}
@end

@implementation QR_Code_VC
@synthesize transitionController;
@synthesize txt_acct_from;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    @try {
        
        
        gblclass=[GlobalStaticClass getInstance];
        detectionString = nil;
        
        self.transitionController = [[TransitionDelegate alloc] init];
        
        
        //    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
        //    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        //    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
        //    [_viewPreview.layer addSublayer:_videoPreviewLayer];
        
        gblclass.arr_qr_code=[[NSMutableArray alloc] init];
        
        _highlightView = [[UIView alloc] init];
        _highlightView.autoresizingMask =  UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        
        // _highlightView.autoresizingMask=uiviewautosizingf
        _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
        //  _highlightView.frame  = CGRectMake(0, 50, 320, 360);
        _highlightView.layer.borderWidth = 3;
        [self.view addSubview:_highlightView];
        
        
        
        _label = [[UILabel alloc] init];
        _label.frame = CGRectMake(0, self.view.bounds.size.height - 80, self.view.bounds.size.width, 80);
        _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = NSTextAlignmentCenter;
        // _label.text = @"(none)";
        
        // [self.view addSubview:_label];
        
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        CGFloat heights = 0.0;
        
        UIImageView *bg_img =[[UIImageView alloc] initWithFrame:CGRectMake(0,60,width,height-138)];
        //    bg_img.frame = CGRectMake(0, self.view.bounds.size.height - 80, self.view.bounds.size.width, 80);
        bg_img.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
        bg_img.image=[UIImage imageNamed:@"Cemra_g1.png"];
        [self.view addSubview:bg_img];
        
        
        _session = [[AVCaptureSession alloc] init];
        _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSError *error = nil;
        
        _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
        if (_input)
        {
            [_session addInput:_input];
        }
        else
        {
            //NSLog(@"Error: %@", error);
            
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention"
                                                            message: error.localizedDescription
                                                           delegate:nil                                             cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
            [alert1 show];
            
            return;
        }
        
        _output = [[AVCaptureMetadataOutput alloc] init];
        [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [_session addOutput:_output];
        
        _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
        
        _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
        //    _prevLayer.frame = self.view.bounds;
        
        
        
        _prevLayer.frame = CGRectMake(0, 60, 320, height);
        //_prevLayer.videoGravity=AVLayerVideoGravityResize;
        _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        [self.view.layer addSublayer:_prevLayer];
        
        [_session startRunning];
        
        [self.view bringSubviewToFront:_highlightView];
        [self.view bringSubviewToFront:bg_img];
        
        
//        [APIdleManager sharedInstance].onTimeout = ^(void){
//            //  [self.timeoutLabel  setText:@"YES"];
//            
//            
//            //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//            
//            storyboard = [UIStoryboard storyboardWithName:
//                          gblclass.story_board bundle:[NSBundle mainBundle]];
//            vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//            
//            
//            [self presentViewController:vc animated:YES completion:nil];
//            
//            
//            APIdleManager * cc1=[[APIdleManager alloc] init];
//            // cc.createTimer;
//            
//            [cc1 timme_invaletedd];
//            
//        };
        
        
    } @catch (NSException *exception) {
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention"
                                                        message: exception.reason
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert1 show];
        
        
    }
    
}

//-(IBAction)btn_start:(id)sender
//{
//    _highlightView = [[UIView alloc] init];
//    _highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
//    _highlightView.layer.borderColor = [UIColor greenColor].CGColor;
//    _highlightView.layer.borderWidth = 3;
//    _highlightView.hidden=NO;
//    [self.view addSubview:_highlightView];
//
//    _label = [[UILabel alloc] init];
//    _label.frame = CGRectMake(0, self.view.bounds.size.height - 40, self.view.bounds.size.width, 40);
//    _label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    _label.backgroundColor = [UIColor colorWithWhite:0.15 alpha:0.65];
//    _label.textColor = [UIColor whiteColor];
//    _label.textAlignment = NSTextAlignmentCenter;
//    _label.text = @"(none)";
//    [self.view addSubview:_label];
//
//    _session = [[AVCaptureSession alloc] init];
//    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    NSError *error = nil;
//
//    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
//    if (_input)
//    {
//        [_session addInput:_input];
//    }
//    else
//    {
//        //NSLog(@"Error: %@", error);
//    }
//
//    _output = [[AVCaptureMetadataOutput alloc] init];
//    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
//    [_session addOutput:_output];
//
//    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
//
//    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
////    _prevLayer.frame = self.view.bounds;
//    _prevLayer.frame = CGRectMake(0, 0, 380, 340);
//    _prevLayer.videoGravity=AVLayerVideoGravityResize;
//   // _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
//    [self.view.layer addSublayer:_prevLayer];
//
//    [_session startRunning];
//
//    [self.view bringSubviewToFront:_highlightView];
//    [self.view bringSubviewToFront:_label];
//
//}
//

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    
    //    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
    //            AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
    //            AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    
    NSArray *barCodeTypes = @[AVMetadataObjectTypeQRCode];
    
    for (AVMetadataObject *metadata in metadataObjects)
    {
        for (NSString *type in barCodeTypes)
        {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                
                highlightViewRect = barCodeObject.bounds;
                
                
                
                //NSLog(@"Bounds: %@", NSStringFromCGRect(barCodeObject.bounds));
                //NSLog(@"Frame: %@", barCodeObject.corners);
                
                //Update Laser
                //***       _highlightView.frame = highlightViewRect;
                
                
                detectionString = nil;
                
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                
                NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,7)]);
                
                if ([@"00" isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,2)]])
                {
                    [self Extract_Raw_Data_MasterPass];
                }
                else
                {
                    [self Extract_Raw_Data_QR];
                }
                //                else if ([@"#m3p2p#" isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,7)]])
                //                {
                //                    [self Extract_Raw_Data_QR];
                //                }
                
                break;
            }
        }
        
        if (detectionString != nil)
        {
            _label.text = detectionString;
            
            break;
        }
        // else
        //  _label.text = @"(none)";
    }
    
    //****  _highlightView.frame = highlightViewRect;
    
    // Stop video capture and make the capture session object nil.
    //     [_session stopRunning];
    //     _session = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    //     [_prevLayer removeFromSuperlayer];
    //     _highlightView.hidden=YES;
}


-(void)Extract_Raw_Data_MasterPass
{
    
    //    NSError* error = nil;
    
    @try {
        
        NSString* SEP_PAN = @"00";
        NSString* SEP_MOBILE=@"01";
        NSString* SEP_ACCOUNT= @"08";
        NSString* SEP_MERCHANT_NAME=@"0A";
        NSString* SEP_MERCHANT_CATEGORY_CODE= @"0B";
        NSString* SEP_MERCHANT_CITY=@"0C";
        NSString* SEP_MERCHANT_COUNTRY_CODE=@"0D";
        NSString* SEP_MERCHANT_CURRENCY_CODE=@"0E";
        
        
        //    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code;
        
        
        
        //       00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
        
        
        //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
        //     PAN   account               Mobile#        name     code        city   cuntry cod   curr code
        
        
        
        
        int bb=0;
        if ([_label.text length]>0)
        {
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,2)]);
            if ([SEP_PAN isEqualToString:[detectionString  substringWithRange:NSMakeRange(0,2)]])
            {
                //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(20,2)]); // For Mobile ide
                
                
                if ([SEP_ACCOUNT isEqualToString:[detectionString  substringWithRange:NSMakeRange(2,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(2,2)]); // FOr Account
                    
                    int calc;
                    int b = [[detectionString  substringWithRange:NSMakeRange(2,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    bb=2;
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    str_Accout_no=[detectionString  substringWithRange:NSMakeRange(bb,calc)]; // FOr Account
                    
                    bb=bb+calc;
                }
                
                if ([SEP_MOBILE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //                   NSNumber* num;
                    int calc;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // For Mobile
                    
                    bb=bb+2;
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                        
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);  // For Mobile
                    
                    str_Mobile=[detectionString  substringWithRange:NSMakeRange(bb,calc-1)];  // For Mobile
                    
                    
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    bb=bb+calc;
                    
                }
                
                
                if ([SEP_MERCHANT_NAME isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    bb=bb+2;
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,b)]); // FOr Name
                    
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    bb=bb+2;
                    str_merchant_name=[detectionString  substringWithRange:NSMakeRange(bb,b)]; // For Name
                    bb=bb+b;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                if ([SEP_MERCHANT_CATEGORY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);// FOr Category Code
                    //                    NSNumber* num;
                    int calc;
                    
                    bb=bb+2;
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    
                    bb=bb+2;
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    str_category_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                    
                    bb=bb+calc;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                }
                
                //00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
                
                if ([SEP_MERCHANT_CITY isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // City
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    //bb=bb+2;
                    int c = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    bb=bb+2;
                    str_city=[detectionString  substringWithRange:NSMakeRange(bb,c)]; // City
                    
                    bb=bb+c;
                }
                
                if ([SEP_MERCHANT_COUNTRY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Country Code
                    
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int c = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    bb=bb+2;
                    str_country_code=[detectionString  substringWithRange:NSMakeRange(bb,c)]; // Country Code
                    
                    bb=bb+c;
                }
                if ([SEP_MERCHANT_CURRENCY_CODE isEqualToString:[detectionString  substringWithRange:NSMakeRange(bb,2)]])
                {
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]); // Currency Code
                    
                    //                   NSNumber* num;
                    int calc;
                    bb=bb+2;
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,2)]);
                    
                    int b = [[detectionString  substringWithRange:NSMakeRange(bb,2)] intValue];
                    //NSLog(@"%d",b);
                    
                    
                    if ([[detectionString  substringWithRange:NSMakeRange(bb,2)] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
                    {
                        // newString consists only of the digits 0 through 9
                        calc=b*2;
                    }
                    else
                    {
                        //NSLog(@"Not Digit");
                    }
                    
                    //NSLog(@"%d",calc);
                    
                    //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(bb,calc)]);
                    bb=bb+2;
                    str_curr_code=[detectionString  substringWithRange:NSMakeRange(bb,calc)];
                }
                
                
                
                //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
                //       PAN   account               Mobile#        name     code        city   cuntry cod   curr code
                
            }
            
            //        NSString* SEP_PAN = @"00";
            //             NSString* SEP_MOBILE = @"01";
            //             NSString* SEP_ACCOUNT = @"08";
            //           //NSString* SEP_RFU = {"03","04","05","06","07","08","09"};
            //             NSString* SEP_MERCHANT_NAME = @"0A";
            //             NSString* SEP_MERCHANT_CATEGORY_CODE = @"0B";
            //             NSString* SEP_MERCHANT_CITY = @"0C";
            //             NSString* SEP_MERCHANT_COUNTRY_CODE = @"0D";
            //             NSString* SEP_MERCHANT_CURRENCY_CODE = @"0E";
            
            
            
            
            //NSLog(@"%@",str_Mobile);
            //NSLog(@"%@",str_Accout_no);
            //NSLog(@"%@",str_merchant_name);
            //NSLog(@"%@",str_category_code);
            //NSLog(@"%@",str_city);
            //NSLog(@"%@",str_country_code);
            //NSLog(@"%@",str_curr_code);
            
            
            [gblclass.arr_qr_code addObject:str_Mobile];
            [gblclass.arr_qr_code addObject:str_Accout_no];
            [gblclass.arr_qr_code addObject:str_merchant_name];
            [gblclass.arr_qr_code addObject:str_category_code];
            [gblclass.arr_qr_code addObject:str_city];
            [gblclass.arr_qr_code addObject:str_country_code];
            [gblclass.arr_qr_code addObject:str_curr_code];
            
            
            // Stop video capture and make the capture session object nil.
            [_session stopRunning];
            _session = nil;
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"QR_Code_Master"]; //QR_Code_Detail1_M QR_Code_Detail1
            [self presentViewController:vc animated:NO completion:nil];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
        // NSError* err;
        
        // [hud hideAnimated:YES];
        
        //         //NSLog(@"%@", [error localizedDescription]);
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Incorrect BarCode"
        //                                                       delegate:self
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil, nil];
        //
        //        [alert show];
    }
    
}



-(void)Extract_Raw_Data_QR
{
    
    //    NSError* error = nil;
    
    @try {
        
        NSString* SEP_PAN = @"00";
        NSString* SEP_MOBILE=@"01";
        NSString* SEP_ACCOUNT= @"08";
        NSString* SEP_MERCHANT_NAME=@"0A";
        NSString* SEP_MERCHANT_CATEGORY_CODE= @"0B";
        NSString* SEP_MERCHANT_CITY=@"0C";
        NSString* SEP_MERCHANT_COUNTRY_CODE=@"0D";
        NSString* SEP_MERCHANT_CURRENCY_CODE=@"0E";
        
        
        //    NSString* str_Mobile, * str_Accout_no,*str_merchant_name,*str_category_code,* str_city,*str_country_code,*str_curr_code;
        
        
        
        
        //       00 08 5295970000002910 01 06 033322583030 0A14 UBL_QR_TESTING 0B025999 0C07KARACHI 0D03PAK 0E025860 7D04
        
        
        //00 085184680420000020 0106031378849560 0A06Sultan 0B025999   0C03LHR   0D03PAK     0E025860  F20E
        //     PAN   account               Mobile#        name     code        city   cuntry cod   curr code
        
        NSArray* split;
        
        split=[[NSArray alloc] init];
        
        int bb=0;
        if ([detectionString length]>0)
        {
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            
            detectionString = [detectionString stringByReplacingOccurrencesOfString:@"#m3p2p#" withString:@""];
            detectionString = [detectionString stringByReplacingOccurrencesOfString:@"#m3p2p#" withString:@""];
            detectionString = [detectionString stringByReplacingOccurrencesOfString:@"|" withString:@""];
            
            
            split = [detectionString componentsSeparatedByString: @"|"];
            
            
            //NSLog(@"%@",[detectionString  substringWithRange:NSMakeRange(0,2)]);
            
        }
        
        
        gblclass.chck_qr_send=@"1";
        [gblclass.arr_qr_code addObject:[split objectAtIndex:0]];
        //       [gblclass.arr_qr_code addObject:[split objectAtIndex:0]];
        //       [gblclass.arr_qr_code addObject:[split objectAtIndex:1]];
        
        
        
        
        
        // Stop video capture and make the capture session object nil.
        [_session stopRunning];
        _session = nil;
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"send_money"]; //send_qr
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    @catch (NSException *exception)
    {
        
        // NSError* err;
        
        // [hud hideAnimated:YES];
        
        //         //NSLog(@"%@", [error localizedDescription]);
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Incorrect BarCode"
        //                                                       delegate:self
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil, nil];
        //
        //        [alert show];
    }
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    //    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
    [self dismissModalStack];
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];
    //
    //     [self presentViewController:vc animated:NO completion:nil];
}


-(void)dismissModalStack
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                    message:Logout
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

-(void)QR_Transaction:(NSString *)strIndustry
{
    
    //    //allCompany = [[NSMutableArray alloc] init];
    //    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
    //                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //
    //
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,@"1234",[GlobalStaticClass getPublicIp],gblclass.is_default_acct_id, nil] forKeys:
    //                               [NSArray arrayWithObjects:@"userID",@"strSessionId",@"IP",@"strAccessKey",@"payAnyOneFromAccountID",@"cmbPayFromSelectedItemText",@"cmbPayFromSelectedItemValue",@"payAnyOneFromBankImd",@"strTxtAmount",@"strTxtComments",@"strCCY",@"scanQRCodePan",@"merchantMobileNo",@"merchantTitle",@"strBankCode", nil]];
    //
    //
    //    //NSLog(@"%@",dictparam);
    //
    //    [manager.requestSerializer setTimeoutInterval:time_out];
    //
    //    [manager POST:@"QRTransaction" parameters:dictparam
    //
    //          success:^(NSURLSessionDataTask *task, id responseObject) {
    //              NSError *error;
    //              NSArray *arr = (NSArray *)responseObject;
    //              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
    //              NSMutableArray* arr_load_payee_list;
    //
    //              arr_load_payee_list=[[NSMutableArray alloc] init];
    //              arr_load_payee_list =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"PayeePayDetail"];
    //
    //              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
    //
    //              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
    //
    //              NSUInteger *set;
    ////              for (dic in arr_load_payee_list)
    ////              {
    ////                  set = [arr_load_payee_list indexOfObject:dic];
    ////
    ////                  branch_code=[dic objectForKey:@"BRANCH_CODE"];
    ////                  access_key=[dic objectForKey:@"TT_NAME"];
    ////                  bank_name=[dic objectForKey:@"BANK_NAME"];
    ////                  bank_imd=[dic objectForKey:@"BANK_IMD"];
    ////
    ////              }
    ////
    ////
    ////              [hud hideAnimated:YES];
    //              //
    //              //              if ([[dic objectForKey:@"Response"] integerValue]==0)
    //              //              {
    //              //                  alert  = [UIAlertController alertControllerWithTitle:@"Successfull" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
    //              //
    //              //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //              //                  [alert addAction:ok];
    //              //
    //              //                  [self presentViewController:alert animated:YES completion:nil];
    //              //
    //              //              }
    //              //              else
    //              //              {
    //              //                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
    //              //
    //              //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //              //                  [alert addAction:ok];
    //              //
    //              //                  [self presentViewController:alert animated:YES completion:nil];
    //              //              }
    //
    //          }
    //
    //          failure:^(NSURLSessionDataTask *task, NSError *error) {
    //              // [mine myfaildata];
    //              [hud hideAnimated:YES];
    //
    //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Request Time Out." preferredStyle:UIAlertControllerStyleAlert];
    //
    //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //              [alert addAction:ok];
    //              [self presentViewController:alert animated:YES completion:nil];
    //
    //          }];
    
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:YES completion:nil];
}




@end

