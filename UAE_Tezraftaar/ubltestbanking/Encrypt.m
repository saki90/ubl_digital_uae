//
//  Encrypt.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 21/09/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Encrypt.h"
#import <CommonCrypto/CommonCryptor.h>
#import <Foundation/Foundation.h>
#import "NSString+Encryption.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>

@implementation Encrypt


- (NSString *)encrypt_Data:(NSString*)data
{
    NSLog(@"%@",data);
    NSString *cypherString = [NSString encryptString:data withKey:@"SkDd9k88Ss7D9S9DSkDd9k88Ss7D9S9D"];
    return cypherString;
}

- (NSDictionary *)de_crypt_Data:(NSData*)data
{
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *cypherString = [NSString decryptData:response withKey:@"SkDd9k88Ss7D9S9DSkDd9k88Ss7D9S9D"];
    NSData *jsonData = [cypherString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    return json;
    
//    return cypherString;
}

- (NSString *)de_crypt_url:(NSString*)data
{
    NSString *response = [NSString stringWithFormat:@"%@",data];
    NSString *cypherString = [NSString decryptData:response withKey:@"SkDd9k88Ss7D9S9DSkDd9k88Ss7D9S9D"];
    return cypherString;
}

- (NSString *)encrypt_IP:(NSString*)IP
{
    NSString * str = [NSString stringWithFormat:@"1.1.1.1"];
    return str;
}



- (NSString *)externalIPAddress
{
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://icanhazip.com/"] encoding:NSUTF8StringEncoding error:nil];
    publicIP = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // IP comes with a newline for some reason
    return publicIP;
    
//    
//    // Check if we have an internet connection then try to get the External IP Address
////    if (![self connectedViaWiFi] && ![self connectedVia3G])
////    {
////        // Not connected to anything, return nil
////        return nil;
////    }
//    
//    // Get the external IP Address based on dynsns.org
//    NSError *error = nil;
//    NSString *theIpHtml = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.dyndns.org/cgi-bin/check_ip.cgi"]
//                                                   encoding:NSUTF8StringEncoding
//                                                      error:&error];
//    if (!error) {
//        NSUInteger  an_Integer;
//        NSArray *ipItemsArray;
//        NSString *externalIP;
//        NSScanner *theScanner;
//        NSString *text = nil;
//        
//        theScanner = [NSScanner scannerWithString:theIpHtml];
//        
//        while ([theScanner isAtEnd] == NO) {
//            
//            // find start of tag
//            [theScanner scanUpToString:@"<" intoString:NULL] ;
//            
//            // find end of tag
//            [theScanner scanUpToString:@">" intoString:&text] ;
//            
//            // replace the found tag with a space
//            //(you can filter multi-spaces out later if you wish)
//            theIpHtml = [theIpHtml stringByReplacingOccurrencesOfString:
//                         [ NSString stringWithFormat:@"%@>", text]
//                                                             withString:@" "] ;
//            ipItemsArray = [theIpHtml  componentsSeparatedByString:@" "];
//            an_Integer = [ipItemsArray indexOfObject:@"Address:"];
//            externalIP =[ipItemsArray objectAtIndex:++an_Integer];
//        }
//        // Check that you get something back
//        if (externalIP == nil || externalIP.length <= 0) {
//            // Error, no address found
//            return nil;
//        }
//        // Return External IP
//        return externalIP;
//    } else {
//        // Error, no address found
//        return nil;
//    }
}

@end
