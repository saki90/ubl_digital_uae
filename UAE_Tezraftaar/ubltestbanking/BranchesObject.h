//
//  BranchesObject.h
//  ubltestbanking
//
//  Created by Mehmood on 22/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BranchesObject : NSObject
{
    NSString* filePath;
    
    //the file contents
    NSString* city_name;
    NSString* branch_id;
    NSString* branch_name;
    NSString* latitude;
    NSString* longitude;
    NSString* Address;
    NSString* Contact_no;
    NSString* Fax_no;
    NSString* ATM;
    NSString* Ameen_window;
    NSString* Lockers_Available;
    NSString* branchType;
    
    
    
}
@property (nonatomic, retain) NSString* filePath;

@property (nonatomic, retain) NSString* city_name;
@property (nonatomic, retain) NSString* branch_id;
@property (nonatomic, retain) NSString* branch_name;
@property (nonatomic, retain) NSString* latitude;
@property (nonatomic, retain) NSString* longitude;
@property (nonatomic, retain) NSString* Address;
@property (nonatomic, retain) NSString* Contact_no;
@property (nonatomic, retain) NSString* Fax_no;
@property (nonatomic, retain) NSString* ATM;
@property (nonatomic, retain) NSString* Ameen_window;
@property (nonatomic, retain) NSString* Lockers_Available;
@property (nonatomic, retain) NSString* branchType;


-(BOOL)loadFile;


@end
