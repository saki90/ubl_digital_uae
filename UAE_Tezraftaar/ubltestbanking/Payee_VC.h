//
//  Payee_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 25/05/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Payee_VC : UIViewController
{
    IBOutlet UIScrollView* scrl;
    IBOutlet UILabel* lbl_balc;
    IBOutlet UILabel* lbl_name;
    IBOutlet UIView* vw_qr;
    IBOutlet UIView* vw_franchise_chck;
    IBOutlet UIButton* btn_pay_within_acct_enable;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong) IBOutlet UILabel* lbl_balance;
@property(nonatomic,strong) IBOutlet UILabel* lbl_acct_frm;

-(IBAction)btn_pay_withmy_acct:(id)sender;
- (IBAction)btn_fbrPayment:(id)sender;

@end

