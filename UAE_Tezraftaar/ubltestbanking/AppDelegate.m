//
//  AppDelegate.m
//  ubltestbanking
//
//  Created by ammar on 21/01/2016.
//  Copyright © 2016 ammar. All rights reserved.
//
// New Branch Commit


#define iosScreenSize [[UIScreen mainScreen] bounds].size
#define SERVICE_NAME @"find_ski"
#define GROUP_NAME @"TZ66KKE5SS.com.m3tech.ublnetbanking.enterprise22" //GROUP NAME should start with application identifier.

#define SERVICE_Key @"1234567890"

#import "AppDelegate.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"

#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "Login_New.h"
#import "Login_VC.h"
#import <Security/Security.h>
#import "Encrypt.h"
#import "Keychain.h"
//#import <GoogleMaps/GoogleMaps.h>
#import "Broken.h"
#import "ubltestbanking-Swift.h"
#import <LocalAuthentication/LocalAuthentication.h>
#include <sys/sysctl.h>
#include <sys/utsname.h>
//#import <FBSDKCoreKit/FBSDKCoreKit.h>
//saki #import <PeekabooConnect/PeekabooConnect.h>
//#import <Instabug/Instabug.h>
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
#import <FirebaseCore/FirebaseCore.h>

//@import GoogleMaps;

@interface AppDelegate ()
{
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSTimer *idleTimer;
    NSString* touch_id_enable;
    NSString *first_time_chk;
    NSString *UDID1;
    Encrypt* encrypt;
    Keychain * keychain;
    Broken* brk;
    NSString *dev_jb_chk;
    NSString* t_n_c_1;
    NSString* tch_enable_chck;
    NSTimer *timer;
    NSString* tch_chk;
    NSString *build_Version;
    NSString* login_chk;
    NSString* instabugToken;
    UIVisualEffectView *blurEffectView;
    
}
@end


@implementation AppDelegate

NSDictionary *globarVariableForAdditionalServices;
NSUserDefaults *userDefaults;
int currentTimesOfOpenApp = 0;

//BOOL isIphoneX (void)
-(void) isiPhoneX
{
    static BOOL isIphoneX = NO;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
       // NSLog(@"saki IsIphoneX");
        
#if TARGET_IPHONE_SIMULATOR
        NSString *model = NSProcessInfo.processInfo.environment[@"SIMULATOR_MODEL_IDENTIFIER"];
#else
        
        struct utsname systemInfo;
        uname(&systemInfo);
        
        NSString *model = [NSString stringWithCString:systemInfo.machine
                                             encoding:NSUTF8StringEncoding];
#endif
        isIphoneX = [model isEqualToString:@"iPhone10,3"] || [model isEqualToString:@"iPhone10,6"];
      //  NSLog(@"%@", model);
    });
    
    // return isIphoneX;
}

- (void) my_callback_function
{
    
   // NSLog(@"saqib jailbreak ixguard");
    // report_to_server(@"jailbroken");
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window.frame = [[UIScreen mainScreen] bounds];
    //    [UIPasteboard pasteboardWithName:@"UBLDigitalM3" create:YES];
    
//    NSLog(@"saki 25 sep");
//    NSLog(@"saki view app delegate load first");
    
    userDefaults  = NSUserDefaults.standardUserDefaults;
    globarVariableForAdditionalServices = @{@"" : @""};
    login_chk = @"";
    
    // Use Firebase library to configure APIs
//   saki 13 jan 2020 [FIRApp configure];
    
    //FaceBook Integration ::
//    [[FBSDKApplicationDelegate sharedInstance] application:application
//                             didFinishLaunchingWithOptions:launchOptions];
    
    //    // Fabric Integration ::
    //    [Fabric with:@[[Crashlytics class]]];
    
    
    
    //Initialize Peekabo SDK ::
    //saki 5 march 2020   initializePeekaboo();
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *peekabo_kfc = [defaults valueForKey:@"peekabo_kfc"];
    
    
    // [FBSDKAppEvents logEvent:@"My custom event saki"];
    
    
    // [GMSServices provideAPIKey:@"AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg"];
   // [GMSServices provideAPIKey:@"AIzaSyD1ANW-2ie4pCzMe-SyCT4TqQThCrlrtBA"];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    // Instabug
    //    [Instabug startWithToken:instabugToken invocationEvents: IBGInvocationEventShake | IBGInvocationEventScreenshot];
    
    build_Version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    //NSString *uuid = [[NSUUID UUID] UUIDString];
    
    encrypt = [[Encrypt alloc] init];
    
    gblclass = [[GlobalStaticClass getInstance] init];
    gblclass.selected_services_onbording = [[NSMutableArray alloc] init];
    gblclass.device_chck = @"0";
    gblclass.isDeviceJailBreaked = @"0";
    gblclass.chk_device_jb = @"0";
    gblclass.setting_ver = @"Ver : 16"; // App version
    
    if (peekabo_kfc.length == 0)
    {
        gblclass.peekabo_kfc_userid = @"";
    }
    else
    {
        gblclass.peekabo_kfc_userid = peekabo_kfc;
    }
    
    
    //** 8 may 2017
    [self jailBreak_Check];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    //27 march 2017
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"]; //first_time
    
    NSString* apply_acct_chck = [[NSUserDefaults standardUserDefaults] stringForKey:@"apply_acct"];
    
    
    //    NSUUID *myDevice = [NSUUID UUID];
    //    NSString *deviceUDID = myDevice.UUIDString;
    
    UIDevice *myDevice1 = [UIDevice currentDevice];
    // NSString *deviceUDID1 = [myDevice1.advertisingIdentifier ];
    
    //[self App_version];
    
    NSString* tyt = [[NSUserDefaults standardUserDefaults] stringForKey:@"buildVersion"];
    
//    NSLog(@"%@",tyt);
//    NSLog(@"%@",build_Version);
    
    gblclass.screen_size = [NSString stringWithFormat:@"%.0f",screenHeight];
    NSString* ud = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    UDID1 = [ud stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    
    
//    [userDefaults setObject:UDID1 forKey:@"device_id_uae"];
//    [userDefaults synchronize];
    
    
    // grab correct storyboard depending on screen height
    gblclass.story_board = [self grabStoryboard];
    
    // display storyboard
    //    self.window.rootViewController = [storyboard instantiateInitialViewController];
    //    [self.window makeKeyAndVisible];
    
    
    if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
    {
        //[self Guid_key_chain_method_make];
        [self Guid_key_chain_method_load];
        
        if ([tch_chk isEqualToString:@"2"] && [login_chk isEqualToString:@"1"])
        {
            UIStoryboard *mainStoryboard =[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            //            [UIStoryboard storyboardWithName:[NSString stringWithFormat:@"%@",[storyboard instantiateInitialViewController]] bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
            self.window.rootViewController = vc;
        }
        else if ([tch_chk isEqualToString:@"0"] && [login_chk isEqualToString:@"1"])
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
            self.window.rootViewController = vc;
        }
        else
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"]; //landing
            self.window.rootViewController = vc;
        }
        
    }
    else if([apply_acct_chck isEqualToString:@"0"])
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        self.window.rootViewController = vc;
    }
    else
    {
      //  NSLog(@"signup");
      //  NSLog(@"%@",gblclass.story_board);
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];//
        self.window.rootViewController = vc;
        
        //[self Guid_key_chain_method_load];
        [self Guid_delete_Item_Pressed];
        [self Guid_key_chain_method_make];
        [self Guid_key_chain_method_load];
    }
    
    
    //for payment check
    
    gblclass.payment_login_chck = @"1";
    
    //gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
    
    //  [self Guid_delete_Item_Pressed];
    //  [self Guid_key_chain_method_make];
    
    [self Guid_key_chain_method_load];
    
    //Cache removed ::
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
  //[self Load_credential];
    
    sleep(1);
    keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    
    
    [self Test_App];
//    [self Live_App];
    
    gblclass.is_iPhoneX = deviceName();
    
    
    //    if ([gblclass.is_iPhoneX isEqualToString:@"iPhone X"])
    //    {
    //        NSLog(@"Iphonex");
    //    }
    
    gblclass.signUp_through_cnic = @"";
    
    //    TEST
    //        gblclass.SSL_Certificate_name = @"der";
    //        gblclass.SSL_name = @"der";
    
    
    //   LIVE
    //    gblclass.SSL_Certificate_name = @"root-DERformat-0";
    //    gblclass.SSL_name = @"root-DERformat-0";
    //
    //    gblclass.SSL_type = @"cer";
    
    //    [self App_version];
    
  //  NSLog(@"saki view app delegate load end");
    
    return YES;
}


- (NSString*)grabStoryboard
{
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    switch (screenHeight) {
            
            // iPhone 4s
        case 480:
            gblclass.story_board = @"Main";//[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 5s
        case 568:
            gblclass.story_board = @"Main";//[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6
        case 667:
            gblclass.story_board = @"Main";//[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6 Plus
        case 736:
            gblclass.story_board = @"Main";//[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        case 812:
            gblclass.story_board = @"iPhone_X";//[UIStoryboard storyboardWithName:@"iPhone_X" bundle:nil];
            break;
            
        case 896:
            gblclass.story_board = @"iPhone_Xr";//[UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        default:
            // it's an iPad
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
    }
    
    //gblclass.story_board = @"Chat";
    return gblclass.story_board;
}

-(void)Live_App;
{
    //LIVE 7 AUG 2019
//    new 20 nov 2020
    //https://mobileapp.ubldirect.com/NetBankingWebServiceUAE/Service.asmx/GetBillData
    
    //old https://mobileapp.ubldirect.com/netbankingwebserviceUAE/service.asmx
    
    // 15 dec 2020
    //https://Mobileapp.ubldirect.com/NetBankingWebServiceUAEV1/Service.asmx
    

    
    //Current URL with Bills 2 march 2021
    NSData *value = [@"https://Mobileapp.ubldirect.com/NetBankingWebServiceUAEV1/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    
    
    //Old URL without Bills 2 march 2021
   //  NSData *value = [@"https://mobileapp.ubldirect.com/netbankingwebserviceUAE/service.asmx"dataUsingEncoding:NSUTF8StringEncoding];
     
    [self Add_item:value];
    [self find_item:value];
    
//    www_mobileapp_ubldirect_com
    //   LIVE   mobileapp_cert2022   root-DERformat-2021  certificate1
    gblclass.SSL_Certificate_name = @"mbapp";    //mbapp
   // gblclass.SSL_name = @"root-DERformat-0";   mbapp    root-DERformat-2021
    gblclass.SSL_name = @"new_certificate";       //root-DERformat-2021
    gblclass.SSL_type = @"cer";
    
    NSString *sadf = @"asdf";
  //NSLog(@"%@",sadf);
    
//    gblclass.SSL_Certificate_name = @"www_mobileapp_ubldirect_com";
//    gblclass.SSL_name = @"www_mobileapp_ubldirect_com";
//    gblclass.SSL_type = @"cer";
    
    //Instabug test token
    //instabugToken = @"307b9c48466cc711ea9d34518c26aa02";    
}

-(void)Test_App
{
   // NSLog(@"saki test app");
    // TEST Pak
    //   NSData *value = [@"https://ubltestapp.com/UBLNetBankingEncrypted/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
     
    //UAE Test
    NSData *value = [@"https://digitaltestapp.ubl.com.pk/UblNetBankingUAE/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    
    //Qatar Test
//     NSData *value = [@"https://digitaltestapp.ubl.com.pk/UblNetBankingQatar/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
     
    [self Add_item:value];
    [self find_item:value];
    
    //TEST
    gblclass.SSL_Certificate_name = @"ubl";
    gblclass.SSL_name = @"ubl";
    gblclass.SSL_type = @"cer";
     
    // Instabug test token
    // instabugToken = @"b5853976806d9047d3f01fbcae6f18e1";
}


-(void)App_version
{
   // NSLog(@"saki Appversion");
    [self getCredentialsForServer_touch:@"finger_print"];
   // NSLog(@"%@",tch_chk);
    
    NSString *App_Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *Build_Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* chck_app = [NSString stringWithFormat:@"%@%@",App_Version,Build_Version];
    
    //CFBundleVersion
    //CFBundleShortVersionString
    
    NSString *previousVersion = [userDefaults objectForKey:@"buildVersion"];
    
    if (!previousVersion)
    {
        login_chk = @"1";
        // first launch
        [userDefaults setObject:chck_app forKey:@"buildVersion"];
        [userDefaults synchronize];
    }
    else if ([previousVersion isEqualToString:chck_app])
    {
        // same version
        login_chk = @"0";
     //   NSLog(@"SAME VERSION.!");
    }
    else
    {
        // other version
        login_chk = @"1";
        [userDefaults setObject:chck_app forKey:@"buildVersion"];
        [userDefaults synchronize];
    }
}


NSString* deviceName()
{
    
   // NSLog(@"saki DEVICE NAME");
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName)
    {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound)
        {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound)
        {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound)
        {
            deviceName = @"iPhone";
        }
        else
        {
            deviceName = @"Unknown";
        }
        
    }
    
    return deviceName;
}

-(void)Add_item:(NSData*)URL
{
    NSString *key = FBEncryptorAES_String_Key;
    
    // TEST
    //  NSData *value = [@"https://ubltestapp.com/UBLNetBankingEncrypted/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    
    ////    //LIVE
    //        NSData *value = [@"https://mobileapp.ubldirect.com/NetBankingWebServiceDAOB/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    
    if([keychain insert:key :URL])
    {
       // NSLog(@"Successfully added data");
    }
    else
    {
        [self Update_item:URL];
//        NSLog(@"Failed to  add data");
    }
}

-(void)find_item:(NSData*)URL
{
    NSString *key =  FBEncryptorAES_String_Key;
    NSData * data = [keychain find:key];
    if(data == nil)
    {
       // NSLog(@"Keychain data not found");
    }
    else
    {
        gblclass.mainurl1 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        gblclass.ssl_pinning_url1 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
       // NSLog(@"Data is = %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
}

-(void)Update_item:(NSData*)URL
{
    
    NSString *key = FBEncryptorAES_String_Key;
    
    //   TEST
    //   NSData *value = [@"https://ubltestapp.com/UBLNetBankingEncrypted/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    
    ////    LIVE
    //      NSData *value = [@"https://mobileapp.ubldirect.com/NetBankingWebServiceDAOB/Service.asmx" dataUsingEncoding:NSUTF8StringEncoding];
    //
    
    if([keychain update:key :URL])
    {
        [self find_item:URL];
       // NSLog(@"Successfully updated data");
    }
    else
    {
        
    }
       // NSLog(@"Failed to add data");
}

-(void)Remove_item
{
    NSString *key = FBEncryptorAES_String_Key;
    
    if([keychain remove:key])
    {
      //  NSLog(@"Successfully removed data");
    }
    else
    {
     //   NSLog(@"Unable to remove data");
    }
}


//For Jail Break Check ::
-(void)jailBreak_Check
{
  //  NSLog(@"saki JAIL BREAK...");
    NSString *filePath = @"/Applications/Cydia.app";
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        // do something useful
    ///    NSLog(@"Cydia is Installed");
    }
    
    [userDefaults setValue:@"0"  forKey:@"isDeviceJailBreaked"];
    [userDefaults synchronize];
    
    NSArray *jail_break_options = [NSArray arrayWithObjects:@"/bin/bash",@"/etc/apt",@"/usr/sbin/sshd",@"/Library/MobileSubstrate/MobileSubstrate.dylib",@"/Applications/Cydia.app",@"/bin/sh",@"/var/cache/apt",@"/var/tmp/cydia.log", nil];
    
    for (int i =0; i < jail_break_options.count; i++)
    {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:jail_break_options[i]])
        {
          //  NSLog(@"Device is JailBreak");
            dev_jb_chk = [userDefaults valueForKey:@"isDeviceJailBreaked"];
            
            gblclass.chk_device_jb = @"1";
            
            if ([dev_jb_chk isEqualToString:@"1"] || [dev_jb_chk isEqualToString:@"0"] || dev_jb_chk == (id)[NSNull null] ||  [dev_jb_chk isEqual:[NSNull null]] ||  [dev_jb_chk length] == 0)
            {
                gblclass.isDeviceJailBreaked = @"1";  //JB check];
                [userDefaults setValue:@"1"  forKey:@"isDeviceJailBreaked"];
                [userDefaults synchronize];
            }
            
            NSString* t_n_c_1 = [userDefaults valueForKey:@"t&c"];
            
            if ([t_n_c_1 isEqualToString:@"1"] || [t_n_c_1 isEqualToString:@"0"] || [t_n_c_1 isEqualToString:@""]) {
                [userDefaults setValue:@"1" forKey:@"t&c"];
                [userDefaults synchronize];
            }
            
            break;
        }
    }
    
   // NSLog(@"%@",gblclass.isDeviceJailBreaked);
}


void html_css()
{
//    NSLog(@"***********  Jail break My fucntion SAKI Start  ***********");
//
//    NSLog(@"Jail break saqib 1 ..");
//    NSLog(@"Jail break saqib 2 ..");
//    NSLog(@"Jail saqib 1 ..");
//    NSLog(@"Jail saqib 2 ..");
//    NSLog(@"Jail saqib 3 ..");
//    NSLog(@"Jail saqib 4 ..");
//
//     NSLog(@"***********  Jail break My fucntion SAKI End  ***********");
    //report_to_server("jailbroken");
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // Bluring all testfields
    // Blur the preview
    
    //    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    //    UIView *blurView = [[UIVisualEffectView alloc] initWithEffect:effect];
    //    blurView.translatesAutoresizingMaskIntoConstraints = NO;
    //    [[UIWindow appearance] addSubview:blurView];
    //    [[[UITextField appearance] contentV] addSubview:blurView];
    //    [UIView appearance] inser
    
    //    // Make blur view fill screen
    //    {
    //        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[blurView]-0-|"
    //                                                                          options:0
    //                                                                          metrics:nil
    //                                                                            views:NSDictionaryOfVariableBindings(blurView)]];
    //        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[blurView]-0-|"
    //                                                                          options:0
    //                                                                          metrics:nil
    //                                                                            views:NSDictionaryOfVariableBindings(blurView)]];
    //    }
    //
    //    // Add a rounded of transparency to the blurView  **not working**
    //    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(50.f, 50.f, 50.f, 50.f)];
    //    mask.alpha = 1.0f;
    //    mask.backgroundColor = [UIColor redColor];
    //    [blurView setMaskView:mask]; // Remove this line, and everything is frosted.  Keeping this line and nothing is frosted.
    
    //
    //    UIView *mask = [[UIView alloc] initWithFrame:(CGRect){0,0,100,100}];
    //    mask.backgroundColor = [UIColor whiteColor];
    //    UIView *areaToReveal = [[UIView alloc] initWithFrame:(CGRect){20,20,50,50}];
    //    areaToReveal.backgroundColor = [UIColor whiteColor];
    //    [mask addSubview:areaToReveal];
    //   [[UITextField appearance] setMaskView:mask];
    
    
    //only apply the blur if the user hasn't disabled transparency effects
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        //            self.window.rootViewController.backgroundColor = [UIColor clearColor];
        
        self.window.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        blurEffectView.frame = self.window.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.window addSubview:blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
    }
    else
    {
        self.window.backgroundColor = [UIColor blackColor];
    }
    
    // Removing local pastBoard to get  copied data from generalPastBoard
    [UIPasteboard removePasteboardWithName:@"UBLDigitalM3"];
}

- (void) timerDidFire:(NSTimer *)timer {
    
  //  NSLog(@"saki TIMER_DID_FIRE");
    
    // This method might be called when the application is in the background.
    // Ensure you do not do anything that will trigger the GPU (e.g. animations)
    // See: http://developer.apple.com/library/ios/DOCUMENTATION/iPhone/Conceptual/iPhoneOSProgrammingGuide/ManagingYourApplicationsFlow/ManagingYourApplicationsFlow.html#//apple_ref/doc/uid/TP40007072-CH4-SW47
  //  NSLog(@"%i", gblclass.remainingTimeOnBording);
    gblclass.remainingTimeOnBording --;
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //    [timer invalidate];
    //    timer = nil;
    //    [NeedHelpVC createTimer:[GlobalStaticClass getInstance].remainingTimeOnBording];
    
    if ([[self.window subviews].lastObject isEqual:blurEffectView]) {
        [blurEffectView removeFromSuperview];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
//    [FBSDKAppEvents activateApp];
    
    // Creating new local pastBoard
    gblclass.pasteBoard = [UIPasteboard pasteboardWithName:@"UBLDigitalM3" create:YES];
    
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
   // NSLog(@"saki APPLICATION_WILL_TERMINATE_");
    
    //FaceBook Integration ::
    [self saveTimesOfOpenApp];
}


- (void)delete_Item_Pressed
{
  //  NSLog(@"saki DELETE ITEM PRESSED..");
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"main27";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
        OSStatus sts = SecItemDelete((__bridge CFDictionaryRef)keychainItem);
     //   NSLog(@"Error Code: %d", (int)sts);
        
        // saki 11 sep 2018[self key_chain_method_make];
        //saki 11 sep 2018 [self key_chain_method_load];
    }
    else
    {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Doesn't Exist.", nil)
        //                                                        message:NSLocalizedString(@"The item doesn't exist. It may have already been deleted.", nil)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
    }
}


// ******************* URL End ********************************













// ******************* GUID ********************************

-(void) Guid_key_chain_method_make
{
  //  NSLog(@"saki GUID_KEY)CHAIN_METHOD_MAKE");
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *GUID = [NSString stringWithFormat:@"%@",UDID1];
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
       // NSLog(@"Guid Already Exists");
        
        //   gblclass.Udid = GUID;
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Already Exists", nil)
        //                                                        message:NSLocalizedString(@"Please update it instead.",)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
    else
    {
        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        
      //  NSLog(@"Guid create");
        keychainItem[(__bridge id)kSecValueData] = [GUID dataUsingEncoding:NSUTF8StringEncoding]; //Our password
        
        OSStatus sts = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
      //  NSLog(@"Error Code: %d", (int)sts);
    }
}


-(void) Guid_key_chain_method_load
{
   // NSLog(@"saki GUID_CHAIN_METHOD_LOAD");
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFDictionaryRef result = nil;
    
    OSStatus sts = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    
//    NSLog(@"Error Code: %d", (int)sts);
//    NSLog(@"%d",noErr);
    
    if(sts == noErr)
    {
     //   NSLog(@"guid load");
        NSDictionary *resultDict = (NSDictionary *)result;
        NSData *pswd = resultDict[(__bridge id)kSecValueData];
        NSString *guid = [[NSString alloc] initWithData:pswd encoding:NSUTF8StringEncoding];
        
        if ( [gblclass.Udid length]==0)
        {
            gblclass.Udid=[NSString stringWithFormat:@"%@",guid];
        }
        
    }
    else
    {
      //  NSLog(@"Guid Not found, Old load guid");
        [self Guid_key_chain_method_make];
        
        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Doesn't Exist", nil)
        //                                                        message:NSLocalizedString(@"No keychain item found for this user.", )
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
}



- (void) Guid_delete_Item_Pressed
{
  //  NSLog(@"saki GUID_DELETE_ITEM_PRESSED");
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //    @"<a class="vglnk" href="http://www.myawesomeservice.com" rel="nofollow"><span>http</span><span>://</span><span>www</span><span>.</span><span>myawesomeservice</span><span>.</span><span>com</span></a>";
    
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
        OSStatus sts = SecItemDelete((__bridge CFDictionaryRef)keychainItem);
     //   NSLog(@"Error Code: %d", (int)sts);
        
    }
    else
    {
        
    }
}


// ******************* GUID End ********************************




-(void)saveTimesOfOpenApp {
    
  //  NSLog(@"saki SAVE_TIME_OF_OPEN_APP");
    //[userDefaults setObject:currentTimesOfOpenApp forKey:@"timesOfOpenApp"];
    [userDefaults setInteger:currentTimesOfOpenApp forKey:@"timesOfOpenApp"];
    
}

-(int)getCurrentTimesOfOpenApp {
    
   // NSLog(@"saki GET_CURRENT_TIME_OF_OPEN_APP");
    return [userDefaults integerForKey:@"timesOfOpenApp"];
}


-(void)writeToPlist : (NSDictionary*) data  plistName : (NSString*) plistName  {
    
    
  //  NSLog(@"saki WRITE_TO_PLIST.");
    NSDictionary *dictionary = data;
    NSFileManager *documentDirectory =  NSFileManager.defaultManager;
    NSURL *fileUrl =  [documentDirectory URLsForDirectory:documentDirectory inDomains:NSUserDomainMask].lastObject;
    
    if ([NSKeyedArchiver archiveRootObject:dictionary toFile:fileUrl.path]) {
        
      //  NSLog(@"true");
    }
    
    NSString *loadedDic =  [NSKeyedUnarchiver unarchiveObjectWithFile:fileUrl.path];
   // NSLog(@"%@", loadedDic);
}

-(NSDictionary*)readingFromPlist : (NSString*) plistName {
    
    
  //  NSLog(@"saki READING_FROM)PLIST.");
    //NSDictionary *dictionary = data;
    NSFileManager *documentDirectory =  NSFileManager.defaultManager;
    NSURL *fileUrl =  [documentDirectory URLsForDirectory:documentDirectory inDomains:NSUserDomainMask].lastObject;
    
    NSDictionary *array;
    NSDictionary *loadedDic = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithFile:fileUrl.path];
    array = loadedDic;
    
    if (array != nil) {
        
        return @{@"NotFound" : @0};
    }
    
    return array;
    
}

-(void)appLaunchStatus {
    
    
   // NSLog(@"saki APP_LAUNCH_STATUS");
    
    NSDictionary* flag = [self readingFromPlist:@"AppLaunchStatus.plist"];
    NSDictionary* continueApplication = [self readingFromPlist:@"ContinueApplication.plist"];
  //  NSLog(@"%@", flag);
    
    if (![flag  isEqual: @{@"NotFound" : @0}]) {
        
        int isNetBankingOn = flag[@"isNetBankingOn"];
        int isSignUpCompleted = flag[@"isSignUpCompleted"];
        int hasAccount = flag[@"hasAccount"];
        
        if (isNetBankingOn == 0 && isSignUpCompleted == 1 && hasAccount == 1)
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"TouchIdSignIn4ViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
        }
        else if(isNetBankingOn == 0 && isSignUpCompleted == 0 && hasAccount == 0){
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"LetsGetStartedViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
        }
        else if(isNetBankingOn == 1 && isSignUpCompleted == 1 && hasAccount == 1)
        {
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
            UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:@"TouchIdSignIn3ViewController"];
            self.window.rootViewController = controller;
            [self.window makeKeyAndVisible];
            
        }
    }
    
    else
    {
      //  NSLog(@"flag Not Found");
    }
}


-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try
    {
       // NSLog(@"saki touch CREDENTIALS");
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
        
        //        // Create dictionary of search parameters
        //        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        //
        //        // Look up server in the keychain
        //        NSDictionary* found = nil;
        //        CFDictionaryRef foundCF;
        //        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        //
        //        // Check if found
        //        found = (__bridge NSDictionary*)(foundCF);
        //        if (!found)
        //            return;
        //
        //        // Found
        //        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        //
        //        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        //
        //        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        //
        //        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
    }
    @catch (NSException *exception)
    {
        // [hud hideAnimated:YES];
        // [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        return ;
    }
}


//Facebook Integration ::
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
//
//    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                                  openURL:url
//                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
//];
// Add any custom logic here.
//return handled;
//}

@end

