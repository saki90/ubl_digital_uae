//
//  TermsAndConditionsViewController.m
//  ubltestbanking
//
//  Created by ammar on 24/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "TermsAndConditionsViewController.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "Globals.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "APIdleManager.h"
#import "Encrypt.h"

@interface TermsAndConditionsViewController ()
{
    
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    Encrypt *encrypt;
    NSString* ssl_count;
    MBProgressHUD *hud;
    NSString* chk_ssl;
    UIAlertController* alert;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation TermsAndConditionsViewController

@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.transitionController = [[TransitionDelegate alloc] init];
    gblclass.chk_term_condition=@"1";
    
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    ssl_count = @"0";
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
    }
    
    NSString* txt = @"<html xmlns:v=\"urn:schemas-microsoft-com:vml\"\nxmlns:o=\"urn:schemas-microsoft-com:office:office\"\nxmlns:w=\"urn:schemas-microsoft-com:office:word\"\nxmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"\nxmlns=\"http://www.w3.org/TR/REC-html40\">\n\n<head>\n</head>\n\n<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in'>\n\n<div class=WordSection1>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL\nDigital App Terms and Conditions <o:p></o:p></span></b></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><v:shapetype\n id=\"_x0000_t5\" coordsize=\"21600,21600\" o:spt=\"5\" adj=\"10800\" path=\"m@0,l,21600r21600,xe\">\n <v:stroke joinstyle=\"miter\"/>\n <v:formulas>\n  <v:f eqn=\"val #0\"/>\n  <v:f eqn=\"prod #0 1 2\"/>\n  <v:f eqn=\"sum @1 10800 0\"/>\n </v:formulas>\n <v:path gradientshapeok=\"t\" o:connecttype=\"custom\" o:connectlocs=\"@0,0;@1,10800;0,21600;10800,21600;21600,21600;@2,10800\"\n  textboxrect=\"0,10800,10800,18000;5400,10800,16200,18000;10800,10800,21600,18000;0,7200,7200,21600;7200,7200,14400,21600;14400,7200,21600,21600\"/>\n <v:handles>\n  <v:h position=\"#0,topLeft\" xrange=\"0,21600\"/>\n </v:handles>\n</v:shapetype>\n<b style='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'><br>\nDefinitions <o:p></o:p></span></b></p>\n\n<p style='text-align:justify'><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>In this document the\nfollowing words and phrases shall have the meanings as set below unless the\ncontext indicates otherwise:<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Account (s)&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> refers to the Customer&#39;s bank account and/ or\ncredit card account and/ or home loan account and/or <span class=SpellE>Cashline</span>\naccount and/ or Omni Account and/or auto loan account and/ or consumer durable\nloan account and/or any other type of account (each an &quot;Account&quot; and\ncollectively &quot;Accounts&quot;, so maintained with United Bank Ltd <b\nstyle='mso-bidi-font-weight:normal'>(&quot;UBL&quot;)</b> which are eligible\nAccount(s) for operations through the use of Digital App.<br style='mso-special-character:\nline-break'>\n<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>\n<![endif]><o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Account Information&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> means information pertaining to the\nAccount(s) maintained by the Customer with the Bank.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Affiliate&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> means the UBL business partners and vendors.<o:p></o:p></span></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Alerts&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> means the Account Information provided by UBL\nto the Customer through the Customer&#39;s email address or mobile phone (based on\nSMS) generated and sent to the Customer by UBL at the specific request of the\nCustomer which request shall be made by the Customer using the Digital App\nServices of UBL.<o:p></o:p></span></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Bank&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> and <b style='mso-bidi-font-weight:normal'>&quot;UBL&quot;</b>\nrefer to United Bank Limited, which are to be used interchangeably.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Customer&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> refers to the UBL Account holders authorized\nto use Digital App. In case of the Customer being a minor, the legal guardian\nof such minor may be permitted to use Digital App subject to necessary\napprovals and conditions.<o:p></o:p></span></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>In this document all\nreferences to the Customer being referred in masculine gender shall be deemed\nto include the feminine gender.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Digital App&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> refers to the Digital App service offered by\nUBL to the Customers, as defined below.<br style='mso-special-character:line-break'>\n<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>\n<![endif]><o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Digital App Service&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> is one of the fastest and the most convenient\nway to access UBL Accounts on the Mobile phones. Customers can view balances,\ntransfer funds, pay bills online and other services as UBL may decide to\nprovide from time to time. The availability/non-availability of a particular\nservice shall be at the sole discretion of UBL.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Payment Instruction/s&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> shall mean any instruction given by the\nCustomer to debit funds via UBL Digital App. These may include but not limited\nto bill payments, purchasing vouchers or mobile top-ups, transferring funds\nfrom the Account held by the Customer to accounts held by other approved\nCustomers with UBL or other banks. The Bank may in its sole and exclusive\ndiscretion confine this facility only to certain permitted Customers or may\nextend it from time to time to be generally available to all Customers.<o:p></o:p></span></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Personal Information&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> refers to the information provided by the\nCustomer to UBL.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;SMS&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> means short message service which includes\nthe storage, routing and delivery of alphanumeric messages over GSM or other\ntelecommunication systems.<o:p></o:p></span></p>\n\n<p style='margin:0in;margin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>&quot;Terms&quot;</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'> refer to terms and conditions herein for use\nof the Digital App Service.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></b></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></b></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Applicability of Terms<o:p></o:p></b></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>These Terms form the\ncontract between the Customer and UBL for Digital App. By registering for Digital\nApp for the first time, the Customer acknowledges and accepts these Terms.\nThese terms and conditions are in addition to those which are agreed by the\nCustomer along with the account opening form.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Digital App Service<span\nstyle='mso-no-proof:yes'> </span></b><o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Digital App can\nonly be used by registered device that has WIFI or GPRS service enabled.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p>&nbsp;</o:p></span></b></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Digital\nApp Accessibility <o:p></o:p></span></b></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer\nirrevocably and unconditionally undertakes to ensure that the password is kept\nstrictly confidential and to not let any person have access to the mobile\ndevice while the Customer is accessing the Digital App.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer agrees\nand acknowledges that UBL shall in no way be held responsible or liable if the\nCustomer incurs any loss, expenses, costs, damages, claims (including claims\nfrom any third party) as a result of use of the Digital App log by the Customer\nnor shall UBL be liable for information being disclosed by UBL regarding his\nAccount(s) as provided and required to be disclosed by law or disclosed to any\nparty with the Customer&#39;s consent pursuant to the access of the Digital App and\nthe Customer shall fully indemnify and hold UBL harmless in respect of the\nabove.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall\nmaintain the secrecy of all information of confidential nature and shall ensure\nthat the same is not disclosed to any person voluntarily, accidentally or by\nmistake.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><![endif]><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>The Customer accepts and undertakes that it is\nthe Customer&#39;s responsibility to ensure that the security of the Customer&#39;s\nmobile device and details of beneficiaries or transactions or any other\nrelevant information registered by the Customer are not compromised. The\nCustomer undertakes not to hold UBL liable or responsible for any loss or\ndamage that may be suffered by the Customer due to transactions through the Digital\nApp and shall keep UBL indemnified against all such losses or damages.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify'><b style='mso-bidi-font-weight:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>Unauthorized Access<span style='mso-no-proof:\nyes'> </span><o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall\ntake all necessary precautions to prevent unauthorized and illegal use of Digital\nApp and unauthorized access to his Accounts accessed through Digital App by\npreventing the sharing of information or password.<o:p></o:p></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer will be\nrequired to change his password on a frequent basis. The Customer understands\nand acknowledges that UBL shall not be responsible for any consequences arising\nout of download of the Digital App application by the Customer from any third\nparty application store which is not published by UBL.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Maintenance of Sufficient Balance<span\nstyle='mso-no-proof:yes'> </span><o:p></o:p></b></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall\nensure that there are sufficient funds at all material times in the Account for\ntransactions to take effect through the Digital App, and UBL shall not be\nliable for any consequences arising out of the Customer&#39;s failure to ensure\nadequacy of funds.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Funds Transfer through Digital App</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall use its best\nefforts to effect funds transfer transaction received through Digital App\nsubject to availability of sufficient funds in the Account and subject to any\nlaws of Pakistan, in force at the time.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall specify and\nupdate from time to time the limit for carrying out the various kinds of funds\ntransfer or bill payment facilities available to the Customer through Digital\nApp. The said facility/<span class=SpellE>ies</span> will be provided in\naccordance with the arrangement between UBL and the Customers and as per\nconditions specified by UBL from time to time. UBL shall not be liable for any\nomission to make all or any of the payments or for late payments for whatsoever\ncause howsoever arising.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Accuracy of Information</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer is\nresponsible for the correctness and accuracy of any information supplied to UBL\nby the Customer for use of Digital App. UBL accepts no liability for any\nconsequences whether arising out of erroneous information or the accuracy,\nreliability or completeness of information supplied by the Customer or\notherwise.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>If the Customer\nnotices an error in the information supplied to UBL either in the registration\nform or any other communication, he shall immediately imitate to UBL in writing<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Instructions</b><br style='mso-special-character:\nline-break'>\n<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>\n<![endif]><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>All instructions for\noperating Digital App shall be given through the Internet by the Customer in\nthe manner indicated by UBL.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall be\nsolely responsible for the accuracy and authenticity of the Payment Instruction\nprovided to UBL and/or the Affiliates and the same shall be considered to be\nsufficient to operate / use the Digital App.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall not be\nrequired to independently verify the Payment Instruction; an instruction is\neffective unless countermanded by further instructions. UBL shall have no\nliability if it does not or is unable to stop or prevent the implementation of\nany instruction.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>Where UBL considers\nthe instructions to be inconsistent or contradictory it may seek clarification\nfrom the Customer before acting on any instruction of the Customer or act upon\nany such instruction as it deems fit.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL may refuse to\ncomply with the instructions without assigning any reason and shall not be\nunder any duty to assess the prudence or otherwise of any instruction and have\nthe right to suspend the operations through the Digital App if it has reason to\nbelieve that the Customer&#39;s instructions will lead or expose to direct or\nindirect loss or may require an indemnity from the Customer before continuing\nto operate / use the Digital App.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>In pursuit to comply\nwith applicable laws and regulations UBL may intercept and investigate any\npayment messages and other information or communications sent to or by the\nCustomer or on the Customer&#39;s behalf through other bank. This process may\ninvolve making future inquiries.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall be\nfree to give Payment Instruction for transfer of funds for such purpose as he/she\nshall deem fit. The Customer however agrees not to use or permit the Payment\nInstruction or any related services for any illegal or improper purposes.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>For\nthis, the Customer ensures that <o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>He has full right,\npower and/or authority to access and avail the services obtained and the goods\npurchased and shall observe and comply with all applicable laws and regulations\nin each jurisdiction in applicable territories.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>He shall provide UBL\nsuch information and/or assistance as is required by UBL for the performance of\nthe services and/or any other obligations of UBL under these Terms.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>He shall not at any\ntime provide to any person, with any details of the accounts held by him with\nUBL including, the passwords, account number, card numbers and PIN which may be\nassigned to me by UBL from time to time.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>He shall have access\nto and use the Digital App from a secure mobile device and that any compromise\nof the Customer Account resulting from an attempt to access it from an unsecure\n/ compromised device, will be the sole responsibility of the Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Risks</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span style='font-size:9.0pt;\nfont-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer hereby acknowledges that he utilizes Digital App\nServices at his own risk. These risks may include, but not limited to, the\nfollowing:<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer acknowledges that in case any third person obtains access to the Account,\nrelevant information and the registered device, such third person would be able\nto instruct fund transfers and provide Payment Instructions.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Internet is\nsusceptible to a number of frauds, misuse, hacking and other actions that could\naffect Payment Instructions to UBL. Whilst UBL shall aim to provide security to\nprevent the same, there cannot be any guarantee from such Internet frauds,\nhacking and other actions that could affect Payment Instructions to UBL. The\nCustomer separately indemnifies UBL against all risks arising out of the same.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The transfer of funds\nto third party accounts shall require proper, accurate and complete details.\nThe Customer shall be required to fill in the account number of the person to\nwhom the funds are to be transferred. In the event of any inaccuracy in this\nregard, the funds may be transferred to incorrect accounts. In such an event\nUBL shall not be liable for any loss or damage caused.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The transaction(s) for\ntransfer of funds as per Customer&#39;s instruction may not be completed for some\nreasons. In such cases, the Customer shall not hold UBL responsible in any\nmanner whatsoever in the said transaction(s) and contracts and the Customer&#39;s\nsole recourse in this regard shall be with the beneficiary of the transaction.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The technology for\nenabling the transfer of funds and other services offered by UBL could be\naffected by virus or other malicious, destructive or corrupting code, program\nor macro. It may also be possible that the system may require maintenance and\nduring such time it may not be able to process the request of the Customer.\nThis could result in delays in the processing of instructions or failure in the\nprocessing of instructions and other such failures and inability.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer\nunderstands that UBL disclaims all and any liability, whether direct or\nindirect, whether arising out of loss of profit or otherwise arising out of any\nfailure or inability by UBL to honor any Customer instruction for whatsoever\nreason. The Customer understands and accepts that UBL shall not be responsible\nfor any of the aforesaid risks and UBL shall disclaim all liability in respect\nof the said risks.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Authority to UBL for Digital App <o:p></o:p></b></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer\nirrevocably and unconditionally authorizes UBL to access all his Account(s) for\neffecting banking or other transactions performed by the Customer through the Digital\nApp. The right to access shall also include the right at UBL&#39;s sole discretion\nto consolidate or merge any or all accounts of the Customer with UBL and the\nright to set off any amounts owing to UBL without prior notice.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;</span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The instructions of\nthe Customer shall be effected only after authentication of the Customer in\naccordance with the prescribed procedure for Digital App by UBL or any one authorized\nby UBL. . UBL shall have no obligation to verify the authenticity of any\ntransaction received from the User other than by Login/Password.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The email\nconfirmation, if any, that is received by the Customer at the time of operation\nof the Digital App by the Customer shall be accepted as conclusive and binding\nfor all purposes.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>All the records of UBL\ngenerated by the transactions arising out of the use of the Digital App,\nincluding the time the transaction recorded shall be conclusive proof of the\ngenuineness and accuracy of the transaction.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>While UBL and the\nAffiliates shall endeavor to carry out the instructions promptly, they shall\nnot be responsible for any delay in carrying on the instructions due to any\nreason whatsoever, including due to failure of operational systems or any\nrequirement of law.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Liability of the Customer<o:p></o:p></b></span></p>\n\n<p style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:.3in;\nmargin-bottom:.0001pt;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>Neither UBL nor the\nAffiliates shall be liable for any unauthorized transactions occurring through Digital\nApp and the Customer hereby fully indemnifies and holds UBL and its Affiliates\nharmless against any action, suit, proceeding initiated against it or any loss,\ncosts or damages incurred by the Customer as a result thereof.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall under no\ncircumstance be held liable to the Customer if the Digital App is not available\nin the desired manner for reasons including but not limited to natural\ncalamities, legal restraints, faults in the telecommunication network or\nnetwork failure, or any other reason beyond the control of UBL. Under no\ncircumstances shall UBL be liable for any damages whatsoever, whether such\ndamages are direct, indirect, incidental consequential and irrespective of\nwhether any claim is based on loss of revenue, interruption of business or any\nloss of any character or nature whatsoever and whether sustained by the\nCustomer or by any other person.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The customer is liable\nto keep his phone and access to the app secure and therefore do not jailbreak\nor root his phone, which is the process of removing software restrictions and\nlimitations imposed by the official operating system of the device. Such\nactivity can make the phone vulnerable to malware/viruses/malicious programs,\ncompromise your phone&#39s security features and it could mean that the UBL\nDigital App won&#39t work properly or at all. Illegal or improper use of the\nDigital App shall render the Customer liable for payment of financial charges\nas decided by UBL at its sole discretion and/or will result in suspension of\nthe Customer&#39;s operations through Digital App.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer\nundertakes to comply with all applicable laws and regulations governing the\naccount of the Customer. For the avoidance of doubt, the governing law is the\nsubstantive and procedural laws of the Islamic Republic of Pakistan.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer may use Digital\nApp to direct UBL to provide the Customer with Account Information through\nEmail or SMS from time to time to be sent directly to his registered email\naddress or mobile number (which will be identified in writing prior to any\ndirections to UBL by the Customer). The format and extent of information to be\nprovided by way of Alerts to the Customer shall be determined by UBL. The\nfrequency of the Alerts to be provided to the Customer will be determined by\nthe Customer in accordance with the options provided by UBL. UBL shall under no\ncircumstances whatsoever, be held responsible or liable by the Customer for any\ndelay in delivery of the Alerts nor the inaccuracy of the information contained\nin such Alerts sent to the Customer by UBL. UBL shall not be liable for the\nincorrect delivery of, nor for its inability to deliver the Alerts altogether.<o:p></o:p></span></p>\n\n<p class=MsoListParagraphCxSpFirst><span style='font-size:9.0pt;line-height:\n115%;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Protecting\nagainst unauthorized logons</span></b><span style='font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#4D4D4D'><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Digital App allows\nthe customer to optionally use the fingerprints he has stored on his Device to\nlogon to the banking services.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>Once the customer has\nnominated how he will logon, he will not be necessarily required to enter his Login\nID and Password into the App.&nbsp; He will only be required to use Biometric\nverification to logon.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>Where the customer has\nopted to enable Biometric Login, any of the fingerprints he has stored on his\nDevice can logon and can authorize any transactions in banking services. Customer\nshould ensure that only his fingerprint/s are stored on his Device.&nbsp; When he\nlogs on using Biometric Login, he instructs the Bank to perform the\ntransactions.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>He can delete any of\nthe fingerprints he has stored on his Device at any time by going into his\nDevice settings.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>We also recommend that\nhe setup a Passcode on his Device itself, so as to prevent unauthorized access\nto his Device and the App.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Protecting\nagainst unauthorized transactions<o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;class=MsoListParagraphCxSpFirst style='text-align:justify;line-height:normal'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'>The Customer agrees that he will<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>not leave the Device\nunattended and left logged into the App;<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>lock his Device or\ntake other steps necessary to stop unauthorized use of the Device and the App;<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>not act fraudulently\nor maliciously in relation to the App or software e.g. you will not copy,\nmodify, adversely effect, reverse engineer, hack into or insert malicious code\ninto the App or its software; and<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>let us know\nimmediately by calling the UBL Contact Centre at (+9221) 111-825-888; in case\nof any unusual/unauthorized transactions is noticed.<o:p></o:p></span></p>\n\n<p class=MsoListParagraphCxSpLast><span style='font-size:9.0pt;line-height:\n115%;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";\ncolor:#444444'><o:p>&nbsp;</o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Charges<o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer hereby\nagrees to bear the charges as may be stipulated by UBL from time to time for\navailing the Digital App Services.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall be\nclearly notified of the charges through schedule of charges of the Bank or\nthrough information posted on the website of the Bank or through emails sent to\nCustomer&#39;s registered email address with the bank.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer hereby\nauthorizes UBL to recover the service charge by debiting one of the Accounts of\nthe Customer or by sending a bill to the Customer who will be liable to make\nthe payment within a specified period. Failure to do so shall result in recovery\nof the service charge by UBL in any manner as may deem fit along with such\ninterest, if any, and/or withdrawal of the Digital App without any liability to\nUBL.<br style='mso-special-character:line-break'>\n<![if !supportLineBreakNewLine]><br style='mso-special-character:line-break'>\n<![endif]><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Applicability\nto Future Accounts<o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL and the Customer\nagree that if the Customer opens further Accounts with UBL and/or subscribes to\nany of the products/services of UBL or any of the Affiliates, and UBL extends Digital\nApp to such Accounts or products or services and the Customer opts for use\nthereof, then these Terms, subject to any changes, shall automatically apply to\nsuch further use of Digital App by the Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Indemnity</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>In consideration of\nUBL providing Digital App to the Customer, the Customer shall indemnify and\nhold UBL and/or UBL&#39;s Affiliates, as the case may be, including both their\nofficers, employees and agents, indemnified against all losses and expenses on\nfull indemnity basis which UBL may incur, sustain, suffer or is likely to\nsuffer in connection with UBL or Affiliates&#39; execution of the Customer&#39;s\ninstructions and against all actions, claims, demands, proceedings, losses,\ndamages, costs, charges and expenses as a consequence or by reason of providing\na service through Digital App for any action taken or omitted to be taken by\nUBL and /or the Affiliates, its officers, employees or agents, on the\ninstructions of the Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer will pay\nUBL and /or its Affiliates such amount as may be determined by UBL at it sole\ndiscretion to be sufficient to indemnify UBL against any loss or expenses even\nthough they may not have arisen or are contingent in nature.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer shall\ntake all necessary precautions to ensure that there are no mistakes and errors\nand that the information given, instructed or provided to UBL is error free,\naccurate, proper complete and up to date at all points of time. On the other\nhand in the event of the Customer&#39;s Account receiving an incorrect credit by\nreason of a mistake committed by any other person, UBL shall be entitled to\nreverse the incorrect credit at any time whatsoever without the prior consent\nof the Customer. The Customer shall be liable and responsible to Bank and\naccede to accept the Bank&#39;s instructions without questions for any unfair or\nunjust gain obtained by the Customer as a result of the same.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Disclosure of Information<o:p></o:p></b></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer accepts\nand agrees that UBL and/or its Affiliates or their contractors may hold and\nprocess his Personal Information and all other information concerning his\nAccount(s) on computer or otherwise in connection with Digital App as well as\nfor analysis, credit scoring and marketing pursuant to being instructed by the\ncustomer in writing and as provided under applicable law.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;&nbsp;&nbsp;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customer also\nagrees that UBL may disclose, as provided under current Pakistan law, to other\ninstitutions Personal Information as may be reasonably necessary for reasons\ninclusive of but not limited to participation in any telecommunication or\nelectronic clearing network, in compliance with a legal directive, for credit\nrating by recognized credit scoring agencies, for fraud prevention purposes.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Change of Terms</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall have\nabsolute discretion to amend or supplement or delete any of the Terms contained\nherein at any time and will endeavor to give prior notice of thirty days for\nsuch changes. Such change to the Terms shall be communicated to the Customer\nthrough its website or through email sent to the Customer&#39;s registered email\naddress. By continuing to use any existing or new services as may be introduced\nby UBL, the Customer shall be deemed to have accepted the amended Terms.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Non-Transferability</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The grant of Digital\nApp to a Customer is purely personal in nature and not transferable under any\ncircumstance and shall be used only by the Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><br>\n<b style='mso-bidi-font-weight:normal'>Notices</b><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL may publish\nnotices of general nature, which are applicable to all Customers in newspapers\nor on its web site or through emails sent to customers registered email\naddress. Such notices will have the same effect as a notice served individually\nto each Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>General</span></b><span\nstyle='font-size:9.0pt;font-family:\"Arial\",sans-serif;mso-fareast-font-family:\n\"Times New Roman\";color:#444444'><o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>All costs incurred by\nthe User, including but not limited to any telecommunication costs to use Digital\nApp, would be borne by the Customer.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Assignment</span></b>\n<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL shall be entitled\nto sell, assign or transfer UBL&#39;s right and obligations under the Terms and any\nsecurity in favor of UBL (including all guarantee/s) to any person of UBL&#39;s\nchoice in whole or in part and in such manner and on such terms and conditions\nas UBL may decide. Any such sale, assignment or transfer shall conclusively\nbind the Customer and all other persons.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Customers, his\nheirs, legal representatives, executors, administrators and successors are\nbound by the Terms and the Customer shall not be entitled to transfer or assign\nany of his rights and obligations.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'><br>\nGoverning Law and Jurisdiction<o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Terms will be\nconstrued and enforced in accordance with, and the rights of the parties hereto\nwill be governed by, the laws of the Islamic Republic of Pakistan applicable\ntherein. Any and all disputes arising under the Terms, whether as to\ninterpretation, performance or otherwise, will be subject to the exclusive\njurisdiction of the courts at Karachi, Pakistan and each of the parties hereto\nhereby irrevocably attorns to the exclusive jurisdiction of such courts.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>The Parties hereby\nagree that any legal action or proceedings arising out of the Terms for Digital\nApp shall be brought in the courts and to irrevocably submit themselves to the\njurisdiction of such courts.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL may, however, in\nits absolute discretion, commence any legal action or proceedings arising out\nof the Terms for Digital App in any other court, tribunal or other appropriate\nforum, and the Customer hereby consents to that jurisdiction.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>Any provision of the\nTerms for Digital App which is prohibited or unenforceable in any jurisdiction\nshall, as to such jurisdiction, be ineffective to the extent of prohibition or\nunenforceability but shall not invalidate the remaining provisions of the Terms\nor affect such provision in any other jurisdiction.<o:p></o:p></span></p>\n\n<p style='margin-left:.3in;text-align:justify'><b\nstyle='mso-bidi-font-weight:normal'><span style='font-size:9.0pt;font-family:\n\"Arial\",sans-serif;mso-fareast-font-family:\"Times New Roman\";color:#444444'>Termination\n<o:p></o:p></span></b></p>\n\n<p style='margin-left:.3in;text-align:justify;text-indent:-.25in;mso-list:l2 level1 lfo9'><span\nstyle='font:12.0pt \"Times New Roman\"'>&#149;\n</span></span></span><span style='font-size:9.0pt;font-family:\"Arial\",sans-serif;\nmso-fareast-font-family:\"Times New Roman\";color:#444444'>UBL reserves the right\nto terminate the facility of the Digital App, either partially or in totality,\nat any time whatsoever, without prior notice. UBL also reserves the right at\nany time without prior notice to add / alter / modify / change or vary all of\nthese Terms and Conditions.<o:p></o:p></span></p>\n\n</div>\n\n</body>\n\n</html>\n";
    
    NSString *htmlString =
    [NSString stringWithFormat:@"<font face='Aspira Light' size='3'>%@", txt];
    [webView1 loadHTMLString:htmlString baseURL:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void) Show_Term_Condition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        // NSLog(@"%@",gblclass.chk_device_jb);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"IP",
                                                                       @"isRooted", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ShowTermCondition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //             NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSString* responsecode =[NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
                  
                  if([responsecode isEqualToString:@"0"])
                  {
                      NSString *urlAddress = [dic objectForKey:@"TermNConditionText"];
                      [webView1 loadHTMLString:urlAddress baseURL:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[error localizedDescription] :@"0"];
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"load"])
        {
            [self Show_Term_Condition:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
//            btn_agree_chck.enabled = NO;
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        [self Show_Term_Condition:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [hud hideAnimated:YES];
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
    //    if (self.responseData == nil) {
    //        self.responseData = [NSMutableData dataWithData:data];
    //        NSLog(@"%@", self.responseData);
    //        [self custom_alert:@"Attention" :@"0"];
    //
    //    } else {
    //        [self.responseData appendData:data];
    //    }
    
    //    if (self.responseData == nil) {
    //        self.responseData = [NSMutableData dataWithData:data];
    //    } else {
    //        [self.responseData appendData:data];
    //    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut)
    {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    }
    else if (error.code == NSURLErrorNotConnectedToInternet)
    {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    
    // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    //   NSLog(@"%@",gblclass.SSL_name);
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}

//- (BOOL)isSSLPinning
//{
//    NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
//    return [envValue boolValue];
//}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void) slideRight
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) slideLeft
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


@end

