//
//  Act_summary_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Act_summary_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
    
    IBOutlet UITableView* table1;
    IBOutlet UITableView* table2;
    
    IBOutlet UILabel* lbl_T_Pin;
    IBOutlet UILabel* lbl_daily_limit;
    IBOutlet UILabel* lbl_monthly_limit;
    IBOutlet UILabel* lbl_welcome_user;
    IBOutlet UIView* user_view;
    
}

@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;

-(IBAction)btn_slide_close:(id)sender;

@end
