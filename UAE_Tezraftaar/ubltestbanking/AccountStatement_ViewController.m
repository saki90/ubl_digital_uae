//
//  AccountStatement_ViewController.m
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "AccountStatement_ViewController.h"
//#import "ACT_Statement_DetailViewController.h"
#import "Slide_menu_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"



@interface AccountStatement_ViewController ()
{
    NSArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblClass;
    APIdleManager* timer_class;
}
@end

@implementation AccountStatement_ViewController
@synthesize secondViewController;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arr_act_type=[[NSArray alloc] init];
    gblClass=[[GlobalStaticClass alloc] init];
    
    table.delegate=self;
    arr_act_type=@[@"Account Summary",@"Account Statement",@"Cheque Management",@"ATM Card Management"];
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //        //  [self.timeoutLabel  setText:@"YES"];
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //
    //        [self presentViewController:myController animated:YES completion:nil];
    //
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *MyIdentifier = @"MyIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Yourstring=[arr_act_type objectAtIndex:indexPath.row];
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    
    
    switch (indexPath.row)
    {
        case 0:
            
            [self performSegueWithIdentifier:@"act_summary" sender:self];
            break;
        case 1:
            
            [self performSegueWithIdentifier:@"act_statement" sender:self];
            break;
            
        case 2:
            [self performSegueWithIdentifier:@"check_management" sender:self];
            break;
            
        case 3:
            [self performSegueWithIdentifier:@"atm_card_management" sender:self];
            break;
            
        default: break;
    }
    
}


-(IBAction)buttonClicked:(id)sender
{
    
    //    self.secondViewController =
    //    [[Slide_menu_VC alloc] initWithNibName:@"SecondViewController"
    //                                          bundle:nil];
    //    [self presentViewController:self.secondViewController animated:YES completion:nil];
    
    
    ///    UIViewController *viewVC = [[UIViewController alloc] init];
    //   [self presentViewController:viewVC animated:YES completion:nil];
    
    
    
    //Loading a view controller from the storyboard
    UIViewController *viewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    self.view.frame = CGRectMake(0, 400, 320, 480);
    
    
    [self presentViewController:viewVC animated:YES completion:nil];
    
}

@end

