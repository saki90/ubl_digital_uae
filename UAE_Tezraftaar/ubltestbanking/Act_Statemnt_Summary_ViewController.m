//
//  Act_Statemnt_Summary_ViewController.m
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Act_Statemnt_Summary_ViewController.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "APIdleManager.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomTransitionVController.h"


@interface Act_Statemnt_Summary_ViewController ()<UIViewControllerTransitioningDelegate>
{
    GlobalStaticClass* gblClass;
    NSNumber* num;
    APIdleManager* timer_class;
    NSNumber *number;
    NSArray* split;
    UIStoryboard *storyboard;
    UIViewController *vc;
}
@end

@implementation Act_Statemnt_Summary_ViewController
@synthesize arr_;
@synthesize transitionController;


- (void)viewDidLoad
{
    
    @try {
        
        [super viewDidLoad];
        // Do any additional setup after loading the view.
        
        gblClass=[GlobalStaticClass getInstance];
        self.transitionController = [[TransitionDelegate alloc] init];
        
        //NSLog(@"%@",gblClass.acct_statmnt_indexpath);
        
        num=[NSNumber numberWithInt:[gblClass.acct_statmnt_indexpath intValue]];
        //NSLog(@"%@",gblClass.arr_acct_statement_global);
        //NSLog(@"%lu",(unsigned long)[gblClass.arr_acct_statement_global count]);
        
        split = [[gblClass.arr_acct_statement_global objectAtIndex:[num intValue]] componentsSeparatedByString: @"|"];
        
        //NSLog(@"%@",gblClass.acct_statmnt_indexpath);
        //NSLog(@"%lu",(unsigned long)[split objectAtIndex:1]);
        
        NSLog(@"%@",gblClass.arr_graph_name);
        
        if([[gblClass.arr_graph_name objectAtIndex:0] isEqualToString:@"1"])
        {
            
            //NSLog(@"%@",[gblClass.arr_graph_name objectAtIndex:5]);
            
            if ([[gblClass.arr_graph_name objectAtIndex:5] isEqualToString:@"1"])
            {
                NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
                attach1.image = [UIImage imageNamed:@"Check-acct_detail.png"];
                attach1.bounds = CGRectMake(10, 0, 15, 15);
                NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
                
                //gblClass.is_default_acct_no
                //[gblclass.arr_graph_name objectAtIndex:4];
                NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[gblClass.arr_graph_name objectAtIndex:4]];
                NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
                [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
                
                [mutableAttriStr1 appendAttributedString:imageStr1];
                lbl_default_acct_id.attributedText = mutableAttriStr1;
            }
            else
            {
                lbl_default_acct_id.text=[gblClass.arr_graph_name objectAtIndex:4];
            }
            
        }
        else
        {
          //  NSLog(@"%@",gblClass.arr_graph_name);
            
            //NSLog(@"%@", gblClass.arr_acct_statement_global);
            
            if ([gblClass.arr_graph_name count]==0)
            {
                lbl_default_acct_id.text = gblClass.is_default_acct_id;
            }
            else
            {
                lbl_default_acct_id.text=[gblClass.arr_graph_name objectAtIndex:4];
            }
            
           // lbl_default_acct_id.text=[gblClass.arr_graph_name objectAtIndex:4];
        }
        
        
        
        // if ([[gblClass.arr_graph_name objectAtIndex:6] isEqualToString:@"Credit Card Account"])
        if([gblClass.acct_summary_section isEqualToString:@"Credit Card Account"])
        {
            //NSLog(@"Credit Card Account");
            
            //For Amount ::
            
            NSString* s;
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"()"];
            s = [[[split objectAtIndex:4] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            //        //NSLog(@"%@", s);
            
            
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"()"];
            NSRange range = [[split objectAtIndex:4] rangeOfCharacterFromSet:cset];
            
            
            NSTextAttachment * attach2 = [[NSTextAttachment alloc] init];
            attach2.image = [UIImage imageNamed:@"Pkr_acct_detail.png"];
            attach2.bounds = CGRectMake(10, 0, 20, 10);
            NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach2];
            
            NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", gblClass.acct_summary_curr,s]];
            NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:18.0]};
            [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
            
            [mutableAttriStr appendAttributedString:imageStr];
            
            lbl_debit_name.text=@"Amount";
            
            if (range.location == NSNotFound)
            {
                // no ( or ) in the string
                //NSLog(@" no ( or ) in the string");
                
                //  lbl_debit.attributedText=mutableAttriStr;
                
                lbl_debit.text=[NSString stringWithFormat:@"%@ %@",gblClass.acct_summary_curr,s];
                
                UIGraphicsBeginImageContext(self.view.frame.size); //  Pkr_acct_detail.png
                [[UIImage imageNamed:@"inner-bg2.png"] drawInRect:self.view.bounds];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.view.backgroundColor = [UIColor colorWithPatternImage:image];
                
                img_cr.image=[UIImage imageNamed:@"debit.png"];
                
                
                lbl_default_acct_id.textColor=[UIColor colorWithRed:16/255.0 green:223/255.0 blue:255/255.0 alpha:1.0];
                lbl_debit_name.textColor=[UIColor colorWithRed:16/255.0 green:223/255.0 blue:255/255.0 alpha:1.0];
                lbl_balc_amt_name.textColor=[UIColor colorWithRed:16/255.0 green:223/255.0 blue:255/255.0 alpha:1.0];
                
            }
            else
            {
                // ( or ) are present
                //NSLog(@"( or ) are present");
                
                NSString* s,*string1;
                
                NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"()"];
                s = [[[split objectAtIndex:4] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
                
                //NSLog(@"%@",s);
                
                
                //lbl_debit.attributedText=mutableAttriStr;
                
                lbl_debit.text=[NSString stringWithFormat:@"%@ -%@",gblClass.acct_summary_curr,s];
                
                UIGraphicsBeginImageContext(self.view.frame.size);
                [[UIImage imageNamed:@"inner-bg3.png"] drawInRect:self.view.bounds];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.view.backgroundColor = [UIColor colorWithPatternImage:image];
                
                img_cr.image=[UIImage imageNamed:@"icon_credit.png"];
                img_cr.contentMode = UIViewContentModeScaleAspectFit;
                
                lbl_default_acct_id.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                lbl_debit_name.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                lbl_balc_amt_name.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                
            }
            
            lbl_balc_amt_name.hidden=YES;
            lbl_balc.hidden=YES;
            
        }
        else
        {
            
            if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
            {
                lbl_debit_name.text=@"Credit Amount";
                lbl_debit.text=[split objectAtIndex:2];
                //NSLog(@"%@",[split objectAtIndex:3]);
                
                NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
                NSNumber *number1 = [NSDecimalNumber decimalNumberWithString:mystring];
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                [formatter setMinimumFractionDigits:2];
                [formatter setMaximumFractionDigits:2];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                if ([mystring isEqualToString:@""])
                {
                    number1=[NSNumber numberWithInt:0];//[NSDecimalNumber decimalNumberWithDecimal:@"12"];
                }
                
                
                NSLog(@"%@",gblClass.arr_graph_name);
                
                
                NSTextAttachment * attach = [[NSTextAttachment alloc] init];
                attach.image = [UIImage imageNamed:@"Pkr_acct_detail.png"];
                attach.bounds = CGRectMake(10, 0, 20, 10);
                NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
                
                NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", gblClass.acct_summary_curr, [formatter stringFromNumber:number1]]];
                NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0]};
                [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
                
                [mutableAttriStr appendAttributedString:imageStr];
                //  lbl_debit.attributedText = mutableAttriStr;
                
                
                lbl_debit.text=[NSString stringWithFormat:@"%@ %@",gblClass.acct_summary_curr,[formatter stringFromNumber:number1]];
                
                UIGraphicsBeginImageContext(self.view.frame.size); //  Pkr_acct_detail.png
                [[UIImage imageNamed:@"inner-bg2.png"] drawInRect:self.view.bounds];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.view.backgroundColor = [UIColor colorWithPatternImage:image];
                
                img_cr.image=[UIImage imageNamed:@"icon_debit.png"];
                img_cr.contentMode = UIViewContentModeScaleAspectFill;
                
                lbl_default_acct_id.textColor=[UIColor colorWithRed:116/255.0 green:174/255.0 blue:233/255.0 alpha:1.0];
                lbl_debit_name.textColor=[UIColor colorWithRed:116/255.0 green:174/255.0 blue:233/255.0 alpha:1.0];
                lbl_balc_amt_name.textColor=[UIColor colorWithRed:116/255.0 green:174/255.0 blue:233/255.0 alpha:1.0];
                
            }
            else
            {
                lbl_debit_name.text=@"Debit Amount";
                lbl_debit.text=[split objectAtIndex:2];
                //NSLog(@"%@",[split objectAtIndex:2]);
                
                NSString *mystring =[NSString stringWithFormat:@"%@",[split objectAtIndex:2]];
                number = [NSDecimalNumber decimalNumberWithString:mystring];
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                [formatter setMinimumFractionDigits:2];
                [formatter setMaximumFractionDigits:2];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                if ([mystring isEqualToString:@""])
                {
                    number=[NSNumber numberWithInt:0];//[NSDecimalNumber decimalNumberWithDecimal:@"12"];
                }
                
                NSTextAttachment * attach = [[NSTextAttachment alloc] init];
                attach.image = [UIImage imageNamed:@"Pkr_acct_detail.png"];
                attach.bounds = CGRectMake(10, 0, 20, 10);
                NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
                
                NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", gblClass.acct_summary_curr,[formatter stringFromNumber:number]]];
                NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:20.0]};
                [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
                
                [mutableAttriStr appendAttributedString:imageStr];
                // lbl_debit.attributedText = mutableAttriStr;
                
                
                lbl_debit.text=[NSString stringWithFormat:@"%@ %@",gblClass.acct_summary_curr,[formatter stringFromNumber:number]];
                
                UIGraphicsBeginImageContext(self.view.frame.size);
                [[UIImage imageNamed:@"inner-bg3.png"] drawInRect:self.view.bounds];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                self.view.backgroundColor = [UIColor colorWithPatternImage:image];
                
                img_cr.image=[UIImage imageNamed:@"icon_credit.png"];
                img_cr.contentMode = UIViewContentModeScaleAspectFill;
                
                lbl_default_acct_id.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                lbl_debit_name.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                lbl_balc_amt_name.textColor=[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0];
                
            }
            
        }
        
        
        lbl_transaction_date.text=[split objectAtIndex:1];
        lbl_desc.text=[split objectAtIndex:0];
        //lbl_debit.text=[split objectAtIndex:2];
        
        NSTextAttachment * attach2 = [[NSTextAttachment alloc] init];
        attach2.image = [UIImage imageNamed:@"Pkr_acct_detail.png"];
        attach2.bounds = CGRectMake(10, 0, 20, 10);
        NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach2];
        
        NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", gblClass.acct_summary_curr,[split objectAtIndex:4]]];
        NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:18.0]};
        [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
        
        [mutableAttriStr appendAttributedString:imageStr];
        //  lbl_balc.attributedText = mutableAttriStr;
        
        
        lbl_balc.text=[NSString stringWithFormat:@"%@ %@",gblClass.acct_summary_curr,[split objectAtIndex:4]];
        
        
        //    if ([[gblClass.arr_graph_name objectAtIndex:6] isEqualToString:@"Credit Card Account"])
        //    {
        //        NSString* s;
        //
        //        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"()"];
        //        s = [[number componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        //
        //        if (range.location == NSNotFound)
        //        {
        //
        //
        //        }
        //        lbl_debit_name.text=@"Amount";
        //        //lbl_debit.text=
        //    }
        
    }
    @catch (NSException *exception)
    {
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
    
    
    //    lbl_default_acct_name.text= gblClass.defaultaccountname;
    //    lbl_default_acct_id.text=gblClass.is_default_acct_no;
    
    
    
    NSTextAttachment * attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"check_select_acct.png"];
    attach.bounds = CGRectMake(10, 0, 15, 15);
    NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
    
    NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:gblClass.acct_statement_id];
    
    NSDictionary * attris;
    
    if ([[split objectAtIndex:3] isEqualToString:@"Cr"])
    {
        attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:116/255.0 green:174/255.0 blue:233/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
    }
    else
    {
        attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:132/255.0 green:12/255.0 blue:2/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
    }
    
    
    [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
    
    [mutableAttriStr appendAttributedString:imageStr];
    lbl_default_acct_id.attributedText = mutableAttriStr;
    
    
    lbl_default_acct_name.text= gblClass.acct_statement_name;
    
    
}


-(void)AttributedTextInUILabelWithGreenText:(NSString *)greenText withYellowText:(NSString *) yellowText withBlueBoldText:(NSString *)blueBoldText
{
    NSString *text = [NSString stringWithFormat:@"Here are %@, %@ and %@",
                      greenText,
                      yellowText,
                      blueBoldText];
    
    //Check If attributed text is unsupported (below iOS6+)
    if (![lbl_default_acct_name respondsToSelector:@selector(setAttributedText:)])
    {
        lbl_default_acct_name.text = text;
    }
    // If attributed text is available
    else
    {
        // Define general attributes like color and fonts for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: lbl_default_acct_name.textColor,
                                  NSFontAttributeName:lbl_default_acct_name.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // yellow text attributes
        UIColor *yellowColor = [UIColor yellowColor];
        NSRange yellowTextRange = [text rangeOfString:greenText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:yellowColor}
                                range:yellowTextRange];
        
        // blue and bold text attributes
        UIColor *blueColor = [UIColor blueColor];
        UIFont *boldFont = [UIFont boldSystemFontOfSize:lbl_default_acct_name.font.pointSize];
        NSRange blueBoldTextRange = [text rangeOfString:blueBoldText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:blueColor,
                                        NSFontAttributeName:boldFont}
                                range:blueBoldTextRange];
        lbl_default_acct_name.attributedText = attributedText;
        
    }
}


-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    CustomTransitionVController *myCustom = [[CustomTransitionVController alloc] init];
    myCustom.type = @"dismiss";
    
    return myCustom;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    
    self.transitioningDelegate = self;
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                    message:Logout
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblClass.story_board bundle:nil];
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}

-(IBAction)btn_screenshot:(id)sender
{
    [self takeAScreenShot];
    
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.bounds];
    flashView.backgroundColor = [UIColor whiteColor];
    flashView.alpha = 1.0;
    
    // Add the flash view to the window
    
    [self.view addSubview:flashView];
    
    // Fade it out and remove after animation.
    
    [UIView animateWithDuration:0.20 animations:^{
        flashView.alpha = 0.0;
    }
                     completion:^(BOOL finished) {
                         [flashView removeFromSuperview];
                     }];
    
    
    // For Sound Play ::
    
    
    //    /* Use this code to play an audio file */
    //    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"test"  ofType:@"m4a"];
    //    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    //
    //    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    //    player.numberOfLoops = -1; //Infinite
    //
    //    [player play];
    
    
}

-(UIImage *) takeAScreenShot
{
    // here i am taking screen shot of whole UIWindow, but you can have the screenshot of any individual UIViews, Tableviews  . so in that case just use  object of your UIViews instead of  keyWindow.
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {// checking for Retina display
        UIGraphicsBeginImageContextWithOptions(keyWindow.bounds.size, YES, [UIScreen mainScreen].scale);
        //if this method is not used for Retina device, image will be blurr.
    }
    else
    {
        UIGraphicsBeginImageContext(keyWindow.bounds.size);
    }
    
    [keyWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // now storing captured image in Photo Library of device
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    /*** if you want to save captured image locally in your app's document directory
     NSData * data = UIImagePNGRepresentation(image);
     
     NSString *imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"testImage.png"];
     //NSLog(@"Path for Image : %@",imagePath);
     [data writeToFile:imagePath atomically:YES];
     
     *******************************/
    return image;
}


-(IBAction)btn_more:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:gblClass.story_board bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblClass.custom_alert_msg=msg1;
    gblClass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblClass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

