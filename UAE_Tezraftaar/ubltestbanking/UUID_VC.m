//
//  UUID_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 30/07/2018.
//  Copyright © 2018 ammar. All rights reserved.
//
#define FBEncryptorAES_String_Key @"abcqwert34567qwertyabc"
#define SERVICE_NAME @"find_ski"

#import "UUID_VC.h"
#import <Security/Security.h>
#import "Keychain.h"

@interface UUID_VC ()
{
    NSString* UDID1;
    NSString* ky_chck;
    Keychain * keychain;
    UIStoryboard *storyboard;
    UIViewController *vc;
}
@end

@implementation UUID_VC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString* ud=[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    UDID1 = [ud stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    // [self Guid_key_chain_method_make];
    [self Guid_key_chain_method_load];
    
    ky_chck = @"";
    lbl_udid.text = [ud stringByReplacingOccurrencesOfString:@"-" withString:@""];
    keychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    
    // [self Add_item:@"udid"];
    [self find_item:@"udid"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// ******************* GUID ********************************

-(void) Guid_key_chain_method_make
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *GUID = [NSString stringWithFormat:@"%@",UDID1];
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    
    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
    {
        NSLog(@"Guid Already Exists");
        
        //   gblclass.Udid = GUID;
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Already Exists", nil)
        //                                                        message:NSLocalizedString(@"Please update it instead.",)
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
    else
    {
        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        
        NSLog(@"Guid create");
        keychainItem[(__bridge id)kSecValueData] = [GUID dataUsingEncoding:NSUTF8StringEncoding]; //Our password
        
        OSStatus sts = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
        NSLog(@"Error Code: %d", (int)sts);
        
        if ([ky_chck isEqualToString:@"1"])
        {
            ky_chck = @"0";
            [self Guid_key_chain_method_load];
        }
        
    }
}


-(void) Guid_key_chain_method_load
{
    //Let's create an empty mutable dictionary:
    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
    
    NSString *username = @"Guid1";
    NSString *website = @"http://www.myawesomeservice.com";
    
    //Populate it with the data and the attributes we want to use.
    
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
    keychainItem[(__bridge id)kSecAttrServer] = website;
    keychainItem[(__bridge id)kSecAttrAccount] = username;
    
    //Check if this keychain item already exists.
    
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    
    CFDictionaryRef result = nil;
    
    OSStatus sts = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    
    NSLog(@"Error Code: %d", (int)sts);
    NSLog(@"%d",noErr);
    
    if(sts == noErr)
    {
        NSLog(@"guid load");
        NSDictionary *resultDict = (__bridge NSDictionary *)result;
        NSData *pswd = resultDict[(__bridge id)kSecValueData];
        NSString *guid = [[NSString alloc] initWithData:pswd encoding:NSUTF8StringEncoding];
        
        lbl_udid_kychain.text = guid;
        
        NSLog(@"%@",guid);
        
        //        if ( [gblclass.Udid length]==0)
        //        {
        //            gblclass.Udid=[NSString stringWithFormat:@"%@",guid];
        //        }
        
    }
    else
    {
        
        [self Guid_key_chain_method_make];
        ky_chck = @"1";
        NSLog(@"Guid Not found, Old load guid");
        //gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
        
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The Item Doesn't Exist", nil)
        //                                                        message:NSLocalizedString(@"No keychain item found for this user.", )
        //                                                       delegate:nil
        //                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
        //                                              otherButtonTitles:nil];
        //        [alert show];
        
    }
}




-(void)Add_item:(NSString*)key
{
    // NSString *key = FBEncryptorAES_String_Key;
    
    // TEST
    NSData *value = [UDID1 dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    if([keychain insert:key :value])
    {
        NSLog(@"Successfully added data");
        [self find_item:@"udid"];
    }
    else
    {
        [self Remove_item:@"udid"];
        NSLog(@"Failed to  add data");
    }
}

-(void)find_item:(NSString*)key
{
    // NSString *key =  FBEncryptorAES_String_Key;
    NSData * data = [keychain find:key];
    
    if(data == nil)
    {
        NSLog(@"Keychain data not found");
        [self Add_item:@"udid"];
    }
    else
    {
        lbl_udid_kyc_lib.text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"Data is =%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
}

-(void)Update_item:(NSString*)key
{
    
    // NSString *key = FBEncryptorAES_String_Key;
    
    //   TEST
    NSData *value = [UDID1 dataUsingEncoding:NSUTF8StringEncoding];
    
    
    if([keychain update:key :value])
    {
        [self find_item:@"udid"];
        NSLog(@"Successfully updated data");
    }
    else
        NSLog(@"Failed to add data");
}

-(void)Remove_item:(NSString*)key
{
    //NSString *key = FBEncryptorAES_String_Key;
    
    if([keychain remove:key])
    {
        NSLog(@"Successfully removed data");
    }
    else
    {
        NSLog(@"Unable to remove data");
    }
}

- (IBAction)btn_back:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)btn_next:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"selection"];
    [self presentViewController:vc animated:YES completion:nil];
}


@end
