//
//  Forgot_pw_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 07/01/2019.
//  Copyright © 2019 ammar. All rights reserved.
//

#import "Forgot_pw_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"


@interface Forgot_pw_VC ()
{
    MBProgressHUD* hud;
    UIAlertController* alert;
    GlobalStaticClass *gblclass;
    NSString *urlAddress;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    NSString* first_time_chk;
}

@end

@implementation Forgot_pw_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    
    
    [hud showAnimated:YES];
    [self checkinternet];
    if (netAvailable)
    {
        if ([gblclass.forget_pw_btn_click isEqualToString:@"1"])
        {
            lbl_header.text = @"SIGN UP";
            //https://www.ubldirect.com/Corporate/uaeebank.aspx
            //https://www.ubldirect.com/gnb/Registration/tabid/64/ctl/noatm/mid/400/Default.aspx
            urlAddress = @"https://www.ubldirect.com/Corporate/uaeebank.aspx";
        }
        else
        {
            lbl_header.text = @"LOCATE US";
            urlAddress = locate_us_link;
        }
        
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:urlAddress];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [webView loadRequest:requestObj];
        
//        webView.delegate = self;
    }
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
    stringForKey:@"enable_touch"]; //first_time
    
}

//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    [hud hideAnimated:YES];
//
//    //NSLog(@"%@",error);
//}



-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
       stringForKey:@"enable_touch"];
       
       if ([first_time_chk isEqualToString:@"2"])
       {
           mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
           vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"]; //new_signup
           [self presentViewController:vc animated:NO completion:nil];
       }
       else if ([first_time_chk isEqualToString:@"0"])
       {
           mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
           vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"]; //new_signup
           [self presentViewController:vc animated:NO completion:nil];
       }
       else
       {
           mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
           vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"]; //new_signup
           [self presentViewController:vc animated:NO completion:nil];
       }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}





@end
