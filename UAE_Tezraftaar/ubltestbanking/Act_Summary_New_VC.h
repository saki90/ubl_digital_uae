//
//  Act_Summary_New_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 07/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Act_Summary_New_VC : UIViewController
{
     IBOutlet UITableView* table1;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end
