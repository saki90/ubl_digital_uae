//
//  Tz_Review.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import "Tz_Review.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"

@interface Tz_Review ()
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    NSString* ssl_count;
    Encrypt *encrypt;
    NSArray *split;
    NSString* responsecode;
    NSMutableArray* a;
    NSString * vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Tz_Review
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    gblclass=[GlobalStaticClass getInstance];
    chk_ssl = @"";
    ssl_count = @"0";
    encrypt = [[Encrypt alloc] init];
    txt_nick.delegate = self;
    vw_down_chck=@"0";
    
    if ([gblclass.arr_tezrftar_bene_additn count]>0)
    {
        NSLog(@"%@",gblclass.acct_add_type);
        if ([gblclass.acct_add_type isEqualToString:@"cash"])
        {
            NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
            lbl_name_heading.text = @"Name";
            lbl_cnic_heading.text = @"Cnic";
            lbl_name.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:0];
            lbl_cnic.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:1];
            lbl_contact_num.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:2];
            lbl_address.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:3];
            lbl_relation.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:5];
        }
        else if([gblclass.acct_add_type  isEqualToString:@"UBL Branch Account"])
        {
            NSLog(@"%@",gblclass.chck_tezraftr_bene_type);
            lbl_name_heading.text = @"Account Title";
            if ([gblclass.chck_tezraftr_bene_type isEqualToString:@"account"])
            {
                lbl_cnic_heading.text = @"Account Number";
            }
            else
            {
                lbl_cnic_heading.text = @"Iban Number";
            }
            
            
            NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
            lbl_name.text = gblclass.acctitle;//[gblclass.arr_tezrftar_bene_additn objectAtIndex:3];
            lbl_cnic.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:0];
            lbl_contact_num.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:1];
            lbl_address.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:2];
            lbl_relation.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:5];
        }
        else if([gblclass.acct_add_type  isEqualToString:@"Other Bank"])
        {
            lbl_cnic_heading.text = @"Iban Number";
            NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
            lbl_name.text = gblclass.acctitle;//[gblclass.arr_tezrftar_bene_additn objectAtIndex:3];
            lbl_cnic.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:0];
            lbl_contact_num.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:1];
            lbl_address.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:2];
            lbl_relation.text = [gblclass.arr_tezrftar_bene_additn objectAtIndex:5];
        
      }
        else
        {
            lbl_name.text = @"-";
            lbl_cnic.text = @"-";
            lbl_contact_num.text = @"-";
            lbl_address.text = @"-";
            lbl_relation.text = @"-";
        }
        
    }
    else
    {
        lbl_name.text = @"-";
        lbl_cnic.text = @"-";
        lbl_contact_num.text = @"-";
        lbl_address.text = @"-";
        lbl_relation.text = @"-";
    }
    
}


- (void)slideLeft
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_next:(id)sender
{
    
    if ([txt_nick.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter nick name"  :@"0"];
        return ;
    }
    else if([txt_nick.text length] >0 && [txt_nick.text length] <5 )
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Enter nick between 5 to 30 characters"  :@"0"];
        return ;
    }
    
    [gblclass.arr_tezrftar_bene_additn addObject:txt_nick.text];
    [gblclass.arr_tezrftar_bene_additn addObject:gblclass.tz_city];
    
    [self checkinternet];
    if (netAvailable)
    {
                  //  gblclass.chck_tezraftr_bene_type
        NSLog(@"%@",gblclass.chck_tezraftr_bene_type);
        if ([gblclass.chck_tezraftr_bene_type isEqualToString:@"account"])
        {
            chk_ssl = @"add_account_title";
        }
        else if ([gblclass.chck_tezraftr_bene_type isEqualToString:@"iban"])
        {
            chk_ssl = @"iban";
        }
        else
        {
            chk_ssl = @"next";
        }
        [hud showAnimated:YES];
       // chk_ssl = @"next";
        [self SSL_Call];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    vw_down_chck=@"1";
    [txt_nick resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
    
    if ([textField isEqual:txt_nick])
    {
        MAX_DIGITS=30;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    
     return YES;
    
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}


-(void)SSL_Call
{
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:130];
//    [self.view addSubview:hud];
   // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"next"])
        {
            [self ValidateTezraftaarPayeeAddition:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            //[self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"next"])
    {
        
        NSLog(@"%@",gblclass.acct_add_type);
        if ([gblclass.acct_add_type isEqualToString:@"cash"])
        {
            [self ValidateTezraftaarPayeeAddition_COC:@""];
        }
        else
        {
            [self ValidateTezraftaarPayeeAddition:@""];
        }
        
       // [self ValidateTezraftaarPayeeAddition:@""];
       // [self AddAccountTitleFetch:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
        NSLog(@"%@",gblclass.arr_payee_list);
        
       // [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"add_account_title"])
    {
         [self ValidateTezraftaarPayeeAddition:@""];
//        [self AddAccountTitleFetch:@""];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"iban"])
    {
        [self ValidateTezraftaarPayeeAddition:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




-(void) AddAccountTitleFetch:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
  //  gblclass.strAccessKey_for_add=acc_type;
    
    NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
      
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"UBL"],//acc_type
                                [encrypt encrypt_Data:@"United Bank Limited"],//bankname
                                [encrypt encrypt_Data:@"588974"],//bankimd
                                [encrypt encrypt_Data:@"0908"],//fetchtitlebranchcode
                                [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccType",
                                                                   @"strBankName",
                                                                   @"strBankImd",
                                                                   @"strBranchCode",
                                                                   @"strAccountNumber",
                                                                   @"strIBFTFormat",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager POST:@"AddAccountTitleFetch" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              a = [[NSMutableArray alloc] init];
              
              responsecode = [dic objectForKey:@"Response"];
              
              if ([responsecode isEqualToString:@"-1"])
              {
                  
                  [self ValidateTezraftaarPayeeAddition:@""];
                //NSLog(@"Done IBFT load branch");
//                  gblclass.acc_type=acc_type;
//                  gblclass.bankname=bankname;
//                  gblclass.bankimd=bankimd;
//                  gblclass.fetchtitlebranchcode=fetchtitlebranchcode;
//                  gblclass.acc_number=self.txtaccountnumber.text;
//                  gblclass.acctitle=[dic objectForKey:@"strlblAccountTitle"];
                  
//                  vw_fetch_title.hidden = NO;
//                  btn_combo.enabled = NO;
//                  img_combo.hidden = YES;
//                  _txtaccountnumber.enabled = NO;
//
//                  txt_accnt_title_2.text=[dic objectForKey:@"strlblAccountTitle"];
//                  txt_accnt_number.text=[dic objectForKey:@"strlblAccountNumber"];
//                  txt_accnt_nick.text=[dic objectForKey:@"strtxtAccountNick"];
                  
  
//                  if ([txt_accnt_title_2.text isEqualToString:@""] || txt_accnt_title_2.text.length ==0)
//                  {
//                      txt_accnt_title_2.enabled = YES;
//                  }
//                  else
//                  {
//                      txt_accnt_title_2.enabled = NO;
//                      txt_accnt_title_2.borderStyle = UITextBorderStyleNone;
//                      [txt_accnt_title_2 setBackgroundColor:[UIColor clearColor]];
//                  }
//
//                  [txt_accnt_title_2 adjustsFontSizeToFitWidth];
//                  [txt_accnt_nick adjustsFontSizeToFitWidth];
//
//                  lbl_h1_1.hidden = NO;
//                  txt_accnt_title_2.hidden = NO;
//
//                  vw_ibft_relation.hidden = YES;
//                  btn_cancel.hidden = YES;
//                  btn_submit.hidden = YES;
//                  btn_add_record.hidden = NO;
//
//                  _txtaccountnumber.hidden = YES;
//                  lbl_acct_numb_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.text = @"Account No.";
//                  lbl_acct_numb_brnch.text = _txtaccountnumber.text;
//
                  
//                  vw_fetch_title.frame = CGRectMake(0, 178, vw_fetch_title.frame.size.width, vw_fetch_title.frame.size.height);
                  
//                  if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
//                  {
//                      arr_ibft_relation_load=[[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
//
//                      vw_ibft_relation.hidden = NO;
//                      vw_IBFT.hidden = YES;
//                      _txtaccountnumber.text = txt_acct_number_ibft.text;
//                      _txtaccountnumber.hidden = NO;
//
//                      for(dic in arr_ibft_relation_load)
//                      {
//
//                          NSString* IBFT_RELATION_DESC=[dic objectForKey:@"IBFT_RELATION_DESC"];
//                          if ([IBFT_RELATION_DESC isEqual:@""])
//                          {
//                              IBFT_RELATION_DESC=@"N/A";
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//
//                          NSString* IBFT_RELATION_ID=[dic objectForKey:@"IBFT_RELATION_ID"];
//                          if ([IBFT_RELATION_ID isEqual:@""])
//                          {
//                              IBFT_RELATION_ID=@"N/A";
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//
//
//                          NSString *bbb = [a componentsJoinedByString:@"|"];
//                          [arr_ibft_relation addObject:bbb];
//
//
//                          //NSLog(@"%@", bbb);
//                          [a removeAllObjects];
//                      }
//
//                      [table_ibft_relation reloadData];
                      
//                  }
                  
                  
              }
              else if ([responsecode isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                 // [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self custom_alert:resstring :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
//              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
//
//              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//              [alert addAction:ok];
//              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
            
//                  gblclass.custom_alert_msg=@"Please try again later.";
//                  gblclass.custom_alert_img=@"0";
                  
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
        
//        gblclass.custom_alert_msg=@"Please try again later.";
//        gblclass.custom_alert_img=@"0";
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
         
    }
}


-(void) ValidateTezraftaarPayeeAddition_COC:(NSString *)strIndustry {

    @try {

       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
        NSLog(@"%@",gblclass.acctitle);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:gblclass.default_acct_num],
                                           [encrypt encrypt_Data:@"A"],
                                           [encrypt encrypt_Data:gblclass.tz_pay_type], //1,2,3
                                           [encrypt encrypt_Data:gblclass.acctitle],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:3]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:gblclass.tz_city],//city 6
                                           [encrypt encrypt_Data:gblclass.beneficiary_Bank],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:2]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:4]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:5]],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryMsgType",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"email",
                                                                              @"strAccountNo",nil]];


        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ValidateTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
            
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:[dic objectForKey:@"strReturnMessage"]
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
                        [self slideRight];
//                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];

                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      gblclass.check_review_acct_type = @"tezraftaar_bene_add";
                      gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];

                      [hud hideAnimated:YES];
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];

                      NSLog(@"%@",gblclass.story_board);
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_add_otp"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
            
                      [hud hideAnimated:YES];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:Call_center_msg  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:Call_center_msg  :@"0"];
    }
}




-(void) ValidateTezraftaarPayeeAddition:(NSString *)strIndustry {

    @try {

       [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
        NSLog(@"%@",gblclass.acctitle);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                          [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                           [encrypt encrypt_Data:gblclass.M3sessionid],
                                           [encrypt encrypt_Data:gblclass.Udid],
                                           [encrypt encrypt_Data:gblclass.token],
                                           [encrypt encrypt_Data:gblclass.default_acct_num],
                                           [encrypt encrypt_Data:@"A"],
                                           [encrypt encrypt_Data:gblclass.tz_pay_type], //1,2,3
                                           [encrypt encrypt_Data:gblclass.acctitle],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:2]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:gblclass.tz_city],//city
                                           [encrypt encrypt_Data:gblclass.beneficiary_Bank],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:1]],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:4]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:5]],
                                           [encrypt encrypt_Data:@"PK"],
                                           [encrypt encrypt_Data:@"App"],
                                           [encrypt encrypt_Data:@""],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:6]],
                                           [encrypt encrypt_Data:[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]],nil]

                                                                     forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                              @"strSessionId",
                                                                              @"Device_ID",
                                                                              @"Token",
                                                                              @"beneficiaryMemAcctNo",
                                                                              @"beneficiaryMsgType",
                                                                              @"beneficiaryPayType",
                                                                              @"beneficiaryName",
                                                                              @"beneficiaryAddr1",
                                                                              @"beneficiaryId",
                                                                              @"beneficiaryCity",
                                                                              @"beneficiaryBank",
                                                                              @"beneficiaryBankBr",
                                                                              @"beneficiaryBrName",
                                                                              @"beneficiaryBrAddr",
                                                                              @"beneficiaryAcctNo",
                                                                              @"beneficiaryTele",
                                                                              @"beneficiaryMobile",
                                                                              @"beneficiaryNation",
                                                                              @"beneficiaryRelationshipCode",
                                                                              @"beneficiaryRelationshipDesc",
                                                                              @"beneficiaryCountry",
                                                                              @"beneficiaryChannel",
                                                                              @"beneficiaryPostDate",
                                                                              @"email",
                                                                              @"strAccountNo",nil]];


        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ValidateTezraftaarPayeeAddition" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {
            
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
                  NSString * return_msg = [dic objectForKey:@"strReturnMessage"];
                  if(return_msg==(NSString *)[NSNull null])
                  {
                      return_msg =@"";
                  }

                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];

                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];

                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      gblclass.check_review_acct_type = @"tezraftaar_bene_add";
                      gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];

                      [hud hideAnimated:YES];
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];

                      NSLog(@"%@",gblclass.story_board);
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_add_otp"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:return_msg :@"0"];
                  }
            
                      [hud hideAnimated:YES];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  [self custom_alert:@"Please try again later."  :@"0"];

              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later."  :@"0"];
    }
}

- (void)slideRight {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)keyboardWillHide
{
    
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)textViewTapped:(UITapGestureRecognizer *)tap {
     //DO SOMTHING
     
     NSLog(@"TAPPP");
     NSLog(@"%@",tap);
     vw_down_chck = @"1";
 }

 #pragma mark - Gesture recognizer delegate

 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
 }

@end
