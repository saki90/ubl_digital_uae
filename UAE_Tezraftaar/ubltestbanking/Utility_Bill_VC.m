//
//  Utility_Bill_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 22/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Utility_Bill_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
//#import "GnbBillList.h"
//#import "Bill_management_Model_class.h"
//#import "OutdtDatasetISP.h"
//#import "OutdtDatasetOB.h"
//#import "OutdtDatasetUBLBP.h"
//#import "OutdtDatasetUBP.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
 
#import "Encrypt.h"

//d761b541ffa61b754142574c2ab39736abe7a3f8 ubl udid iphone 5


@interface Utility_Bill_VC ()
{
    GlobalStaticClass* gblclass;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSArray* split;
    NSArray* split_bill;
    NSDictionary* dic;
    MBProgressHUD* hud;
    UIAlertController *alert;
    NSMutableArray* arr_get_bill;
    NSMutableArray* arr_get_bill_load;
    NSMutableArray* a;
    // UIAlertController *alert;
    
    NSString* payfrom_acct_id;
    NSString* Payfrm_selected_item_text;
    NSString* Payfrm_selected_item_value;
    NSString* str_amnt;
    NSString* lbl_customer_id;
    NSString* lbl_compny_name;
    NSString* str_comment;
    NSString* str_access_key;
    NSString* str_Tc_access_key;
    NSString* str_ccy;
    NSString* str_TT_accesskey;
    NSString* str_tt_id;
    NSString* str_tt_name;
    NSString* str_regt_consumer_id;
    NSString* str_build;
    NSString* lb_customer_id;
    NSString* lbl_customer_nick;
    NSString* status;
    NSString* str_bill_id;
    NSString* str_due_datt;
    NSString* service_code;
    NSString* PAYMENT_STATUS;
    NSString* Nick;
    NSString* Curr_Balc;
    NSString* lbl_transaction_type;
    NSNumber* IS_PAYBLE_1;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    
    NSMutableArray* arr_bill_UBLBP;
    NSMutableArray* arr_bill_OB;
    NSMutableArray* arr_bill_ISP;
    NSMutableArray* arr_bill_UBP;
    NSMutableArray* search;
    NSString* search_flag;
    UILabel* label;
    UIImageView* img;
    NSString* lbl_balc;
    NSNumber* balc;
    NSString* logout_chck;
    NSString* chk_ssl;
    NSNumberFormatter *numberFormatter;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* min_balc,*max_balc;
    NSString* curr_balance;
    NSString* curr_blac_amnt;
    NSArray* arr_cc;
    NSString *vw_down_chck;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Utility_Bill_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    
    gblclass=[GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    table_from.hidden=YES;
    table_to.hidden=YES;
    
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    arr_bill_UBLBP=[[NSMutableArray alloc] init];
    arr_bill_UBP=[[NSMutableArray alloc] init];
    arr_bill_OB=[[NSMutableArray alloc] init];
    arr_bill_ISP=[[NSMutableArray alloc] init];
    search=[[NSMutableArray alloc] init];
    
    arr_get_bill=[[NSMutableArray alloc] init];
    arr_get_bill_load=[[NSMutableArray alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass.arr_review_withIn_myAcct=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    arr_cc = [[NSArray alloc] init];
    
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    search_flag=@"0";
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
     
    arr_cc = [NSArray arrayWithObjects: @"Full Payment", @"User Specified", nil];
 
    vw_down_chck = @"0";
    curr_balance = @"";
    ssl_count = @"0";
    txt_combo_bill_names.delegate=self;
    txt_cc_comment.delegate = self;
    txt_amnt.delegate = self;
    txt_cc_amnt.delegate = self;
    txt_comment.delegate = self;
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_from.hidden=YES;
    vw_to.hidden=YES;
    txt_t_pin.delegate=self;
    txt_comment.delegate=self;
    lbl_t_pin.hidden=YES;
    txt_t_pin.hidden=YES;
    vw_table.hidden=YES;
    
    txt_cc_amnt.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_cc_comment.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_amnt.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_comment.autocorrectionType = UITextAutocorrectionTypeNo;
    
    
    // [btn_submit setBackgroundColor:  [UIColor colorWithRed:7/255.0f green:89/255.0f blue:139/255.0f alpha:1.0f]];
    txt_combo_frm.text= gblclass.is_default_acct_id_name;
    
    Payfrm_selected_item_text=gblclass.is_default_acct_id_name;
    Payfrm_selected_item_value=gblclass.is_default_acct_no;
    payfrom_acct_id=gblclass.is_default_acct_id;
    lbl_balc= gblclass.is_default_m3_balc;
    txt_Utility_bill_name.text = gblclass.pay_combobill_name;
    
    
    if ([gblclass.arr_bill_load_all count]>0)
    {
        
        split_bill = [[gblclass.arr_bill_load_all objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        //  split_bill=[NSMutableArray arrayWithArray:[gblclass.arr_bill_load_all objectAtIndex:[gblclass.indexxx integerValue]]];
        
//        str_tt_name=[split_bill objectAtIndex:0];
        str_tt_id = [split_bill objectAtIndex:0];
        lbl_customer_id = [split_bill objectAtIndex:3];
        lbl_customers_id.text = [split_bill objectAtIndex:3];
        lbl_nick.text = [split_bill objectAtIndex:6];
//        str_due_datt=[split_bill objectAtIndex:4];
//        str_amnt=[split_bill objectAtIndex:5];
//        status=[split_bill objectAtIndex:14];  //is_payable
//        str_bill_id=[split_bill objectAtIndex:7];
        str_access_key=[split_bill objectAtIndex:2];
        str_regt_consumer_id=[split_bill objectAtIndex:8];
        lbl_compny_name=[split_bill objectAtIndex:2];
        lbl_company.text = [NSString stringWithFormat:@"%@ - %@",[split_bill objectAtIndex:2],[split_bill objectAtIndex:4]];
        lbl_transaction_type = [NSString stringWithFormat:@"%@|%@",[split_bill objectAtIndex:2],[split_bill objectAtIndex:5]];
        service_code = [split_bill objectAtIndex:5];
       
        
        str_TT_accesskey =[split_bill objectAtIndex:7]; //@"ME_TELCO";
//          lbl_customer_nick=[split_bill objectAtIndex:3];
//        PAYMENT_STATUS=[split_bill objectAtIndex:13];
//        IS_PAYBLE_1=[split_bill objectAtIndex:14];
          str_Tc_access_key = @"ME_TELCO";
        
 //       lbl_customers_id.text=[split_bill objectAtIndex:2];
 //       lbl_amnts.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSDecimalNumber decimalNumberWithString:[split_bill objectAtIndex:5]]]];;
 //       lbl_due_dat.text=[split_bill objectAtIndex:4];
 //       lbl_status.text=[split_bill objectAtIndex:6];
 //       txt_combo_bill_names.text=[split_bill objectAtIndex:3];
 //       lbl_company.text=[split_bill objectAtIndex:10];
        _lbl_acct_frm.text=gblclass.is_default_acct_no;
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split_bill objectAtIndex:0]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSDecimalNumber decimalNumberWithString:[split_bill objectAtIndex:0]]]];
        }
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        lbl_amnts.attributedText = mutableAttriStr1;
        
        
        // if ([status isEqualToString:@"PAID"] || [status isEqualToString:@"paid"] || [status isEqualToString:@"IN PROCESS"] || [status isEqualToString:@"in process"])
        
//        if ([status isEqualToString:@"0"])
//        {
//            btn_review_pay_chck.enabled=NO;
//        }
//        else
//        {
//            btn_review_pay_chck.enabled=YES;
//        }
        
        
        //        if (netAvailable)
        //        {
        
        // btn_review_pay_chck.enabled=NO;
        
            NSLog(@"%@",gblclass.bill_type_chck);
        if ([gblclass.bill_type_chck isEqualToString:@"1"])
        {
            vw_postpaid.hidden = NO;
            txt_comment.hidden = YES;
            txt_amnt.hidden = YES;
            lbl_note.hidden = YES;
        }
        else
        {
            vw_postpaid.hidden = YES;
            txt_comment.hidden = NO;
            txt_amnt.hidden = NO;
            lbl_note.hidden = NO;
        }
        
        [self Detail_Utility_Bill_load_data:@""];
        //}
    }
    
  //  [self Detail_Utility_Bill_load_data:@""];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    vw_acct.layer.cornerRadius = 5;
    vw_acct.layer.masksToBounds = YES;
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
}


-(void) Detail_Utility_Bill_load_data:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
     
    arr_get_bill=[[NSMutableArray alloc] init];
    NSString *uuid = [[NSUUID UUID] UUIDString];
//    NSLog(@"%@",gblclass.arr_bill_load_all);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                                   [encrypt encrypt_Data:gblclass.user_id],
                                                                   [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                   [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:str_tt_id],[encrypt encrypt_Data:str_access_key],[encrypt encrypt_Data:lbl_compny_name],[encrypt encrypt_Data:lbl_customer_id],[encrypt encrypt_Data:
                                                                                                                                                                                                                                                                                                                    service_code],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token], nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",
                                @"strSessionId",
                                @"IP",
                                @"strTT_ID",
                                @"access_key",
                                @"lblCompanyName",
                                @"lblCustomerID",
                                @"serviceCode",
                                @"Device_ID",
                                @"Token", nil]];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"DetailBillLoadDataUAE" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //           NSArray *arr = (NSArray *)responseObject;
        
        
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
        
//        NSString* aa;
//        aa=@"1";
//        if ([aa isEqualToString:@"1"])
              
//               if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
//              {
//                  chk_ssl=@"logout";
//                  logout_chck = @"1";
//                  [hud hideAnimated:YES];
//                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//                  return;
//
//              }
        
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [hud hideAnimated:YES];
//                  lbl_curr_balc.text
                  curr_balance = [dic objectForKey:@"strlbltxtAmount"];
                  //[NSString stringWithFormat:@"Current Balance %@",
                  
                  str_bill_id = [dic objectForKey:@"strBillId"];
                  
                  status = [NSString stringWithFormat:@"%@",[dic objectForKey:@"btnPay"]];
                  
                  min_balc = [NSString stringWithFormat:@"%@",[dic objectForKey:@"strMinPayable"]];
                  max_balc = [NSString stringWithFormat:@"%@",[dic objectForKey:@"strMaxPayable"]];
                  
//                  if ([min_balc isEqualToString:@""])
//                  {
//                      min_balc = @"0";
//                  }
//
//                  if ([max_balc isEqualToString:@""])
//                  {
//                      max_balc = @"0";
//                  }
                  
                  lbl_note.text = [NSString stringWithFormat:@"Please enter a minimum amount of AED %@",min_balc];
                  
                      
                  if ([curr_balance isEqualToString:@""] || [curr_balance isEqualToString:@" "] || curr_balance == (NSString *) [NSNull null] || [curr_balance isEqualToString:@"N/A"])
                  {
                      lbl_curr_balc.hidden = YES;
                  }
                  else
                  {
//                      // Input
//                      NSString *originalString = curr_balance;
//                      // Intermediate
//                      NSString *numberString;
//                      NSScanner *scanner = [NSScanner scannerWithString:originalString];
//                      NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
//                      // Throw away characters before the first number.
//                      [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
//                      // Collect numbers.
//                      [scanner scanCharactersFromSet:numbers intoString:&numberString];
//                      // Result.
//                      int number = [numberString integerValue];
//                      curr_blac_amnt = [NSString stringWithFormat:@"%d",number];
//                      NSLog(@"%d",number);
                      
                  //  NSLog(@"%@",[self extractNumberFromText:curr_balance]);
                      
                      lbl_curr_balc.text = [NSString stringWithFormat:@"Current Balance %@ %@",gblclass.base_currency,curr_balance];
                  }
                  
                  if ([status isEqualToString:@"0"])
                  {
                      btn_review_pay_chck.enabled=NO;
                  }
                  else
                  {
                      btn_review_pay_chck.enabled=YES;
                  }
                  
                  // btn_review_pay_chck.enabled=YES;
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"kjghkdfhghdfhkgkh"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
        
              [hud hideAnimated:YES];
              [self custom_alert:Call_center_msg :@"0"];
//              [self custom_alert:@"Retry" :@"0"];
              
          }];
}
 
- (NSString *)extractNumberFromText:(NSString *)text
{
  NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
  return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_combo_to:(id)sender
{
    
}

-(IBAction)btn_submit:(id)sender
{
    
    //    if (PAYMENT_STATUS== "U")
    //    {
    //
    //        if (IS_PAYBLE == "0")
    //        {
    //            “Can’t Pay”
    //        }
    //
    //        else if (PAYABLE_AMOUNT > 0){
    //            “Payable”
    //        }
    //
    //        Else
    //        {
    //            “Can’t Pay”
    //        }
    //    }
    //
    //    Else
    //    {
    //        “Can’t Pay”
    //    }
    
    
    [hud showAnimated:YES];
    
    if ([txt_combo_bill_names.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Bill" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if([txt_comment.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    PAYMENT_STATUS=@"U";
    if ([PAYMENT_STATUS isEqualToString:@"U"])
    {
        IS_PAYBLE_1=0;
        if ([IS_PAYBLE_1  isEqual: @"0"])
        {
            //NSLog(@"return");
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Can’t Pay" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if (IS_PAYBLE_1 > 0 ) //@"0"
        {
            //pay
            //NSLog(@"Pay");
            
            if (lbl_t_pin.hidden==YES && txt_t_pin.hidden==YES)
            {
                lbl_t_pin.hidden=NO;
                txt_t_pin.hidden=NO;
                
            }
            else if ([txt_t_pin.text isEqualToString:@""])
            {
                alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                
                [self presentViewController:alert animated:YES completion:nil];
                [hud hideAnimated:YES];
                return;
                
            }
            else
            {
                [self Pay_Bill:@""];
            }
            
        }
        else
        {
            //cannt pay
            //NSLog(@"cannt pay");
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Can’t Pay" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        //cantt pay
        //NSLog(@"cannt pay");
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Can’t Pay" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [hud hideAnimated:YES];
}

-(IBAction)btn_Utility_bill_names:(id)sender
{
    if (table_bill.isHidden==YES)
    {
        vw_bill.hidden=NO;
        table_bill.hidden=NO;
        vw_from.hidden=YES;
        table_from.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        table_bill.hidden=YES;
        vw_bill.hidden=YES;
    }
    
}


-(IBAction)btn_combo_frm:(id)sender
{
    
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    
    if (table_from.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
}

-(IBAction)btn_bill_names:(id)sender
{
    if (table_to.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=NO;
        table_to.hidden=NO;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        [table_to bringSubviewToFront:self.view];
    }
    else
    {
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView==table_from)
    {
        
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
    else
        
    {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            return [search count];
        }
        else
        {
            return [arr_get_bill_load count];
        }
    }
    else if(tableView==table_CC)
    {
        return [arr_cc count];
    }
    else
    {
        return [arr_bill_names count];
    }
    
    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        //        cell.textLabel.text = [split objectAtIndex:0];
        //        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
     //   label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
      //  label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        NSString *numberString = [NSString stringWithFormat:@"%@", [split objectAtIndex:6]];
        NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:number]];
        label.textColor=[UIColor whiteColor];
      //  label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        //        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        //        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        //        attach1.bounds = CGRectMake(10, 8, 20, 10);
        //        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        //
        //        NSString* balc;
        //
        //        if ([split objectAtIndex:6]==0)
        //        {
        //            balc=@"0";
        //        }
        //        else
        //        {
        //            balc=[split objectAtIndex:6];
        //        }
        //
        //
        //        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        //        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:16]};
        //        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        //
        //        [mutableAttriStr1 appendAttributedString:imageStr1];
        //       // cell.attributedText = mutableAttriStr1;
        //
        //        [cell.contentView addSubview:[NSString stringWithFormat:@"%@",mutableAttriStr1]];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [split_bill objectAtIndex:3];
         //   cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        else
        {
            split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [split_bill objectAtIndex:3];
          //  cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
    }
    else if(tableView==table_CC)
    {
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [arr_cc objectAtIndex:indexPath.row];
        label.textColor=[UIColor whiteColor];
        //label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
    }
    else
    {
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        cell.textLabel.text = [split_bill objectAtIndex:0];
      //  cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    table_CC.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [self checkinternet];
    if (netAvailable)
    {
        
        [table_from reloadData];
        if (tableView==table_from)
        {
            split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            txt_combo_frm.text=[split objectAtIndex:0];
            Payfrm_selected_item_text=[split objectAtIndex:0];
            Payfrm_selected_item_value=[split objectAtIndex:1];
            payfrom_acct_id=[split objectAtIndex:2];
            _lbl_acct_frm.text=[split objectAtIndex:1];
            
            NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
            attach1.image = [UIImage imageNamed:@"Pkr.png"];
            attach1.bounds = CGRectMake(10, 8, 20, 10);
            NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
            
            //   NSString* balc;
            
            if ([split objectAtIndex:6]==0)
            {
                balc=0;
            }
            else
            {
                balc=[split objectAtIndex:6];
            }
            
            
            NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
            NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
            [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
            
            [mutableAttriStr1 appendAttributedString:imageStr1];
            // _lbl_balance.attributedText = mutableAttriStr1;
            
            lbl_balc=balc;
            
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
            myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
            
            
            table_from.hidden=YES;
            vw_from.hidden=YES;
            vw_table.hidden=YES;
            
        }
        else if(tableView==table_to)
        {
            if ([search_flag isEqualToString:@"1"])
            {
                split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            }
            else
            {
                split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            }
            
            str_tt_name=[split_bill objectAtIndex:0];
            str_tt_id=[split_bill objectAtIndex:1];
            lbl_customer_id=[split_bill objectAtIndex:2];
            str_due_datt=[split_bill objectAtIndex:4];
            str_amnt=[split_bill objectAtIndex:5];
            status=[split_bill objectAtIndex:6];
            str_bill_id=[split_bill objectAtIndex:7];
            str_access_key=[split_bill objectAtIndex:8];
            str_regt_consumer_id=[split_bill objectAtIndex:9];
            lbl_compny_name=[split_bill objectAtIndex:10];
            str_TT_accesskey=[split_bill objectAtIndex:12];
            lbl_customer_nick=[split_bill objectAtIndex:3];
            PAYMENT_STATUS=[split_bill objectAtIndex:13];
            IS_PAYBLE_1=[split_bill objectAtIndex:14];
            
            lbl_amnts.text=[NSString stringWithFormat:@"%@", [numberFormatter stringFromNumber:[NSDecimalNumber decimalNumberWithString:[split_bill objectAtIndex:5]]]];
            lbl_due_dat.text=[split_bill objectAtIndex:4];
            lbl_status.text=[split_bill objectAtIndex:6];
            txt_combo_bill_names.text=[split_bill objectAtIndex:3];
            
            search_flag=@"0";
            [table_to reloadData];
            
            [gblclass.arr_bill_elements removeAllObjects];
            if ([[split_bill objectAtIndex:12] isEqualToString:@"OB"])
            {
                [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
                
                //NSLog(@" arr %@",gblclass.arr_bill_elements);
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"OB_bill"];
                [self presentViewController:vc animated:NO completion:nil];
            }
            else if ([[split_bill objectAtIndex:12] isEqualToString:@"ISP"])
            {
                [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
                
                //NSLog(@" arr %@",gblclass.arr_bill_elements);
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                [self presentViewController:vc animated:NO completion:nil];
            }
            else if ([[split_bill objectAtIndex:12] isEqualToString:@"UBLBP"])
            {
                [gblclass.arr_bill_elements addObjectsFromArray:split_bill];
                
                //NSLog(@" arr %@",gblclass.arr_bill_elements);
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"UBP_bill"];
                [self presentViewController:vc animated:NO completion:nil];
            }
            
            vw_to.hidden=YES;
            table_to.hidden=YES;
            txt_t_pin.text=@"";
            txt_comment.text=@"";
        }
        else if(tableView==table_CC)
        {
            [table_CC reloadData];
            txt_Combo_box.text = [arr_cc objectAtIndex:indexPath.row];
            
            if ([txt_Combo_box.text isEqualToString:[arr_cc objectAtIndex:0]])
            {
                txt_cc_amnt.enabled = NO;
                txt_Combo_box.text = [arr_cc objectAtIndex:0];
                
                if ([curr_balance rangeOfString:@"("].location == NSNotFound)
                {
                    txt_cc_amnt.text = curr_balance;
                }
                else
                {
                    NSString *currr = [curr_balance stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    NSString *currr1 = [currr stringByReplacingOccurrencesOfString:@")" withString:@""];
                    
                    txt_cc_amnt.text =  currr1;
                }
            }
            else
            {
                txt_Combo_box.text = [arr_cc objectAtIndex:1];
                txt_cc_amnt.enabled = YES;
                txt_cc_amnt.text = @"";
            }
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
            myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
            
            vw_to.hidden=YES;
            table_CC.hidden=YES;
            txt_t_pin.text=@"";
          //txt_Combo_box.text=@"";
            vw_table.hidden = YES;
            
        }
        else
        {
            split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            gblclass.pay_combobill_name=[split_bill objectAtIndex:0];
            
            switch (indexPath.row)
            {
                case 0:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"]; //utility_bill   //bill_all
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                case 1:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_voucher"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 2:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"online_shopping"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 3:
                    
                     
                    
                    break;
                    
                case 4:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_within_acct"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 5:
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_other_acct"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                case 6:
                    
                    //                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    //                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                    //    [self presentViewController:vc animated:NO completion:nil];
                    
                    break;
                    
                default: break;
                    
            }
            
            vw_bill.hidden=YES;
            table_bill.hidden=YES;
            vw_to.hidden=YES;
            table_to.hidden=YES;
            txt_t_pin.text=@"";
            txt_comment.text=@"";
            
        }
        
    }
}

-(void) Get_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_get_bill=[[NSMutableArray alloc] init];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,@"ISP,OB,UBP,UBLBP",gblclass.M3sessionid,[GlobalStaticClass getPublicIp],@"abcd", nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",@"strTcAccessKey",@"strSessionId",@"IP",@"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              arr_bill_UBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              arr_bill_OB= [(NSDictionary *)[dic objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
              arr_bill_ISP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
              arr_bill_UBLBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              
              a=[[NSMutableArray alloc] init];
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              // For UBP :::::
              
              for (dic in arr_bill_UBP) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS2=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS2.length==0 || [PAYMENT_STATUS2 isEqualToString:@" "] || [PAYMENT_STATUS2 isEqualToString:nil])
                  {
                      PAYMENT_STATUS2=@"N/A";
                      [a addObject:PAYMENT_STATUS2];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS2];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              // UBP END :::
              
              
              
              // For ISP :::::
              
              for (dic in arr_bill_ISP) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS4=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS4.length==0 || [PAYMENT_STATUS4 isEqualToString:@" "] || [PAYMENT_STATUS4 isEqualToString:nil])
                  {
                      PAYMENT_STATUS4=@"N/A";
                      [a addObject:PAYMENT_STATUS4];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS4];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              // ISP END ::::
              
              
              
              
              // For OB :::::
              
              for (dic in arr_bill_OB) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS1=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS1.length==0 || [PAYMENT_STATUS1 isEqualToString:@" "] || [PAYMENT_STATUS1 isEqualToString:nil])
                  {
                      PAYMENT_STATUS1=@"N/A";
                      [a addObject:PAYMENT_STATUS1];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS1];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              
              
              // FOR OB END ::::
              
              
              
              // For UBLBP :::::
              
              for (dic in arr_bill_UBLBP) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC2=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC2.length==0 || [PAYMENT_STATUS_DESC2 isEqualToString:@" "] || [PAYMENT_STATUS_DESC2 isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC2=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC2];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC2];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS2=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS2.length==0 || [PAYMENT_STATUS2 isEqualToString:@" "] || [PAYMENT_STATUS2 isEqualToString:nil])
                  {
                      PAYMENT_STATUS2=@"N/A";
                      [a addObject:PAYMENT_STATUS2];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              
              // For UBLBP :::
              
              [hud hideAnimated:YES];
              [table_to reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}



-(void) Pay_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_get_bill=[[NSMutableArray alloc] init];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //NSLog(@"%@", gblclass.user_id);
    //NSLog(@"%@", payfrom_acct_id);
    //NSLog(@"%@", Payfrm_selected_item_text);
    //NSLog(@"%@", Payfrm_selected_item_value);
    //NSLog(@"%@", str_amnt);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_compny_name);
    //NSLog(@"%@", txt_comment.text);
    //NSLog(@"%@", str_access_key);
    //NSLog(@"%@", str_Tc_access_key);//str_TT_accesskey
    //NSLog(@"%@", gblclass.base_currency);
    //NSLog(@"%@", txt_t_pin.text);
    //NSLog(@"%@", str_TT_accesskey);
    //NSLog(@"%@", str_tt_id);
    //NSLog(@"%@", str_regt_consumer_id);
    //NSLog(@"%@", str_bill_id);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_customer_nick);
    //NSLog(@"%@", uuid);
    
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,
                                                                   [GlobalStaticClass getPublicIp],
                                                                   payfrom_acct_id,
                                                                   Payfrm_selected_item_text,
                                                                   Payfrm_selected_item_value,
                                                                   str_amnt,lbl_customer_id,
                                                                   lbl_compny_name,
                                                                   txt_comment.text,
                                                                   str_access_key,
                                                                   @"",
                                                                   @"",
                                                                   gblclass.base_currency,
                                                                   txt_t_pin.text,
                                                                   str_TT_accesskey,
                                                                   str_tt_id,
                                                                   str_regt_consumer_id,
                                                                   str_bill_id,
                                                                   lbl_customer_id,
                                                                   lbl_customer_nick,
                                                                   uuid, nil] forKeys:
                               
                               [NSArray arrayWithObjects:@"strUserId",
                                @"strSessionId",
                                @"IP",
                                @"PayFromAccountID",
                                @"cmbPayFromSelectedItemText",
                                @"cmbPayFromSelectedItemValue",
                                @"strtxtAmount",
                                @"strlblCustomerID",
                                @"strlblCompanyName",
                                @"strtxtComments",
                                @"strAccessKey",
                                @"strTCAccessKey",
                                @"strlblTransactionType",
                                @"strCCY",
                                @"strtxtPin",
                                @"strTTAccessKey",
                                @"strTT_ID",
                                @"strRegisteredConsumersId",
                                @"strBillId",
                                @"strlbCustomerId",
                                @"strlblConsumerNick",
                                @"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //          NSError *error;
              //          NSArray *arr = (NSArray *)responseObject;
              
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [txt_comment resignFirstResponder];
    
    vw_down_chck  = @"1";
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_from.hidden=YES;
    vw_to.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    vw_table.hidden=YES;
    
    
    [txt_combo_bill_names resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_cc_comment resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_cc_amnt resignFirstResponder];
    
    // [self keyboardWillHide];
}


#define kOFFSET_FOR_KEYBOARD 45.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        //[self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
//        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //   //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [search  removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", substring];
    
    search= [NSMutableArray arrayWithArray: [arr_get_bill_load filteredArrayUsingPredicate:resultPredicate]];
    
    //    }
    
    
    table_to.hidden=NO;
    [table_to reloadData];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
    //    if(string.length > 0)
    //    {
    
    //NSLog(@"%@",textField);
    
    if ([textField isEqual:txt_comment])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;
            }
        }
        //}
        
        
        //    if ([textField isEqual:txt_comment])
        //    {
        //        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789  "] invertedSet];
        //
        //        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        //        {
        //            //NSLog(@"NO SPECIAL CHARACTER");
        //            //return [textField.text stringByReplacingCharactersInRange:range withString:set];
        //        }
        //        else
        //        {
        //            //NSLog(@"HAS SPECIAL CHARACTER");
        //            textField.text=@"";
        //
        //            return 0;
        //        }
        
        
        
        //        //NSLog(@"%lu",txt_comment.text.length);
        //
        //        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        //        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        //
        //        [textField addTarget:self
        //                      action:@selector(textFieldDidChange:)
        //            forControlEvents:UIControlEventEditingChanged];
        //
        //        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        //        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    if ([textField isEqual:txt_cc_comment])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;
            }
        }
    }
    else if ([textField isEqual:txt_amnt])
    {
        MAX_DIGITS=15;
        
        //NSLog(@"%lu",txt_amnt_without.text.length);
        
//        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
//
//        [textField addTarget:self
//                      action:@selector(textFieldDidChange:)
//            forControlEvents:UIControlEventEditingChanged];
//
//        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
           NSString *expression = @"^[0-9]*((\.|,)[0-9]{0,2})?$";
           NSError *error = nil;
           NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
           NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
           
        if ([newString length]<= MAX_DIGITS)
        {
            return numberOfMatches != 0;
        }
        else
        {
            return 0;
        }
        
        
    }
    else if ([textField isEqual:txt_cc_amnt])
       {
           MAX_DIGITS=15;
           
           //NSLog(@"%lu",txt_amnt_without.text.length);
           
           
//           NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
//           for (int i = 0; i < [string length]; i++)
//           {
//               unichar c = [string characterAtIndex:i];
//               if (![myCharSet characterIsMember:c])
//               {
//                   return NO;
//               }
//               else
//               {
//                   return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 25;
//               }
//           }
           
           
           
           NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
              NSString *expression = @"^[0-9]*((\.|,)[0-9]{0,2})?$";
              NSError *error = nil;
              NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
              NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
           
           if ([newString length]<= MAX_DIGITS)
           {
               return numberOfMatches != 0;
           }
           else
           {
               return 0;
           }
           
           
           
           
//           NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
//           NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
//
//           [textField addTarget:self
//                         action:@selector(textFieldDidChange:)
//               forControlEvents:UIControlEventEditingChanged];
//
//           stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
//           return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
       }
    else if ([textField isEqual:txt_combo_bill_names])
    {
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            
            search_flag=@"1";
            [self   searchAutocompleteEntriesWithSubstring:txt_combo_bill_names.text];
            
            //return [textField.text stringByReplacingCharactersInRange:range withString:set];
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    
    //NSLog(@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS);
    
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    
}

-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    if ([theTextField isEqual:txt_amnt])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        //        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        //        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        //        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
        
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_amnt.text=[nf stringFromNumber:myNumber];
    }
    else if ([theTextField isEqual:txt_cc_amnt])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterNoStyle];
        NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithDouble:[textFieldText1 doubleValue]]];
        
   
        NSNumber *myNumber = [NSNumber numberWithDouble:[textFieldText1 doubleValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        [nf setFormatterBehavior:NSNumberFormatterBehavior10_4];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
        [nf setGroupingSeparator:@""];
        [nf setDecimalSeparator:@"."];
        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_cc_amnt.text=[nf stringFromNumber:myNumber];
    }
    
    
    //NSLog(@"text changed: %@", theTextField.text);
    
    //    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    //    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}

-(IBAction)btn_combo:(id)sender
{
    if (table_CC.isHidden==YES)
    {
        table_CC.hidden = NO;
        vw_table.hidden = NO;
        
//        vw_bill.hidden=YES;
//        table_bill.hidden=YES;
//        vw_from.hidden=NO;
//        table_from.hidden=NO;
//        table_to.hidden=YES;
//        vw_to.hidden=YES;
//        vw_table.hidden=NO;
    }
    else
    {
        table_CC.hidden = YES;
        vw_table.hidden = YES;
        
//        table_from.hidden=YES;
//        vw_from.hidden=YES;
//        vw_table.hidden=YES;
    }
}


-(IBAction)btn_review:(id)sender
{
    
    @try {
        
        [hud showAnimated:YES];
        double frm_balc,to_balc;
        double amt;
        NSString* comnt;
         
        
//        NSLog(@"%@",split_bill);
//        NSLog(@"%@",txt_amnt.text);
        NSString* txt_amnt2;
        
         
//        NSLog(@"%@",[split_bill objectAtIndex:4]);
        int aa = [txt_amnt.text intValue];
    
        if ([[split_bill objectAtIndex:4] isEqualToString:@"topup"] || [[split_bill objectAtIndex:4] isEqualToString:@"Top Up"] || [[split_bill objectAtIndex:4] isEqualToString:@"al wasel topup"] || [[split_bill objectAtIndex:4] isEqualToString:@"Al Wasel Top Up"])
        {
            
            
            txt_amnt2 = [txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            amt = [txt_amnt2 doubleValue];
            
            if (aa >= [max_balc intValue])
            {
                [hud hideAnimated:YES];
                [hud hideAnimated:YES];
                 [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            else if (amt < [min_balc intValue])
            {
                [hud hideAnimated:YES];
                [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            
            if (amt > [max_balc intValue])
            {
                [hud hideAnimated:YES];
                [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            
            
            if( aa % 10 == 0 )
            {
//                NSLog(@"Zero");
            }
            else
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please enter an amount in multiples of 10" :@"0"];
//                NSLog(@"Not Zero");
                return;
            }
            
//            else
//            {
//                [hud hideAnimated:YES];
//                [self custom_alert:@"Please enter an amount in multiples of 10" :@"0"];
//                NSLog(@"Not Zero");
//                return;
//            }
            
        }
         
     //   [self amt_calc:[txt_amnt.text intValue]];
         
        if ([gblclass.bill_type_chck isEqualToString:@"1"])
        {
            
            if ([txt_Combo_box.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please select payment type" :@""];
                return;
            }
            
            if ([txt_cc_comment.text isEqualToString:@""])
            {
                txt_cc_comment.text=@"";
                comnt = @"";
            }
            else
            {
                comnt = txt_cc_comment.text;
            }
            
            txt_amnt2 = [txt_cc_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            amt = [txt_amnt2 doubleValue];
            
            if (![min_balc  isEqual: @""])
            {
            
            if (amt < [min_balc intValue])
            {
                [hud hideAnimated:YES];
                [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            
            if (amt > [max_balc intValue])
            {
                [hud hideAnimated:YES];
                [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            
            }
                       
            
             if ([txt_Combo_box.text isEqualToString:[arr_cc objectAtIndex:0]])
             {
                 
             }
            else
            {
                if ([txt_cc_amnt.text isEqualToString:@""] || [txt_cc_amnt.text isEqualToString:@"0"])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Please enter bill amount" :@"0"];
                    return;
                }
                
              //NSString* lbl_balced =  lbl_balc;
                txt_amnt2 = [txt_cc_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
                frm_balc=[lbl_balc doubleValue];
                to_balc=[txt_amnt2 doubleValue];
            }
        }
        else
        {
            if ([txt_comment.text isEqualToString:@""])
            {
                txt_comment.text=@"";
                comnt = @"";
            }
            else
            {
                comnt = txt_comment.text;
            }
            
            if ([txt_amnt.text isEqualToString:@""] || [txt_amnt.text isEqualToString:@"0"])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please enter bill amount" :@"0"];
                return;
            }
            
          //NSString* lbl_balced =  lbl_balc;
            txt_amnt2 = [txt_amnt.text stringByReplacingOccurrencesOfString:@"," withString:@""];
            frm_balc=[lbl_balc doubleValue];
            to_balc=[txt_amnt2 doubleValue];
            
            if (aa >= [max_balc intValue])
            {
                [hud hideAnimated:YES];
                [hud hideAnimated:YES];
                 [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
            else if (amt < [min_balc intValue])
            {
                [hud hideAnimated:YES];
                [self custom_alert:[NSString stringWithFormat:@"Amount must be between %@ - %@",min_balc,max_balc] :@"0"];
                return;
            }
        }
        
        
        if (to_balc>frm_balc)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Available balance is less then bill amount" :@"0"];
            return;
        }
        
        [gblclass.arr_values removeAllObjects];
        [gblclass.arr_review_withIn_myAcct removeAllObjects];
        
        [gblclass.arr_review_withIn_myAcct addObject:lbl_company.text];
        [gblclass.arr_review_withIn_myAcct addObject:lbl_customer_id];
        [gblclass.arr_review_withIn_myAcct addObject:payfrom_acct_id];
        [gblclass.arr_review_withIn_myAcct addObject:txt_combo_frm.text];
        [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
        [gblclass.arr_review_withIn_myAcct addObject:txt_amnt2]; // txt_amnt.text
        [gblclass.arr_review_withIn_myAcct addObject:comnt];
        
        
        gblclass.check_review_acct_type=@"Utility_bill";
        gblclass.reviewCommentFieldText = txt_comment.placeholder;
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:payfrom_acct_id];
        [gblclass.arr_values addObject:Payfrm_selected_item_text];
        [gblclass.arr_values addObject:Payfrm_selected_item_value];
        [gblclass.arr_values addObject:txt_amnt2];
        [gblclass.arr_values addObject:lbl_customer_id];
        [gblclass.arr_values addObject:lbl_compny_name];
        [gblclass.arr_values addObject:comnt];  //txt_comment.text
        [gblclass.arr_values addObject:str_access_key];
        [gblclass.arr_values addObject:str_Tc_access_key];
        [gblclass.arr_values addObject:lbl_transaction_type];//@"UTILITY BILL"];
        [gblclass.arr_values addObject:gblclass.base_currency];
        [gblclass.arr_values addObject:@"1111"];
        [gblclass.arr_values addObject:str_TT_accesskey];
        [gblclass.arr_values addObject:str_tt_id];
        [gblclass.arr_values addObject:str_regt_consumer_id];
        [gblclass.arr_values addObject:str_bill_id];
        [gblclass.arr_values addObject:str_bill_id]; //lbl_customer_id
        [gblclass.arr_values addObject:lbl_nick.text];
        
        if ([vw_down_chck isEqualToString:@""])
        {
            [self keyboardWillHide];
        }
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"review";
            [self SSL_Call];
        }
        
      //  [self Pay_Bill_Utility:@""];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void) Pay_Bill_Utility:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //NSString *uuid = [[NSUUID UUID] UUIDString];
    //str_TT_accesskey=str_access_key;
    
//    NSLog(@"%@",gblclass.arr_values);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:15]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],//str_access_key
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"PayBillSubmitClick" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
        //           NSError *error;
        //           NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              gblclass.Is_Otp_Required = [NSString stringWithFormat:@"%@",[dic objectForKey:@"IsOtpRequired"]];
              gblclass.rev_transaction_type = [NSString stringWithFormat:@"%@",[dic objectForKey:@"lblTransactionType"]];
              NSString* response = [NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
        
              [hud hideAnimated:YES];
              
//          NSString* aa;
//              aa=@"1";
//              if ([aa isEqualToString:@"1"])
                    
//                     if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
//                    {
//                        chk_ssl=@"logout";
//                        [hud hideAnimated:YES];
//                        [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//                        return;
//
//                    }
        
        
            if ([response isEqualToString:@"-78"] || [response isEqualToString:@"-79"])
            {
                [hud hideAnimated:YES];
                logout_chck=@"1";
                [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                return ;
            
             }
        //if ([[dic objectForKey:@"Response"] integerValue]==0)
             else  if ([response isEqualToString:@"0"])
              {
                  
                  [hud hideAnimated:YES];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //    [self pay_Bill_Confirm_Submit:@""];
                  
              }//gblclass.Is_Otp_Required
              //else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==1)
              else if ([gblclass.Is_Otp_Required isEqualToString:@"1"])
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
              }
             // else if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
              else if ([gblclass.Is_Otp_Required isEqualToString:@"-1"])
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[NSString stringWithFormat:@"%@",[dic objectForKey:@"strReturnMessage"]]] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}


-(IBAction)btn_Edit_Bill:(id)sender
{
    
    gblclass.Edit_bill_type=@"Utility";
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"edit_bill"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_Delete_Bill:(id)sender
{
    UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                     message:@"Are your sure, you want to delete this bill?"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert3 show];
    
    // [self Delete_Bills:@""];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        if ([logout_chck isEqualToString:@"1"])
        {
            logout_chck=@"0";
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
            //[self mob_App_Logout:@""];
        }
        
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        [hud showAnimated:YES];
        
        [self Delete_Bills:@""];
         
    }
}



-(void) Delete_Bills:(NSString *)strIndustry
{
    
    @try {
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:str_tt_id],
                                                                       [encrypt encrypt_Data:lbl_customer_id],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                       @"tt_id",@"consumerNo",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strSessionId",
                                                                       @"IP", nil]];
        
        
        //NSLog(@"%@",dictparam);
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"DeleteBills" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  
//                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
//                  {
//
//                      [hud hideAnimated:YES];
//                      logout_chck=@"1";
//                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//
//                      return ;
//
//                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      
                      //  [self custom_alert:@"Record Delete Successfully" :@"1"];
                      
                      UIAlertView *alert4 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:@"Bill deleted successfully"
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert4 show];
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
            
                  [hud hideAnimated:YES];
                  [self custom_alert:[error localizedDescription] :@"0"];
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES]; 
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


//-(IBAction)btn_logout:(id)sender
//{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                    message:Logout
//                                                   delegate:self
//                                          cancelButtonTitle:@"Cancel"
//                                          otherButtonTitles:@"OK", nil];
//
//    [alert show];
//}




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ( [chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            [self Pay_Bill_Utility:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ( [chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        [self Pay_Bill_Utility:@""];
        
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) amt_calc:(int)aa
{
//    NSLog(@"%d",aa);
    
    if( aa % 10 == 0 )
     {
//         NSLog(@"Zero");
       //return a / b + 1;
     }
    else
    {
//        NSLog(@"Not Zero");
    }
}

@end

