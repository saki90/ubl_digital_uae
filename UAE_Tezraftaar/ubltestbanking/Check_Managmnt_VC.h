//
//  Check_Managmnt_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Check_Managmnt_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table1;
    IBOutlet UITableView* table2;
}


@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;



@property(nonatomic,strong) IBOutlet UITextField* txt_check_name;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

- (IBAction)segmentSwitch:(id)sender;
- (IBAction)indexChanged:(UISegmentedControl *)sender;


@end
