//
//  Ubl_fund_invstment_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ubl_fund_invstment_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
}



@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;

@property (nonatomic, strong) IBOutlet UILabel *act_name;
@property (nonatomic, strong) IBOutlet UILabel *act_num;
@property (nonatomic, strong) IBOutlet UILabel *act_type;
@property (nonatomic, strong) IBOutlet UILabel *act_balcdate;
@property (nonatomic, strong) IBOutlet UILabel *act_portfoliobal;

@property (nonatomic, strong) IBOutlet UILabel *act_totalbal;


@property (nonatomic, strong) IBOutlet UILabel *upper_currency;
@property (nonatomic, strong) IBOutlet UILabel *upper_amount;



@end
