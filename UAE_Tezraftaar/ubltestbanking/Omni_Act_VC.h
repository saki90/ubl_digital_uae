//
//  Omni_Act_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 04/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Omni_Act_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
}
@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

@property (nonatomic, strong) IBOutlet UILabel *act_name;
@property (nonatomic, strong) IBOutlet UILabel *act_num;
//@property (nonatomic, strong) IBOutlet UILabel *act_branch;
@property (nonatomic, strong) IBOutlet UILabel *avl_balc;
@property (nonatomic, strong) IBOutlet UILabel *total_bal;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;

@property (nonatomic, strong) IBOutlet UILabel *upper_currency;
@property (nonatomic, strong) IBOutlet UILabel *upper_amount;



@end
