//
//  Features_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Faq_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Faq_detail.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "Encrypt.h"
#import "Peekaboo.h"


@interface Faq_VC ()
{
    MBProgressHUD* hud;
//    UIWebView* web;
    NSString* data;
    
    UIAlertController* alert;
    UILabel* label;
    NSMutableArray* arr_heading;
    UIStoryboard *mainStoryboard;
       UIViewController *vc;
    
    UIStoryboard *storyboard;
//    UIViewController *vc;
    GlobalStaticClass *gblclass;
    NSString* chk_ssl;
    NSMutableArray*  a;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Faq_VC
static UIView* loadingView = nil;
//@synthesize webView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    [self.view addSubview:hud];
    gblclass =  [GlobalStaticClass getInstance];
    ssl_count = @"0";
    
    
    [self checkinternet];
    if (netAvailable)
    {
        
        NSURL* url = [[NSBundle mainBundle] URLForResource:@"FAQ" withExtension:@"HTML"];   //@"faq"
        
//        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
    
    arr_heading=[[NSMutableArray alloc] init];
    
    [arr_heading addObject:@"What is UBL Digital Banking App?"];
    [arr_heading addObject:@"Why use UBL Digital Banking App?"];
    [arr_heading addObject:@"Is this app available for Business / Non Individual account holders?"];
    [arr_heading addObject:@"What can I do with UBL Digital Banking App?"];
    [arr_heading addObject:@"What is the installation procedure?"];
    [arr_heading addObject:@"How to login?"];
    [arr_heading addObject:@"How secure is UBL Digital Banking App?"];
    [arr_heading addObject:@"What security precautions should I take when using the Digital Banking App?"];
    [arr_heading addObject:@"How much does it cost to purchase this App?"];
    [arr_heading addObject:@"I have two mobile phones can I have access to Digital Banking App for my account on both my phones?"];
    [arr_heading addObject:@"What if I change my phone or if I lost my phone?"];
    [arr_heading addObject:@"What do I do if I have forgotten my Login password?"];
    [arr_heading addObject:@"Can I access Digital Banking App when I am outside UAE?"];
    [arr_heading addObject:@"Where should I contact in case of further queries?"];
    
    [table reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    [self slide_left];
    
   NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
    stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"2"])
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"]; //new_signup
        [self presentViewController:vc animated:NO completion:nil];
    }
    else if ([first_time_chk isEqualToString:@"0"])
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"]; //new_signup
        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"]; //new_signup
        [self presentViewController:vc animated:NO completion:nil];
    }
     
    
   // [self dismissViewControllerAnimated:NO completion:nil];
}

//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    [hud hideAnimated:YES];
//}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_heading count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    //For Header ::
    
    label=(UILabel*)[cell viewWithTag:2];
    label.text=[arr_heading objectAtIndex:indexPath.row];
    //  label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    
    if (indexPath.row % 2)
    {
        [cell setBackgroundColor:[UIColor colorWithRed:229/255.0f green:233/255.0f blue:242/255.0f alpha:1.0f]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    
    gblclass.faq_index=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"faq_desc"];
    [self presentViewController:vc animated:NO completion:nil];
    
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}


-(IBAction)btn_feature:(id)sender
{
    
    
    NSString*  first_time_chk = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"enable_touch"]; //first_time
    //
    
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
    }
    else
    {
        
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
    
    //     [self slide_right];
    //
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"feature"]; //feature  receive_qr_login
    //     [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    
    // gblclass.tab_bar_login_pw_check=@"login";
    
    [self custom_alert:Offer_msg :@""];
    
// [self peekabooSDK:@"deals"];
//   [self peekabooSDK:@{@"type": @"deals",}];

   
    
    
    
    //     [self slide_right];
    //
    //
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#004681",
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#efefef",
    //       }
    //                          launchOptions    : nil];
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
    
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"offer"];
    //     [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.forget_pw_btn_click=@"0";
    gblclass.forgot_pw_click = @"1";
//    [self peekabooSDK:@"locator"];
//      [self peekabooSDK:@{@"type": @"locator",}];
    
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//
//
////    new_signup  debit_card_optn
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];

}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        //        [self.connection cancel];
    }
    
    
    //    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}



///////////************************** SSL PINNING END ******************************////////////////



-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllow" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  a =[[NSMutableArray alloc] init];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


//- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"United Arab Emirates",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
//}


- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    
    //saki
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}




@end

