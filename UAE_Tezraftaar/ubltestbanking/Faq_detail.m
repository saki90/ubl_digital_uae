//
//  Faq_detail.m
//  ubltestbanking
//
//  Created by Mehmood on 06/10/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Faq_detail.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "Encrypt.h"
#import "Peekaboo.h"


@interface Faq_detail ()
{
    NSMutableArray* arr_heading;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    NSMutableArray*  a;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Faq_detail
static UIView* loadingView = nil;
//@synthesize webView;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    [self.view addSubview:hud];
    gblclass =  [GlobalStaticClass getInstance];
    ssl_count = @"0";
    arr_heading=[[NSMutableArray alloc] init];
    
    [arr_heading addObject:@"UBL Digital Banking App brings convenience and exciting features of UBL Netbanking on smartphones. Now customers can manage their accounts, pay their bills and transfer funds anytime, anywhere they want through the iOS App. It has never been so easy to access and manage your finances in a secure, real-time online environment."];
    
    [arr_heading addObject:@"UBL Digital Banking App gives you 24/7 access to your bank accounts via your smartphone. Now customers can manage their accounts, pay their bills and transfer funds anytime anywhere while on the go."];
    
    [arr_heading addObject:@"No. The Digital Banking app is only for Individual customer who use regular net banking."];
    
    [arr_heading addObject:@"<p>Following features are being offered via UBL Digital Banking App</p><li>Manage Multiple Accounts</li><li>View Account Statement</li><li>Pay your Credit Card bills</li><li>Transfer funds to any bank in UAE</li><li>Transfer within own UBL accounts</li><li>Send remittance to Pakistan (Tezraftaar)</li>"];
    
    [arr_heading addObject:@"Please access Google Play/Apple Store and search for UBL Digital App UAE. Click on the Install application button. Once installed, proceed to Sign In."];
    
    
    [arr_heading addObject:@"Please note that the same net banking web portal login ID and password is used for signing into the Digital Banking App."];
    
    [arr_heading addObject:@"Advanced security measures have been taken to provide you with a secure environment where you can perform your financial transactions with complete peace of mind."];
    
    [arr_heading addObject:@"<li>Do not store your Digital Banking App Password on your device</li> <li>Always sign out of the Digital Banking App after completing your banking activities</li>  <li>Never leave your device un-attended</li>  <li>Clear your mobile phone browser cache and cookies on a regular basis</li>"];
    
    [arr_heading addObject:@"There is no cost of downloading and installing the app. This app is absolutely free."];
    
    [arr_heading addObject:@"Yes"];
    
    [arr_heading addObject:@"You just need to download the Digital Banking App, register the device through OTP and login with your credentials."];
    
    [arr_heading addObject:@"In case you have forgotten your Login password, access the ‘Forgot Password’ option on the App and follow the steps OR call UBL Phone Banking."];
    
    [arr_heading addObject:@"Yes. As long as your device has got Internet connectivity."];
    
    [arr_heading addObject:@"Please feel free to contact us via any of the following channels for all your queries:<br>For UAE: 600 533 335 or +971 600 533 335 (if overseas)<br>Email: contactubl@ublint.com"];
    
    //Aspira
 //   int textFontSize=2;
//    NSString *myHTML=[NSString stringWithFormat: @"<html><body><span style='font-size:10.0pt;font-family:Aspira;color:#000000'><p style='text-align:justify'><script>var str = '%@'; document.write(str.fontsize(%d));</script></style></body></html>",[arr_heading objectAtIndex:[gblclass.faq_index  integerValue]],textFontSize];
    
    NSString *body = [arr_heading objectAtIndex:[gblclass.faq_index  integerValue]];
    NSString *myHTML;
    
    if ([gblclass.story_board isEqualToString:@"iPhone_X"])
    {
        myHTML = [NSString stringWithFormat:@"<html><body><span style ='font-size:11pt;font-family:Aspira;color:#000000'>%@</span></body></html>", body];
    }
    else if ([gblclass.story_board isEqualToString:@"iPhone_Xr"])
    {
        myHTML = [NSString stringWithFormat:@"<html><body><span style='font-size:12pt;font-family:Aspira;color:#000000'>%@</span></body></html>", body];
    }
    else
    {
        myHTML = [NSString stringWithFormat:@"<html><body><span style='font-size:9pt;font-family:Aspira;color:#000000'>%@</span></body></html>", body];
    }
    
    
    [webview1 loadHTMLString:myHTML baseURL:nil];
    
    
//    NSString *embedHTML =[NSString stringWithFormat:@"%@", [arr_heading objectAtIndex:[gblclass.faq_index  integerValue]]];
//        [webView loadHTMLString:embedHTML baseURL:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
}

//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    //    [hud showAnimated:YES];
//    //    [self.view addSubview:hud];
//    //    [hud show:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    // [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    // [hud hideAnimated:YES];
//}


-(IBAction)btn_feature:(id)sender
{
    
    NSString*  first_time_chk = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"enable_touch"]; //first_time
    //
    
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
    }
    else
    {
        
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
    
    
    //    [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"receive_qr_login"]; //feature   receive_qr_login
    //     [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_offer:(id)sender
{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //    [hud show:YES];
    
    // gblclass.tab_bar_login_pw_check=@"login";
    
//
    
     [self custom_alert:Offer_msg :@""];
//       [self peekabooSDK:@"deals"];
//       [self peekabooSDK:@{@"type": @"deals",}];

   
    
    
    
    //    [self slide_right];
    //
    //
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#004681",
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#efefef",
    //       }
    //                          launchOptions    : nil];
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    //
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
    
    
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    //     [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_find_us:(id)sender
{
      gblclass.forget_pw_btn_click=@"0";
      gblclass.forgot_pw_click = @"1";
//      [self peekabooSDK:@"locator"];
//        [self peekabooSDK:@{@"type": @"locator",}];
    
//    gblclass.forget_pw_btn_click=@"0";
//    gblclass.forgot_pw_click = @"1";
//    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//
//
//    //new_signup  debit_card_optn
//
//
////    new_signup  debit_card_optn
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
            
        }
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        //        [self.connection cancel];
    }
    
    
    //    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}



///////////************************** SSL PINNING END ******************************////////////////



-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllow" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  a = [[NSMutableArray alloc] init];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard= [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_instant_qr"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


//- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"United Arab Emirates",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
//}

- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    //saki
    
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}
 

@end

