//
//  AccountStatement_ViewController.h
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"



@interface Accounts : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
    IBOutlet UILabel* lbl_name;
    IBOutlet UILabel* lbl_acct;
    IBOutlet UILabel* lbl_balc;
    IBOutlet UILabel* lbl_curr;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;


-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_back:(id)sender;

-(IBAction)btn_listof_acct:(id)sender;
-(IBAction)btn_card_management:(id)sender;
-(IBAction)btn_acct_statement:(id)sender;

-(IBAction)btn_acct:(id)sender;

@end




 