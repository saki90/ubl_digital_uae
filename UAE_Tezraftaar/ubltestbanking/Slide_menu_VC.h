//
//  Slide_menu_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 10/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"


@protocol SlidemenuDelegate <NSObject>
-(void)ChangeImage;
@end

@interface Slide_menu_VC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UILabel* lbl_pin_message;
    IBOutlet UILabel* lbl_daily_limits;
    IBOutlet UILabel* lbl_monthly_limits;
    IBOutlet UITableView* table;
    IBOutlet UIView* vw_logout;
    IBOutlet UIButton* btn_logout_close;
    IBOutlet UIButton* btn_logout_ok;
    
    IBOutlet UIView* vw_datt;
    IBOutlet UIDatePicker* datt_picker;
    IBOutlet UILabel* lbl_acct_name;
    IBOutlet UILabel* lbl_acct_name1;
    IBOutlet UILabel* lbl_acct_no;
    IBOutlet UIImageView* img_tch;
    
    IBOutlet UILabel* lbl_dat_frm;
    IBOutlet UILabel* lbl_dat_to;
    IBOutlet UIButton* btn_touch_chk;
    IBOutlet UIView* bg_view;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

//-(void)ChangeImage;

@property (nonatomic, strong) AVPlayer *avplayer;


@property (nonatomic, strong) IBOutlet UIView *profile_view;
@property (strong, nonatomic) IBOutlet UIImageView *Profile_imageView;
@property(nonatomic,strong) IBOutlet UITableView* table;

@property (nonatomic, weak) id delegate;
//-(IBAction)btn_cancel:(id)sender;

-(IBAction)btn_Transaction_histry:(id)sender;
-(IBAction)btn_Generate_TPIN:(id)sender;
-(IBAction)btn_Feedback:(id)sender;
-(IBAction)btn_account:(id)sender;
-(IBAction)btn_btn:(id)sender;
- (IBAction)btn_profile:(UIButton *)sender;

+(Slide_menu_VC *)getInstance;

@end
