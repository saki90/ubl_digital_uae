//
//  Next_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/05/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import <AVFoundation/AVFoundation.h>
 
#import "Reachability.h"


@interface Next_VC : UIViewController
{
     NSMutableData* responseData;
} 

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (nonatomic, strong) TransitionDelegate *transitionController;


@end
