//
//  Feature_Detail_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 29/12/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Feature_Detail_VC.h"
#import "GlobalStaticClass.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>


@interface Feature_Detail_VC ()
{
    GlobalStaticClass* gblclass;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIAlertController* alert;
}
@end

@implementation Feature_Detail_VC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    gblclass=[GlobalStaticClass getInstance];
    NSString* str;
    
    
    if (  [gblclass.app_features isEqualToString:@"1"])
    {
        
        _lbl_heading.text=@"ABOUT";
        
        str=@"<style>p,li {font-size:12px}</style><p>Following features are being offered via UBL Digital Banking App:</p>                   <ul><li>    Manage Multiple Accounts</li><li>View Account Statement</li>                                <li>Pay your Credit Card & Loan Accounts</li>                                                           <li>Pay Utility, Broadband & Mobile Bills</li>                                                          <li>Buy Prepaid vouchers & Top-ups</li>                                                                 <li>Pay to Online Shopping companies</li>                                                                   <li>Pay via QR Code</li>                                                                                <li>Transfer within own Accounts</li>                                                                   <li>Transfer Funds to UBL Accounts, 1 Link member Banks and UBL Omni Accounts</li>                      <li>Top-up WIZ Cards</li>                                                                               <<li>Order new ATM Cards</li>";
    }
    else
    {
        _lbl_heading.text=@"APP FEATURES";
        
        str=@"<style>p,li {font-size:12px}</style><p>UBL Digital Mobile App brings the exciting features of UBL Netbanking on smartphones. Now you can manage your bank accounts 24/7 from anywhere, anytime in a simple, secure & convenient manner. It has never been so easy to access and manage your finances in a real-time online environment. </p><p> Features available on UBL Digital Banking App </p>                                                                  <p><h4>➢    Accounts  </h4></p>                                                                                                                                                                                  <li>Experience  Account Statement with Graphical illustration </li>                                                    <li>Order new Debit Cards </li>                                                                        <p><h4>➢    Pay</h4></p>                                                                                                                                                                                                                                                                                                      <li>Pay your Credit Card & Loan Accounts </li>                                                     <li>Pay Utility, Broadband & Mobile Bills </li>                                                                                   <li>Buy Prepaid Vouchers & Top-ups </li>                                                               <li>Pay to Online Shopping Companies</li>                                                           <li>Pay via MasterPass QR Code </li>                                                                   <li>Transfer within own Accounts </li>                                                               <li>Transfer Funds to UBL Accounts, 1 Link member Banks and UBL Omni Accounts </li>                 <li>Top-up WIZ Cards </li>                                                                        <p><h4>➢    Add Payee</h4></p>                                                                                             <p><h4>➢    E-Transaction History</h4></p>                                                                                                         <p><h4>➢    Feedback</h4></p>                                                                                                                                                                                                                           <p><h4>➢    Settings</h4></p><li>Change Daily Limit </li>                                                                                      <li>Change MasterPass QR Limit</li>  ";
    }
    
    
    // NSString *htmlString =[NSString stringWithFormat:@"<font face='Font_name' size='15'>%@", str];
    
//    10 sep 2020
//    [_webView loadHTMLString:str baseURL:nil];
//    [_webView setBackgroundColor:[UIColor clearColor]];
//    [_webView setOpaque:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_offer:(id)sender
{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    // gblclass.tab_bar_login_pw_check=@"login";
    
    // [self slide_right];
    
    //saki  [self peekabooSDK:@"deals"];
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
    
    
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    //
    //     [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    
    //gblclass.tab_bar_login_pw_check=@"login";
    
    
    //saki     [self peekabooSDK:@"locator"];
    
    
    
    //    [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //
    //    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": @""
//                                                              }) animated:YES completion:nil];
}
@end

