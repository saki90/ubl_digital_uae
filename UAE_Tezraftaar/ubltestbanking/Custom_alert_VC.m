//
//  Custom_alert_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 14/12/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Custom_alert_VC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"

@interface Custom_alert_VC ()
{
      GlobalStaticClass* gblclass;
      UIStoryboard *storyboard;
      UIViewController *vc;
}
@end

@implementation Custom_alert_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    gblclass=[GlobalStaticClass getInstance];
    
    if ([gblclass.custom_alert_img isEqualToString:@"0"])
    {
          img.image=[UIImage imageNamed:@"warning_1.png"];
    }
    else if ([gblclass.custom_alert_img isEqualToString:@"1"])
    {
        img.image=[UIImage imageNamed:@"msg_right.png"];
    }
  
    
    lbl_heading.text=@"ATTENTION";
    
    NSLog(@"%@",gblclass.custom_alert_msg);
    
    lbl_desc.text=gblclass.custom_alert_msg;
    
    
    btn_close.layer.borderWidth=2;
    btn_close.layer.cornerRadius = 35;
    btn_close.layer.borderWidth = 1;
    
    
    // addProject.layer.cornerRadius=addProject.frame.size.width/2;
    btn_close.layer.borderColor=[UIColor whiteColor].CGColor;
    btn_close.layer.borderWidth=1.0f;
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_click:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
}


- (void)slide_left
{
    gblclass.qr_prompt=@"0";
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_close:(id)sender
{
    
    NSLog(@"%@",gblclass.parent_vc_onbording);
    
    if ([gblclass.qr_prompt isEqualToString:@"1"]) {
        
        [self slide_left];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
        
    } else if ([gblclass.parent_vc_onbording isEqualToString:@"FindPlace"]) {
        
        storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"FindPlace_swift"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:NO completion:nil];

    }
    
}
 

@end
