//
//  UIAlertView+DismissOnTimeout.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 08/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (DismissOnTimeout)

-(void)hide;

@end
