//
//  GnbATMCardList.m
//
//  Created by   on 26/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbATMCardList.h"


NSString *const kGnbATMCardListATMFLAG1 = @"ATM_FLAG_1";
NSString *const kGnbATMCardListStatus = @"Status";
NSString *const kGnbATMCardListATMTYPE = @"ATM_TYPE";
NSString *const kGnbATMCardListPENDINGATM = @"PENDING_ATM";
NSString *const kGnbATMCardListPENDINGSTATUS = @"PENDING_STATUS";
NSString *const kGnbATMCardListATMMAINSEQ = @"ATM_MAIN_SEQ";
NSString *const kGnbATMCardListATMREPLSEQ = @"ATM_REPL_SEQ";
NSString *const kGnbATMCardListATMCARDTYPE = @"ATM_CARD_TYPE";
NSString *const kGnbATMCardListATMCUSTNAME = @"ATM_CUST_NAME";
NSString *const kGnbATMCardListATMCARDNO = @"ATM_CARD_NO";
NSString *const kGnbATMCardListATMNATURE = @"ATM_NATURE";


@interface GnbATMCardList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbATMCardList

@synthesize aTMFLAG1 = _aTMFLAG1;
@synthesize status = _status;
@synthesize aTMTYPE = _aTMTYPE;
@synthesize pENDINGATM = _pENDINGATM;
@synthesize pENDINGSTATUS = _pENDINGSTATUS;
@synthesize aTMMAINSEQ = _aTMMAINSEQ;
@synthesize aTMREPLSEQ = _aTMREPLSEQ;
@synthesize aTMCARDTYPE = _aTMCARDTYPE;
@synthesize aTMCUSTNAME = _aTMCUSTNAME;
@synthesize aTMCARDNO = _aTMCARDNO;
@synthesize aTMNATURE = _aTMNATURE;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.aTMFLAG1 = [self objectOrNilForKey:kGnbATMCardListATMFLAG1 fromDictionary:dict];
            self.status = [self objectOrNilForKey:kGnbATMCardListStatus fromDictionary:dict];
            self.aTMTYPE = [self objectOrNilForKey:kGnbATMCardListATMTYPE fromDictionary:dict];
            self.pENDINGATM = [self objectOrNilForKey:kGnbATMCardListPENDINGATM fromDictionary:dict];
            self.pENDINGSTATUS = [self objectOrNilForKey:kGnbATMCardListPENDINGSTATUS fromDictionary:dict];
            self.aTMMAINSEQ = [self objectOrNilForKey:kGnbATMCardListATMMAINSEQ fromDictionary:dict];
            self.aTMREPLSEQ = [self objectOrNilForKey:kGnbATMCardListATMREPLSEQ fromDictionary:dict];
            self.aTMCARDTYPE = [self objectOrNilForKey:kGnbATMCardListATMCARDTYPE fromDictionary:dict];
            self.aTMCUSTNAME = [self objectOrNilForKey:kGnbATMCardListATMCUSTNAME fromDictionary:dict];
            self.aTMCARDNO = [self objectOrNilForKey:kGnbATMCardListATMCARDNO fromDictionary:dict];
            self.aTMNATURE = [self objectOrNilForKey:kGnbATMCardListATMNATURE fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.aTMFLAG1 forKey:kGnbATMCardListATMFLAG1];
    [mutableDict setValue:self.status forKey:kGnbATMCardListStatus];
    [mutableDict setValue:self.aTMTYPE forKey:kGnbATMCardListATMTYPE];
    [mutableDict setValue:self.pENDINGATM forKey:kGnbATMCardListPENDINGATM];
    [mutableDict setValue:self.pENDINGSTATUS forKey:kGnbATMCardListPENDINGSTATUS];
    [mutableDict setValue:self.aTMMAINSEQ forKey:kGnbATMCardListATMMAINSEQ];
    [mutableDict setValue:self.aTMREPLSEQ forKey:kGnbATMCardListATMREPLSEQ];
    [mutableDict setValue:self.aTMCARDTYPE forKey:kGnbATMCardListATMCARDTYPE];
    [mutableDict setValue:self.aTMCUSTNAME forKey:kGnbATMCardListATMCUSTNAME];
    [mutableDict setValue:self.aTMCARDNO forKey:kGnbATMCardListATMCARDNO];
    [mutableDict setValue:self.aTMNATURE forKey:kGnbATMCardListATMNATURE];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.aTMFLAG1 = [aDecoder decodeObjectForKey:kGnbATMCardListATMFLAG1];
    self.status = [aDecoder decodeObjectForKey:kGnbATMCardListStatus];
    self.aTMTYPE = [aDecoder decodeObjectForKey:kGnbATMCardListATMTYPE];
    self.pENDINGATM = [aDecoder decodeObjectForKey:kGnbATMCardListPENDINGATM];
    self.pENDINGSTATUS = [aDecoder decodeObjectForKey:kGnbATMCardListPENDINGSTATUS];
    self.aTMMAINSEQ = [aDecoder decodeObjectForKey:kGnbATMCardListATMMAINSEQ];
    self.aTMREPLSEQ = [aDecoder decodeObjectForKey:kGnbATMCardListATMREPLSEQ];
    self.aTMCARDTYPE = [aDecoder decodeObjectForKey:kGnbATMCardListATMCARDTYPE];
    self.aTMCUSTNAME = [aDecoder decodeObjectForKey:kGnbATMCardListATMCUSTNAME];
    self.aTMCARDNO = [aDecoder decodeObjectForKey:kGnbATMCardListATMCARDNO];
    self.aTMNATURE = [aDecoder decodeObjectForKey:kGnbATMCardListATMNATURE];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_aTMFLAG1 forKey:kGnbATMCardListATMFLAG1];
    [aCoder encodeObject:_status forKey:kGnbATMCardListStatus];
    [aCoder encodeObject:_aTMTYPE forKey:kGnbATMCardListATMTYPE];
    [aCoder encodeObject:_pENDINGATM forKey:kGnbATMCardListPENDINGATM];
    [aCoder encodeObject:_pENDINGSTATUS forKey:kGnbATMCardListPENDINGSTATUS];
    [aCoder encodeObject:_aTMMAINSEQ forKey:kGnbATMCardListATMMAINSEQ];
    [aCoder encodeObject:_aTMREPLSEQ forKey:kGnbATMCardListATMREPLSEQ];
    [aCoder encodeObject:_aTMCARDTYPE forKey:kGnbATMCardListATMCARDTYPE];
    [aCoder encodeObject:_aTMCUSTNAME forKey:kGnbATMCardListATMCUSTNAME];
    [aCoder encodeObject:_aTMCARDNO forKey:kGnbATMCardListATMCARDNO];
    [aCoder encodeObject:_aTMNATURE forKey:kGnbATMCardListATMNATURE];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbATMCardList *copy = [[GnbATMCardList alloc] init];
    
    if (copy) {

        copy.aTMFLAG1 = [self.aTMFLAG1 copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.aTMTYPE = [self.aTMTYPE copyWithZone:zone];
        copy.pENDINGATM = [self.pENDINGATM copyWithZone:zone];
        copy.pENDINGSTATUS = [self.pENDINGSTATUS copyWithZone:zone];
        copy.aTMMAINSEQ = [self.aTMMAINSEQ copyWithZone:zone];
        copy.aTMREPLSEQ = [self.aTMREPLSEQ copyWithZone:zone];
        copy.aTMCARDTYPE = [self.aTMCARDTYPE copyWithZone:zone];
        copy.aTMCUSTNAME = [self.aTMCUSTNAME copyWithZone:zone];
        copy.aTMCARDNO = [self.aTMCARDNO copyWithZone:zone];
        copy.aTMNATURE = [self.aTMNATURE copyWithZone:zone];
    }
    
    return copy;
}


@end
