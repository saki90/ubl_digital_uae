//
//  Bill_Toll_All_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 21/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Bill_Toll_All_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_combo_frm;
    IBOutlet UITextField* txt_combo_bill_names;
    IBOutlet UILabel* lbl_bill_mangmnt;
    IBOutlet UILabel* lbl_due_dat;
    IBOutlet UILabel* lbl_amnts;
    IBOutlet UITextField* txt_comment;
    IBOutlet UILabel* lbl_t_pin;
    IBOutlet UITextField* txt_t_pin;
    IBOutlet UIButton* btn_submit;
    IBOutlet UILabel* lbl_status;
    
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table;
    IBOutlet UIButton* button;
    
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

-(IBAction)btn_back:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_bill_names:(id)sender;
-(IBAction)btn_addpayee:(id)sender;


@end

