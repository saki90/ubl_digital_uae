//
//  Acct_statemnt_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 24/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "BEMSimpleLineGraphView.h"
#import "StatsViewController.h"
#import "Reachability.h"
#import <Charts/Charts.h>
#import "ubltestbanking-Swift.h"


@interface Acct_statemnt_VC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,BEMSimpleLineGraphDataSource,BEMSimpleLineGraphDelegate,UIGestureRecognizerDelegate,ChartViewDelegate>
{
    //IBOutlet UITableView* table;
    IBOutlet UITableView* drpdwntblview;
    //   IBOutlet UITextField* txt_acct_name;
    // IBOutlet UIView* drpdwnview;
    IBOutlet UIButton* btn;
    IBOutlet UIScrollView* scrl;
    IBOutlet UILabel* lbl_tot_cre;
    IBOutlet UILabel* lbl_tot_de;
    IBOutlet UIView* vw_datt;
    IBOutlet UIDatePicker* datt_picker;
    IBOutlet UIDatePicker* datt_picker_to;
    IBOutlet UILabel* lbl_dat_frm;
    IBOutlet UILabel* lbl_dat_to;
    
    IBOutlet UILabel* lbl_name;
    IBOutlet UILabel* lbl_acct;
    IBOutlet UILabel* lbl_balc;
    
    IBOutlet UILabel* lbl_debit;
    IBOutlet UILabel* lbl_credit;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

// Basit Changed

@property (nonatomic, strong) IBOutlet NSArray *options;


@property (nonatomic, strong) IBOutlet LineChartView *chartView;
@property (nonatomic, strong) IBOutlet UISlider *sliderX;
@property (nonatomic, strong) IBOutlet UISlider *sliderY;
@property (nonatomic, strong) IBOutlet UITextField *sliderTextX;
@property (nonatomic, strong) IBOutlet UITextField *sliderTextY;

// Basit Changed


@property (nonatomic, strong) NSMutableArray *searchResult;
@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong) IBOutlet UITableView* table;

@property(nonatomic,strong) IBOutlet UITableView* drpdwntblview;
@property(nonatomic,strong) IBOutlet UIView* drpdwnview;
@property(nonatomic,strong) IBOutlet UITextField* txt_account;


-(IBAction)btn_back:(id)sender;
-(IBAction)btn_more:(id)sender;
-(IBAction)btn_account:(id)sender;
//-(IBAction)btn_combo:(id)sender;
-(IBAction)btn_combo_cancel:(id)sender;
-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_datt_frm:(id)sender;
-(IBAction)btn_datt_to:(id)sender;
-(IBAction)btn_datt_cancel:(id)sender;
-(IBAction)btn_datt_done:(id)sender;
-(IBAction)btn_datt_click:(id)sender;


///Graph :::


@property (weak, nonatomic) IBOutlet BEMSimpleLineGraphView *myGraph;

@property (strong, nonatomic) NSMutableArray *arrayOfValues;
@property (strong, nonatomic) NSMutableArray *arrayOfDates;

@property (strong, nonatomic) IBOutlet UILabel *labelValues;
@property (strong, nonatomic) IBOutlet UILabel *labelDates;

@property (weak, nonatomic) IBOutlet UISegmentedControl *graphColorChoice;
@property (strong, nonatomic) IBOutlet UISegmentedControl *curveChoice;
@property (weak, nonatomic) IBOutlet UIStepper *graphObjectIncrement;

- (IBAction)refresh:(id)sender;
- (IBAction)addOrRemovePointFromGraph:(id)sender;
- (IBAction)displayStatistics:(id)sender;


@end

