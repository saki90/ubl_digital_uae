//
//  Req_New_Atm_Detail_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 06/06/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Req_New_Atm_Detail_VC.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Req_New_ATM_VC.h"
#import "APIdleManager.h"

@interface Req_New_Atm_Detail_VC ()
{
    UIImage *btnImage;
    NSString* btn_flag_gender;
    NSArray* arr_gender;
    NSArray* arr_marital;
    GlobalStaticClass* gblclass;
    UIImageView* img;
    NSString* split;
    UILabel* label;
    UIStoryboard *storyboard;
    UIViewController *vc;
    Req_New_ATM_VC* req;
    UIAlertController* alert;
    
}
@end

@implementation Req_New_Atm_Detail_VC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass =  [GlobalStaticClass getInstance];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"inner-bg2.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    // req=[[Req_New_ATM_VC alloc] init];
    
    
    arr_gender=@[@"Male",@"Female"];
    arr_marital=@[@"Single",@"Married"];
    
    btn_flag_gender=@"0";
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_gender:(id)sender
{
    if ([btn_flag_gender isEqualToString:@"0"])
    {
        btnImage = [UIImage imageNamed:@"check.png"];
        [btn_check_gender setImage:btnImage forState:UIControlStateNormal];
        btn_flag_gender=@"1";
        vw_marital.hidden=YES;
        vw_gender.hidden=NO;
        
    }
    else
    {
        btnImage = [UIImage imageNamed:@"uncheck.png"];
        [btn_check_gender setImage:btnImage forState:UIControlStateNormal];
        btn_flag_gender=@"0";
        vw_marital.hidden=NO;
        vw_gender.hidden=YES ;
    }
    
}


-(BOOL)isValidEmail:(NSString *)emailchk

{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailchk];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        split=[arr_gender objectAtIndex:indexPath.row];
        gblclass.atm_req_gender_flag_value=split;
        //NSLog(@"%@",split);
        
    }
    else
    {
        split=[arr_marital objectAtIndex:indexPath.row];
        gblclass.atm_req_gender_flag_value=split;
        //NSLog(@"%@",split);
        
    }
    
    
    req.txtfieldsurname.text=@"saqib";
    req.txtfieldfirstname.text=@"sadfg sjfgdjsf";
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        return  [arr_gender count];
    }
    else
    {
        return [arr_marital count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        split = [arr_gender objectAtIndex:indexPath.row];
    }
    else
    {
        split = [arr_marital objectAtIndex:indexPath.row];
    }
    
    NSString* img_check;
    
    // For Image::
    
    img=(UIImageView*)[cell viewWithTag:1];
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        img.image=[UIImage imageNamed:@"req_check.png"];
    }
    else
    {
        img.image=[UIImage imageNamed:@"succsefull_1.png"];
    }
    
    [cell.contentView addSubview:img];
    
    //For Display ID ::
    
    label=(UILabel*)[cell viewWithTag:2];
    label.text=split;
    label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    return cell;
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


@end

