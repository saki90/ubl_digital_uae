////
////  Pie_chart_VC.m
////  ubltestbanking
////
////  Created by Mehmood on 26/05/2016.
////  Copyright © 2016 ammar. All rights reserved.
////
//
//#import "Pie_chart_VC.h"
//#import "PieChartView.h"
//#import "GlobalClass.h"
//#import "GlobalStaticClass.h"
//
//@interface Pie_chart_VC ()
//{
//     NSUserDefaults *user_default;
//    GlobalStaticClass *gblclass;
//    
//}
//@end
//
//@implementation Pie_chart_VC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    
//     user_default = [NSUserDefaults standardUserDefaults];
//     gblclass =  [GlobalStaticClass getInstance];
//    
//    NSMutableArray *valueArray = [[NSMutableArray alloc] initWithObjects:
//                                  [NSNumber numberWithInt:3],
//                                  [NSNumber numberWithInt:2],
//                                  //                                  [NSNumber numberWithInt:1],
//                                  //                                  [NSNumber numberWithInt:3],
//                                  //                                  [NSNumber numberWithInt:2],
//                                  nil];
//    
//    NSMutableArray *colorArray = [[NSMutableArray alloc] initWithObjects:
//                                  [UIColor blueColor],
//                                  [UIColor redColor],
//                                  //                                  [UIColor whiteColor],
//                                  //                                  [UIColor greenColor],
//                                  //                                  [UIColor purpleColor],
//                                  nil];
//    
//    // 必须先创建一个相同大小的container view，再将PieChartView add上去
//    
//   // UIView *container = [[UIView alloc] initWithFrame:CGRectMake(90, 300, 250, 250)]; //X(320 - 250) / 2
//    PieChartView* pieView = [[PieChartView alloc] initWithFrame:CGRectMake(0, 0, 250, 250)];
//    [container addSubview:pieView];
//    
//    pieView.mValueArray = [NSMutableArray arrayWithArray:valueArray];
//    pieView.mColorArray = [NSMutableArray arrayWithArray:colorArray];
//    pieView.mInfoTextView = [[UITextView alloc] initWithFrame:CGRectMake(20, 350, 300, 80)];
//    pieView.mInfoTextView.backgroundColor = [UIColor clearColor];
//    pieView.mInfoTextView.editable = NO;
//    pieView.mInfoTextView.userInteractionEnabled = NO;
//  //  [self.view addSubview:container];
//  //  [self.view addSubview:pieView.mInfoTextView];
//    
//    
//    
//    
//    welcome_name.text=[NSString stringWithFormat:@"%@ %@",@"Welcome" ,gblclass.welcome_msg];
//    //
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
//    //
//    ////    NSInteger day = [components day];
//    ////    NSInteger week = [components month];
//    ////    NSInteger year = [components year];
//    //
//    
//    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
//    
//    [dateformate setDateFormat:@"MMM dd,YYYY"];//@"dd/MM/YYYY"
//    
//    NSString *date_String=[dateformate stringFromDate:[NSDate date]];
//    dateformate.dateFormat = @"EEEE";
//    NSString *dayName = [dateformate stringFromDate:[NSDate date]];
//    
//    welcome_date.text=[NSString stringWithFormat:@"%@",date_String];
//    welcome_day.text=[NSString stringWithFormat:@"Today is %@ ",dayName];
//    
//    
//    _Profile_imageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
//    _Profile_imageView.layer.cornerRadius=20;
//    _Profile_imageView.layer.borderWidth=2.0;
//    _Profile_imageView.layer.masksToBounds = YES;
//    _Profile_imageView.layer.borderColor=[[UIColor whiteColor] CGColor];
//    
//    
//    //229,233,242
//    
//    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
//    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    
//    //NSLog(@"%@",gblclass.profile_pic_name);
//    
//    
//    //  _Profile_imageView=[[UIImageView alloc] init];
//    
//    // getting an NSString
//    NSString *pro_img = [user_default stringForKey:@"profile_Img"];
//    
//    
//    //  self.Profile_imageView.image =pro_img;
//    
//    [self.Profile_imageView setImage:[UIImage imageNamed:pro_img]];
//    
//    //    if (![gblclass.profile_pic_name isEqualToString:[NSNull null]])
//    //    {
//    //       // [self imagePickerController:@"" didFinishPickingMediaWithInfo:gblclass.profile_pic_name];
//    //    }
//    
//    //    formatter = [[NSNumberFormatter alloc] init];
//    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    //    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
//    //    [formatter setGroupingSeparator:groupingSeparator];
//    //    [formatter setGroupingSize:3];
//    //    [formatter setUsesGroupingSeparator:YES];
//
//    
//    self.profile_view.hidden=YES;
//    
//    // For camera check :::
//    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//        
//        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                              message:@"Device has no camera"
//                                                             delegate:nil
//                                                    cancelButtonTitle:@"OK"
//                                                    otherButtonTitles: nil];
//        
//        [myAlertView show];
//        
//    }
//
//    
//    
//
//}
//
//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
// 
//}
//
//
//- (IBAction)btn_profile:(UIButton *)sender
//{
//    self.profile_view.hidden=NO;
//}
//
//- (IBAction)takePhoto:(UIButton *)sender
//{
//    
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    
//    [self presentViewController:picker animated:YES completion:NULL];
//    
//    self.profile_view.hidden=YES;
//    
//}
//
//- (IBAction)selectPhoto:(UIButton *)sender
//{
//    
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    
//    [self presentViewController:picker animated:YES completion:NULL];
//    
//    self.profile_view.hidden=YES;
//    
//    
//    
//}
//
//#pragma mark - Image Picker Controller delegate methods
//
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//{
//    //NSLog(@"%@",picker);
//    //NSLog(@"%@",[info objectForKey:@"UIImagePickerControllerReferenceURL"]);
//    //  gblclass.profile_pic_name=[info objectForKey:@"UIImagePickerControllerReferenceURL"];
//    
//    NSString* str=[NSString stringWithFormat:@"%@",[info objectForKey:@"UIImagePickerControllerReferenceURL"]];
//    
//    user_default=[NSUserDefaults standardUserDefaults];
//    // saving an NSString
//    [user_default setObject:str forKey:@"profile_Img"];
//    
//    
//    
//    
//    
//    
//    
//    
//    NSURL *url = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
//    NSString *ref = url.absoluteString;
//    
//    
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    self.Profile_imageView.image = chosenImage;
//    
//    
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    
//}
//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
//{
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//    
//}
//
// 
//
//@end
