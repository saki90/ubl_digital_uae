//
//  Req_New_ATM_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 08/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "Req_New_ATM_VC.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "UITextField+RuntimeAttributes.h"
#import "Encrypt.h"

#define ACCEPTABLE_CHARECTERS @"0123456789"

@interface Req_New_ATM_VC ()<NSURLConnectionDataDelegate>
{
    NSString* flash_btn;
    NSArray* arraycardtype;
    NSArray* arraygender;
    NSArray* arraystatus;
    NSString* table_row;
    UILabel* label;
    MBProgressHUD *hud;
    NSString* flagoftype;
    NSString* dateFormat;
    NSString *name,*fname,*surname,*cnic,*gender,*martislstatus,*email,*strdob,*keyword,*poc,*mailngadd,*relationship;
    
    //    UIDatePicker *datePicker ;
    UIToolbar *toolbar;
    UIStoryboard *storyboard;
    UIViewController *vc;
    GlobalStaticClass* gblclass;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* btn_flag_check;
    NSArray* arr_gender;
    NSArray* arr_marital;
    NSArray* split;
    UIImageView* img;
    NSString *formattedCCNumber;
    NSString* chk_ssl;
    BOOL keywordValidation;
    Encrypt *encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Req_New_ATM_VC
@synthesize cardtypeddview;
@synthesize scrollview;
@synthesize transitionController;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.transitionController = [[TransitionDelegate alloc] init];
    gblclass =  [GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    typetableview.delegate=self;
    cardtypeddview.hidden=YES;
    flash_btn=@"0";
    flagoftype=@"p";
    ssl_count = @"0";
    
    top_view.layer.cornerRadius = 5;
    top_view.layer.masksToBounds = YES;
    
    
    arraycardtype=@[@"Primary",@"Supplementary"];
    arraygender=@[@"Male",@"Female"];
    arraystatus=@[@"Married",@"Unmarried"];
    
    _vw_table.hidden=YES;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg_atm.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.vw_table.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    _txtfieldcardtype.delegate=self;
    _txtfieldprimarycardname.delegate=self;
    _txtfieldsupcardname.delegate=self;
    _txtfieldfirstname.delegate=self;
    _txtfieldsurname.delegate=self;
    _txtfieldemail.delegate=self;
    _txtfielddob.delegate=self;
    _txtfieldkeyowrd.delegate=self;
    _txtfieldpoc.delegate=self;
    _txtfieldmailingaddress.delegate=self;
    _txtfieldrelationship.delegate=self;
    _txtfieldcnic.delegate=self;
    _txtfieldsupcardname.delegate=self;
    
    [_datePicker addTarget:self action:@selector(updateTextField:)
          forControlEvents:UIControlEventValueChanged];
    
    btn_flag_check=@"0";
    [self.txtfielddob setInputView:self.datePicker];
    scrollview.contentSize=CGSizeMake(0,500);
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    _primaryview.hidden=NO;
    _supplementaryview.hidden=YES;
    
    UIImage *buttonImage = [UIImage imageNamed:@"req_check.png"];
    [btn_check setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    UIImage *buttonImage1 = [UIImage imageNamed:@"Non.png"];
    [btn_check1 setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
    [btn_virtualWiz_check setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
    
    NSString *birthday = @"06/15/1985";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [formatter dateFromString:birthday];
    

    NSString* creditCardNumber;
    
    //creditCardNumber=@"4210130713801";
    
    if ([creditCardNumber length] == 12)
    {
        formattedCCNumber = [creditCardNumber stringByReplacingOccurrencesOfString:@"(\\d{5})(\\d{7})(\\d{1})" withString:@"$1-$2-$3" options:NSRegularExpressionSearch range:NSMakeRange(0, [creditCardNumber length])];
    }
    else if ([creditCardNumber length] == 15)
    {
        formattedCCNumber = [creditCardNumber stringByReplacingOccurrencesOfString:@"(\\d{4})(\\d{6})(\\d+)" withString:@"$1-$2-$3" options:NSRegularExpressionSearch range:NSMakeRange(0, [creditCardNumber length])];
    }
    
    //NSLog(@"%@",formattedCCNumber);
    // ccNumberTextField = formattedCCNumber;
}

- (IBAction)text:(UIButton *)sender
{
     self.dateview.hidden=false;
    _datePicker.datePickerMode = UIDatePickerModeDate;
    //NSLog(@"Done");
}
- (IBAction)pickerDoneClicked:(UIButton *)sender
{
    _txtfielddob.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _txtfielddob.layer.borderWidth=1.0;
    _txtfielddob.layer.cornerRadius=5.0;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:self.datePicker.date];
    
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *DateOfBirth=[format dateFromString:[NSString stringWithFormat:@"%ld-%ld-%ld",(long)year,(long)month,(long)day]];
    NSDate *currentTime = [NSDate date];
    
    //NSLog(@"DateOfBirth======%@",DateOfBirth);
    
    NSInteger years = [[[NSCalendar currentCalendar] components:NSYearCalendarUnit
                                                       fromDate:DateOfBirth
                                                         toDate:currentTime options:0]year];
    
    
    if (years>=18)
    {
        //NSLog(@"DONE IS PRESSED");
        //[datePicker setHidden:YES];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        NSString* dateFormated= [NSString stringWithFormat:@"%@",self.datePicker.date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        NSString *dateString = [dateFormat stringFromDate:self.datePicker.date];
        self.txtfielddob.text=dateString;
        self.dateview.hidden=true;
    }
    else
    {
        [self custom_alert:@"Not valid age to request for the card" :@"0"];
        
        // [self showAlert:@"Not valid age to request for the card":@"Attension"];
    }
    
}

-(void)updateTextField:(UIDatePicker *)sender
{
    //UIDatePicker *picker = (UIDatePicker*)self.txtfielddob.inputView;
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    //  NSString* dateFormated= [NSString stringWithFormat:@"%@",self.datePicker.date];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:self.datePicker.date];
    self.txtfielddob.text=dateString;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cardtypeddbtntapped:(UIButton *)sender
{
    if(cardtypeddview.hidden)
    {
        cardtypeddview.hidden=false;
    }
    else
    {
        cardtypeddview.hidden=true;
        self.genderview.hidden=true;
        self.statusview.hidden=true;
    }
}
- (IBAction)statustypeddbtntapped:(UIButton *)sender
{
    
    [self.view endEditing:YES];
    
    gblclass.atm_req_gender_flag=@"marital";
    _vw_table.hidden=NO;
    lbl_heading.text=@"Select Marital Status";
    
    arr_marital=@[@"Single",@"Married"];
    [gendertableview reloadData];
    
}

- (IBAction)genderbtntapped:(UIButton *)sender
{
    
    [self.view endEditing:YES];
    
    gblclass.atm_req_gender_flag=@"gender";
    _vw_table.hidden=NO;
    lbl_heading.text=@"Select Gender";
    arr_gender=@[@"Male",@"Female"];
    [gendertableview reloadData];
}

- (IBAction)submitbtn:(UIButton *)sender{
    
    BOOL isValidated = YES;
    
    [_txtfieldcardtype resignFirstResponder];
    [_txtfieldprimarycardname resignFirstResponder];
    [_txtfieldsupcardname resignFirstResponder];
    [_txtfieldfirstname resignFirstResponder];
    [_txtfieldsurname resignFirstResponder];
    [_txtfieldemail resignFirstResponder];
    [_txtfielddob resignFirstResponder];
    [_txtfieldkeyowrd resignFirstResponder];
    [_txtfieldpoc resignFirstResponder];
    [_txtfieldmailingaddress resignFirstResponder];
    [_txtfieldrelationship resignFirstResponder];
    [_txtfieldcnic resignFirstResponder];
    [_txtfieldprimarycardname resignFirstResponder];
    
    if([flagoftype isEqualToString:@"p"]) {
        if ([_txtfieldprimarycardname.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter name that will appear on your card" :@"0"];
            _txtfieldprimarycardname.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldprimarycardname.layer.borderWidth=1.0;
            _txtfieldprimarycardname.layer.cornerRadius=5.0;
            
            isValidated = NO;
        }
    } else if([flagoftype isEqualToString:@"s"]) {
        
        if ([_txtfieldsupcardname.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter name that will appear on your card" :@"0"];
            _txtfieldsupcardname.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldsupcardname.layer.borderWidth=1.0;
            _txtfieldsupcardname.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfieldfirstname.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter your First Name" :@"0"];
            _txtfieldfirstname.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldfirstname.layer.borderWidth=1.0;
            _txtfieldfirstname.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfieldsurname.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter your Surname" :@"0"];
            _txtfieldsurname.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldsurname.layer.borderWidth=1.0;
            _txtfieldsurname.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
            
        } if ([_txtfieldcnic.text isEqualToString:@""] || (_txtfieldcnic.text.length < 13) ) {
            [self custom_alert:@"Please enter valid CNIC number" :@"0"];
            _txtfieldcnic.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldcnic.layer.borderWidth=1.0;
            _txtfieldcnic.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_lblgender.text isEqualToString:@""]) {
            [self custom_alert:@"Please select your Gender" :@"0"];
            _lblgender.layer.borderColor = [UIColor redColor].CGColor;
            _lblgender.layer.borderWidth=1.0;
            _lblgender.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfielddob.text isEqualToString:@""]) {
            [self custom_alert:@"Please select your Date of Birth" :@"0"];
            _txtfielddob.layer.borderColor = [UIColor redColor].CGColor;
            _txtfielddob.layer.borderWidth=1.0;
            _txtfielddob.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        }
        
        if ([_lblstatus.text isEqualToString:@""]) {
            [self custom_alert:@"Please select your Marital Status" :@"0"];
            _lblstatus.layer.borderColor = [UIColor redColor].CGColor;
            _lblstatus.layer.borderWidth=1.0;
            _lblstatus.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        }
        
        if ([_txtfieldkeyowrd.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter Keyword" :@"0"];
            _txtfieldkeyowrd.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldkeyowrd.layer.borderWidth=1.0;
            _txtfieldkeyowrd.layer.cornerRadius=5.0;
            isValidated = NO;
            return;
            
            
        } else if (!keywordValidation) {
            [self custom_alert:@"Please ensure that you have entered at least one character and one digit in keyword" :@"0"];
            _txtfieldkeyowrd.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldkeyowrd.layer.borderWidth=1.0;
            _txtfieldkeyowrd.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } else if(_txtfieldkeyowrd.text.length<6) {
            [self custom_alert:@"Minimum length of keyword is 6 characters" :@"0"];
            _txtfieldkeyowrd.layer.borderWidth=1.0;
            _txtfieldkeyowrd.layer.cornerRadius=5.0;
            isValidated = NO;
            return;
        }
        if (![self stringIsValidEmail:_txtfieldemail.text]) {
            [self custom_alert:@"Please enter Email Address" :@"0"];
            _txtfieldemail.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldemail.layer.borderWidth=1.0;
            _txtfieldemail.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfieldpoc.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter Place of Birth" :@"0"];
            _txtfieldpoc.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldpoc.layer.borderWidth=1.0;
            _txtfieldpoc.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfieldmailingaddress.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter Mailing Address" :@"0"];
            _txtfieldmailingaddress.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldmailingaddress.layer.borderWidth=1.0;
            _txtfieldmailingaddress.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
            
        } if ([_txtfieldrelationship.text isEqualToString:@""]) {
            [self custom_alert:@"Please enter Relationship" :@"0"];
            _txtfieldrelationship.layer.borderColor = [UIColor redColor].CGColor;
            _txtfieldrelationship.layer.borderWidth=1.0;
            _txtfieldrelationship.layer.cornerRadius=5.0;
            
            isValidated = NO;
            return;
        }
        
    }
 
    
    [self checkinternet];
    
    if(isValidated) {
        if (netAvailable)
        {
            name=self.txtfieldsupcardname.text;
            fname=self.txtfieldfirstname.text;
            surname=self.txtfieldsurname.text;
            cnic=self.txtfieldcnic.text;
            gender=self.lblgender.text;
            martislstatus=self.lblstatus.text;
            email=self.txtfieldemail.text;
            strdob=self.txtfielddob.text;
            keyword=self.txtfieldkeyowrd.text;
            poc=self.txtfieldpoc.text;
            mailngadd=self.txtfieldmailingaddress.text;
            relationship=self.txtfieldrelationship.text;
            
            
            
            self.genderview.hidden=true;
            self.statusview.hidden=true;
            cardtypeddview.hidden=true;
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"submit";
                [self SSL_Call];
            }
        }
    }
    
    
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_Cancel:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}


-(void) Requestprimary:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:self.txtfieldprimarycardname.text],
                                [encrypt encrypt_Data:@"G"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strNameOnCard",
                                                                   @"strCmbCardTypeSelectedValue",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"RequestUBLPrimaryATMCard" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic;    // = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              dic=(NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              NSString* responsecode = [dic objectForKey:@"Response"];
              NSString* responsstring = [dic objectForKey:@"strReturnMessage"];
              
              //Condition -78
              
              
              
              if([responsecode isEqualToString:@"0"])
              {
                  // [self showAlert:responsstring:@"Attention"];
                  
                  
                  gblclass.custom_alert_msg=responsstring;
                  gblclass.custom_alert_img=@"1";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  // [self showAlert:responsstring :@"Attention"];
                  
                  gblclass.custom_alert_msg=responsstring;
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
              }
              //  [self parsearray];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              gblclass.custom_alert_msg=@"Retry";
              gblclass.custom_alert_img=@"0";
              
              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
              vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
              vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
              vc.view.alpha = alpha1;
              [self presentViewController:vc animated:NO completion:nil];
              
          }];
}


-(void) Requestsupplementary:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:name],
                                [encrypt encrypt_Data:@"S"],
                                [encrypt encrypt_Data:fname],
                                [encrypt encrypt_Data:surname],
                                [encrypt encrypt_Data:cnic],
                                [encrypt encrypt_Data:gender],
                                [encrypt encrypt_Data:martislstatus],
                                [encrypt encrypt_Data:email],
                                [encrypt encrypt_Data:strdob],
                                [encrypt encrypt_Data:keyword],
                                [encrypt encrypt_Data:poc],
                                [encrypt encrypt_Data:mailngadd],
                                [encrypt encrypt_Data:relationship],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strNameOnCard",
                                                                   @"strCmbCardTypeSelectedValue",
                                                                   @"strFirstName",
                                                                   @"strSurName",
                                                                   @"strCNIC",
                                                                   @"strGender",
                                                                   @"strMaritalStatus",
                                                                   @"strEmailAddress",
                                                                   @"strDOB",
                                                                   @"strKeyword",
                                                                   @"strPlaceOfBirth",
                                                                   @"strMailingAddress",
                                                                   @"strRelationShipOfApplicantWithCardHolder",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"RequestUBLSupplementaryATMCard" parameters:dictparam progress:nil
     
     
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              NSString* responsecode = [dic objectForKey:@"Response"];
              NSString* responsstring = [dic objectForKey:@"strReturnMessage"];
              
              
              if([responsecode isEqualToString:@"0"])
              {
                  //   [self showAlert:responsstring :@"Attention"];
                  
                  gblclass.custom_alert_msg=responsstring;
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  //  [self showAlert:responsstring :@"Attention"];
                  
                  gblclass.custom_alert_msg=responsstring;
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              gblclass.custom_alert_msg=@"Retry";
              gblclass.custom_alert_img=@"0";
              
              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
              vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
              vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
              vc.view.alpha = alpha1;
              [self presentViewController:vc animated:NO completion:nil];
              
          }];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    //[alert release];
}
-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:YES completion:nil];
}
-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self.view resignFirstResponder];
    
    cardtypeddview.hidden=true;
    _genderview.hidden=true;
    _statusview.hidden=true;
    _dateview .hidden=true;
    
}


-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
        
        //    [self mob_App_Logout:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        
    }
    else if(buttonIndex == 1)
    {
        [self slideRight];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}






#define kOFFSET_FOR_KEYBOARD 100.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    
    if(textField.isEmailAddress){
        if(![self stringIsValidEmail:textField.text]){
            //            [self custom_alert:[NSString stringWithFormat:@"%@ is not a valid E-Mail address. Please enter a valid E-Mail address.", textField.text] :@"0"];
            
            //            UIAlertView *noMailAddressAlert = [[UIAlertView alloc]
            //                                               initWithTitle:@"Not a valid E-Mail address" message:[NSString stringWithFormat:@"%@ is not a valid E-Mail address. Please enter a valid E-Mail address.", textField.text] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //            [noMailAddressAlert show];
            //            textField.layer.borderColor=[UIColor redColor].CGColor;
            //            textField.layer.borderWidth=1.0;
            //            textField.layer.cornerRadius=5.0;
            //            textField.text = @"";
        }
    }
    
    if(textField.isCnicNumber) {
        if((textField.text.length < 13)){
            
            //            [self custom_alert:[NSString stringWithFormat:@"%@ is not a valid CNIC number. Please enter a valid CNIC number.", textField.text] :@"0"];
            
            //            UIAlertView *noCnicNumberAlert = [[UIAlertView alloc]
            //                                              initWithTitle:@"Not a valid CNIC number" message:[NSString stringWithFormat:@"%@ is not a valid CNIC number. Please enter a valid CNIC number.", textField.text] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //            [noCnicNumberAlert show];
            
            //            textField.layer.borderColor=[UIColor redColor].CGColor;
            //            textField.layer.borderWidth=1.0;
            //            textField.layer.cornerRadius=5.0;
            //            textField.text = @"";
        }
    }
    
    if (textField == _txtfieldkeyowrd) {
        keywordValidation=NO;
        int numberofCharacters = 0;
        BOOL character=NO;
        BOOL digit=NO;
        BOOL specialCharacter = NO;
        //        if([textField.text length] >= 10)
        //        {
        for (int i = 0; i < [textField.text length]; i++)
        {
            unichar c = [textField.text.lowercaseString characterAtIndex:i];
            
            if(!character)
            {
                character = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
            }
            
            if(!digit)
            {
                digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
            }
            //            if(!specialCharacter)
            //            {
            //                specialCharacter = [[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet] characterIsMember:c];
            //            }
        }
        
        if(digit && character)
        {
            keywordValidation = YES;
        }
        else
        {
            //            [self custom_alert:@"Please ensure that you have entered at least one character and one digit in keyword" :0];
            
            
            //            textField.layer.borderColor=[UIColor redColor].CGColor;
            //            textField.layer.borderWidth=1.0;
            //            textField.layer.cornerRadius=5.0;
            //            textField.text = @"";
            
            
            
            //            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please ensure that you have entered at least one character and one digit in keyword" preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //                [_txtfieldkeyowrd becomeFirstResponder];
            //            }];
            //
            //
            //            [alertController addAction:alertAction];
            //            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        //        }
        //        else
        //        {
        //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
        //                                                            message:@"Please Enter at least 10 password"
        //                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //            [alert show];
        //        }
    }
    
}


-(BOOL) stringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


//-(void)textFieldDidEndEditing:(UITextField *)textField {
//    NSString *rawString = [textField text];
//    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
//    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
//    if ([trimmed length] == 0) {
//        textField.backgroundColor=[UIColor redColor];
//    } else {
//        textField.backgroundColor=[UIColor whiteColor];
//    }
//}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //    [textField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    //   BOOL stringIsValid;
    
    MAX_DIGITS=13;
    
    if (textField==_txtfieldcnic)
    {
        //NSLog(@"LENGTH :: %lu",textField.text.length);
        _txtfieldcnic.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldcnic.layer.borderWidth=1.5;
        _txtfieldcnic.layer.cornerRadius=5.0;
        
        if (textField.text.length <13)
        {
            // //NSLog(@"%lu",[textField.text length]);
            
            //            if ([textField.text length] < 13)
            //            {
            //                formattedCCNumber = [textField.text stringByReplacingOccurrencesOfString:@"(\\d{5})(\\d{7})(\\d{1})" withString:@"$1-$2-$3" options:NSRegularExpressionSearch range:NSMakeRange(0, [textField.text length])];
            //            }
            
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
        else
        {
            
            if (range.length==1 && string.length==0)
            {
                //NSLog(@"backspace tapped");
            }
            else
            {
                return 0;
            }
            
            
        }
    }
    
    if ([textField isEqual:_txtfieldsupcardname])
    {
        
        MAX_DIGITS = 30;
        
        _txtfieldsupcardname.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldsupcardname.layer.borderWidth=1.0;
        _txtfieldsupcardname.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    else if ([textField isEqual:_txtfieldfirstname])
    {
        MAX_DIGITS = 30;
        
        _txtfieldfirstname.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldfirstname.layer.borderWidth=1.0;
        _txtfieldfirstname.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    else if ([textField isEqual:_txtfieldprimarycardname])
    {
        MAX_DIGITS = 30;
        
        _txtfieldprimarycardname.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldprimarycardname.layer.borderWidth=1.0;
        _txtfieldprimarycardname.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    else if ([textField isEqual:_txtfieldsurname])
    {
        MAX_DIGITS = 30;
        
        _txtfieldsurname.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldsurname.layer.borderWidth=1.0;
        _txtfieldsurname.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    else if ([textField isEqual:_txtfieldemail])
    {
        MAX_DIGITS = 30;
        
        _txtfieldemail.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldemail.layer.borderWidth=1.0;
        _txtfieldemail.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@."];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    else if ([textField isEqual:_txtfieldkeyowrd])
    {
        
        _txtfieldkeyowrd.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldkeyowrd.layer.borderWidth=1.0;
        _txtfieldkeyowrd.layer.cornerRadius=5.0;
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 20; //6
        
        //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
        //        for (int i = 0; i < [string length]; i++)
        //        {
        //            unichar c = [string characterAtIndex:i];
        //            if (![myCharSet characterIsMember:c])
        //            {
        //                return NO;
        //            }
        //            else
        //            {
        //                return YES;
        //            }
        //
        //        }
        
    }
    else if ([textField isEqual:_lblgender])
    {
        
        _lblgender.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _lblgender.layer.borderWidth=1.0;
        _lblgender.layer.cornerRadius=5.0;
    }
    //    else if ([textField isEqual:_txtfieldrelationship])
    //    {
    //
    //        _txtfieldrelationship.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //        _txtfieldrelationship.layer.borderWidth=1.0;
    //        _txtfieldrelationship.layer.cornerRadius=5.0;
    //    }
    else if ([textField isEqual:_txtfieldrelationship])
    {
        MAX_DIGITS = 30;
        
        _txtfieldrelationship.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldrelationship.layer.borderWidth=1.0;
        _txtfieldrelationship.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    else if ([textField isEqual: _txtfieldmailingaddress])
    {
        MAX_DIGITS = 65;
        
        _txtfieldmailingaddress.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldmailingaddress.layer.borderWidth=1.0;
        _txtfieldmailingaddress.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ 0123456789./-"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    else if ([textField isEqual: _txtfielddob])
    {
        _txtfielddob.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfielddob.layer.borderWidth=1.0;
        _txtfielddob.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        
    }
    else if ([textField isEqual: _txtfieldpoc])
    {
        MAX_DIGITS = 50;
        
        _txtfieldpoc.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _txtfieldpoc.layer.borderWidth=1.0;
        _txtfieldpoc.layer.cornerRadius=5.0;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    
    //NSLog(@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS);
    
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    
}

-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"" withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    if ([theTextField isEqual:_txtfieldcnic])
    {
        _txtfieldcnic.text=formattedOutput;
    }
    
}


-(IBAction)btn_chck:(id)sender
{
    
    UIImage *uncheckedImage = [UIImage imageNamed:@"Non.png"];
    UIImage *checkedImage = [UIImage imageNamed:@"req_check.png"];
    
    [btn_check setBackgroundImage:uncheckedImage forState:UIControlStateNormal];
    [btn_check1 setBackgroundImage:uncheckedImage forState:UIControlStateNormal];
    [btn_virtualWiz_check setBackgroundImage:uncheckedImage forState:UIControlStateNormal];
    
    UIButton *button =(UIButton *)sender;
    [button setBackgroundImage:checkedImage forState:UIControlStateNormal];
    
    if (btn_check.touchInside)
    {
        flagoftype=@"p";
        self.primaryview.hidden=NO;
        self.supplementaryview.hidden=YES;
        
    }
    else if (btn_check1.touchInside)
    {
        flagoftype=@"s";
        self.primaryview.hidden=YES;
        self.supplementaryview.hidden=NO;
        
    }
    else if (btn_virtualWiz_check.touchInside)
    {
        //saki 7 Nov ..
//        flagoftype=@"v";
//        btn_virtualWiz_check.hidden = YES;
//        self.primaryview.hidden = YES;
//        self.supplementaryview.hidden = YES;
//        
//        [self slideRight];
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"LoadVirtualWizCard"];
//        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


-(IBAction)btn_gender_marital:(id)sender
{
    if ([btn_flag_check isEqualToString:@"0"])
    {
        gblclass.atm_req_gender_flag=@"gender";
    }
    else
    {
        gblclass.atm_req_gender_flag=@"maritual";
    }
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"atm_detail"];
    
    [self presentViewController:vc animated:YES completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [gendertableview cellForRowAtIndexPath:indexPath];
    [tableView reloadData];
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        
        _lblgender.text=[arr_gender objectAtIndex:indexPath.row];
        _lblgender.layer.borderColor=[UIColor lightGrayColor].CGColor;
        _lblgender.layer.borderWidth=1.0;
        _lblgender.layer.cornerRadius=5.0;
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"req_check.png"];
        }
        
        [cell.contentView addSubview:img];
        _vw_table.hidden=YES;
        
        
        //        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        //        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    else
    {
        if ([gblclass.atm_req_gender_flag isEqualToString:@"marital"]) {
            _lblstatus.layer.borderColor=[UIColor lightGrayColor].CGColor;
            _lblstatus.layer.borderWidth=1.0;
            _lblstatus.layer.cornerRadius=5.0;
        }
        
        _lblstatus.text=[arr_marital objectAtIndex:indexPath.row];
        img=(UIImageView*)[cell viewWithTag:1];
        img.image=[UIImage imageNamed:@"req_check.png"];
        [cell.contentView addSubview:img];
        _vw_table.hidden=YES;
        
        //        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        //        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        return  [arr_gender count];
    }
    else
    {
        return [arr_marital count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        split = [arr_gender objectAtIndex:indexPath.row];
    }
    else
    {
        split = [arr_marital objectAtIndex:indexPath.row];
    }
    
    NSString* img_check;
    
    // For Image::
    
    img=(UIImageView*)[cell viewWithTag:1];
    if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
    {
        img.image=[UIImage imageNamed:@"Non.png"];
    }
    else
    {
        img.image=[UIImage imageNamed:@"Non.png"];
    }
    
    [cell.contentView addSubview:img];
    
    //For Display ID ::
    
    label=(UILabel*)[cell viewWithTag:2];
    label.text=split;
    label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

-(IBAction)btn_cancel:(id)sender
{
    _vw_table.hidden=YES;
}


-(BOOL)isValidEmail:(NSString *)emailchk

{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailchk];
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"submit"])
        {
            
            
            chk_ssl=@"";
            if([flagoftype isEqualToString:@"p"])
            {
                if([self.txtfieldprimarycardname.text length]==0)
                {
                    
                    [hud hideAnimated:YES];
                    //  [self showAlert:@"Please enter card name" :@"Attention"];
                    
                    gblclass.custom_alert_msg=@"Please enter card name";
                    gblclass.custom_alert_img=@"0";
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.view.alpha = alpha1;
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    return ;
                }
                else
                {
                    [self Requestprimary:@""];
                }
            }
            else
            {
                
                if( ([name  length]==0)   ||   ([fname length]==0) ||
                   ([ surname  length]==0)     ||      ([cnic  length]==0) ||
                   ([ gender  length]==0) ||
                   ([ martislstatus  length]==0) ||
                   ([ email   length]==0)   ||       ([strdob  length]==0) ||
                   ([ keyword  length]==0)   ||       ([ poc  length]==0) ||
                   ([ mailngadd  length]==0) ||
                   ([relationship  length]==0))
                {
                    
                    [hud hideAnimated:YES];
                    //  [self showAlert:@"All fields are mandatory" :@"Attention"];
                    
                    gblclass.custom_alert_msg=@"All fields are mandatory";
                    gblclass.custom_alert_img=@"0";
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.view.alpha = alpha1;
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    return;
                }
                else  if([self.txtfieldcnic.text length]<13)
                {
                    
                    [hud hideAnimated:YES];
                    //  [self showAlert:@"Please enter card name" :@"Attention"];
                    
                    gblclass.custom_alert_msg=@"Please enter 13 digits CNIC No.";
                    gblclass.custom_alert_img=@"0";
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.view.alpha = alpha1;
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    return ;
                }
                else if(_txtfieldkeyowrd.text.length<6)
                {
                    
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Minimum length of keyword is 6 characters" :@"0"];
                    return;
                }
                else if(![self isValidEmail:email])
                {
                    
                    [hud hideAnimated:YES];
                    //   [self showAlert:@"Enter valid email address" :@"Attention"];
                    
                    gblclass.custom_alert_msg=@"Enter valid email address";
                    gblclass.custom_alert_img=@"0";
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    vc.view.alpha = alpha1;
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    return;
                }
                else
                {
                    flagoftype=@"s";
                    [self Requestsupplementary:@""];
                }
            }
            
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"submit"])
    {
        
        
        chk_ssl=@"";
        if([flagoftype isEqualToString:@"p"])
        {
            if([self.txtfieldprimarycardname.text length]==0)
            {
                //                [self.connection cancel];
                [hud hideAnimated:YES];
                //  [self showAlert:@"Please enter card name" :@"Attention"];
                
                gblclass.custom_alert_msg=@"Please enter card name";
                gblclass.custom_alert_img=@"0";
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                return ;
            }
            else
            {
                [self Requestprimary:@""];
            }
        }
        else
        {
            
            if( ([name  length]==0)   ||   ([fname length]==0) ||
               ([ surname  length]==0)     ||      ([cnic  length]==0) ||
               ([ gender  length]==0) ||
               ([ martislstatus  length]==0) ||
               ([ email   length]==0)   ||       ([strdob  length]==0) ||
               ([ keyword  length]==0)   ||       ([ poc  length]==0) ||
               ([ mailngadd  length]==0) ||
               ([relationship  length]==0))
            {
                //                [self.connection cancel];
                [hud hideAnimated:YES];
                //  [self showAlert:@"All fields are mandatory" :@"Attention"];
                
                gblclass.custom_alert_msg=@"All fields are mandatory";
                gblclass.custom_alert_img=@"0";
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                return;
            }
            else  if([self.txtfieldcnic.text length]<13)
            {
                //                [self.connection cancel];
                [hud hideAnimated:YES];
                //  [self showAlert:@"Please enter card name" :@"Attention"];
                
                gblclass.custom_alert_msg=@"Please enter 13 digits CNIC No.";
                gblclass.custom_alert_img=@"0";
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                return ;
            }
            else if(_txtfieldkeyowrd.text.length<6)
            {
                //                [self.connection cancel];
                [hud hideAnimated:YES];
                [self custom_alert:@"Minimum length of keyword is 6 characters" :@"0"];
                return;
            }
            else if(![self isValidEmail:email])
            {
                //                [self.connection cancel];
                [hud hideAnimated:YES];
                //   [self showAlert:@"Enter valid email address" :@"Attention"];
                
                gblclass.custom_alert_msg=@"Enter valid email address";
                gblclass.custom_alert_img=@"0";
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                vc.view.alpha = alpha1;
                [self presentViewController:vc animated:NO completion:nil];
                
                return;
            }
            else
            {
                flagoftype=@"s";
                [self Requestsupplementary:@""];
            }
        }
        
    }
    
    chk_ssl=@"";
    //    [self.connection cancel];
    
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
   // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
}


- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(IBAction)btn_cancel_datte:(id)sender
{
    _dateview.hidden=YES;
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

