//
//  AccountStatement_ViewController.m
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Accounts.h"
//#import "ACT_Statement_DetailViewController.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "APIdleManager.h"

@interface Accounts ()
{
    NSArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblclass;
    //MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UILabel* lbl;
    UIAlertView* alert;
}
@end

@implementation Accounts
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    lbl=[[UILabel alloc] init];
    arr_act_type=[[NSArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    table.delegate=self;
    gblclass.arr_graph_name=[[NSMutableArray alloc] init];
    
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    lbl_name.text=gblclass.is_default_acct_id_name;
    
    
    if ([gblclass.is_default_acct_no length]==0)
    {
        gblclass.is_default_acct_no=@"00000000000000";
    }
    
    NSTextAttachment * attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"Check_acct.png"];
    attach.bounds = CGRectMake(0, 0, 15, 15);
    NSAttributedString * imageStr = [NSAttributedString attributedStringWithAttachment:attach];
    
    NSMutableAttributedString * mutableAttriStr = [[NSMutableAttributedString alloc] initWithString:gblclass.is_default_acct_no];
    NSDictionary * attris = @{NSForegroundColorAttributeName:[UIColor colorWithRed:92/255.0 green:94/255.0 blue:102/255.0 alpha:1.0],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont systemFontOfSize:12.0]};
    [mutableAttriStr setAttributes:attris range:NSMakeRange(0,mutableAttriStr.length)];
    
    [mutableAttriStr appendAttributedString:imageStr];
    lbl_acct.attributedText = mutableAttriStr;
    
//    NSLog(@"%@",[gblclass peekabo_kfc_userid]);
    
    if ([gblclass.tot_balc isEqualToString:@"NaN"] || [gblclass.tot_balc isEqualToString:@""])
    {
        gblclass.tot_balc=@"0";
    }
    
    //92,94,102
    
    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    attach1.bounds = CGRectMake(10, 8, 20, 10);
    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    
    
    //NSLog(@"%@",gblclass.is_default_m3_balc);
    if ([gblclass.is_default_m3_balc isEqualToString:@"NaN"] || [gblclass.is_default_m3_balc isEqualToString:@""] || gblclass.is_default_m3_balc.length<0 || [gblclass.is_default_m3_balc isEqualToString:@"(null)"])
    {
        gblclass.is_default_m3_balc=@"0";
    }
    
    NSString *mystring =[NSString stringWithFormat:@"%@",gblclass.is_default_m3_balc];
    NSNumber *number = [NSDecimalNumber decimalNumberWithString:mystring];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setMinimumFractionDigits:2];
    [formatter setMaximumFractionDigits:2];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    //NSLog(@"%@",[formatter stringFromNumber:number]);
    
    
    //    gblclass.tot_balc
    //    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
    //    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    //    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    //
    //    [mutableAttriStr1 appendAttributedString:imageStr1];
    //  lbl_balc.attributedText = mutableAttriStr1;
    
    
    // gblclass.base_currency
    lbl_balc.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
    
    arr_act_type=@[@"Account Summary",@"Account Statement",@"ATM Card Management"];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:14];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Yourstring=[arr_act_type objectAtIndex:indexPath.row];
}


-(IBAction)btn_back:(id)sender
{
    gblclass.landing = @"1";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName :gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


-(IBAction)btn_listof_acct:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
}

-(IBAction)btn_card_management:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"atmmanagment"];
        [self presentViewController:vc animated:NO completion:nil];
        
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window. layer addAnimation:transition forKey:nil];
        
    }
}

-(IBAction)btn_acct_statement:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        gblclass.wiz_card_chck = @"0";
        [gblclass.arr_graph_name addObject:@"0"];
        
        gblclass.frm_acct_default_chck=@"1";
        
        gblclass.chk_acct_statement=@"0";
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"act_statement"];
        [self presentViewController:vc animated:NO completion:nil];
        
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
}

-(IBAction)btn_acct:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        [ self presentViewController: vc animated:NO completion: nil];
        
    }
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            //    [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert=[[UIAlertView alloc] initWithTitle:@"Attention"
            //                                             message:statusString
            //                                            delegate:nil
            //                                   cancelButtonTitle:@"OK"
            //                                   otherButtonTitles:nil];
            //
            //            [alert show];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end



