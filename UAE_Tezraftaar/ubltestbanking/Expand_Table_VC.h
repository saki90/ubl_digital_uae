//
//  Expand_Table_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 22/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Expand_Table_VC : UIViewController<UISplitViewControllerDelegate>{
}

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end
