//
//  MapKitViewController.m
//  RMS App
//
//  Created by INCISIVESOFT MM1 on 06/03/2014.
//  Copyright (c) 2014 INCISIVESOFT MM1. All rights reserved.
//

#import "MapKitViewController.h"
#import "Annotation.h"
#import <MapKit/MKAnnotation.h>
#import "GlobalClass.h"
#import "GlobalStaticClass.h"

@interface MapKitViewController ()
{
    GlobalStaticClass* gblclass;
    float lati;
    float longi;
    UIAlertController *alert;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
}

@property (nonatomic, retain) MKPolylineView *routeLineView;
@property (nonatomic, retain) MKPolyline *routeLine;


@end

//24.916666700000000000; 67.083333300000050000;

//24.7509717,67.0861803

//Gulshan Cordinate ::
#define gulshan_lati 24.7509717;
#define gulshan_longi 67.0861803;
#define M_PI   3.14159265358979323846264338327950288
#define kOneMileMeters 1609.344



//Gulshan Nipa Cordinate ::
#define gulshan_Nipa_lati 24.8238697;
#define gulshan_Nipa_longi 67.035581;

//Span ::
#define The_span 0.020f;


#define METERS_MILE 1809.344
#define METERS_FEET 3.28084


@implementation MapKitViewController
@synthesize mapview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    @try {
        
        
        gblclass=[GlobalStaticClass getInstance];
        
        
        //In ViewDidLoad
        if(kCFCoreFoundationVersionNumber_iOS_8_4)
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        
        //NSLog(@"%@",gblclass.Offer_id);
        
        //NSLog(@"%lu",(unsigned long)[gblclass.arr_ph_address count]);
        //NSLog(@"%lu",(unsigned long)[gblclass.arr_Get_Detail_Desc  count]);
        
        //NSLog(@"%@",[[gblclass.arr_Get_Detail_Desc  objectAtIndex:[gblclass.Offer_id integerValue]] componentsSeparatedByString: @"|"]);
        
        
        NSArray* split = [[gblclass.arr_ph_address  objectAtIndex:[gblclass.Offer_id integerValue]] componentsSeparatedByString: @"|"];
        
        //Create Region
        MKCoordinateRegion myregion;
        
        
        //NSLog(@"lati = %f",[[split objectAtIndex:2] floatValue]);
        //NSLog(@"longi = %f",[[split objectAtIndex:3] floatValue]);
        
        
        lati=[[split objectAtIndex:2] floatValue];
        longi=[[split objectAtIndex:3] floatValue];
        
        //Create Center
        CLLocationCoordinate2D center;
        center.latitude=lati;
        //gulshan_Nipa_lati;
        center.longitude=longi;
        //gulshan_Nipa_longi;
        
        //The Span
        MKCoordinateSpan span;
        span.latitudeDelta=The_span;
        span.longitudeDelta=The_span;
        
        myregion.center=center;
        myregion.span=span;
        
        //Set our Mapview
        
        //Create Coordinate for use to Annotation
        
        NSMutableArray *Locations=[[NSMutableArray alloc] init];
        CLLocationCoordinate2D location;
        Annotation *myAnn;
        
        CLLocation *currentLocation = [[CLLocation alloc] init];
        
        NSString* str,*sst;
        
        str = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        sst = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        //NSLog(@"mehmood lat:%f..long:%f",originValue.latitude,originValue.longitude);
        
        myAnn=[[Annotation alloc] init];
        location.latitude=lati;
        //gulshan_Nipa_lati;
        location.longitude=longi;
        
        //gulshan_Nipa_longi;
        myAnn.coordinate=location;
        myAnn.title=gblclass.offer_name;
        //@"Restaurant Location";
        
        myAnn.subtitle=[split objectAtIndex:0];
        //@"Fast Food and Buffet";
        [Locations addObject:myAnn];
        
        //    //Second pin
        //    myAnn=[[Annotation alloc] init];
        //    location.latitude=gulshan_lati;
        //    location.longitude=gulshan_longi;
        //    myAnn.coordinate=location;
        //    myAnn.title=@"Gulshan Block 5...........";
        //    myAnn.subtitle=@"Niknor Institute";
        //    [Locations addObject:myAnn];
        
        /*
         CLLocationCoordinate2D Gul_location;
         Gul_location.latitude=gulshan_lati;
         Gul_location.longitude=gulshan_longi;
         
         Annotation * myAnn=[Annotation alloc];
         myAnn.coordinate=Gul_location;
         myAnn.Title=@"Gulshan Here";
         myAnn.Sub_title=@"Gulshan iqbal";
         
         
         [self.mapview addAnnotation:myAnn];*/
        
        double lat1=gulshan_lati;
        double long1=gulshan_longi;
        
        double lat2=gulshan_Nipa_lati;
        double long2=gulshan_Nipa_longi;
        
        // For
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:lat1 longitude:lat2];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:lat2 longitude:long2];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        //
        //    float distInMeter = [locA distanceFromLocation:locB];//
        //
        //    //NSLog(@"%f",(distance / 1609.344));
        
        int intEarthRadius = 3963;
        
        
        //        double dblLat1 = DegreesToRadians(firstLatitude);
        //        double dblLon1 = DegreesToRadians(firstLongitude);
        //
        //        double dblLat2 = DegreesToRadians(secondLatitude);
        //        double dblLon2 = DegreesToRadians(secondLongitude);
        //
        //
        //        double fltLat = locA;
        //        double fltLon = locB;
        //
        
        //        double a = sin(fltLat/2) * sin(fltLat/2) + cos(dblLat2) * cos(dblLat2) * sin(fltLon/2) * sin(fltLon/2);
        
        
        //        double c = 2 * atan2(sqrt(a), sqrt(1-a));
        //        double d = intEarthRadius * c;
        
        //        double dMeters = d * kOneMileMeters;
        
        //        NSString *strDistance = [NSString stringWithFormat:@"%1.2f meters",dMeters];
        
        
        [self distance];
        [self.mapview addAnnotations:Locations];
        [mapview setRegion:myregion animated:YES];
        
        
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        [self.locationManager startUpdatingLocation];
        [self.locationManager requestWhenInUseAuthorization]; // Add This Line
        
    }
    @catch (NSException *exception)
    {
        
        [self custom_alert:@"Location Not Found" :@"0"];
        
        
        //        UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
        //                                                        message:@"Location Not Found"
        //                                                       delegate:self
        //                                              cancelButtonTitle:nil
        //                                              otherButtonTitles:@"OK", nil];
        //
        //        [alert1 show];
        
    }
    
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy =
    //kCLLocationAccuracyBest;
    kCLLocationAccuracyBestForNavigation;
    [self.locationManager startUpdatingLocation];
    
    mapview.zoomEnabled = YES;
    mapview.showsUserLocation=YES;
    [self.view addSubview:self.mapview];
    [mapview setUserTrackingMode:MKUserTrackingModeNone animated:YES];
    // hide the prefs UI for user tracking mode - if MKMapView is not capable of it
    
    //self.locationManager.distanceFilter = 500.0f;
    // [self performSelector:@selector(getCurrentLocation) withObject:nil afterDelay:0.5];
    
    
    
    //NSLog(@"mehmood lat:%f..long:%f",originValue.latitude,originValue.longitude);
    
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"Location: %@", [newLocation description]);
}


//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    CLLocation *location = [locations lastObject];
//
//      //NSLog(@"%@",[NSString stringWithFormat:@"%f", location.coordinate.latitude]);
//      //NSLog(@"%@",[NSString stringWithFormat:@"%f", location.coordinate.longitude]);
//
////    self.latitudeValue.text = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
////    self.longtitudeValue.text = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
//
//
//}


-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [manager stopUpdatingLocation];
    CLLocation *location = locations.lastObject;
    [[self labelLatitude] setText:[NSString stringWithFormat:@"%.6f", location.coordinate.latitude]];
    [[self labelLongitude] setText:[NSString stringWithFormat:@"%.6f", location.coordinate.longitude]];
    [[self labelAltitude] setText:[NSString stringWithFormat:@"%.2f feet", location.altitude*METERS_FEET]];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2*METERS_MILE, 8*METERS_MILE);
    [[self mapview] setRegion:viewRegion animated:YES];
}


-(void)distance
{
    double  long1 = gulshan_Nipa_longi;   double  lat1 = gulshan_Nipa_lati;
    double  long2 = gulshan_longi;   double  lat2 = gulshan_lati;
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude: lat1 longitude:long1];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude: lat2 longitude:long2];
    //NSLog(@"LOC1  = %f, %f", location1.coordinate.latitude,  location1.coordinate.longitude);
    //NSLog(@"LOC2 = %f, %f", location2.coordinate.latitude, location2.coordinate.longitude);
    // now calculating the distance using inbuild method distanceFromLocation:
    float distInMeter = [location1 distanceFromLocation:location2]; // which returns in meters
    //converting meters to mile
    //float distInMile = 0.000621371192 * distInMeter;
    float distInMile = distInMeter/1000;
    
    //NSLog(@"Actual Distance in Mile : %f",distInMile);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    NSString *annotationIdentifier = @"CustomViewAnnotation";
    MKAnnotationView* annotationView = [mapview dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if(!annotationView)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:annotationIdentifier];
    }
    annotationView.image = [UIImage imageNamed:@"map_location_pin.png"];
    annotationView.canShowCallout= YES;
    
    return annotationView;
}

-(BOOL) prefersStatusBarHidden
{
    return YES;
}

-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
//{
//    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
//    polylineView.strokeColor = [UIColor redColor];
//    polylineView.lineWidth = 1.0;
//    return polylineView;
//}



- (IBAction)getRoute:(id)sender {
    
    
    
    
    
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(37.776142, -122.424774) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(37.73787, -122.373962) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        //NSLog(@"response = %@",response);
        NSArray *arrRoutes = [response routes];
        [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            MKRoute *rout = obj;
            
            MKPolyline *line = [rout polyline];
            [mapview addOverlay:line];
            //NSLog(@"Rout Name : %@",rout.name);
            //NSLog(@"Total Distance (in Meters) :%f",rout.distance);
            
            NSArray *steps = [rout steps];
            
            //NSLog(@"Total Steps : %lu",(unsigned long)[steps count]);
            
            [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                //NSLog(@"Rout Instruction : %@",[obj instructions]);
                ////NSLog(@"Rout Distance : %f",[obj distance]);
            }];
        }];
    }];
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    
    if(overlay == _routeLine)
    {
        if(nil == _routeLineView)
        {
            _routeLineView = [[MKPolylineView alloc] initWithPolyline:_routeLine];
            
            _routeLineView.strokeColor = [UIColor redColor];
            _routeLineView.lineWidth = 5;
            
        }
        
        return self.routeLineView;
    }
    
    return nil;
}



- (void)displayRouteFrom:(CLLocationCoordinate2D)sourceCoordinate
                      to:(CLLocationCoordinate2D)destinationCoordinate
{
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    directionsRequest.transportType = MKDirectionsTransportTypeAny;
    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceCoordinate
                                                         addressDictionary:nil];
    MKMapItem *sourceItem = [[MKMapItem alloc] initWithPlacemark:sourcePlacemark];
    MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destinationCoordinate
                                                              addressDictionary:nil];
    MKMapItem *destinationItem = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    [directionsRequest setSource:sourceItem];
    [directionsRequest setDestination:destinationItem];
    directionsRequest.transportType = MKDirectionsTransportTypeAny;
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    //   [self.indicatorIB startAnimating];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        // [self.indicatorIB stopAnimating];
        //  [self.indicatorIB setHidden:YES];
        if (error) {
            //NSLog(@"Unable to find directions for the bus stop.");
            //            [Utility showAlert:nil mess:@"Unable to find directions for the bus stop."];
        } else {
            MKRoute *routeDetails = response.routes.lastObject;
            [mapview setVisibleMapRect:[routeDetails.polyline boundingMapRect] edgePadding:UIEdgeInsetsMake(15.0, 15.0, 15.0, 15.0) animated:true];
            [mapview addOverlay:routeDetails.polyline];
        }
    }];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

