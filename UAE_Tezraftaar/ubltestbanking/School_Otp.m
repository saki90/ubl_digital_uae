//
//  School_Otp.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 27/08/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "School_Otp.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface School_Otp ()
{
    
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    UIAlertController* alert;
    NSString* str_Otp;
    NSString* alert_chck;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pw;
    int txt_nam;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* chk_service_call;
    NSString* logout_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@end

@implementation School_Otp
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    lbl_heading.text = @"OTP AUTHENTICATION"; //gblclass.bill_type;
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //[NSString stringWithFormat:@"ADD %@",gblclass.acct_add_type]; //
    //gblclass.Mobile_No
    
    chk_service_call = @"0";
    ssl_count = @"0";
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    logout_chck = @"";
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    // Set textfield cursor color ::
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    //    arr_pin_pattern=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    //    arr_pw_pin=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    
    
    
    //To add a new payee via smartphone a four digits One Time Passcode is sent to your mobile number *******3456 via SMS. Please enter the sent code below.
    
    NSString *subStr ;//= [str substringWithRange:NSMakeRange(0, 20)];
    
    //NSLog(@"%@", gblclass.Mobile_No);
    alert_chck=@"";
    
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below.",subStr];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}


-(IBAction)btn_resend_otp:(id)sender
{
    [hud showAnimated:YES];
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"re_generate_otp";
        [self SSL_Call];
    }
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(void)slideRight
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slideLeft
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_submit:(id)sender
{
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        
        [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
        
        return;
    }
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
 
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"submit";
        [self SSL_Call];
    }
}




-(void)addA2pBeneficiaryInquiry:(NSString *)strIndustry {
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        

        
    //    gblclass.arr_resend_otp_cnic = [[NSMutableArray alloc] init];
        
//        [gblclass.arr_resend_otp_cnic addObject:lbl_receiver_name.text];
//        [gblclass.arr_resend_otp_cnic addObject:lbl_receiver_cnic.text];
//        [gblclass.arr_resend_otp_cnic addObject:lbl_receiver_cnic.text];
//        [gblclass.arr_resend_otp_cnic addObject:lbl_receiver_cell.text];
//        [gblclass.arr_resend_otp_cnic addObject:lbl_receiver_email.text];
//        [gblclass.arr_resend_otp_cnic addObject:gblclass.user_id];
        
        NSLog(@"%@",gblclass.arr_resend_otp_cnic);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_resend_otp_cnic objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.arr_resend_otp_cnic objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_resend_otp_cnic objectAtIndex:2]],
                                    [encrypt encrypt_Data:[gblclass.arr_resend_otp_cnic objectAtIndex:3]],
                                    [encrypt encrypt_Data:[gblclass.arr_resend_otp_cnic objectAtIndex:4]],
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"ADDITION"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"txtReceiverNameText",
                                                                       @"txtReceiverCNICText",
                                                                       @"txtReConfirmReceiverCNICText",
                                                                       @"txtReceiverMobileNumberText",
                                                                       @"txtReceiverEmailAddressText",
                                                                       @"strUserId",
                                                                       @"strCallingOTPType",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AddA2pBeneficiaryInquiry" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      logout_chck = @"1";
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      [hud hideAnimated:YES];
                      [self slideRight];
                      gblclass.relation_id = [dic objectForKey:@"strRelationshipId"];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                      
                } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
}




-(void) Re_Generate_OTP:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
//    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"MOB", nil]];
  
    
    NSLog(@"%@",gblclass.school_fee_data);
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],//1
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"ADDITION"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:
                                                                   @"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  logout_chck = @"1";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              
              NSString* responsecode = [NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                   [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  logout_chck = @"1";
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
          }];
    
    
}

- (void)clearOTP {
    txt_1.text = @"";
    txt_2.text = @"";
    txt_3.text = @"";
    txt_4.text = @"";
    txt_5.text = @"";
    txt_6.text = @"";
}

-(void) Add_Bill_Companies:(NSString *)strIndustry
{
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        //    [gblclass.school_fee_data addObject:[split objectAtIndex:3]]; //Merchant ID
        //    [gblclass.school_fee_data addObject:[split objectAtIndex:5]]; //Tc access key
        //    [gblclass.school_fee_data addObject:[split objectAtIndex:6]]; //TT access key
        //    [gblclass.school_fee_data addObject:[split objectAtIndex:7]]; //tt id
        
        //   [gblclass.descriptionDetailModel addObjectsFromArray:@[name,gblclass.arr_school_name,txt_consumer_no.text,txt_region_code.text]];
        
        //2030000217349
        
        NSLog(@"%@",gblclass.school_fee_data);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:3]],
                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],//1
                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],
                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:5]],
                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:4]],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTtId",
                                                                       @"strAccessKey",
                                                                       @"strType",
                                                                       @"strtxtNick",
                                                                       @"strtxtCustomerID",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AddBillCompanies" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//                  NSString* response = [dic2 objectForKey:@"Response"];
//                  NSString* name = [dic2 objectForKey:@"ConsumerTitle"];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic2 objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      logout_chck = @"1";
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                        message:@"Fee Added Successfully"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                      
                      [message show];
                      
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"school_list"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];//strReturnMessage
                      [self clearOTP];
                      [self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
             // [mine myfaildata];
             
             [self clearOTP];
             [hud hideAnimated:YES];
             [self custom_alert:@"Please try again later." :@"0"];
             
         }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self clearOTP];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}

-(void)addA2pPayee:(NSString *)strIndustry {
    
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
//        AddA2pPayee(string strSessionId, string IP, string strUserId, string Token, string Device_ID, string relationshipId, string txtReceiverCNICText, string txtReceiverNameText, string txtReceiverMobileNumberText, string txtReceiverEmailAddressText, string txtOTPText)
        
        

//    strSessionId:
//    IP:
//    strUserId:
//    Token:
//    Device_ID:
//    relationshipId:
//    txtReceiverCNICText:
//    txtReceiverNameText:
//    txtReceiverMobileNumberText:
//    txtReceiverEmailAddressText:
//    txtOTPText:
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.descriptionDetailModel objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.descriptionDetailModel objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.descriptionDetailModel objectAtIndex:2]],
                                    [encrypt encrypt_Data:[gblclass.descriptionDetailModel objectAtIndex:3]],
                                    [encrypt encrypt_Data:gblclass.relation_id],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"txtReceiverNameText",
                                                                       @"txtReceiverCNICText",
                                                                       @"txtReceiverMobileNumberText",
                                                                       @"txtReceiverEmailAddressText",
                                                                       @"relationshipId",
                                                                       @"txtOTPText",
                                                                       @"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AddA2pPayee" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      logout_chck = @"1";
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [self slideLeft];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"payee_list"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self clearOTP];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
}


-(void) payWizRequestSubmit:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1 ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        public string payWizRequestSubmit(string strSessionId, string IP, string Device_ID, string Token, Int64 strUserId,string wizRequestProfileNo,string strTxtAmount,string lblCnicNoText,string cmbPayFromSelectedValue,string cmbPayFromSelectedItemRegAccount,string cmbPayFromSelectedItemText,string cmbPayFromSelectedItemCurrency,string strOTPPIN,string strTCAccessKey,string strTTAccessKey,string strlblCustomerID,out string strReturnMessage)
        //
        //
        //        Sample data:
        //        string strRes = string.Empty;
        //
        //        string strOtpPin = "1234";
        //        string cmbPayFromSelectedValue = "212549568";
        //        string cmbPayFromSelectedItemRegAccount = "2279625";
        //        string cmbPayFromSelectedItemText = "SHOAIB IQBAL";
        //        string cmbPayFromSelectedItemCurrency = "PKR";
        //        string strTCAccessKey = "WIZ_REQUEST";
        //        string strTTAccessKey = "WIZ_REQUEST";
        //
        //        string isPostWizRequestTrx = string.Empty;
        
        
        
        //    strSessionId:
        //    IP:
        //    Device_ID:
        //    Token:
        //    strUserId:
        //    wizRequestProfileNo:
        //    strTxtAmount:
        //    lblCnicNoText:
        //    cmbPayFromSelectedValue:
        //    cmbPayFromSelectedItemRegAccount:
        //    cmbPayFromSelectedItemText:
        //    cmbPayFromSelectedItemCurrency:
        //    strOTPPIN:
        //    strTCAccessKey:
        //    strTTAccessKey:
        //    strlblCustomerID:
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.wizRequestProfileNo],
                                    [encrypt encrypt_Data:gblclass.wizLoadAmount],
                                    [encrypt encrypt_Data:gblclass.cnic_no],
                                    [encrypt encrypt_Data:gblclass.acc_number],
                                    [encrypt encrypt_Data:gblclass.is_default_acct_id],
                                    [encrypt encrypt_Data:gblclass.acctitle],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.wizTrasnctionType],
                                    [encrypt encrypt_Data:gblclass.wizTrasnctionType],
                                    [encrypt encrypt_Data:@"0"], nil]
                                   
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strUserId",
                                                                       @"wizRequestProfileNo",
                                                                       @"strTxtAmount",
                                                                       @"lblCnicNoText",
                                                                       @"cmbPayFromSelectedValue",
                                                                       @"cmbPayFromSelectedItemRegAccount",
                                                                       @"cmbPayFromSelectedItemText",
                                                                       @"cmbPayFromSelectedItemCurrency",
                                                                       @"strOTPPIN",
                                                                       @"strTCAccessKey",
                                                                       @"strTTAccessKey",
                                                                       @"strlblCustomerID", nil]];
        
        
        
        
        
        ////NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"payWizRequestSubmit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //                  {
                  //                      Response = "-3";
                  //                      emailToolTip = "<null>";
                  //                      lblCnicNoText = "<null>";
                  //                      lblEmailText = "<null>";
                  //                      lblMobileNoMaskText = "<null>";
                  //                      lblMobileNumberText = "<null>";
                  //                      mobileToolTip = "<null>";
                  //                      strReturnMessage = "Provided OTP is incorrect, please enter the OTP that has been sent to your Mobile Number.";
                  //                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      logout_chck = @"1";
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  else if ([[dic objectForKey:@"IsOtpRequired"] isEqualToString:@"-1"])
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      
                      [hud hideAnimated:YES];
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"NewUblWizCard"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                      
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self clearOTP];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
    
}

-(void)generateOTPForAddition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:gblclass.acc_number],
                                                                       [encrypt encrypt_Data:gblclass.wizTrasnctionType],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:gblclass.wizTrasnctionType],
                                                                       [encrypt encrypt_Data:@"MOBILE APP"],
                                                                       [encrypt encrypt_Data:@"ADDITION"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userId",
                                                                       @"strSessionId",
                                                                       @"M3Key",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strBranchCode",
                                                                       @"strAccountNo",
                                                                       @"strAccessKey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
        //                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
        //                                    [encrypt encrypt_Data:gblclass.M3sessionid],
        //                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
        //                                    [encrypt encrypt_Data:@""],
        //                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:4]],
        //                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],
        //                                    [encrypt encrypt_Data:@"Generate OTP"],
        //                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:3]],
        //                                    [encrypt encrypt_Data:[gblclass.school_fee_data objectAtIndex:2]],//1
        //                                    [encrypt encrypt_Data:@"SMS"],
        //                                    [encrypt encrypt_Data:@"ADDITION"],
        //                                    [encrypt encrypt_Data:gblclass.Udid],
        //                                    [encrypt encrypt_Data:gblclass.token],
        //                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
        //
        //                                                              forKeys:[NSArray arrayWithObjects:
        //                                                                       @"userId",
        //                                                                       @"strSessionId",
        //                                                                       @"IP",
        //                                                                       @"strBranchCode",
        //                                                                       @"strAccountNo",
        //                                                                       @"strAccesskey",
        //                                                                       @"strCallingOTPType",
        //                                                                       @"TTID",
        //                                                                       @"TC_AccessKey",
        //                                                                       @"Channel",
        //                                                                       @"strMessageType",
        //                                                                       @"Device_ID",
        //                                                                       @"Token",
        //                                                                       @"M3Key",nil]];
        
        
        
        [manager.requestSerializer setTimeoutInterval: time_out];
        [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      logout_chck = @"1";
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [hud hideAnimated:YES];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"CongratulationsVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"atmmanagment"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        if ([logout_chck isEqualToString:@"1"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
        }
        
    }
    else if(buttonIndex == 1)
    {
        [hud showAnimated:YES];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSUInteger MAX_DIGITS = 1;
//
//    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    //NSLog(@"Responder :: %@",str);
//
//    if ([arr_pin_pattern count]>1)
//    {
//
//        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
//
//        //NSLog(@"sre pw :: %@",str_pw);
//        textField.text = string;
//
//        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
//
//        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
//        {
//            //NSLog(@"NO SPECIAL CHARACTER");
//        }
//        else
//        {
//            //NSLog(@"HAS SPECIAL CHARACTER");
//            textField.text=@"";
//
//            return 0;
//        }
//
//        if( [str length] > 0 )
//        {
//            //   [arr_pin_pattern removeObjectAtIndex:0];
//
//            int j;
//            int i;
//            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
//            {
//                j=[[arr_pw_pin objectAtIndex:i] integerValue];
//
//                if (j>textField.tag)
//                {
//                    j=i;
//                    break;
//                }
//
//            }
//
//
//            textField.text = string;
//            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
//
//            //NSLog(@"Responder :: %@",str);
//            //NSLog(@"tag %ld",(long)textField.tag);
//
//            NSLog(@"%@",nextResponder);
//            NSLog(@"%@",[textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]);
//
//            if (nextResponder)
//            {
//                [textField resignFirstResponder];
//                [nextResponder becomeFirstResponder];
//            }
//
//
//            if (textField.tag == 6)
//            {
//                [hud showAnimated:YES];
//                [self.view addSubview:hud];
//                [hud showAnimated:YES];
//
//                //  textField.tag = [@"7" integerValue];
//
//
//                txt_6.enabled = NO;
//                txt_6.text=string;
//
//                str_Otp=@"";
//                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
//
//                chk_ssl = @"submit";
//                [self SSL_Call];
//
//                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//                // return 0;
//            }
//
//
//
//            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//        }
//
//    }
//
//    //NSLog(@"%ld",(long)textField.tag);
//
//    if( [str length] > 0 )
//    {
//        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//    }
//    else
//    {
//        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
//
//        txt_nam=textField.tag;
//        [self txt_field_back_move];
//        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
//
//        //NSLog(@"Responder :: %@",nextResponder);
//
//        if (nextResponder)
//        {
//            [textField resignFirstResponder];
//            [nextResponder becomeFirstResponder];
//        }
//
//        //textField.text=@"";
//        return 0;
//    }
//
//    return YES;



    NSUInteger maxLength = 1;
    
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text length :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    
    NSString *str1 = @"hello ";
    str1 = [str1 stringByAppendingString:str];
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
           // btn_next.enabled=YES;
            if ([gblclass.str_processed_login_chk isEqualToString:@"0"])
            {
                
                
//                if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
//                {
//                   // btn_next.enabled=YES;
//                }
//                else if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
//                {
//                    [hud hideAnimated:YES];
//
//                    txt_1.text=@"";
//                    txt_2.text=@"";
//                    txt_3.text=@"";
//                    txt_4.text=@"";
//                    txt_5.text=@"";
//                    txt_6.text=@"";
//
//                    [txt_1 becomeFirstResponder];
//
                    //  [self custom_alert:@"Kindly accept Terms & Conditions" :@"0"];
                    //  return 0;
               // }
                //                else if ([t_n_c isEqualToString:@"0"])
                //                {
                //                    [hud hideAnimated:YES];
                //
                //                    txt_1.text=@"";
                //                    txt_2.text=@"";
                //                    txt_3.text=@"";
                //                    txt_4.text=@"";
                //                    txt_5.text=@"";
                //                    txt_6.text=@"";
                //
                //                    [txt_1 becomeFirstResponder];
                //
                //                    [self custom_alert:@"Kindly accept Terms & Conditions" :@"0"];
                //                    return 0;
                //                }
//                else if ([t_n_c isEqualToString:@"1"])
//                {
//                    btn_next.enabled=YES;
//                }
                
            }
            
            if (textField.tag == 6)
            {
                
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                [txt_6 resignFirstResponder];
                
                
                if ([gblclass.acct_add_type isEqualToString:@"TRANSFER FUNDS TO CNIC"]) {
                    chk_ssl=@"addA2pPayee";
                } else if ([gblclass.acct_add_type isEqualToString:@"LOAD VIRTUAL WIZ CARD"]) {
                    chk_ssl=@"payWizRequestSubmit";
                }else {
                    chk_ssl=@"submit";
                }
                
                [self SSL_Call];
                return 0;
                
            }
            
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    


}

-(void)call_ssl_service
{
    chk_service_call = @"1";
    [self SSL_Call];
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
    s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"re_generate_otp"])
        {
            chk_ssl = @"";
            
            if ([gblclass.acct_add_type isEqualToString:@"LOAD VIRTUAL WIZ CARD"]) {
                [self generateOTPForAddition:@""];
            }else {
                [self Re_Generate_OTP:@""];
            }
        }
        else if ([chk_ssl isEqualToString:@"submit"]) {
            chk_ssl = @"";
            [self Add_Bill_Companies:@""];
            
        } else if ([chk_ssl isEqualToString:@"addA2pPayee"]) {
            
            chk_ssl = @"";
            [self addA2pPayee:@""];
            
        } else if ([chk_ssl isEqualToString:@"payWizRequestSubmit"]) {
            chk_ssl = @"";
            [self payWizRequestSubmit:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check = @"1";
        }
        else
        {
            [self.connection cancel];
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"re_generate_otp"])
    {
        chk_ssl = @"";
       
        if ([gblclass.acct_add_type isEqualToString:@"TRANSFER FUNDS TO CNIC"])
        {
             [self addA2pBeneficiaryInquiry:@""];
        }
        else
        {
             [self Re_Generate_OTP:@""];
        }
        
       
    }
    else if ([chk_ssl isEqualToString:@"submit"])
    {
        chk_ssl = @"";
        [self Add_Bill_Companies:@""];
    }
    else if ([chk_ssl isEqualToString:@"addA2pPayee"]) {
        [self addA2pPayee:@""];
        
    }  else if ([chk_ssl isEqualToString:@"payWizRequestSubmit"]) {
        chk_ssl = @"";
        [self payWizRequestSubmit:@""];
    }
   
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    }
    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString = @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            [self custom_alert:statusString :@""];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             // [mine myfaildata];
             [hud hideAnimated:YES];
             
             gblclass.custom_alert_msg=@"Please try again later.";
             gblclass.custom_alert_img=@"0";
             
             storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
             vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
             vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
             vc.view.alpha = alpha1;
             [self presentViewController:vc animated:NO completion:nil];
             
         }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
}

//#define kOFFSET_FOR_KEYBOARD 30.0
//
//-(void)keyboardWillShow
//{
//    // Animate the current view out of the way
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)keyboardWillHide
//{
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:sender.text])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}
//
////method to move the view up/down whenever the keyboard is shown/dismissed
//-(void)setViewMovedUp:(BOOL)movedUp
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
//
//    CGRect rect = self.view.frame;
//
//    //   //NSLog(@"%d",rect );
//
//    if (movedUp)
//    {
//        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
//        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//
//
//        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
//
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//    }
//    else
//    {
//        // revert back to the normal state.
//        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
//        [self.view endEditing:YES];
//    }
//    self.view.frame = rect;
//
//    [UIView commitAnimations];
//}
//
//
//- (void)viewWillAppear:(BOOL)animated
//{
//    [hud hideAnimated:YES];
//    [super viewWillAppear:animated];
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    dispatch_async(dispatch_get_main_queue(),
//                   ^{
//                       //  self.fundtextfield.delegate = self;
//
//
//                       //         [mine showmyhud];
//                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
//
//                       //                       [self getFolionumber];
//                   });
//
//
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}


@end
