//
//  Receive_QR.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Receive_QR : UIViewController
{
    
    
    //  UIImageView *imgView;
    IBOutlet UIImageView* img_logo;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

//@property (nonatomic,retain) UIImageView *imgView;


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

