//
//  QR_Code1_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 28/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMScannerView.h"

@interface QR_Code1_VC : UIViewController<RMScannerViewDelegate, UIAlertViewDelegate, UIBarPositioningDelegate>

@property (strong, nonatomic) IBOutlet RMScannerView *scannerView;
@property (weak, nonatomic) IBOutlet UILabel *statusText;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sessionToggleButton;
- (IBAction)startNewScannerSession:(id)sender;


@end

