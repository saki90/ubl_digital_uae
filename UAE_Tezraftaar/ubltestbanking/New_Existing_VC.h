//
//  New_Existing_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 31/05/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface New_Existing_VC : UIViewController
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UILabel* lbl_shoulder;
    
}


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

-(IBAction)btn_next:(id)sender;
-(IBAction)btn_Signup:(id)sender;

@end
