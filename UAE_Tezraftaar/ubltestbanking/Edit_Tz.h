//
//  Edit_Tz.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 30/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Edit_Tz : UIViewController<UITextFieldDelegate>
{
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    IBOutlet UITextField *txt_nick;
    IBOutlet UITextField *txt_email;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end


NS_ASSUME_NONNULL_END
