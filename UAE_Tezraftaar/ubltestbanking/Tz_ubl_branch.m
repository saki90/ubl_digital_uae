//
//  Tz_ubl_branch.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 25/06/2020.
//  Copyright © 2020 ammar. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#import "Tz_ubl_branch.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "GlobalClass.h"
#import "Encrypt.h"

@interface Tz_ubl_branch ()
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    NSString* ssl_count;
    Encrypt *encrypt;
    NSArray *split;
    UILabel* label;
    UIImageView* img;
    NSArray* arr_acct;
    NSString* relation_code;
    NSArray *searchResults;
    NSMutableArray* arr_search,*arr_search_brcode;
    NSString* br_code;
    NSString* responsecode;
    NSString* email;
    NSString* err_code;
    NSString * vw_down_chck,* Keyboard_show,*chk_vw;
    NSArray* split_select;
    NSString* search_chck;
    NSMutableArray* companytype;
    NSMutableArray* branchname,*branchcode;
    
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end


@implementation Tz_ubl_branch
@synthesize transitionController;
@synthesize searchResult;

- (void)viewDidLoad
{
    [super viewDidLoad];
  //Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    gblclass = [GlobalStaticClass getInstance];
    chk_ssl = @"";
    ssl_count = @"0";
    encrypt = [[Encrypt alloc] init];
    vw_down_chck=@"0";
    Keyboard_show = @"0";
    chk_vw = @"0";
    search.hidden = YES;
    vw_branch_acct.hidden = YES;
    table_iban.hidden = YES;
    vw_table.hidden = YES;
    table_branch.hidden = YES;
    table_city.hidden = YES;
    table_relation.hidden = YES;
    
    txt_relation.delegate = self;
    txt_city.delegate = self;
    txt_iban.delegate = self;
    txt_select_branch.delegate = self;
    txt_email.delegate = self;
    txt_address.delegate = self;
    txt_acct_numbr.delegate = self;
    txt_contact_num.delegate = self;
    txt_select_iban_acct.delegate = self;
    txt_name.delegate = self;
    email = @"";
    arr_search_brcode=[[NSMutableArray alloc] init];
    searchResult=[[NSMutableArray alloc] init];
    arr_search=[[NSMutableArray alloc] init];
    arr_acct = [[NSArray alloc] init];
    err_code = @"account number";
    br_code = @"";
    search_chck = @"";
    search.barTintColor=[UIColor colorWithRed:44/255.0 green:128/255.0 blue:197/255.0 alpha:1.0];
    search.tintColor=[UIColor whiteColor];
    //search.placeholder = @"Search";
    self.definesPresentationContext = true;
    search.delegate=self;
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    arr_acct = @[@"IBAN",@"Account Number"];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    gblclass.chck_tezraftr_bene_type =@"account";//iban
    
    NSLog(@"%@",gblclass.arr_tezrftar_bank);
    txt_acct_numbr.keyboardType = UIKeyboardTypeNumberPad;
    if ([ gblclass.acct_add_type  isEqualToString:@"Other Bank" ])
    {
        vw_branch_acct.hidden = YES;
    }
    else
    {
        vw_branch_acct.hidden = NO;
    }
    
    companytype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    
    
    [self checkinternet];
    if (netAvailable)
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        chk_ssl=@"loadbranches";
        [self SSL_Call];
    }
    
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_next:(id)sender
{
    
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    [txt_relation resignFirstResponder];
    
    vw_down_chck = @"1";
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
    if (self.view.frame.origin.y >= 0)
       {
           [self setViewMovedUp:YES];
       }
       else if (self.view.frame.origin.y < 0)
       {
           if ([vw_down_chck isEqualToString:@"1"])
           {
               [self setViewMovedUp:NO];
               vw_down_chck = @"0";
           }
       }
    }
    
    
    if (vw_branch_acct.isHidden)
    {
        NSString* ibann = [txt_iban.text uppercaseString];
        txt_iban.text = ibann;
        
        if ([txt_select_iban_acct.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select bank" :@"0"];
        }
        else if ([txt_iban.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter IBAN number" :@"0"];
        }
        else if (txt_iban.text.length <24)
        {
            [self custom_alert:@"Invalid IBAN number" :@"0"];
        }
        else if([txt_name.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter name" :@"0"];
        }
        else if([txt_contact_num.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter contact number" :@"0"];
        }
        else if([txt_address.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter address" :@"0"];
        }
        else if([txt_city.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select city" :@"0"];
        }
        else if([txt_relation.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select relation" :@"0"];
        }
       else if (![txt_email.text isEqualToString:@""])
        {
            if (![self validateEmail:[txt_email text]])
            {
                [self custom_alert:@"Invalid email address" :@"0"];
                return;
            }
            else
            {
                email = txt_email.text;
            }
            
            gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
            NSLog(@"%@",gblclass.chck_tezraftr_bene_type);
            
            gblclass.acctitle = txt_name.text;
            //[gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_iban.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_city.text];
            [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
            [gblclass.arr_tezrftar_bene_additn addObject:email];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            
            gblclass.acctitle = txt_name.text;//[dic objectForKey:@"strlblAccountTitle"];
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
            [self presentViewController:vc animated:NO completion:nil];
            
            //21 july2020
//            [self checkinternet];
//            if (netAvailable)
//            {
//                [hud showAnimated:YES];
//                chk_ssl = @"iban";
//                [self SSL_Call];
//            }
        }
        else
        {
            gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
            NSLog(@"%@",gblclass.chck_tezraftr_bene_type);
            
            gblclass.acctitle = txt_name.text;
          //[gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_iban.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_city.text];
            [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
            [gblclass.arr_tezrftar_bene_additn addObject:email];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            
            
            gblclass.acctitle = txt_name.text;//[dic objectForKey:@"strlblAccountTitle"];
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
            [self presentViewController:vc animated:NO completion:nil];
            
            //21 july2020
//            [self checkinternet];
//            if (netAvailable)
//            {
//                [hud showAnimated:YES];
//                chk_ssl = @"iban";
//                [self SSL_Call];
//            }
        }
    }
    else
    {
        NSLog(@"%lu",(unsigned long)txt_acct_numbr.text.length);
        
        NSString* str = [NSString stringWithFormat:@"Please enter %@",err_code];
        NSString* str_invalid = [NSString stringWithFormat:@"Invalid %@",err_code];
        
        NSString* ibann = [txt_acct_numbr.text uppercaseString];
        txt_acct_numbr.text = ibann;
        
       // NSString* str = [NSString stringWithFormat:@"Please enter %@",err_code];
       // NSString* str_invalid = [NSString stringWithFormat:@"Invalid %@",err_code];
        if ([txt_select_branch.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select branch" :@"0"];
        }
        else if([txt_acct_numbr.text isEqualToString:@""])
        {
            [self custom_alert:str :@"0"];
        }
//        else if(txt_acct_numbr.text.length < 12 && [str isEqualToString:@"Please enter account number"])
//        {
//            [self custom_alert:str_invalid :@"0"];
//        }
        else if(txt_acct_numbr.text.length < 24 && [str isEqualToString:@"Please enter iban"])
        {
            [self custom_alert:str_invalid :@"0"];
        }
        else if([txt_contact_num.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter contact number" :@"0"];
        }
        else if(txt_contact_num.text.length <14)
        {
            [self custom_alert:@"Contact number must be 14 digits" :@"0"];
        }
        else if([txt_address.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter address" :@"0"];
        }
        else if([txt_city.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select city" :@"0"];
        }
        else if([txt_relation.text isEqualToString:@""])
        {
            [self custom_alert:@"Please select relation" :@"0"];
        }
        else if (![txt_email.text isEqualToString:@""])
        {
            if (![self validateEmail:[txt_email text]])
            {
                [self custom_alert:@"Invalid email address" :@"0"];
                return;
            }
            else if ([self validateEmail:[txt_email text]])
            {
                email = txt_email.text;
            }
            
            gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
            
            // [gblclass.arr_tezrftar_bene_additn addObject:@"account"];
            
            //  [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            //  [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_city.text];
            [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
            [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
            [gblclass.arr_tezrftar_bene_additn addObject:email];
            
            //              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            //              vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
            //              [self presentViewController:vc animated:NO completion:nil];
            
            [self checkinternet];
            if (netAvailable)
            {
                [hud showAnimated:YES];
//                if ([err_code isEqualToString:@"iban"])
//                {
//                    chk_ssl = @"iban";
//                }
//                else
//                {
                    chk_ssl = @"add_account_title";
//                }
                
                [self SSL_Call];
            }
                         
            
        }
//        else if([txt_email.text isEqualToString:@""])
//        {
//            [self custom_alert:@"Please enter email address" :@"0"];
//        }
        else
        {
            gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
            
           // [gblclass.arr_tezrftar_bene_additn addObject:@"account"];
            
            //  [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
            //  [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
              [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
              [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
              [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
              [gblclass.arr_tezrftar_bene_additn addObject:txt_city.text];
              [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
              [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
              [gblclass.arr_tezrftar_bene_additn addObject:email];
              
//              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//              vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
//              [self presentViewController:vc animated:NO completion:nil];
            
            [self checkinternet];
            if (netAvailable)
            {
                [hud showAnimated:YES];
//                if ([err_code isEqualToString:@"iban"])
//                {
//                    chk_ssl = @"iban";
//                }
//                else
//                {
                    chk_ssl = @"add_account_title";
//                }
                [self SSL_Call];
            }
              
        }
    }
}

- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (void)slideLeft
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_combo_iban:(id)sender
{
    
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
    lbl_header.text = @"Select bank";
    search.hidden = NO; //bank
    search.text = @"";
    search_chck = @"bank";
    
    if (table_iban.isHidden == YES)
    {
        vw_table.hidden = NO;
        table_bank.hidden = NO;
    }
    else
    {
        vw_table.hidden = YES;
        table_bank.hidden = YES;
    }
}

-(IBAction)btn_vw_combo_iban:(id)sender
{
    
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
    search.hidden = YES;
    lbl_header.text = @"Choose account type";
    
    if (table_iban.isHidden==YES)
    {
        vw_table.hidden = NO;
        table_city.hidden = YES;
        table_iban.hidden = NO;
        table_branch.hidden = YES;
        table_relation.hidden = YES;
        table_bank.hidden = YES;
    }
    else
    {
        vw_table.hidden = YES;
        table_city.hidden = YES;
        table_iban.hidden = YES;
        table_branch.hidden = YES;
        table_relation.hidden = YES;
        table_bank.hidden = YES;
    }
}

-(IBAction)btn_combo_branch:(id)sender
{
    
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
    lbl_header.text = @"Select branch";
    
    vw_down_chck = @"1";
    search.hidden = NO;
    search.text = @"";
    search_chck = @"branch";
  //  [table_branch reloadData];
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
    
    if (self.view.frame.origin.y >= 0)
       {
           [self setViewMovedUp:YES];
       }
       else if (self.view.frame.origin.y < 0)
       {
           if ([vw_down_chck isEqualToString:@"1"])
           {
               [self setViewMovedUp:NO];
               vw_down_chck = @"0";
           }
       }
    }
    
    
    NSLog(@"%@",gblclass.chck_tezraftr_bene_type);
    if ([gblclass.chck_tezraftr_bene_type isEqualToString:@"iban"])
    {
        
        if (table_iban.isHidden == YES)
        {
            vw_table.hidden = NO;
            table_bank.hidden = YES;
            table_branch.hidden = NO; //table_bank
            table_city.hidden = YES;
            table_iban.hidden = YES;
            table_relation.hidden = YES;
        }
        else
        {
            vw_table.hidden = YES;
            table_bank.hidden = YES;
            table_branch.hidden = YES; //table_bank
            table_city.hidden = YES;
            table_iban.hidden = YES;
            table_relation.hidden = YES;
        }
    }
    else
    {
        if (table_branch.isHidden==YES)
        {
            vw_table.hidden = NO;
            table_city.hidden = YES;
            table_iban.hidden = YES;
            table_branch.hidden = NO;
            table_relation.hidden = YES;
            table_bank.hidden = YES;
           // [table_branch reloadData];
        }
        else
        {
           vw_table.hidden = YES;
            table_city.hidden = YES;
            table_iban.hidden = YES;
            table_branch.hidden = YES;
            table_relation.hidden = YES;
            table_bank.hidden = YES;
        }
    }
    
}

-(IBAction)btn_combo_city:(id)sender
{
    
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
    lbl_header.text = @"Select city";
    
    vw_down_chck = @"1";
    search.hidden = NO;
    search.text = @"";
    search_chck = @"city";
 //   [table_city reloadData];
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
    
    if (self.view.frame.origin.y >= 0)
       {
           [self setViewMovedUp:YES];
       }
       else if (self.view.frame.origin.y < 0)
       {
           if ([vw_down_chck isEqualToString:@"1"])
           {
               [self setViewMovedUp:NO];
               vw_down_chck = @"0";
           }
       }
    }
    
    if (table_city.isHidden==YES)
    {
        vw_table.hidden = NO;
        table_city.hidden = NO;
        table_iban.hidden = YES;
        table_branch.hidden = YES;
        table_relation.hidden = YES;
        table_bank.hidden = YES;
        // [table_branch reloadData];
    }
    else
    {
        vw_table.hidden = YES;
        table_city.hidden = YES;
        table_iban.hidden = YES;
        table_branch.hidden = YES;
        table_relation.hidden = YES;
        table_bank.hidden = YES;
    }
}

-(IBAction)btn_combo_relation:(id)sender
{
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    
    lbl_header.text = @"Select relation";
    
    
    vw_down_chck = @"1";
    search.hidden = NO;
    search.text = @"";
    search_chck = @"relation";
  //  [table_relation reloadData];
    
    if ([chk_vw isEqualToString:@"1"])
    {
        
    
    if (self.view.frame.origin.y >= 0)
       {
           [self setViewMovedUp:YES];
       }
       else if (self.view.frame.origin.y < 0)
       {
           if ([vw_down_chck isEqualToString:@"1"])
           {
               [self setViewMovedUp:NO];
               vw_down_chck = @"0";
           }
       }
    }
    
    
    if (table_relation.isHidden==YES)
    {
        vw_table.hidden = NO;
        table_city.hidden = YES;
        table_iban.hidden = YES;
        table_branch.hidden = YES;
        table_relation.hidden = NO;
        table_bank.hidden = YES;
        // [table_branch reloadData];
    }
    else
    {
        vw_table.hidden = YES;
        table_city.hidden = YES;
        table_iban.hidden = YES;
        table_branch.hidden = YES;
        table_relation.hidden = YES;
        table_bank.hidden = YES;
    }
}

-(IBAction)btn_close:(id)sender
{
    vw_table.hidden = YES;
    table_iban.hidden = YES;
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(isFiltered)
    {
        return [searchResult count];
    }
    else if (tableView == table_city)
    {
        return [gblclass.arr_tezrftar_city count];
    }
    else if(tableView == table_branch)
    {
        return [branchname count]; //gblclass.arr_tezrftar_branch
    }
    else if(tableView == table_relation)
    {
        return [gblclass.arr_tezraftar_relation count];
    }
    else if(tableView == table_bank)
    {
        return [gblclass.arr_tezrftar_bank count];
    }
    else
    {
        return [arr_acct count]; //arr_acct
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    if(isFiltered)
    {
       // searchResult  arrsearchname
        if ([searchResult count]>0)
        {
            split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
               
            if ([search_chck isEqualToString:@"branch"])
            {
                //Branch Name ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text = [NSString stringWithFormat:@"%@ - %@",[split objectAtIndex:0],[split objectAtIndex:1]];
                label.textColor=[UIColor whiteColor];
                //label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];
            }
            else if ([search_chck isEqualToString:@"city"])
            {
                //City Name ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text = [split objectAtIndex:1];
                label.textColor=[UIColor whiteColor];
                //label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];
            }
            else if ([search_chck isEqualToString:@"relation"])
            {
                //relation Name ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text = [split objectAtIndex:1];
                label.textColor=[UIColor whiteColor];
                //label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];
            }
            else if ([search_chck isEqualToString:@"bank"])
            {
                //relation Name ::
                label=(UILabel*)[cell viewWithTag:2];
                label.text = [split objectAtIndex:1];
                label.textColor=[UIColor whiteColor];
                //label.font=[UIFont systemFontOfSize:12];
                [cell.contentView addSubview:label];
            }
            
            
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];
            
            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            [cell.contentView addSubview:img];
            
        }
    }
    else if (tableView == table_branch)
    {
        //gblclass.arr_tezrftar_branch
       // split = [[branchname objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
            //Branch Name ::
            label=(UILabel*)[cell viewWithTag:2];
//            label.text = [NSString stringWithFormat:@"%@ - %@",[split objectAtIndex:0],[split objectAtIndex:1]];
            label.text = [NSString stringWithFormat:@"%@", [branchname objectAtIndex:indexPath.row]];
            label.textColor=[UIColor whiteColor];
            //label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
        
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];

            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }

            [cell.contentView addSubview:img];
        //
    }
    else if(tableView == table_city)
    {
        split = [[gblclass.arr_tezrftar_city objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
            //City Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split objectAtIndex:1];
            label.textColor=[UIColor whiteColor];
            //label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
        
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];

            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }

            [cell.contentView addSubview:img];
    }
    else if(tableView == table_relation)
    {
        split = [[gblclass.arr_tezraftar_relation objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
            //City Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split objectAtIndex:1];
            label.textColor=[UIColor whiteColor];
            //label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
        
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];

            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }

            [cell.contentView addSubview:img];
    }
    else if(tableView == table_bank)
    {
        split = [[gblclass.arr_tezrftar_bank objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
            //City Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split objectAtIndex:1];
            label.textColor=[UIColor whiteColor];
            //label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
        
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];

            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }

            [cell.contentView addSubview:img];
    }
    else
    {
//        arr_acct
         split = [[arr_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
          //  cell.textLabel.text = [split objectAtIndex:0];//[arr_act_type objectAtIndex:indexPath.row];
            
            //Name ::
            label=(UILabel*)[cell viewWithTag:2];
            label.text = [split objectAtIndex:0];
            label.textColor=[UIColor whiteColor];
            //label.font=[UIFont systemFontOfSize:12];
            [cell.contentView addSubview:label];
        //
        //
            img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];

            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }

            [cell.contentView addSubview:img];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    table_iban.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // Yourstring = [arr_act_type objectAtIndex:indexPath.row];
    
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    //UIStoryboard *mainStoryboard;
    //UIViewController *vc;
    
    if([searchResult count]>0)    //(isFiltered)
    {
            if ([searchResult count] >0)
            {
                split_select = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
                
                if ([search_chck isEqualToString:@"branch"])
                {
                    br_code = [split_select objectAtIndex:0];
                    txt_select_branch.text = [NSString stringWithFormat:@"%@ - %@",[split_select objectAtIndex:0],[split_select objectAtIndex:1]];
                    
//                    br_code = [branchcode objectAtIndex:indexPath.row];
//                    txt_select_branch.text = [NSString stringWithFormat:@"%@",[branchname objectAtIndex:0]];
                    
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
                    myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
                }
                else if ([search_chck isEqualToString:@"city"])
                {
                    gblclass.tz_city = [split_select objectAtIndex:0];
                    txt_city.text = [split_select objectAtIndex:1];
                    
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
                    myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
                }
                else if ([search_chck isEqualToString:@"relation"])
                {
                    relation_code = [split_select objectAtIndex:0];
                    txt_relation.text = [split_select objectAtIndex:1];
                    
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
                    myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
                }
                else if ([search_chck isEqualToString:@"bank"])
                {
                   if ([gblclass.acct_add_type isEqualToString:@"UBL Branch Account"])
                    {
                        [table_branch reloadData];
                       // split_select = [[gblclass.arr_tezrftar_branch objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                        
                         br_code = [split_select objectAtIndex:0];
                         gblclass.beneficiary_Bank = br_code;
                         txt_select_branch.text = [split_select objectAtIndex:1];
                    }
                    else
                    {
                        [table_bank reloadData];
                      //  split_select = [[gblclass.arr_tezrftar_bank objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
                        
                         br_code = [split_select objectAtIndex:0];
                         txt_select_iban_acct.text = [split_select objectAtIndex:1];
                    }
                    
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
                    myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
                }
               
            }
        
        isFiltered = NO;
        [table_branch reloadData];
        [table_city reloadData];
        [table_relation reloadData];
        [table_bank reloadData];
        
        
//        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
//        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];

        
    }
    else if (tableView == table_branch)
    {
        [table_branch reloadData];
       // split = [[gblclass.arr_tezrftar_branch objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
            //QATAR , DOHA
        
//        br_code = [split objectAtIndex:0];  //@"0908";
//        txt_select_branch.text = [NSString stringWithFormat:@"%@ - %@",[split objectAtIndex:0],[split objectAtIndex:1]];// [split objectAtIndex:1]; // @"QATAR , DOHA";//
        
        br_code = [branchcode objectAtIndex:indexPath.row];
        txt_select_branch.text = [branchname objectAtIndex:indexPath.row];
      //[NSString stringWithFormat:@"%@ - %@",[split objectAtIndex:0],[split objectAtIndex:1]];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    else if(tableView == table_iban)
    {
         [table_iban reloadData];
        
//        if ([gblclass.acct_add_type  isEqualToString:@"Other Bank"])
//        {
//            split = [[gblclass.arr_tezrftar_bank objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
//
//            br_code = [split objectAtIndex:0];
//            txt_select_iban_acct.text = [split objectAtIndex:1];
//        }
//        else
//        {
            
         split = [[arr_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
         //txt_iban.text = [split objectAtIndex:0];
        
        NSLog(@"%@",gblclass.acct_add_type);
         if ([[split objectAtIndex:0] isEqualToString:@"Account Number"])
         {
             err_code = @"account number";
             gblclass.chck_tezraftr_bene_type = @"account";
             vw_branch_acct.hidden = NO;
             txt_select_iban_acct.hidden = YES;
             txt_select_branch.placeholder = @"Select Branch";
             txt_acct_numbr.keyboardType = UIKeyboardTypeNumberPad;
             txt_iban.hidden = YES;
             txt_acct_numbr.placeholder = @"Account Number";
             txt_select_branch.text = @"";
             txt_acct_numbr.text = @"";
             [table_branch reloadData];
         }
         else
         {
             err_code = @"iban";
             gblclass.chck_tezraftr_bene_type = @"iban";
             txt_select_iban_acct.hidden = NO;
             txt_iban.hidden = NO;
             txt_select_branch.placeholder = @"Select Branch";
             txt_acct_numbr.placeholder = @"IBAN";
             txt_acct_numbr.keyboardType = UIKeyboardTypeDefault;
             txt_select_branch.text = @"";
             vw_branch_acct.hidden = NO;
             txt_acct_numbr.text = @"";
         }
            
       // }
         
         UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
         UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
         myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
         
    }
    else if(tableView == table_city)
    {
         [table_city reloadData];
         split = [[gblclass.arr_tezrftar_city objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
         gblclass.tz_city = [split objectAtIndex:0];
         txt_city.text = [split objectAtIndex:1];
         
         UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
         UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
         myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
         
    }
    else if(tableView == table_relation)
    {
         [table_relation reloadData];
         split = [[gblclass.arr_tezraftar_relation objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
         relation_code = [split objectAtIndex:0];
         txt_relation.text = [split objectAtIndex:1];
         
         UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
         UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
         myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
         
    }
    else if(tableView == table_bank)
    {
        if ([gblclass.acct_add_type isEqualToString:@"UBL Branch Account"])
        {
            [table_branch reloadData];
             split = [[gblclass.arr_tezrftar_branch objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
             br_code = [split objectAtIndex:0];
             gblclass.beneficiary_Bank = br_code;
             txt_select_branch.text = [split objectAtIndex:1];
        }
        else
        {
            [table_bank reloadData];
             split = [[gblclass.arr_tezrftar_bank objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
             br_code = [split objectAtIndex:0];
             gblclass.beneficiary_Bank = br_code;
             txt_select_iban_acct.text = [split objectAtIndex:1];
        }
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    
    vw_table.hidden = YES;
    table_iban.hidden = YES;
    table_branch.hidden = YES;
    table_city.hidden = YES;
    table_relation.hidden = YES;
    table_bank.hidden = YES;
    search.hidden = YES;
    
    isFiltered = NO;
    [search resignFirstResponder];
    [searchResult removeAllObjects];
    
}

//UISearchDisplayController
- (void)searchDisplayController:(UISearchController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    //tableView.backgroundColor =[UIColor redColor];
    //[UIColor colorWithPatternImage:[UIImage imageNamed:@"default.png"]];
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    isFiltered = YES;
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
 
            if(searchText.length==0)
            {
                isFiltered=NO;
            }

            else
            {
                
                isFiltered=YES;
                arrsearchresult = [[NSMutableArray alloc]init];
                arrsearchname = [[NSMutableArray alloc]init];
                searchResult = [[NSMutableArray alloc] init];
                
                
                if ([search_chck isEqualToString:@"branch"])
                {
                    for ( NSString * str in gblclass.arr_tezrftar_branch)
                    {
                        NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                        
                        NSInteger index;
                        if(stringRange.location != NSNotFound)
                        {
                            index = [arrfullname indexOfObject:str];
                            [searchResult addObject:str];
                        }
                    }
                    
                    [table_branch reloadData];
                }
                else if( [search_chck isEqualToString:@"city"])
                {
                    for ( NSString * str in gblclass.arr_tezrftar_city)
                    {
                        NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                        
                        NSInteger index;
                        if(stringRange.location != NSNotFound)
                        {
                            index = [arrfullname indexOfObject:str];
                            [searchResult addObject:str];
                        }
                    }
                    
                    [table_city reloadData];
                }
                else if( [search_chck isEqualToString:@"relation"])
                {
                    for ( NSString * str in gblclass.arr_tezraftar_relation)
                    {
                        NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                        
                        NSInteger index;
                        if(stringRange.location != NSNotFound)
                        {
                            index = [arrfullname indexOfObject:str];
                            [searchResult addObject:str];
                        }
                    }
                    
                    [table_relation reloadData];
                }
                else if( [search_chck isEqualToString:@"bank"])
                {
                    for ( NSString * str in gblclass.arr_tezrftar_bank)
                    {
                        NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
                        
                        NSInteger index;
                        if(stringRange.location != NSNotFound)
                        {
                            index = [arrfullname indexOfObject:str];
                            [searchResult addObject:str];
                        }
                    }
                    
                    [table_bank reloadData];
                }
            }

//    [table_branch reloadData];
}

//-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//    [self.tableviewcompany resignFirstResponder];
//}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isFiltered=NO;
   // [self.tableviewcompany reloadData];

}



-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    vw_down_chck=@"1";
    [txt_acct_numbr resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_select_branch resignFirstResponder];
    [txt_select_iban_acct resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_city resignFirstResponder];
    [txt_name resignFirstResponder];
    [txt_address resignFirstResponder];
    [txt_contact_num resignFirstResponder];
    [txt_relation resignFirstResponder];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS; // 999,999,999.99
    BOOL stringIsValid;
    
    if ([textField isEqual:txt_acct_numbr])
    {
        NSCharacterSet * set;
        if ([err_code isEqualToString:@"iban"])
        {
            MAX_DIGITS = 24;
            set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        }
        else
        {
            MAX_DIGITS = 13;
            set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        }
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_contact_num])
    {
        MAX_DIGITS=14;
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_name])
    {
        MAX_DIGITS=30;

        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];

        if ([string rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            textField.text=@"";
            return 0;
        }
    }
    else if ([textField isEqual:txt_email])
    {
        MAX_DIGITS=50;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@._ "] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
    }
    else if ([textField isEqual:txt_address])
    {
        MAX_DIGITS=60;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
    }
    else if ([textField isEqual:txt_iban])
    {
        MAX_DIGITS=24;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        
//        NSString *unfilteredString = @"!@#$%^&*()_+|abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
//        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"] invertedSet];
//        NSString *resultString = [[unfilteredString componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
//        NSLog (@"Result: %@", resultString);
        
        
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
//
//             NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];

          //   return [string isEqualToString:filtered];
        
        
        
        NSLog(@"%@",textField.text);
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound)
        {
//            NSString *resultString = [[textField.text componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
//            NSLog (@"Result: %@", resultString);
            
            NSLog(@"%@",textField.text);
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text = @"";
            
            return 0;
        }
    }
    
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
//    [hud showAnimated:YES];
//    [hud hideAnimated:YES afterDelay:130];
//    [self.view addSubview:hud];
   // [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"next"])
        {
            //[self ValidateTezraftaarPayeeAddition:@""];
            chk_ssl=@"";
        }
        else if ([chk_ssl isEqualToString:@"load_payee_list"])
        {
            //[self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
            chk_ssl=@"";
        }
        else if([chk_ssl isEqualToString:@"add_account_title"])
            {
        //        if ([err_code isEqualToString:@"iban"])
        //        {
        //            [self get_IBAN_FTTitle:@""];
        //        }
        //        else
        //        {
                    [self AddAccountTitleFetch:@""];
        //        }
                
                chk_ssl=@"";
            }
            else if([chk_ssl isEqualToString:@"iban"])
            {
                [self get_IBAN_FTTitle:@""];
                chk_ssl=@"";
            }
            else if([chk_ssl isEqualToString:@"loadbranches"])
            {
                [self Load_Branches:@""];
                chk_ssl = @"";
            }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"next"])
    {
       // [self ValidateTezraftaarPayeeAddition:@""];
        [self AddAccountTitleFetch:@""];
        chk_ssl=@"";
    }
    else if ([chk_ssl isEqualToString:@"load_payee_list"])
    {
        NSLog(@"%@",gblclass.arr_payee_list);
        
       // [self  load_pay_list_to:[gblclass.arr_payee_list objectAtIndex:1]];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"add_account_title"])
    {
        if ([err_code isEqualToString:@"iban"])
        {
            [self get_IBAN_FTTitle:@""];
        }
        else
        {
            [self AddAccountTitleFetch:@""];
        }
        
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"iban"])
    {
        [self get_IBAN_FTTitle:@""];
        chk_ssl=@"";
    }
    else if([chk_ssl isEqualToString:@"loadbranches"])
    {
        [self Load_Branches:@""];
        chk_ssl = @"";
    }
    
    
    chk_ssl=@"";
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) Load_Branches:(NSString *)strIndustry
{
    
    companytype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //GetIBFTBankName
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
//                                [encrypt encrypt_Data: gblclass.M3sessionid],
//                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                [encrypt encrypt_Data:gblclass.Udid], nil]
//
//                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
//                                                                   @"strSessionId",
//                                                                   @"IP",
//                                                                   @"Device_ID", nil]];
    
  //LoadBranches
  //    countrybranch = @"1";
  //    fttitle
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1"], //countrybranch
                                    [encrypt encrypt_Data:@"1000"], //gblclass.UserType
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
    
                                                              forKeys:[NSArray arrayWithObjects:@"strCountry",
                                                                       @"StrUserType",
                                                                       @"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];

    [manager POST:@"LoadBranches" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  companytype = [[dic objectForKey:@"dtUBLBankList"] objectForKey:@"Table1"];
                  
                  for(int i=0;i<[companytype count];i++)
                  {
                      NSDictionary *dict = [companytype objectAtIndex:i];
//                      if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
//                      {
//                          [branchname addObject:[dict objectForKey:@"bank_name"]];
//                          [branchcode addObject:[dict objectForKey:@"bank_imd"]];
//                      }
//                      else
//                      {
                          [branchname addObject:[dict objectForKey:@"brdetail"]];
                          [branchcode addObject:[dict objectForKey:@"brcode"]];
//                      }
                  }
                  
                  [hud hideAnimated:YES];
                  [table_branch reloadData];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              [hud hideAnimated:YES];
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}

-(void) AddAccountTitleFetch:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
  //  gblclass.strAccessKey_for_add=acc_type;
    
    NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
    //[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"UBLPK"],//acc_type UBL
                                [encrypt encrypt_Data:@"United Bank Limited"],//bankname
                                [encrypt encrypt_Data:@"588974"],//bankimd
                                [encrypt encrypt_Data:br_code],//fetchtitlebranchcode
                                [encrypt encrypt_Data:txt_acct_numbr.text],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccType",
                                                                   @"strBankName",
                                                                   @"strBankImd",
                                                                   @"strBranchCode",
                                                                   @"strAccountNumber",
                                                                   @"strIBFTFormat",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager POST:@"AddAccountTitleFetch" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSMutableArray* a = [[NSMutableArray alloc] init];
              
              responsecode = [dic objectForKey:@"Response"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  
                 // [self ValidateTezraftaarPayeeAddition:@""];
                //NSLog(@"Done IBFT load branch");
//                  gblclass.acc_type=acc_type;
//                  gblclass.bankname=bankname;
//                  gblclass.bankimd=bankimd;
//                  gblclass.fetchtitlebranchcode=fetchtitlebranchcode;
//                  gblclass.acc_number=self.txtaccountnumber.text;
                    gblclass.acctitle=[dic objectForKey:@"strlblAccountTitle"];
                  
                  gblclass.arr_tezrftar_bene_additn = [[NSMutableArray alloc] init];
                  
                  // [gblclass.arr_tezrftar_bene_additn addObject:@"account"];
                  
                  //  [gblclass.arr_tezrftar_bene_additn addObject:txt_name.text];
                  //  [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:txt_acct_numbr.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:txt_contact_num.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:txt_address.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:txt_city.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:relation_code];
                  [gblclass.arr_tezrftar_bene_additn addObject:txt_relation.text];
                  [gblclass.arr_tezrftar_bene_additn addObject:email];

                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
//                  vw_fetch_title.hidden = NO;
//                  btn_combo.enabled = NO;
//                  img_combo.hidden = YES;
//                  _txtaccountnumber.enabled = NO;
//
//                  txt_accnt_title_2.text=[dic objectForKey:@"strlblAccountTitle"];
//                  txt_accnt_number.text=[dic objectForKey:@"strlblAccountNumber"];
//                  txt_accnt_nick.text=[dic objectForKey:@"strtxtAccountNick"];
                  
  
//                  if ([txt_accnt_title_2.text isEqualToString:@""] || txt_accnt_title_2.text.length ==0)
//                  {
//                      txt_accnt_title_2.enabled = YES;
//                  }
//                  else
//                  {
//                      txt_accnt_title_2.enabled = NO;
//                      txt_accnt_title_2.borderStyle = UITextBorderStyleNone;
//                      [txt_accnt_title_2 setBackgroundColor:[UIColor clearColor]];
//                  }
//
//                  [txt_accnt_title_2 adjustsFontSizeToFitWidth];
//                  [txt_accnt_nick adjustsFontSizeToFitWidth];
//
//                  lbl_h1_1.hidden = NO;
//                  txt_accnt_title_2.hidden = NO;
//
//                  vw_ibft_relation.hidden = YES;
//                  btn_cancel.hidden = YES;
//                  btn_submit.hidden = YES;
//                  btn_add_record.hidden = NO;
//
//                  _txtaccountnumber.hidden = YES;
//                  lbl_acct_numb_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.text = @"Account No.";
//                  lbl_acct_numb_brnch.text = _txtaccountnumber.text;
//
                  
//                  vw_fetch_title.frame = CGRectMake(0, 178, vw_fetch_title.frame.size.width, vw_fetch_title.frame.size.height);
                  
//                  if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
//                  {
//                      arr_ibft_relation_load=[[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
//
//                      vw_ibft_relation.hidden = NO;
//                      vw_IBFT.hidden = YES;
//                      _txtaccountnumber.text = txt_acct_number_ibft.text;
//                      _txtaccountnumber.hidden = NO;
//
//                      for(dic in arr_ibft_relation_load)
//                      {
//
//                          NSString* IBFT_RELATION_DESC=[dic objectForKey:@"IBFT_RELATION_DESC"];
//                          if ([IBFT_RELATION_DESC isEqual:@""])
//                          {
//                              IBFT_RELATION_DESC=@"N/A";
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//
//                          NSString* IBFT_RELATION_ID=[dic objectForKey:@"IBFT_RELATION_ID"];
//                          if ([IBFT_RELATION_ID isEqual:@""])
//                          {
//                              IBFT_RELATION_ID=@"N/A";
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//
//
//                          NSString *bbb = [a componentsJoinedByString:@"|"];
//                          [arr_ibft_relation addObject:bbb];
//
//
//                          //NSLog(@"%@", bbb);
//                          [a removeAllObjects];
//                      }
//
//                      [table_ibft_relation reloadData];
                      
//                  }
                  
                  
              }
              else if ([responsecode isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self custom_alert:resstring :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
//              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
//
//              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//              [alert addAction:ok];
//              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
    if (buttonIndex == 0)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
 
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}



-(void) get_IBAN_FTTitle:(NSString *)strIndustry{
    
     @try {
         
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
  //  gblclass.strAccessKey_for_add=acc_type;
    
//    NSLog(@"%@",gblclass.mainurl1);
//    NSLog(@"%@",gblclass.arr_tezrftar_bene_additn);
    //[gblclass.arr_tezrftar_bene_additn objectAtIndex:0]
    
//    NSLog(@"%@",txt_acct_numbr.text);
      
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:@"UBL"],//acc_type  UBLPK
                                [encrypt encrypt_Data:txt_select_iban_acct.text],//bankname
                                [encrypt encrypt_Data:@"588974"],//bankimd
                                [encrypt encrypt_Data:br_code],//fetchtitlebranchcode
                                [encrypt encrypt_Data:[txt_acct_numbr.text uppercaseString]],  //txt_iban
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"strAccType",
                                                                   @"strBankName",
                                                                   @"strBankImd",
                                                                   @"strBranchCode",
                                                                   @"strAccountNumber",
                                                                   @"strIBFTFormat",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager POST:@"getIBANFTTitle" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             NSMutableArray* a = [[NSMutableArray alloc] init];
              
              responsecode = [dic objectForKey:@"Response"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  
                 // [self ValidateTezraftaarPayeeAddition:@""];
                //  NSLog(@"Done IBFT load branch");
//                  gblclass.acc_type=acc_type;
//                  gblclass.bankname=bankname;
//                  gblclass.bankimd=bankimd;
//                  gblclass.fetchtitlebranchcode=fetchtitlebranchcode;
//                  gblclass.acc_number=self.txtaccountnumber.text;
                    gblclass.acctitle = [dic objectForKey:@"strlblAccountTitle"];
                  
                   
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
//                  vw_fetch_title.hidden = NO;
//                  btn_combo.enabled = NO;
//                  img_combo.hidden = YES;
//                  _txtaccountnumber.enabled = NO;
//
//                  txt_accnt_title_2.text=[dic objectForKey:@"strlblAccountTitle"];
//                  txt_accnt_number.text=[dic objectForKey:@"strlblAccountNumber"];
//                  txt_accnt_nick.text=[dic objectForKey:@"strtxtAccountNick"];
                  
  
//                  if ([txt_accnt_title_2.text isEqualToString:@""] || txt_accnt_title_2.text.length ==0)
//                  {
//                      txt_accnt_title_2.enabled = YES;
//                  }
//                  else
//                  {
//                      txt_accnt_title_2.enabled = NO;
//                      txt_accnt_title_2.borderStyle = UITextBorderStyleNone;
//                      [txt_accnt_title_2 setBackgroundColor:[UIColor clearColor]];
//                  }
//
//                  [txt_accnt_title_2 adjustsFontSizeToFitWidth];
//                  [txt_accnt_nick adjustsFontSizeToFitWidth];
//
//                  lbl_h1_1.hidden = NO;
//                  txt_accnt_title_2.hidden = NO;
//
//                  vw_ibft_relation.hidden = YES;
//                  btn_cancel.hidden = YES;
//                  btn_submit.hidden = YES;
//                  btn_add_record.hidden = NO;
//
//                  _txtaccountnumber.hidden = YES;
//                  lbl_acct_numb_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.hidden = NO;
//                  lbl_acct_num_txt_brnch.text = @"Account No.";
//                  lbl_acct_numb_brnch.text = _txtaccountnumber.text;
//
                  
//                  vw_fetch_title.frame = CGRectMake(0, 178, vw_fetch_title.frame.size.width, vw_fetch_title.frame.size.height);
                  
//                  if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
//                  {
//                      arr_ibft_relation_load=[[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
//
//                      vw_ibft_relation.hidden = NO;
//                      vw_IBFT.hidden = YES;
//                      _txtaccountnumber.text = txt_acct_number_ibft.text;
//                      _txtaccountnumber.hidden = NO;
//
//                      for(dic in arr_ibft_relation_load)
//                      {
//
//                          NSString* IBFT_RELATION_DESC=[dic objectForKey:@"IBFT_RELATION_DESC"];
//                          if ([IBFT_RELATION_DESC isEqual:@""])
//                          {
//                              IBFT_RELATION_DESC=@"N/A";
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_DESC];
//                          }
//
//                          NSString* IBFT_RELATION_ID=[dic objectForKey:@"IBFT_RELATION_ID"];
//                          if ([IBFT_RELATION_ID isEqual:@""])
//                          {
//                              IBFT_RELATION_ID=@"N/A";
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//                          else
//                          {
//                              [a addObject:IBFT_RELATION_ID];
//                          }
//
//
//                          NSString *bbb = [a componentsJoinedByString:@"|"];
//                          [arr_ibft_relation addObject:bbb];
//
//
//                          //NSLog(@"%@", bbb);
//                          [a removeAllObjects];
//                      }
//
//                      [table_ibft_relation reloadData];
                      
//                  }
                  
                  
              }
              else if ([responsecode isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                //  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else if ([responsecode isEqualToString:@"-1"])
              {
                  [hud hideAnimated:YES];
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"Tz_Review"];
                  [self presentViewController:vc animated:NO completion:nil];
                 //[self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                 // return ;
              }
              else
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self custom_alert:resstring :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Please try again later."  :@"0"];
          }];
     }
         @catch (NSException *exception)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please try again later."  :@"0"];
        }
}


#define kOFFSET_FOR_KEYBOARD 115.0

-(void)keyboardWillShow
{
   
//    if ([Keyboard_show isEqualToString:@"1"])
//    {
           // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
 // }
}

-(void)keyboardWillHide
{
   
//        if ([Keyboard_show isEqualToString:@"1"])
//           {
    
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
    }
    //       }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        chk_vw = @"1";
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
    }
    else
    {
        chk_vw = @"0";
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    NSLog(@"%@",textField.description);
    
//    if (textField == txt_acct_numbr)
//    {
//        Keyboard_show = @"0";
//        NSLog(@"account num");
//    }
//    else if(textField == txt_email)
//    {
//        Keyboard_show = @"1";
//        NSLog(@"email");
//    }
   return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    
//    NSLog(@"%@",);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)textViewTapped:(UITapGestureRecognizer *)tap {
     //DO SOMTHING
     
     NSLog(@"TAPPP");
     NSLog(@"%@",tap);
     vw_down_chck = @"1";
 }

 #pragma mark - Gesture recognizer delegate

 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
 }

 

@end
