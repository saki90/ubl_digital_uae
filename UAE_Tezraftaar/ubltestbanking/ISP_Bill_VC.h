//
//  ISP_Bill_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 28/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface ISP_Bill_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_combo_frm;
    IBOutlet UITextField* txt_combo_bill_names;
    IBOutlet UILabel* lbl_bill_mangmnt;
    IBOutlet UILabel* lbl_mobile_no;
    IBOutlet UILabel* lbl_company_name;
    IBOutlet UITextField* txt_amnts;
    IBOutlet UITextField* txt_comment;
    IBOutlet UILabel *lbl_nick;
    IBOutlet UILabel* lbl_t_pin;
    IBOutlet UITextField* txt_t_pin;
    IBOutlet UIButton* btn_submit;
    IBOutlet UILabel* lbl_status;
    IBOutlet UILabel* lbl_outstanding_amnt;
    IBOutlet UILabel* txt_outstanding_amnt;
    IBOutlet UIView* ui_view;
    IBOutlet UITextField* txt_Utility_bill_name;
    
    IBOutlet UITextField* txt_amnt_with;
    IBOutlet UITextField* txt_comment_with;
    IBOutlet UITextField* txt_t_pin_with;
    IBOutlet UILabel* lbl_t_pin_with;
    
    IBOutlet UITableView* table_from;
    IBOutlet UITableView* table_to;
    IBOutlet UITableView* table_bill;
    
    IBOutlet UIView* vw_from;
    IBOutlet UIView* vw_to;
    IBOutlet UIView* vw_bill;
    IBOutlet UIView* vw_table;
    IBOutlet UIButton* btn_review_pay_chck;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property(nonatomic,strong) IBOutlet UILabel* lbl_balance;
@property(nonatomic,strong) IBOutlet UILabel* lbl_acct_frm;

-(IBAction)btn_back:(id)sender;
-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_combo_frm:(id)sender;
-(IBAction)btn_bill_names:(id)sender;



@end

