//
//  Features_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface Features_VC : UIViewController
{
    IBOutlet UIScrollView* vw_scrl;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIImageView* pro_img;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

//@property (weak, nonatomic) IBOutlet UIWebView *webView;

-(IBAction)btn_back:(id)sender;
-(IBAction)btn_feature_details:(id)sender;

@end

