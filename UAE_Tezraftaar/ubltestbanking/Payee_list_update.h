//
//  Payee_list_update.h
//  ubltestbanking
//
//  Created by Mehmood on 27/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Payee_list_update : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_nick;
    IBOutlet UITextField* txt_email;
    IBOutlet UITextField* txt_mobile;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

