//
//  ISP_Bill_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 28/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "ISP_Bill_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import "Encrypt.h"


@interface ISP_Bill_VC ()<NSURLConnectionDataDelegate>
{
    NSMutableArray* arr_Get_ISPBill_load;
    NSMutableArray* arr_ISP_bill;
    GlobalStaticClass* gblclass;
    NSDictionary* dic;
    MBProgressHUD* hud;
    UIAlertController  *alert;
    
    NSArray* split;
    NSArray* split_bill;
    
    NSString* payfrom_acct_id;
    NSString* Payfrm_selected_item_text;
    NSString* Payfrm_selected_item_value;
    NSString* str_amnt;
    NSString* lbl_customer_id;
    NSString* lbl_compny_name;
    NSString* str_comment;
    NSString* str_access_key;
    NSString* str_Tc_access_key;
    NSString* str_ccy;
    NSString* str_TT_accesskey;
    NSString* str_tt_id;
    NSString* str_tt_name;
    NSString* str_regt_consumer_id;
    NSString* str_build;
    NSString* lb_customer_id;
    NSString* lbl_customer_nick;
    NSString* status;
    NSString* str_bill_id;
    NSString* str_due_datt;
    NSString* PAYMENT_STATUS;
    NSNumber* IS_PAYBLE_1;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    
    
    NSString* d_bill_id;
    NSString* d_bill_payable_amnt;
    int d_bill_btn_pay_enable;
    NSString* d_bill_lbl_consumer_id;
    NSString* d_bill_txt_amnt;
    NSString* d_bill_txt_consumer_id;
    NSString* d_bill_lbl_company_name;
    
    
    NSString* str_commnt;
    NSString* str_amnts;
    APIdleManager* timer_class;
    NSMutableArray* arr_bill_names;
    
    NSMutableArray* arr_bill_UBLBP;
    NSMutableArray* arr_bill_OB;
    NSMutableArray* arr_bill_ISP;
    NSMutableArray* arr_bill_UBP;
    NSMutableArray* arr_get_bill_load;
    NSMutableArray* search;
    NSString* search_flag;
    NSArray* splits;
    UILabel* label;
    UIImageView* img;
    NSString* lbl_balc;
    NSString* logout_chck;
    NSString* chk_ssl;
    NSNumberFormatter *numberFormatter;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation ISP_Bill_VC
@synthesize transitionController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.transitionController = [[TransitionDelegate alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    arr_Get_ISPBill_load=[[NSMutableArray alloc] init];
    arr_ISP_bill=[[NSMutableArray alloc] init];
    arr_bill_names=[[NSMutableArray alloc] init];
    encrypt = [[Encrypt alloc] init];
    
    numberFormatter = [NSNumberFormatter new];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    arr_bill_UBLBP=[[NSMutableArray alloc] init];
    arr_bill_UBP=[[NSMutableArray alloc] init];
    arr_bill_OB=[[NSMutableArray alloc] init];
    arr_bill_ISP=[[NSMutableArray alloc] init];
    arr_get_bill_load=[[NSMutableArray alloc] init];
    search=[[NSMutableArray alloc] init];
    gblclass.arr_review_withIn_myAcct=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    
    ssl_count = @"0";
    search_flag=@"0";
    txt_combo_bill_names.delegate=self;
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    ui_view.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    lbl_t_pin.hidden=YES;
    txt_t_pin.hidden=YES;
    lbl_t_pin_with.hidden=YES;
    txt_t_pin_with.hidden=YES;
    ui_view.hidden=YES;
    
    txt_amnts.delegate=self;
    txt_comment.delegate=self;
    txt_comment_with.delegate=self;
    txt_t_pin.delegate=self;
    txt_t_pin_with.delegate=self;
    txt_amnt_with.delegate=self;
    
    
    //[btn_submit setBackgroundColor: [UIColor colorWithRed:1/255.0f green:112/255.0f blue:181/255.0f alpha:1.0f]];
    txt_combo_frm.text= gblclass.is_default_acct_id_name;
    
    Payfrm_selected_item_text=gblclass.is_default_acct_id_name;
    Payfrm_selected_item_value=gblclass.is_default_acct_no;
    payfrom_acct_id=gblclass.is_default_acct_id;
    
    
    
    arr_bill_names=[[NSMutableArray alloc] initWithArray:@[@"Bill Management",@"Prepaid Services",@"Online Shopping",@"Transfer within My Account",@"Transfer Fund to Anyone",@"Masterpass"]];
    
    if ([gblclass.arr_bill_isp count]>0)
    {
        
        splits=[[gblclass.arr_bill_isp objectAtIndex:[gblclass.indexxx integerValue]] componentsSeparatedByString: @"|"];
        
        txt_Utility_bill_name.text= gblclass.pay_combobill_name;
        
        //    txt_combo_bill_names.text=[gblclass.arr_bill_elements objectAtIndex:3];
        //    lbl_company_name.text=[gblclass.arr_bill_elements objectAtIndex:0];
        //    lbl_mobile_no.text=[gblclass.arr_bill_elements objectAtIndex:2];
        
        
        txt_combo_bill_names.text=[splits objectAtIndex:3];
        lbl_company_name.text=[splits objectAtIndex:0];
        lbl_mobile_no.text=[splits objectAtIndex:2];
        lbl_nick.text=[splits objectAtIndex:3];
        
        
        
        str_tt_name=[splits objectAtIndex:0];
        str_tt_id=[splits objectAtIndex:1];
        lbl_customer_id=[splits objectAtIndex:2];
        str_due_datt=[splits objectAtIndex:4];
        str_amnt=[splits objectAtIndex:5];
        status=[splits objectAtIndex:6];
        str_bill_id=[splits objectAtIndex:7];
        str_access_key=[splits objectAtIndex:8];
        str_regt_consumer_id=[splits objectAtIndex:9];
        lbl_compny_name=[splits objectAtIndex:10];
        str_TT_accesskey=[splits objectAtIndex:8];
        lbl_customer_nick=[splits objectAtIndex:3];
        str_Tc_access_key=[splits objectAtIndex:12];
        PAYMENT_STATUS=[splits objectAtIndex:13];
        IS_PAYBLE_1=[splits objectAtIndex:14];
        
        
        //lbl_amnts.text=[splits objectAtIndex:5];
        //lbl_due_dat.text=[splits objectAtIndex:4];
        lbl_company_name.text=[splits objectAtIndex:0];
        lbl_mobile_no.text=[splits objectAtIndex:2];
        lbl_status.text=[splits objectAtIndex:6];
        txt_combo_bill_names.text=[splits objectAtIndex:3];
        
        
        
        if ([[splits objectAtIndex:10] isEqual:@"WITRIBE"] || [[splits objectAtIndex:10] isEqual:@"CYBERNET"]
            || [[splits objectAtIndex:10] isEqual:@"WORLDCALL"] || [[splits objectAtIndex:10] isEqual:@"QUBEE"]
            || [[splits objectAtIndex:10] isEqual:@"PTCL Evo Postpaid"])
        {
            txt_amnt_with.enabled=NO;
        }
        else
        {
            txt_amnt_with.enabled=YES;
        }
        
    }
    
    
    if ([status isEqualToString:@"PAID"] || [status isEqualToString:@"paid"]
        || [status isEqualToString:@"IN PROCESS"] || [status isEqualToString:@"in process"]
        || [status isEqualToString:@"-"])
    {
        btn_review_pay_chck.enabled=NO;
    }
    else
    {
        btn_review_pay_chck.enabled=YES;
    }
    
    
    
    search_flag=@"0";
    [table_to reloadData];
    
    [self checkinternet];
    if (netAvailable)
    {
        
        chk_ssl=@"load";
        [self SSL_Call];
        
        //  [self Detail_ISP_Bill_loadData];
    }
    
    vw_table.hidden=YES;
    ui_view.backgroundColor=[UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    
    NSAttributedString * imageStr1;
    NSDecimalNumber *number ;
    if ([gblclass.arr_transfer_within_acct count]>0)
    {
    
    split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    _lbl_acct_frm.text=[split objectAtIndex:1];
    
    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    attach1.bounds = CGRectMake(10, 8, 20, 10);
    imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    
    NSString* balc;
    
    if ([split objectAtIndex:6]==0)
    {
        balc=@"0";
    }
    else
    {
        balc=[split objectAtIndex:6];
    }
    
        NSString *numberString = [NSString stringWithFormat:@"%@", [split objectAtIndex:6]];
        number = [NSDecimalNumber decimalNumberWithString:numberString];
        
    
    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    
    [mutableAttriStr1 appendAttributedString:imageStr1];
    _lbl_balance.attributedText = mutableAttriStr1;
    
    lbl_balc=gblclass.is_default_m3_balc;
        
    }
    else
    {
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString *mystring =[NSString stringWithFormat:@"%@",@"0"];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        
        //lbl_balance.attributedText = mutableAttriStr1;
        
        
        lbl_balc=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[formatter stringFromNumber:number]];
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void) Get_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:@"ISP,OB,UBP,UBLBP"],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abcd"], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strTcAccessKey",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              //             NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              arr_Get_ISPBill_load= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
              
              
              arr_bill_UBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
              arr_bill_OB= [(NSDictionary *)[dic objectForKey:@"outdtDatasetOB"] objectForKey:@"Table"];
              arr_bill_ISP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetISP"] objectForKey:@"GnbBillList"];
              arr_bill_UBLBP= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBLBP"] objectForKey:@"GnbBillList"];
              
              
              
              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
              NSMutableArray* a;
              a=[[NSMutableArray alloc] init];
              
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              // For ISP :::::
              
              for (dic in arr_bill_ISP) //arr_get_bill
              {
                  
                  NSString* TT_NAME=[dic objectForKey:@"TT_NAME"];
                  if (TT_NAME.length==0 || [TT_NAME isEqualToString:@" "] || [TT_NAME isEqualToString:nil])
                  {
                      TT_NAME=@"N/A";
                      [a addObject:TT_NAME];
                  }
                  else
                  {
                      [a addObject:TT_NAME];
                  }
                  
                  NSString* TT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                  if (TT_ID.length==0 || [TT_ID isEqualToString:@" "])
                  {
                      TT_ID=@"N/A";
                      [a addObject:TT_ID];
                  }
                  else
                  {
                      [a addObject:TT_ID];
                  }
                  
                  NSString* CONSUMER_NO=[dic objectForKey:@"CONSUMER_NO"];
                  if (CONSUMER_NO.length==0 || [CONSUMER_NO isEqualToString:@" "] || [CONSUMER_NO isEqualToString:nil])
                  {
                      CONSUMER_NO=@"N/A";
                      [a addObject:CONSUMER_NO];
                  }
                  else
                  {
                      [a addObject:CONSUMER_NO];
                  }
                  
                  NSString* NICK=[dic objectForKey:@"NICK"];
                  if (NICK.length==0 || [NICK isEqualToString:@" "] || [NICK isEqualToString:nil])
                  {
                      NICK=@"N/A";
                      [a addObject:NICK];
                  }
                  else
                  {
                      [a addObject:NICK];
                  }
                  
                  NSString* DUE_DATE=[dic objectForKey:@"DUE_DATE"];
                  if (DUE_DATE.length==0 || [DUE_DATE isEqualToString:@" "] || [DUE_DATE isEqualToString:nil])
                  {
                      DUE_DATE=@"N/A";
                      [a addObject:DUE_DATE];
                  }
                  else
                  {
                      [a addObject:DUE_DATE];
                  }
                  
                  NSString* PAYABLE_AMOUNT=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PAYABLE_AMOUNT"]];
                  if (PAYABLE_AMOUNT.length==0 || [PAYABLE_AMOUNT isEqualToString:@" "] || [PAYABLE_AMOUNT isEqualToString:nil])
                  {
                      PAYABLE_AMOUNT=@"N/A";
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  else
                  {
                      [a addObject:PAYABLE_AMOUNT];
                  }
                  
                  NSString* PAYMENT_STATUS_DESC=[dic objectForKey:@"PAYMENT_STATUS_DESC"];
                  if (PAYMENT_STATUS_DESC.length==0 || [PAYMENT_STATUS_DESC isEqualToString:@" "] || [PAYMENT_STATUS_DESC isEqualToString:nil])
                  {
                      PAYMENT_STATUS_DESC=@"N/A";
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS_DESC];
                  }
                  
                  NSString* BILL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"BILL_ID"]];
                  if (BILL_ID.length==0 || [BILL_ID isEqualToString:@" "] || [BILL_ID isEqualToString:nil])
                  {
                      BILL_ID=@"N/A";
                      [a addObject:BILL_ID];
                  }
                  else
                  {
                      [a addObject:BILL_ID];
                  }
                  
                  NSString* ACCESS_KEY=[dic objectForKey:@"ACCESS_KEY"];
                  if (ACCESS_KEY.length==0 || [ACCESS_KEY isEqualToString:@" "] || [ACCESS_KEY isEqualToString:nil])
                  {
                      ACCESS_KEY=@"N/A";
                      [a addObject:ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:ACCESS_KEY];
                  }
                  
                  NSString* REGISTERED_CONSUMERS_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_CONSUMERS_ID"]];
                  if (REGISTERED_CONSUMERS_ID.length==0 || [REGISTERED_CONSUMERS_ID isEqualToString:@" "] || [REGISTERED_CONSUMERS_ID isEqualToString:nil])
                  {
                      REGISTERED_CONSUMERS_ID=@"N/A";
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_CONSUMERS_ID];
                  }
                  
                  NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                  if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || [COMPANY_NAME isEqualToString:nil])
                  {
                      COMPANY_NAME=@"N/A";
                      [a addObject:COMPANY_NAME];
                  }
                  else
                  {
                      [a addObject:COMPANY_NAME];
                  }
                  
                  NSString* REGISTERED_ACCOUNT_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"REGISTERED_ACCOUNT_ID"]];
                  
                  
                  if (REGISTERED_ACCOUNT_ID.length==0 || [REGISTERED_ACCOUNT_ID isEqualToString:@" "] || [REGISTERED_ACCOUNT_ID isEqualToString:nil])
                  {
                      REGISTERED_ACCOUNT_ID=@"N/A";
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  else
                  {
                      [a addObject:REGISTERED_ACCOUNT_ID];
                  }
                  
                  
                  NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                  if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || [TC_ACCESS_KEY isEqualToString:nil])
                  {
                      TC_ACCESS_KEY=@"N/A";
                      [a addObject:TC_ACCESS_KEY];
                  }
                  else
                  {
                      [a addObject:TC_ACCESS_KEY];
                  }
                  
                  NSString* PAYMENT_STATUS1=[dic objectForKey:@"PAYMENT_STATUS"];
                  if (PAYMENT_STATUS1.length==0 || [PAYMENT_STATUS1 isEqualToString:@" "] || [PAYMENT_STATUS1 isEqualToString:nil])
                  {
                      PAYMENT_STATUS1=@"N/A";
                      [a addObject:PAYMENT_STATUS1];
                  }
                  else
                  {
                      [a addObject:PAYMENT_STATUS1];
                  }
                  
                  NSString* IS_PAYBLE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"IS_PAYBLE"]];
                  if (IS_PAYBLE.length==0 || [IS_PAYBLE isEqualToString:@" "] || [IS_PAYBLE isEqualToString:nil])
                  {
                      IS_PAYBLE=@"N/A";
                      [a addObject:IS_PAYBLE];
                  }
                  else
                  {
                      [a addObject:IS_PAYBLE];
                  }
                  
                  
                  NSString* bbb = [a componentsJoinedByString:@"|"];
                  //NSLog(@"%@", bbb);
                  [arr_get_bill_load addObject:bbb];
                  [a removeAllObjects];
                  
              }
              
              // ISP END ::::
              
              
              [hud hideAnimated:YES];
              [table_to reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==table_from)
    {
        return [gblclass.arr_transfer_within_acct count];
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            return [search count];
        }
        else
        {
            return [arr_get_bill_load count];
        }
        
    }
    else
    {
        return [arr_bill_names count];
    }
    
    //count number of row from counting array hear cateGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        //        cell.textLabel.text = [split objectAtIndex:0];
        //        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        NSString *numberString = [NSString stringWithFormat:@"%@", [split objectAtIndex:6]];
        NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:numberString];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [numberFormatter stringFromNumber:number];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
        
    }
    else if(tableView==table_to)
    {
        if ([search_flag isEqualToString:@"1"])
        {
            split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [split_bill objectAtIndex:3];
            cell.textLabel.textColor=[UIColor whiteColor];
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
        else
        {
            split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            cell.textLabel.text = [split_bill objectAtIndex:3];
            cell.textLabel.textColor=[UIColor whiteColor];
            cell.textLabel.font=[UIFont systemFontOfSize:12];
        }
    }
    else
    {
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        cell.textLabel.text = [split_bill objectAtIndex:0];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [table_from reloadData];
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        txt_combo_frm.text=[split objectAtIndex:0];
        Payfrm_selected_item_text=[split objectAtIndex:0];
        Payfrm_selected_item_value=[split objectAtIndex:1];
        payfrom_acct_id=[split objectAtIndex:2];
        _lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:balc];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        _lbl_balance.attributedText = mutableAttriStr1;
        
        lbl_balc=balc;
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        
        vw_table.hidden=YES;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        
    }
    else if(tableView==table_to)
    {
        
        if ([search_flag isEqualToString:@"1"])
        {
            split_bill = [[search objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        }
        else
        {
            split_bill = [[arr_get_bill_load objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        }
        
        
        str_tt_name=[split_bill objectAtIndex:0];
        str_tt_id=[split_bill objectAtIndex:1];
        lbl_customer_id=[split_bill objectAtIndex:2];
        str_due_datt=[split_bill objectAtIndex:4];
        str_amnt=[split_bill objectAtIndex:5];
        status=[split_bill objectAtIndex:6];
        str_bill_id=[split_bill objectAtIndex:7];
        str_access_key=[split_bill objectAtIndex:8];
        str_regt_consumer_id=[split_bill objectAtIndex:9];
        lbl_compny_name=[split_bill objectAtIndex:10];
        str_TT_accesskey=[split_bill objectAtIndex:12];
        lbl_customer_nick=[split_bill objectAtIndex:3];
        str_Tc_access_key=[split_bill objectAtIndex:12];
        PAYMENT_STATUS=[split_bill objectAtIndex:13];
        IS_PAYBLE_1=[split_bill objectAtIndex:14];
        
        
        //lbl_amnts.text=[split_bill objectAtIndex:5];
        //lbl_due_dat.text=[split_bill objectAtIndex:4];
        lbl_company_name.text=[split_bill objectAtIndex:0];
        lbl_mobile_no.text=[split_bill objectAtIndex:2];
        lbl_status.text=[split_bill objectAtIndex:6];
        txt_combo_bill_names.text=[split_bill objectAtIndex:3];
        
        
        
        if ([[split_bill objectAtIndex:8] isEqual:@"WITRIBE"])
        {
            txt_amnt_with.enabled=NO;
        }
        else
        {
            txt_amnt_with.enabled=YES;
        }
        
        search_flag=@"0";
        [table_to reloadData];
        
        vw_to.hidden=YES;
        table_to.hidden=YES;
        txt_t_pin.text=@"";
        txt_comment.text=@"";
        
        
        [hud showAnimated:YES];
        
        [self Detail_ISP_Bill_loadData];
    }
    else
    {
        
        split_bill = [[arr_bill_names objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        gblclass.pay_combobill_name=[split_bill objectAtIndex:0];
        
        switch (indexPath.row)
        {
            case 0:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"utility_bill"]; //utility_bill   //bill_all
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 1:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_voucher"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 2:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"online_shopping"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 3:
                
                
                
                break;
                
            case 4:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_within_acct"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 5:
                
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"pay_other_acct"];
                [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            case 6:
                
                //                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //                vc = [storyboard instantiateViewControllerWithIdentifier:@"ISP_bill"];
                //    [self presentViewController:vc animated:NO completion:nil];
                
                break;
                
            default: break;
                
        }
        
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=YES;
        table_to.hidden=YES;
        txt_t_pin.text=@"";
        
    }
    
    
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [txt_comment resignFirstResponder];
    
    
    vw_bill.hidden=YES;
    table_bill.hidden=YES;
    vw_to.hidden=YES;
    vw_from.hidden=YES;
    table_from.hidden=YES;
    table_to.hidden=YES;
    vw_table.hidden=YES;
    
    [txt_combo_bill_names resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_t_pin_with resignFirstResponder];
    [txt_outstanding_amnt resignFirstResponder];
    [txt_comment_with resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_amnts resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_amnt_with resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_t_pin_with resignFirstResponder];
    
    
    // [self keyboardWillHide];
}


#define kOFFSET_FOR_KEYBOARD 45.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //   //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_combo_frm:(id)sender
{
    
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        return;
    }
    
    
    
    if (table_from.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_from.hidden=NO;
        table_from.hidden=NO;
        table_to.hidden=YES;
        vw_to.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_from.hidden=YES;
        vw_table.hidden=YES;
    }
}


-(IBAction)btn_Utility_bill_names:(id)sender
{
    if (table_bill.isHidden==YES)
    {
        vw_bill.hidden=NO;
        table_bill.hidden=NO;
        vw_from.hidden=YES;
        table_from.hidden=YES;
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    else
    {
        table_bill.hidden=YES;
        vw_bill.hidden=YES;
    }
    
}


-(IBAction)btn_bill_names:(id)sender
{
    if (table_to.isHidden==YES)
    {
        vw_bill.hidden=YES;
        table_bill.hidden=YES;
        vw_to.hidden=NO;
        table_to.hidden=NO;
        table_from.hidden=YES;
        vw_from.hidden=YES;
        [table_to bringSubviewToFront:self.view];
    }
    else
    {
        table_to.hidden=YES;
        vw_to.hidden=YES;
    }
    
    txt_amnt_with.text=@"";
    txt_amnts.text=@"";
    txt_comment_with.text=@"";
    txt_comment.text=@"";
    txt_t_pin_with.text=@"";
    txt_t_pin.text=@"";
    txt_amnts.enabled=YES;
    ui_view.hidden=YES;
}

-(IBAction)btn_submit:(id)sender
{
    
    //    if (PAYMENT_STATUS== "U")
    //    {
    //
    //        if (IS_PAYBLE == "0")
    //        {
    //            “Can’t Pay”
    //        }
    //
    //        else if (PAYABLE_AMOUNT > 0){
    //            “Payable”
    //        }
    //
    //        Else
    //        {
    //            “Can’t Pay”
    //        }
    //    }
    //
    //    Else
    //    {
    //        “Can’t Pay”
    //    }
    
    
    if ([txt_combo_bill_names.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Select Bill" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        [hud hideAnimated:YES];
        
        return;
    }
    else if(ui_view.hidden==YES)
    {
        if ([txt_amnt_with.text isEqualToString:@""] || [txt_amnt_with.text isEqualToString:nil])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if([txt_comment_with.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        
        
    }
    else
    {
        
        if ([txt_amnts.text isEqualToString:@""] || [txt_amnts.text isEqualToString:nil])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Amount" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if([txt_comment.text isEqualToString:@""])
        {
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Comment" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
    }
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    //      PAYMENT_STATUS=@"U";
    if ([PAYMENT_STATUS isEqualToString:@"U"])
    {
        //        IS_PAYBLE_1=@"1";
        if ([IS_PAYBLE_1  isEqual: @"0"])
        {
            //NSLog(@"return");
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            [hud hideAnimated:YES];
            
            return;
        }
        else if (IS_PAYBLE_1 > 0) //@"0"
        {
            //pay
            //NSLog(@"Pay");
            
            if (ui_view.hidden==YES)
            {
                if (lbl_t_pin_with.hidden==YES && txt_t_pin_with.hidden==YES)
                {
                    lbl_t_pin_with.hidden=NO;
                    txt_t_pin_with.hidden=NO;
                    [hud hideAnimated:YES];
                    
                    return;
                }
                else if ([txt_t_pin_with.text isEqualToString:@""])
                {
                    
                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [hud hideAnimated:YES];
                    return;
                    
                }
                else
                {
                    [self Pay_Bill:@""];
                    return;
                }
                
                
            }
            else
            {
                
                if (lbl_t_pin.hidden==YES && txt_t_pin.hidden==YES)
                {
                    lbl_t_pin.hidden=NO;
                    txt_t_pin.hidden=NO;
                    [hud hideAnimated:YES];
                    
                }
                else if ([txt_t_pin.text isEqualToString:@""])
                {
                    
                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter T-Pin" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [hud hideAnimated:YES];
                    return;
                    
                }
                else
                {
                    [self Pay_Bill:@""];
                }
            }
            
        }
        else
        {
            
            [hud hideAnimated:YES];
            //cannt pay
            //NSLog(@"cannt pay");
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else
    {
        [hud hideAnimated:YES];
        //cantt pay
        //NSLog(@"cannt pay");
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"YOU CANNOT PAY" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void) Pay_Bill:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    arr_Get_ISPBill_load=[[NSMutableArray alloc] init];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    
    //NSLog(@"%@", gblclass.user_id);
    //NSLog(@"%@", payfrom_acct_id);
    //NSLog(@"%@", Payfrm_selected_item_text);
    //NSLog(@"%@", Payfrm_selected_item_value);
    //NSLog(@"%@", str_amnt);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_compny_name);
    //NSLog(@"%@", txt_comment.text);
    //NSLog(@"%@", str_access_key);
    //NSLog(@"%@", str_Tc_access_key);//str_TT_accesskey
    //NSLog(@"%@", gblclass.base_currency);
    //NSLog(@"%@", txt_t_pin.text);
    //NSLog(@"%@", str_TT_accesskey);
    //NSLog(@"%@", str_tt_id);
    //NSLog(@"%@", str_regt_consumer_id);
    //NSLog(@"%@", str_bill_id);
    //NSLog(@"%@", lbl_customer_id);
    //NSLog(@"%@", lbl_customer_nick);
    //NSLog(@"%@", uuid);
    
    
    
    if (ui_view.isHidden==TRUE)
    {
        str_amnts=txt_amnt_with.text;
        str_commnt=txt_comment_with.text;
    }
    else
    {
        str_amnts=txt_amnts.text;
        str_commnt=txt_comment.text;
    }
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:payfrom_acct_id],
                                [encrypt encrypt_Data:Payfrm_selected_item_text],
                                [encrypt encrypt_Data:Payfrm_selected_item_value],
                                [encrypt encrypt_Data:str_amnts],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:lbl_compny_name],
                                [encrypt encrypt_Data:str_commnt],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:str_Tc_access_key],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:txt_t_pin.text],
                                [encrypt encrypt_Data:str_TT_accesskey],
                                [encrypt encrypt_Data:str_tt_id],
                                [encrypt encrypt_Data:str_regt_consumer_id],
                                [encrypt encrypt_Data:str_bill_id],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:lbl_customer_nick],
                                [encrypt encrypt_Data:uuid], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"strtxtPin",
                                                                   @"strTTAccessKey",
                                                                   @"strTT_ID",
                                                                   @"strRegisteredConsumersId",
                                                                   @"strBillId",
                                                                   @"strlbCustomerId",
                                                                   @"strlblConsumerNick",
                                                                   @"strGuid", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //          NSError *error;
              //          NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  [hud hideAnimated:YES];
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  [self presentViewController:vc animated:NO completion:nil];
              }
              else
              {
                  [hud hideAnimated:YES];
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
          }];
}


-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert5 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert5 show];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//    if (buttonIndex == 0)
//    {
//        //Do something
//        //NSLog(@"1");
//    }
//    else if(buttonIndex == 1)
//    {
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
//        [self presentModalViewController:vc animated:YES];
//    }
//
//}


-(void)Detail_ISP_Bill_loadData
{
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    arr_Get_ISPBill_load=[[NSMutableArray alloc] init];
    
    
    //NSLog(@"%@",gblclass.user_id);
    //NSLog(@"%@",str_tt_id);
    //NSLog(@"%@",str_access_key);
    //NSLog(@"%@",lbl_company_name.text);
    //NSLog(@"%@",lbl_customer_id);
    
    
    //NSLog(@"%@",gblclass.M3sessionid);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:str_tt_id],
                                [encrypt encrypt_Data:str_access_key],
                                [encrypt encrypt_Data:lbl_company_name.text],
                                [encrypt encrypt_Data:lbl_customer_id],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTT_ID",
                                                                   @"access_key",
                                                                   @"lblCompanyName",
                                                                   @"lblCustomerID",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"DetailISPBillLoadData" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //          NSError *error;
              //          NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  logout_chck=@"1";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  d_bill_id=[dic objectForKey:@"strBillId"];
                  d_bill_payable_amnt=[dic objectForKey:@"strPayableAmount"];
                  d_bill_btn_pay_enable=[[dic objectForKey:@"btnPay"] intValue];
                  d_bill_lbl_company_name=[dic objectForKey:@"strlblConsumer"];
                  d_bill_txt_amnt=[dic objectForKey:@"strlbltxtAmount"];
                  d_bill_lbl_consumer_id=[dic objectForKey:@"strlblCustomerID"];
                  d_bill_lbl_company_name=[dic objectForKey:@"strlblCompanyName"];
                  
                  lbl_outstanding_amnt.text=[dic objectForKey:@"strlbltxtAmount"];
                  txt_outstanding_amnt.text=[dic objectForKey:@"strPayableAmount"];
                  
                  str_bill_id=[dic objectForKey:@"strBillId"];
                  
                  //NSLog(@"%d",d_bill_btn_pay_enable);
                  
                  if (d_bill_btn_pay_enable==0)
                  {
                      //** btn_review_pay_chck.enabled=NO;
                  }
                  else
                  {
                      btn_review_pay_chck.enabled=YES;
                  }
                  
                  
                  //NSLog(@"%@",d_bill_lbl_company_name);
                  
                  
                  if ([d_bill_txt_amnt isEqualToString:@""])
                  {
                      //NSLog(@"%@",d_bill_txt_amnt);
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Wateen"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          ui_view.hidden=NO;
                          return ;
                      }
                      
                      
                      //10051111 ptcl evo
                      //1005 qubee
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Cybernet Bill Payment"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"PTCL Evo Postpaid"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Witribe Bill Payment"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"WorldCall Bill Payment"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Wateen"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          ui_view.hidden=NO;
                          return ;
                      }
                      
                  }
                  else
                  {
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Wateen"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          ui_view.hidden=NO;
                          return ;
                      }
                      
                      
                      if ([d_bill_lbl_company_name isEqualToString:@"PTCL Vfone / Evo Prepaid"])
                      {
                          //NSLog(@"%@",d_bill_txt_amnt);
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=NO;
                          return ;
                      }
                      
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Qubee Bill Payment"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Witribe Bill Payment"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          txt_amnt_with.text=d_bill_payable_amnt;
                          ui_view.hidden=YES;
                          return ;
                      }
                      
                      if ([d_bill_lbl_company_name rangeOfString:@"Wateen"].location==NSNotFound)
                      {
                          
                      }
                      else
                      {
                          ui_view.hidden=NO;
                          return ;
                      }
                      
                  }
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
              [hud hideAnimated:YES];
          }
     
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:Call_center_msg :@"0"];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
          }];
    
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];                   });
    
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [search  removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", substring];
    
    search= [NSMutableArray arrayWithArray: [arr_get_bill_load filteredArrayUsingPredicate:resultPredicate]];
    
    //    }
    
    
    table_to.hidden=NO;
    [table_to reloadData];
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    if ([theTextField isEqual:txt_amnt_with])
    {
        
        MAX_DIGITS=11;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
    }
    else if ([theTextField isEqual:txt_amnts])
    {
        MAX_DIGITS=11;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        
    }
    //    else if ([theTextField isEqual:txt_combo_bill_names])
    //    {
    //
    //        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "] invertedSet];
    //
    //        if ([theTextField.text rangeOfCharacterFromSet:set].location == NSNotFound)
    //        {
    //            //NSLog(@"NO SPECIAL CHARACTER");
    //
    //            search_flag=@"1";
    //            [self   searchAutocompleteEntriesWithSubstring:txt_combo_bill_names.text];
    //
    //            //return [textField.text stringByReplacingCharactersInRange:range withString:set];
    //        }
    //        else
    //        {
    //            //NSLog(@"HAS SPECIAL CHARACTER");
    //
    //            return 0;
    //        }
    //    }
    
    
    
    if ([theTextField isEqual:txt_comment])
    {
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    if ([theTextField isEqual:txt_comment_with])
    {
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
    }
    
    
    //
    //    if ([theTextField isEqual:txt_t_pin])
    //    {
    //
    //        MAX_DIGITS=4;
    //
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //
    //        }
    //
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    
    return YES ;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //    //NSLog(@"text changed: %@", theTextField.text);
    //    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    //    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    //    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText intValue]]];
    
    if ([theTextField isEqual:txt_amnt_with])
    {
        txt_amnt_with.text=formattedOutput;
    }
    else if ([theTextField isEqual:txt_amnts])
    {
        txt_amnts.text=formattedOutput;
    }
    
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}

-(IBAction)btn_review:(id)sender
{
    [hud showAnimated:YES];
    gblclass.reviewCommentFieldText = txt_comment.placeholder;
    [txt_combo_bill_names resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_t_pin_with resignFirstResponder];
    [txt_outstanding_amnt resignFirstResponder];
    [txt_comment_with resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_amnts resignFirstResponder];
    [txt_comment resignFirstResponder];
    [txt_amnt_with resignFirstResponder];
    [txt_t_pin resignFirstResponder];
    [txt_t_pin_with resignFirstResponder];
    
    
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
}

-(IBAction)btn_Edit_Bill:(id)sender
{
    gblclass.Edit_bill_type=@"ISP";
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"edit_bill"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_Delete_Bill:(id)sender
{
    UIAlertView *alert4 = [[UIAlertView alloc] initWithTitle:@"Confirm Delete"
                                                     message:@"Are you sure you want to delete ISP bill ?"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    alert4.alertViewStyle = UIAlertViewStyleDefault;
    
    //UIAlertViewStylePlainTextInput;
    
    [alert4 show];
    
    // [self Delete_Bills:@""];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        if ([logout_chck isEqualToString:@"1"])
        {
            logout_chck=@"0";
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
            
            //[self mob_App_Logout:@""];
        }
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        [hud showAnimated:YES];
        [self Delete_Bills:@""];
    }
}



-(void) Delete_Bills:(NSString *)strIndustry
{
    
    @try {
        
        //allCompany = [[NSMutableArray alloc] init];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:str_tt_id],
                                                                       [encrypt encrypt_Data:lbl_customer_id],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.token],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"UserId",@"tt_id",@"consumerNo",@"Device_ID",@"Token",@"strSessionId",@"IP", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"DeleteBills" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //          NSError *error;
                  //          NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //   arr_get_bill= [(NSDictionary *)[dic objectForKey:@"outdtDatasetUBP"] objectForKey:@"GnbBillList"];
                  //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
                  [hud hideAnimated:YES];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      logout_chck=@"1";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:@"ISP bill deleted successfully"
                                                                      delegate:self
                                                             cancelButtonTitle:@"Ok"
                                                             otherButtonTitles:nil, nil];
                      
                      [alert3 show];
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",error);
                  //NSLog(@"%@",task);
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception.reason  preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}




-(void) Pay_Bill_ISP:(NSString *)strIndustry
{
    
    //allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //   NSString *uuid = [[NSUUID UUID] UUIDString];
    
    
    NSString *amounts = [[gblclass.arr_values objectAtIndex:6] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                                                   [encrypt encrypt_Data:amounts],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:10]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:11]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:12]],
                                                                   [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:13]],
                                                                   [encrypt encrypt_Data:str_access_key],
                                                                   [encrypt encrypt_Data:gblclass.Udid],
                                                                   [encrypt encrypt_Data:gblclass.token],
                                                                   [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"PayFromAccountID",
                                                                   @"cmbPayFromSelectedItemText",
                                                                   @"cmbPayFromSelectedItemValue",
                                                                   @"strtxtAmount",
                                                                   @"strlblCustomerID",
                                                                   @"strlblCompanyName",
                                                                   @"strtxtComments",
                                                                   @"strAccessKey",
                                                                   @"strTCAccessKey",
                                                                   @"strlblTransactionType",
                                                                   @"strCCY",
                                                                   @"tcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"PayBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              //            NSArray *arr = (NSArray *)responseObject;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //NSLog(@"Response %@",[dic objectForKey:@"Response"]);
              gblclass.Is_Otp_Required=[dic objectForKey:@"IsOtpRequired"];
              gblclass.rev_transaction_type=[dic objectForKey:@"lblTransactionType"];
              
              NSString* IsOtpRequired = [dic objectForKey:@"IsOtpRequired"];
              NSString* lblTransactionType = [dic objectForKey:@"lblTransactionType"];
              
              if (IsOtpRequired ==(NSString *) [NSNull null])
              {
                  IsOtpRequired = @"";
              }
              
              
              if (lblTransactionType ==(NSString *) [NSNull null])
              {
                  lblTransactionType = @"";
              }
              
              
              //              {
              //                  IsOtpRequired = "<null>";
              //                  OtpMessage = "<null>";
              //                  Response = "-1";
              //                  lblTransAmount = "<null>";
              //                  lblTransactionType = "<null>";
              //                  strReturnMessage = "Please enter an amount between PKR 500.00 and PKR 5,000.00.";
              //              }
              
              
              
              [hud hideAnimated:YES];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  logout_chck=@"1";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  [hud hideAnimated:YES];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //    [self pay_Bill_Confirm_Submit:@""];
                  
              }
              else if ([IsOtpRequired isEqualToString:@"1"])
              {
                  [hud hideAnimated:YES];
                  
                  UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                   message:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]]
                                                                  delegate:self
                                                         cancelButtonTitle:@"Ok"
                                                         otherButtonTitles:nil, nil];
                  
                  [alert2 show];
                  
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([IsOtpRequired isEqualToString:@"-1"])
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",error);
              //NSLog(@"%@",task);
              
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}


///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        @try {
            
            
            if ([chk_ssl isEqualToString:@"load"])
            {
                chk_ssl=@"";
                [self Detail_ISP_Bill_loadData];
                
                
                return;
            }
            else if ([chk_ssl isEqualToString:@"logout"])
            {
                chk_ssl=@"";
                [self mob_App_Logout:@""];
                
                return;
            }
            
            double frm_balc,to_balc;
            
            PAYMENT_STATUS=@"U";
            if ([PAYMENT_STATUS isEqualToString:@"U"])
            {
                IS_PAYBLE_1=@"1";
                if ([IS_PAYBLE_1  isEqual: @"0"])
                {
                    [self.connection cancel];
                    [self custom_alert:@"YOU CANNOT PAY" :@"0"];
                    
                    [hud hideAnimated:YES];
                    
                    return;
                }
                else if (IS_PAYBLE_1 >0 )  //[@"0" integerValue]
                {
                    
                    [gblclass.arr_review_withIn_myAcct removeAllObjects];
                    
                    if (ui_view.isHidden==YES)
                    {
                        if ([txt_amnt_with.text isEqualToString:@""])
                        {
                            
                            
                            [self custom_alert:@"Enter Amount" :@"0"];
                            
                            [hud hideAnimated:YES];
                            
                            return;
                        }
                        else if ([txt_amnt_with.text isEqualToString:@"0"])
                        {
                            
                            [self custom_alert:@"Enter valid Amount" :@"0"];
                            [hud hideAnimated:YES];
                            
                            return;
                        }
                        else if([txt_comment_with.text isEqualToString:@""])
                        {
                            txt_comment_with.text=@"";
                        }
                        
                        str_amnts=txt_amnt_with.text;
                        str_commnt=txt_comment_with.text;
                        
                        
                        
                        
                        
                        // NSString* lbl_balced =  lbl_balc;
                        NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                        frm_balc=[lbl_balc doubleValue];
                        to_balc=[txt_amnt2 doubleValue];
                        
                        
                        if (to_balc>frm_balc)
                        {
                            [hud hideAnimated:YES];
                            
                            
                            
                            [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                            
                            
                            return;
                        }
                        
                        
                    }
                    else
                    {
                        if ([txt_amnts.text isEqualToString:@""])
                        {
                            
                            [self custom_alert:@"Enter Amount" :@"0"];
                            [hud hideAnimated:YES];
                            
                            return;
                        }
                        else if([txt_comment.text isEqualToString:@""])
                        {
                            txt_comment.text=@"";
                        }
                        
                        str_amnts=txt_amnts.text;
                        str_commnt=txt_comment.text;
                    }
                    
                    [gblclass.arr_review_withIn_myAcct addObject:lbl_company_name.text];
                    [gblclass.arr_review_withIn_myAcct addObject:lbl_customer_id];
                    [gblclass.arr_review_withIn_myAcct addObject:txt_combo_frm.text];
                    [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
                    [gblclass.arr_review_withIn_myAcct addObject:str_amnts];
                    [gblclass.arr_review_withIn_myAcct addObject:str_commnt];
                    
                    
                    gblclass.check_review_acct_type=@"isp";
                    
                    if (ui_view.isHidden==TRUE)
                    {
                        if ([txt_amnt_with.text isEqualToString:@""])
                        {
                            
                            [self custom_alert:@"Enter Amount" :@"0"];
                            
                            [hud hideAnimated:YES];
                            
                            return;
                        }
                        else if([txt_comment_with.text isEqualToString:@""])
                        {
                            txt_comment_with.text=@"";
                        }
                        
                        str_amnts=txt_amnt_with.text;
                        str_commnt=txt_comment_with.text;
                        
                        
                        // NSString* lbl_balced =  lbl_balc;
                        NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                        frm_balc=[lbl_balc doubleValue];
                        to_balc=[txt_amnt2 doubleValue];
                        
                        
                        if (to_balc>frm_balc)
                        {
                            [hud hideAnimated:YES];
                            
                            
                            [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                            
                            return;
                        }
                        
                    }
                    else
                    {
                        if ([txt_amnts.text isEqualToString:@""])
                        {
                            
                            
                            [self custom_alert:@"Enter Amount" :@"0"];
                            
                            [hud hideAnimated:YES];
                            
                            return;
                        }
                        else if([txt_comment.text isEqualToString:@""])
                        {
                            txt_comment.text=@"";
                        }
                        
                        str_amnts=txt_amnts.text;
                        str_commnt=txt_comment.text;
                        
                        // NSString* lbl_balced =  lbl_balc;
                        NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                        frm_balc=[lbl_balc doubleValue];
                        to_balc=[txt_amnt2 doubleValue];
                        
                        
                        
                        if (to_balc>frm_balc)
                        {
                            [hud hideAnimated:YES];
                            
                            
                            
                            [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                            
                            
                            return;
                        }
                        
                        
                    }
                    
                    [gblclass.arr_values removeAllObjects];
                    
                    [gblclass.arr_values addObject:gblclass.user_id];
                    [gblclass.arr_values addObject:gblclass.M3sessionid];
                    [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                    [gblclass.arr_values addObject:payfrom_acct_id];
                    [gblclass.arr_values addObject:Payfrm_selected_item_text];
                    [gblclass.arr_values addObject:Payfrm_selected_item_value];
                    [gblclass.arr_values addObject:str_amnts];
                    [gblclass.arr_values addObject:lbl_customer_id];
                    [gblclass.arr_values addObject:lbl_compny_name];
                    [gblclass.arr_values addObject:str_commnt];
                    [gblclass.arr_values addObject:str_access_key];
                    [gblclass.arr_values addObject:str_Tc_access_key];
                    [gblclass.arr_values addObject:@""];
                    [gblclass.arr_values addObject:gblclass.base_currency];
                    [gblclass.arr_values addObject:@"1111"];
                    [gblclass.arr_values addObject:str_TT_accesskey];
                    [gblclass.arr_values addObject:str_tt_id];
                    [gblclass.arr_values addObject:str_regt_consumer_id];
                    [gblclass.arr_values addObject:str_bill_id];
                    [gblclass.arr_values addObject:lbl_customer_id];
                    [gblclass.arr_values addObject:lbl_customer_nick];
                    
                    
                    [self Pay_Bill_ISP:@""];
                    
                }
                else
                {
                    
                    [hud hideAnimated:YES];
                    //cannt pay
                    //NSLog(@"cannt pay");
                    
                    [self custom_alert:@"YOU CANNOT PAY" :@"0"];
                }
                [self.connection cancel];
            }
            else
            {
                
                [hud hideAnimated:YES];
                //cannt pay
                //NSLog(@"cannt pay");
                
                [self custom_alert:@"YOU CANNOT PAY" :@"0"];
            }
            
            
        }
        @catch (NSException *exception)
        {
            
            [hud hideAnimated:YES];
            //cannt pay
            //NSLog(@"cannt pay");
            
            [self custom_alert:@"Try again later." :@"0"];
        }
        
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    @try {
        
        [self.connection cancel];
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self Detail_ISP_Bill_loadData];
            
            //            [self.connection cancel];
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
            
            return;
        }
        
        double frm_balc,to_balc;
        
        PAYMENT_STATUS=@"U";
        if ([PAYMENT_STATUS isEqualToString:@"U"])
        {
            IS_PAYBLE_1=@"1";
            if ([IS_PAYBLE_1  isEqual: @"0"])
            {
                [self custom_alert:@"YOU CANNOT PAY" :@"0"];
                [hud hideAnimated:YES];
                return;
            }
            else if (IS_PAYBLE_1 >0 )  //[@"0" integerValue]
            {
                
                [gblclass.arr_review_withIn_myAcct removeAllObjects];
                
                if (ui_view.isHidden==YES)
                {
                    if ([txt_amnt_with.text isEqualToString:@""])
                    {
                        [self custom_alert:@"Enter Amount" :@"0"];
                        [hud hideAnimated:YES];
                        return;
                    }
                    else if ([txt_amnt_with.text isEqualToString:@"0"])
                    {
                        [self custom_alert:@"Enter valid Amount" :@"0"];
                        [hud hideAnimated:YES];
                        return;
                    }
                    else if([txt_comment_with.text isEqualToString:@""])
                    {
                        txt_comment_with.text=@"";
                    }
                    
                    str_amnts=txt_amnt_with.text;
                    str_commnt=txt_comment_with.text;
                    
                    
                    // NSString* lbl_balced =  lbl_balc;
                    NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                    frm_balc=[lbl_balc doubleValue];
                    to_balc=[txt_amnt2 doubleValue];
                    
                    
                    if (to_balc>frm_balc)
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                        return;
                    }
                    
                }
                else
                {
                    if ([txt_amnts.text isEqualToString:@""])
                    {
                        [self custom_alert:@"Enter Amount" :@"0"];
                        [hud hideAnimated:YES];
                        return;
                    }
                    else if([txt_comment.text isEqualToString:@""])
                    {
                        txt_comment.text=@"";
                    }
                    
                    str_amnts=txt_amnts.text;
                    str_commnt=txt_comment.text;
                }
                
                [gblclass.arr_review_withIn_myAcct addObject:lbl_company_name.text];
                [gblclass.arr_review_withIn_myAcct addObject:lbl_customer_id];
                [gblclass.arr_review_withIn_myAcct addObject:txt_combo_frm.text];
                [gblclass.arr_review_withIn_myAcct addObject:_lbl_acct_frm.text];
                [gblclass.arr_review_withIn_myAcct addObject:str_amnts];
                [gblclass.arr_review_withIn_myAcct addObject:str_commnt];
                
                
                gblclass.check_review_acct_type=@"isp";
                
                if (ui_view.isHidden==TRUE)
                {
                    if ([txt_amnt_with.text isEqualToString:@""])
                    {
                        [self custom_alert:@"Enter Amount" :@"0"];
                        [hud hideAnimated:YES];
                        return;
                    }
                    else if([txt_comment_with.text isEqualToString:@""])
                    {
                        txt_comment_with.text=@"";
                    }
                    
                    str_amnts=txt_amnt_with.text;
                    str_commnt=txt_comment_with.text;
                    
                    
                    // NSString* lbl_balced =  lbl_balc;
                    NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                    frm_balc=[lbl_balc doubleValue];
                    to_balc=[txt_amnt2 doubleValue];
                    
                    
                    if (to_balc>frm_balc)
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                        return;
                    }
                    
                }
                else
                {
                    if ([txt_amnts.text isEqualToString:@""])
                    {
                        [self custom_alert:@"Enter Amount" :@"0"];
                        [hud hideAnimated:YES];
                        return;
                    }
                    else if([txt_comment.text isEqualToString:@""])
                    {
                        txt_comment.text=@"";
                    }
                    
                    str_amnts=txt_amnts.text;
                    str_commnt=txt_comment.text;
                    
                    // NSString* lbl_balced =  lbl_balc;
                    NSString* txt_amnt2 = [str_amnts stringByReplacingOccurrencesOfString:@"," withString:@""];
                    frm_balc=[lbl_balc doubleValue];
                    to_balc=[txt_amnt2 doubleValue];
                    
                    //   0334-2434034
                    
                    if (to_balc>frm_balc)
                    {
                        [hud hideAnimated:YES];
                        [self custom_alert:@"Available balance is less then bill amount" :@"0"];
                        return;
                    }
                    
                    
                }
                
                [gblclass.arr_values removeAllObjects];
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:payfrom_acct_id];
                [gblclass.arr_values addObject:Payfrm_selected_item_text];
                [gblclass.arr_values addObject:Payfrm_selected_item_value];
                [gblclass.arr_values addObject:str_amnts];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_compny_name];
                [gblclass.arr_values addObject:str_commnt];
                [gblclass.arr_values addObject:str_access_key];
                [gblclass.arr_values addObject:str_Tc_access_key];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:gblclass.base_currency];
                [gblclass.arr_values addObject:@"1111"];
                [gblclass.arr_values addObject:str_TT_accesskey];
                [gblclass.arr_values addObject:str_tt_id];
                [gblclass.arr_values addObject:str_regt_consumer_id];
                [gblclass.arr_values addObject:str_bill_id];
                [gblclass.arr_values addObject:lbl_customer_id];
                [gblclass.arr_values addObject:lbl_customer_nick];
                
                
                [self Pay_Bill_ISP:@""];
                
            }
            else
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"YOU CANNOT PAY" :@"0"];
            }
            //            [self.connection cancel];
        }
        else
        {
            //            [self.connection cancel];
            [hud hideAnimated:YES];
            //cannt pay
            //NSLog(@"cannt pay");
            
            [self custom_alert:@"YOU CANNOT PAY" :@"0"];
        }
        
        
    }
    @catch (NSException *exception)
    {
        //        [self.connection cancel];
        [hud hideAnimated:YES];
        //cannt pay
        //NSLog(@"cannt pay");
        
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
    //    [self.connection cancel];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES]; 
            [self custom_alert:statusString :@"0"];
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

