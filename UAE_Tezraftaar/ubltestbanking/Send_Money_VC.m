//
//  Send_Money_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 13/02/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "Send_Money_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import "Encrypt.h"


@interface Send_Money_VC ()
{
    UIStoryboard *storyboard;
    UIViewController *vc;
    
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    GlobalStaticClass* gblclass;
    APIdleManager* timer_class;
    NSString *detectionString;
    
    NSString* acct_to_chck;
    NSString* str_acct_name_to;
    NSString* str_acct_no_to;
    NSString* str_acct_id_frm;
    MBProgressHUD* hud;
    UILabel* label;
    NSArray* split;
    UIImageView* img;
    
    NSNumber *number;
    NSDictionary* dic;
    NSString* chk_ssl;
    // NSString* str_acct_id_frm;
    NSString* mask_acct_no;
    Encrypt* encrypt;
    NSString* ssl_count;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@property(nonatomic,strong) IBOutlet UITextField* txt_acct_to;


@end

@implementation Send_Money_VC
@synthesize transitionController;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    // [hud showAnimated:YES];
    // [self.view addSubview:hud];
    
    ssl_count = @"0";
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        
//        
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    
    txt_qr_pin.delegate=self;
    txt_amnt.delegate=self;
    
    txt_acct_from.text=gblclass.is_default_acct_id_name;
    str_acct_name_to=gblclass.is_default_acct_id_name;
    str_acct_no_to=gblclass.is_default_acct_no;
    str_acct_id_frm=gblclass.is_default_acct_id;
    lbl_acct_frm.text=gblclass.is_default_acct_no;
    acct_to_chck=gblclass.is_default_acct_no;
    
    if ([gblclass.arr_transfer_within_acct count]>0)
    {
        
        
    split = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
    
    NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
    attach1.image = [UIImage imageNamed:@"Pkr.png"];
    attach1.bounds = CGRectMake(10, 8, 20, 10);
    NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
    
    NSString* balc;
    
    if ([split objectAtIndex:6]==0 || [[split objectAtIndex:6] isEqualToString:@""])
    {
        balc=@"0";
    }
    else
    {
        balc=[split objectAtIndex:6];
        
    }
    
    NSString *mystring =[NSString stringWithFormat:@"%@",balc];
    number = [NSDecimalNumber decimalNumberWithString:mystring];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setMinimumFractionDigits:2];
    [formatter setMaximumFractionDigits:2];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
    NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
    [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
    
    [mutableAttriStr1 appendAttributedString:imageStr1];
    
    //lbl_balance.attributedText = mutableAttriStr1;
    
    
    lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,number];
    
    }
    
    
    if ([gblclass.chck_qr_send isEqualToString:@"1"])
    {
        
        //NSLog(@"%@",gblclass.arr_qr_code);
        
        // split = [[gblclass.arr_qr_code objectAtIndex:0] componentsSeparatedByString: @"|"];
        
        
        //        txt_name_frm.text=[gblclass.arr_qr_code objectAtIndex:0];
        //     // txt_amnt.text=[gblclass.arr_qr_code objectAtIndex:0];
        //        str_acct_name_to=[gblclass.arr_qr_code objectAtIndex:0];
        //        str_acct_no_to=[gblclass.arr_qr_code objectAtIndex:1];
        //        str_acct_id_to=[gblclass.arr_qr_code objectAtIndex:2];
        //        acct_to_chck=[gblclass.arr_qr_code objectAtIndex:1];
        //
        
        
        
        txt_name_frm.delegate=self;
        txt_desp.delegate=self;
        txt_amnt.delegate=self;
        txt_qr_pin.delegate=self;
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"load";
            [self SSL_Call];
        }
        
        
        btn_qr.hidden=YES;
        btn_cancl.hidden=NO;
        btn_pay.hidden=NO;
        
    }
    else
    {
        btn_qr.hidden=NO;
        btn_cancl.hidden=YES;
        btn_pay.hidden=YES;
    }
    
    vw_table.hidden=YES;
    
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_combo_frm:(id)sender
{
    
    
    if ([gblclass.arr_transfer_within_acct count]==1)
    {
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Only One Account Exist." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        
        gblclass.custom_alert_msg=@"Only One Account Exist.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        [hud hideAnimated:YES];
        
        return;
    }
    
    if (table_from.isHidden==YES)
    {
        table_from.hidden=NO;
        vw_table.hidden=NO;
    }
    else
    {
        table_from.hidden=YES;
        vw_table.hidden=YES;
    }
    
}


-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}


-(IBAction)btn_scan_qr:(id)sender
{
    
    
    if ([txt_amnt.text isEqualToString:@""])
    {
        
        [self custom_alert:@"Enter Amount" :@"0"];
        
        return;
        
    }
    else if ([txt_amnt.text isEqualToString:@"0"])
    {
        
        [self custom_alert:@"Enter Valid Amount Pin" :@"0"];
        
        return;
        
    }
    else if ([txt_qr_pin.text isEqualToString:@""])
    {
        
        [self custom_alert:@"Enter QR Pin" :@"0"];
        
        return;
        
    }
    
    //    if ([txt_amnt.text isEqualToString:@""])
    //    {
    //        [self custom_alert:@"Enter Amount" :@"0"];
    //    }
    //    else if([txt_amnt.text isEqualToString:@"0"])
    //    {
    //        [self custom_alert:@"Enter Valid Amount" :@"0"];
    //    }
    
    
    gblclass.scan_qr_amnt=txt_amnt.text;
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"send_qr"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissModalStack];
    
    
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}



-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    //    if (tableView==table_from)
    //    {
    return [gblclass.arr_transfer_within_acct count];    //count number of row from counting array hear cataGorry is An Array
    // }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
    //NSLog(@"%@",gblclass.arr_transfer_within_acct);
    
    
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
        //Name ::
        label=(UILabel*)[cell viewWithTag:2];
        label.text = [split objectAtIndex:0];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        
        //Acct. No ::
        label=(UILabel*)[cell viewWithTag:3];
        label.text = [split objectAtIndex:1];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:12];
        [cell.contentView addSubview:label];
        
        //Balc ::
        label=(UILabel*)[cell viewWithTag:4];
        label.text = [split objectAtIndex:6];
        label.textColor=[UIColor whiteColor];
        label.font=[UIFont systemFontOfSize:18];
        [cell.contentView addSubview:label];
        
        
        img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    
    table_from.separatorStyle = UITableViewCellSeparatorStyleNone;
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [table_from reloadData];
    if (tableView==table_from)
    {
        split = [[gblclass.arr_transfer_within_acct objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        txt_acct_from.text=[split objectAtIndex:0];
        str_acct_name_to=[split objectAtIndex:0];
        lbl_acct_frm.text=[split objectAtIndex:1];
        str_acct_no_to=[split objectAtIndex:1];
        str_acct_id_frm=[split objectAtIndex:2];
        // lbl_balance.text=[split objectAtIndex:6];
        
        
        acct_to_chck=[split objectAtIndex:1];
        
        lbl_acct_frm.text=[split objectAtIndex:1];
        
        NSTextAttachment * attach1 = [[NSTextAttachment alloc] init];
        attach1.image = [UIImage imageNamed:@"Pkr.png"];
        attach1.bounds = CGRectMake(10, 8, 20, 10);
        NSAttributedString * imageStr1 = [NSAttributedString attributedStringWithAttachment:attach1];
        
        NSString* balc;
        
        if ([split objectAtIndex:6]==0)
        {
            balc=@"0";
        }
        else
        {
            balc=[split objectAtIndex:6];
        }
        
        NSString *mystring =[NSString stringWithFormat:@"%@",balc];
        number = [NSDecimalNumber decimalNumberWithString:mystring];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setMinimumFractionDigits:2];
        [formatter setMaximumFractionDigits:2];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        
        NSMutableAttributedString * mutableAttriStr1 = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromNumber:number]];
        NSDictionary * attris1 = @{NSForegroundColorAttributeName:[UIColor blackColor],NSBackgroundColorAttributeName:[UIColor clearColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:22]};
        [mutableAttriStr1 setAttributes:attris1 range:NSMakeRange(0,mutableAttriStr1.length)];
        
        [mutableAttriStr1 appendAttributedString:imageStr1];
        //    lbl_balance.attributedText = mutableAttriStr1;
        
        
        lbl_balance.text=[NSString stringWithFormat:@"%@ %@",gblclass.base_currency,number];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        
        table_from.hidden=YES;
        vw_table.hidden=YES;
    }
}


-(IBAction)btn_cancel:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissModalStack];
}


-(IBAction)btn_pay:(id)sender
{
    
    if ([txt_amnt.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter amount" :@"0"];
        
        return;
    }
    else if ([txt_amnt.text isEqualToString:@"0"])
    {
        [self custom_alert:@"Please enter valid amount" :@"0"];
        return;
    }
    else if ([txt_qr_pin.text isEqualToString:@""])
    {
        [self custom_alert:@"Please enter 6 digits QR Pin" :@"0"];
        return;
    }
    else
    {
        [hud showAnimated:YES];
        
        //  [self showAlertwithcancel:@"Transaction Processed Successfully." :@"Attention"];
        
        NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code  count]);
        NSLog(@"%@",gblclass.arr_qr_code);
        
        if ([gblclass.arr_qr_code  count]>3)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:3 withObject:txt_amnt.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_amnt.text];
        }
        
        if ([gblclass.arr_qr_code count]>4)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:4 withObject:txt_desp.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_desp.text];  //txt_pin
        }
        
        if ([gblclass.arr_qr_code count]>5)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:5 withObject:txt_acct_from.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_acct_from.text];
        }
        
        if ([gblclass.arr_qr_code count]>6)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:6 withObject:lbl_acct_frm.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:lbl_acct_frm.text];
        }
        
        if ([gblclass.arr_qr_code count]>7)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:7 withObject:str_acct_id_frm];
        }
        else
        {
            [gblclass.arr_qr_code addObject:str_acct_id_frm];
        }
        
        if ([gblclass.arr_qr_code count]>8)
        {
            [gblclass.arr_qr_code replaceObjectAtIndex:8 withObject:txt_qr_pin.text];
        }
        else
        {
            [gblclass.arr_qr_code addObject:txt_qr_pin.text];
        }
        
        
        NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code  count]);
        NSLog(@"%@",gblclass.arr_qr_code);
        
        gblclass.instant_qr_pin=txt_qr_pin.text;
        
        gblclass.check_review_acct_type=@"QR_Code_p2p";
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"review";
            [self SSL_Call];
        }
    }
    
    //   [self showAlertwithcancel:@"Transaction Processed Successfully" :@"Attention"];
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}




- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //   BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    
    if ([theTextField isEqual:txt_desp])
    {
        NSInteger MAX_DIGITS=30;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@ "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    if ([theTextField isEqual:txt_amnt])
    {
        
        // MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if ([theTextField isEqual:txt_qr_pin])
    {
        
        MAX_DIGITS=6;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}


-(void) slide_left {
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // [txt_comment resignFirstResponder];
    
    
    
    [txt_name_frm resignFirstResponder];
    [txt_desp resignFirstResponder];
    [txt_amnt resignFirstResponder];
    [txt_qr_pin resignFirstResponder];
    
    
    // [self keyboardWillHide];
}


-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil,nil];
                       [alert1 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        // txtbillservice.text=@"";
        //  _txtbilltype.text=@"";
        
        if ([chk_ssl isEqualToString:@"logout"]) {
            [self checkinternet];
            if (netAvailable) {
                chk_ssl=@"logout";
                [self SSL_Call];
                return;
            }
        } else {
            [self slide_left];
            [self dismissViewControllerAnimated:NO completion:nil];
            
            
            //            UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
            //                                                             message:[dic objectForKey:@"OutstrReturnMessage"]
            //                                                            delegate:self
            //                                                   cancelButtonTitle:@"OK"
            //                                                   otherButtonTitles:nil];
            //            [alert1 show];
            
        }
        
        
    }
    else if(buttonIndex == 1) {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}




-(void) Check_OTP_ReqDaily_Limit:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSLog(@"%@",gblclass.check_review_acct_type);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:txt_amnt.text],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@"after"],
                                                                       [encrypt encrypt_Data:gblclass.check_review_acct_type], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTxtAmount",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"place",
                                                                       @"strAccessKey", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"CheckOTPReqQRDailyLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      
                      [self showAlert:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      
                      
                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      
                      
                      [self GenerateOTP:@""];
                      
                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      
                  }
                  else if([[dic objectForKey:@"Response"] isEqualToString:@"-2"]) // Otp not required
                  {
                      [hud hideAnimated:YES];
                      
                      
                      gblclass.check_review_acct_type=@"QR_Code_p2p";
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert: [dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  
                  // [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        
        
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



-(void) GenerateOTP:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"MOB", nil]];
    
    
    NSLog(@"%@",gblclass.arr_qr_code);
    
    // [gblclass.arr_qr_code objectAtIndex:1]  lbl_acct_frm
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                   [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                   [encrypt encrypt_Data:@"1.1.1.1"],
                                                                   [encrypt encrypt_Data:@""],
                                                                   [encrypt encrypt_Data:mask_acct_no],
                                                                   [encrypt encrypt_Data:@"limit_QR_P2P"],
                                                                   [encrypt encrypt_Data:@"Addition"],
                                                                   [encrypt encrypt_Data:@"300"],
                                                                   [encrypt encrypt_Data:@"QR_P2P"],
                                                                   [encrypt encrypt_Data:@"MOB"],
                                                                   [encrypt encrypt_Data:@"QRPINTRAN"],
                                                                   [encrypt encrypt_Data:gblclass.Udid],
                                                                   [encrypt encrypt_Data:gblclass.token],
                                                                   [encrypt encrypt_Data: @"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic1 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              
              NSString*   responsecode = [dic1 objectForKey:@"Response"];
              NSString* responsemsg = [dic1 objectForKey:@"strReturnMessage"];
              
              
              
              if ([[dic1 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic1 objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  
                  [hud hideAnimated:YES];
                  
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  gblclass.check_review_acct_type=@"QR_Code_p2p";
                  
                  gblclass.Is_Otp_Required=@"1";
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"review_within_myAcct"];
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
              
              
              
          }];
    
}




-(void) QR_Instant_Transaction:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:@"1.1.1.1"],
                                                                       [encrypt encrypt_Data:@"QR_INSTANT"],
                                                                       [encrypt encrypt_Data:gblclass.is_default_acct_no],
                                                                       [encrypt encrypt_Data:str_acct_no_to],
                                                                       [encrypt encrypt_Data:txt_amnt.text],
                                                                       [encrypt encrypt_Data:txt_desp.text],
                                                                       [encrypt encrypt_Data:gblclass.Udid], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"strAccessKey",
                                                                       @"payAnyOneFromAccount",
                                                                       @"payAnyOneToAccount",
                                                                       @"strTxtAmount",
                                                                       @"strTxtComments",
                                                                       @"strDeviceId", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"QRInstantTransaction" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      [self showAlertwithcancel:[dic objectForKey:@"OutstrReturnMessage"] :@"Attention"];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      [self custom_alert:[dic objectForKey:@"OutstrReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    } @catch (NSException *exception) {
        
        [hud hideAnimated:YES];
        
        
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



-(void) Get_QR_Account_Info:(NSString *)strIndustry
{
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSLog(@"%@",gblclass.arr_qr_code);
        NSLog(@"%@",[gblclass.arr_qr_code objectAtIndex:0]);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:[gblclass.arr_qr_code objectAtIndex:0]], nil] forKeys:
                                   
                                   [NSArray arrayWithObjects:@"userid",@"strSessionId",@"IP",@"strDeviceid",@"strQR_String", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"GetQRAccountInfo" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //              NSError *error;
                  //              NSArray *arr = (NSArray *)responseObject;Samsung Gear 360 SM-C200
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      chk_ssl=@"logout";
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      NSMutableArray* arr_display_trans;
                      
                      arr_display_trans=[[NSMutableArray alloc] init];
                      arr_display_trans=[dic objectForKey:@"dtAccInfo"];
                      
                      
                      for (dic in arr_display_trans)
                      {
                          txt_name_frm.text=[dic objectForKey:@"AccountTitle"];
                          
                          mask_acct_no=[dic objectForKey:@"Account_No"];
                          
                          NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                          
                          if ([gblclass.arr_qr_code count]>13)
                          {
                              [gblclass.arr_qr_code replaceObjectAtIndex:13 withObject:txt_name_frm.text];
                          }
                          else
                          {
                              [gblclass.arr_qr_code addObject:txt_name_frm.text];
                          }
                          
                          
                          NSLog(@"%lu",(unsigned long)[gblclass.arr_qr_code count]);
                          
                          if ([gblclass.arr_qr_code count]>14)
                          {
                              [gblclass.arr_qr_code replaceObjectAtIndex:13 withObject:[dic objectForKey:@"Account_No"]]; //060510947958
                          }
                          else
                          {
                              [gblclass.arr_qr_code addObject:[dic objectForKey:@"Account_No"]];// 060510947958
                          }
                          
                      }
                      
                  }
                  else {
                      gblclass.camera_back = @"1";
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@""];
                      
                      
                      
                      //                      UIAlertController *alertController  = [UIAlertController alertControllerWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                      UIAlertAction* action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                      //                          [self slide_left];
                      //                          [self dismissViewControllerAnimated:NO completion:nil];
                      //                      }];
                      //
                      //                      [alertController addAction:action];
                      //                      [self presentViewController:alertController animated:YES completion:nil];
                      
                      
                      //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  gblclass.camera_back = @"1";
                  [hud hideAnimated:YES];
                  [self showAlert:@"Retry" :@""];
                  
                  
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        
    } @catch (NSException *exception) {
        gblclass.camera_back = @"1";
        [hud hideAnimated:YES];
        [self showAlert:@"Try again later." :@""];
    }
    
    
    
}




///////////************************** SSL PINNING START ******************************////////////////




//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            [self Check_OTP_ReqDaily_Limit:@""];
            
            // [self QR_Instant_Transaction:@""];
        }
        else if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self Get_QR_Account_Info:@""];
        }
        else if([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
    }
    else
    {
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        [self Check_OTP_ReqDaily_Limit:@""];
        
        // [self QR_Instant_Transaction:@""];
    }
    else if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self Get_QR_Account_Info:@""];
    }
    else if([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else{
        return true;
    }
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString  :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    if (buttonIndex == 1)
//    {
//        // [self checkinternet];
//        // if (netAvailable)
//        // {
//        chk_ssl=@"logout";
//        [self SSL_Call];
//        // }
//
//        // [self mob_App_Logout:@""];
//    }
//    else if(buttonIndex == 0)
//    {
//        CATransition *transition = [ CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [self.view.window.layer addAnimation:transition forKey:nil];
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self presentViewController:vc animated:NO completion:nil];
//    }
//}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
    //[alert release];
}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        //        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:@"abcd"],
                                                                       [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}


#define kOFFSET_FOR_KEYBOARD 70.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}





@end

