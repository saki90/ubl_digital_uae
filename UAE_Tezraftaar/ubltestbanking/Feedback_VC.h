//
//  Feedback_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 22/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Feedback_VC : UIViewController<UITextViewDelegate,UITextFieldDelegate>
{
    IBOutlet UITextView* txt_comment;
    IBOutlet UIButton* btn_submit;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

-(IBAction)btn_submit:(id)sender;
-(IBAction)btn_back:(id)sender;
-(IBAction)btn_account:(id)sender;
-(IBAction)btn_more:(id)sender;

@end

