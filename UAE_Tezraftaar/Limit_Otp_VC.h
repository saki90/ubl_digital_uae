//
//  Limit_Otp_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 11/04/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Limit_Otp_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt1;
    IBOutlet UITextField* txt2;
    IBOutlet UITextField* txt3;
    IBOutlet UITextField* txt4;
    IBOutlet UITextField* txt5;
    IBOutlet UITextField* txt6;
    IBOutlet UILabel* lbl_text;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

