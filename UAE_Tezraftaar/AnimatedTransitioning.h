//
//  AnimatedTransitioning.h
//  CustomTransitionExample
//
//  Created by Blanche Faur on 10/24/13.
//  Copyright (c) 2013 Blanche Faur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "UIViewControllerContextTransitioning.h"



@interface AnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>//<UIViewControllerAnimatedTransitioning>
//<UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL isPresenting;

@end
