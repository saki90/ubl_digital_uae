//
//  RegisterAccountVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 21/11/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "RegisterAccountVC.h"
#import "UIViewController+TextFieldDelegate.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"
#import <GoogleMaps/GoogleMaps.h>


@interface RegisterAccountVC ()<NSURLConnectionDataDelegate> {
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString *anotherCitizenship;
    NSUserDefaults *defaults;
    NSString* t_n_c;
    NSMutableArray* a;
    NSString* session_id;
    NSString*  first_time_chk;
    NSString *stepedVC;
    
}


@property (strong, nonatomic) IBOutlet UITextField *nameCnic;
@property (strong, nonatomic) IBOutlet UITextField *cnicNumber;
@property (strong, nonatomic) IBOutlet UITextField *mobileNumber;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *citizenStatus;

@property (strong, nonatomic) IBOutlet UIView *citizenShipView;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxYes;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxNo;

- (IBAction)nameField:(id)sender;
- (IBAction)cnicField:(id)sender;
- (IBAction)mobileField:(id)sender;
- (IBAction)emailField:(id)sender;
- (IBAction)citizenField:(id)sender;
- (IBAction)btn_next:(id)sender;
- (IBAction)btn_back:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;

@end


@implementation RegisterAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    gblclass.vc = @"RegisterAccountVC";
    
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    self.citizenShipView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.citizenShipView.layer.borderWidth = 1.0;
    anotherCitizenship = @"0";
    ssl_count  = @"0";
    [self.checkBoxNo setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
    
    session_id = @"";
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    // Initilizing Textfields.
    self.nameCnic.text = gblclass.user_name_onbording;
    self.cnicNumber.text = gblclass.cnic_onbording;
    self.mobileNumber.text = gblclass.mobile_number_onbording;
    self.email.text = gblclass.email_onbording;
    self.citizenStatus.text = gblclass.nationality_onbording;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    self.nameCnic.userInteractionEnabled = NO;
    self.cnicNumber.userInteractionEnabled = NO;
    self.mobileNumber.userInteractionEnabled = NO;
    self.email.userInteractionEnabled = NO;
    self.citizenStatus.userInteractionEnabled = NO;
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}

-(IBAction)btn_back:(id)sender {
    
    
    gblclass.user_name_onbording = self.nameCnic.text;
    gblclass.cnic_onbording = self.cnicNumber.text;
    gblclass.mobile_number_onbording = self.mobileNumber.text;
    gblclass.email_onbording = self.email.text;
    gblclass.nationality_onbording = anotherCitizenship;
    
    gblclass.register_acct_back = @"1";
    
    [self slide_left];
    //    [self dismissViewControllerAnimated:NO completion:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NewApplicationVC"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //your main thread task here
        [self presentViewController:vc animated:NO completion:nil];
    });
    
}

- (IBAction)checkYesButton:(id)sender
{
    anotherCitizenship = @"1";
    UIButton *btn = (UIButton *)sender;
    [btn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
    [self.checkBoxNo setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
}

- (IBAction)checkNoButton:(id)sender
{
    anotherCitizenship = @"0";
    UIButton *btn = (UIButton *)sender;
    [btn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
    [self.checkBoxYes setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
}

- (IBAction)nameField:(id)sender {
    [self.view endEditing:YES];
    self.nameCnic.userInteractionEnabled = YES;
    [self.nameCnic becomeFirstResponder];
}

- (IBAction)cnicField:(id)sender {
    [self.view endEditing:YES];
    self.cnicNumber.userInteractionEnabled = YES;
    [self.cnicNumber becomeFirstResponder];
}

- (IBAction)mobileField:(id)sender {
    [self.view endEditing:YES];
    self.mobileNumber.userInteractionEnabled = YES;
    [self.mobileNumber becomeFirstResponder];
}

- (IBAction)emailField:(id)sender {
    [self.view endEditing:YES];
    self.email.userInteractionEnabled = YES;
    [self.email becomeFirstResponder];
}

- (IBAction)citizenField:(id)sender {
    [self.view endEditing:YES];
    self.citizenStatus.userInteractionEnabled = YES;
    [self.citizenStatus becomeFirstResponder];
}

- (IBAction)btn_next:(id)sender {
    
    gblclass.parent_vc_onbording = @"RegisterAccountVC";
    
    
    [self.nameCnic resignFirstResponder];
    [self.cnicNumber resignFirstResponder];
    [self.mobileNumber resignFirstResponder];
    [self.email resignFirstResponder];
    [self.citizenStatus resignFirstResponder];
    
    if ([self.nameCnic.text isEqualToString:@""]) {
        
        [self custom_alert:@"Please enter full name" :@"0"];
        return ;
    } else if ([self.nameCnic.text length] < 3) {
        
        [self custom_alert:@"Full name should be at least 3 characters" :@"0"];
        return ;
    } else if ([self.cnicNumber.text isEqualToString:@""]) {
        
        [self custom_alert:@"Please enter NADRA CNIC number" :@"0"];
        return ;
        
    } else if ([self.cnicNumber.text length] < 13) {
        [self custom_alert:@"Invalid NADRA CNIC number" :@"0"];
        return ;
        
    } else if([self.mobileNumber.text isEqualToString:@""]) {
        [self custom_alert:@"Please enter mobile number" :@"0"];
        return ;
        
    } else if ([self.mobileNumber.text length] < 11) {
        [self custom_alert:@"Invalid mobile number" :@"0"];
        return ;
        
    } else if ([self.email.text isEqualToString:@""] ) {
        [self custom_alert:@"Please enter email address" :@"0"];
        return ;
        
    } else if (![self validateEmail:[self.email text]]) {
        [self custom_alert:@"Invalid email address" :@"0"];
        [hud hideAnimated:YES];
        return;
        
    } else {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self.view endEditing:YES];
        [self checkinternet];
        
        gblclass.user_name_onbording = self.nameCnic.text;
        gblclass.cnic_onbording = self.cnicNumber.text;
        gblclass.mobile_number_onbording = self.mobileNumber.text;
        gblclass.email_onbording = self.email.text;
        gblclass.nationality_onbording = anotherCitizenship;
        
        if (netAvailable)
        {
            chk_ssl=@"applyForAccount";
            session_id = @"1234|n";
            [self SSL_Call];
        }
        
    }
    
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    //NSLog(@"%@",gblclass.mainurl1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"applyForAccount"])
        {
            chk_ssl=@"";
            [self ApplyForAccount:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
            
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f]; //15.0f
        [request setHTTPMethod:@"POST"];
        connection = [NSURLConnection connectionWithRequest:request delegate:self];
        connection.start;
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [connection cancel];
    
    if ([chk_ssl isEqualToString:@"applyForAccount"])
    {
        chk_ssl=@"";
        [self ApplyForAccount:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [connection cancel];
    }
    
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (responseData == nil)
    {
        responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]) {
        return false;
    } else if (gblclass.ssl_pinning_url1 == nil) {
        return false;
    } else {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////

//////////************************** KEYBOARD CONTROLS ***************************///////////

#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    [textField resignFirstResponder];
    self.nameCnic.userInteractionEnabled = NO;
    self.cnicNumber.userInteractionEnabled = NO;
    self.mobileNumber.userInteractionEnabled = NO;
    self.email.userInteractionEnabled = NO;
    self.citizenStatus.userInteractionEnabled = NO;
    
    return NO;
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //  //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    gblclass.step_no_onbording = @"0";
    gblclass.step_no_help_onbording = @"0";
    
    // register for k.eyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

//////////************************** END KEYBOARD CONTROLS ***************************///////////


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}



-(void) ApplyForAccount:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        
        //        NSLog(@"%@",gblclass.user_name_onbording);
        //        NSLog(@"%@",gblclass.mobile_number_onbording);
        //        NSLog(@"%@",gblclass.cnic_onbording);
        //        NSLog(@"%@",gblclass.email_onbording);
        //        NSLog(@"%@",gblclass.nationality_onbording);
        
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_name_onbording],
                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                                                       [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.email_onbording],
                                                                       [encrypt encrypt_Data:gblclass.nationality_onbording],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:session_id],
                                                                       [encrypt encrypt_Data:@"0"],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"FullName",
                                                                       @"strMobileNo",
                                                                       @"strCNIC",
                                                                       @"strEmailAddress",
                                                                       @"IsPakistani",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"strSessionId",
                                                                       @"ChannelId", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ApplyForAccount" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
                      gblclass.request_id_onbording =[NSString stringWithFormat:@"%@", [dic objectForKey:@"Temp_RequestId"]];
                      gblclass.M3sessionid =  [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
                      
                      // [self Verify_OTP:@""];
                      [self Generate_OTP_For_DA_Request_Email:@""];
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) Verify_OTP:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        // NSLog(@"%@",gblclass.request_id_onbording);
        
        // gblclass.otpPin = str_Otp;
        
        // NSLog(@"%@",gblclass.mobile_number_onbording);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:@"343"],
                                                                       [encrypt encrypt_Data:@"saqib.ahmed@m3tech.com.pk"],
                                                                       [encrypt encrypt_Data:@"111111"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"EMAIL"],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strBranchCode",
                                                                       @"strAccountNumber",
                                                                       @"strOTPPIN",
                                                                       @"strTTAccessKey",
                                                                       @"Channel",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        
        
        //          public void VerifyOTPForDigitalAccountReq(string strUserId, string strSessionId, string IP, string strBranchCode, string strAccountNumber, string strOTPPIN, string strTTAccessKey, string Channel, string Device_ID, string Token)
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"VerifyOTPForDigitalAccountReq" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  //                  [self clearOTP];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      //Apply acct check
                      // [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      // [defaults setValue:@"0" forKey:@"apply_acct"];
                      // [defaults synchronize];
                      //[self GetOnBoardProducts:@""];
                      
                      [self Generate_OTP_For_DA_Request_Email:@""];
                      
                  }
                  //                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"2"]) {
                  //                      //Apply acct check
                  //
                  //                      //                      {
                  //                      //                          RequestId = 1087;
                  //                      //                          Response = 2;
                  //                      //                          SRNumber = "";
                  //                      //                          strReturnMessage = InProcess;
                  //                      //                      }
                  //
                  //                      isContinued = YES;
                  //                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                  //                      [defaults setValue:@"0" forKey:@"apply_acct"];
                  //                      [defaults synchronize];
                  //                      [self GetOnBoardProducts:@""];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"3"]) {
                  //                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                  //                      [defaults setValue:[encrypt encrypt_Data:[dic objectForKey:@"SRNumber"]] forKey:@"sr_num"];
                  //                      [defaults setValue:@"1" forKey:@"apply_acct"];
                  //                      [defaults synchronize];
                  //
                  //                      [hud hideAnimated:YES];
                  //                      [self slide_right];
                  //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                  //                      [self presentViewController:vc animated:NO completion:nil];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                  //                      [self clearOTP];
                  //                      [hud hideAnimated:YES];
                  //                      [txt_1 becomeFirstResponder];
                  //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"5"]) {
                  //                      [self clearOTP];
                  //                      [hud hideAnimated:YES];
                  //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-3"] || [[dic objectForKey:@"Response"] isEqualToString:@"-4"]) {
                  //                      [hud hideAnimated:YES];
                  //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                  //                                                                          message:[dic objectForKey:@"strReturnMessage"]
                  //                                                                         delegate:self
                  //                                                                cancelButtonTitle:@"Ok"
                  //                                                                otherButtonTitles:nil];
                  //
                  //                      [alertView show];
                  //                      [self slide_left];
                  //                      [self dismissViewControllerAnimated:NO completion:nil];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  //                  {
                  //
                  //                      [self clearOTP];
                  //                      [hud hideAnimated:YES];
                  //
                  //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                  //                                                                          message:[dic objectForKey:@"strReturnMessage"]
                  //                                                                         delegate:self
                  //                                                                cancelButtonTitle:@"OK"
                  //                                                                otherButtonTitles:nil];
                  //
                  //                      [alertView show];
                  //
                  //                      [self slide_right];
                  //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                  //                      [self presentViewController:vc animated:NO completion:nil];
                  //                      return ;
                  //
                  //                  }
                  else
                  {
                      // [self clearOTP];
                      [hud hideAnimated:YES];
                      // [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  //    [self clearOTP];
                  [hud hideAnimated:YES];
                  //      [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        //   [self clearOTP];
        [hud hideAnimated:YES];
        //   [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) Generate_OTP_For_DA_Request_Email:(NSString *)strIndustry
{
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        NSLog(@"%@",gblclass.cnic_onbording);
        //        NSLog(@"%@",gblclass.token);
        //        NSLog(@"%@",gblclass.mobile_number_onbording);
        
        
        //   gblclass.mobile_number_onbording = @"saqib.ahmed@m3tech.com.pk";
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.email_onbording],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"], //
                                                                       [encrypt encrypt_Data:@"EMAIL"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      //                      gblclass.token = @"12345";  // [dic objectForKey:@"Token"];
                      //                      gblclass.request_id_onbording = [dic objectForKey:@"RequestId"];
                      
                      gblclass.arr_reset_otp_onbarding = [[NSMutableArray alloc] init];
                      
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.cnic_onbording];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.M3sessionid];
                      [gblclass.arr_reset_otp_onbarding addObject:[GlobalStaticClass getPublicIp]];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.Udid];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.token];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.mobile_number_onbording];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"Generate OTP"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"2"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"EMAIL"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      
                      
                      [hud hideAnimated:YES];
                      [self slide_right];
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Verify_Otp_Mobile"]; // VerifyOtpVC
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  }  else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




//-(void) Generate_OTP_For_DA_Request:(NSString *)strIndustry
//{
//    @try {
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//        NSLog(@"%@",gblclass.cnic_onbording);
//        NSLog(@"%@",gblclass.token);
//        NSLog(@"%@",gblclass.mobile_number_onbording);
//
//       // gblclass.mobile_number_onbording = @"saqib.ahmed@m3tech.com.pk";
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
//                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
//                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                                                       [encrypt encrypt_Data:gblclass.Udid],
//                                                                       [encrypt encrypt_Data:gblclass.token],
//                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
//                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
//                                                                       [encrypt encrypt_Data:@"Generate OTP"],
//                                                                       [encrypt encrypt_Data:@"2"],
//                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"], //
//                                                                       [encrypt encrypt_Data:@"SMS"],
//                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
//                                                                       @"strSessionId",
//                                                                       @"IP",
//                                                                       @"Device_ID",
//                                                                       @"Token",
//                                                                       @"strMobileNo",
//                                                                       @"strAccesskey",
//                                                                       @"strCallingOTPType",
//                                                                       @"TTID",
//                                                                       @"TC_AccessKey",
//                                                                       @"Channel",
//                                                                       @"strMessageType", nil]];
//
//
//
//
//        [manager.requestSerializer setTimeoutInterval:time_out];
//        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//
//                  NSError *error=nil;
//                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
//
//                      [hud hideAnimated:YES];
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:[dic objectForKey:@"strReturnMessage"]
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
//                      [self slide_right];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];
//                      return ;
//
//                  }
//                  else  if ([[dic objectForKey:@"Response"] integerValue]==0)
//                  {
//
////                      gblclass.token = @"12345";  // [dic objectForKey:@"Token"];
////                      gblclass.request_id_onbording = [dic objectForKey:@"RequestId"];
//
//                      gblclass.arr_reset_otp_onbarding = [[NSMutableArray alloc] init];
//
//                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.cnic_onbording];
//                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.M3sessionid];
//                      [gblclass.arr_reset_otp_onbarding addObject:[GlobalStaticClass getPublicIp]];
//                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.Udid];
//                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.token];
//                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.mobile_number_onbording];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"Generate OTP"];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"2"];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"SMS"];
//                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
//
//
//                      [hud hideAnimated:YES];
//                      [self slide_right];
//
//                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyOtpVC"];
//                      [self presentViewController:vc animated:NO completion:nil];
//
//
//                  }  else {
//                       [hud hideAnimated:YES];
//                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//                  }
//
//
//              } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  [hud hideAnimated:YES];
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
//              }];
//
//    } @catch (NSException *exception) {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Try again later." :@"0"];
//    }
//
//}
//
//




- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS=13;
    
    if ([theTextField isEqual:_nameCnic])
    {
        MAX_DIGITS = 60;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c]) {
                return NO;
            }
            else {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:_cnicNumber])
    {
        MAX_DIGITS = 13;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                [self custom_alert:@"NADRA CNIC number should not start with zero." :@"0"];
                return NO;
                
            }  else if (![myCharSet characterIsMember:c]) {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:_mobileNumber])
    {
        MAX_DIGITS = 11;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            
            unichar c = [string characterAtIndex:i];
            
            if (range.location == 0 && ![string isEqualToString:@"0"]) {
                [self custom_alert:@"Mobile number should start with zero." :@"0"];
                return NO;
                
            } else {
                if (![myCharSet characterIsMember:c]) {
                    return NO;
                } else {
                    return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                }
            }
            
        }
    }
    
    if ([theTextField isEqual:_email])
    {
        MAX_DIGITS = 100;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@._-"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    
    return YES;
}




- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


//////////************************** START TABBAR CONTROLS ***************************///////////

-(IBAction)btn_feature:(id)sender
{
    
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
    
    
    
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender {
    
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

//////////************************** END TABBAR CONTROLS ***************************///////////

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end
