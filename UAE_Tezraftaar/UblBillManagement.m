

//
//  AddPayee.m
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "UblBillManagement.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
//#import "Loadbranches.h"
//#import "LoadIBFTBanks.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface UblBillManagement ()<NSURLConnectionDataDelegate>
{
    // NSMutableArray* bill_service;
    NSMutableArray* bill_type;
    NSMutableArray* branchtype;
    NSInteger selectedrowpicker;
    NSMutableArray* ttid,*ttacceskey,*placeholdername,*placeholdername2,*tc_access_key;
    // NSMutableArray* branchname,*branchcode;
    MBProgressHUD *hud;
    NSMutableArray* name,*branchcode,*branchname;
    
    GlobalStaticClass* gblclass;
    
    //fet title
    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    NSString*  strTtId,*strbilltype,*brcode,*strloannumber,*strrepaymentacnumber,*strccnumber,*strAccessKey,*strType,*strtxtNick,*strtxtCustomerID;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString *cardtype;
    NSString* responsecode;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* alert_chck;
    NSMutableArray* a;
    NSMutableArray* field_condition;
    int Max_Value_Field1, Min_Value_Field1, Max_Value_Field2, Min_Value_Field2;
    NSArray* split;
    NSString* place_holder_field1,*place_holder_field2;
    NSString* str_access_key,*str_Tc_access_key;
    NSString* str_Account_No;
    
    NSArray *searchResults;
    NSMutableArray* arr_search,*arr_search_brcode;
    NSString* chk_search;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;


@end

@implementation UblBillManagement
@synthesize searchResult;

@synthesize txtbillservice;
@synthesize transitionController;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // bill_service = [[NSMutableArray alloc]init];
    // strbilltype = [[NSMutableArray alloc]init];
    encrypt = [[Encrypt alloc] init];
    
    chk_search=@"0";
    ssl_count = @"0";
    
    strloannumber=@"";
    strrepaymentacnumber=@"";
    brcode=@"";
    strccnumber = @"";
    alert_chck=@"";
    
    _txtnick.delegate=self;
    _txtphonenumber.delegate=self;
    _txtcompany.delegate=self;
    txt_cashline_nick.delegate=self;
    
    _tableviewbilltype.hidden=YES;
    _tableviewcompany.hidden=YES;
    txt_cashline_nick.hidden=YES;
    
    
    arr_search_brcode=[[NSMutableArray alloc] init];
    searchResult=[[NSMutableArray alloc] init];
    arr_search=[[NSMutableArray alloc] init];
    tc_access_key=[[NSMutableArray alloc] init];
    field_condition=[[NSMutableArray alloc] init];
    a=[[NSMutableArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    //NSLog(@"Base Currency .%@",gblclass.base_currency);
    
    
    //search.backgroundColor=[UIColor blueColor];
    search.barTintColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    search.tintColor=[UIColor whiteColor];
    
    
    self.definesPresentationContext=true;
    
    
    // search.showsBookmarkButton = NO;
    // search.showsCancelButton = YES;
    // [self.view addSubview:search];
    
    
    // searchResult = [NSMutableArray arrayWithCapacity:[searchResults count]];
    
    
    search.delegate=self;
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
        
        // [self GetBillTypeData:@""];
    }
    
    gblclass.arr_values=[[NSMutableArray alloc] init];
    gblclass.arr_re_genrte_OTP_additn=[[NSMutableArray alloc] init];
    
    //NSLog(@"%@",gblclass.bill_type);
    
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //        //  [self.timeoutLabel  setText:@"YES"];
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //        storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //        [self presentViewController:vc animated:YES completion:nil];
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
    
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    lbl_heading.text=gblclass.bill_type.uppercaseString;
    vw_table.hidden=YES;
    
    
    _txtcompany.hidden=YES;
    img_dd3.hidden=YES;
    _txtnick.hidden=YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //   BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    
    
    if ([theTextField isEqual:_txtphonenumber])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:_txtnick])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_cashline_nick])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:_txtcompany])
    {
        //NSLog(@"%@",self.txtbilltype.text);
        if ([self.txtbilltype.text isEqualToString:@"UBL Credit Card"])
        {
            MAX_DIGITS=Max_Value_Field1;    //16;
        }
        else if ([self.txtbilltype.text isEqualToString:@"UBL CashLine"])
        {
            MAX_DIGITS= Max_Value_Field1;   //12;
        }
        else
        {
            MAX_DIGITS=Max_Value_Field1;    //9
        }
        
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        
        
        //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        //        for (int i = 0; i < [string length]; i++)
        //        {
        //            unichar c = [string characterAtIndex:i];
        //            if (![myCharSet characterIsMember:c])
        //            {
        //                return NO;
        //            }
        //            else
        //            {
        //                return YES;
        //            }
        //
        //        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}

- (IBAction)btn_submitfetchtitle:(UIButton *)sender
{
    //[self AddAccountTitleFetch:@""];
    
    
    @try {
        
        [txtbillservice resignFirstResponder];
        [_txtbilltype resignFirstResponder];
        [_txtcompany resignFirstResponder];
        [ _txtphonenumber resignFirstResponder];
        [_txtnick resignFirstResponder];
        
         
        if (_txtbilltype.text.length==0 || [_txtbilltype.text isEqualToString:@""])
        {
            //  [self showAlert:@"Select Ubl Bills" :@"Attention"];
            
            [self custom_alert:@"Select Ubl Bills" :@"0"];
            
            return;
        }
        
        strtxtNick=self.txtnick.text;
        
        //strtxtCustomerID=self.txtphonenumber.text;
        
        [gblclass.arr_values removeAllObjects];
        [gblclass.arr_values addObject:gblclass.user_id];       //0
        [gblclass.arr_values addObject:gblclass.M3sessionid];   //1
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];             //2
        [gblclass.arr_values addObject:strTtId];                //3
        [gblclass.arr_values addObject:strbilltype];            //4
        //    [gblclass.arr_values addObject:strtxtNick];
        //5
        //    [gblclass.arr_values addObject:brcode];
        
        
        
        if (self.txtcompany.text.length==0)
        {
            //[self showAlert:@"Please enter your nick" :@"Attention" ];
            
            NSString* str;
            
            if ([place_holder_field1 isEqualToString:@"Branch Code"])
            {
                str=@"Please Select";
            }
            else
            {
                str=@"Please Enter";
            }
            
            //[self showAlert:[NSString stringWithFormat:@"%@ %@",str,place_holder_field1] :@"Attention"];
            
            [self custom_alert:[NSString stringWithFormat:@"%@ %@",str,place_holder_field1] :@"0"];
            
        }
        
        else if([strtxtNick length]==0)
        {
            [self custom_alert:[NSString stringWithFormat:@"Please Enter %@",place_holder_field2] :@"0"];
        }
        else if ([_txtnick.text length] < 5) {
            
            [self custom_alert:[NSString stringWithFormat:@"%@ should be between 5 - 30 characters",place_holder_field2]  :@"0"];
            [hud hideAnimated:YES];
        }
        else
        {
            if([self.txtbilltype.text isEqualToString:@"UBL Cashline"])
            {
                str_Account_No=_txtnick.text;
                strtxtNick=@"";
                strrepaymentacnumber=self.txtnick.text;
                cardtype=strrepaymentacnumber;
                [gblclass.arr_values addObject:strrepaymentacnumber];
                [gblclass.arr_values addObject:txt_cashline_nick.text];
                [gblclass.arr_values addObject: brcode];
                [gblclass.arr_values addObject:strAccessKey];
                [gblclass.arr_values addObject:@"false"];
                
            }
            else if([self.txtbilltype.text isEqualToString:@"UBL Credit Card"])
            {
                strccnumber = self.txtcompany.text;
                cardtype=strccnumber;
                str_Account_No=self.txtcompany.text;
                strtxtNick=@"";
                strrepaymentacnumber=self.txtnick.text;
                cardtype=strrepaymentacnumber;
                [gblclass.arr_values addObject:strccnumber];
                [gblclass.arr_values addObject:str_Account_No]; // for nick
                [gblclass.arr_values addObject: @""]; // selected branch value
                [gblclass.arr_values addObject:strAccessKey];
                [gblclass.arr_values addObject:@"false"];
                
                
                //            [gblclass.arr_values addObject:strccnumber];
                //            [gblclass.arr_values addObject:cardtype];
            }
            else
            {
                //  UBL DRIVE ::
                
                str_Account_No=_txtcompany.text;
                strloannumber=self.txtcompany.text;
                cardtype=strloannumber;
                [gblclass.arr_values addObject:strloannumber];
                [gblclass.arr_values addObject:_txtnick.text];
                [gblclass.arr_values addObject:@""];  //selected branch value
                [gblclass.arr_values addObject:strAccessKey];
                [gblclass.arr_values addObject:@"false"];
                
                
            }
            
            
            //    strTtId,strtxtNick,strbilltype,brcode,strloannumber,strrepaymentacnumber,strccnumber,
            
            //6
            //        [gblclass.arr_values addObject:strloannumber];          //7
            //        [gblclass.arr_values addObject:strrepaymentacnumber];   //8
            //        [gblclass.arr_values addObject:strccnumber];            //9
            
            //**** 29 sept
            //         [gblclass.arr_values addObject:strTtId];               //10
            //         [gblclass.arr_values addObject:strbilltype];           //11
            //         [gblclass.arr_values addObject:cardtype];              //12
            //         [gblclass.arr_values addObject:@""];                   //13
            //         [gblclass.arr_values addObject:brcode];                //14
            //         [gblclass.arr_values addObject:@"true"];               //15
            
            
            //NSLog(@"%lu",_txtcompany.text.length);
            
            
            if (_txtcompany.text.length < Min_Value_Field1)
            {
                //NSLog(@"MIN");
                
                [self custom_alert:[NSString stringWithFormat:@"Length of %@ is not less then %d", place_holder_field1,Min_Value_Field1] :@"0"];
                
                //  [self showAlert:[NSString stringWithFormat:@"Length of %@ is not less then %d", place_holder_field1,Min_Value_Field1] :@"Attention"];
                
                return;
            }
            
            
            if (_txtnick.text.length < Min_Value_Field2)
            {
                //NSLog(@"MIN");
                
                [self custom_alert:[NSString stringWithFormat:@"Length of %@ is not less then %d", place_holder_field2,Min_Value_Field2] :@"0"];
                
                //[self showAlert:[NSString stringWithFormat:@"Length of %@ is not less then %d", place_holder_field2,Min_Value_Field2] :@"Attention"];
                
                return;
            }
            
            
            //    strTtId,strbilltype,cardtype,@"",brcode,@"true"
            
            //            [self Generate_OTP:@""];
            
            
            if([self.txtbilltype.text isEqualToString:@"UBL Credit Card"])
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"add_bill";   //otp
                    [self SSL_Call];
                }
            }
            else
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"otp";   //otp
                    [self SSL_Call];
                }
            }
            
            
            
            
            //   [self AddConfirmBillDetail:@""];
            
            
            
            // [self AddBillDetail:@""];
        }
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Try again later." :@"0"];
        
        // [self showAlert:@"Try again later." :@"Attention"];
        
    }
    
}


-(void) Generate_OTP:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    // gblclass.is_default_acct_no=@"227536212"; //****
    //strAccessKey
    
    
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,brcode,str_Account_No,strAccessKey,@"Generate OTP",strTtId,str_Tc_access_key, nil]];
    
    
    
    //
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"SMS",@"ADDITION",@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil]
    //                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"M3Key",nil]];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:brcode],
                                [encrypt encrypt_Data:str_Account_No],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:strTtId],
                                [encrypt encrypt_Data:str_Tc_access_key],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"ADDITION"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring =[dic objectForKey:@"strReturnMessage"];
              
              //   [self showAlert:resstring :@"Attention"];
              
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  gblclass.add_bill_type=self.txtbilltype.text;
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"add_bill_otp"]; //pay_within_acct
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  //  [self custom_alert:resstring :@"1"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@""];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later." :@""];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
          }];
}





-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}




- (IBAction)btn_dd_company:(UIButton *)sender
{
    //self.billserviceview.hidden=true;
    self.billtype.hidden=true;
    self.headerLbl.text=@"Select Branch";
    search.hidden=NO;
    
    //[search becomeFirstResponder];
    
    //self.searchDisplayController.active=true;
    
    
    if(_tableviewcompany.hidden)  //self.company.hidden
    {
        self.company.hidden=false;
        _tableviewcompany.hidden=NO;
        vw_table.hidden=NO;
        _tableviewbilltype.hidden=YES;
        
        //  bill_service = [[NSMutableArray alloc]init];
        
        // bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    }
    else
    {
        
        self.company.hidden=true;
        _tableviewcompany.hidden=YES;
        vw_table.hidden=YES;
        //  bill_service = [[NSMutableArray alloc]init];
        
        // bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    }
}


- (IBAction)btn_dd_billtype:(UIButton *)sender
{
    //self.billserviceview.hidden=true;
    self.company.hidden=true;
    self.headerLbl.text=@"Select Your UBL Bill";
    
    
    search.hidden=YES;
    
    if(_tableviewbilltype.hidden)  //self.billtype.hidden
    {
        self.billtype.hidden=false;
        _tableviewbilltype.hidden=NO;
        vw_table.hidden=NO;
        _tableviewcompany.hidden=YES;
    }
    else
    {
        self.billtype.hidden=true;
        _tableviewbilltype.hidden=YES;
        vw_table.hidden=YES;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    _txtcompany.hidden=NO;
    img_dd3.hidden=NO;
    _txtnick.hidden=NO;
    
    
    self.billtype.hidden=true;
    self.company.hidden=true;
    self.txtcompany.text=@"";
    self.txtnick.text=@"";
    self.txtphonenumber.text=@"";
    //    strTtId=@"";
    //    strAccessKey=@"";
    //    strType=@"";
    //    strtxtNick=@"";
    //    strtxtCustomerID=@"";
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        
        // split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        self.txtcompany.text=[searchResult objectAtIndex:indexPath.row];
        
        NSString *branch_code =[searchResult objectAtIndex:indexPath.row];
        brcode=[branch_code substringToIndex:4];
        
        
        self.txtnick.text=@"";
        self.txtphonenumber.text=@"";
        
        search.hidden=YES;
        self.searchDisplayController.active=false;
        
        search.text = @"";
        [search resignFirstResponder];
        
        search.hidden=YES;
        
    }
    
    
    if(tableView==_tableviewbilltype)
    {
        [_tableviewbilltype reloadData];
        strTtId =[ttid objectAtIndex:indexPath.row];
        strbilltype = [name objectAtIndex:indexPath.row];
        
        self.txtbilltype.text=[name objectAtIndex:indexPath.row];
        NSString* txttypebillvalue = self.txtbilltype.text;
        self.txtcompany.placeholder = [placeholdername objectAtIndex:indexPath.row];
        place_holder_field1=[placeholdername objectAtIndex:indexPath.row];
        
        
        if([[placeholdername2 objectAtIndex:indexPath.row] length]==0)
        {
            self.txtnick.placeholder = @"Alias/Nick";
            place_holder_field2 = @"Alias/Nick";
        }
        else
        {
            self.txtnick.placeholder = [placeholdername2 objectAtIndex:indexPath.row];
            place_holder_field2 =[placeholdername2 objectAtIndex:indexPath.row];
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            str_Tc_access_key=[tc_access_key objectAtIndex:indexPath.row];
            
            [_txtnick resignFirstResponder];
        }
        if([self.txtbilltype.text isEqualToString:@"UBL Cashline"])
        {
            if (netAvailable)
            {
                chk_ssl=@"ubl_cashline";
                [self SSL_Call];
                
                //              [self LoadBranches:@""];
            }
            
            self.dd3.hidden=false;
            img_dd3.hidden=NO;
            self.txtcompany.userInteractionEnabled=NO;
            self.txtcompany.enabled=NO;
            txt_cashline_nick.hidden=NO;
            [_txtnick setKeyboardType:UIKeyboardTypeNumberPad];
        }
        else
        {
            txt_cashline_nick.hidden=YES;
            self.txtcompany.enabled=YES;
            self.dd3.hidden=true;
            img_dd3.hidden=YES;
            self.txtcompany.userInteractionEnabled=YES;
            [_txtnick setKeyboardType:UIKeyboardTypeDefault];
            
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            str_Tc_access_key=[tc_access_key objectAtIndex:indexPath.row];
        }
        
        split = [[field_condition objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        
        Max_Value_Field1=[[split objectAtIndex:0] intValue];
        Min_Value_Field1=[[split objectAtIndex:1] intValue];
        
        Max_Value_Field2=[[split objectAtIndex:2] intValue];
        Min_Value_Field2=[[split objectAtIndex:3] intValue];
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    else if(tableView==_tableviewcompany)
    {
        [_tableviewcompany reloadData];
        self.txtcompany.text=[branchname objectAtIndex:indexPath.row];
        brcode= [branchcode objectAtIndex:indexPath.row];
        //self.txtcompany.text=[name objectAtIndex:indexPath.row];
        self.txtnick.text=@"";
        self.txtphonenumber.text=@"";
        
        
        //        strTtId=@"";
        //        strAccessKey=@"";
        //        strType=@"";
        //        strtxtNick=@"";
        //        strtxtCustomerID=@"";
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    
    vw_table.hidden=YES;
    //   [self.view endEditing:YES];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [searchResult count];
    }
    else if(tableView== _tableviewbilltype)
    {
        if([bill_type count]==0)
        {
            return 0;
        }
        else
        {
            return [bill_type count];
        }
    }
    else if(tableView== _tableviewcompany)
    {
        if([branchtype count]==0)
        {
            return 0;
        }
        else
        {
            return [branchtype  count];
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    //NSLog(@"%@",tableView);
    
    
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        
        split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        cell.textLabel.text=[searchResult objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:12];
    }
    
    if(tableView==_tableviewbilltype)
    {
        
        NSDictionary *dic = [bill_type objectAtIndex:indexPath.row];
        
        //if([self.txtbilltype.text isEqualToString:@"Utility Bills"]){
        //[name addObject:[dic objectForKey:@"COMPANY_NAME"]];
        
        //}
        UILabel* label=(UILabel*)[cell viewWithTag:2];
        
        //[bill_type addObject:[dic objectForKey:@"BillType"]];
        label.text=[dic objectForKey:@"BillType"];
        
        [name addObject:[dic objectForKey:@"BillType"]];
        [placeholdername2 addObject: [dic objectForKey:@"FieldName2"]];
        [placeholdername addObject: [dic objectForKey:@"FieldName1"]];
        [ttid addObject: [dic objectForKey:@"TT_ID"]];
        [ttacceskey addObject: [dic objectForKey:@"TT_ACCESS_KEY"]];
        [tc_access_key addObject: [dic objectForKey:@"TC_ACCESS_KEY"]];
        
        NSString* MaxValueField1=[dic objectForKey:@"MaxValueField1"];
        [a addObject:MaxValueField1];
        NSString* MinValueField1=[dic objectForKey:@"MinValueField1"];
        [a addObject:MinValueField1];
        
        NSString* MaxValueField2=[dic objectForKey:@"MaxValueField2"];
        [a addObject:MaxValueField2];
        NSString* MinValueField2=[dic objectForKey:@"MinValueField2"];
        [a addObject:MinValueField2];
        
        
        NSString *bbb = [a componentsJoinedByString:@"|"];
        [field_condition addObject:bbb];
        [a removeAllObjects];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
    }
    
    else if(tableView==_tableviewcompany)
    {
        
        
        //        if ([chk_search isEqualToString:@"1"])
        //        {
        //
        //            //NSLog(@"%@",[searchResults objectAtIndex:indexPath.row]);
        //
        ////            cell.textLabel.text=[searchResults objectAtIndex:indexPath.row];
        ////            cell.textLabel.font=[UIFont systemFontOfSize:14];
        //
        //             UILabel* label=(UILabel*)[cell viewWithTag:3];
        //             label.text=[searchResults objectAtIndex:indexPath.row];  //searchResults
        //             label.font=[UIFont systemFontOfSize:12];
        //             label.textColor=[UIColor whiteColor];
        //             [cell.contentView addSubview:label];
        //        }
        //        else
        //        {
        UILabel* label=(UILabel*)[cell viewWithTag:3];
        NSDictionary *dic = [branchtype objectAtIndex:indexPath.row];
        [branchname addObject:[dic objectForKey:@"brdetail"]];
        [branchcode addObject:[dic objectForKey:@"brcode"]];
        
        
        //branchname
        label.text=[branchname objectAtIndex:indexPath.row];  //searchResults
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        //        }
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    
    _tableviewbilltype.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableviewcompany.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    return cell;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
//    searchResults = [arr_search filteredArrayUsingPredicate:resultPredicate];
//
//    chk_search=@"1";
//    [_tableviewcompany reloadData];
//}

//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString ///*)searchString
//{
//    [self filterContentForSearchText:searchString
//                                   scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
//                                          objectAtIndex:[self.searchDisplayController.searchBar
//                                                         selectedScopeButtonIndex]]];
//
//    return YES;
//}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    searchResult = [NSMutableArray arrayWithArray: [arr_search filteredArrayUsingPredicate:resultPredicate]];
    //NSLog(@"Search ::  %@",searchResult);
    
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}




-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertController * alert=   [UIAlertController
                                                     alertControllerWithTitle:Title
                                                     message:exception
                                                     preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:UIAlertActionStyleDefault
                                                                        handler:nil]; //You can use a block here to handle a press on this button
                       [alert addAction:actionOk];
                       
                       [self presentViewController:alert animated:YES completion:nil];
                       
                   });
    //[alert release];
}
-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    alert_chck=@"0";
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert2 show];
                   });
    //[alert release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if ([chk_ssl isEqualToString:@"logout"])
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        return;
    }
    
    if ([alert_chck isEqualToString:@"0"])
    {
        
        [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if(buttonIndex ==0)
        {
            
            // txtbillservice.text=@"";
            // _txtbilltype.text=@"";
            
            if([responsecode isEqualToString:@"0"]){
                if([self.txtbilltype.text isEqualToString:@"UBL Cashline"]||([self.txtbilltype.text isEqualToString:@"UBL Credit Card"]))
                {
                    if([self.txtcompany.text length]==0){
                    }
                    else
                    {
                        [self AddConfirmBillDetail:@""];
                    }
                    _txtcompany.text=@"";
                    _txtphonenumber .text=@"";
                    _txtnick.text=@"";
                    
                }
                else
                {
                    _txtcompany.text=@"";
                    _txtphonenumber .text=@"";
                    _txtnick.text=@"";
                }
            }
            else
            {
                responsecode=@"";
            }
        }
        
    }
    else if ([alert_chck isEqualToString:@"1"])
    {
        if (buttonIndex == 0)
        {
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 1)
        {
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            [self presentViewController:vc animated:NO completion:nil];
        }
    }
    else
    {
        
    }
    
}

-(void) GetBillTypeData:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    placeholdername2= [[NSMutableArray alloc]init];
    name= [[NSMutableArray alloc]init];
    placeholdername = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    
    [manager POST:@"GetBillTypeData" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              //          NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //            NSString* responsecode = [dic objectForKey:@"Response"];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  bill_type=[(NSDictionary *)[dic objectForKey:@"outdtBillTypeDataset"]objectForKey:@"Table1"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
              }
              
              
              //NSLog(@"Done");
              
              //[self parsearray];
              [hud hideAnimated:YES];
              
              //  [_tableviewcompany reloadData];
              [_tableviewbilltype reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) LoadBranches:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    branchtype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1"],
                                [encrypt encrypt_Data:@"1000"],
                                [encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strCountry",
                                                                   @"StrUserType",
                                                                   @"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"LoadBranches" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  branchtype = [(NSDictionary *)[dic objectForKey:@"dtUBLBankList"] objectForKey:@"Table1"];
                  //ttid=
 
                  for (dic in branchtype)
                  {
 
                      //                  //NSLog(@"%@",[dic objectForKey:@"brdetail"]);
                      //
                      //
                      //                  NSString*brdetail =[dic objectForKey:@"brdetail"];
                      //                    [a addObject:brdetail];
                      //
                      //                  NSString*brcode =[dic objectForKey:@"brcode"];
                      //                  [a addObject:brcode];
                      //
                      //                  NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                      //                  [arr_search addObject:bbb];
                      
                      [arr_search addObject:[dic objectForKey:@"brdetail"]];
                      [arr_search_brcode addObject:[dic objectForKey:@"brcode"]];
                      
                      
                  }
                  
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              
              //NSLog(@"Done load branch");
              //[self parsearray];
              [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later." :@""];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}

-(void) AddBillDetail:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:strTtId],
                                [encrypt encrypt_Data:strtxtNick],
                                [encrypt encrypt_Data:strbilltype],
                                [encrypt encrypt_Data:brcode],
                                [encrypt encrypt_Data:strloannumber],
                                [encrypt encrypt_Data:strrepaymentacnumber],
                                [encrypt encrypt_Data:strccnumber],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:gblclass.Udid], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTT_ID",
                                                                   @"strBillNick",
                                                                   @"strSelectedBillType",
                                                                   @"strBranchSelectedValue",
                                                                   @"strLoanNumber",
                                                                   @"strRepaymentAccNumber",
                                                                   @"strCCNumber",
                                                                   @"strOTPPIN",
                                                                   @"strAccesskey",
                                                                   @"Token",
                                                                   @"Device_ID", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"AddBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode = [dic objectForKey:@"Response"];
              
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              if([self.txtbilltype.text isEqualToString:@"UBL Cashline"]||([self.txtbilltype.text isEqualToString:@"UBL Credit Card"]))
              {
                  
                  if([responsecode isEqualToString:@"0"])
                  {
                      
                      
                      [self checkinternet];
                      if (netAvailable)
                      {
                          chk_ssl=@"otp";
                          [self SSL_Call];
                      }
                      
                      //                      NSString* msg= @"Do you want to add ";
                      //
                      //                      NSString* labelnam= [dic objectForKey:@"outlblTitle"];
                      //                      msg= [msg stringByAppendingString:labelnam];
                      //                      NSString* halfmsg= @" in your paylist.";
                      //                      msg= [msg stringByAppendingString:halfmsg];
                      //
                      //                      [self showAlertwithcancel:msg :@"Attention"];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:resstring :@"0"];
                      
                      //[self showAlert:resstring :@"Attention"];
                  }
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:resstring :@"0"];
                  
                  //[self showAlert:resstring :@"Attention"];
                  
              } //NSLog(@"Done");
              [hud hideAnimated:YES];
              
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later." :@""];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) AddConfirmBillDetail:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam= [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:strTtId],[encrypt encrypt_Data:strbilltype],[encrypt encrypt_Data:cardtype],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:brcode],[encrypt encrypt_Data:@"true"], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strTT_ID",@"strSelectedBillType",@"lblCardNo",@"txtNickCL",@"strBranchSelectedValue",@"bIsConfirmationRequired" ,nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"AddConfirmBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode = [dic objectForKey:@"Response"];
              
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              [self showAlert:resstring :@"Attention"];
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later." :@""];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(IBAction)btn_logout:(id)sender
{
    alert_chck=@"1";
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}


-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_back:(id)sender
{
    chk_search=@"0";
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    chk_search=@"0";
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    chk_search=@"0";
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtphonenumber resignFirstResponder];
    [_txtnick resignFirstResponder];
    [txt_cashline_nick resignFirstResponder];
    
    //    [search resignFirstResponder];
    self.billtype.hidden=true;
    self.company.hidden=true;
}

#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
    _tableviewcompany.hidden=YES;
    _tableviewbilltype.hidden=YES;
}




///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            [self GetBillTypeData:@""];
        }
        else if ( [chk_ssl isEqualToString:@"ubl_cashline"])
        {
            chk_ssl=@"";
            [self LoadBranches:@""];
        }
        else if ([chk_ssl isEqualToString:@"otp"])
        {
            chk_ssl=@"";
            [self Generate_OTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"add_bill"])
        {
            chk_ssl=@"";
            [self AddBillDetail:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        [self GetBillTypeData:@""];
    }
    else if ( [chk_ssl isEqualToString:@"ubl_cashline"])
    {
        chk_ssl=@"";
        [self LoadBranches:@""];
    }
    else if ([chk_ssl isEqualToString:@"otp"])
    {
        chk_ssl=@"";
        [self Generate_OTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"add_bill"])
    {
        chk_ssl=@"";
        [self AddBillDetail:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

