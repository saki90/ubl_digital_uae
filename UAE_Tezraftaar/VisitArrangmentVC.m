//
//  VisitArrangmentVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 06/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "VisitArrangmentVC.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"
#import "ubltestbanking-Swift.h"


@interface VisitArrangmentVC () {
    
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSInteger selectedRow;
    BOOL isSelected;
    NSArray* split;
    NSMutableArray* data;
    NSString* ssl_count;
    NSString* t_n_c;
    NSMutableArray* a;
    NSUserDefaults *defaults;
    NSString*  first_time_chk;
    NSString *stepedVC;
}

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)btn_back:(id)sender;
- (IBAction)btn_help:(id)sender;
- (IBAction)btn_next:(id)sender;

//- (IBAction)btn_morning:(id)sender;
//- (IBAction)btn_afternoon:(id)sender;
//- (IBAction)btn_evening:(id)sender;
//@property (strong, nonatomic) IBOutlet UIButton *btn_morning;
//@property (strong, nonatomic) IBOutlet UIButton *btn_afternoon;
//@property (strong, nonatomic) IBOutlet UIButton *btn_evening;


- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;

@end

@implementation VisitArrangmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    gblclass.noOfPerformedAttempts = 0;
    
    selectedRow = 10;
    ssl_count = @"0";
    
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    
    // TableView Settings
    self.tableView.alwaysBounceVertical = NO;
    self.tableData = [[NSArray alloc] init];
    self.tableData = gblclass.rm_avalibilityTimings;
    
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    //    self.tableData = self.rmTimings;
    
    // Initilize TableData Variables
    //    self.tableData = [NSMutableArray arrayWithObjects:
    //                      [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"id", @"M",@"code", @"Morning",@"Name", nil],
    //                      [NSDictionary dictionaryWithObjectsAndKeys:@"2",@"id", @"A",@"code", @"Afternoon",@"Name", nil],
    //                      [NSDictionary dictionaryWithObjectsAndKeys:@"3",@"id", @"E",@"code", @"Evening",@"Name", nil],  nil];
    [self.tableView reloadData];
    
    //    [self.btn_morning setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
    
    // Load the Relationship Manager Avalibility Timings.
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //    [self checkinternet];
    //    if (netAvailable) {
    //        if ([gblclass.map_call_service  isEqualToString:@"1"]) {
    //            chk_ssl=@"set_on_board_data";
    //            [self SSL_Call];
    //        } else {
    //            chk_ssl=@"getOnBoardRMavail";
    //            [self SSL_Call];
    //        }
    //    }
    
}

-(void)viewWillAppear:(BOOL)animated {
    gblclass.step_no_onbording = @"3";
//    gblclass.step_no_help_onbording = @"6";
    gblclass.step_no_help_onbording = @"DOB05";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}

///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call {
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]) {
        
        if ([chk_ssl isEqualToString:@"getOnBoardRMavail"]) {
            chk_ssl=@"";
            [self  GetOnBoardRMavail:@""];
        }
        else if ([chk_ssl isEqualToString:@"set_on_board_data"])
        {
            chk_ssl=@"";
            [self  Set_On_Board_Data:@""];
        } else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
        {
            chk_ssl=@"";
            [self NeedOnBoardHelp:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
    } else {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    if ([chk_ssl isEqualToString:@"set_on_board_data"]) {
        chk_ssl=@"";
        [self Set_On_Board_Data:@""];
        
    }  else if ([chk_ssl isEqualToString:@"getOnBoardRMavail"]) {
        chk_ssl=@"";
        [self GetOnBoardRMavail:@""];
    } else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
    {
        chk_ssl=@"";
        [self NeedOnBoardHelp:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [self.connection cancel];
    }
    
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    //  NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



-(void)Set_On_Board_Data:(NSString *)strIndustry
{
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        NSLog(@"%@",gblclass.mode_of_meeting);
        //        NSLog(@"%@",gblclass.preferred_time);
        
        
        //      NSLog(@"%@", gblclass.selcted_branch_code);
        
        //        gblclass.selcted_branch_code = @"0682";
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.selcted_branch_code],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:gblclass.user_name_onbording],
                                                                       [encrypt encrypt_Data:gblclass.selected_city],// @"Karachi"
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@""],    nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",
                                                                       @"strBranchCode",
                                                                       @"ModeOfMeetingId",
                                                                       @"PreferredTimeId",
                                                                       @"DebitCardName",
                                                                       @"City",
                                                                       @"StepNo",
                                                                       @"Remarks", nil]];
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"SetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }  else if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      //self.tableData = [dic objectForKey:@"Timmings"];
                      chk_ssl=@"getOnBoardRMavail";
                      [self SSL_Call];
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}






-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}



-(void)GetOnBoardRMavail:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        // For Test.
        //        gblclass.token = @"12345";  // [dic objectForKey:@"Token"];
        //        gblclass.request_id_onbording = @"23233";
        
        //Branch Code 0525 corporate branch i.i chundrigar road ...
        
        NSLog(@"%@",gblclass.selcted_branch_code);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.selcted_branch_code], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token",
                                                                       @"strBranchCode",nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardRMavail" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* a;
                  
                  a = [[NSMutableArray alloc] init];
                  data = [[NSMutableArray alloc] init];
                  self.tableData  = [[NSMutableArray alloc] init];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      data = [(NSDictionary *)[dic objectForKey:@"GetRMTime"] objectForKey:@"Table"];
                      
                      
                      for (dic in data)  {
                          
                          NSString* DESCRIPTION=[dic objectForKey:@"DESCRIPTION"];
                          
                          if (DESCRIPTION.length==0 || [DESCRIPTION isEqualToString:@" "] || [DESCRIPTION isEqualToString:nil]) {
                              DESCRIPTION=@"N/A";
                              [a addObject:DESCRIPTION];
                          } else {
                              [a addObject:DESCRIPTION];
                          }
                          
                          NSString* PREFERREDTIME_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PREFERREDTIME_ID"]];
                          
                          if (PREFERREDTIME_ID.length==0 || [PREFERREDTIME_ID isEqualToString:@" "] || [PREFERREDTIME_ID isEqualToString:nil]) {
                              PREFERREDTIME_ID=@"0";
                              [a addObject:PREFERREDTIME_ID];
                          } else {
                              [a addObject:PREFERREDTIME_ID];
                          }
                          
                          NSString* bbb = [a componentsJoinedByString:@"|"];
                          //                          [self.tableData addObject:bbb];
                          [a removeAllObjects];
                      }
                      
                      NSLog(@"%@",self.tableData);
                      NSLog(@"%lu",(unsigned long)[self.tableData count]);
                      
                      [self.tableView reloadData];
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==92) {
                      [self slide_right];
                      gblclass.mode_of_meeting = @"2";
                      gblclass.preferred_time = @"0";
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MeetRepresentativeVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)NeedOnBoardHelp:(NSString *)strIndustryOmai
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                    [encrypt encrypt_Data:gblclass.email_onbording],
                                    [encrypt encrypt_Data:gblclass.request_id_onbording],
                                    [encrypt encrypt_Data:gblclass.cnic_onbording],
                                    [encrypt encrypt_Data:gblclass.user_name_onbording],
                                    [encrypt encrypt_Data:gblclass.step_no_help_onbording],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strEmailAddress",
                                                                       @"RequestId",
                                                                       @"strCNIC",
                                                                       @"Name",
                                                                       @"StepNo",
                                                                       nil]];
        
        
        //        {
        //            NoOfAttempts = "10";
        //            Response = "0";
        //            TimerLimit = "120";
        //            strReturnMessage = "message to be on alertiew";
        //        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"NeedOnBoardHelp" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  gblclass.parent_vc_onbording = @"VisitArrangmentVC";
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                      
                      // Changed 4 Apr.
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"-1"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      gblclass.timmerLimitOnBording = [[dic objectForKey:@"TimerLimit"] intValue];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      //                      [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      
                      //                                        [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      //                                        gblclass.helpidentifier = [dic objectForKey:@"HelpIdentifier"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"] || [[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  //                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  //                      [alert show];
                  [self slide_right];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  //                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];//                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //                      [alert show];
        [self slide_right];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        //        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

//////////************************** START TABBAR CONTROLS ***************************///////////

-(IBAction)btn_back:(id)sender {
    [self slide_left];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"FindingLocationVC"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"lets_meet_Swift"]; // EnableGpsVC FindingLocationVC FindPlace_swift
    [self presentViewController:vc animated:NO completion:nil];
    //    [gblclass.controllers_Refrence_Stack addObject:self];
    
    //    for (int i = 0; i< gblclass.controllers_Refrence_Stack.count; i++) {
    //
    //        NSMutableArray *arry = gblclass.controllers_Refrence_Stack;
    //        id chck = gblclass.controllers_Refrence_Stack[i];
    //
    //        if ([chck isKindOfClass:[LetsMeet class]]) {
    //
    //            LetsMeet *vc = (LetsMeet*)chck;
    //            [vc dismissViewControllerAnimated:NO completion:nil];
    //            [self dismissViewControllerAnimated:NO completion:nil];
    //        }
    //        if ([chck isKindOfClass:[LocationServices class]]) {
    //
    //            LocationServices *vc = (LocationServices*)chck;
    //            [vc dismissViewControllerAnimated:NO completion:nil];
    //            [self dismissViewControllerAnimated:NO completion:nil];
    //        }
    //
    //        if ([chck isKindOfClass:[FindPlace class]]) {
    //
    //            FindPlace *vc = (FindPlace*)chck;
    //            [vc dismissViewControllerAnimated:NO completion:nil];
    //            [self dismissViewControllerAnimated:NO completion:nil];
    //        }
    //
    //    }
    
    
}

-(IBAction)btn_help:(id)sender
{
    if (gblclass.noOfPerformedAttempts <= gblclass.noOfAvailableAttempts) {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self checkinternet];
        if (netAvailable) {
            gblclass.selected_account_onbording = @"";
            chk_ssl = @"needOnBoardHelp";
            [self SSL_Call];
        } else {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please make sure you have connected to wifi" :@"0"];
        }
        
        //        [self slide_right];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
        //        [self presentViewController:vc animated:NO completion:nil];
    } else {
        [self custom_alert:[NSString stringWithFormat:@"You have made %d attempts. Please contact us on 111-825-888 (UAN) or  in order to help you.", gblclass.noOfPerformedAttempts] :@"0"];
    }
}

-(IBAction)btn_next:(id)sender
{
    gblclass.parent_vc_onbording = @"VisitArrangmentVC";
    if (selectedRow == 10)
    {
        [self custom_alert:@"Please select preferred timing" :@"0"];
        return;
    } else {
        [self slide_right];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MeetRepresentativeVC"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}

//- (IBAction)btn_morning:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    [btn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
//    [self.btn_afternoon setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//    [self.btn_evening setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//}
//
//- (IBAction)btn_afternoon:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    [btn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
//    [self.btn_morning setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//    [self.btn_evening setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//}
//
//- (IBAction)btn_evening:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    [btn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:UIControlStateNormal];
//    [self.btn_morning setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//    [self.btn_afternoon setImage:[UIImage imageNamed:@"icon_checkbox_unselected"] forState:UIControlStateNormal];
//}

-(IBAction)btn_feature:(id)sender
{
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

//////////************************** END TABBAR CONTROLS ***************************///////////

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

/////****************** TABLEVIEW DELEGATE ******************/////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *cellImage = (UIImageView *)[cell viewWithTag:10];
    if (selectedRow == indexPath.row) {
        cellImage.image = [UIImage imageNamed:@"icon_selected"];
    } else {
        cellImage.image = [UIImage imageNamed:@"icon_checkbox_unselected.png"];
    }
    
    
    // split = [[self.tableData objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
    
    UILabel *cellLabel = (UILabel *)[cell viewWithTag:11];
    cellLabel.text =  [NSString stringWithFormat:@"%@", [self.tableData[indexPath.row] objectForKey:@"DESCRIPTION"]];
    
    UIButton *cellButton = [cell viewWithTag:1];
    [cellButton addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
    cellButton.tag = indexPath.row;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //    UIButton *button = (UIButton *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:indexPath.row];
    //    UIImage *image = [UIImage imageNamed:@"icon_checkbox_unselected"];
    //
    //    if ([button.currentImage isEqual:image])
    //    {
    //        [button setImage:[UIImage imageNamed:@"icon_selected"] forState:UIControlStateNormal];
    //        isSelected = YES;
    //        selectedRow = indexPath.row;
    //        availabilityTiming = (NSString *)self.tableData[indexPath.row];
    //    }
    //    else
    //    {
    //        [button setImage:image forState:UIControlStateNormal];
    //        isSelected = NO;
    //    }
    //
    //        [self.tableView reloadData];
    //
}


/////****************** END TABLEVIEW DELEGATE ******************/////

-(void)check:(id)sender {
    
    UIButton *button =(UIButton *)sender;
    //    UIImage *image = [UIImage imageNamed:@"icon_checkbox_unselected"];
    //
    //    if ([button.currentImage isEqual:image]) {
    //        [button setImage:[UIImage imageNamed:@"icon_selected"] forState:UIControlStateNormal];
    //        gblclass.preferred_time = (NSString *)[[self.tableData objectAtIndex:button.tag] objectForKey:@"code"];
    //        gblclass.mode_of_meeting = @"RM";
    ////        selectedRow = button.tag;
    //    } else {
    //        [button setImage:image forState:UIControlStateNormal];
    //    }
    //
    selectedRow = button.tag;
    gblclass.preferred_time = [NSString stringWithFormat:@"%@", [self.tableData[selectedRow] objectForKey:@"PREFERREDTIME_ID"]];
    [self.tableView reloadData];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end

