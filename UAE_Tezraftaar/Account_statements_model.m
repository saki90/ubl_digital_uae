//
//  Account_statements_model.m
//
//  Created by   on 24/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Account_statements_model.h"
#import "OutdtDataset.h"


NSString *const kAccount_statements_modelResponse = @"Response";
NSString *const kAccount_statements_modelStrTotal = @"strTotal";
NSString *const kAccount_statements_modelOutdtData = @"outdtData";
NSString *const kAccount_statements_modelOutdtDataset = @"outdtDataset";
NSString *const kAccount_statements_modelStrReturnMessage = @"strReturnMessage";


@interface Account_statements_model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Account_statements_model

@synthesize response = _response;
@synthesize strTotal = _strTotal;
@synthesize outdtData = _outdtData;
@synthesize outdtDataset = _outdtDataset;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kAccount_statements_modelResponse fromDictionary:dict];
            self.strTotal = [self objectOrNilForKey:kAccount_statements_modelStrTotal fromDictionary:dict];
            self.outdtData = [self objectOrNilForKey:kAccount_statements_modelOutdtData fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kAccount_statements_modelOutdtDataset]];
            self.strReturnMessage = [self objectOrNilForKey:kAccount_statements_modelStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kAccount_statements_modelResponse];
    [mutableDict setValue:self.strTotal forKey:kAccount_statements_modelStrTotal];
    [mutableDict setValue:self.outdtData forKey:kAccount_statements_modelOutdtData];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kAccount_statements_modelOutdtDataset];
    [mutableDict setValue:self.strReturnMessage forKey:kAccount_statements_modelStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kAccount_statements_modelResponse];
    self.strTotal = [aDecoder decodeObjectForKey:kAccount_statements_modelStrTotal];
    self.outdtData = [aDecoder decodeObjectForKey:kAccount_statements_modelOutdtData];
    self.outdtDataset = [aDecoder decodeObjectForKey:kAccount_statements_modelOutdtDataset];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kAccount_statements_modelStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kAccount_statements_modelResponse];
    [aCoder encodeObject:_strTotal forKey:kAccount_statements_modelStrTotal];
    [aCoder encodeObject:_outdtData forKey:kAccount_statements_modelOutdtData];
    [aCoder encodeObject:_outdtDataset forKey:kAccount_statements_modelOutdtDataset];
    [aCoder encodeObject:_strReturnMessage forKey:kAccount_statements_modelStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    Account_statements_model *copy = [[Account_statements_model alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.strTotal = [self.strTotal copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
