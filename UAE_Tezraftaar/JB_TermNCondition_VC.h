//
//  JB_TermNCondition_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 04/10/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import <WebKit/WebKit.h>

@interface JB_TermNCondition_VC : UIViewController<WKUIDelegate>
{
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    IBOutlet UIButton* btn_resend_otp;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIButton* btn_agree_chck;
    
    
    IBOutlet UIButton* btn_agree_ipad;
    IBOutlet UIButton* btn_dis_agree_ipad;
    
    IBOutlet UIButton* btn_login_color;
    
    __weak IBOutlet UITextField *emailOutlet;
    __weak IBOutlet UIView *emailView;
    
    __weak IBOutlet UITextField *passOutlet;
    __weak IBOutlet UIView *passView;
    
    __weak IBOutlet UIButton *forgetPass;
    
    IBOutlet WKWebView* webview1;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

