//
//  Table1.m
//
//  Created by   on 16/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "LoadIBFTBanks.h"


NSString *const kTable1ApplicationCode = @"applicationCode";
NSString *const kTable1BankName = @"bank_name";
NSString *const kTable1BankImd = @"bank_imd";
NSString *const kTable1AccountNumberLength = @"accountNumberLength";
NSString *const kTable1MinaccountNumberLength = @"minaccountNumberLength";
NSString *const kTable1Format = @"format";


@interface LoadIBFTBanks ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LoadIBFTBanks

@synthesize applicationCode = _applicationCode;
@synthesize bankName = _bankName;
@synthesize bankImd = _bankImd;
@synthesize accountNumberLength = _accountNumberLength;
@synthesize minaccountNumberLength = _minaccountNumberLength;
@synthesize format = _format;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    if(self && [dict isKindOfClass:[NSDictionary class]])
    {
            self.applicationCode = [self objectOrNilForKey:kTable1ApplicationCode fromDictionary:dict];
            self.bankName = [self objectOrNilForKey:kTable1BankName fromDictionary:dict];
            self.bankImd = [self objectOrNilForKey:kTable1BankImd fromDictionary:dict];
            self.accountNumberLength = [self objectOrNilForKey:kTable1AccountNumberLength fromDictionary:dict];
            self.minaccountNumberLength = [self objectOrNilForKey:kTable1MinaccountNumberLength fromDictionary:dict];
            self.format = [self objectOrNilForKey:kTable1Format fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.applicationCode forKey:kTable1ApplicationCode];
    [mutableDict setValue:self.bankName forKey:kTable1BankName];
    [mutableDict setValue:self.bankImd forKey:kTable1BankImd];
    [mutableDict setValue:self.accountNumberLength forKey:kTable1AccountNumberLength];
    [mutableDict setValue:self.minaccountNumberLength forKey:kTable1MinaccountNumberLength];
    [mutableDict setValue:self.format forKey:kTable1Format];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.applicationCode = [aDecoder decodeObjectForKey:kTable1ApplicationCode];
    self.bankName = [aDecoder decodeObjectForKey:kTable1BankName];
    self.bankImd = [aDecoder decodeObjectForKey:kTable1BankImd];
    self.accountNumberLength = [aDecoder decodeObjectForKey:kTable1AccountNumberLength];
    self.minaccountNumberLength = [aDecoder decodeObjectForKey:kTable1MinaccountNumberLength];
    self.format = [aDecoder decodeObjectForKey:kTable1Format];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_applicationCode forKey:kTable1ApplicationCode];
    [aCoder encodeObject:_bankName forKey:kTable1BankName];
    [aCoder encodeObject:_bankImd forKey:kTable1BankImd];
    [aCoder encodeObject:_accountNumberLength forKey:kTable1AccountNumberLength];
    [aCoder encodeObject:_minaccountNumberLength forKey:kTable1MinaccountNumberLength];
    [aCoder encodeObject:_format forKey:kTable1Format];
}

- (id)copyWithZone:(NSZone *)zone
{
    LoadIBFTBanks *copy = [[LoadIBFTBanks alloc] init];
    
    if (copy) {

        copy.applicationCode = [self.applicationCode copyWithZone:zone];
        copy.bankName = [self.bankName copyWithZone:zone];
        copy.bankImd = [self.bankImd copyWithZone:zone];
        copy.accountNumberLength = [self.accountNumberLength copyWithZone:zone];
        copy.minaccountNumberLength = [self.minaccountNumberLength copyWithZone:zone];
        copy.format = [self.format copyWithZone:zone];
    }
    
    return copy;
}


@end
