//
//  API_Handler.m
//  ubltestbanking
//
//  Created by Basit on 16/01/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "API_Handler.h"

@implementation API_Handler
+(void)get_branches:(NSDictionary*)params onCompletionBlock:(ObjectBlock)completionBlock onerror:(ErrorBlock)errorblock {
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"https://secure-sdk.peekaboo.guru/kbprosamdmnioblcruahnhdcjhs_ahajlhljlgjhaskjgl4"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"7db159dc932ec461c1a6b9c1778bb2b0" forHTTPHeaderField:@"ownerkey"];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSLog(@"RESPONSE: %@",response);
        NSLog(@"DATA: %@",data);
        
        if (!error)
        {
            // Success
            if ([response isKindOfClass:[NSHTTPURLResponse class]])
            {
                NSError *jsonError;
                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                if (jsonError)
                {
                    // Error Parsing JSON
                }
                else
                {
                    // Success Parsing JSON
                    // Log NSDictionary response:
                    completionBlock(jsonResponse);
                    NSLog(@"%@",jsonResponse);
                }
                
            }
            else
            {
                //Web server is returning an error
            }
        }
        else
        {
            // Fail
            NSLog(@"error : %@", error.description);
        }
        
    }];
    
    [postDataTask resume];
    
}

@end
