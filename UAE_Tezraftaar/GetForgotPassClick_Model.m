//
//  GetForgotPassClick_Model.m
//
//  Created by   on 08/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GetForgotPassClick_Model.h"
#import "OutdtDataset.h"


NSString *const kGetForgotPassClick_ModelStrUserNameOut = @"strUserNameOut";
NSString *const kGetForgotPassClick_ModelResponse = @"Response";
NSString *const kGetForgotPassClick_ModelOutdtData = @"outdtData";
NSString *const kGetForgotPassClick_ModelOutdtDataset = @"outdtDataset";
NSString *const kGetForgotPassClick_ModelStrReturnMessage = @"strReturnMessage";


@interface GetForgotPassClick_Model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GetForgotPassClick_Model

@synthesize strUserNameOut = _strUserNameOut;
@synthesize response = _response;
@synthesize outdtData = _outdtData;
@synthesize outdtDataset = _outdtDataset;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.strUserNameOut = [self objectOrNilForKey:kGetForgotPassClick_ModelStrUserNameOut fromDictionary:dict];
            self.response = [self objectOrNilForKey:kGetForgotPassClick_ModelResponse fromDictionary:dict];
            self.outdtData = [self objectOrNilForKey:kGetForgotPassClick_ModelOutdtData fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kGetForgotPassClick_ModelOutdtDataset]];
            self.strReturnMessage = [self objectOrNilForKey:kGetForgotPassClick_ModelStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.strUserNameOut forKey:kGetForgotPassClick_ModelStrUserNameOut];
    [mutableDict setValue:self.response forKey:kGetForgotPassClick_ModelResponse];
    [mutableDict setValue:self.outdtData forKey:kGetForgotPassClick_ModelOutdtData];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kGetForgotPassClick_ModelOutdtDataset];
    [mutableDict setValue:self.strReturnMessage forKey:kGetForgotPassClick_ModelStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.strUserNameOut = [aDecoder decodeObjectForKey:kGetForgotPassClick_ModelStrUserNameOut];
    self.response = [aDecoder decodeObjectForKey:kGetForgotPassClick_ModelResponse];
    self.outdtData = [aDecoder decodeObjectForKey:kGetForgotPassClick_ModelOutdtData];
    self.outdtDataset = [aDecoder decodeObjectForKey:kGetForgotPassClick_ModelOutdtDataset];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kGetForgotPassClick_ModelStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_strUserNameOut forKey:kGetForgotPassClick_ModelStrUserNameOut];
    [aCoder encodeObject:_response forKey:kGetForgotPassClick_ModelResponse];
    [aCoder encodeObject:_outdtData forKey:kGetForgotPassClick_ModelOutdtData];
    [aCoder encodeObject:_outdtDataset forKey:kGetForgotPassClick_ModelOutdtDataset];
    [aCoder encodeObject:_strReturnMessage forKey:kGetForgotPassClick_ModelStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    GetForgotPassClick_Model *copy = [[GetForgotPassClick_Model alloc] init];
    
    if (copy) {

        copy.strUserNameOut = [self.strUserNameOut copyWithZone:zone];
        copy.response = [self.response copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
