//
//  Mpin_login_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 15/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface Mpin_login_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_mpin;
    IBOutlet UIButton* btn_touch_id;

    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********

}


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UITextField *urlField;




@property (nonatomic, retain) IBOutlet UITextView* message;
@property (nonatomic, retain) IBOutlet UITextView* encrypted;
@property (nonatomic, retain) IBOutlet UITextView* decrypted;
@property (nonatomic, retain) IBOutlet UITextField* key;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UISwitch* separateline;

- (IBAction)encrypt:(id)sender;
- (IBAction)decrypt:(id)sender;

@end
