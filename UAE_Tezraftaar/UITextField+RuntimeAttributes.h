//
//  UITextField+RuntimeAttribute.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (RuntimeAttribute)

@property(nonatomic, assign) NSNumber* maxLength;
@property(nonatomic, assign) BOOL isEmailAddress;
@property(nonatomic, assign) BOOL isDecimalField;
@property(nonatomic, assign) BOOL isCnicNumber;

@end
