//
//  AccountStatement_ViewController.h
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@protocol SettingsDelegate <NSObject>
-(void)dismissSlideMenu;
@end

@interface Settings : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView* table;
    IBOutlet UILabel* lbl_ver;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (nonatomic, weak) id delegate;

//-(void)dismissSlideMenu;
-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_back:(id)sender;


@end





