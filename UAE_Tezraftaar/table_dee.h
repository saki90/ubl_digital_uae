//
//  Table2.h
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Table_dee : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *isActive;
@property (nonatomic, strong) NSString *fundsTransferEnabled;
@property (nonatomic, assign) id mobileNo;
@property (nonatomic, assign) id userAccountRole;
@property (nonatomic, assign) id isEmployee;
@property (nonatomic, strong) NSString *isFCY;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *relationshipID;
@property (nonatomic, strong) NSString *isFA;
@property (nonatomic, strong) NSString *isPBC;
@property (nonatomic, strong) NSString *isPinValid;
@property (nonatomic, strong) NSString *isENRP;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *isSTAFF;
@property (nonatomic, assign) id isCO;
@property (nonatomic, strong) NSString *statusCode;
@property (nonatomic, strong) NSString *loginName;
@property (nonatomic, assign) id tempMobileNo;
@property (nonatomic, assign) id email;
@property (nonatomic, assign) id userAccountTypeCode;
@property (nonatomic, strong) NSString *isCCARD;
@property (nonatomic, assign) id isCoGroupID;
@property (nonatomic, assign) id bOUserID;
@property (nonatomic, strong) NSString *isEnrp;
@property (nonatomic, strong) NSString *isETA;
@property (nonatomic, strong) NSString *isORION;
@property (nonatomic, assign) id employeeNo;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *baseCCY;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
