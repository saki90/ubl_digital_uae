//
//  Table1.h
//
//  Created by   on 16/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface LoadIBFTBanks : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *applicationCode;
@property (nonatomic, strong) NSString *bankName;
@property (nonatomic, strong) NSString *bankImd;
@property (nonatomic, strong) NSString *accountNumberLength;
@property (nonatomic, strong) NSString *minaccountNumberLength;
@property (nonatomic, strong) NSString *format;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
