//
//  MPin_change_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface MPin_change_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_old_pw;
    IBOutlet UITextField* txt_new_pw;
    IBOutlet UITextField* txt_retype_pw;

    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@end
