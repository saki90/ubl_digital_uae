//
//  add_bill_Otp_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 01/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "add_bill_Otp_VC.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface add_bill_Otp_VC ()<NSURLConnectionDataDelegate>
{
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    UIAlertController* alert;
    NSString* str_Otp;
    NSString* alert_chck;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pw;
    int txt_nam;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* chk_service_call;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation add_bill_Otp_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    lbl_heading.text = @"OTP AUTHENTICATION"; //gblclass.bill_type;
    
    //[NSString stringWithFormat:@"ADD %@",gblclass.acct_add_type]; //
    //gblclass.Mobile_No
    
    chk_service_call = @"0";
    ssl_count = @"0";
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    // Set textfield cursor color ::
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    //    arr_pin_pattern=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    //    arr_pw_pin=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    
    
    
    //To add a new payee via smartphone a four digits One Time Passcode is sent to your mobile number *******3456 via SMS. Please enter the sent code below.
    
    NSString *subStr ;//= [str substringWithRange:NSMakeRange(0, 20)];
    
    //NSLog(@"%@", gblclass.Mobile_No);
    alert_chck=@"";
    
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below.",subStr];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //        //  [self.timeoutLabel  setText:@"YES"];
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //
    //        [self presentViewController:myController animated:YES completion:nil];
    //
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btn_submit:(id)sender
{
    
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        
        
        [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter All Pin" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
    
    
    
    
    //NSLog(@"%@",gblclass.add_bill_type);
    
    //@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
    //****
    
    //    if ([gblclass.add_bill_type isEqualToString:@"Utility Bills"])
    //    {
    //        [self AddUtilityCompanies:@""];
    //    }
    //    else if ([gblclass.add_bill_type isEqualToString:@"Mobile Bills"])
    //    {
    //        [self AddUtilityCompanies:@""];
    //    }
    //    else if ([gblclass.add_bill_type isEqualToString:@"UBL Address"] || [gblclass.add_bill_type isEqualToString:@"UBL Cashline"] || [gblclass.add_bill_type isEqualToString:@"UBL Credit Card"] || [gblclass.add_bill_type isEqualToString:@"UBL Drive"] || [gblclass.add_bill_type isEqualToString:@"UBL Money"])
    //    {
    //        [self AddBillDetail:@""];
    //    }
    //    else if ([gblclass.add_bill_type isEqualToString:@"Broadband Internet Bills"])
    //    {
    //        [self ConfirmAddISPBill:@""];
    //    }
    //    else
    //    {
    //        [self AddBillCompanies:@""];
    //    }
    
    //****
}


-(void) ConfirmAddISPBill:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:str_Otp], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"tt_ID",@"strAccessKey",@"strtxtCustomerID",@"strOTPPIN", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"ConfirmAddISPBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              // NSString* responsecode =(NSDictionary *)[dic objectForKey:@"Response"];
              
              NSString* responsecode1 =[dic objectForKey:@"Response"];
              
              if([responsecode1 isEqualToString:@"0"])
              {
                  
                  //  _normaltxtnick.hidden=false;
                  //  _normaltxtnick.text=[dic objectForKey:@"strtxtNick"];
                  // strtxtNick": "SYSTEM",
                  //NSLog(@"Response done");
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                   alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  //                  [self showAlert:resstring :@"Attention"];
                  //NSLog(@"Done");
                  //[self parsearray];
                  //  [_tableviewcompany reloadData];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}





-(void) Add_Confirm_bill_detail:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values  count]);
//    NSLog(@"%@",gblclass.arr_values);
//    NSLog(@"%@",[gblclass.arr_values objectAtIndex:3]);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",[gblclass.arr_values objectAtIndex:3]]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                [encrypt encrypt_Data:str_Otp],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTT_ID",
                                                                   @"strSelectedBillType",
                                                                   @"lblCardNo",
                                                                   @"txtNickCL",
                                                                   @"strBranchSelectedValue",
                                                                   @"strOTPPin",
                                                                   @"strAccessKey",
                                                                   @"bIsConfirmationRequired",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    
    [manager POST:@"AddConfirmBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              // NSString* responsecode =(NSDictionary *)[dic objectForKey:@"Response"];
              
              NSString* responsecode1 =[dic objectForKey:@"Response"];
              
              if([responsecode1 isEqualToString:@"0"])
              {
                  //                  chk_confirm_add_isp=@"1";
                  //                  _normalview.hidden=NO;
                  //                  _normaltxtnick.hidden=NO;
                  //                  _normaltxtnick.text=[dic objectForKey:@"strtxtNick"];
                  //                  // strtxtNick": "SYSTEM",
                  //                  //NSLog(@"Response done");
                  
                  
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self showAlert:resstring :@"Attention"];
                  //NSLog(@"Done");
                  [self slideRight];
                  //bill_all
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                   alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else if([[dic objectForKey:@"Response"] isEqualToString:@"-1"])
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self showAlert:resstring :@"Attention"];
                  [self slideLeft];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  //                  UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //                  UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                  //                      [self slideLeft];
                  //                      [self dismissViewControllerAnimated:NO completion:nil];
                  //                  }];
                  //                  [alertController addAction:alertAction];
                  //                  [self presentViewController:alertController animated:NO completion:nil];
                  
              }
              else
              {
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  //[self showAlert:resstring :@"Attention"];
                  
                  //NSLog(@"Done");
                  //[self parsearray];
                  //[_tableviewcompany reloadData];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              [self clearOTP];
              [txt_1 becomeFirstResponder];
              txt_6.enabled = YES;
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}








-(void) AddBillDetail:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],[encrypt encrypt_Data:str_Otp],[encrypt encrypt_Data:@""], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strTT_ID",@"strBillNick",@"strSelectedBillType",@"strBranchSelectedValue",@"strLoanNumber",@"strRepaymentAccNumber",@"strCCNumber",@"strOTPPIN",@"strAccessKey", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"AddBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring =[dic objectForKey:@"strReturnMessage"];
              
              if([gblclass.add_bill_type isEqualToString:@"UBL Cashline"]||([gblclass.add_bill_type isEqualToString:@"UBL Credit Card"]))
              {
                  if([responsecode isEqualToString:@"0"])
                  {
                      NSString* msg= @"Do you want to add ";
                      
                      NSString* labelnam=[dic objectForKey:@"outlblTitle"];
                      msg= [msg stringByAppendingString:labelnam];
                      NSString* halfmsg= @" in your paylist.";
                      msg= [msg stringByAppendingString:halfmsg];
                      
                      [self showAlertwithcancel:msg :@"Attention"];
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                       alert_chck = @"1";
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              //NSLog(@"Done");
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if ([alert_chck isEqualToString:@"0"])
    {
        
        [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        if(buttonIndex ==0)
        {
            
            //  txtbillservice.text=@"";
            //  _txtbilltype.text=@"";
            
            //NSLog(@"%@",gblclass.add_bill_type);
            
            if([responsecode isEqualToString:@"0"])
            {
                
                if([gblclass.add_bill_type isEqualToString:@"UBL Cashline"]||([gblclass.add_bill_type isEqualToString:@"UBL Credit Card"]))
                {
                    //                    if([self.txtcompany.text length]==0)
                    //                   {
                    
                    //                   }
                    //                    else
                    //                    {
                    [self AddConfirmBillDetail:@""];
                    //                    }
                    //                    _txtcompany.text=@"";
                    //                    _txtphonenumber.text=@"";
                    //                    _txtnick.text=@"";
                    
                }
                else
                {
                    //                    _txtcompany.text=@"";
                    //                    _txtphonenumber.text=@"";
                    //                    _txtnick.text=@"";
                }
            }
            else
            {
                responsecode=@"";
            }
        }
        
    }
    else if ([alert_chck isEqualToString:@"1"])
    {
        if (buttonIndex == 0)
        {
            alert_chck = @"";
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
            }
        }
        else if(buttonIndex == 1)
        {
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:vc animated:NO completion:nil];
        }
    }
    else
    {
        
    }
    
}


-(void) AddConfirmBillDetail:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //NSLog(@"%lu",(unsigned long)[gblclass.arr_values count]);
    NSDictionary *dictparam;
    
    
    if ([gblclass.add_bill_type isEqualToString:@"Wiz Card"])
    {
        dictparam= [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                                        [encrypt encrypt_Data:str_Otp],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                                                        [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                                                        [encrypt encrypt_Data:gblclass.Udid],
                                                        [encrypt encrypt_Data:gblclass.token], nil]
                    
                                               forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                        @"strSessionId",
                                                        @"IP",
                                                        @"strTT_ID",
                                                        @"strSelectedBillType",
                                                        @"lblCardNo",
                                                        @"txtNickCL",
                                                        @"strBranchSelectedValue",
                                                        @"strOTPPin",
                                                        @"strAccessKey",
                                                        @"bIsConfirmationRequired",
                                                        @"Device_ID",
                                                        @"Token", nil]];
    }
    else
    {
        //NSLog(@"%@",gblclass.arr_values);
        
        dictparam= [NSDictionary dictionaryWithObjects:
                    [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:3]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                     [encrypt encrypt_Data:str_Otp],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:8]],
                     [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:9]],
                     [encrypt encrypt_Data:gblclass.Udid],
                     [encrypt encrypt_Data:gblclass.token], nil]
                                               forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                        @"strSessionId",
                                                        @"IP",
                                                        @"strTT_ID",
                                                        @"strSelectedBillType",
                                                        @"lblCardNo",
                                                        @"txtNickCL",
                                                        @"strBranchSelectedValue",
                                                        @"strOTPPin",
                                                        @"strAccessKey",
                                                        @"bIsConfirmationRequired",
                                                        @"Device_ID",
                                                        @"Token", nil]];
    }
    
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    
    [manager POST:@"AddConfirmBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //          NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              
              
              NSString* resstring =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
              
              
              //      [self showAlert:resstring :@"Attention"];
              //NSLog(@"Done");
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [self showAlert:resstring :@"Attention"];
                  
                  
                  if ([gblclass.add_bill_type isEqualToString:@"Wiz Card"])
                  {
                      
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else
                  {
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }else if ([[dic objectForKey:@"Response"] isEqualToString:@"-1"])
              {
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  [self slideLeft];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                  //                      [self slideLeft];
                  //                      [self dismissViewControllerAnimated:NO completion:nil];
                  //                  }];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                   alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  
                  [self clearOTP];
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              
              //[self parsearray];
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [self clearOTP];
              [txt_1 becomeFirstResponder];
              txt_6.enabled = YES;
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
              // [mine myfaildata];
              //NSLog(@"%@",error);
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    //[alert release];
}

-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    alert_chck=@"0";
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert2 show];
                   });
    //[alert release];
}


//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//
//
//        if (buttonIndex == 0)
//        {
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl=@"logout";
//                [self SSL_Call];
//            }
//
//            //[self mob_App_Logout:@""];
//
//            //Do something
//            //NSLog(@"1");
//        }
//        else if(buttonIndex == 1)
//        {
//            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//            [self presentViewController:vc animated:YES completion:nil];
//        }
//
//}


-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        
        [hud hideAnimated:YES];
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
    }
}


-(void) AddBill_Companies_UAE:(NSString *)strIndustry
{
    @try {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSString* acct_no_chck;
        NSLog(@"%@",gblclass.arr_values);
         
        //[gblclass.arr_values objectAtIndex:4]],
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", [gblclass.arr_values objectAtIndex:3]]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]], //@"ME_TELCO"],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:7]],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTtId",
                                                                       @"strAccessKey",
                                                                       @"strMerchanId",
                                                                       @"strtxtNick",
                                                                       @"strtxtCustomerID",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AddBillCompaniesUAE" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //           NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  responsecode =[dic objectForKey:@"Response"];
                  
                  NSString* resstring = [dic objectForKey:@"strReturnMessage"];
                  
                  //NSLog(@"Done");
                  
                  if ([responsecode isEqualToString:@"0"])
                  {
                      //***
                      
                      if ([gblclass.add_bill_type isEqualToString:@"Prepaid Vouchers"])
                      {
                          [self showAlert:resstring :@"Attention"];
                          [self slideRight];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];  //prepaid_load
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          //                          UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Success" message:resstring preferredStyle:UIAlertControllerStyleAlert];
                          //                          UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                          //                              [self slideRight];
                          //                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          //                              vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                          //                              [self presentViewController:vc animated:NO completion:nil];
                          //
                          //                          }];
                          //                          [alertController addAction:alertAction];
                          //                          [self presentViewController:alertController animated:YES completion:nil];
                          
                          //                      CATransition *transition = [ CATransition animation];
                          //                      transition.duration = 0.3;
                          //                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          //                      transition.type = kCATransitionPush;
                          //                      transition.subtype = kCATransitionFromRight;
                          //                      [self.view.window.layer addAnimation:transition forKey:nil];
                          //
                          //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                          //                       [self presentViewController:vc animated:NO completion:nil];
                      }
                      else
                      {
                          
                          txt_6.enabled = YES;
                          [self showAlert:resstring :@"Attention"];
                          [self slideRight];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];  //prepaid_load
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-33"]) {
                      
                      [txt_1 becomeFirstResponder];
                      txt_6.enabled = YES;
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      [self slideLeft];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      
                      //  UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //                  UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                      //                      [self slideLeft];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      //
                      //                  }];
                      //
                      //                  [alertViewController addAction:alertAction];
                      //                  [self presentViewController:alertViewController animated:YES completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      alert_chck = @"1";
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else
                  {
                      [txt_1 becomeFirstResponder];
                      txt_6.enabled = YES;
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert: [self stringByStrippingHTML : [dic objectForKey:@"strReturnMessage"]] :@"0"];
//                      [self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      
                   // [self showAlert:resstring :@"Attention"];
                  }
                  
                //[self parsearray];
                  [hud hideAnimated:YES];
                //[_tableviewcompany reloadData];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error)
              {
                //[mine myfaildata];
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self clearOTP];
                  [hud hideAnimated:YES];
//                [self custom_alert:@"Try again later" :@"0"];
                  [self custom_alert:Call_center_msg :@"0"];
            
              }];
    }
    @catch (NSException *exception)
    {
//        NSLog(@"%@",exception.reason);
//        NSLog(@"%@",exception.userInfo);
//        NSLog(@"%@",exception.debugDescription);
        [txt_1 becomeFirstResponder];
        txt_6.enabled = YES;
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) AddBillCompanies:(NSString *)strIndustry
{
    
    
    @try {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        
        //    name = [[NSMutableArray alloc]init];
        //    ttid = [[NSMutableArray alloc]init];
        //    ttacceskey = [[NSMutableArray alloc]init];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSString* acct_no_chck;
        
//        NSLog(@"%@",gblclass.arr_values);
        
        if ([gblclass.bill_click isEqualToString:@"Prepaid Vouchers"])
        {
            acct_no_chck = [gblclass.arr_values objectAtIndex:3];
        }
        else
        {
            acct_no_chck = [gblclass.arr_values objectAtIndex:7];
        }
        
        //[gblclass.arr_values objectAtIndex:7]
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", [gblclass.arr_values objectAtIndex:3]]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                    [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", acct_no_chck]],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strTtId",
                                                                       @"strAccessKey",
                                                                       @"strType",
                                                                       @"strtxtNick",
                                                                       @"strtxtCustomerID",
                                                                       @"strOTPPIN",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"AddBillCompanies" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //           NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  responsecode =[dic objectForKey:@"Response"];
                  
                  NSString* resstring = [dic objectForKey:@"strReturnMessage"];
                  
                  //NSLog(@"Done");
                  
                  if ([responsecode isEqualToString:@"0"])
                  {
                      //***
                      
                      if ([gblclass.add_bill_type isEqualToString:@"Prepaid Vouchers"])
                      {
                          [self showAlert:resstring :@"Attention"];
                          [self slideRight];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];  //prepaid_load
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          //                          UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"Success" message:resstring preferredStyle:UIAlertControllerStyleAlert];
                          //                          UIAlertAction *alertAction=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                          //                              [self slideRight];
                          //                              storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          //                              vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                          //                              [self presentViewController:vc animated:NO completion:nil];
                          //
                          //                          }];
                          //                          [alertController addAction:alertAction];
                          //                          [self presentViewController:alertController animated:YES completion:nil];
                          
                          //                      CATransition *transition = [ CATransition animation];
                          //                      transition.duration = 0.3;
                          //                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          //                      transition.type = kCATransitionPush;
                          //                      transition.subtype = kCATransitionFromRight;
                          //                      [self.view.window.layer addAnimation:transition forKey:nil];
                          //
                          //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          //                      vc = [storyboard instantiateViewControllerWithIdentifier:@"prepaid_load"];
                          //                       [self presentViewController:vc animated:NO completion:nil];
                      }
                      else
                      {
                          
                          txt_6.enabled = YES;
                          [self showAlert:resstring :@"Attention"];
                          [self slideRight];
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];  //prepaid_load
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-33"]) {
                      
                      [txt_1 becomeFirstResponder];
                      txt_6.enabled = YES;
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      [self slideLeft];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      
                      //                  UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //                  UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                      //                      [self slideLeft];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      //
                      //                  }];
                      //
                      //                  [alertViewController addAction:alertAction];
                      //                  [self presentViewController:alertViewController animated:YES completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      alert_chck = @"1";
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else
                  {
                      [txt_1 becomeFirstResponder];
                      txt_6.enabled = YES;
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      // [self showAlert:resstring :@"Attention"];
                  }
                  
                  //[self parsearray];
                  [hud hideAnimated:YES];
                  //  [_tableviewcompany reloadData];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Try again later" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [txt_1 becomeFirstResponder];
        txt_6.enabled = YES;
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
}


-(void) clearOTP {
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}

-(void) AddUtilityCompanies:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
//    NSLog(@"%@",gblclass.arr_values);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:1]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:2]],
                                [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",[gblclass.arr_values objectAtIndex:3]]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:5]],
                                [encrypt encrypt_Data:[gblclass.arr_values objectAtIndex:6]],
                                [encrypt encrypt_Data:str_Otp],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTtId",
                                                                   @"strAccessKey",
                                                                   @"strtxtNick",
                                                                   @"strtxtCustomerID",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"AddUtilityCompanies" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  CATransition *transition = [ CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"];
                  [self presentViewController:vc animated:NO completion:nil];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                   alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-33"]) {
                  
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  [self slideLeft];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  
                  //                  UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //                  UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                  //                      [self slideLeft];
                  //                      [self dismissViewControllerAnimated:NO completion:nil];
                  //
                  //                  }];
                  //                  [alertViewController addAction:alertAction];
                  //                  [self presentViewController:alertViewController animated:YES completion:nil];
                  
              }
              else
              {
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //  [self showAlert:resstring :@"Attention"];
              }
              
              
              //  [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"%@",error);
              [txt_1 becomeFirstResponder];
              txt_6.enabled = YES;
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void)slideLeft {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
}

-(void)slideRight {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    //   [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    
    // [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger MAX_DIGITS = 1;
    
    
    //    if ([textField isEqual:txt_1])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    if ([textField isEqual:txt_2])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    if ([textField isEqual:txt_3])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    if ([textField isEqual:txt_4])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    if ([textField isEqual:txt_5])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    if ([textField isEqual:txt_6])
    //    {
    //        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        for (int i = 0; i < [string length]; i++)
    //        {
    //            unichar c = [string characterAtIndex:i];
    //            if (![myCharSet characterIsMember:c])
    //            {
    //                return NO;
    //            }
    //            else
    //            {
    //                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    //            }
    //        }
    //        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    //    }
    //
    //    return YES;
    
    
    
    
    
    
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
//            NSLog(@"%@",nextResponder);
//            NSLog(@"%@",[textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]);
            
             
            if (textField.tag == 6)
            {
//                [hud showAnimated:YES];
//                [self.view addSubview:hud];
//                [hud showAnimated:YES];
//
//                //  textField.tag = [@"7" integerValue];
//
//
//                txt_6.enabled = NO;
//                txt_6.text=string;
//
//                str_Otp=@"";
//                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
//                NSLog(@"**** ALERT ***** SIX");
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                [hud showAnimated:YES];
                [hud hideAnimated:YES afterDelay:130];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
//                NSLog(@"**** ALERT ***** SERVICE CALL");
                [self SSL_Call];
                
             //   return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                 return 0;
            }
            
            textField = nil;
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
    return YES;
}

-(void)call_ssl_service
{
    chk_service_call = @"1";
    [self SSL_Call];
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"re_send_otp"])
        {
            chk_ssl=@"";
            [self Generate_OTP_For_Bill:@""];
        }
        else
        {
            [txt_6 resignFirstResponder];
            
            if ([gblclass.add_bill_type isEqualToString:@"Utility Bills"])
            {
                [self AddBill_Companies_UAE:@""]; // [self AddUtilityCompanies:@""];
            }
            else if ([gblclass.add_bill_type isEqualToString:@"Mobile Bills"])
            {
                // [self AddUtilityCompanies:@""];
                
                [self AddBillCompanies:@""];
            }
            else if ([gblclass.add_bill_type isEqualToString:@"UBL Address"] || [gblclass.add_bill_type isEqualToString:@"UBL Cashline"] || [gblclass.add_bill_type isEqualToString:@"UBL Credit Card"] || [gblclass.add_bill_type isEqualToString:@"UBL Drive"] || [gblclass.add_bill_type isEqualToString:@"UBL Money"])
            {
                // [self AddBillDetail:@""];
                
                [self AddConfirmBillDetail:@""];
            }
            else if ([gblclass.add_bill_type isEqualToString:@"ISP Bill"]) //Broadband Internet Bills
            {
                //  [self ConfirmAddISPBill:@""];
                
                [self Add_Confirm_bill_detail:@""];
            }
            else if ([gblclass.add_bill_type isEqualToString:@"Add_payee_omni"] ) //Broadband Internet Bills
            {
                
                [self AddPKRAccount:@""];
            }
            else if ([gblclass.add_bill_type isEqualToString:@"Wiz Card"])
            {
                
                [self AddConfirmBillDetail:@""];
            }
            else
            {
                
                [self AddBillCompanies:@""];
            }
        }
        
    }
    else
    {
        //
        //        if ([chk_service_call isEqualToString:@"1"])
        //        {
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
        //        }
        
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    chk_service_call = @"0";
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"re_send_otp"])
    {
        //[self.connection cancel];
        chk_ssl=@"";
        [self Generate_OTP_For_Bill:@""];
    }
    else
    {
        //NSLog(@"%@",gblclass.add_bill_type);
        [txt_6 resignFirstResponder];
//        NSLog(@"%@",gblclass.add_bill_type);
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"Utility Bills"])
        {
           [self AddBill_Companies_UAE:@""];
           // [self AddUtilityCompanies:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"Mobile Bills"])
        {
            // [self AddUtilityCompanies:@""];
            //[self.connection cancel];
            [self AddBillCompanies:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"UBL Address"] || [gblclass.add_bill_type isEqualToString:@"UBL Cashline"] || [gblclass.add_bill_type isEqualToString:@"UBL Credit Card"] || [gblclass.add_bill_type isEqualToString:@"UBL Drive"] || [gblclass.add_bill_type isEqualToString:@"UBL Money"])
        {
            // [self AddBillDetail:@""];
           // [self.connection cancel];
            [self AddConfirmBillDetail:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"ISP Bill"]) //Broadband Internet Bills
        {
            //  [self ConfirmAddISPBill:@""];
           // [self.connection cancel];
            [self Add_Confirm_bill_detail:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"Add_payee_omni"] ) //Broadband Internet Bills
        {
           // [self.connection cancel];
            [self AddPKRAccount:@""];
        }
        else if ([gblclass.add_bill_type isEqualToString:@"Wiz Card"])
        {
           // [self.connection cancel];
            [self AddConfirmBillDetail:@""];
        }
        else
        {
            //[self.connection cancel];
            [self AddBillCompanies:@""];
        }
        
       // [self.connection cancel];
    }
    
   // [self.connection cancel];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void) AddPKRAccount:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    // manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    //self.acceptableContentTypes
    //   manager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    //680801
    
    //NSLog(@"%@",gblclass.ibft_relation);
    
    if ([gblclass.ibft_relation isEqualToString:@""] || gblclass.ibft_relation.length==0)
    {
        gblclass.ibft_relation=@"";
    }
    
    
//    NSLog(@"%@",gblclass.M3sessionid);
//    NSLog(@"%@",[GlobalStaticClass getPublicIp]);
//    NSLog(@"%@",gblclass.acc_type);
//    NSLog(@"%@",gblclass.bankname);
//    NSLog(@"%@",gblclass.bankimd);
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.acc_number);
//    NSLog(@"%@",gblclass.base_currency);
//    NSLog(@"%@",gblclass.acctitle);
//    NSLog(@"%@",gblclass.acct_nick);
//    NSLog(@"%@",gblclass.add_payee_email);
//    NSLog(@"%@",gblclass.ibft_relation);
//    NSLog(@"%@", str_Otp);
//    NSLog(@"%@", gblclass.Udid);
//    NSLog(@"%@", gblclass.token);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.acc_type],
                                [encrypt encrypt_Data:gblclass.bankname],
                                [encrypt encrypt_Data:gblclass.bankimd],
                                [encrypt encrypt_Data:gblclass.fetchtitlebranchcode],
                                [encrypt encrypt_Data:[gblclass.acc_number uppercaseString]],
                                [encrypt encrypt_Data:gblclass.base_currency],
                                [encrypt encrypt_Data:gblclass.acctitle],
                                [encrypt encrypt_Data:gblclass.acct_nick],
                                [encrypt encrypt_Data:gblclass.add_payee_email],
                                [encrypt encrypt_Data:gblclass.ibft_relation],
                                [encrypt encrypt_Data:str_Otp],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"struserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccType",
                                                                   @"strBankName",
                                                                   @"strBankImd",
                                                                   @"strBranchCode",
                                                                   @"strAccountNumber",
                                                                   @"StrBaseCCY",
                                                                   @"strlblAccountTitle",
                                                                   @"strlblAccountNick",
                                                                   @"strAccountEmail",
                                                                   @"strIBFTRelation",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token",nil]];
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"AddPKRAccount" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              //**   [self showAlert:responsemsg :@"Attention"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  //**  [self dismissModalStack];
                  UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                    message:[dic objectForKey:@"strReturnMessage"]
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];

                  [message show];
                   
                  CATransition *transition = [CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"payee_list"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [self clearOTP];
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [self clearOTP];
              [txt_1 becomeFirstResponder];
              txt_6.enabled = YES;
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
              
          }];
}

-(void)dismissModalStack
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"YourDismissAllViewControllersIdentifier" object:self];
}



-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(IBAction)btn_Re_Generate_OTP:(id)sender
{
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"re_send_otp";
        [self SSL_Call];
    }
    
}


-(void) Generate_OTP_For_Bill:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
//    NSLog(@"%@",gblclass.arr_re_genrte_OTP_additn);
    
    //    [gblclass.arr_values addObject:gblclass.user_id];
    //    [gblclass.arr_values addObject:gblclass.M3sessionid];
    //    [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
    //    [gblclass.arr_values addObject:strTtId];
    //    [gblclass.arr_values addObject:strAccessKey];
    //    [gblclass.arr_values addObject:strtxtNick];sghdjh
    //    [gblclass.arr_values addObject:strtxtCustomerID];
    
    
//    NSLog(@"%@",gblclass.arr_re_genrte_OTP_additn);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:0]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:1]],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:2]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:3]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:4]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:5]],
                                [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", [gblclass.arr_re_genrte_OTP_additn objectAtIndex:6]]],
                                [encrypt encrypt_Data:[gblclass.arr_re_genrte_OTP_additn objectAtIndex:7]],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"ADDITION"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"SMS",@"ADDITION",@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil]
    //                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"M3Key",nil]];
    
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],@"",str_account_no,strAccessKey,@"Generate OTP",strTtId,tc_access_key,@"SMS",@"ADDITION",@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil]
    //                                                          forKeys:[NSArray arrayWithObjects:@"UserId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"M3Key", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  txt_1.text=@"";
                  txt_2.text=@"";
                  txt_3.text=@"";
                  txt_4.text=@"";
                  txt_5.text=@"";
                  txt_6.text=@"";
                  
                  [self custom_alert:resstring :@"1"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                   alert_chck = @"1";
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              }
              else
              {
                  [txt_1 becomeFirstResponder];
                  txt_6.enabled = YES;
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [txt_1 becomeFirstResponder];
              txt_6.enabled = YES;
              [self clearOTP];
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later" :@"0"];
          }];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end
