//
//  RegisterEmailViewController.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 15/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RegisterEmailViewController.h"
#import "UIViewController+TextFieldDelegate.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"

#import "SWRevealViewController.h"
#import "Slide_menu_VC.h"
#import "TransitionDelegate.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
 
#import "Reachability.h"
#import "Encrypt.h"
#import "Globals.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>


#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."


@interface RegisterEmailViewController () <NSURLConnectionDataDelegate> {
    
    GlobalStaticClass* gblclass;
    TransitionDelegate *transitionController;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIAlertController *alert;
    MBProgressHUD* hud;
    AVPlayer *avplayer;
    BOOL runOnce;
    NSString* chk_ssl;
    NSInteger randomNumber ;
    NSMutableArray*  a;
    NSDictionary *dic;
    NSMutableData *responseData;
    NSURLConnection *connection;
    UITextView *textOutput;
    NSString* login_status_chck;
    //    Reachability *internetReach;
    //    BOOL netAvailable;
    NSString* chck_datt;
    NSDate *to_date,*frm_date;
    NSDateFormatter * frm_dat,* to_dat;
    NSDate  *dt_frm;
    NSDate* dt_to;
    Encrypt* encrypt;
    NSString* ssl_count;
    NSString* t_n_c;
    NSString* first_time_chk;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@property (strong, nonatomic) IBOutlet UILabel *lbl_heading;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) IBOutlet UIButton *dob;
@property (strong, nonatomic) IBOutlet UIView *emailAddressBottomView;
@property (strong, nonatomic) IBOutlet UITextField *mobileNo;
@property (strong, nonatomic) IBOutlet UIView *mobileNoBottomView;
@property (strong, nonatomic) IBOutlet UITextField *accountNumber;
@property (strong, nonatomic) IBOutlet UIView *accountNumberBottomView;
@property (strong, nonatomic) IBOutlet UIButton *btn_next_outlet;
@property (strong, nonatomic) IBOutlet UIImageView *movieView;
@property (strong, nonatomic) IBOutlet UIView* vw_datt;
@property (strong, nonatomic) IBOutlet UIDatePicker* datt_picker;

- (IBAction)btn_selectDob:(id)sender;
- (IBAction)btn_next:(id)sender;
- (IBAction)btn_datt_cancel:(id)sender;
- (IBAction)btn_datt_done:(id)sender;


@end


@implementation RegisterEmailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    vc = [[UIViewController alloc] init];
    //
    //    self.emailAddress.delegate = vc;
    //    self.mobileNo.delegate = vc;
    //    self.accountNumber.delegate = vc;
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    runOnce = NO;
    ssl_count = @"0";
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    
    if([gblclass.btn_reg_selected isEqualToString:@"email"])
    {
        self.emailAddress.hidden=NO;
        //        self.emailAddress.enabled=YES;
        //        self.emailAddress.placeholder=@"Enter Email address";
        self.dob.hidden=YES;
        // self.dob.enabled=NO;
        
    }
    else if([gblclass.btn_reg_selected isEqualToString:@"dob"])
    {
        self.emailAddress.hidden=YES;
        //        self.emailAddress.enabled=NO;
        //        self.emailAddress.placeholder=@"MM/DD/YYYY";
        self.dob.hidden=NO;
        // self.dob.enabled=YES;
        
    }
    
    self.emailAddress.delegate = self;
    self.mobileNo.delegate = self;
    self.accountNumber.delegate = self;
    
    
    transitionController = [[TransitionDelegate alloc] init];
    a=[[NSMutableArray alloc] init];
    
    self.lbl_heading.text=@"Please register your device for UBL Digital Mobile App.";
    
    //   UIFont *font = [UIFont fontWithName:@"Aspira-Light" size:14];
    //   [lbl_heading setFont:font];
    
    //    UIGraphicsBeginImageContext(self.view.frame.size);
    //    [[UIImage imageNamed:@"login-bg.png"] drawInRect:self.view.bounds];
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    
    
    
    //
    //    self.lbl_heading.frame = CGRectMake(self.view.frame.size.width + self.lbl_heading.frame.size.width, self.lbl_heading.frame.origin.y, self.lbl_heading.frame.size.width, self.lbl_heading.frame.size.height);
    //
    //    self.emailAddress.frame = CGRectMake(self.view.frame.size.width + self.emailAddress.frame.size.width, self.emailAddress.frame.origin.y, self.emailAddress.frame.size.width, self.emailAddress.frame.size.height);
    //    self.emailAddressBottomView.frame = CGRectMake(self.view.frame.size.width + self.emailAddressBottomView.frame.size.width, self.emailAddressBottomView.frame.origin.y, self.emailAddressBottomView.frame.size.width, self.emailAddressBottomView.frame.size.height);
    //
    //    self.mobileNo.frame = CGRectMake(self.view.frame.size.width + self.mobileNo.frame.size.width, self.mobileNo.frame.origin.y, self.mobileNo.frame.size.width, self.mobileNo.frame.size.height);
    //    self.mobileNoBottomView.frame = CGRectMake(self.view.frame.size.width + self.mobileNoBottomView.frame.size.width, self.mobileNoBottomView.frame.origin.y, self.mobileNoBottomView.frame.size.width, self.mobileNoBottomView.frame.size.height);
    //
    //    self.accountNumber.frame = CGRectMake(self.view.frame.size.width + self.accountNumber.frame.size.width, self.accountNumber.frame.origin.y, self.accountNumber.frame.size.width, self.accountNumber.frame.size.height);
    //    self.accountNumberBottomView.frame = CGRectMake(self.view.frame.size.width + self.accountNumberBottomView.frame.size.width, self.accountNumberBottomView.frame.origin.y, self.accountNumberBottomView.frame.size.width, self.accountNumberBottomView.frame.size.height);
    //
    //    self.btn_next_outlet.frame = CGRectMake(self.view.frame.size.width + self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.origin.y, self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.size.height);
    //
    //
    //    [NSTimer scheduledTimerWithTimeInterval:0.2
    //                                     target:self
    //                                   selector:@selector(showAnimation)
    //                                   userInfo:nil
    //                                    repeats:NO];
    //
    
    [self Play_bg_video];
}


// ********** Page Controller Start ******************

//-(void)page_control
//{
//    UIPageControl *pageControl = [UIPageControl appearance];
//    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
//    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
//    pageControl.backgroundColor = [UIColor whiteColor];
//
//
//    _pageTitles = @[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Bookmark Favorite Tip", @"Free Regular Update"];
//    _pageImages = @[@"page1.png", @"page2.png", @"page3.png", @"page4.png"];
//
//    // Create page view controller
//    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
//    self.pageViewController.dataSource = self;
//
//    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
//    NSArray *viewControllers = @[startingViewController];
//    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
//
//    // Change the size of page view controller
//    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height );
//
//    [self addChildViewController:_pageViewController];
//    [self.view addSubview:_pageViewController.view];
//    [self.pageViewController didMoveToParentViewController:self];
//
//}

//- (IBAction)startWalkthrough:(id)sender
//{
//    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
//    NSArray *viewControllers = @[startingViewController];
//    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
//}

//- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
//{
//    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
//        return nil;
//    }
//
//    // Create a new view controller and pass suitable data.
//    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
//    pageContentViewController.imageFile = self.pageImages[index];
//    pageContentViewController.titleText = self.pageTitles[index];
//    pageContentViewController.pageIndex = index;
//
//    return pageContentViewController;
//}

#pragma mark - Page View Controller Data Source

//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
//{
//    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
//
//    if ((index == 0) || (index == NSNotFound))
//    {
//        return nil;
//    }
//
//    index--;
//
//    return [self viewControllerAtIndex:index];
//}

//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
//{
//    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
//
//    if (index == NSNotFound)
//    {
//        return nil;
//    }
//
//    index++;
//
//    if (index == [self.pageTitles count])
//    {
//        return nil;
//    }
//
//    return [self viewControllerAtIndex:index];
//}

//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
//{
//    return [self.pageTitles count];
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 0;
//}


// ********** Page Controller End ******************



-(void) showAnimation
{
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.lbl_heading.frame = CGRectMake((self.view.frame.size.width/2) - (self.lbl_heading.frame.size.width/2), self.lbl_heading.frame.origin.y, self.lbl_heading.frame.size.width, self.lbl_heading.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        
        self.emailAddress.frame = CGRectMake((self.view.frame.size.width/2) - (self.emailAddress.frame.size.width/2), self.emailAddress.frame.origin.y, self.emailAddress.frame.size.width, self.emailAddress.frame.size.height);
        
        self.emailAddressBottomView.frame = CGRectMake((self.view.frame.size.width/2) - (self.emailAddressBottomView.frame.size.width/2), self.emailAddressBottomView.frame.origin.y, self.emailAddressBottomView.frame.size.width, self.emailAddressBottomView.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        
        self.mobileNo.frame = CGRectMake((self.view.frame.size.width/2) - (self.mobileNo.frame.size.width/2), self.mobileNo.frame.origin.y, self.mobileNo.frame.size.width, self.mobileNo.frame.size.height);
        
        self.mobileNoBottomView.frame = CGRectMake((self.view.frame.size.width/2) - (self.mobileNoBottomView.frame.size.width/2), self.mobileNoBottomView.frame.origin.y, self.mobileNoBottomView.frame.size.width, self.mobileNoBottomView.frame.size.height);
        
    }];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.accountNumber.frame = CGRectMake((self.view.frame.size.width/2) - (self.accountNumber.frame.size.width/2), self.accountNumber.frame.origin.y, self.accountNumber.frame.size.width, self.accountNumber.frame.size.height);
        
        self.accountNumberBottomView.frame = CGRectMake((self.view.frame.size.width/2) - (self.accountNumberBottomView.frame.size.width/2), self.accountNumberBottomView.frame.origin.y, self.accountNumberBottomView.frame.size.width, self.accountNumberBottomView.frame.size.height);
        
    }];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.btn_next_outlet.frame = CGRectMake((self.view.frame.size.width/2) - (self.btn_next_outlet.frame.size.width/2), self.btn_next_outlet.frame.origin.y, self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.size.height);
        
    }];
    
    
}

//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
//
//
//    [manager stopUpdatingLocation];
//
//    gblclass.arr_current_location = [[NSMutableArray alloc] init];
//    float latitude = myLcationManager.location.coordinate.latitude;
//    float longitude = myLcationManager.location.coordinate.longitude;
//
//    NSString *lati=[NSString stringWithFormat:@"%f",latitude];
//    NSString *longi=[NSString stringWithFormat:@"%f",longitude];
//    [gblclass.arr_current_location addObject:lati];
//    [gblclass.arr_current_location addObject:longi];
//
//}



-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    if ([avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [avplayer seekToTime:kCMTimeZero];
    [avplayer setVolume:0.0f];
    [avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
    
    //    txt_pw1.text=@"";
    
    //    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults] stringForKey:@"enable_touch"];
    //
    //
    //    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    //    {
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //    }
    //    else
    //    {
    //
    //    }
    
    
    
    //    if (runOnce == YES) {
    //
    //
    //        self.lbl_heading.frame = CGRectMake(-(self.view.frame.size.width + self.lbl_heading.frame.size.width) ,self.lbl_heading.frame.origin.y, self.lbl_heading.frame.size.width, self.lbl_heading.frame.size.height);
    //
    //        self.emailAddress.frame = CGRectMake(-(self.view.frame.size.width + self.emailAddress.frame.size.width), self.emailAddress.frame.origin.y, self.emailAddress.frame.size.width, self.emailAddress.frame.size.height);
    //
    //        //        emailView.frame = CGRectMake(-(self.view.frame.size.width + emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
    //
    //        self.accountNumber.frame = CGRectMake(-(self.view.frame.size.width + self.accountNumber.frame.size.width), self.accountNumber.frame.origin.y, self.accountNumber.frame.size.width, self.accountNumber.frame.size.height);
    //
    //        //        passView.frame = CGRectMake(-(self.view.frame.size.width + passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
    //
    //        self.btn_next_outlet.frame = CGRectMake(-(self.view.frame.size.width + self.btn_next_outlet.frame.size.width), self.btn_next_outlet.frame.origin.y, self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.size.height);
    //
    //        [self showAnimation];
    //
    //
    //    }
    
    
    
    //    [UIView animateWithDuration:10.f animations:^{
    //        self.view.frame = CGRectMake(0.f, 0.f, 320.f, 568.f);
    //    }];
    
    [avplayer play];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [avplayer play];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //777    BOOL stringIsValid;
    NSInteger MAX_DIGITS=13;
    
    
    if ([theTextField isEqual:_emailAddress])
    {
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@._/"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:_mobileNo])
    {
        MAX_DIGITS = 11;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:_accountNumber])
    {
        
        MAX_DIGITS = 4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    
    return YES;
}


- (IBAction)btn_selectDob:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([gblclass.btn_reg_selected isEqualToString:@"dob"]) {
        
        if (self.vw_datt.isHidden==YES) {
            
            self.datt_picker.hidden=NO;
            self.vw_datt.hidden=NO;
            
        }
        else
        {
            self.vw_datt.hidden=YES;
        }
    }
}

-(IBAction)btn_next:(id)sender
{
    
    //    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNetworkChnageNotification:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    
    
    [_emailAddress resignFirstResponder];
    [_mobileNo resignFirstResponder];
    [_accountNumber resignFirstResponder];
    
    if ([gblclass.isEmail_Exist isEqualToString:@"1"] && [gblclass.btn_reg_selected isEqualToString:@"email"])
    {
        if ([self.emailAddress.text isEqualToString:@""])
        {
            [self custom_alert:@"Please enter registered email address" :@"0"];
            return ;
        }
    }
    else
    {
        if ([self.dob.titleLabel.text isEqualToString:@"MM/DD/YYYY"])
        {
            [self custom_alert:@"Please select your date of birth" :@"0"];
            return ;
        }
    }
    
    if([self.mobileNo.text isEqualToString:@""] || [self.mobileNo.text length] < 11) {
        
        [self custom_alert:@"Please enter registered mobile number" :@"0"];
        return ;
        
    } else if ([self.mobileNo.text isEqualToString:@""] || [self.mobileNo.text length] < 4) {
        
        [self custom_alert:@"Please enter 4 digit ATM PIN" :@"0"];
        return ;
    }
    else
    {
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        gblclass.signup_mobile = self.mobileNo.text;
        //        gblclass.dob = self.emailAddress.text;
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"User_Signup_Step2";
            [self SSL_Call];
        }
    }
    
}


-(void) UserSignup__Non_DebitCard_Step2:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    UIDevice *deviceInfo = [UIDevice currentDevice];
    
    
    randomNumber = arc4random() % 10+1;
    NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
    
    gblclass.login_session=random;
    NSDictionary *dictparam = [[NSDictionary alloc] init];
    
    if ([gblclass.btn_reg_selected isEqualToString:@"email"])
    {
        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:@"1234"],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"abc"],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.sign_up_token],[encrypt encrypt_Data:gblclass.cnic_no],[encrypt encrypt_Data:gblclass.acc_number],[encrypt encrypt_Data:gblclass.isEmail_Exist],[encrypt encrypt_Data:self.emailAddress.text],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:self.mobileNo.text], nil]
                     
                                                forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"LogBrowserInfo",@"Device_ID",
                                                         @"Token",@"strCNIC",@"strAccountNumber",@"isEmail_Exist",@"email",@"DOB",@"mobile_num", nil]];
    }
    else if ([gblclass.btn_reg_selected isEqualToString:@"dob"])
    {
        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:@"1234"],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:@"abc"],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.sign_up_token],[encrypt encrypt_Data:gblclass.cnic_no],[encrypt encrypt_Data:gblclass.acc_number],[encrypt encrypt_Data:gblclass.isEmail_Exist],[encrypt encrypt_Data:@""],[encrypt encrypt_Data:self.dob.titleLabel.text],[encrypt encrypt_Data:self.mobileNo.text], nil]
                     
                                                forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"LogBrowserInfo",@"Device_ID",
                                                         @"Token",@"strCNIC",@"strAccountNumber",@"isEmail_Exist",@"email",@"DOB",@"mobile_num", nil]];
    }
    
    
    //    NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"UserSignup__Non_DebitCard_Step2" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSError *error1=nil;
         //NSArray *arr = (NSArray *)responseObject;
         dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
         
         
         if ([[dic objectForKey:@"Response"] integerValue]==0)
         {
             [avplayer pause];
             [hud hideAnimated:YES];
             [self slide_right];
             mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
             vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
             [self presentViewController:vc animated:NO completion:nil];
             
         } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
             [hud hideAnimated:YES];
             [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
             return ;
         } else {
             [hud hideAnimated:YES];
             [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
         }
         
         [hud hideAnimated:YES];
     }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              
              //   NSLog(@"%@",[error localizedDescription]);
              
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}



- (NSString *)platformRawString
{
    size_t size;
    //sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    //   sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

- (NSString *)platformNiceString
{
    
    NSString *platform = [self platformRawString];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (4G,2)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (4G,3)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

//-(void) UserLogin:(NSString *)strIndustry
//{
//
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
//                                                                                   ]];//baseURL];
//
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

//
//    UIDevice *deviceInfo = [UIDevice currentDevice];
//
//    //NSLog(@"Device name:  %@", deviceInfo.name);
//
//
//
//    login_name = [txt_login.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//
//    randomNumber = arc4random() % 10+1;
//
//    NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
//
//    gblclass.login_session=random;
//
//    //NSLog(@"%ld",(long)randomNumber);
//
//
//
//    //    NSData *jsonSource = [NSData dataWithContentsOfURL:
//    //                          [NSURL URLWithString:@"http://codlobbyz.com/app/service.php"]];
//    //    NSError *err;
//    //    id jsonObjects = [NSJSONSerialization JSONObjectWithData:
//    //                      jsonSource options:NSJSONReadingMutableContainers error:&err];
//    //    //NSLog(@"%@", err);
//    //
//
//
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:login_name,random,@"1234",[GlobalStaticClass getPublicIp],@"osdf",nil] forKeys:[NSArray
//                                                                                                                                                       arrayWithObjects:@"strLoginName",@"strSerialNumber",@"strSessionId",@"IP",@"LogBrowserInfo",nil]];
//
//
//    [manager.requestSerializer setTimeoutInterval:time_out];
//
//    [manager POST:@"UserLogin" parameters:dictparam progress:nil
//
//          success:^(NSURLSessionDataTask *task, id responseObject) {
//              //      NSError *error;
//              //      NSArray *arr = (NSArray *)responseObject;
//              dic = (NSDictionary *)responseObject;
//
//              //  LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
//
//              // //NSLog(@"%@",[dic objectForKey:@"response"]);
//              login_status_chck=[dic objectForKey:@"Response"];
//              gblclass.pin_pattern=[dic objectForKey:@"PinPattern"];
//              gblclass.user_id=[dic objectForKey:@"UserId"];
//              gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
//
//
//              if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
//              {
//
//                  [hud hideAnimated:YES];
//
//                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
//
//                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                  [alert addAction:ok];
//
//                  [self presentViewController:alert animated:YES completion:nil];
//
//                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
//
//                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
//                  [self presentViewController:vc animated:NO completion:nil];
//                  [hud hideAnimated:YES];
//
//                  return ;
//              }
//              else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
//              {
//
//                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
//
//                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
//                  [self presentViewController:vc animated:NO completion:nil];
//                  [hud hideAnimated:YES];
//
//                  return ;
//              }
//              else if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==-96)
//              {
//                  [self segue];
//              }
//
//              [hud hideAnimated:YES];
//
//              [self segue];
//
//          }
//
//          failure:^(NSURLSessionDataTask *task, NSError *error) {
//              // [mine myfaildata];
//              [hud hideAnimated:YES];
//
//
//              [self custom_alert:[error localizedDescription] :@"0"];
//
//              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
//              //
//              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//              //              [alert addAction:ok];
//              //              [self presentViewController:alert animated:YES completion:nil];
//
//          }];
//}

-(void)segue
{
    if ([login_status_chck isEqualToString:@"0"] || [login_status_chck isEqualToString:@"-96"])
    {
        
        //NSLog(@"%@",txt_login.text);
        //        gblclass.user_login_name=login_name;
        
        
        //        [timer_class createTimer];
        
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        
        
        //        lbl_heading.frame = CGRectMake(self.view.frame.size.width + lbl_heading.frame.size.width, lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        //
        //        emailOutlet.frame = CGRectMake(self.view.frame.size.width + emailOutlet.frame.size.width, emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //        emailView.frame = CGRectMake(self.view.frame.size.width + emailView.frame.size.width, emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        passOutlet.frame = CGRectMake(self.view.frame.size.width + passOutlet.frame.size.width, passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //        passView.frame = CGRectMake(self.view.frame.size.width + passView.frame.size.width, passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        forgetPass.frame = CGRectMake(self.view.frame.size.width + forgetPass.frame.size.width, forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //        btn_login_color.frame = CGRectMake(self.view.frame.size.width + btn_login_color.frame.size.width, btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        
        runOnce = YES;
        
        
        
        [UIView animateWithDuration:0.4 animations:^{
            
            self.lbl_heading.frame = CGRectMake(-(self.view.frame.size.width+ self.lbl_heading.frame.size.width), self.lbl_heading.frame.origin.y, self.lbl_heading.frame.size.width, self.lbl_heading.frame.size.height);
            
        }];
        
        [UIView animateWithDuration:0.6 animations:^{
            
            self.emailAddress.frame = CGRectMake(-(self.view.frame.size.width+ self.emailAddress.frame.size.width), self.emailAddress.frame.origin.y, self.emailAddress.frame.size.width, self.emailAddress.frame.size.height);
            //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
            
        }];
        
        [UIView animateWithDuration:0.8 animations:^{
            
            self.mobileNo.frame = CGRectMake(-(self.view.frame.size.width+ self.mobileNo.frame.size.width), self.mobileNo.frame.origin.y, self.mobileNo.frame.size.width, self.mobileNo.frame.size.height);
            //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
            
        }];
        
        [UIView animateWithDuration:0.8 animations:^{
            
            self.accountNumber.frame = CGRectMake(-(self.view.frame.size.width+ self.accountNumber.frame.size.width), self.accountNumber.frame.origin.y, self.accountNumber.frame.size.width, self.accountNumber.frame.size.height);
            //            passView.frame = CGRectMake(-(self.view.frame.size.width+passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
            
        }];
        
        [UIView animateWithDuration:1 animations:^{
            
            //            forgetPass.frame = CGRectMake(-(self.view.frame.size.width+forgetPass.frame.size.width), forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
            
            
        }];
        
        [UIView animateWithDuration:1.1 animations:^{
            
            self.btn_next_outlet.frame = CGRectMake(-(self.view.frame.size.width+ self.btn_next_outlet.frame.size.width), self.btn_next_outlet.frame.origin.y, self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.size.height);
            
            
        }];
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.8
                                         target:self
                                       selector:@selector(present_Controller)
                                       userInfo:nil
                                        repeats:NO];
        
        
        
        //        CATransition *transition = [ CATransition animation ];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        
        //[self performSegueWithIdentifier:@"question" sender:nil];
    }
    else
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Retry" :@"0"];
        
    }
}

-(void)present_Controller
{
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailAddress resignFirstResponder];
    [self.mobileNo resignFirstResponder];
    [self.accountNumber resignFirstResponder];
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self.view endEditing:YES];
//}

#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //  //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_feature:(id)sender
{
    
    if ([t_n_c isEqualToString:@"1"] || [t_n_c isEqualToString:@"2"])
    {
        [hud showAnimated:YES];
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
    }
    else
    {
        [self custom_alert:@"Please register your User." :@"0"];
    }
}

-(IBAction)btn_offer:(id)sender
{
    
//    gblclass.tab_bar_login_pw_check=@"login";
//    [self peekabooSDK:@"deals"];
    

    //saki     [self custom_alert:Offer_msg :@""];
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    //saki     [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_Mpin:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Mpin_login"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_back:(id)sender
{
    // gblclass.user_login_name=@"";
    
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
}



///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"User_Signup_Step2"])
        {
            chk_ssl=@"";
            [self UserSignup__Non_DebitCard_Step2:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"User_Signup_Step2"])
    {
        chk_ssl=@"";
        [self UserSignup__Non_DebitCard_Step2:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
    }
    
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (responseData == nil)
    {
        responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    //    NSLog(@"%@", error.localizedDescription);
}


- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = textOutput.text;
    textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]){
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil){
        return false;
    }
    else {
        return true;
    }
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
}


///////////************************** SSL PINNING END ******************************////////////////


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"2"])
        {
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else
        {
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
            [self presentViewController:vc animated:NO completion:nil];
        }
    }
    else if(buttonIndex == 1)
    {
        
    }
    
}




-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                       
                   });
    
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


//-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
//
//    @try {
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
//                                                                                       ]];//baseURL];
//        manager.responseSerializer = [AFJSONResponseSerializer serializer];
//        ////////// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
//
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1234",[GlobalStaticClass getPublicIp],gblclass.Udid, nil] forKeys:[NSArray arrayWithObjects:@"strSessionId",@"IP",@"Device_ID", nil]];
//
//        //NSLog(@"%@",dictparam);
//
//        [manager.requestSerializer setTimeoutInterval:time_out];
//        [manager POST:@"IsInstantPayAllow" parameters:dictparam progress:nil
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//                  // NSError *error;
//
//                  NSDictionary *dic2 = (NSDictionary *)responseObject;
//
//
//                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
//                  {
//
//                      gblclass.chk_qr_demo_login=@"1";
//                      gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
//                      gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
//
//                      gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
//
//
//                      [gblclass.arr_instant_qr removeAllObjects];
//
//                      NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
//
//                      if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
//                      {
//                          acct_name=@"N/A";
//                          [a addObject:acct_name];
//                      }
//                      else
//                      {
//                          [a addObject:acct_name];
//                      }
//
//                      NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
//
//                      if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
//                      {
//                          strAccountTitle=@"N/A";
//                          [a addObject:strAccountTitle];
//                      }
//                      else
//                      {
//                          [a addObject:strAccountTitle];
//                      }
//
//                      NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
//
//                      if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
//                      {
//                          strDefaultAccQRCode=@"N/A";
//                          [a addObject:strDefaultAccQRCode];
//                      }
//                      else
//                      {
//                          [a addObject:strDefaultAccQRCode];
//                      }
//
//                      NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
//
//                      if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
//                      {
//                          strDefaultAccountMask=@"N/A";
//                          [a addObject:strDefaultAccountMask];
//                      }
//                      else
//                      {
//                          [a addObject:strDefaultAccountMask];
//                      }
//
//
//                      NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
//                      [gblclass.arr_instant_qr addObject:bbb];
//
//                      bbb=@"";
//
//
//
//
//
//
//                      CATransition *transition = [CATransition animation];
//                      transition.duration = 0.3;
//                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                      transition.type = kCATransitionPush;
//                      transition.subtype = kCATransitionFromRight;
//                      [self.view.window.layer addAnimation:transition forKey:nil];
//
//                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_instant_qr"]; //   receive_qr_login login_instant_qr
//                      [self presentViewController:vc animated:NO completion:nil];
//
//
//                      [hud hideAnimated:YES];
//                  }
//                  else
//                  {
//                      [hud hideAnimated:YES];
//
//                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
//                      gblclass.custom_alert_img=@"0";
//
//                      //                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
//                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                      vc.view.alpha = alpha1;
//                      [self presentViewController:vc animated:NO completion:nil];
//                  }
//
//              }
//
//              failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  // [mine myfaildata];
//                  [hud hideAnimated:YES];
//
//                  gblclass.custom_alert_msg=@"Please try again later.";
//                  gblclass.custom_alert_img=@"0";
//
//
//                  CATransition *transition = [ CATransition animation ];
//                  transition.duration = 0.3;
//                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                  transition.type = kCATransitionPush;
//                  transition.subtype = kCATransitionFromRight;
//                  [ self.view.window. layer addAnimation:transition forKey:nil];
//
//
//                  //                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
//
//                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
//                  //
//                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//                  //
//                  //                  [alert addAction:ok];
//                  //                  [self presentViewController:alert animated:YES completion:nil];
//
//              }];
//
//    }
//    @catch (NSException *exception)
//    {
//        [hud hideAnimated:YES];
//
//        gblclass.custom_alert_msg=@"Please try again later.";
//        gblclass.custom_alert_img=@"0";
//
//        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
//        //
//        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//        //        [alert addAction:ok];
//        //        [self presentViewController:alert animated:YES completion:nil];
//
//    }
//
//
//}




//-(IBAction)btn_email_chck:(id)sender
//{
//
//    if ([btn_flag_email isEqualToString:@"0"])
//    {
//        btnImage = [UIImage imageNamed:@"check.png"];
//        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
//        btn_flag_email=@"1";
//        gblclass.chk_term_condition=@"1";
//        btn_agree.enabled=YES;

//
//    }
//    else
//    {
//        btnImage = [UIImage imageNamed:@"uncheck.png"];
//        [btn_check_mail setImage:btnImage forState:UIControlStateNormal];
//        btn_flag_email=@"0";
//        gblclass.chk_term_condition=@"0";
//        btn_agree.enabled=NO;
//
//    }
//
//}


//-(IBAction)btn_agree:(id)sender
//{
//    vw_term.hidden=YES;
//
//}


-(IBAction)btn_datt_cancel:(id)sender
{
    self.vw_datt.hidden=YES;
}

-(IBAction)btn_datt_done:(id)sender
{
    [self.datt_picker setMaximumDate:[NSDate date]];
    frm_dat = [[NSDateFormatter alloc] init];
    frm_date = self.datt_picker.date;
    [frm_dat setDateFormat:@"MM/dd/yyyy"];
    
    NSString *dateString = [frm_dat stringFromDate:frm_date];
    [self.dob setTitle:dateString forState:UIControlStateNormal];
    
    //    NSLog(@"%@",[NSString stringWithFormat:@"%@",dateString]);
    
    self.vw_datt.hidden=YES;
    
    
}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //        NSLog(@"%@",gblclass.chk_device_jb);
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic2 objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          
                          gblclass.instant_credit_chk=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"isCredit"]];
                          gblclass.instant_debit_chk=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"isDebit"]];
                          gblclass.M3Session_ID_instant=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"strRetSessionId"]];
                          gblclass.token_instant=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"Token"]];
                          gblclass.user_id=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"strUserId"]];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}


- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
}


@end

