//
//  Verification_account.m
//  ubltestbanking
//
//  Created by ammar on 04/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Verification_account.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "Globals.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "APIdleManager.h"
#import <Security/Security.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <PeekabooConnect/PeekabooConnect.h>
#import "Encrypt.h"
#import "Peekaboo.h"


@interface Verification_account ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIAlertController* alert;
    NSString* str_Otp;
    
    NSString* Pw_status_chck;
    UIStoryboard *storyboard;
    UIViewController *vc;
    //UIStoryboard *storyboard;
    
    NSNumber* aai;
    NSMutableArray  *a;
    NSMutableArray  *aa;
    NSString* str_pw;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    int txt_nam;
    APIdleManager* timer_class;
    
    NSString* user,*pass;
    NSString* chk_ssl;
    UIImage *btn_chck_Image;
    NSString* chk_termNCondition;
    NSString* device_jail;
    NSString* termNcondi_txt;
    NSString* t_n_c;
    NSString* debit_lock_chck;
    Encrypt* encrypt;
    NSString* device_jb_chk;
    NSString* ssl_count;
    NSUserDefaults *defaults;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@property (weak, nonatomic) IBOutlet UILabel *heading;
@property (weak, nonatomic) IBOutlet UIView *otpView;
@property (weak, nonatomic) IBOutlet UIButton *nxt_btn;

@end

@implementation Verification_account
static UIView* loadingView = nil;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    encrypt = [[Encrypt alloc] init];
    ssl_count = @"0";
    
    NSString *subStr;
    
    //    if ([gblclass.Mobile_No length]>0)
    //    {
    //        subStr = [gblclass.signup_mobile substringWithRange:NSMakeRange(9,3)];
    //    }
    
    
    //NSLog(@"%@",gblclass.exis_user_mobile_pin);
    
    
   // NSLog(@"%@",gblclass.device_chck);
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    t_n_c = [defaults valueForKey:@"t&c"];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     t_n_c = [defaults valueForKey:@"t&c"];
    
    NSUserDefaults *default_jb = [NSUserDefaults standardUserDefaults];
    device_jb_chk = [default_jb valueForKey:@"isDeviceJailBreaked"];
    
    debit_lock_chck = @"no";
    gblclass.is_debit_lock = @"0";
    
    if ([gblclass.str_processed_login_chk isEqualToString:@"0"])
    {
        
//        if ([device_jb_chk isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
//        {
//            device_jail = @"yes";
//            vw_JB.hidden = YES;
//            vw_tNc.hidden = NO;
//            btn_next.enabled = YES;
//            btn_resend_otp.enabled = YES;
//
//            btn_chck_Image = [UIImage imageNamed:@"chck_chck_term.png"];
//            [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
//        }
//        else if ([device_jb_chk isEqualToString:@"1"])
//        {
//            device_jail = @"yes";
//            vw_JB.hidden=YES;
//            vw_tNc.hidden=YES;
//            btn_next.enabled=NO;
//            btn_resend_otp.enabled=NO;
//
//            btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
//            [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
//        }
//        else if ([t_n_c isEqualToString:@"0"])
//        {
//            device_jail = @"no";
//            vw_JB.hidden=NO;
//            vw_tNc.hidden=YES;
//            btn_next.enabled=NO;
//            btn_resend_otp.enabled=NO;
//
//            btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
//            [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
//        }
//        else if ([t_n_c isEqualToString:@"1"])
//        {
//            device_jail = @"no";
//            vw_JB.hidden=YES;
//            vw_tNc.hidden=YES;
//            btn_next.enabled=YES;
//            btn_resend_otp.enabled=YES;
//
//            btn_chck_Image = [UIImage imageNamed:@"non_chck_term.png"];
//            [btn_check_JB setBackgroundImage:btn_chck_Image forState:UIControlStateNormal];
//
//        }
//        else
//        {
//            device_jail = @"no";
//
//            vw_JB.hidden=YES;
//            vw_tNc.hidden=YES;
//            btn_next.enabled=YES;
//            btn_resend_otp.enabled=YES;
//
//            //        btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
//            //        [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
//        }
        //
        
    }
    else
    {
//        device_jail = @"no";
//        
//        vw_JB.hidden=YES;
//        vw_tNc.hidden=YES;
//        btn_next.enabled=YES;
//        btn_resend_otp.enabled=YES;
    }
    
    if ([gblclass.From_Exis_login isEqualToString:@"1"])
    {
        subStr = [gblclass.exis_user_mobile_pin substringWithRange:NSMakeRange(8,4)];
    }
    else if([gblclass.Otp_mobile_verifi length]>0)
    {
        subStr = [gblclass.signup_mobile substringWithRange:NSMakeRange(8,4)];
    }
    
    
    //    if ( [gblclass.Otp_mobile_verifi length]>0)
    //    {
    //        subStr = [gblclass.signup_mobile substringWithRange:NSMakeRange(9,3)];
    //    }
    
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    //   arr_pin_pattern=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    //  arr_pw_pin=@[@"1",@"2",@"3",@"4",@"5",@"6"];
    
    
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    
    // Set textfield cursor color ::
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    lbl_text.text=[NSString stringWithFormat:@"Please enter the One Time Password (OTP) sent to your mobile number *******%@ via SMS. Please enter the sent code below.",subStr];
    
    
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    [self Play_bg_video];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
}


-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    // [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    _heading.frame = CGRectMake(self.view.frame.size.width + _heading.frame.size.width, _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
    _otpView.frame = CGRectMake(self.view.frame.size.width + _otpView.frame.size.width, _otpView.frame.origin.y, _otpView.frame.size.width, _otpView.frame.size.height);
    _nxt_btn.frame = CGRectMake(self.view.frame.size.width + _nxt_btn.frame.size.width, _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
    
    [UIView animateWithDuration:0.4 animations:^{
        
        _heading.frame = CGRectMake((self.view.frame.size.width/2) - (_heading.frame.size.width/2), _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        
        _otpView.frame = CGRectMake((self.view.frame.size.width/2) - (_otpView.frame.size.width/2), _otpView.frame.origin.y, _otpView.frame.size.width, _otpView.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        
        _nxt_btn.frame = CGRectMake((self.view.frame.size.width/2) - (_nxt_btn.frame.size.width/2), _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
        
    }];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) UserMobRegStep2:(NSString *)strIndustry{
    
    
    @try {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
        
        //NSLog(@"%@",gblclass.signup_relationshipid);
        //NSLog(@"%@",str_Otp);
        //NSLog(@"%@",gblclass.M3sessionid);
        
//        NSLog(@"%@",gblclass.user_id);
//        NSLog(@"%@",gblclass.Exis_relation_id);
//        NSLog(@"%@",gblclass.Udid);
//        NSLog(@"%@",gblclass.sign_up_token);
//        NSLog(@"%@",gblclass.From_Exis_login);
        
        
        NSDictionary *dictparam;
        if ([gblclass.From_Exis_login isEqualToString:@"1"])
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                          [encrypt encrypt_Data:gblclass.M3sessionid],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:@"abv"],
                          [encrypt encrypt_Data:gblclass.Exis_relation_id],
                          [encrypt encrypt_Data:str_Otp],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.sign_up_token], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"hCode",
                                                             @"strRelationShipId",
                                                             @"strActivationCode",
                                                             @"strDeviceID",
                                                             @"Token", nil]];
        }
        else
        {
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                          [encrypt encrypt_Data:@"1234"],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:@"abv"],
                          [encrypt encrypt_Data:gblclass.signup_relationshipid],
                          [encrypt encrypt_Data:str_Otp],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.sign_up_token], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"hCode",
                                                             @"strRelationShipId",
                                                             @"strActivationCode",
                                                             @"strDeviceID",
                                                             @"Token", nil]];
        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"UserMobRegStep2" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //            NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
                  //              gblclass.actsummaryarr =
                  // companytype = [(NSDictionary *)[dic objectForKey:@"Response"] objectForKey:@"Table1"];
                  //ttid=
                  
                  NSString* responsecode =[dic objectForKey:@"Response"];
                  
                  
                  if ([responsecode isEqualToString:@"-78"])
                  {
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      [self clearOTP];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                  }
                  
                  
                  if ([responsecode isEqualToString:@"-1"])
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      //[self custom_alert:@"Please ensure you have entered correct OTP" :@"0"];
                      
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  else  if ([responsecode isEqualToString:@"-2"])
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      CATransition *transition = [CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [ self.view.window. layer addAnimation:transition forKey:nil];
                      
                  }
                  else if([responsecode isEqualToString:@"0"])
                  {
                      [self.avplayer pause];
                      gblclass.signup_activationcode=str_Otp; //self.txtcode.text;
                      gblclass.signup_userid=[dic objectForKey:@"struserID"];
                      
                      
                      //********* 27 Oct New Method ***************
                      [self UserMobMpinReg:@""];
                      
                      //                  if([dic objectForKey:@"strLoginName"]==[NSNull null])
                      //                  {
                      //                      gblclass.signup_username=@"";
                      //                  }
                      //                  else
                      //                  {
                      //                      gblclass.signup_username=[dic objectForKey:@"strLoginName"];
                      //                  }
                      //
                      
                      
                      
                      //                   if ([gblclass.From_Exis_login isEqualToString:@"1"])
                      //                   {
                      //                       [hud hideAnimated:YES];
                      //
                      //                       UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                       UIViewController* vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"touch_id"]; //question
                      //                        [self presentViewController:vc animated:NO completion:nil];
                      //
                      //                       CATransition *transition = [ CATransition animation ];
                      //                       transition.duration = 0.3;
                      //                       transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      //                       transition.type = kCATransitionPush;
                      //                       transition.subtype = kCATransitionFromRight;
                      //                       [ self.view.window. layer addAnimation:transition forKey:nil];
                      //                   }
                      //                   else
                      //                   {
                      //                       [hud hideAnimated:YES];
                      //
                      //                       UIStoryboard* mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                       UIViewController* vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
                      //                        [self presentViewController:vc animated:NO completion:nil];
                      //
                      //                       CATransition *transition = [ CATransition animation ];
                      //                       transition.duration = 0.3;
                      //                       transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      //                       transition.type = kCATransitionPush;
                      //                       transition.subtype = kCATransitionFromRight;
                      //                       [ self.view.window. layer addAnimation:transition forKey:nil];
                      //
                      //                   }
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      [self.connection cancel];
                      [self clearOTP];
                      [txt_1 becomeFirstResponder];
                      
                      
                      //  NSString* responsemsg =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      //  [self showAlert:responsemsg :@"Attention"];
                      
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                      
                  }
                  
                  
                  //    [hud hideAnimated:YES];
                  
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  
                  // [mine myfaildata];
                  //NSLog(@"failed load branch");
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        //NSLog(@"xompanytype cpount %lu",[companytype count]);
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) UserMobMpinReg:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam;
    
    dictparam = [NSDictionary dictionaryWithObjects:
                 [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                  [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                  [encrypt encrypt_Data:@"abv"],
                  [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",gblclass.signup_userid]],
                  [encrypt encrypt_Data:gblclass.Exis_relation_id],
                  [encrypt encrypt_Data:gblclass.Udid],
                  [encrypt encrypt_Data:@"0"],
                  [encrypt encrypt_Data:gblclass.sign_up_token],
                  [encrypt encrypt_Data:@"SignUp"],
                  [encrypt encrypt_Data:@"1234"],
                  [encrypt encrypt_Data:gblclass.device_chck],
                  [encrypt encrypt_Data:gblclass.chk_termNCondition], nil]
                 
                                            forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                     @"IP",
                                                     @"hCode",
                                                     @"strUserId",
                                                     @"StrRelationShipId",
                                                     @"StrDeviceId",
                                                     @"IsTouchLoign",
                                                     @"Token",
                                                     @"calling_source",
                                                     @"strLogBrowserInfo",
                                                     @"isRooted",
                                                     @"isTnCAccepted", nil]];
    
    
    //NSLog(@"%@",dictparam);
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"UserMobMpinRegRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              NSString* responsecode = [NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
              
              if([responsecode isEqualToString:@"1"]) //1 krna hia live pe
              {
              //  [self showAlert:@"Thank you for joining UBL DIGITAL, Welcome to UBL." :@"Attention"];
                  
                  [self mob_Sign_In:@""];
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  NSString* responsemsg =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  [self showAlert:responsemsg :@"Attention"];
              }
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              [self clearOTP];
              alert = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}




-(void) Get_MobUser_Login_CreateDate:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"1"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"ChannelId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetMobUserLoginCreateDate" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSString* pekaboo = [defaults valueForKey:@"peekabo_kfc"];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      if (pekaboo.length == 0)
                      {
                          NSString* msg = [dic2 objectForKey:@"strEncryptUserId"];
                          [[NSUserDefaults standardUserDefaults] setObject:msg forKey:@"peekabo_kfc"];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          gblclass.peekabo_kfc_userid = msg;
                      }
                  }
                  
 
                  [self segue1];
                  
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
//                  [hud hideAnimated:YES];
//                  [self custom_alert:@"Please try again later." :@"0"];

                   [self segue1];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Please try again later." :@"0"];

         [self segue1];
    }
    
}



-(void) mob_Sign_In:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];   //baseURL];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    gblclass.arr_transfer_within_acct_deposit_chck = [[NSMutableArray alloc] init];
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    gblclass.arr_transfer_within_acct_to=[[NSMutableArray alloc] init];
    
    
    //NSLog(@"%@",gblclass.user_login_name);
    
    
    NSDictionary *dictparam;
    
    
    //   NSString *msg = [[NSUserDefaults standardUserDefaults]
    //                    stringForKey:@"Mpin-pw"];
    
    
//    NSLog(@"%@",gblclass.signup_login_name);
//     NSLog(@"%@",gblclass.Udid);
    
   // t_n_c = @"0";
    
    if ([t_n_c isEqualToString:@"2"])
    {
        t_n_c = @"1";
    }
    
//    NSLog(@"%@",gblclass.setting_ver);
    
    dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.signup_login_name],[encrypt encrypt_Data:gblclass.signup_pw],[encrypt encrypt_Data:gblclass.Udid],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:gblclass.setting_ver],[encrypt encrypt_Data:@"0"],[encrypt encrypt_Data:gblclass.device_chck],[encrypt encrypt_Data:t_n_c], nil] forKeys:[NSArray arrayWithObjects:@"LoginName",@"strPassword",@"strDeviceID",@"strSessionId",@"IP",@"hCode",@"isTouchLogin",@"isRooted",@"isTnCAccepted", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:@"mobSignInRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              @try {
                  
                  //NSError *error;
                  //NSArray *arr = (NSArray *)responseObject;
                  NSDictionary *dic = (NSDictionary *) [encrypt de_crypt_Data:responseObject];
                  NSDictionary *dic1 = dic;
                  
                  a=[[NSMutableArray alloc] init];
                  NSMutableArray*  aa = [[NSMutableArray alloc] init];
                  //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  [self removeAllCredentialsForServer:@"finger_print"];
                  
                  //for finger_print enable/disable ::
                  [self finger_print_enable:@"0" forServer:@"finger_print"];
                  
                  
                  
                  //                  gblclass.token=[dic objectForKey:@"Token"];
                  //                  Pw_status_chck=[dic objectForKey:@"Response"];
                  //                  gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  //                  gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  //                  gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                  //                  gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  //                  gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  //                  gblclass.user_id=[dic objectForKey:@"strUserId"];
                  //                  gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                  //                  gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  //                  gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                  
                  //NSLog(@"%@",Pw_status_chck);
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==-2)
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      return;
                  }
                  else  if ([[dic objectForKey:@"Response"] integerValue] == 0)
                  {
                      gblclass.token=[dic objectForKey:@"Token"];
                      Pw_status_chck=[dic objectForKey:@"Response"];
                      gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                      gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                      gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                      gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                      gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                      gblclass.user_id=[dic objectForKey:@"strUserId"];
                      gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                      gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                      gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                      
                      
                      
                      //                      // Remove Credential from Keychain ::
                      //                      [self removeAllCredentialsForServer:@"enable_touch"];
                      //
                      //                      // Save Credential from Keychain ::
                      //                      [self saveUsername:gblclass.sign_in_username withPassword:gblclass.sign_in_pw forServer:@"enable_touch"];
                      
                      
                      //gblclass.actsummaryarr =
                      NSArray* array=    [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                      
                      gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                      gblclass.relation_id=[[array objectAtIndex:0]objectForKey:@"RelationshipID"];
                      gblclass.franchise_btn_chck=[[array objectAtIndex:0]objectForKey:@"isFA"];
                      
//                      NSLog(@"%@",gblclass.user_id);
                      
                      gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                      //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                      gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                      
                      NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      if(mobileno==(NSString *)[NSNull null])
                      {
                          gblclass.Mobile_No =@"";
                      }
                      else
                      {
                          gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      }
                      
                      
                      gblclass.arr_user_pw_table2 =
                      [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                      
                      gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                      
                      
                      NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                      
                      if (Email ==(NSString *)[NSNull null])
                      {
                          Email=@"N/A";
                          [a addObject:Email];
                      }
                      else
                      {
                          // [a addObject:Email];
                          gblclass.user_email=Email;
                      }
                      
                      NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                      
                      if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                      {
                          FundsTransferEnabled=@"N/A";
                          [a addObject:FundsTransferEnabled];
                      }
                      else
                      {
                          // [a addObject:FundsTransferEnabled];
                      }
                      
                      NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                      
                      if (Mobile_No == (NSString *)[NSNull null])
                      {
                          Mobile_No=@"N/A";
                          [a addObject:Mobile_No];
                      }
                      else
                      {
                          // [a addObject:Mobile_No];
                      }
                      
                      //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //              [a removeAllObjects];
                      //              bbb=@"";
                      
                      
                      //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                      
                      gblclass.actsummaryarr =
                      [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                      
                  }
                  //NSLog(@"%@",gblclass.actsummaryarr);
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"change_pw"];
                      [self presentViewController:alert animated:YES completion:nil];
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
                  {
                      [self clearOTP];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                      [self presentViewController:alert animated:YES completion:nil];
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]== 0  || [[dic objectForKey:@"Response"] integerValue]==101)
                  {
                      [self clearOTP];
                      [a removeAllObjects];
                      
                      //NSLog(@"%@",gblclass.actsummaryarr);
                      
                      
                      //                      NSUInteger *set;
                      //                      for (dic in gblclass.actsummaryarr)
                      //                      {
                      //
                      //                          NSString* acct_name=[dic objectForKey:@"account_name"];
                      //
                      //                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                      //                          {
                      //                              acct_name=@"N/A";
                      //                              [a addObject:acct_name];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_name];
                      //                          }
                      //
                      //
                      //                          NSString* acct_no=[dic objectForKey:@"account_no"];
                      //                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                      //                          {
                      //                              acct_no=@"N/A";
                      //                              [a addObject:acct_no];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_no];
                      //                          }
                      //
                      //
                      //                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                      //                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                      //                          {
                      //                              acct_id=@"N/A";
                      //                              [a addObject:acct_id];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_id];
                      //                          }
                      //
                      //                          NSString* is_default=[dic objectForKey:@"is_default"];
                      //                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                      //                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                      //                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                      //                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                      //
                      //                          if ([is_default isEqualToString:@"1"])
                      //                          {
                      //
                      //                              gblclass.is_default_acct_id=acct_id;
                      //                              gblclass.is_default_acct_id_name=is_default_name;
                      //                              gblclass.is_default_acct_no=is_default_acct_no;
                      //                              gblclass.is_default_m3_balc=is_default_balc;
                      //                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                      //                          }
                      //
                      //                          //privileges
                      //                          NSString* privileges=[dic objectForKey:@"privileges"];
                      //                          NSString* pri=[dic objectForKey:@"privileges"];
                      //                          privileges=[privileges substringWithRange:NSMakeRange(2,1)]; //Second bit 1 for withDraw ..
                      //
                      //
                      //                          if ([privileges isEqualToString:@"0"])
                      //                          {
                      //                              privileges=@"0";
                      //                              [a addObject:privileges];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:privileges];
                      //                          }
                      //
                      //                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                      //                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                      //                          {
                      //                              account_type_desc=@"N/A";
                      //                              [a addObject:account_type_desc];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:account_type_desc];
                      //                          }
                      //                          //account_type
                      //
                      //                          NSString* account_type=[dic objectForKey:@"account_type"];
                      //                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                      //                          {
                      //                              account_type=@"N/A";
                      //                              [a addObject:account_type];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:account_type];
                      //                          }
                      //
                      //                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                      //
                      //                          //((NSString *)[NSNull null] == m3_balc)
                      //                          //[m3_balc isEqualToString:@" "] ||
                      //
                      //
                      //                          //27 OCt New Sign IN Method :::
                      //
                      //                            if (m3_balc == [NSNull null])
                      //                            {
                      //                                m3_balc=@"0";
                      //                                [a addObject:m3_balc];
                      //                            }
                      //                            else
                      //                            {
                      //                                [a addObject:m3_balc];
                      //                            }
                      //
                      //
                      //
                      //
                      //                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                      //                          if ( branch_name == [NSNull null])
                      //                          {
                      //                              branch_name=@"N/A";
                      //                              [a addObject:branch_name];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:branch_name];
                      //                          }
                      //
                      //                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                      //                          if ( available_balance == [NSNull null])
                      //                          {
                      //                              available_balance=@"N/A";
                      //                              [a addObject:available_balance];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:available_balance];
                      //                          }
                      //
                      //
                      //
                      //                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      //                          [gblclass.arr_act_statmnt_name addObject:bbb];
                      //
                      //                          if ([privileges isEqualToString:@"1"])
                      //                          {
                      //                              [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //                          }
                      //
                      //                          //                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //
                      //
                      //
                      //
                      //                          //NSLog(@"%@", bbb);
                      //                          [a removeAllObjects];
                      //                          [aa removeAllObjects];
                      //
                      //                      }
                      //
                      //                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      //
                      //
                      //                      [hud hideAnimated:YES];
                      //                      [self segue1];
                      
                      
                      //                    NSUInteger *set;
                      for (dic in gblclass.actsummaryarr)
                      {
                          
                          NSString* acct_name=[dic objectForKey:@"account_name"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          
                          NSString* acct_no=[dic objectForKey:@"account_no"];
                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                          {
                              acct_no=@"N/A";
                              [a addObject:acct_no];
                          }
                          else
                          {
                              [a addObject:acct_no];
                          }
                          
                          
                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                          {
                              acct_id=@"N/A";
                              [a addObject:acct_id];
                          }
                          else
                          {
                              [a addObject:acct_id];
                          }
                          
                          NSString* is_default=[dic objectForKey:@"is_default"];
                          NSString* is_default_curr=[dic objectForKey:@"ccy"];
                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                          NSString* is_account_type=[dic objectForKey:@"account_type"];
                          NSString* is_br_code=[dic objectForKey:@"br_code"];
                          
                          
                          //privileges
                          NSString* privileges=[dic objectForKey:@"privileges"];
                          //777         NSString* pri=[dic objectForKey:@"privileges"];
                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //(1,1) //Second bit 1 for withDraw ..
                          
                          
                          if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"1"])
                          {
                              
                              
                              gblclass.is_default_acct_id=acct_id;
                              gblclass.is_default_acct_id_name=is_default_name;
                              gblclass.is_default_acct_no=is_default_acct_no;
                              gblclass.is_default_m3_balc=is_default_balc;
                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                              gblclass.is_default_currency=is_default_curr;
                              gblclass.is_default_acct_type=is_account_type;
                              gblclass.is_default_br_code=is_br_code;
                              
                              
//                              NSLog(@"%@",acct_id);
//                              NSLog(@"%@",is_default_name);
//                              NSLog(@"%@",is_default_acct_no);
//                              NSLog(@"%@",is_default_balc);
//                              NSLog(@"%@",is_default_regstd_acct_id);
//                              NSLog(@"%@",is_default_curr);
//                              NSLog(@"%@",is_account_type);
//                              NSLog(@"%@",is_br_code);
                              
                          }
                          else if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"0"])
                          {
                              debit_lock_chck = @"yes";
                              gblclass.is_debit_lock = @"1";
                              
                              gblclass.is_default_acct_id = acct_id;
                              gblclass.is_default_acct_id_name = is_default_name;
                              gblclass.is_default_acct_no = is_default_acct_no;
                              gblclass.is_default_m3_balc = is_default_balc;
                              gblclass.registd_acct_id_trans = is_default_regstd_acct_id;
                              gblclass.is_default_currency = is_default_curr;
                              gblclass.is_default_acct_type = is_account_type;
                              gblclass.is_default_br_code = is_br_code;
                          }
                          
                          
                          //                          //privileges
                          //                          NSString* privileges=[dic objectForKey:@"privileges"];
                          //                          NSString* pri=[dic objectForKey:@"privileges"];
                          //                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //Second bit 1 for withDraw ..
                          
                          
                          if ([privileges isEqualToString:@"0"])
                          {
                              privileges=@"0";
                              [a addObject:privileges];
                          }
                          else
                          {
                              [a addObject:privileges];
                          }
                          
                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                          {
                              account_type_desc=@"N/A";
                              [a addObject:account_type_desc];
                          }
                          else
                          {
                              [a addObject:account_type_desc];
                          }
                          //account_type
                          
                          NSString* account_type=[dic objectForKey:@"account_type"];
                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                          {
                              account_type=@"N/A";
                              [a addObject:account_type];
                          }
                          else
                          {
                              [a addObject:account_type];
                          }
                          
                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                          
                          
                          
                          //((NSString *)[NSNull null] == m3_balc)
                          //[m3_balc isEqualToString:@" "] ||
                          
                          
                          //27 OCt New Sign IN Method :::
                          
                          if (m3_balc == (NSString *)[NSNull null])
                          {
                              m3_balc=@"0";
                              [a addObject:m3_balc];
                          }
                          else
                          {
                              [a addObject:m3_balc];
                          }
                          
                          
                          
                          
                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                          if ( branch_name == (NSString *)[NSNull null])
                          {
                              branch_name=@"N/A";
                              [a addObject:branch_name];
                          }
                          else
                          {
                              [a addObject:branch_name];
                          }
                          
                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                          if ( available_balance == (NSString *)[NSNull null])
                          {
                              available_balance=@"0";
                              [a addObject:available_balance];
                          }
                          else
                          {
                              [a addObject:available_balance];
                          }
                          
                          NSString* br_code=[dic objectForKey:@"br_code"];
                          if ( br_code == (NSString *)[NSNull null])
                          {
                              br_code=@"0";
                              [a addObject:br_code];
                          }
                          else
                          {
                              [a addObject:br_code];
                          }
                          
                          NSString* ccy=[dic objectForKey:@"ccy"];
                          if ( ccy == (NSString *)[NSNull null])
                          {
                              ccy=@"0";
                              [a addObject:ccy];
                          }
                          else
                          {
                              [a addObject:ccy];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_act_statmnt_name addObject:bbb];
                          
                          if ([privileges isEqualToString:@"1"])
                          {
                              if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                              {
                                  
                                  [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                                  
                              }
                          }
                          
                          
                          if ([account_type_desc isEqualToString:@"Deposit Account"])
                          {
                              [gblclass.arr_transfer_within_acct_deposit_chck addObject:bbb];
                          }
                          
                          NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
                          privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
                          
                          
                          if ([privileges_to isEqualToString:@"1"])
                          {
                              [gblclass.arr_transfer_within_acct_to addObject:bbb];
                          }
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                          [aa removeAllObjects];
                          
                      }
                      
                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      
                      
                      if ([debit_lock_chck isEqualToString:@"yes"])
                      {
                          
//                          NSLog(@"%lu", (unsigned long)[gblclass.arr_transfer_within_acct count]);
//                          NSLog(@"%@",gblclass.arr_transfer_within_acct);
                          
                          if ([gblclass.arr_transfer_within_acct count]>0)
                          {
                          
                          NSArray*  split_debit = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
                          
                         // NSLog(@"%@",[split_debit objectAtIndex:0]);
                          
                          gblclass.is_default_acct_id = [split_debit objectAtIndex:2];
                          gblclass.is_default_acct_id_name = [split_debit objectAtIndex:0];
                          gblclass.is_default_acct_no = [split_debit objectAtIndex:1];
                          gblclass.is_default_m3_balc = [split_debit objectAtIndex:6];
                          gblclass.registd_acct_id_trans = [split_debit objectAtIndex:2];
                          gblclass.is_default_currency = [split_debit objectAtIndex:10];
                          gblclass.is_default_acct_type = [split_debit objectAtIndex:5];
                          gblclass.is_default_br_code = [split_debit objectAtIndex:9];
                          
//                          NSLog(@"%@",gblclass.is_default_acct_id);
//                          NSLog(@"%@",gblclass.is_default_acct_id_name);
//                          NSLog(@"%@",gblclass.is_default_acct_no);
//                          NSLog(@"%@",gblclass.is_default_m3_balc);
//                          NSLog(@"%@",gblclass.registd_acct_id_trans);
//                          NSLog(@"%@",gblclass.is_default_currency);
//                          NSLog(@"%@",gblclass.is_default_acct_type);
//                          NSLog(@"%@",gblclass.is_default_br_code);
                              
                          }
                          
                      }
                      
                      [self Get_MobUser_Login_CreateDate:@""];
                      [hud hideAnimated:YES];
                     // [self segue1];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      if ( [[dic objectForKey:@"Response"] isEqualToString:@"0"])
                      {
                          //NSLog(@" response 0");
                      }
                      else
                      {
                          //NSLog(@" response 1");
                      }
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Retry" preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
              }
              @catch (NSException *exception)
              {
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",exception.description);
                  //NSLog(@"%@",exception);
                  
                  //NSLog(@"%@",[exception debugDescription]);
                  
                  //exception.reason
                                    
                  txt_1.text=@"";
                  txt_2.text=@"";
                  txt_3.text=@"";
                  txt_4.text=@"";
                  txt_5.text=@"";
                  txt_6.text=@"";
                  
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
                  
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              //NSLog(@"%@",[error localizedDescription]);
              //NSLog(@"%@",[error localizedFailureReason]);
              //NSLog(@"%@",[error localizedRecoverySuggestion]);
              //NSLog(@"%@",task);
              
              txt_1.text=@"";
              txt_2.text=@"";
              txt_3.text=@"";
              txt_4.text=@"";
              txt_5.text=@"";
              txt_6.text=@"";
              
              [self custom_alert:@"Unable to Signup, Please try again later." :@"0"];
              
          }];
}


-(void)segue1
{
    
    //NSLog(@"%@",gblclass.ssl_pin_check);
    
    if ([Pw_status_chck isEqualToString:@"101"] || [Pw_status_chck isEqualToString:@"0"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        
        
        //first_time
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"enable_touch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        //[self removeAllCredentialsForServer:@"touch_enable"];
        
        //NSLog(@"%@",gblclass.signup_username);
        //NSLog(@"%@",gblclass.signup_pw);
        
        
        [self with_touch_enable:@"1" forServer:@"touch_enable"];
        [self getCredentialsForServer:@"touch_enable"];
        
        
        
        //     [self saveUsername:gblclass.signup_username withPassword:gblclass.signup_pw forServer:@"touch_enable"];
        
        
        NSString *msg = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"enable_touch"]; // first_time
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];  //act_summary pie_chart
        [self presentViewController:vc animated:NO completion:nil];
        [self slide_right];
        
    }
    
}



-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}





- (IBAction)btn_submit:(UIButton *)sender
{
    //    if([self.txtcode.text length] ==0)
    //    {
    //        [self showAlert:@"Please enter verifcation code recived on email" :@"Attention"];
    //    }
    //    else
    //    {
    
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
    
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter All Pin" preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
        [self custom_alert:@"please enter 6 digits OTP that has been sent to your mobile number" :@"0"];
        
        return;
    }
    
    
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
    
    //    if ([gblclass.From_Mpin_login isEqualToString:@"1"])
    //    {
    //
    //    }
    
    //   [self UserMobRegStep2:@""];
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
    // }
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ( [chk_ssl isEqualToString:@"logout"])
    {
        
        [self slide_right];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        
        [self presentViewController:vc animated:NO completion:nil];
    }
}




- (IBAction)btn_backbtn:(UIButton *)sender
{
    //    CATransition *transition = [ CATransition animation ];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromLeft;
    //    [ self.view.window. layer addAnimation:transition forKey:nil];
    //
    //    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    [UIView animateWithDuration:0.4 animations:^{
        
        _heading.frame = CGRectMake((self.view.frame.origin.x + _heading.frame.size.width), _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        
        _otpView.frame = CGRectMake((self.view.frame.origin.x + _otpView.frame.size.width), _otpView.frame.origin.y, _otpView.frame.size.width, _otpView.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        
        _nxt_btn.frame = CGRectMake(self.view.frame.size.width + _nxt_btn.frame.size.width, _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
        
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:0.4
                                     target:self
                                   selector:@selector(dissmiss)
                                   userInfo:nil
                                    repeats:NO];
    
}

-(void)dissmiss {
    
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    [self slide_left];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self presentViewController:vc animated:NO completion:nil];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSUInteger MAX_DIGITS = 1;
    
    NSUInteger maxLength = 1;
     
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text length :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    //NSLog(@"Responder :: %@",str);
//
//
//    NSString *str1 = @"hello ";
//    str1 = [str1 stringByAppendingString:str];
    
//    NSLog(@"%ld",(long)textField.tag);
//    NSLog(@"%@",textField.text);
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
//        NSLog(@"**** ALERT ***** FIRST");
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            return 0;
        }
        
       // NSLog(@"**** ALERT ***** LENGTH");
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j = 0;
            int i;
            
          //  NSLog(@"**** ALERT ***** LOOP START");
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j = [[arr_pw_pin objectAtIndex:i] intValue];
                
                if (j>textField.tag)
                {
                   // NSLog(@"**** ALERT ***** LOOP BREAK");
                    j=i;
                    break;
                }
            }
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] intValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"**** ALERT ***** RESPONDER");
            //saki 3 Nov 2020
//            if (nextResponder)
//            {
//                NSLog(@"%ld",(long)textField.tag);
//                NSLog(@"%@",textField.text);
//                NSLog(@"%@",textField);
////                textField = nil;
//                NSLog(@"%ld",(long)textField.tag);
//                NSLog(@"%@",textField);
//                NSLog(@"%@",textField.text);
//                [textField resignFirstResponder];
//                [nextResponder becomeFirstResponder];
////              textField.keyboardType = UIKeyboardTypeNumberPad;
//            }
            
             btn_next.enabled=YES;
            if ([gblclass.str_processed_login_chk isEqualToString:@"0"])
            {
                
                if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
                {
                    btn_next.enabled=YES;
                }
                else if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
                {
                    [hud hideAnimated:YES];
                    
                    txt_1.text=@"";
                    txt_2.text=@"";
                    txt_3.text=@"";
                    txt_4.text=@"";
                    txt_5.text=@"";
                    txt_6.text=@"";
                    
                    [txt_1 becomeFirstResponder];
                    
                  //  [self custom_alert:@"Kindly accept Terms & Conditions" :@"0"];
                  //  return 0;
                }
                else if ([t_n_c isEqualToString:@"1"])
                {
                    btn_next.enabled=YES;
                }
            }
            
            if (textField.tag == 6)
            {
               // NSLog(@"**** ALERT ***** SIX");
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                [hud showAnimated:YES];
                [hud hideAnimated:YES afterDelay:130];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
               // NSLog(@"**** ALERT ***** SERVICE CALL");
                [self SSL_Call];
                
                return 0;
            }
            
            textField = nil;
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
      //txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam = [textField tag];
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
          
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
//        return YES;
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    int i = 0;
    
    
    for (i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] intValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)btn_back:(id)sender
{
    gblclass.user_login_name=@"";
    
    [self slide_left];
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    
    
  //  [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}


-(void) MpinSignIn:(NSString *)strIndustry
{
    
    //  allCompany = [[NSMutableArray alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    gblclass.arr_transfer_within_acct_deposit_chck = [[NSMutableArray alloc] init];
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    
    //NSLog(@"%@",gblclass.user_login_name);
    
    //gblclass.user_id,gblclass.user_login_name,password,gblclass.login_session,gblclass.M3sessionid
    
    //password
    NSDictionary *dictparam;
    
    if ([gblclass.touch_id_enable isEqualToString:@"1"])
    {
        // gblclass.touch_id_Mpin
        
        NSString *Pw_Mpin = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"Mpin-pw"];
        
        
        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.Udid,Pw_Mpin,@"1234",[GlobalStaticClass getPublicIp],@"abcd", nil] forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
    }
    else
    {
        
        dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.Udid,gblclass.Mpin_login,@"1234",[GlobalStaticClass getPublicIp],@"abcd", nil] forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
        
    }
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"mpinSignIn" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              @try {
                  
                  //                  NSError *error;
                  //                  NSArray *arr = (NSArray *)responseObject;
                  NSDictionary *dic = (NSDictionary *)responseObject;
                  NSDictionary *dic1 = (NSDictionary *)responseObject;
                  
                  aa=[[NSMutableArray alloc] init];
                  //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  
                  Pw_status_chck=[dic objectForKey:@"Response"];
                  gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  gblclass.user_id=[dic objectForKey:@"strUserId"];
                  gblclass.M3sessionid=@"1234";
                  gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  
                  
                  //gblclass.actsummaryarr =
                  NSArray* array=    [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                  //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                  gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                  
                  NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  if(mobileno==(NSString *)[NSNull null])
                  {
                      gblclass.Mobile_No =@"";
                  }
                  else
                  {
                      gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  }
                  
                  
                  gblclass.arr_user_pw_table2 =
                  [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  
                  gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                  
                  
                  NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                  
                  if (Email == (NSString *)[NSNull null])
                  {
                      Email=@"N/A";
                      [a addObject:Email];
                  }
                  else
                  {
                      // [a addObject:Email];
                      gblclass.user_email=Email;
                  }
                  
                  NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                  
                  if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                  {
                      FundsTransferEnabled=@"N/A";
                      [a addObject:FundsTransferEnabled];
                  }
                  else
                  {
                      // [a addObject:FundsTransferEnabled];
                  }
                  
                  NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                  
                  if (Mobile_No == (NSString *)[NSNull null])
                  {
                      Mobile_No=@"N/A";
                      [a addObject:Mobile_No];
                  }
                  else
                  {
                      // [a addObject:Mobile_No];
                  }
                  
                  //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                  //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //              [a removeAllObjects];
                  //              bbb=@"";
                  
                  
                  //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                  
                  gblclass.actsummaryarr =
                  [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
                  {
                      
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"change_pw"];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
                  {
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
                  {
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==101)
                  {
                      [a removeAllObjects];
                      
                      //NSLog(@"%@",gblclass.actsummaryarr);
                      
                      
                      //                  NSUInteger *set;
                      for (dic in gblclass.actsummaryarr)
                      {
                          
                          NSString* acct_name=[dic objectForKey:@"account_name"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          
                          NSString* acct_no=[dic objectForKey:@"account_no"];
                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                          {
                              acct_no=@"N/A";
                              [a addObject:acct_no];
                          }
                          else
                          {
                              [a addObject:acct_no];
                          }
                          
                          
                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                          {
                              acct_id=@"N/A";
                              [a addObject:acct_id];
                          }
                          else
                          {
                              [a addObject:acct_id];
                          }
                          
                          NSString* is_default=[dic objectForKey:@"is_default"];
                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                          if ([is_default isEqualToString:@"1"])
                          {
                              gblclass.is_default_acct_id=acct_id;
                              gblclass.is_default_acct_id_name=is_default_name;
                              gblclass.is_default_acct_no=is_default_acct_no;
                          }
                          
                          //privileges
                          NSString* privileges=[dic objectForKey:@"privileges"];
                          NSString* pri=[dic objectForKey:@"privileges"];
                          privileges=[privileges substringWithRange:NSMakeRange(2,1)]; //Second bit 1 for withDraw ..
                          
                          
                          if ([privileges isEqualToString:@"0"])
                          {
                              privileges=@"0";
                              [a addObject:privileges];
                          }
                          else
                          {
                              [a addObject:privileges];
                          }
                          
                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                          {
                              account_type_desc=@"N/A";
                              [a addObject:account_type_desc];
                          }
                          else
                          {
                              [a addObject:account_type_desc];
                          }
                          //account_type
                          
                          NSString* account_type=[dic objectForKey:@"account_type"];
                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                          {
                              account_type=@"N/A";
                              [a addObject:account_type];
                          }
                          else
                          {
                              [a addObject:account_type];
                          }
                          
                          NSString* m3_balc=[dic objectForKey:@"balance"];
                          if (m3_balc.length==0 || [m3_balc isEqualToString:@" "])
                          {
                              m3_balc=@"N/A";
                              [a addObject:m3_balc];
                          }
                          else
                          {
                              [a addObject:m3_balc];
                          }
                          
                          
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_act_statmnt_name addObject:bbb];
                          
                          if ([privileges isEqualToString:@"1"])
                          {
                              [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                          }
                          
                          //                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                          
                          
                          
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                          [aa removeAllObjects];
                          
                      }
                      
                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      
                      
                      [hud hideAnimated:YES];
                      [self segue];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
              }
              @catch (NSException *exception)
              {
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",exception.description);
                  //NSLog(@"%@",exception);
                  
                  
                  alert  = [UIAlertController alertControllerWithTitle:@"Error" message:exception.reason preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",[error localizedDescription]);
              //NSLog(@"%@",[error localizedFailureReason]);
              //NSLog(@"%@",[error localizedRecoverySuggestion]);
              //NSLog(@"%@",task);
              
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
}

-(void)segue
{
    if ([Pw_status_chck isEqualToString:@"101"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        
        
        
        
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        vc = [storyboard instantiateViewControllerWithIdentifier:@"landing"];  //act_summary pie_chart
        [self presentViewController:vc animated:NO completion:nil];
    }
}


-(IBAction)btn_feature:(id)sender
{
    
    [self custom_alert:@"Please register your User." :@"0"];
    
    //    [self slide_right];
    //
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"feature"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


-(IBAction)btn_offer:(id)sender
{
      [self custom_alert:Offer_msg :@""];
//    [self peekabooSDK:@"deals"];
//      [self peekabooSDK:@{@"type": @"deals",}];
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.forget_pw_btn_click=@"0";
    gblclass.forgot_pw_click = @"1";
    
//    [self peekabooSDK:@"locator"];
//      [self peekabooSDK:@{@"type": @"locator",}];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//
//
////  new_signup  debit_card_optn
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


//- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
//    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
//    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
//    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"MyLocalCertificate" ofType:@"cer"];
//    NSData *localCertData = [NSData dataWithContentsOfFile:cerPath];
//    if ([remoteCertificateData isEqualToData:localCertData]) {
//        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
//        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
//    }
//    else
//    {
//        [[challenge sender] cancelAuthenticationChallenge:challenge];
//    }
//}
//
//- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
//{
//    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
//    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
//    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
//    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"myCertName" ofType:@"cer"];
//    NSData *localCertData = [NSData dataWithContentsOfFile:cerPath];
//
//    if ([remoteCertificateData isEqualToData:localCertData])
//    {
//        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
//        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
//        completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
//    }
//    else
//    {
//        [[challenge sender] cancelAuthenticationChallenge:challenge];
//        completionHandler(NSURLSessionAuthChallengeRejectProtectionSpace, nil);
//    }
//}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        [self UserMobRegStep2:@""];
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self clearOTP];
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    
    
    [self.connection cancel];
    
    [self UserMobRegStep2:@""];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
   // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
   // NSLog(@"%@",gblclass.SSL_name);
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}



- (BOOL)isSSLPinning
{
    
  //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}

//- (BOOL)isSSLPinning
//{
//    NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
//    return [envValue boolValue];
//}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(IBAction)btn_resend_OTP:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [self Existing_UserLogin_Resend_OTP:@""];
}



-(void) Existing_UserLogin_Resend_OTP:(NSString *)strIndustry
{
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    UIDevice *deviceInfo = [UIDevice currentDevice];
    
    //NSLog(@"Device name:  %@", deviceInfo.name);
    
    
//    NSLog(@"%@",device_jail);
//    NSLog(@"%@",t_n_c);
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"asdf"],
                                [encrypt encrypt_Data:@"abcd"],
                                [encrypt encrypt_Data:gblclass.signup_login_name],
                                [encrypt encrypt_Data:gblclass.signup_pw],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.device_chck],
                                [encrypt encrypt_Data:t_n_c],nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                   @"IP",
                                                                   @"LogBrowserInfo",
                                                                   @"hCode",
                                                                   @"strLoginName",
                                                                   @"strPassword",
                                                                   @"strDeviceID",
                                                                   @"isRooted",
                                                                   @"isTnCAccepted",nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"ExistingUserMobRegRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSString* login_status_chck;
              
              gblclass.token=[dic objectForKey:@"Token"];
              login_status_chck=[dic objectForKey:@"Response"];
              
              if ([[dic objectForKey:@"Response"] integerValue]==0 || [[dic objectForKey:@"Response"] integerValue]==-96)
              {
                  gblclass.From_Exis_login=@"1";
                  gblclass.exis_user_mobile_pin=[dic objectForKey:@"strMobileNum"];
                  gblclass.Exis_relation_id=[dic objectForKey:@"strRelationshipId"];
                  gblclass.M3sessionid=[dic objectForKey:@"strRetSessionId"];
                  gblclass.sign_up_token=[dic objectForKey:@"Token"];
                  
                  [self clearOTP];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Re-Generate OTP Successfully." preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
                  
                  [txt_6 resignFirstResponder];
                  
                  [self custom_alert:@"OTP Generated Successfully" :@"1"];
                  
                  [hud hideAnimated:YES];
                  
                  // [self segue];
              }
              else
              {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) clearOTP
{
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_termNCondition:(id)sender
{
    //vw_tNc.hidden=NO;
    
    
    //    [self.view addSubview:hud];
    //    [hud showAnimated:YES];
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
    [self presentViewController:vc animated:NO completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_agree:(id)sender
{
    chk_termNCondition=@"1";
    btn_chck_Image = [UIImage imageNamed:@"check.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled=YES;
    vw_tNc.hidden=YES;
    btn_resend_otp.enabled=YES;
}

-(IBAction)btn_disagree:(id)sender
{
    chk_termNCondition=@"0";
    btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled=NO;
    vw_tNc.hidden=YES;
    btn_resend_otp.enabled=NO;
}



//*********************** Login credential Start ******************************



-(void) with_touch_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


-(void) removeAllCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
}


-(void) getCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Look up server in the keychain
    NSDictionary* found = nil;
    CFDictionaryRef foundCF;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
    
    // Check if found
    found = (__bridge NSDictionary*)(foundCF);
    if (!found)
        return;
    
    // Found
    
    //17 march 2017
    // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
    pass = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    
    gblclass.sign_in_username = user;
    gblclass.sign_in_pw = pass;
    
}


-(void) finger_print_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


//*********************** End Login credential Start ******************************




// ********************** Device Check **********************************

//-(void) Guid_key_chain_method_make
//{
//    //Let's create an empty mutable dictionary:
//    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
//
//    NSString *username = @"Guid1";
//    NSString *GUID = [NSString stringWithFormat:@"%@",UDID1];
//    NSString *website = @"http://www.myawesomeservice.com";
//
//    //Populate it with the data and the attributes we want to use.
//
//    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
//    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
//    keychainItem[(__bridge id)kSecAttrServer] = website;
//    keychainItem[(__bridge id)kSecAttrAccount] = username;
//
//    //Check if this keychain item already exists.
//
//
//    if(SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, NULL) == noErr)
//    {
//        NSLog(@"Guid Already Exists");
//    }
//    else
//    {
//        // gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
//
//        NSLog(@"Guid create");
//        keychainItem[(__bridge id)kSecValueData] = [GUID dataUsingEncoding:NSUTF8StringEncoding]; //Our password
//
//        OSStatus sts = SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
//        NSLog(@"Error Code: %d", (int)sts);
//    }
//
//}
//
//
//-(void) Guid_key_chain_method_load
//{
//    //Let's create an empty mutable dictionary:
//    NSMutableDictionary *keychainItem = [NSMutableDictionary dictionary];
//
//    NSString *username = @"Guid1";
//    NSString *website = @"http://www.myawesomeservice.com";
//
//    //Populate it with the data and the attributes we want to use.
//
//    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassInternetPassword; // We specify what kind of keychain item this is.
//    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked; // This item can only be accessed when the user unlocks the device.
//    keychainItem[(__bridge id)kSecAttrServer] = website;
//    keychainItem[(__bridge id)kSecAttrAccount] = username;
//
//    //Check if this keychain item already exists.
//
//    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
//    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
//
//    CFDictionaryRef result1 = nil;
//
//    OSStatus sts = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result1);
//
//    NSLog(@"Error Code: %d", (int)sts);
//
//    if(sts == noErr)
//    {
//        NSLog(@"guid load");
//        NSDictionary *resultDict = (NSDictionary *)result1;
//        NSData *pswd = resultDict[(__bridge id)kSecValueData];
//        NSString *guid = [[NSString alloc] initWithData:pswd encoding:NSUTF8StringEncoding];
//
//        gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
//
//        //gblclass.Udid=guid;
//
//    }
//    else
//    {
//        NSLog(@"Guid Not found, Old load guid");
//        gblclass.Udid=[NSString stringWithFormat:@"%@",UDID1];
//
//    }
//}


// ********************** End Device Check **********************************


//- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"United Arab Emirates",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
//}

- (void)peekabooSDK:(NSDictionary *)params {
    /**
         For showing splash screen while view controller is fully presented
     */
//    if (!loadingView) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
//        UIViewController *uvc = [sb instantiateInitialViewController];
//        loadingView = uvc.view;
//        CGRect frame = self.view.frame;
//        frame.origin = CGPointMake(0, 0);
//        loadingView.frame = frame;
//        loadingView.layer.zPosition = 1;
//    }
    
    
    //saki
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:loadingView];
    UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
    peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:peekabooViewController animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [loadingView removeFromSuperview];
        });
    }];

   /**
           For not showing loader just present peekaboo view controller without adding any subview to keyWindow
    */
//        UIViewController *peekabooViewController = [Peekaboo.sharedManager getPeekabooUIViewController:params];
//        peekabooViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:peekabooViewController animated:YES completion:nil];

}




@end
