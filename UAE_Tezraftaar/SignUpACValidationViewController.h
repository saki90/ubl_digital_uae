//
//  SignUpACValidationViewController.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface SignUpACValidationViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

//@property (class) NSString *selectedOption;

//+(NSString *) selectedOption;
//+(void) setSelectedOptions;

@end

