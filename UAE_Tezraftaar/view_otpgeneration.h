//
//  view_otpgeneration.h
//  ubltestbanking
//
//  Created by ammar on 16/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface view_otpgeneration : UIViewController
{
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}
@property (nonatomic, strong) TransitionDelegate *transitionController;

@property (nonatomic, strong) IBOutlet UITextField* txtNick;
@property (nonatomic, strong) IBOutlet UITextField* txtemail;
@property (nonatomic, strong) IBOutlet UITextField* tctotp;

@property (nonatomic, strong) IBOutlet UILabel* txtaccountnumber;
@property (nonatomic, strong) IBOutlet UILabel* txtaccounttitle;
@property (nonatomic, strong) IBOutlet UILabel* txtphonenumber;

@property (nonatomic, strong) IBOutlet UILabel* txtmobilenumber;

@property (nonatomic, strong) IBOutlet UIButton* btnotp;

@end

