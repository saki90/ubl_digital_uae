//
//  GnbForgotPassword.m
//
//  Created by   on 08/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbForgotPassword.h"


NSString *const kGnbForgotPasswordID = @"ID";
NSString *const kGnbForgotPasswordTEXT = @"TEXT";


@interface GnbForgotPassword ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbForgotPassword

@synthesize iDProperty = _iDProperty;
@synthesize tEXT = _tEXT;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [self objectOrNilForKey:kGnbForgotPasswordID fromDictionary:dict];
            self.tEXT = [self objectOrNilForKey:kGnbForgotPasswordTEXT fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iDProperty forKey:kGnbForgotPasswordID];
    [mutableDict setValue:self.tEXT forKey:kGnbForgotPasswordTEXT];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeObjectForKey:kGnbForgotPasswordID];
    self.tEXT = [aDecoder decodeObjectForKey:kGnbForgotPasswordTEXT];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iDProperty forKey:kGnbForgotPasswordID];
    [aCoder encodeObject:_tEXT forKey:kGnbForgotPasswordTEXT];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbForgotPassword *copy = [[GnbForgotPassword alloc] init];
    
    if (copy) {

        copy.iDProperty = [self.iDProperty copyWithZone:zone];
        copy.tEXT = [self.tEXT copyWithZone:zone];
    }
    
    return copy;
}


@end
