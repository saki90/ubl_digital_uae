
//
//  JB_TermNCondition_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 04/10/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "JB_TermNCondition_VC.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "Globals.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "APIdleManager.h"
#import "Encrypt.h"


@interface JB_TermNCondition_VC ()
{
    UIImage *btn_chck_Image;
    NSString* chk_termNCondition;
    NSString* device_jail;
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UIAlertController* alert;
    NSString* chk_ssl;
    NSString* login_status_chck;
    APIdleManager* timer_class;
    BOOL runOnce;
    NSUserDefaults *defaults;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSDictionary* dic;
    NSString *login_name;
    NSString *login_pw;
    UIStoryboard *mainStoryboard;
    NSString* t_n_c,* device_jb_chk;
    NSString* logout_chck;
    NSString* chck_103;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation JB_TermNCondition_VC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    //  [hud showAnimated:YES];
    
    defaults = [NSUserDefaults standardUserDefaults];
    timer_class=[[APIdleManager alloc] init];
    
    //  btn_agree_chck.enabled = NO;
    runOnce = NO;
    
  //  NSLog(@"%@",gblclass.story_board);
    
    t_n_c = [defaults valueForKey:@"t&c"];
    device_jb_chk = [defaults valueForKey:@"isDeviceJailBreaked"];
    logout_chck = @"";
    
    if([gblclass.is_iPhoneX rangeOfString:@"iPad"].location == NSNotFound)
    {
        btn_agree_ipad.hidden = YES;
        btn_dis_agree_ipad.hidden = YES;
    }
    else
    {
        btn_agree_ipad.hidden = NO;
        btn_dis_agree_ipad.hidden = NO;
    }
    
   // NSLog(@"%@",gblclass.jb_chk_login_back);
    
    if ([gblclass.parent_vc_onbording isEqualToString:@"SecureCodeSignupVC"]) {
        [webview1 loadHTMLString:gblclass.termsAndconditions baseURL:nil];
        
    }  else if ([gblclass.term_conditn_chk_place isEqualToString:@"create_virtual_wiz"]) {
        [webview1 loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"VirtualWizT&C" ofType:@"html"] isDirectory:NO]]];
        
    }
    else
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        ssl_count = @"0";
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"load";
            [self SSL_Call];
        }
    }
    
//
//    //icrosoft Word 15 (filtered)
//    NSString* txt = @"<html><head><meta http-equiv=Content-Type content=text/html;charset = windows-1252><meta name=Generator content=Microsoft word 15 (filtered)><style><!-- /* Font Definitions */ @font-face{font-family:Wingdings;panose-1:5 0 0 0 0 0 0 0 0 0;}@font-face{font-family:Cambria Math;    panose-1:2 4 5 3 5 4 6 3 2 4;}@font-face    {font-family:Calibrpanose-1:2 15 5 2 2 2 4 3 2 4;}@font-face{font-family:Sakkal Majalla;}@font-face{font-family:Segoe UI;panose-1:2 11 5 2 4 2 4 2 2 3;} /* Style Definitions */p.MsoNormal,li.MsoNormal,div.MsoNormal{margin-top:0in;margin-right:0in;margin-bottom:10.0pt;margin-left:0in;line-height:115%;font-size:11.0pt;font-family:Calibri,sans-serif;}p.MsoFooter, li.MsoFooter, div.MsoFooter{mso-style-link:Footer Char;margin:0in;font-size:11.0pt;font-family:Calibri,sans-serif;}span.FooterChar{mso-style-name:Footer Char;mso-style-link:Footer;}.MsoChpDefault{font-family:Calibri,sans-serif;}.MsoPapDefault{margin-bottom:10.0pt;line-height:115%;}/* Page Definitions */@page WordSection1{size:8.5in 11.0in;margin:1.0in 1.0in 1.0in 1.0in;}div.WordSection1{page:WordSection1;}/* List Definitions */ol{margin-bottom:0in;}ul{margin-bottom:0in;}--></style></head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p><b>UBL Digital App Terms and Conditions</b></p><p class=p1><b><br>Definitions</b></p><p class=p2><span class=s1>In this document the following words and phrases shall have the meanings as set below unless the context indicates otherwise:</span></p><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Account (s)</b> refers to the Customer's bank account and/ or credit card account and/ or home loan account and/or any other type of account (each an Account and collectively Accounts, so maintained with United Bank Ltd (UBL) which are eligible Account(s) for operations through the use of Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Account Information</b> means information pertaining to the Account(s) maintained by the Customer with the Bank.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Affiliate</b> means the UBL business partners and vendors.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Alerts</b> means the Account Information provided by UBL to the Customer through the Customer's email address or mobile phone (based on SMS) generated and sent to the Customer by UBL at the specific request of the Customer which request shall be made by the Customer using the Digital App Services of UBL.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Bank</b> and <b>UBL</b> refer to United Bank Limited, which are to be used interchangeably.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Customer</b> refers to the UBL Account holders authorized to use Digital App. In case of the Customer being a minor, the legal guardian of such minor may be permitted to use Digital App subject to necessary approvals and conditions. In this document all references to the Customer being referred in masculine gender shall be deemed to include the feminine gender.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><b>Digital App</b> refers to UBL's NetBanking application  designed for mobile/smart phones.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Digital App Service</b> refers to the services available to Customers availing the Digital App including but not limited to viewing account balances, transferring funds, sending home remittances, paying bills online and any other services as UBL may decide to provide from time to time. The availability/non-availability of a particular service shall be at the sole discretion of UBL.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Payment Instruction(s)</b> shall mean any instruction given by the Customer to debit funds via the Digital App. These may include but not limited to bill payments, or mobile top-ups, home remittances transferring funds from the Account held by the Customer to accounts held by other approved Customers with UBL or other banks. The Bank may in its sole and exclusive discretion confine this facility only to certain permitted Customers or may extend it from time to time to be generally available to all Customers.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Personal Information</b> refers to the information provided by the Customer to UBL.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>SMS</b> means short message service which includes the storage, routing and delivery of alphanumeric messages over GSM or other telecommunication systems.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>Terms</b> refer to terms and conditions herein for use of the Digital App Service.</span></li></ul><ul class=ul1><li class=li3><span class=s2><b></b></span><span class=s1><b>OTP</b> means a six (06) digit one-time password, used for security verification.</span></li></ul><p class=p4><span class=s3><br></span><b>Applicability of Terms</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>These Terms form the contract between the Customer and UBL for use of the Digital App. By registering for the Digital App, the Customer acknowledges and accepts these Terms. These terms and conditions are in addition to those which are agreed by the Customer at the time of account opening.</span></li></ul><p class=p4><span class=s1><b><br>Digital App Accessibility</span></b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Digital App can only be used over the internet and the Customer must ensure that they have WIFI and/or GPRS services enabled prior to use of the Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer irrevocably and unconditionally undertakes to ensure that the password is kept strictly confidential and to not let any person have access to the mobile device while the Customer is accessing the Digital App. The Customer hereby indemnifies and holds harmless the Bank against any loss and/or liability whether financial, civil or criminal in nature arising from the Customer sharing/disclosing his/her password to any individual and/or party or permitting any person access to their smart phone/mobile device which accessing the Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer agrees and acknowledges that UBL shall in no way be held responsible or liable if the Customer incurs any loss, expenses, costs, damages, claims (including claims from any third party) as a result of use of the Digital App log by the Customer nor shall UBL be liable for information being disclosed by UBL regarding his Account(s) as provided and required to be disclosed by law or disclosed to any party with the Customer's consent pursuant to the access of the Digital App and the Customer shall fully indemnify and hold UBL harmless in respect of the above.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall maintain the secrecy of all information of confidential nature and shall ensure that the same is not disclosed to any person voluntarily, accidentally or by mistake.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer accepts and undertakes that it is the Customer's responsibility to ensure that the security of the Customer's mobile device and details of beneficiaries or transactions or any other relevant information registered by the Customer are not compromised. The Customer undertakes not to hold UBL liable or responsible for any loss or damage that may be suffered by the Customer due to transactions through the Digital App and shall keep UBL indemnified against all such losses or damages.</span></li></ul><p class=p4><span class=s1><b><br>OTP Service</b></span></p><ul class=ul1><li class=li3><span class=s2></span>OTP is used for security verification where customers are required to key in a six (6) digit code when making online transactions/operating UBL digital application. The OTP will be sent via short messaging system (SMS)/e-mail to customer's registered mobile number/e-mail address with UBL.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>Customer will receive OTP via SMS/e-mail; when abroad if the registered mobile phone number is on roaming mode at Customer's own cost of prevailing telco charges or any fee imposed by respective mobile phone service provider or any other party, provided telco supports and provides such international SMS on roaming facilities.</span></li></ul><ul class=ul1><li class=li3><span class=s2>Please acknowledge that the delivery of the OTP may be delayed or prevented by factor(s) outside the Bank's control. UBL shall not be liable for any loss, damage, expenses, fees, costs (including legal costs on a full indemnity basis) that may arise, directly or indirectly, in whole or in part, from:</span></li><ul class=ul2><li class=li3><span class=s2></span><span class=s1>The non-delivery, the delayed delivery, or the misdirected delivery of the OTP</span></li></ul><ul class=ul2><li class=li3><span class=s2></span><span class=s1>The non-receipt of the OTP</span></li></ul><ul class=ul2><li class=li3><span class=s2></span><span class=s1>Inaccurate or incomplete content in the OTP</span></li></ul><ul class=ul2><li class=li3><span class=s2></span><span class=s1>Reliance on or use of the information provided in the OTP.</span></li></ul><ul class=ul2><li class=li3><span class=s2></span><span class=s1>Misuse of OTP</span></li></ul></ul><p class=p4><span class=s1><b><br>Unauthorized Access</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall take all necessary precautions to prevent unauthorized and illegal use of Digital App and unauthorized access to his Accounts accessed through Digital App by preventing the sharing of information or password.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer will be required to change his password on a frequent basis. The Customer understands and acknowledges that UBL shall not be responsible for any consequences arising out of download of the Digital App application by the Customer from any third party application store which is not published by UBL.</span></li></ul><p class=p5><br><b>Maintenance of Sufficient Balance</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall ensure that there are sufficient funds at all material times in the Account for transactions to take effect through the Digital App, and UBL shall not be liable for any consequences arising out of the Customer's failure to ensure adequacy of funds.</span></li></ul><p class=p5><span class=s1><br><b>Funds Transfer through Digital App</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall use its best efforts to effect funds transfer transaction received through Digital App subject to availability of sufficient funds in the Account and subject to any laws of the QATAR, in force at the time.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall specify and update from time to time the limit for carrying out the various kinds of funds transfer or bill payment facilities available to the Customer through Digital App. The said facility/ies will be provided in accordance with the arrangement between UBL and the Customers and as per conditions specified by UBL from time to time. UBL shall not be liable for any omission to make all or any of the payments or for late payments for whatsoever cause howsoever arising.</span></li></ul><ul class=ul1><li class=li3><span class=s4></span>UBL currently allocates the Maximum Transaction Limit of QAR. 100,000</span><span class=s1>. per day to be conducted accumulatively through Net</span><span class=s6> </span><span class=s1>banking / Digital App. No Upper Limit is currently applicable for Transactions among Linked accounts, if the transaction amount is within daily transaction limit.</span></li></ul><p class=p5><span class=s1><br><b>Accuracy of Information</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer is responsible for the correctness and accuracy of any information supplied to UBL by the Customer for use of Digital App. UBL accepts no liability for any consequences whether arising out of erroneous information or the accuracy, reliability or completeness of information supplied by the Customer or otherwise.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>If the Customer notices an error in the information supplied to UBL either in the registration form or any other communication, he shall immediately imitate to UBL in writing</span></li></ul><p class=p5><span class=s1><br><b>Instructions</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>All instructions for operating Digital App shall be given through the Internet by the Customer in the manner indicated by UBL.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall be solely responsible for the accuracy and authenticity of the Payment Instruction provided to UBL and/or the Affiliates and the same shall be considered to be sufficient to operate / use the Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall not be required to independently verify the Payment Instruction; an instruction is effective unless countermanded by further instructions. UBL shall have no liability if it does not or is unable to stop or prevent the implementation of any instruction.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>Where UBL considers the instructions to be inconsistent or contradictory it may seek clarification from the Customer before acting on any instruction of the Customer or act upon any such instruction as it deems fit.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL may refuse to comply with the instructions without assigning any reason and shall not be under any duty to assess the prudence or otherwise of any instruction and have the right to suspend the operations through the Digital App if it has reason to believe that the Customer's instructions will lead or expose to direct or indirect loss or may require an indemnity from the Customer before continuing to operate / use the Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>In pursuit to comply with applicable laws and regulations UBL may intercept and investigate any payment messages and other information or communications sent to or by the Customer or on the Customer's behalf through other bank. This process may involve making future inquiries.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall be free to give Payment Instruction for transfer of funds for such purpose as he/she shall deem fit. The Customer however agrees not to use or permit the Payment Instruction or any related services for any illegal or improper purposes.</span></li></ul><p class=p5><b><br>For this, the Customer ensures that</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>He has full right, power and/or authority to access and avail the services obtained and the goods purchased and shall observe and comply with all applicable laws and regulations in each jurisdiction in applicable territories.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>He shall provide UBL such information and/or assistance as is required by UBL for the performance of the services and/or any other obligations of UBL under these Terms.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>He shall not at any time provide to any person, with any details of the accounts held by him with UBL including, the passwords, accountnumber, card numbers and PIN which may be assigned to me by UBL from time to time.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>He shall have access to and use the Digital App from a secure mobile device and that any compromise of the Customer Account resulting from an attempt to access it from an unsecure / compromised device, will be the sole responsibility of the Customer.</span></li></ul><p class=p5><span class=s1><br><b>Risks</b></span></p><p class=p5><span class=s1>The Customer hereby acknowledges that he utilizes Digital App Services at his own risk. These risks may include, but not limited to, the following:</span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer acknowledges that in case any third person obtains access to the Account, relevant information and the registered device, such third person would be able to instruct fund transfers and provide Payment Instructions.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Internet is susceptible to a number of frauds, misuse, hacking and other actions that could affect Payment Instructions to UBL. Whilst UBL shall aim to provide security to prevent the same, there cannot be any guarantee from such Internet frauds, hacking and other actions that could affect Payment Instructions to UBL. The Customer separately indemnifies UBL against all risks arising out of the same.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The transfer of funds to third party accounts shall require proper, accurate and complete details. The Customer shall be required to fill in the account number of the person to whom the funds are to be transferred. In the event of any inaccuracy in this regard, the funds may be transferred to incorrect accounts. In such an event UBL shall not be liable for any loss or damage caused.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The transaction(s) for transfer of funds as per Customer's instruction may not be completed for some reasons. In such cases, the Customer shall not hold UBL responsible in any manner whatsoever in the said transaction(s) and contracts and the Customer's sole recourse in this regard shall be with the beneficiary of the transaction.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The technology for enabling the transfer of funds and other services offered by UBL could be affected by virus or other malicious, destructive or corrupting code, program or macro. It may also be possible that the system may require maintenance and during such time it may not be able to process the request of the Customer. This could result in delays in the processing of instructions or failure in the processing of instructions and other such failures and inability.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer understands that UBL disclaims all and any liability, whether direct or indirect, whether arising out of loss of profit or otherwise arising out of any failure or inability by UBL to honor any Customer instruction for whatsoever reason. The Customer understands and accepts that UBL shall not be responsible for any of the aforesaid risks and UBL shall disclaim all liability in respect of the said risks.</span></li></ul><p class=p5><br><b>Authority to UBL for Digital App</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer irrevocably and unconditionally authorizes UBL to access all his Account(s) for effecting banking or other transactions performed by the Customer through the Digital App. The right to access shall also include the right at UBL's sole discretion to consolidate or merge any or all accounts of the Customer with UBL and the right to set off any amounts owing to UBL without prior notice.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The instructions of the Customer shall be effected only after authentication of the Customer in accordance with the prescribed procedure for Digital App by UBL or any one authorized by UBL. UBL shall have no obligation to verify the authenticity of any transaction received from the User other than by Login/Password.<br>The email confirmation, if any, that is received by the Customer at the time of operation of the Digital App by the Customer shall be accepted as conclusive and binding for all purposes.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span>All the records of UBL generated by the transactions arising out of the use of the Digital App, including the time the transaction recorded shall be conclusive proof of the genuineness and accuracy of the transaction.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>While UBL and the Affiliates shall endeavor to carry out the instructions promptly, they shall not be responsible for any delay in carrying on the instructions due to any reason whatsoever, including due to failure of operational systems or any requirement of law.</span></li></ul><p class=p5><span class=s1><br><b>Liability of the Customer</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>Neither UBL nor its Affiliates shall be liable for any unauthorized transactions occurring through Digital App and the Customer hereby fully indemnifies and holds UBL and its Affiliates harmless against any action, suit, proceeding initiated against it or any loss, costs or damages incurred by the Customer as a result thereof.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall under no circumstance be held liable to the Customer if the Digital App is not available in the desired manner for reasons including but not limited to natural calamities, legal restraints, faults in the telecommunication network or network failure, or any other reason beyond the control of UBL. Under no circumstances shall UBL be liable for any damages whatsoever, whether such damages are direct, indirect, incidental consequential and irrespective of whether any claim is based on loss of revenue, interruption of business or any loss of any character or nature whatsoever and whether sustained by the Customer or by any other person.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer is liable to keep his phone and access to the app secure and therefore do not jailbreak or root his phone, which is the process of removing software restrictions and limitations imposed by the official operating system of the device. Such activity can make the phone vulnerable to malware/viruses/malicious programs, compromise your phone's security features and it could mean that the UBL Digital App won't work properly or at all. Illegal or improper use of the Digital App shall render the Customer liable for payment of financial charges as decided by UBL at its sole discretion and/or will result in suspension of the Customer's operations through Digital App.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer undertakes to comply with all applicable laws and regulations governing the account of the Customer. For the avoidance of doubt, the governing law is the substantive and procedural laws of the Qatar.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer may use Digital App to direct UBL to provide the Customer with Account Information through Email or SMS from time to time to be sent directly to his registered email address or mobile number (which will be identified in writing prior to any directions to UBL by the Customer). The format and extent of information to be provided by way of Alerts to the Customer shall be determined by UBL. The frequency of the Alerts to be provided to the Customer will be determined by the Customer in accordance with the options provided by UBL. UBL shall under no circumstances whatsoever, be held responsible or liable by the Customer for any delay in delivery of the Alerts nor the inaccuracy of the information contained in such Alerts sent to the Customer by UBL. UBL shall not be liable for the incorrect delivery of, nor for its inability to deliver the Alerts altogether.</span></li></ul><p class=p5><span class=s1><b><br>Protecting against unauthorized logons</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Digital App allows the customer to optionally use the fingerprints he has stored on his Device to logon to the banking services.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>Once the Customer has nominated how he will logon, he will not be necessarily required to enter his Login ID and Password into the App. He will only be required to use Biometric verification to logon.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>Where the customer has opted to enable Biometric Login, any of the fingerprints he has stored on his Device can logon and can authorize any transactions in banking services. Customer should ensure that only his fingerprint/s are stored on his Device. When he logs on using Biometric Login, he instructs the Bank to perform the transactions.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>He can delete any of the fingerprints he has stored on his Device at any time by going into his Device settings.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>We also recommend that he setup a Passcode on his Device itself, so as to prevent unauthorized access to his Device and the App.</span></li></ul><p class=p5><span class=s1><b><br>Protecting against unauthorized transactions</b></span></p><p class=p5><span class=s1>The Customer agrees that he will</span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>not leave the Device unattended and left logged into the App;</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>lock his Device or take other steps necessary to stop unauthorized use of the Device and the App;</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>not act fraudulently or maliciously in relation to the App or software e.g. you will not copy, modify, adversely effect, reverse engineer, hack into or insert malicious code into the App or its software; and</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>let us know immediately by calling the </span><span class=s6><b>UBL Contact Centre 44441314 (with in Qatar) at (+974) 4444 1314 (outside Qatar) </b></span><span class=s1>; in case of any unusual/unauthorized transactions is noticed.</span></li></ul><p class=p5><span class=s1><b><br>Charges</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer hereby agrees to bear the charges as may be stipulated by UBL from time to time for availing the Digital App Services.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall be clearly notified of the charges through schedule of charges of the Bank or through information posted on the website of the Bank or through emails sent to Customer's registered email address with the bank.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span>The Customer hereby authorizes UBL to recover the service charge by debiting one of the Accounts of the Customer or by sending a bill to the Customer who will be liable to make the payment within a specified period. Failure to do so shall result in recovery of the service charge by UBL in any manner as may deem fit along with such interest, if any, and/or withdrawal of the Digital App without any liability to UBL.</span></li></ul><p class=p5><span class=s1><b><br>Applicability to Future Accounts</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL and the Customer agree that if the Customer opens further Accounts with UBL and/or subscribes to any of the products/services of UBL or any of the Affiliates, and UBL extends Digital App to such Accounts or products or services and the Customer opts for use thereof, then these Terms, subject to any changes, shall automatically apply to such further use of Digital App by the Customer.</span></li></ul><p class=p5><span class=s1><b><br>Indemnity</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>In consideration of UBL providing Digital App to the Customer, the Customer shall indemnify and hold UBL and/or UBL's Affiliates, as the case may be, including both their officers, employees and agents, indemnified against all losses and expenses on full indemnity basis which UBL may incur, sustain, suffer or is likely to suffer in connection with UBL or Affiliates' execution of the Customer's instructions and against all actions, claims, demands, proceedings, losses, damages, costs, charges and expenses as a consequence or by reason of providing a service through Digital App for any action taken or omitted to be taken by UBL and /or the Affiliates, its officers, employees or agents, on the instructions of the Customer.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer will pay UBL and /or its Affiliates such amount as may be determined by UBL at it sole discretion to be sufficient to indemnify UBL against any loss or expenses even though they may nothave arisen or are contingent in nature.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer shall take all necessary precautions to ensure that there are no mistakes and errors and that the information given, instructed or provided to UBL is error free, accurate, proper complete and up to date at all points of time. On the other hand in the event of the Customer's Account receiving an incorrect credit by reason of a mistake committed by any other person, UBL shall be entitled to reverse the incorrect credit at any time whatsoever without the prior consent of the Customer. The Customer shall be liable and responsible to Bank and accede to accept the Bank's instructions without questions for any unfair or unjust gain obtained by the Customer as a result of the same.</span></li></ul><p class=p5><span class=s1><br><b>Disclosure of Information</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customer accepts and agrees that UBL and/or its Affiliates or their contractors may hold and process his Personal Information and all other information concerning his Account(s) on computer or otherwise in connection with Digital App as well as for analysis, credit scoring and marketing. The Customer also agrees that UBL may disclose, as provided under current QATAR law, to other institutions Personal Information as may be reasonably necessary for reasons inclusive of but not limited to participation in any telecommunication or electronic clearing network, in compliance with a legal directive, for credit rating by recognized credit scoring agencies, for fraud prevention purposes.</span></li></ul><p class=p5><span class=s1><br><b>Change of Terms</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall have absolute discretion to amend or supplement or delete any of the Terms contained herein at any time and will endeavor to give prior notice of thirty days for such changes. Such change to the Terms shall be communicated to the Customer through its website or through email sent to the Customer's registered email address. By continuing to use any existing or new services as may be introduced by UBL, the Customer shall be deemed to have accepted the amended Terms.</span></li></ul><p class=p5><span class=s1><br><b>Non-Transferability</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The grant of Digital App to a Customer is purely personal in nature and not transferable under any circumstance and shall be used only by the Customer.</span></li></ul><p class=p5><span class=s1><br><b>Notices</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL may publish notices of general nature, which are applicable to all Customers in newspapers or on its web site or through emails sent to customers registered email address. Such notices will have the same effect as a notice served individually to each Customer.</span></li></ul><p class=p5><span class=s1><b><br>General</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>All costs incurred by the User, including but not limited to any telecommunication costs to use Digital App, would be borne by the Customer.</span></li></ul><p class=p5><span class=s1><b><br>Assignment</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>UBL shall be entitled to sell, assign or transfer UBL's right and obligations under the Terms and any security in favor of UBL (including all guarantee/s) to any person of UBL's choice in whole or in part and in such manner and on such terms and conditions as UBL may decide. Any such sale, assignment or transfer shall conclusively bind the Customer and all other persons.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Customers, his heirs, legal representatives, executors, administrators and successors are bound by the Terms and the Customer shall not be entitled to transfer or assign any of his rights and obligations.</span></li></ul><p class=p5><span class=s1><b><br>Governing Law and Jurisdiction</b></span></p><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Terms will be construed and enforced in accordance with, and the rights of the parties hereto will be governed by, the laws of the Qatar applicable therein. Any and all disputes arising under the Terms, whether as to interpretation, performance or otherwise, will be subject to the exclusive jurisdiction of the courts at State of Qatar and each of the parties hereto hereby irrevocably submits to the exclusive jurisdiction of such courts.</span></li></ul><ul class=ul1><li class=li3><span class=s2></span><span class=s1>The Parties hereby agree that any legal action or proceedings arising out of the Terms for Digital App shall be brought in the courts and to irrevocably submit themselves to the jurisdiction of such courts.</span></li></ul><ul class=ul1><li class="li3"><span class="s2"></span><span class="s1">UBL may, however, in its absolute discretion, commence any legal action or proceedings arising out of the Terms for Digital App in any other court, tribunal or other appropriate forum, and the Customer hereby consents to that jurisdiction.</span></li></ul><ul class="ul1"><li class=li3><span class="s2"></span><span class="s1">Any provision of the Terms for Digital App which is prohibited or unenforceable in any jurisdiction shall, as to such jurisdiction, be ineffective to the extent of prohibition or unenforceability but shall not invalidate the remaining provisions of the Terms or affect such provision in any other jurisdiction.</span></li></ul><p class="p5"><span class="s1"><b><br>Termination</span></b></span></p><ul class="ul1"><li class="li3"><span class="s2"></span><span class="s1">UBL reserves the right to terminate the facility of the Digital App, either partially or in totality, at any time whatsoever, without prior notice. UBL also reserves the right at any time without prior notice to add / alter / modify / change or vary all of these Terms and Conditions.</span></li></ul><div class=WordSection2><br><p class=MsoNormal align=center dir=RTL style='margin-bottom:0in;text-align:center;direction:rtl;unicode-bidi:embed'><b><span lang=AR-SA style='font-size:16.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1588;&#1585;&#1608;&#1591;&#1608;&#1571;&#1581;&#1603;&#1575;&#1605; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;</span></b><b><span lang=AR-EG style='font-size:16.0pt;line-height115%;fontfamily:"SakkalMajalla"'>&#1575;&#1604;<span><b><b><spanlang=ARSAstyle='fontsize16.0pt;lineheight115%;fontfamily:"SakkalMajalla"'>&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1604;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; </span></b><b><span dir=LTRstyle='font-size:16.0pt;line-height:115%;font-family:"Sakkal Majalla"'>UBL</span></b></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:16.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1575;&#1604;&#1578;&#1593;&#1585;&#1610;&#1601;&#1575;&#1578;</span></u></b></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='fontsize9.0pt;lineheight115%;fontfamily:"SakkalMajalla"'>&#1601;&#1610;&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1578;&#1581;&#1605;&#1604;&#1575;&#1604;&#1603;&#1604;&#1605;&#1575;&#1578;&#1608;&#1575;&#1604;&#1593;&#1576;&#1575;&#1585;&#1575;&#1578;&#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577;&#1575;&#1604;&#1605;&#1593;&#1575;&#1606;&#1610;&#1575;&#1604;&#1605;&#1608;&#1590;&#1581;&#1577;&#1571;&#1583;&#1606;&#1575;&#1607;&#1605;&#1575; &#1604;&#1605;&#1610;&#1583;&#1604; &#1575;&#1604;&#1587;&#1610;&#1575;&#1602;&#1593;&#1604;&#1609; &#1582;&#1604;&#1575;&#1601; &#1584;&#1604;&#1603;:</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;lineheight115%;fontfamily:"SakkalMajalla"'>&#1610;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576(&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;)&quot;&#1573;&#164;&#1609;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1604;&#1605;&#1589;&#1585;&#1601;&#1610;&#1604;&#1604;&#1593;&#1605;&#1610;&#1604;&#1571;&#1608;&#1581;&#1587;&#1575;&#1576;&#1576;&#1591;&#1575;&#1602;&#1577;&#1575;&#1604;&#1575;&#1574;&#1578;&#1605;&#1575;&#1606;&#1571;&#1608;&#1581;&#1587;&#1575;&#1576;&#1575;&#1604;&#1602;&#1585;&#1590;&#1575;&#1604;&#1587;&#1603;&#1606;&#1610;&#1571;&#1608;&#1571;&#1610;&#1606;&#1608;&#1593;&#1570;&#1582;&#1585;&#1605;&#1606;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578(&#1603;&#1604;&#1581;&#1587;&#1575;&#1576;&#1601&#1585;&#1583;&#1610;&#1571;&#1608;&#1603;&#1604;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1576;&#1588;&#1603;&#1604;&#1580;&#105;&#1575;&#1;#1610;&#1610;&#1581;&#1578;&#1601;&#1592;&#1576;&#1607;&#1575;&#1605;&#1593;&#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&quot;)&#1608;&#1607;&#1610;&1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1605;&#1572;&#1607;&#1604;&#1577;&#1604;&#1604;&#1578;&#1593;&#1575;&#1605;&#1604;&#1605;&#1606;&#1582;&#1604;&#1575;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;.</span></p><p class=MsoNormaldir=RTLstyle='margintop0in;marginright46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SAstyle='fontsize9.0pt;lineheight115%;fontfamily:"SakkalMajalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&quot;&#1573;&#1604&#1609;&#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1575;&#1604;&#1605;&#1578;&#1593;&#1604;&#1602;&#1577;&#1576;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1608;&#1575;&#1604;&#1578;&#1610;&#1610;&#1581;&#1578;&#1601;&#1592; &#1576;&#1607;&#1575;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1604;&#1583;&#1609; &#1575;&#1604;&#1576;&#1606;&#1603;.</span></p><pclass=MsoNormaldir=RTLstyle='margintop0in;marginright46.35pt;marginbottom0in;marginleft0in;textalign:justify;textindent:-25in;direction:rtl;unicodebidi:embed'><spanstyle='fontsize9.0pt;lineheight115%;fontfamily:Symbol'>∑<spanstyle='font7.0pt"TimesNewRoman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1578;&#1575;&#1576;&#1593;&quot;&#1573;&#1604;&#1609;&#1588;&#1585;&#1603;&#1575;&#1569;&#1575;&#1604;&#1571;&#1593;&#1605;&#1575;&#1604;&#1608;&#1575;&#1604;&#1605;&#1608;&#1585;&#1583;&#1610;&#1606; &#1601;&#1610;&#1575;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times NewRoman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1585;&#1575;&#1578;&quot; &#1573;&#1604;&#1609;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576; &#1575;&#1604;&#1578;&#1610; &#1610;&#1602;&#1583;&#1605;&#1607;&#1575;&#1575;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1604;&#1604;&#1593;&#1605;&#1610;&#1604;&#1605;&#1606;&#1582;&#1604;&#1575;&#1604;&#1593;&#1606;&#1608;&#1575;&#1606;&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;&#1604;&#1604;&#1593;&#1605;&#1610;&#1604; &#1571;&#1608;&#1575;&#1604;&#1607;&#1575;&#1578;&#1601; &#1575;&#1604;&#1605;&#1581;&#1605;&#1608;&#1604;(&#1601;&#1610; &#1589;&#1608;&#1585;&#1577;&#1585;&#1587;&#1575;&#1574;&#1604; &#1606;&#1589;&#1610;&#1577;&#1602;&#1589;&#1610;&#1585;&#1577;) &#1608;&#1575;&#1604;&#1578;&#1610;&#1578;&#1603;&#1578;&#1576;&#1608;&#1578;&#1585;&#1587;&#1604;&#1573;&#1604;&#1609;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1576;&#1608;&#1575;&#1587;&#1591;&#1577; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1576;&#1606;&#1575;&#1569;&#1611; &#1593;&#1604;&#1609; &#1591;&#1604;&#1576;&#1605;&#1581;&#1583;&#1583; &#1605;&#1606;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1608;&#1575;&#1604;&#1584;&#1610;&#1610;&#1580;&#1576;&#1571;&#1606;&#1610;&#1602;&#1583;&#1605;&#1607;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605; &#1582;&#1583;&#1605;&#1577;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1604;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1588;&#1610;&#1585;&#1605;&#1589;&#1591;&#1604;&#1581; &quot;&#1575;&#1604;&#1576;&#1606;&#1603;&quot;&#1608; &quot;&#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&quot; &#1573;&#1604;&#1609;&#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1548;&#1608;&#1610;&#1587;&#1578;&#1582;&#1583;&#1605;&#1575;&#1606;&#1576;&#1575;&#1604;&#1578;&#1576;&#1575;&#1583;&#1604;.</span></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1588;&#1610;&#1585;&quot;<b>&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;</b>&quot;&#1573;&#1604;&#1609; &#1571;&#1589;&#1581;&#1575;&#1576;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1575;&#1604;&#1605;&#1589;&#1585;&#1581; &#1604;&#1607;&#1605;&#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;. &#1601;&#1610;&#1581;&#1575;&#1604;&#1577; &#1603;&#1608;&#1606;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1602;&#1575;&#1589;&#1585;&#1611;&#1575;&#1548;&#1610;&#1580;&#1608;&#1586; &#1604;&#1604;&#1608;&#1589;&#1610;&#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1604;&#1607;&#1584;&#1575;&#1575;&#1604;&#1602;&#1575;&#1589;&#1585;&#1576;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610; &#1608;&#1601;&#1602;&#1611;&#1575;&#1604;&#1604;&#1605;&#1608;&#1575;&#1601;&#1602;&#1575;&#1578;&#1608;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&#1575;&#1604;&#1604;&#1575;&#1586;&#1605;&#1577;. &#1601;&#1610;&#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;&#1548; &#1578;&#1588;&#1605;&#1604;&#1575;&#1604;&#1573;&#1588;&#1575;&#1585;&#1575;&#1578; &#1573;&#1604;&#1609;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1576;&#1575;&#1604;&#1580;&#1606;&#1587;&#1575;&#1604;&#1605;&#1584;&#1603;&#1585; &#1575;&#1604;&#1580;&#1606;&#1587; &#1575;&#1604;&#1605;&#1572;&#1606;&#1579;&#1571;&#1610;&#1590;&#1611;&#1575;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&quot; &#1573;&#1604;&#1609;&#1578;&#1591;&#1576;&#1610;&#1602; </span><span dir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>Net Banking</span><span dir=RTL></span><spanlang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span dir=RTL></span> &#1605;&#1606; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1575;&#1604;&#1605;&#1589;&#1605;&#1605;&#1604;&#1604;&#1607;&#1608;&#1575;&#1578;&#1601; &#1575;&#1604;&#1584;&#1603;&#1610;&#1577;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times NewRoman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"SakkalMajalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1582;&#1583;&#1605;&#1577;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1578;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577;&quot; &#1573;&#1604;&#1609;&#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;&#1575;&#1604;&#1605;&#1578;&#1575;&#1581;&#1577;&#1604;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569;&#1575;&#1604;&#1584;&#1610;&#1606;&#1610;&#1587;&#1578;&#1582;&#1583;&#1605;&#1608;&#1606;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603;&#1593;&#1604;&#1609; &#1587;&#1576;&#1610;&#1604;&#1575;&#1604;&#1605;&#1579;&#1575;&#1604; &#1604;&#1575;&#1575;&#1604;&#1581;&#1589;&#1585;&#1605;&#1593;&#1585;&#1601;&#1577;&#1571;&#1585;&#1589;&#1583;&#1577;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1548;&#1608;&#1578;&#1581;&#1608;&#1610;&#1604;&#1575;&#1604;&#1571;&#1605;&#1608;&#1575;&#1604;&#1548; &#1608;&#1573;&#1585;&#1587;&#1575;&#1604;&#1578;&#1581;&#1608;&#1610;&#1604;&#1575;&#1578; &#1605;&#1606; &#1575;&#1604;&#1605;&#1606;&#1586;&#1604;&#1548;&#1608;&#1583;&#1601;&#1593;&#1575;&#1604;&#1601;&#1608;&#1575;&#1578;&#1610;&#1585;&#1593;&#1576;&#1585;&#1575;&#1604;&#1573;&#1606;&#1578;&#1585;&#1606;&#1578; &#1608;&#1571;&#1610;&#1582;&#1583;&#1605;&#1575;&#1578; &#1571;&#1582;&#1585;&#1609; &#1602;&#1583;&#1578;&#1602;&#1585;&#1585; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1578;&#1602;&#1583;&#1610;&#1605;&#1607;&#1575;&#1605;&#1606; &#1608;&#1602;&#1578; &#1604;&#1570;&#1582;&#1585;.&#1610;&#1582;&#1590;&#1593; &#1578;&#1608;&#1601;&#1585; &#1571;&#1608;&#1593;&#1583;&#1605; &#1578;&#1608;&#1601;&#1585; &#1582;&#1583;&#1605;&#1577;&#1605;&#1593;&#1610;&#1606;&#1577;&#1604;&#1578;&#1602;&#1583;&#1610;&#1585;&#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1608;&#1581;&#1583;&#1607;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;fontfamily:"SakkalMajalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1578;&#1593;&#1604;&#1610;&#1605;&#1575;&#1578;&#1575;&#1604;&#1583;&#1601;&#1593;&quot; &#1573;&#1604;&#1609;&#1578;&#1593;&#1604;&#1610;&#1605;&#1575;&#1578;&#1610;&#1602;&#1583;&#1605;&#1607;&#1575;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1604;&#1583;&#1601;&#1593;&#1575;&#1604;&#1571;&#1605;&#1608;&#1575;&#1604; &#1593;&#1576;&#1585;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1548;&#1608;&#1602;&#1583; &#1578;&#1588;&#1605;&#1604; &#1593;&#1604;&#1609;&#1587;&#1576;&#1610;&#1604; &#1575;&#1604;&#1605;&#1579;&#1575;&#1604;&#1604;&#1575; &#1575;&#1604;&#1581;&#1589;&#1585; &#1583;&#1601;&#1593;&#1575;&#1604;&#1601;&#1608;&#1575;&#1578;&#1610;&#1585;&#1548; &#1571;&#1608;&#1588;&#1581;&#1606;&#1575;&#1604;&#1607;&#1575;&#1578;&#1601;&#1575;&#1604;&#1605;&#1581;&#1605;&#1608;&#1604;&#1548;&#1608;&#1575;&#1604;&#1578;&#1581;&#1608;&#1610;&#1604;&#1575;&#1578;&#1575;&#1604;&#1605;&#1606;&#1586;&#1604;&#1610;&#1577; &#1571;&#1610;&#1578;&#1581;&#1608;&#1610;&#1604;&#1575;&#1604;&#1571;&#1605;&#1608;&#1575;&#1604; &#1605;&#1606;&#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1604;&#1605;&#1605;&#1604;&#1608;&#1603; &#1604;&#1604;&#1593;&#1605;&#1610;&#1604;&#1573;&#1604;&#1609; &#1575;&#1604;&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1575;&#1604;&#1605;&#1605;&#1604;&#1608;&#1603;&#1577;&#1604;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569; &#1575;&#1604;&#1570;&#1582;&#1585;&#1610;&#1606;&#1575;&#1604;&#1605;&#1593;&#1578;&#1605;&#1583;&#1610;&#1606;&#1604;&#1583;&#1609; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1571;&#1608; &#1575;&#1604;&#1576;&#1606;&#1608;&#1603;&#1575;&#1604;&#1571;&#1582;&#1585;&#1609;. &#1610;&#1580;&#1608;&#1586;&#1604;&#1604;&#1576;&#1606;&#1603;&#1548; &#1608;&#1601;&#1602;&#1611;&#1575;&#1604;&#1578;&#1602;&#1583;&#1610;&#1585;&#1607;&#1575;&#1604;&#1582;&#1575;&#1589;&#1608;&#1575;&#1604;&#1581;&#1589;&#1585;&#1610;&#1548; &#1602;&#1589;&#1585; &#1575;&#1604;&#1587;&#1605;&#1575;&#1581;&#1576;&#1607;&#1584;&#1607;&#1575;&#1604;&#1578;&#1587;&#1607;&#1610;&#1604;&#1575;&#1578;&#1593;&#1604;&#1609; &#1576;&#1593;&#1590;&#1575;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569; &#1601;&#1602;&#1591;&#1571;&#1608; &#1602;&#1583; &#1610;&#1605;&#1583;&#1583;&#1607;&#1575;&#1605;&#1606; &#1608;&#1602;&#1578; &#1604;&#1570;&#1582;&#1585;&#1604;&#1578;&#1603;&#1608;&#1606; &#1605;&#1578;&#1575;&#1581;&#1577;&#1576;&#1588;&#1603;&#1604; &#1593;&#1575;&#1605;&#1604;&#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1575;&#1604;&#1588;&#1582;&#1589;&#1610;&#1577;&quot; &#1573;&#1604;&#1609;&#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1575;&#1604;&#1578;&#1610;&#1602;&#1583;&#1605;&#1607;&#1575; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1604;&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583; &#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;.</span></p>margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1585;&#1587;&#1575;&#1574;&#1604;&#1575;&#1604;&#1602;&#1589;&#1610;&#1585;&#1577;&quot; &#1573;&#1604;&#1609;&#1582;&#1583;&#1605;&#1577;&#1575;&#1604;&#1585;&#1587;&#1575;&#1574;&#1604;&#1575;&#1604;&#1602;&#1589;&#1610;&#1585;&#1577; &#1575;&#1604;&#1578;&#1610;&#1578;&#1578;&#1590;&#1605;&#1606; &#1578;&#1582;&#1586;&#1610;&#1606;&#1608;&#1578;&#1608;&#1580;&#1610;&#1607;&#1608;&#1578;&#1587;&#1604;&#1610;&#1605;&#1575;&#1604;&#1585;&#1587;&#1575;&#1574;&#1604;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577;&#1593;&#1576;&#1585; </span><span dir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>GSM</span><span dir=RTL></span><spanlang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span dir=RTL></span> &#1571;&#1608; &#1571;&#1606;&#1592;&#1605;&#1577;&#1575;&#1604;&#1575;&#1578;&#1589;&#1575;&#1604;&#1575;&#1578;&#1575;&#1604;&#1571;&#1582;&#1585;&#1609;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1578;&#1588;&#1610;&#1585;&quot;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&quot; &#1573;&#1604;&#1609;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;&#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1607;&#1606;&#1575;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1582;&#1583;&#1605;&#1577;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1578;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1578;&#1593;&#1606;&#1610;&#1603;&#1604;&#1605;&#1577;&quot;&#1603;&#1604;&#1605;&#1577;&#1605;&#1585;&#1608;&#1585; &#1604;&#1605;&#1585;&#1577;&#1608;&#1575;&#1581;&#1583;&#1577; </span><span dir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>OTP</span><span dir=RTL></span><spanlang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><spandir=RTL></span>&quot; &#1573;&#1604;&#1609; &#1603;&#1604;&#1605;&#1577;&#1605;&#1585;&#1608;&#1585; &#1605;&#1603;&#1608;&#1606;&#1577; &#1605;&#1606;&#1587;&#1578;&#1577; &#1571;&#1585;&#1602;&#1575;&#1605; (6) &#1578;&#1587;&#1578;&#1582;&#1583;&#1605;&#1604;&#1605;&#1585;&#1577; &#1608;&#1575;&#1581;&#1583;&#1577;&#1604;&#1604;&#1578;&#1581;&#1602;&#1602;&#1575;&#1604;&#1571;&#1605;&#1606;&#1610;.</span></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span style='text-decoration: none'>&nbsp;</span></span></u></b></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&#1575;&#1604;&#1605;&#1593;&#1605;&#1608;&#1604; &#1576;&#1607;&#1575;</span></u></b><spanlang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>:</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"SakkalMajalla"'>&#1578;&#1588;&#1603;&#1604;&#1607;&#1584;&#1607; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&#1575;&#1604;&#1593;&#1602;&#1583; &#1575;&#1604;&#1605;&#1576;&#1585;&#1605;&#1576;&#1610;&#1606; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1608;&#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1585;&#1602;&#1605;&#1610;.&#1576;&#1605;&#1580;&#1585;&#1583; &#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;&#1601;&#1610; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1548; &#1573;&#1606;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1610;&#1602;&#1585;&#1608;&#1610;&#1602;&#1576;&#1604; &#1607;&#1584;&#1607;&#1575;&#1604;&#1588;&#1585;&#1608;&#1591;&#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;&#1576;&#1575;&#1604;&#1573;&#1590;&#1575;&#1601;&#1577;&#1573;&#1604;&#1609;&#1578;&#1604;&#1603; &#1575;&#1604;&#1578;&#1610; &#1608;&#1575;&#1601;&#1602;&#1593;&#1604;&#1610;&#1607;&#1575; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1601;&#1610;&#1608;&#1602;&#1578; &#1601;&#1578;&#1581; &#1575;&#1604;&#1581;&#1587;&#1575;&#1576;.</span></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span style='text-decoration: none'>&nbsp;</span></span></u></b></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"SakkalMajalla"'>&#1573;&#1605;&#1603;&#1575;&#1606;&#1610;&#1577;&#1575;&#1604;&#1608;&#1589;&#1608;&#1604; &#1573;&#1604;&#1609;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1578;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1577;</span></u></b></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;fontfamily:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1604;&#1575;&#1610;&#1605;&#1603;&#1606;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1573;&#1604;&#1575;&#1593;&#1576;&#1585;&#1575;&#1604;&#1573;&#1606;&#1578;&#1585;&#1606;&#1578;&#1608;&#1610;&#1580;&#1576; &#1593;&#1604;&#1609;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1575;&#1604;&#1578;&#1571;&#1603;&#1583; &#1605;&#1606;&#1578;&#1605;&#1603;&#1610;&#1606; &#1582;&#1583;&#1605;&#1575;&#1578; </span><spandir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>WIFI</span><spandir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span dir=RTL></span> &#1571;&#1608; </span><spandir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>GPRS</span><spandir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span dir=RTL></span> &#1602;&#1576;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1578;&#1593;&#1607;&#1583;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1576;&#1588;&#1603;&#1604;&#1604;&#1575;&#1585;&#1580;&#1593;&#1577; &#1601;&#1610;&#1607;&#1608;&#1583;&#1608;&#1606; &#1602;&#1610;&#1583; &#1571;&#1608;&#1588;&#1585;&#1591; &#1576;&#1590;&#1605;&#1575;&#1606;&#1575;&#1604;&#1581;&#1601;&#1575;&#1592; &#1593;&#1604;&#1609;&#1587;&#1585;&#1610;&#1577; &#1603;&#1604;&#1605;&#1577;&#1575;&#1604;&#1605;&#1585;&#1608;&#1585; &#1608;&#1593;&#1583;&#1605;&#1575;&#1604;&#1587;&#1605;&#1575;&#1581; &#1604;&#1571;&#1610;&#1588;&#1582;&#1589; &#1576;&#1575;&#1604;&#1608;&#1589;&#1608;&#1604;&#1573;&#1604;&#1609; &#1575;&#1604;&#1580;&#1607;&#1575;&#1586;&#1575;&#1604;&#1605;&#1581;&#1605;&#1608;&#1604;&#1571;&#1579;&#1606;&#1575;&#1569; &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;&#1604;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;. &#1610;&#1593;&#1608;&#1590;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1576;&#1605;&#1608;&#1580;&#1576;&#1607;&#1584;&#1575;&#1575;&#1604;&#1576;&#1606;&#1603;&#1608;&#1610;&#1578;&#1581;&#1605;&#1604;&#1593;&#1606;&#1607; &#1575;&#1604;&#1590;&#1585;&#1585; &#1590;&#1583;&#1571;&#1610; &#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577; &#1571;&#1608;&#1605;&#1583;&#1606;&#1610;&#1577; &#1571;&#1608; &#1573;&#1580;&#1585;&#1575;&#1605;&#1610;&#1577;&#1606;&#1575;&#1578;&#1580;&#1577; &#1593;&#1606;&#1605;&#1588;&#1575;&#1585;&#1603;&#1577;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1604;&#1603;&#1604;&#1605;&#1577;&#1575;&#1604;&#1605;&#1585;&#1608;&#1585;&#1575;&#1604;&#1582;&#1575;&#1589;&#1577; &#1576;&#1607; &#1571;&#1608;&#1603;&#1588;&#1601;&#1607;&#1575; &#1604;&#1571;&#1610; &#1601;&#1585;&#1583;&#1571;&#1608; &#1591;&#1585;&#1601; &#1571;&#1608;&#1575;&#1604;&#1587;&#1605;&#1575;&#1581; &#1604;&#1571;&#1610;&#1588;&#1582;&#1589; &#1576;&#1575;&#1604;&#1608;&#1589;&#1608;&#1604;&#1573;&#1604;&#1609;&#1607;&#1608;&#1575;&#1578;&#1601;&#1607;&#1605;&#1575;&#1604;&#1584;&#1603;&#1610;&#1577; &#1575;&#1604;&#1578;&#1610;&#1610;&#1605;&#1603;&#1606;&#1607;&#1575;&#1575;&#1604;&#1608;&#1589;&#1608;&#1604; &#1573;&#1604;&#1609;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1608;&#1575;&#1601;&#1602;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1608;&#1610;&#1602;&#1585;&#1576;&#1571;&#1606; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1604;&#1606; &#1610;&#1578;&#1581;&#1605;&#1604;&#1576;&#1571;&#1610; &#1581;&#1575;&#1604; &#1605;&#1606;&#1575;&#1604;&#1571;&#1581;&#1608;&#1575;&#1604;&#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577; &#1593;&#1606;&#1571;&#1610; &#1582;&#1587;&#1575;&#1585;&#1577; &#1571;&#1608;&#1605;&#1589;&#1585;&#1608;&#1601;&#1575;&#1578; &#1571;&#1608;&#1578;&#1603;&#1575;&#1604;&#1610;&#1601; &#1571;&#1608;&#1571;&#1590;&#1585;&#1575;&#1585; &#1571;&#1608;&#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578;(&#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603; &#1575;&#1604;&#1605;&#1591;&#1575;&#1604;&#1576;&#1575;&#1578;&#1605;&#1606; &#1571;&#1610; &#1591;&#1585;&#1601;&#1579;&#1575;&#1604;&#1579;) &#1610;&#1578;&#1603;&#1576;&#1583;&#1607;&#1575;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1606;&#1578;&#1610;&#1580;&#1577; &#1575;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;&#1593;&#1604;&#1609; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1548; &#1608;&#1604;&#1606;&#1610;&#1578;&#1581;&#1605;&#1604; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577;&#1593;&#1606; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1575;&#1604;&#1578;&#1610; &#1610;&#1601;&#1589;&#1581;&#1593;&#1606;&#1607;&#1575; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1604;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1601;&#1610;&#1605;&#1575;&#1610;&#1578;&#1593;&#1604;&#1602; &#1576;&#1581;&#1587;&#1575;&#1576;&#1607;(&#1581;&#1587;&#1575;&#1576;&#1575;&#1578;&#1607;) &#1593;&#1604;&#1609;&#1575;&#1604;&#1606;&#1581;&#1608;&#1575;&#1604;&#1605;&#1606;&#1589;&#1608;&#1589; &#1593;&#1604;&#1610;&#1607;&#1608;&#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1575;&#1604;&#1603;&#1588;&#1601; &#1593;&#1606;&#1607;&#1576;&#1605;&#1608;&#1580;&#1576;&#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606; &#1571;&#1608; &#1606;&#1578;&#1610;&#1580;&#1577;&#1575;&#1604;&#1603;&#1588;&#1601; &#1593;&#1606;&#1607;&#1575;&#1604;&#1571;&#1610;&#1591;&#1585;&#1601;&#1576;&#1605;&#1608;&#1575;&#1601;&#1602;&#1577;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1608;&#1601;&#1602;&#1611;&#1575;&#1604;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604; &#1593;&#1604;&#1609;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610; &#1608;&#1610;&#1580;&#1576;&#1593;&#1604;&#1609;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1578;&#1593;&#1608;&#1610;&#1590; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1593;&#1606; &#1575;&#1604;&#1590;&#1585;&#1585;&#1575;&#1604;&#1606;&#1575;&#1578;&#1580; &#1593;&#1605;&#1575;&#1587;&#1576;&#1602;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1580;&#1576;&#1593;&#1604;&#1609; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1575;&#1604;&#1581;&#1601;&#1575;&#1592; &#1593;&#1604;&#1609;&#1587;&#1585;&#1610;&#1577; &#1580;&#1605;&#1610;&#1593;&#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;&#1584;&#1575;&#1578;&#1575;&#1604;&#1591;&#1576;&#1610;&#1593;&#1577;&#1575;&#1604;&#1587;&#1585;&#1610;&#1577; &#1608;&#1610;&#1580;&#1576;&#1575;&#1604;&#1578;&#1571;&#1603;&#1583; &#1605;&#1606; &#1593;&#1583;&#1605;&#1575;&#1604;&#1603;&#1588;&#1601; &#1593;&#1606;&#1607;&#1575;&#1604;&#1571;&#1610; &#1588;&#1582;&#1589;&#1591;&#1608;&#1575;&#1593;&#1610;&#1577; &#1571;&#1608;&#1593;&#1585;&#1590;&#1610;&#1577; &#1571;&#1608; &#1593;&#1606;&#1591;&#1585;&#1610;&#1602; &#1575;&#1604;&#1582;&#1591;&#1571;.</span></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;textalign:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times NewRoman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1610;&#1602;&#1585;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1608;&#1610;&#1578;&#1593;&#1607;&#1583; &#1576;&#1571;&#1606;&#1607;&#1607;&#1608;&#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604; &#1593;&#1606;&#1590;&#1605;&#1575;&#1606; &#1571;&#1605;&#1606; &#1580;&#1607;&#1575;&#1586;&#1607;&#1575;&#1604;&#1605;&#1581;&#1605;&#1608;&#1604;&#1608;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604;&#1575;&#1604;&#1605;&#1587;&#1578;&#1601;&#1610;&#1583;&#1610;&#1606;&#1608;&#1575;&#1604;&#1605;&#1593;&#1575;&#1605;&#1604;&#1575;&#1578; &#1608;&#1571;&#1610;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1571;&#1582;&#1585;&#1609;&#1584;&#1575;&#1578; &#1589;&#1604;&#1577; &#1605;&#1587;&#1580;&#1604;&#1577;&#1605;&#1606; &#1602;&#1576;&#1604; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;&#1548;&#1608;&#1610;&#1578;&#1593;&#1607;&#1583;&#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1576;&#1593;&#1583;&#1605;&#1578;&#1581;&#1605;&#1610;&#1604; &#1575;&#1604;&#1576;&#1606;&#1603;&#1575;&#1604;&#1605;&#1578;&#1581;&#1583; &#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577; &#1593;&#1606;&#1571;&#1610; &#1582;&#1587;&#1575;&#1585;&#1577; &#1571;&#1608;&#1590;&#1585;&#1585; &#1602;&#1583; &#1610;&#1578;&#1603;&#1576;&#1583;&#1607;&#1576;&#1587;&#1576;&#1576; &#1605;&#1593;&#1575;&#1605;&#1604;&#1575;&#1578;&#1607;&#1593;&#1604;&#1609;&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;&#1608;&#1610;&#1580;&#1576;&#1593;&#1604;&#1610;&#1607; &#1578;&#1593;&#1608;&#1610;&#1590;&#1575;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583; &#1593;&#1606; &#1603;&#1604;&#1607;&#1584;&#1607; &#1575;&#1604;&#1582;&#1587;&#1575;&#1574;&#1585;&#1571;&#1608; &#1575;&#1604;&#1571;&#1590;&#1585;&#1575;&#1585;.</span></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><span style='text-decoration: none'>&nbsp;</span></span></u></b></p><p class=MsoNormal dir=RTL style='margin-bottom:0in;text-align:justify;direction:rtl;unicode-bidi:embed'><b><u><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>&#1582;&#1583;&#1605;&#1577;</span></u></b><spanlang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'><b><u>&#1603;&#1604;&#1605;&#1577; &#1605;&#1585;&#1608;&#1585;&#1604;&#1605;&#1585;&#1577; &#1608;&#1575;&#1581;&#1583;&#1577; </u></b></span><b><u><spandir=LTR style='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>OTP</span></u></b></p><p class=MsoNormal dir=RTL style='margin-top:0in;margin-right:46.35pt;margin-bottom:0in;margin-left:0in;text-align:justify;text-indent:-.25in;direction:rtl;unicode-bidi:embed'><span style='font-size:9.0pt;line-height:115%;font-family:Symbol'>∑<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span dir=RTL></span><span lang=AR-SA style='font-size:9.0pt;line-height:115%;font-family:"SakkalMajalla"'>&#1578;&#1615;&#1587;&#1578;&#1582;&#1583;&#1605;&#1603;&#1604;&#1605;&#1577; &#1605;&#1585;&#1608;&#1585;&#1604;&#1605;&#1585;&#1577; &#1608;&#1575;&#1581;&#1583;&#1577;&#1604;&#1604;&#1578;&#1581;&#1602;&#1602; &#1605;&#1606;&#1575;&#1604;&#1571;&#1605;&#1575;&#1606; &#1581;&#1610;&#1579;&#1610;&#1615;&#1591;&#1604;&#1576; &#1605;&#1606;&#1575;&#1604;&#1593;&#1605;&#1604;&#1575;&#1569;&#1573;&#1583;&#1582;&#1575;&#1604; &#1585;&#1605;&#1586;&#1605;&#1603;&#1608;&#1606; &#1605;&#1606; &#1587;&#1578;&#1577; (6)&#1571;&#1585;&#1602;&#1575;&#1605; &#1593;&#1606;&#1583;&#1573;&#1580;&#1585;&#1575;&#1569;&#1575;&#1604;&#1605;&#1593;&#1575;&#1605;&#1604;&#1575;&#1578; &#1593;&#1576;&#1585;&#1575;&#1604;&#1573;&#1606;&#1578;&#1585;&#1606;&#1578; &#1571;&#1608;&#1578;&#1588;&#1594;&#1610;&#1604; &#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1604;&#1576;&#1606;&#1603; &#1575;&#1604;&#1605;&#1578;&#1581;&#1583;&#1575;&#1604;&#1605;&#1581;&#1583;&#1608;&#1583;&#1575;&#1604;&#1585;&#1602;&#1605;&#1610;. &#1587;&#1610;&#1578;&#1605;&#1573;&#1585;&#1587;&#1575;&#1604; &#1603;&#1604;&#1605;&#1577;&#1605;&#1585;&#1608;&#1585; &#1604;&#1605;&#1585;&#1577;&#1608;&#1575;&#1581;&#1583;&#1577; &#1593;&#1576;&#1585;&#1606;&#1592;&#1575;&#1605; &#1575;&#1604;&#1585;&#1587;&#1575;&#1574;&#1604;&#1575;&#1604;&#1602;&#1589;&#1610;&#1585;&#1577; (</span><span dir=LTRstyle='font-size:9.0pt;line-height:115%;font-family:"Sakkal Majalla"'>SMS</span></p>\n\n</div>\n\n</body>\n</html>\n";
    
//    NSString *htmlString =
//    [NSString stringWithFormat:@"<font face='Aspira Light' size='3'>%@", txt];
//    [webview1 loadHTMLString:htmlString baseURL:nil];
     
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    //[self Play_bg_video];
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
   
    if ([gblclass.forget_pw_btn_click isEqualToString:@"1"])
    {
        [hud hideAnimated:YES];
    }
//    gblclass.forget_pw_btn_click=@"1";
//    gblclass.forgot_pw_click = @"1";
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:YES];
    // [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}

-(IBAction)btn_back:(id)sender
{
    
  //  NSLog(@"%@",gblclass.otp_chck);
    
    if ( [gblclass.otp_chck isEqualToString:@"cnic"])
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"verification_acc"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
}

-(IBAction)btn_agree:(id)sender
{
    
        [hud showAnimated:YES];
        [self.view addSubview:hud];
    //    [hud showAnimated:YES];
    
    gblclass.chk_termNCondition = @"1";
    btn_chck_Image = [UIImage imageNamed:@"check.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled = YES;
    vw_tNc.hidden = YES;
    btn_resend_otp.enabled = YES;
    
    gblclass.TnC_chck_On = @"1";
    
  //  NSLog(@"%@",gblclass.isDeviceJailBreaked);
    if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"])
    {
        // defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"2"  forKey:@"isDeviceJailBreaked"];
        [defaults synchronize];
    }
    
    [defaults setValue:@"2" forKey:@"t&c"];
    [defaults synchronize];
    
//    NSLog(@"%@",gblclass.isDeviceJailBreaked);
//    NSLog(@"%@",gblclass.term_conditn_chk_place);

    
    //29 june 2018 saki
//    if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login"])
//    {
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
//    {
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
//    {
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
//    {
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
//    {
//        //SignUpOTPViewController
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
//    {
//        //SignUpOTPViewController
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
//
//    }
//    else
//    {
    
    
   //     NSLog(@"%@",gblclass.jb_chk_login_back);
    
        if ([gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"]) {
            [hud showAnimated:YES];
            
            [self checkinternet];
            if (netAvailable) {
                chk_ssl = @"signup";
                [self SSL_Call];
            }
            
        } else if ([gblclass.term_conditn_chk_place isEqualToString:@"create_virtual_wiz"]) {
            
            gblclass.acct_add_type = @"LOAD VIRTUAL WIZ CARD";
            [hud showAnimated:YES];
            
            [self checkinternet];
            if (netAvailable) {
                chk_ssl = @"postWizRequest";
                [self SSL_Call];
            }
            
        } else if([gblclass.jb_chk_login_back isEqualToString:@"1"]) {
            [hud hideAnimated:YES];
            gblclass.jb_chk_login_back = @"0";
            
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
            [self presentViewController:vc animated:NO completion:nil];
            
        } else if([gblclass.parent_vc_onbording isEqualToString:@"SecureCodeSignupVC"]) {
            [hud showAnimated:YES];
            [self.view addSubview:hud];
            [self checkinternet];
            if (netAvailable) {
                chk_ssl = @"generateOTPForAddition";
                [self SSL_Call];
            }
        } else {
            
             [hud showAnimated:YES];
            if ([gblclass.signUp_through_cnic isEqualToString:@"1"])
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl = @"signup";
                    [self SSL_Call];
                }
            }
            else
            {
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl = @"next";
                    [self SSL_Call];
                }
            }
            
            
  //      }
       
        
        
        //29 june 2018 saki
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"verification_acc"];
//        [self presentViewController:vc animated:NO completion:nil];
//
//        //        [self dismissViewControllerAnimated:NO completion:nil];
//
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.3;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionPush;
//        transition.subtype = kCATransitionFromRight;
//        [ self.view.window.layer addAnimation:transition forKey:nil];
        
    }
}

-(IBAction)btn_disagree:(id)sender
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    gblclass.chk_termNCondition=@"0";
    btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled=NO;
    vw_tNc.hidden=YES;
    btn_resend_otp.enabled=NO;
    
    gblclass.TnC_chck_On=@"0";
    
    [defaults setValue:@"0" forKey:@"t&c"];
    [defaults synchronize];
    
    [hud showAnimated:YES];
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"disagree";
        [self SSL_Call];
    }
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    //    [self presentViewController:vc animated:NO completion:nil];
    //
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromRight;
    //    [ self.view.window.layer addAnimation:transition forKey:nil];
    
}


-(void) Show_Term_Condition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        // NSLog(@"%@",gblclass.chk_device_jb);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"IP",
                                                                       @"isRooted", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ShowTermCondition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //             NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSString* responsecode =[NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
                  
                  if([responsecode isEqualToString:@"0"])
                  {
                      
                      btn_agree_chck.enabled = YES;
                      
                      // termNcondi_txt = [dic objectForKey:@"strReturnMessage"];
                      
                      NSString *urlAddress = [dic objectForKey:@"TermNConditionText"];
                      // @"http://www.ubldirect.com/Corporate/ebank.aspx";
                      
                      //                  //Create a URL object.
                      //                  NSURL *url = [NSURL URLWithString:urlAddress];
                      //
                      //                  //URL Requst Object
                      //                  NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
                      //
                      //                  //Load the request in the UIWebView.
                      //                  [_webView loadRequest:requestObj];
                      //
                      //                  _webView.delegate = self;
                      
                      
                      //                  NSString *html = [NSString stringWithFormat:[NSString stringWithFormat:@"%@ %@ %@"],@"<html><p>",urlAddress,@"</p></html>"];
                      
//                      NSArray *components = [urlAddress componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//
//                      NSMutableArray *componentsToKeep = [NSMutableArray array];
//
//                      for (int i = 0; i < [components count]; i = i + 2)
//                      {
//                          [componentsToKeep addObject:[components objectAtIndex:i]];
//                      }
//
//                      NSString *plainText = [componentsToKeep componentsJoinedByString:@""];
//
//                      NSLog(@"%@",plainText);
                        
                      [webview1 loadHTMLString:urlAddress baseURL:nil];
                       
//                      sleep(2);
                     // [hud hideAnimated:YES];
                      
                      //[_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"DigitalApp_TCs_1" ofType:@"html"]isDirectory:NO]]];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                   [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:[error localizedDescription] :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



-(void) Is_Rooted_Device:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        //  public void IsRootedDevice(string Device_ID, string IP, Int16 isRooted, string isTncAccepted, string callingSource)
        
        
//        NSLog(@"%@",gblclass.TnC_chck_On);
//        NSLog(@"%@",gblclass.TnC_calling_source);
//        NSLog(@"%@",gblclass.chk_device_jb);
//        NSLog(@"%@",gblclass.isDeviceJailBreaked);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb],
                                    [encrypt encrypt_Data:gblclass.TnC_chck_On],
                                    [encrypt encrypt_Data:gblclass.TnC_calling_source], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"IP",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"callingSource", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsRootedDevice" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                 
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                     // NSLog(@"%@",gblclass.term_conditn_chk_place);
                      
                      if ([gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
                      {// new_signup
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"registration_type"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else if ([gblclass.term_conditn_chk_place isEqualToString:@"create_virtual_wiz"])
                      {// new_signup
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"LoadVirtualWizCard"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else
                      {
                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      
                      //29 june 2018 saki
                      
//                      if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login"])
//                      {
//
//                          //                          [self slideRight];
//                          //    [self dismissViewControllerAnimated:NO completion:nil];
//
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//
//                          //    [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //       [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//
//                          //         [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"verification_acc"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//                      }
//
//                  }
//                  else
//                  {
//                      [hud hideAnimated:YES];
//
//
//                      if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login"])
//                      {
//
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//
//                      }
//                      else
//                      {
//                          storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"verification_acc"];
//                          [self presentViewController:vc animated:NO completion:nil];
//
//                          //                          [self dismissViewControllerAnimated:NO completion:nil];
//
//                          CATransition *transition = [CATransition animation];
//                          transition.duration = 0.3;
//                          transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
//                          transition.type = kCATransitionPush;
//                          transition.subtype = kCATransitionFromRight;
//                          [ self.view.window.layer addAnimation:transition forKey:nil];
//                      }
//
//                      //[self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Try again later." :@"0"];
        
    }
}



//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
//{
//    [hud hideAnimated:YES];
//    
//    //NSLog(@"%@",error);
//}



-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void) slideRight {

    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

-(void) slideLeft {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}


//29 june 2018 saki
//-(void) Existing_UserLogin:(NSString *)strIndustry
//{
//
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
//                                                                                   ]];
//
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
//
//
//    NSString* device_value;
//    if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"])
//    {
//        device_value=@"No";
//    }
//    else if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"])
//    {
//        device_value=@"Yes";
//    }
//    else
//    {
//        device_value=@"No";
//    }
//
//   // NSLog(@"%@",gblclass.chk_device_jb);
//
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
//                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                [encrypt encrypt_Data:@"asdf"],
//                                [encrypt encrypt_Data:@"0"],
//                                [encrypt encrypt_Data:gblclass.signup_username],
//                                [encrypt encrypt_Data:gblclass.signup_pw],
//                                [encrypt encrypt_Data:gblclass.Udid],
//                                [encrypt encrypt_Data:device_jail],
//                                [encrypt encrypt_Data:chk_termNCondition],
//                                [encrypt encrypt_Data:@"0"], nil]
//
//                                                          forKeys:[NSArray arrayWithObjects:@"strSessionId",
//                                                                   @"IP",
//                                                                   @"LogBrowserInfo",
//                                                                   @"hCode",
//                                                                   @"strLoginName",
//                                                                   @"strPassword",
//                                                                   @"strDeviceID",
//                                                                   @"strIsTouchLogin",
//                                                                   @"isRooted",
//                                                                   @"isTnCAccepted", nil]];
//
//
//    //NSLog(@"%@",dictparam);
//
//    [manager.requestSerializer setTimeoutInterval:time_out];
//
//    [manager POST:@"ExistingUserMobReg" parameters:dictparam progress:nil
//
//          success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         NSError *error1=nil;
//         //NSArray *arr = (NSArray *)responseObject;
//         NSDictionary* dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//         login_status_chck=[dic objectForKey:@"Response"];
//
//         //   if ([login_status_chck isEqualToString:@"0"])
//
//         if ([[dic objectForKey:@"Response"] integerValue]==0)
//         {
//
//             [self.avplayer pause];
//
//             gblclass.From_Exis_login=@"1";
//             gblclass.sign_Up_chck=@"yes";
//             gblclass.signup_login_name = gblclass.signup_username;
//             gblclass.signup_pw = gblclass.signup_pw;
//             gblclass.exis_user_mobile_pin=[dic objectForKey:@"strMobileNum"];
//             gblclass.M3sessionid=[dic objectForKey:@"strRetSessionId"];
//             gblclass.Exis_relation_id=[dic objectForKey:@"strRelationshipId"];
//             gblclass.user_id=[dic objectForKey:@"strUserid"];
//             gblclass.sign_up_token=[dic objectForKey:@"Token"];
//
//             [self segue];
//
//         }
//         else
//         {
//             [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//         }
//
//         [hud hideAnimated:YES];
//
//     }
//
//          failure:^(NSURLSessionDataTask *task, NSError *error) {
//              // [mine myfaildata];
//
//              [hud hideAnimated:YES];
//
//              [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
//
//          }];
//}

-(void)segue
{
    if ([login_status_chck isEqualToString:@"0"] || [login_status_chck isEqualToString:@"-96"])
    {
        //NSLog(@"%@",txt_login.text);
        gblclass.user_login_name=login_name;
        [timer_class createTimer];
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        
        
        //        lbl_heading.frame = CGRectMake(self.view.frame.size.width + lbl_heading.frame.size.width, lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        //
        //        emailOutlet.frame = CGRectMake(self.view.frame.size.width + emailOutlet.frame.size.width, emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //        emailView.frame = CGRectMake(self.view.frame.size.width + emailView.frame.size.width, emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        passOutlet.frame = CGRectMake(self.view.frame.size.width + passOutlet.frame.size.width, passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //        passView.frame = CGRectMake(self.view.frame.size.width + passView.frame.size.width, passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        forgetPass.frame = CGRectMake(self.view.frame.size.width + forgetPass.frame.size.width, forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //        btn_login_color.frame = CGRectMake(self.view.frame.size.width + btn_login_color.frame.size.width, btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        
        runOnce = YES;
        
        
        
        //        [UIView animateWithDuration:0.4 animations:^{
        //            lbl_heading.frame = CGRectMake(-(self.view.frame.size.width+lbl_heading.frame.size.width), lbl_heading.frame.origin.y, lbl_heading.frame.size.width, lbl_heading.frame.size.height);
        //
        //        }];
        
        //        [UIView animateWithDuration:0.6 animations:^{
        //
        //            emailOutlet.frame = CGRectMake(-(self.view.frame.size.width+emailOutlet.frame.size.width), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:0.8 animations:^{
        //
        //            emailOutlet.frame = CGRectMake(-(self.view.frame.size.width+emailOutlet.frame.size.width), emailOutlet.frame.origin.y, emailOutlet.frame.size.width, emailOutlet.frame.size.height);
        //            emailView.frame = CGRectMake(-(self.view.frame.size.width+emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:0.8 animations:^{
        //
        //            passOutlet.frame = CGRectMake(-(self.view.frame.size.width+passOutlet.frame.size.width), passOutlet.frame.origin.y, passOutlet.frame.size.width, passOutlet.frame.size.height);
        //            passView.frame = CGRectMake(-(self.view.frame.size.width+passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
        //
        //        }];
        //
        //        [UIView animateWithDuration:1 animations:^{
        //
        //            forgetPass.frame = CGRectMake(-(self.view.frame.size.width+forgetPass.frame.size.width), forgetPass.frame.origin.y, forgetPass.frame.size.width, forgetPass.frame.size.height);
        //
        //
        //        }];
        //
        //        [UIView animateWithDuration:1.1 animations:^{
        //
        //            btn_login_color.frame = CGRectMake(-(self.view.frame.size.width+btn_login_color.frame.size.width), btn_login_color.frame.origin.y, btn_login_color.frame.size.width, btn_login_color.frame.size.height);
        //
        //
        //        }];
        
        //        [UIView animateWithDuration:1.1 animations:^{
        //
        //            vw_apply_acct.frame = CGRectMake(-(self.view.frame.size.width+vw_apply_acct.frame.size.width), vw_apply_acct.frame.origin.y, vw_apply_acct.frame.size.width, vw_apply_acct.frame.size.height);
        //
        //        }];
        
        
        
        [NSTimer scheduledTimerWithTimeInterval:0.8
                                         target:self
                                       selector:@selector(present_Controller)
                                       userInfo:nil
                                        repeats:NO];
 
        
        //        CATransition *transition = [ CATransition animation ];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window. layer addAnimation:transition forKey:nil];
        
        
        //[self performSegueWithIdentifier:@"question" sender:nil];
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Retry" :@"0"];
        
    }
}

-(void)present_Controller
{
    [hud hideAnimated:YES];
    [self slideRight];
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
    [self presentViewController:vc animated:NO completion:nil];
    
}



///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"load"]) {
            [self Show_Term_Condition:@""];
            
        } else if ([chk_ssl isEqualToString:@"next"]) {
            chk_ssl=@"";
            [self Existing_UserLogin:@""];
            
        } else if ([chk_ssl isEqualToString:@"disagree"]) {
            chk_ssl=@"";
            [self Is_Rooted_Device:@""];
            
        } else if ([chk_ssl isEqualToString:@"signup"]) {
            chk_ssl=@"";
            [self User_Signup_Step2:@""];
            
        } else if([chk_ssl isEqualToString:@"mobileAppSecureCodeVerify"]) {
            chk_ssl=@"";
            [self mobileAppSecureCodeVerify:@""];
            
        } else if([chk_ssl isEqualToString:@"generateOTPForAddition"]) {
            chk_ssl=@"";
            [self generateOTPForAddition:@""];
            
        } else if([chk_ssl isEqualToString:@"postWizRequest"]) {
            chk_ssl=@"";
            [self postWizRequest:@""];
            
        }
        
    }
    else
    {
        
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
      //          NSLog(@"%@",gblclass.SSL_Certificate_name);
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            btn_agree_chck.enabled = NO;
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        [self Show_Term_Condition:@""];
    }
    else if ([chk_ssl isEqualToString:@"next"])
    {
        chk_ssl=@"";
        [self Existing_UserLogin:@""];
    }
    else if ([chk_ssl isEqualToString:@"disagree"])
    {
        chk_ssl=@"";
        [self Is_Rooted_Device:@""];
    }
    else if ([chk_ssl isEqualToString:@"signup"])
    {
        chk_ssl=@"";
        [self User_Signup_Step2:@""];
    } else if([chk_ssl isEqualToString:@"mobileAppSecureCodeVerify"]) {
        chk_ssl=@"";
        [self mobileAppSecureCodeVerify:@""];
        
    } else if([chk_ssl isEqualToString:@"generateOTPForAddition"]) {
        chk_ssl=@"";
        [self generateOTPForAddition:@""];
        
    } else if([chk_ssl isEqualToString:@"postWizRequest"]) {
        chk_ssl=@"";
        [self postWizRequest:@""];
        
    }
    
    chk_ssl=@"";
}


 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [hud hideAnimated:YES];
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
//    if (self.responseData == nil) {
//        self.responseData = [NSMutableData dataWithData:data];
//        NSLog(@"%@", self.responseData);
//        [self custom_alert:@"Attention" :@"0"];
//
//    } else {
//        [self.responseData appendData:data];
//    }
    
//    if (self.responseData == nil) {
//        self.responseData = [NSMutableData dataWithData:data];
//    } else {
//        [self.responseData appendData:data];
//    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut)
    {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    }
    else if (error.code == NSURLErrorNotConnectedToInternet)
    {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    
   // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
 //   NSLog(@"%@",gblclass.SSL_name);
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
  //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}

//- (BOOL)isSSLPinning
//{
//    NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
//    return [envValue boolValue];
//}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
    s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



-(void) Existing_UserLogin:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        UIDevice *deviceInfo = [UIDevice currentDevice];
        
        //NSLog(@"Device name:  %@", deviceInfo.name);
        
        login_name = [gblclass.signup_login_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        login_pw = [gblclass.signup_pw stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
//        randomNumber = arc4random() % 10+1;
//
//        NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
        
  //      gblclass.login_session=random;
        
        
        
        //    gblclass.signup_username=login_name;
        //    gblclass.signup_pw=login_pw;
        
        NSString* device_value;
        chck_103 = @"";
        
        if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"]) {
            device_value=@"No";
            gblclass.chk_termNCondition = @"0";
        } else if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"]) {
            device_value=@"Yes";
        } else {
            device_value=@"No";
            gblclass.chk_termNCondition = @"0";
        }
        
        //NSLog(@"%@",gblclass.chk_device_jb);
        //NSLog(@"%@",gblclass.chk_termNCondition);
        
        t_n_c = [defaults valueForKey:@"t&c"];
        device_jb_chk = [defaults valueForKey:@"isDeviceJailBreaked"];
        
        if ([t_n_c isEqualToString:@"2"]) {
            t_n_c = @"1";
        }
        
        if ([device_jb_chk isEqualToString:@"2"]) {
            device_jb_chk = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"asdf"],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:gblclass.signup_login_name],
                                    [encrypt encrypt_Data:gblclass.signup_pw],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:device_jb_chk],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"LogBrowserInfo",
                                                                       @"hCode",
                                                                       @"strLoginName",
                                                                       @"strPassword",
                                                                       @"strDeviceID",
                                                                       @"strIsTouchLogin",
                                                                       @"isRooted",
                                                                       @"isTnCAccepted", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ExistingUserMobRegRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject)
         {
             NSError *error1 = nil;
             //NSArray *arr = (NSArray *)responseObject;
             dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             login_status_chck = [dic objectForKey:@"Response"];
             
             
             //   if ([login_status_chck isEqualToString:@"0"])
             
             if ([[dic objectForKey:@"Response"] integerValue]==0)
             {
                 
                 [self.avplayer pause];
                 
                 gblclass.From_Exis_login = @"1";
                 gblclass.sign_Up_chck = @"yes";
                 //gblclass.signup_login_name = txt_login.text;
                 //gblclass.signup_pw = txt_pw1.text;
                 gblclass.exis_user_mobile_pin = [dic objectForKey:@"strMobileNum"];
                 gblclass.M3sessionid = [dic objectForKey:@"strRetSessionId"];
                 gblclass.Exis_relation_id = [dic objectForKey:@"strRelationshipId"];
                 gblclass.user_id = [dic objectForKey:@"struserId"];
                 gblclass.sign_up_token = [dic objectForKey:@"Token"];
                 
                 [self segue];
                 
             }
             else if ([[dic objectForKey:@"Response"] integerValue]==103)
             {
                 //NSLog(@"%@",gblclass.str_processed_login_chk);
                 gblclass.chck_103 = @"1";
                 chck_103 = @"1";
                 [self showAlert:@"Dear Customer, You have made 3 attempts of entering incorrect password. Please change your password from the given option." :@"Attention"];
                 
//                 if ([gblclass.str_processed_login_chk isEqualToString:@"1"])
//                 {
//                     mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                     vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"create_option"];
//                     [self presentViewController:vc animated:NO completion:nil];
//                 }
//                 else
//                 {
//                     mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                     vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup"];
//                     [self presentViewController:vc animated:NO completion:nil];
//                 }
                 
             }
             else
             {
                 [hud hideAnimated:YES];
                 [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
             }
         }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:Call_center_msg :@"0"];
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:Call_center_msg :@"0"];
    }
}

-(void) User_Signup_Step2:(NSString *)strIndustry
{
    
    @try {
        
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        UIDevice *deviceInfo = [UIDevice currentDevice];
        
        
//        randomNumber = arc4random() % 10+1;
//        NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
        
      //  gblclass.login_session=random;
        NSDictionary *dictparam = [[NSDictionary alloc] init];
        
        
        gblclass.arr_resend_with_atm_signup_email = [[NSMutableArray alloc] init];
        gblclass.arr_resend_with_atm_signup_dob = [[NSMutableArray alloc] init];
        
        if ([gblclass.btn_reg_selected isEqualToString:@"email"])
        {
//            gblclass.nic_account_no;
//            gblclass.nic_mobile_no;
//            gblclass.nic_email;
            
            [gblclass.arr_resend_with_atm_signup_email addObject:gblclass.nic_email];
            [gblclass.arr_resend_with_atm_signup_email addObject:gblclass.nic_mobile_no];
            [gblclass.arr_resend_with_atm_signup_email addObject:[gblclass.nic_account_no
                                                                  substringFromIndex:[gblclass.nic_account_no length] - 4]];
            
            dictparam = [NSDictionary dictionaryWithObjects:
                         [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                          [encrypt encrypt_Data:@"1234"],
                          [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                          [encrypt encrypt_Data:@"abc"],
                          [encrypt encrypt_Data:gblclass.Udid],
                          [encrypt encrypt_Data:gblclass.sign_up_token],
                          [encrypt encrypt_Data:gblclass.cnic_no],
                          [encrypt encrypt_Data:[gblclass.frist_last_debt_no substringToIndex:4]],
                          [encrypt encrypt_Data:[gblclass.frist_last_debt_no substringFromIndex:[gblclass.frist_last_debt_no length] - 4]],
                          [encrypt encrypt_Data:gblclass.isEmail_Exist],
                          [encrypt encrypt_Data:gblclass.nic_email],
                          [encrypt encrypt_Data:@""],
                          [encrypt encrypt_Data:gblclass.nic_mobile_no],
                          [encrypt encrypt_Data:[gblclass.nic_account_no substringFromIndex:[gblclass.nic_account_no length] - 4]], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                             @"strSessionId",
                                                             @"IP",
                                                             @"LogBrowserInfo",
                                                             @"Device_ID",
                                                             @"Token",
                                                             @"strCNIC",
                                                             @"strDebitcard_left",
                                                             @"strDebitcard_right",
                                                             @"isEmail_Exist",
                                                             @"email",
                                                             @"DOB",
                                                             @"mobile_num",
                                                             @"account_num", nil]];
        }
        else if ([gblclass.btn_reg_selected isEqualToString:@"dob"])
        {
            
            [gblclass.arr_resend_with_atm_signup_dob addObject:gblclass.nic_email];
            [gblclass.arr_resend_with_atm_signup_dob addObject:gblclass.nic_mobile_no];
            [gblclass.arr_resend_with_atm_signup_dob addObject:[gblclass.nic_account_no
                                                                substringFromIndex:[gblclass.nic_account_no length] - 4]];
            
            
            dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id], [encrypt encrypt_Data:@"1234"], [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], [encrypt encrypt_Data:@"abc"], [encrypt encrypt_Data:gblclass.Udid], [encrypt encrypt_Data:gblclass.sign_up_token], [encrypt encrypt_Data:gblclass.cnic_no], [encrypt encrypt_Data:[gblclass.frist_last_debt_no substringToIndex:4]], [encrypt encrypt_Data:[gblclass.frist_last_debt_no substringFromIndex:[gblclass.frist_last_debt_no length] - 4]], [encrypt encrypt_Data:gblclass.isEmail_Exist], [encrypt encrypt_Data:@""], [encrypt encrypt_Data:gblclass.nic_email], [encrypt encrypt_Data:gblclass.nic_mobile_no], [encrypt encrypt_Data:[gblclass.nic_account_no substringFromIndex:[gblclass.nic_account_no length] - 4]], nil]
                         
                                                    forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"LogBrowserInfo",@"Device_ID",
                                                             @"Token",@"strCNIC",@"strDebitcard_left",@"strDebitcard_right",@"isEmail_Exist",@"email",@"DOB",@"mobile_num",@"account_num", nil]];
        }
        
       // NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"UserSignupStep2OverLoad" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSError *error1=nil;
                  //NSArray *arr = (NSArray *)responseObject;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                 // NSLog(@"%@",dic);
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      //[avplayer pause];
                      [hud hideAnimated:YES];
                      
                      [self slideRight];
                      gblclass.otp_chck = @"cnic";
                      gblclass.term_conditn_chk_place = @"sign_with_cnic";
                      
                      //5 july 2018 saki
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-2"])
                  {
                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                          [self slideLeft];
                          mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      //                 [self slideLeft];
                      //                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
                      //                 [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  //   NSLog(@"%@",[error localizedDescription]);
                [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
                  
               //   [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

- (void)mobileAppSecureCodeVerify:(NSString *)strIndustry {
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects: [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:@"iOS"],
                                    [encrypt encrypt_Data:device_jb_chk],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:gblclass.cnic],
                                    [encrypt encrypt_Data:gblclass.secureCode],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects: @"strSessionId",
                                                                       @"IP",
                                                                       @"strDeviceID",
                                                                       @"LogBrowserInfo",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"strCNIC",
                                                                       @"strSecureCode", nil]];
        
        // Input
        //        @"strSessionId"
        //        @"IP"
        //        @"LogBrowserInfo"
        //        @"isRooted"
        //        @"isTncAccepted"
        //        @"strDeviceID"
        //        @"strCNIC"
        //        @"strSecureCode"
        //
        //        // output
        //        @"struserId"
        //        @"strRetVal"
        //        @"strReturnMessage"
        //        @"Token"
        //        @"strParaSessionID"
        //        @"relationshipID"
        
        //        Service Response:
        //         0    Successful
        //        -1    Secure code could not be verified
        //        -2    UserException
        //        -3    Exception
        //        -4    Invalid CNIC
        //        -77   Token generation failed
        
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"MobileAppSecureCodeVerify" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue] == 0) {
                      
                      //                      [hud hideAnimated:YES];
                      gblclass.user_id = [dic objectForKey:@"struserId"];
//                      gblclass.retValue = [dic objectForKey:@"strRetVal"];
                      gblclass.sign_up_token = [dic objectForKey:@"Token"];
                      gblclass.M3sessionid = [dic objectForKey:@"strParaSessionID"];
                      gblclass.Exis_relation_id = [dic objectForKey:@"relationshipID"];
                      
                      [self generateOTPForAddition:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue] == -1 ||
                             [[dic objectForKey:@"Response"] integerValue] == -2 ||
                             [[dic objectForKey:@"Response"] integerValue] == -3 ||
                             [[dic objectForKey:@"Response"] integerValue] == -4 ||
                             [[dic objectForKey:@"Response"] integerValue] == -77 ) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideLeft];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)generateOTPForAddition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:gblclass.cnic],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"NEWSIGNUP"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userId",
                                                                       @"strSessionId",
                                                                       @"M3Key",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strBranchCode",
                                                                       @"strAccountNo",
                                                                       @"strAccessKey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        
        [manager.requestSerializer setTimeoutInterval: time_out];
        [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [self slideRight];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifySecureCodeOTP"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



//-(void) User_Sign_Up:(NSString *)strIndustry
//{
//
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
//                                                                                   ]];
//
//    // manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//    //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
//
//    UIDevice *deviceInfo = [UIDevice currentDevice];
//
//    //NSLog(@"Device name:  %@", deviceInfo.name);
//
//    //    login_name = [txt_login.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    //    login_pw = [txt_pw1.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
////    randomNumber = arc4random() % 10+1;
////    NSString* random=[NSString stringWithFormat:@"%ld",(long)randomNumber];
//
// //   gblclass.login_session=random;
//
//    //NSLog(@"%ld",(long)randomNumber);
//
//    //    gblclass.signup_username=login_name;
//    //    gblclass.signup_pw=login_pw;
//
//
//    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1234", @"1.1.1.1", @"asdf", @"0", login_name, login_pw, gblclass.Udid, @"0", nil]
//    //                                                          forKeys:[NSArray arrayWithObjects:@"strSessionId",@"IP",@"LogBrowserInfo",@"hCode",@"strLoginName",@"strPassword",@"strDeviceID",@"strIsTouchLogin", nil]];
//
//
////    NSLog(@"%@",self.cnicNumber.text);
////    NSLog(@"%@",self.atmCardNo.text);
////    NSLog(@"%@",self.atmCardPin.text);
////    NSLog(@"%@",t_n_c);
//    NSLog(@"%@",gblclass.isDeviceJailBreaked);
//
//    if ([t_n_c isEqualToString:@"2"])
//    {
//        t_n_c = @"1";
//    }
//
//
////    gblclass.txt_cnic_signup
////    gblclass.txt_atmCard_signup
////    gblclass.txt_atmPin_signup
//
//    NSLog(@"%@",[gblclass.txt_atmCard_signup substringFromIndex:[gblclass.txt_atmCard_signup length] - 4]);
//    NSLog(@"%@",[gblclass.txt_atmCard_signup substringToIndex:4]);
//    NSLog(@"%@",gblclass.txt_cnic_signup);
//
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
//                                                                   [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                                                   [encrypt encrypt_Data:gblclass.Udid],
//                                                                   [encrypt encrypt_Data: gblclass.txt_cnic_signup],
//                                                                   [encrypt encrypt_Data:[gblclass.txt_atmCard_signup substringToIndex:4]],
//                                                                   [encrypt encrypt_Data:[gblclass.txt_atmCard_signup substringFromIndex:[gblclass.txt_atmCard_signup length] - 4]],
//                                                                   [encrypt encrypt_Data:gblclass.txt_atmPin_signup],
//                                                                   [encrypt encrypt_Data:@"abcd"],
//                                                                   [encrypt encrypt_Data:device_jb_chk],
//                                                                   [encrypt encrypt_Data:t_n_c],    nil]
//
//                                                          forKeys:[NSArray arrayWithObjects:@"strSessionId",
//                                                                   @"IP",
//                                                                   @"Device_ID",
//                                                                   @"strCNIC",
//                                                                   @"strDebitCardFirst",
//                                                                   @"strDebitCardLast",
//                                                                   @"cvv",
//                                                                   @"LogBrowserInfo",
//                                                                   @"isRooted",
//                                                                   @"isTncAccepted",    nil]];
//
//    //    public string UserSignUp(string strSessionId, string IP, string Device_ID, string strCNIC, string strDebitCardFirst, string strDebitCardLast, string cvv,
//    //        out string struserId, out string isEmail_Exist, out string strMobileNum, out string relationshipID, out string strReturnMessage, out string Token)
//
//    NSLog(@"%@",dictparam);
//
//    [manager.requestSerializer setTimeoutInterval:time_out];
//    [manager POST:@"UserSignUpRooted" parameters:dictparam progress:nil
//
//          success:^(NSURLSessionDataTask *task, id responseObject) {
//              NSError *error1=nil;
//              //NSArray *arr = (NSArray *)responseObject;
//
//              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//              //         login_status_chck=[dic objectForKey:@"Response"];
//              //
//
//              gblclass.arr_new_signup=[[NSMutableArray alloc] init];
//
//              //
//              //         //   if ([login_status_chck isEqualToString:@"0"])
//
//
//
//              if ([[dic objectForKey:@"Response"] integerValue] == 0)
//              {
//
//                //  [avplayer pause];
//
//                  gblclass.cnic_no = gblclass.txt_atmCard_signup;
//                  gblclass.frist_last_debt_no = gblclass.txt_atmCard_signup;
//
//                  gblclass.user_id=[dic objectForKey:@"struserId"];
//                  gblclass.isEmail_Exist=[dic objectForKey:@"isEmail_Exist"];
//                  gblclass.exis_user_mobile_pin=[dic objectForKey:@"strMobileNum"];
//                  gblclass.Exis_relation_id=[dic objectForKey:@"relationshipID"];
//                  gblclass.sign_up_token=[dic objectForKey:@"Token"];
//
//                  NSLog(@"%@",gblclass.Exis_relation_id);
//
//                  [self slideRight];
//                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"registration_type"];
//                  [self presentViewController:vc animated:NO completion:nil];
//
//
//                  //[self present_Controller];
//
//              } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-2"]) {
//
////                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
////
////                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
////                      [self slideLeft];
////                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
////                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
////                      [self presentViewController:vc animated:NO completion:nil];
////                  }];
////                  [alert addAction:ok];
////                  [self presentViewController:alert animated:YES completion:nil];
//
//                  [hud hideAnimated:YES];
//                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//
//              } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
//              {
//
//                  logout_chck = @"1";
//                  [hud hideAnimated:YES];
//                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//
//                  //                 [self slideLeft];
//                  //                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  //                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
//                  //                 [self presentViewController:vc animated:NO completion:nil];
//
//                  return ;
//
//              }
//              else
//              {
//
//                  [hud hideAnimated:YES];
//                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
//
////                  [hud hideAnimated:YES];
////                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//
//              }
//
//              [hud hideAnimated:YES];
//          }
//
//          failure:^(NSURLSessionDataTask *task, NSError *error) {
//              // [mine myfaildata];
//              [hud hideAnimated:YES];
//
//              [self custom_alert:@"Retry" :@"0"];
//
//          }];
//}


-(void) postWizRequest:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1 ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        WebMethod]
        //        public string PostWizRequest(Int64 strUserId, string strSessionId, string IP, string Device_ID, string Token,  bool cBterms, bool cBDeclare, string amount, string cmbPayFromSelectedItem, string cardTitle, string cmbPayFromAccount_no, string cmbPayFromAccountTitle, string otherBankPreCard, string cmbPayFromCurrency,string channel, out string wizRequestProfileNo, out string lblTransCharges,out string strNarration, out DataTable dtConfirmationLabels, out string strReturnMessage)
        //
        //
        //
        //        Sample Data:
        //        string strAmount = "500";
        //        string cmbPayFromSelectedItem = "2279625";
        //        string cardTitle = "MY CARD TITLE";
        //        string cmbPayFromAccount_no = "212549568";
        //        string cmbPayFromAccountTitle = "SHOAIB IQBAL";
        //
        //        string otherBankPreCard = "N/A";
        //        string cmbPayFromCurrency = "PKR";
        //        string strChannel = "MOBILE APP";  // discuss with bashir bhai
        //        string wizRequestProfileNo = "";
        //        string postWizReuest = "";
        //        string lblTransCharges = "";
        //        string strNarration = "";
        //        DataTable dtConfirmationLabels = new DataTable("dtConfirmationLabels");
        
        
        //    strUserId:
        //    strSessionId:
        //    IP:
        //    strDeviceid:
        //    Token:
        
        //    cBterms:
        //    cBDeclare:
        //    amount:
        //    cmbPayFromSelectedItem:
        //    cardTitle:
        //    cmbPayFromAccount_no:
        //    cmbPayFromAccountTitle:
        //    otherBankPreCard:
        //    cmbPayFromCurrency:
        //    Channel:
        
        t_n_c = [defaults valueForKey:@"t&c"];
        if ([t_n_c isEqualToString:@"2"]) {
            t_n_c = @"true";
        }
    
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:gblclass.wizLoadAmount], // [encrypt encrypt_Data:gblclass.wizLoadAmount],
                                    [encrypt encrypt_Data:gblclass.is_default_acct_id],
                                    [encrypt encrypt_Data:@"Virtual Wiz Card"],
                                    [encrypt encrypt_Data:gblclass.acc_number],
                                    [encrypt encrypt_Data:gblclass.acctitle],
                                    [encrypt encrypt_Data:gblclass.wizExistingCardDetails], // [encrypt encrypt_Data:txt_comment_without.text],
                                    [encrypt encrypt_Data:gblclass.base_currency],
                                    [encrypt encrypt_Data:@"MOBILE APP"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strDeviceid",
                                                                       @"Token",
                                                                       @"cBterms",
                                                                       @"cBDeclare",
                                                                       @"amount",
                                                                       @"cmbPayFromSelectedItem",
                                                                       @"cardTitle",
                                                                       @"cmbPayFromAccount_no",
                                                                       @"cmbPayFromAccountTitle",
                                                                       @"otherBankPreCard",
                                                                       @"cmbPayFromCurrency",
                                                                       @"Channel", nil]];
        
        
        
        ////NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"PostWizRequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  //                  {
                  //                      Response = 0;
                  //                      dtConfirmationLabels =     (
                  //                                                  {
                  //                                                      currency = PKR;
                  //                                                      lblActNum = 212549568;
                  //                                                      lblActTitle = "SHOAIB IQBAL NICK";
                  //                                                      lblAmount = 1000;
                  //                                                      lblBeniActNo = "";
                  //                                                      lblBeniActTitle = "";
                  //                                                      lblComments = "<null>";
                  //                                                      lblTransactionType = "WIZ_REQUEST";
                  //                                                      regAccount = 2279625;
                  //                                                  }
                  //                                                  );
                  //                      lblTransCharges = "UBL Wiz Prepaid Virtual Card issuance charges are Rs. 300 + FED.";
                  //                      strNarration = "You have requested to transfer PKR 1000from your A/C No. 212549568(SHOAIB IQBAL NICK)  as an initial load in your UBL Wiz Virtual Prepaid Card.";
                  //                      strReturnMessage = Successful;
                  //                      wizRequestProfileNo = 17543;
                  //                  }
                  
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                  }
                  
                  if ([[dic objectForKey:@"IsOtpRequired"] integerValue]==-1)
                  {
                      [hud hideAnimated:YES];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"OtpMessage"]] :@"0"];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      gblclass.wizRequestProfileNo = [dic objectForKey:@"wizRequestProfileNo"];
                      gblclass.wizTrasnctionType = [[[dic objectForKey:@"dtConfirmationLabels"] lastObject] objectForKey:@"lblTransactionType"];
                      gblclass.wizTransctionAmount = [dic objectForKey:@"lblTransCharges"];
                      
                      
                      [self slideRight];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"descriptionDetailsVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self.connection cancel];
                      [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
                      
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
    
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if ( [logout_chck isEqualToString:@"1"])
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
    }
    else
    {
        if (buttonIndex == 0)
        {
            if ([gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
            {
                gblclass.forget_pw_btn_click=@"1"; //0 saki 12 april 2019
                gblclass.forgot_pw_click = @"1";
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
                [self presentViewController:vc animated:YES completion:nil];
            }
            else  if ([chck_103 isEqualToString:@"1"])
            {
                if ([gblclass.str_processed_login_chk isEqualToString:@"1"])
                {
                    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"create_option"];
                    [self presentViewController:vc animated:NO completion:nil];
                }
                else
                {
                    gblclass.forget_pw_btn_click=@"1"; //0 saki 12 april 2019
                    gblclass.forgot_pw_click = @"1";
                    
                    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_pw"]; //new_signup
                    [self presentViewController:vc animated:NO completion:nil];
                }
            }
            else
            {
                storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
                [self presentViewController:vc animated:YES completion:nil];
            }
        }
        else if(buttonIndex == 1)
        {
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}

-(IBAction)btn_agree_ipad:(id)sender
{
    gblclass.chk_termNCondition = @"1";
    btn_chck_Image = [UIImage imageNamed:@"check.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled = YES;
    vw_tNc.hidden = YES;
    btn_resend_otp.enabled = YES;
    
    gblclass.TnC_chck_On = @"1";
    
   // NSLog(@"%@",gblclass.isDeviceJailBreaked);
    if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"])
    {
        // defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"2"  forKey:@"isDeviceJailBreaked"];
        [defaults synchronize];
    }
    
    [defaults setValue:@"2" forKey:@"t&c"];
    [defaults synchronize];
    
    //    NSLog(@"%@",gblclass.isDeviceJailBreaked);
    //    NSLog(@"%@",gblclass.term_conditn_chk_place);
    
    
    //29 june 2018 saki
    //    if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login"])
    //    {
    //
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
    //    {
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"login_new"])
    //    {
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
    //    {
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"0"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic_otp"])
    //    {
    //        //SignUpOTPViewController
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup_otp"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else  if ([gblclass.isDeviceJailBreaked isEqualToString:@"1"] && [gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
    //    {
    //        //SignUpOTPViewController
    //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:@"new_signup"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //
    //        //        [self dismissViewControllerAnimated:NO completion:nil];
    //
    //        CATransition *transition = [CATransition animation];
    //        transition.duration = 0.3;
    //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //        transition.type = kCATransitionPush;
    //        transition.subtype = kCATransitionFromRight;
    //        [ self.view.window.layer addAnimation:transition forKey:nil];
    //
    //    }
    //    else
    //    {
    
    
   // NSLog(@"%@",gblclass.jb_chk_login_back);
    
    if ([gblclass.term_conditn_chk_place isEqualToString:@"sign_with_cnic"])
    {
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl = @"signup";
            [self SSL_Call];
        }
    }
    else if([gblclass.jb_chk_login_back isEqualToString:@"1"])
    {
        [hud hideAnimated:YES];
        gblclass.jb_chk_login_back = @"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        [hud showAnimated:YES];
        if ([gblclass.signUp_through_cnic isEqualToString:@"1"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl = @"signup";
                [self SSL_Call];
            }
        }
        else
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl = @"next";
                [self SSL_Call];
            }
        }
        
        
        //      }
        
        
        
        //29 june 2018 saki
        //        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        vc = [storyboard instantiateViewControllerWithIdentifier:@"verification_acc"];
        //        [self presentViewController:vc animated:NO completion:nil];
        //
        //        //        [self dismissViewControllerAnimated:NO completion:nil];
        //
        //        CATransition *transition = [CATransition animation];
        //        transition.duration = 0.3;
        //        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        //        transition.type = kCATransitionPush;
        //        transition.subtype = kCATransitionFromRight;
        //        [ self.view.window.layer addAnimation:transition forKey:nil];
        
    }
}

-(IBAction)btn_dis_agree_ipad:(id)sender
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    gblclass.chk_termNCondition=@"0";
    btn_chck_Image = [UIImage imageNamed:@"uncheck.png"];
    [btn_check_JB setImage:btn_chck_Image forState:UIControlStateNormal];
    btn_next.enabled=NO;
    vw_tNc.hidden=YES;
    btn_resend_otp.enabled=NO;
    
    gblclass.TnC_chck_On=@"0";
    
    [defaults setValue:@"0" forKey:@"t&c"];
    [defaults synchronize];
    
    [hud showAnimated:YES];
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"disagree";
        [self SSL_Call];
    }
    
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    //    [self presentViewController:vc animated:NO completion:nil];
    //
    //    CATransition *transition = [CATransition animation];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromRight;
    //    [ self.view.window.layer addAnimation:transition forKey:nil];
}

@end

