//
//  PayeePayDetail.h
//
//  Created by   on 18/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PayeePayDetail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *aCCOUNTNAME;
@property (nonatomic, strong) NSString *tTNAME;
@property (nonatomic, strong) NSString *lASTPAYDATE;
@property (nonatomic, strong) NSString *dEBITABLEACCTYPES;
@property (nonatomic, strong) NSString *bANKIMD;
@property (nonatomic, strong) NSString *aCCOUNTNO;
@property (nonatomic, assign) id rELATIONSHIPBENEFICIARY;
@property (nonatomic, assign) double bRANCHCODE;
@property (nonatomic, assign) id eMAIL;
@property (nonatomic, assign) id bENEFICIARYID;
@property (nonatomic, strong) NSString *bANKNAME;
@property (nonatomic, assign) double uSERACCOUNTID;
@property (nonatomic, assign) id cDG;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
