//
//  CustomTransitionVController.m
//  CustomTransition
//
//  Created by Mehmood on 22/11/2016.
//  Copyright © 2016 M3Tech. All rights reserved.
//

#import "CustomTransitionVController.h"
#import "GlobalStaticClass.h"

@implementation CustomTransitionVController


- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext{
    
    if ([_type isEqual:@"dismiss"]) {
        
        return 0.6;
    }
    
    return 1;
    
}



//let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
//let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
//let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
//let containerView = transitionContext.containerView()
//let bounds = UIScreen.mainScreen().bounds
//toViewController.view.frame = CGRectOffset(finalFrameForVC, 0, bounds.size.height)
//containerView.addSubview(toViewController.view)
//
//UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: .CurveLinear, animations: {
//    fromViewController.view.alpha = 0.5
//    toViewController.view.frame = finalFrameForVC
//}, completion: {
//    finished in
//    transitionContext.completeTransition(true)
//    fromViewController.view.alpha = 1.0
//})

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    

    
    
    id from = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *to = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    CGRect finalFrame = [transitionContext finalFrameForViewController:to];
    id container = [transitionContext containerView];
    //CGRect bounds = [[UIScreen mainScreen] bounds];
    
    
    
    if (![self.type isEqual:@"dismiss"]) {

    to.view.frame = CGRectMake(0, _centerPoint.y- 23 , 48, 46);
    
    [container addSubview:to.view];
        
        [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            //((UIViewController*)from).view.alpha = 0.5;
            to.view.frame = finalFrame;
            
            
        } completion:^(BOOL finished) {
            
            [transitionContext completeTransition:YES];
            //((UIViewController*)from).view.alpha = 1;
            
        }];

    
        
    }
    
    else {
        
        
        GlobalStaticClass *sharedManager = [GlobalStaticClass getInstance];
        
        
        [container addSubview:to.view];
        //[((UIViewController*)from).view.subviews setValue:@YES forKeyPath:@"alpha"];
        
        UIView *snapshot =  [((UIViewController*)from).view snapshotViewAfterScreenUpdates:NO];
        snapshot.frame = ((UIViewController*)from).view.frame;
        
        [container addSubview:snapshot];
        //to.view.alpha = 0;
        
        
        [UIView animateWithDuration:0.5 animations:^{
            //((UIViewController*)from).view.alpha = 0.5;
            //[snapshot.subviews setValue:@YES forKeyPath:@"alpha"];
             snapshot.frame = CGRectMake(0, sharedManager.centrePoint.y - 23, 48 , 46);
            
        } completion:^(BOOL finished) {
           // ((UIViewController*)from).view.alpha = 0;
            [transitionContext completeTransition:YES];
            
        }];
        
//        [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
//            
//            //((UIViewController*)from).view.alpha = 0.5;
//           
//            ((UIViewController*)from).view.alpha = 0;
//            //to.view.frame = finalFrame;
//           // to.view.alpha = 0.5;
//            
//            
//        } completion:^(BOOL finished) {
//            
//            
//            [transitionContext completeTransition:YES];
//            //((UIViewController*)from).view.alpha = 0;
//            
//            //to.view.alpha = 1;
//            
//        }];

    
    }
    
    
    
    
}


@end

