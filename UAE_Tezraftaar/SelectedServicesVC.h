//
//  SelectedServicesVC.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 04/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SelectedServicesVC : UIViewController <CLLocationManagerDelegate>

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *tableData;

+(NSMutableArray *)getTableData;


@end

