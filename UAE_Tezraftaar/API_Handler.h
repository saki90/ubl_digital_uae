//
//  API_Handler.h
//  ubltestbanking
//
//  Created by Basit on 16/01/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ObjectBlock)(id responseObject);
typedef void (^ErrorBlock)(NSError* error);
typedef void (^ErrorBlockDescription)(NSString* errorStr);

@interface API_Handler : NSObject

+(void)get_branches:(NSDictionary*)params onCompletionBlock:(ObjectBlock)completionBlock onerror:(ErrorBlock)errorblock;

@end
