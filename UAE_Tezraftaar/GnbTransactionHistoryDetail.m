//
//  GnbTransactionHistoryDetail.m
//
//  Created by   on 23/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbTransactionHistoryDetail.h"


NSString *const kGnbTransactionHistoryDetailLblToAccNick = @"lblToAccNick";
NSString *const kGnbTransactionHistoryDetailLblSerialNo = @"lblSerialNo";
NSString *const kGnbTransactionHistoryDetailDlblReceivedBy = @"dlblReceivedBy";
NSString *const kGnbTransactionHistoryDetailLblToAcc = @"lblToAcc";
NSString *const kGnbTransactionHistoryDetailDlblStatus = @"dlblStatus";
NSString *const kGnbTransactionHistoryDetailLblFromAcc = @"lblFromAcc";
NSString *const kGnbTransactionHistoryDetailLblAmount = @"lblAmount";
NSString *const kGnbTransactionHistoryDetailDlblTranID = @"dlblTranID";
NSString *const kGnbTransactionHistoryDetailDlblAmount = @"dlblAmount";
NSString *const kGnbTransactionHistoryDetailLblFCPVouchertId = @"lblFCPVouchertId";
NSString *const kGnbTransactionHistoryDetailLblReceivedBy = @"lblReceivedBy";
NSString *const kGnbTransactionHistoryDetailDlblDepositedTo = @"dlblDepositedTo";
NSString *const kGnbTransactionHistoryDetailDlblBank = @"dlblBank";
NSString *const kGnbTransactionHistoryDetailDlblTranType = @"dlblTranType";
NSString *const kGnbTransactionHistoryDetailLblBank = @"lblBank";
NSString *const kGnbTransactionHistoryDetailDlblFCPVouchertId = @"dlblFCPVouchertId";
NSString *const kGnbTransactionHistoryDetailLblDepositedTo = @"lblDepositedTo";
NSString *const kGnbTransactionHistoryDetailDlblToAcc = @"dlblToAcc";
NSString *const kGnbTransactionHistoryDetailLblTranType = @"lblTranType";
NSString *const kGnbTransactionHistoryDetailLblStatus = @"lblStatus";
NSString *const kGnbTransactionHistoryDetailDlblVoucherExpDate = @"dlblVoucherExpDate";
NSString *const kGnbTransactionHistoryDetailLblTranID = @"lblTranID";
NSString *const kGnbTransactionHistoryDetailLblComments = @"lblComments";
NSString *const kGnbTransactionHistoryDetailDlblComments = @"dlblComments";
NSString *const kGnbTransactionHistoryDetailLblVoucherExpDate = @"lblVoucherExpDate";
NSString *const kGnbTransactionHistoryDetailDlblSerialNo = @"dlblSerialNo";
NSString *const kGnbTransactionHistoryDetailLblInitByCustomer = @"lblInitByCustomer";
NSString *const kGnbTransactionHistoryDetailDlblInitByCustomer = @"dlblInitByCustomer";
NSString *const kGnbTransactionHistoryDetailDlblToAccNick = @"dlblToAccNick";
NSString *const kGnbTransactionHistoryDetailDlblFromAcc = @"dlblFromAcc";


@interface GnbTransactionHistoryDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbTransactionHistoryDetail

@synthesize lblToAccNick = _lblToAccNick;
@synthesize lblSerialNo = _lblSerialNo;
@synthesize dlblReceivedBy = _dlblReceivedBy;
@synthesize lblToAcc = _lblToAcc;
@synthesize dlblStatus = _dlblStatus;
@synthesize lblFromAcc = _lblFromAcc;
@synthesize lblAmount = _lblAmount;
@synthesize dlblTranID = _dlblTranID;
@synthesize dlblAmount = _dlblAmount;
@synthesize lblFCPVouchertId = _lblFCPVouchertId;
@synthesize lblReceivedBy = _lblReceivedBy;
@synthesize dlblDepositedTo = _dlblDepositedTo;
@synthesize dlblBank = _dlblBank;
@synthesize dlblTranType = _dlblTranType;
@synthesize lblBank = _lblBank;
@synthesize dlblFCPVouchertId = _dlblFCPVouchertId;
@synthesize lblDepositedTo = _lblDepositedTo;
@synthesize dlblToAcc = _dlblToAcc;
@synthesize lblTranType = _lblTranType;
@synthesize lblStatus = _lblStatus;
@synthesize dlblVoucherExpDate = _dlblVoucherExpDate;
@synthesize lblTranID = _lblTranID;
@synthesize lblComments = _lblComments;
@synthesize dlblComments = _dlblComments;
@synthesize lblVoucherExpDate = _lblVoucherExpDate;
@synthesize dlblSerialNo = _dlblSerialNo;
@synthesize lblInitByCustomer = _lblInitByCustomer;
@synthesize dlblInitByCustomer = _dlblInitByCustomer;
@synthesize dlblToAccNick = _dlblToAccNick;
@synthesize dlblFromAcc = _dlblFromAcc;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.lblToAccNick = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblToAccNick fromDictionary:dict];
            self.lblSerialNo = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblSerialNo fromDictionary:dict];
            self.dlblReceivedBy = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblReceivedBy fromDictionary:dict];
            self.lblToAcc = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblToAcc fromDictionary:dict];
            self.dlblStatus = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblStatus fromDictionary:dict];
            self.lblFromAcc = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblFromAcc fromDictionary:dict];
            self.lblAmount = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblAmount fromDictionary:dict];
            self.dlblTranID = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblTranID fromDictionary:dict];
            self.dlblAmount = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblAmount fromDictionary:dict];
            self.lblFCPVouchertId = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblFCPVouchertId fromDictionary:dict];
            self.lblReceivedBy = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblReceivedBy fromDictionary:dict];
            self.dlblDepositedTo = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblDepositedTo fromDictionary:dict];
            self.dlblBank = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblBank fromDictionary:dict];
            self.dlblTranType = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblTranType fromDictionary:dict];
            self.lblBank = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblBank fromDictionary:dict];
            self.dlblFCPVouchertId = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblFCPVouchertId fromDictionary:dict];
            self.lblDepositedTo = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblDepositedTo fromDictionary:dict];
            self.dlblToAcc = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblToAcc fromDictionary:dict];
            self.lblTranType = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblTranType fromDictionary:dict];
            self.lblStatus = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblStatus fromDictionary:dict];
            self.dlblVoucherExpDate = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblVoucherExpDate fromDictionary:dict];
            self.lblTranID = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblTranID fromDictionary:dict];
            self.lblComments = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblComments fromDictionary:dict];
            self.dlblComments = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblComments fromDictionary:dict];
            self.lblVoucherExpDate = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblVoucherExpDate fromDictionary:dict];
            self.dlblSerialNo = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblSerialNo fromDictionary:dict];
            self.lblInitByCustomer = [self objectOrNilForKey:kGnbTransactionHistoryDetailLblInitByCustomer fromDictionary:dict];
            self.dlblInitByCustomer = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblInitByCustomer fromDictionary:dict];
            self.dlblToAccNick = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblToAccNick fromDictionary:dict];
            self.dlblFromAcc = [self objectOrNilForKey:kGnbTransactionHistoryDetailDlblFromAcc fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.lblToAccNick forKey:kGnbTransactionHistoryDetailLblToAccNick];
    [mutableDict setValue:self.lblSerialNo forKey:kGnbTransactionHistoryDetailLblSerialNo];
    [mutableDict setValue:self.dlblReceivedBy forKey:kGnbTransactionHistoryDetailDlblReceivedBy];
    [mutableDict setValue:self.lblToAcc forKey:kGnbTransactionHistoryDetailLblToAcc];
    [mutableDict setValue:self.dlblStatus forKey:kGnbTransactionHistoryDetailDlblStatus];
    [mutableDict setValue:self.lblFromAcc forKey:kGnbTransactionHistoryDetailLblFromAcc];
    [mutableDict setValue:self.lblAmount forKey:kGnbTransactionHistoryDetailLblAmount];
    [mutableDict setValue:self.dlblTranID forKey:kGnbTransactionHistoryDetailDlblTranID];
    [mutableDict setValue:self.dlblAmount forKey:kGnbTransactionHistoryDetailDlblAmount];
    [mutableDict setValue:self.lblFCPVouchertId forKey:kGnbTransactionHistoryDetailLblFCPVouchertId];
    [mutableDict setValue:self.lblReceivedBy forKey:kGnbTransactionHistoryDetailLblReceivedBy];
    [mutableDict setValue:self.dlblDepositedTo forKey:kGnbTransactionHistoryDetailDlblDepositedTo];
    [mutableDict setValue:self.dlblBank forKey:kGnbTransactionHistoryDetailDlblBank];
    [mutableDict setValue:self.dlblTranType forKey:kGnbTransactionHistoryDetailDlblTranType];
    [mutableDict setValue:self.lblBank forKey:kGnbTransactionHistoryDetailLblBank];
    [mutableDict setValue:self.dlblFCPVouchertId forKey:kGnbTransactionHistoryDetailDlblFCPVouchertId];
    [mutableDict setValue:self.lblDepositedTo forKey:kGnbTransactionHistoryDetailLblDepositedTo];
    [mutableDict setValue:self.dlblToAcc forKey:kGnbTransactionHistoryDetailDlblToAcc];
    [mutableDict setValue:self.lblTranType forKey:kGnbTransactionHistoryDetailLblTranType];
    [mutableDict setValue:self.lblStatus forKey:kGnbTransactionHistoryDetailLblStatus];
    [mutableDict setValue:self.dlblVoucherExpDate forKey:kGnbTransactionHistoryDetailDlblVoucherExpDate];
    [mutableDict setValue:self.lblTranID forKey:kGnbTransactionHistoryDetailLblTranID];
    [mutableDict setValue:self.lblComments forKey:kGnbTransactionHistoryDetailLblComments];
    [mutableDict setValue:self.dlblComments forKey:kGnbTransactionHistoryDetailDlblComments];
    [mutableDict setValue:self.lblVoucherExpDate forKey:kGnbTransactionHistoryDetailLblVoucherExpDate];
    [mutableDict setValue:self.dlblSerialNo forKey:kGnbTransactionHistoryDetailDlblSerialNo];
    [mutableDict setValue:self.lblInitByCustomer forKey:kGnbTransactionHistoryDetailLblInitByCustomer];
    [mutableDict setValue:self.dlblInitByCustomer forKey:kGnbTransactionHistoryDetailDlblInitByCustomer];
    [mutableDict setValue:self.dlblToAccNick forKey:kGnbTransactionHistoryDetailDlblToAccNick];
    [mutableDict setValue:self.dlblFromAcc forKey:kGnbTransactionHistoryDetailDlblFromAcc];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.lblToAccNick = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblToAccNick];
    self.lblSerialNo = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblSerialNo];
    self.dlblReceivedBy = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblReceivedBy];
    self.lblToAcc = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblToAcc];
    self.dlblStatus = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblStatus];
    self.lblFromAcc = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblFromAcc];
    self.lblAmount = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblAmount];
    self.dlblTranID = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblTranID];
    self.dlblAmount = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblAmount];
    self.lblFCPVouchertId = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblFCPVouchertId];
    self.lblReceivedBy = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblReceivedBy];
    self.dlblDepositedTo = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblDepositedTo];
    self.dlblBank = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblBank];
    self.dlblTranType = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblTranType];
    self.lblBank = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblBank];
    self.dlblFCPVouchertId = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblFCPVouchertId];
    self.lblDepositedTo = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblDepositedTo];
    self.dlblToAcc = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblToAcc];
    self.lblTranType = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblTranType];
    self.lblStatus = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblStatus];
    self.dlblVoucherExpDate = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblVoucherExpDate];
    self.lblTranID = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblTranID];
    self.lblComments = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblComments];
    self.dlblComments = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblComments];
    self.lblVoucherExpDate = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblVoucherExpDate];
    self.dlblSerialNo = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblSerialNo];
    self.lblInitByCustomer = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailLblInitByCustomer];
    self.dlblInitByCustomer = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblInitByCustomer];
    self.dlblToAccNick = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblToAccNick];
    self.dlblFromAcc = [aDecoder decodeObjectForKey:kGnbTransactionHistoryDetailDlblFromAcc];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_lblToAccNick forKey:kGnbTransactionHistoryDetailLblToAccNick];
    [aCoder encodeObject:_lblSerialNo forKey:kGnbTransactionHistoryDetailLblSerialNo];
    [aCoder encodeObject:_dlblReceivedBy forKey:kGnbTransactionHistoryDetailDlblReceivedBy];
    [aCoder encodeObject:_lblToAcc forKey:kGnbTransactionHistoryDetailLblToAcc];
    [aCoder encodeObject:_dlblStatus forKey:kGnbTransactionHistoryDetailDlblStatus];
    [aCoder encodeObject:_lblFromAcc forKey:kGnbTransactionHistoryDetailLblFromAcc];
    [aCoder encodeObject:_lblAmount forKey:kGnbTransactionHistoryDetailLblAmount];
    [aCoder encodeObject:_dlblTranID forKey:kGnbTransactionHistoryDetailDlblTranID];
    [aCoder encodeObject:_dlblAmount forKey:kGnbTransactionHistoryDetailDlblAmount];
    [aCoder encodeObject:_lblFCPVouchertId forKey:kGnbTransactionHistoryDetailLblFCPVouchertId];
    [aCoder encodeObject:_lblReceivedBy forKey:kGnbTransactionHistoryDetailLblReceivedBy];
    [aCoder encodeObject:_dlblDepositedTo forKey:kGnbTransactionHistoryDetailDlblDepositedTo];
    [aCoder encodeObject:_dlblBank forKey:kGnbTransactionHistoryDetailDlblBank];
    [aCoder encodeObject:_dlblTranType forKey:kGnbTransactionHistoryDetailDlblTranType];
    [aCoder encodeObject:_lblBank forKey:kGnbTransactionHistoryDetailLblBank];
    [aCoder encodeObject:_dlblFCPVouchertId forKey:kGnbTransactionHistoryDetailDlblFCPVouchertId];
    [aCoder encodeObject:_lblDepositedTo forKey:kGnbTransactionHistoryDetailLblDepositedTo];
    [aCoder encodeObject:_dlblToAcc forKey:kGnbTransactionHistoryDetailDlblToAcc];
    [aCoder encodeObject:_lblTranType forKey:kGnbTransactionHistoryDetailLblTranType];
    [aCoder encodeObject:_lblStatus forKey:kGnbTransactionHistoryDetailLblStatus];
    [aCoder encodeObject:_dlblVoucherExpDate forKey:kGnbTransactionHistoryDetailDlblVoucherExpDate];
    [aCoder encodeObject:_lblTranID forKey:kGnbTransactionHistoryDetailLblTranID];
    [aCoder encodeObject:_lblComments forKey:kGnbTransactionHistoryDetailLblComments];
    [aCoder encodeObject:_dlblComments forKey:kGnbTransactionHistoryDetailDlblComments];
    [aCoder encodeObject:_lblVoucherExpDate forKey:kGnbTransactionHistoryDetailLblVoucherExpDate];
    [aCoder encodeObject:_dlblSerialNo forKey:kGnbTransactionHistoryDetailDlblSerialNo];
    [aCoder encodeObject:_lblInitByCustomer forKey:kGnbTransactionHistoryDetailLblInitByCustomer];
    [aCoder encodeObject:_dlblInitByCustomer forKey:kGnbTransactionHistoryDetailDlblInitByCustomer];
    [aCoder encodeObject:_dlblToAccNick forKey:kGnbTransactionHistoryDetailDlblToAccNick];
    [aCoder encodeObject:_dlblFromAcc forKey:kGnbTransactionHistoryDetailDlblFromAcc];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbTransactionHistoryDetail *copy = [[GnbTransactionHistoryDetail alloc] init];
    
    if (copy) {

        copy.lblToAccNick = [self.lblToAccNick copyWithZone:zone];
        copy.lblSerialNo = [self.lblSerialNo copyWithZone:zone];
        copy.dlblReceivedBy = [self.dlblReceivedBy copyWithZone:zone];
        copy.lblToAcc = [self.lblToAcc copyWithZone:zone];
        copy.dlblStatus = [self.dlblStatus copyWithZone:zone];
        copy.lblFromAcc = [self.lblFromAcc copyWithZone:zone];
        copy.lblAmount = [self.lblAmount copyWithZone:zone];
        copy.dlblTranID = [self.dlblTranID copyWithZone:zone];
        copy.dlblAmount = [self.dlblAmount copyWithZone:zone];
        copy.lblFCPVouchertId = [self.lblFCPVouchertId copyWithZone:zone];
        copy.lblReceivedBy = [self.lblReceivedBy copyWithZone:zone];
        copy.dlblDepositedTo = [self.dlblDepositedTo copyWithZone:zone];
        copy.dlblBank = [self.dlblBank copyWithZone:zone];
        copy.dlblTranType = [self.dlblTranType copyWithZone:zone];
        copy.lblBank = [self.lblBank copyWithZone:zone];
        copy.dlblFCPVouchertId = [self.dlblFCPVouchertId copyWithZone:zone];
        copy.lblDepositedTo = [self.lblDepositedTo copyWithZone:zone];
        copy.dlblToAcc = [self.dlblToAcc copyWithZone:zone];
        copy.lblTranType = [self.lblTranType copyWithZone:zone];
        copy.lblStatus = [self.lblStatus copyWithZone:zone];
        copy.dlblVoucherExpDate = [self.dlblVoucherExpDate copyWithZone:zone];
        copy.lblTranID = [self.lblTranID copyWithZone:zone];
        copy.lblComments = [self.lblComments copyWithZone:zone];
        copy.dlblComments = [self.dlblComments copyWithZone:zone];
        copy.lblVoucherExpDate = [self.lblVoucherExpDate copyWithZone:zone];
        copy.dlblSerialNo = [self.dlblSerialNo copyWithZone:zone];
        copy.lblInitByCustomer = [self.lblInitByCustomer copyWithZone:zone];
        copy.dlblInitByCustomer = [self.dlblInitByCustomer copyWithZone:zone];
        copy.dlblToAccNick = [self.dlblToAccNick copyWithZone:zone];
        copy.dlblFromAcc = [self.dlblFromAcc copyWithZone:zone];
    }
    
    return copy;
}


@end
