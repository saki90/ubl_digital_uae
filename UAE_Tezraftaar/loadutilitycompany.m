//
//  Table.m
//
//  Created by   on 03/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "loadutilitycompany.h"


NSString *const kTableTTID = @"TT_ID";
NSString *const kTableMERCHANTID = @"MERCHANT_ID";
NSString *const kTableCOMPANYNAME = @"COMPANY_NAME";
NSString *const kTableTCACCESSKEY = @"TC_ACCESS_KEY";
NSString *const kTablePAYTYPE = @"PAY_TYPE";
NSString *const kTableLENGTHMAX = @"LENGTH_MAX";
NSString *const kTableLENGTHMIN = @"LENGTH_MIN";
NSString *const kTableNAME = @"NAME";
NSString *const kTableTTACCESSKEY = @"TT_ACCESS_KEY";
NSString *const kTableSHOWCONSUMERNO = @"SHOW_CONSUMER_NO";


@interface loadutilitycompany ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation loadutilitycompany

@synthesize tTID = _tTID;
@synthesize mERCHANTID = _mERCHANTID;
@synthesize cOMPANYNAME = _cOMPANYNAME;
@synthesize tCACCESSKEY = _tCACCESSKEY;
@synthesize pAYTYPE = _pAYTYPE;
@synthesize lENGTHMAX = _lENGTHMAX;
@synthesize lENGTHMIN = _lENGTHMIN;
@synthesize nAME = _nAME;
@synthesize tTACCESSKEY = _tTACCESSKEY;
@synthesize sHOWCONSUMERNO = _sHOWCONSUMERNO;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tTID = [[self objectOrNilForKey:kTableTTID fromDictionary:dict] doubleValue];
            self.mERCHANTID = [self objectOrNilForKey:kTableMERCHANTID fromDictionary:dict];
            self.cOMPANYNAME = [self objectOrNilForKey:kTableCOMPANYNAME fromDictionary:dict];
            self.tCACCESSKEY = [self objectOrNilForKey:kTableTCACCESSKEY fromDictionary:dict];
            self.pAYTYPE = [self objectOrNilForKey:kTablePAYTYPE fromDictionary:dict];
            self.lENGTHMAX = [[self objectOrNilForKey:kTableLENGTHMAX fromDictionary:dict] doubleValue];
            self.lENGTHMIN = [[self objectOrNilForKey:kTableLENGTHMIN fromDictionary:dict] doubleValue];
            self.nAME = [self objectOrNilForKey:kTableNAME fromDictionary:dict];
            self.tTACCESSKEY = [self objectOrNilForKey:kTableTTACCESSKEY fromDictionary:dict];
            self.sHOWCONSUMERNO = [self objectOrNilForKey:kTableSHOWCONSUMERNO fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.tTID] forKey:kTableTTID];
    [mutableDict setValue:self.mERCHANTID forKey:kTableMERCHANTID];
    [mutableDict setValue:self.cOMPANYNAME forKey:kTableCOMPANYNAME];
    [mutableDict setValue:self.tCACCESSKEY forKey:kTableTCACCESSKEY];
    [mutableDict setValue:self.pAYTYPE forKey:kTablePAYTYPE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lENGTHMAX] forKey:kTableLENGTHMAX];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lENGTHMIN] forKey:kTableLENGTHMIN];
    [mutableDict setValue:self.nAME forKey:kTableNAME];
    [mutableDict setValue:self.tTACCESSKEY forKey:kTableTTACCESSKEY];
    [mutableDict setValue:self.sHOWCONSUMERNO forKey:kTableSHOWCONSUMERNO];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tTID = [aDecoder decodeDoubleForKey:kTableTTID];
    self.mERCHANTID = [aDecoder decodeObjectForKey:kTableMERCHANTID];
    self.cOMPANYNAME = [aDecoder decodeObjectForKey:kTableCOMPANYNAME];
    self.tCACCESSKEY = [aDecoder decodeObjectForKey:kTableTCACCESSKEY];
    self.pAYTYPE = [aDecoder decodeObjectForKey:kTablePAYTYPE];
    self.lENGTHMAX = [aDecoder decodeDoubleForKey:kTableLENGTHMAX];
    self.lENGTHMIN = [aDecoder decodeDoubleForKey:kTableLENGTHMIN];
    self.nAME = [aDecoder decodeObjectForKey:kTableNAME];
    self.tTACCESSKEY = [aDecoder decodeObjectForKey:kTableTTACCESSKEY];
    self.sHOWCONSUMERNO = [aDecoder decodeObjectForKey:kTableSHOWCONSUMERNO];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_tTID forKey:kTableTTID];
    [aCoder encodeObject:_mERCHANTID forKey:kTableMERCHANTID];
    [aCoder encodeObject:_cOMPANYNAME forKey:kTableCOMPANYNAME];
    [aCoder encodeObject:_tCACCESSKEY forKey:kTableTCACCESSKEY];
    [aCoder encodeObject:_pAYTYPE forKey:kTablePAYTYPE];
    [aCoder encodeDouble:_lENGTHMAX forKey:kTableLENGTHMAX];
    [aCoder encodeDouble:_lENGTHMIN forKey:kTableLENGTHMIN];
    [aCoder encodeObject:_nAME forKey:kTableNAME];
    [aCoder encodeObject:_tTACCESSKEY forKey:kTableTTACCESSKEY];
    [aCoder encodeObject:_sHOWCONSUMERNO forKey:kTableSHOWCONSUMERNO];
}

- (id)copyWithZone:(NSZone *)zone
{
    loadutilitycompany *copy = [[loadutilitycompany alloc] init];
    
    if (copy) {

        copy.tTID = self.tTID;
        copy.mERCHANTID = [self.mERCHANTID copyWithZone:zone];
        copy.cOMPANYNAME = [self.cOMPANYNAME copyWithZone:zone];
        copy.tCACCESSKEY = [self.tCACCESSKEY copyWithZone:zone];
        copy.pAYTYPE = [self.pAYTYPE copyWithZone:zone];
        copy.lENGTHMAX = self.lENGTHMAX;
        copy.lENGTHMIN = self.lENGTHMIN;
        copy.nAME = [self.nAME copyWithZone:zone];
        copy.tTACCESSKEY = [self.tTACCESSKEY copyWithZone:zone];
        copy.sHOWCONSUMERNO = [self.sHOWCONSUMERNO copyWithZone:zone];
    }
    
    return copy;
}


@end
