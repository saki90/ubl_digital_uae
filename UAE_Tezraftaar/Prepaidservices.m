//
//  AddPayee.m
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Prepaidservices.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
//#import "Loadbranches.h"
//#import "LoadIBFTBanks.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Prepaidservices ()<NSURLConnectionDataDelegate>

{
    //NSMutableArray* bill_service;
    NSMutableArray* bill_type;
    NSMutableArray* companytype;
    NSInteger selectedrowpicker;
    NSMutableArray* ttid,*ttacceskey,*str_TCAccess_Key;
    NSMutableArray* name,*branchcode;
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    
    //fet title
    //NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    NSString*  strTtId,*strAccessKey,*strType,*strtxtNick,*strtxtCustomerID;
    
    UIStoryboard *storyboard;
    UIViewController *vc;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* tc_access_key;
    NSString* str_account_no;
    NSString* btn_wiz_chk;
    int Max_Length, Min_Length;
    NSMutableArray* field_condition;
    NSMutableArray* a;
    NSArray* split;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Prepaidservices
@synthesize txtbillservice;
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    numberFormatter = [NSNumberFormatter new];
    //    [numberFormatter setMinimumFractionDigits:2];
    //    [numberFormatter setMaximumFractionDigits:2];
    //    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    // bill_service = [[NSMutableArray alloc]init];
    
    gblclass =  [GlobalStaticClass getInstance];
    gblclass.arr_re_genrte_OTP_additn=[[NSMutableArray alloc] init];
    encrypt = [[Encrypt alloc] init];
    
    bill_type = [[NSMutableArray alloc]init];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    a=[[NSMutableArray alloc] init];
    self.tableviewcompany.hidden=YES;
    vw_table.hidden=YES;
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    //bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    //bill_type=@[@"Mobile Topup",@"Prepaid Vouchers",@"Wiz Topup"];
    
    //    NSString* subStr;
    
    ssl_count = @"0";
    Max_Length=0;
    Min_Length=0;
    
    str_TCAccess_Key=[[NSMutableArray alloc] init];
    gblclass.arr_values=[[NSMutableArray alloc] init];
    _txtphonenumber.delegate=self;
    _txtnick.delegate=self;
    btn_wiz_chk=@"0";
    
    //bill_type=@[@"Mobile Topup",@"Prepaid Vouchers"];
    //  bill_type=@[@"Mobile Topup",@"Prepaid Vouchers"];
    
    bill_type=[[NSMutableArray alloc] initWithArray:@[@"Mobile Topup",@"Prepaid Vouchers"]];
    
    self.txtphonenumber.delegate=self;
    // self.billtype.delgate=self;
    
//    [APIdleManager sharedInstance].onTimeout = ^(void){
//        //  [self.timeoutLabel  setText:@"YES"];
//        
//        
//        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
//        
//        storyboard = [UIStoryboard storyboardWithName:
//                      gblclass.story_board bundle:[NSBundle mainBundle]];
//        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
//        
//        [self presentViewController:vc animated:YES completion:nil];
//        
//        APIdleManager * cc1=[[APIdleManager alloc] init];
//        // cc.createTimer;
//        
//        [cc1 timme_invaletedd];
//        
//    };
    
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    //NSLog(@"%@",gblclass.bill_type);
    
    txt_phonenumber.delegate=self;
    txt_nick.delegate=self;
    
    self.txtcompany.delegate=self;
    self.txtnick.delegate=self;
    self.txtphonenumber.delegate=self;
    
    self.txtcompany.text=@"";
    self.txtnick.text=@"";
    self.txtphonenumber.text=@"";
    
    // _txtretypephonenumber.text=@"";
    
    strTtId=@"";
    strAccessKey=@"";
    strType=@"";
    strtxtNick=@"";
    strtxtCustomerID=@"";
    
    //  self.txtbilltype.text=[bill_type objectAtIndex:indexPath.row];
    // NSString* txttypebillvalue = self.txtbilltype.text;
    
    
    // UITextField *textFieldPhone = [[UITextField alloc] initWithFrame:CGRectMake(20, 150, 260, 30) labelHeight:15];
    
    
    /////////Mobile BIll
    //NSLog(@"%@",gblclass.bill_click);
    
    vw_wiz_topup.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_wiz_topup.hidden=YES;
    
    lbl_heading.text=[gblclass.bill_type uppercaseString];
    if([gblclass.bill_click isEqualToString:@"Mobile TopUp"])
    {
        img_arrow.hidden=NO;
        btn_combo_box.enabled=YES;
        
        lbl_header_tble_name.text=@"MOBILE TOP-UP COMPANIES";
        
        self.txtbilltype.text=@"Mobile Topup";
        //NSLog(@"Mobile Topup");
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"mobile";
            [self SSL_Call];
            
            //[self GetBillCompaniesDetail:@""];
        }
    }
    else if ([gblclass.bill_click isEqualToString:@"Prepaid Vouchers"])
    {
        img_arrow.hidden=NO;
        btn_combo_box.enabled=YES;
        
        lbl_header_tble_name.text=@"Company";
        
        
        self.txtbilltype.text=@"Prepaid Vouchers";
        _txtphonenumber.placeholder=@"Enter Voucher Nick";
        _txtcompany.placeholder=@"Select Product Name";
        _txtnick.hidden=YES;
        
        
        [_txtphonenumber setKeyboardType:UIKeyboardTypeDefault];
        [_txtphonenumber reloadInputViews];
        
        //NSLog(@"prepaid voucher");
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"ppv";
            [self SSL_Call];
            
            //[self GetPPVBillData:@""];
        }
        
    }
    else if ([gblclass.bill_click isEqualToString:@"Wiz TopUp"])
    {
        //        img_arrow.hidden=YES;
        //        btn_combo_box.enabled=NO;
        //
        //        self.txtbilltype.text=@"Wiz TopUp";
        //        _txtphonenumber.placeholder=@"Enter 16 digit card number";
        //        _txtcompany.text=@"Wiz Card Nick";
        //        _txtcompany.enabled=NO;
        //        _txtnick.hidden=NO;
        
        
        vw_wiz_topup.hidden=NO;
        txt_phonenumber.placeholder=@"Enter 16 digit card number";
        txt_nick.placeholder=@"Enter Nick";
        
        
        [hud hideAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_submitfetchtitle:(UIButton *)sender
{
    strtxtNick=self.txtnick.text;
    strtxtCustomerID=self.txtphonenumber.text;
    
    //    if(([gblclass.bill_click isEqualToString:@"Wiz TopUp"]) && [txt_nick.text length] < 5) {
    //        [self custom_alert:@"Bill nick should be between 5 - 30 characters" :@"0"];
    //        [hud hideAnimated:YES];
    //    } else if(([gblclass.bill_click isEqualToString:@"Mobile TopUp"]) && [_txtnick.text length] < 5) {
    //        [self custom_alert:@"Bill nick should be between 5 - 30 characters" :@"0"];
    //        [hud hideAnimated:YES];
    //    }
    
    //    else if([_txtcompany.text length]==0)
    //    {
    //        [self showAlert:@"Please select your service provider" :@"Attention" ];
    //    }
    //    else if([strtxtCustomerID length]==0)
    //    {
    //        [self showAlert:@"Please enter your mobile number" :@"Attention" ];
    //    }
    //    else if([strtxtNick length]==0 && _txtnick.hidden==NO)
    //    {
    //        [self showAlert:@"Please enter your nick" :@"Attention" ];
    //    }
    //    else
    //    {
    
    //NSLog(@"%@",gblclass.is_default_acct_no);
    //NSLog(@"%@",gblclass.is_default_acct_id);
    
    
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtphonenumber resignFirstResponder];
    [_txtnick resignFirstResponder];
    [_billtype resignFirstResponder];
    [txt_phonenumber resignFirstResponder];
    [txt_nick resignFirstResponder];
    
    
    NSLog(@"%@",gblclass.bill_click);
    
    if([gblclass.bill_click isEqualToString:@"Mobile TopUp"])
    {
        
        if([_txtcompany.text length]==0)
        {
            // [self showAlert:@"Please select your service provider" :@"Attention" ];
            
            [self custom_alert:@"Please select your service provider" :@"0"];
            return;
        }
        else if([strtxtCustomerID length]<11)
        {
            //[self showAlert:@"Please enter your mobile number" :@"Attention" ];
            [self custom_alert:@"Please enter 11 digit mobile number" :@"0"];
            return;
        }
        else if([strtxtNick isEqualToString:@""] && _txtnick.hidden==NO)
        {
            //[self showAlert:@"Please enter your nick" :@"Attention" ];
            [self custom_alert:[NSString stringWithFormat:@"Please enter %@ nick",gblclass.bill_click] :@"0"];
            return;
            
        } else if(([strtxtNick length] < 5) && (_txtnick.hidden==NO)) {
            [self custom_alert:[NSString stringWithFormat:@"%@ nick should be between 5 to 30 characters",gblclass.bill_click] :@"0"];
            return;
        }
        
        gblclass.add_bill_type =@"Prepaid Vouchers";
        
        str_account_no=self.txtphonenumber.text;
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:strTtId];
        [gblclass.arr_values addObject:strAccessKey];
        [gblclass.arr_values addObject:strType];
        [gblclass.arr_values addObject:_txtnick.text];
        [gblclass.arr_values addObject:str_account_no];
        
    }
    else if ([gblclass.bill_click isEqualToString:@"Prepaid Vouchers"])
    {
        if([_txtcompany.text length]==0)
        {
            // [self showAlert:@"Please select your service provider" :@"Attention" ];
            [self custom_alert:@"Please select your service provider" :@"0"];
            
            return;
        }
        else if([strtxtCustomerID isEqualToString:@""])
        {
            // [self showAlert:@"Please enter your Voucher Nick" :@"Attention" ];
            [self custom_alert:[NSString stringWithFormat:@"Please enter %@ nick",gblclass.bill_click] :@"0"];
            return;
        }
        else if([strtxtCustomerID length] < 5) {
            [self custom_alert:[NSString stringWithFormat:@"%@ nick should be between 5 to 30 characters",gblclass.bill_click] :@"0"];
            return;
        }
        
        gblclass.add_bill_type =@"Prepaid Vouchers";
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:strTtId];
        [gblclass.arr_values addObject:strAccessKey];
        [gblclass.arr_values addObject:strType];
        [gblclass.arr_values addObject:_txtphonenumber.text];
        [gblclass.arr_values addObject:str_account_no];
    }
    else if ([gblclass.bill_click isEqualToString:@"Wiz TopUp"])
    {
        strtxtNick=txt_nick.text;
        strtxtCustomerID=txt_phonenumber.text;
        
        if([strtxtCustomerID length]==0)
        {
            // [self showAlert:@"Please enter your 16 Digits Card number" :@"Attention"];
            [self custom_alert:@"Please enter your 16 Digit Card number" :@"0"];
            
            return;
        }
        else if([strtxtNick length]==0 && _txtnick.hidden==NO)
        {
            // [self showAlert:@"Please enter your nick" :@"Attention"];
            [self custom_alert:@"Please enter your nick" :@"0"];
            
            return;
        }
        else if([strtxtNick length] < 5 && _txtnick.hidden==NO) {
            [self custom_alert:[NSString stringWithFormat:@"%@ nick should be between 5 to 30 characters",gblclass.bill_click] :@"0"];
            return;
        }
        
        gblclass.add_bill_type=@"Wiz Card";
        
        [gblclass.arr_values addObject:gblclass.user_id];
        [gblclass.arr_values addObject:gblclass.M3sessionid];
        [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
        [gblclass.arr_values addObject:@"147"];
        [gblclass.arr_values addObject:@"WizCard"];
        [gblclass.arr_values addObject:strtxtCustomerID];
        [gblclass.arr_values addObject:strtxtNick];
        [gblclass.arr_values addObject:@""];
        [gblclass.arr_values addObject:@"WIZ_RELOAD"];
        [gblclass.arr_values addObject:@"false"];
    }
    
    
    //[self Generate_OTP:@""];
    
    
    [self checkinternet];
    if (netAvailable)
    {
        [hud showAnimated:YES];
        chk_ssl=@"otp";
        [self SSL_Call];
    }
    
    
    //*** [self AddBillCompanies:@""];
    //   }
    
    //[self AddAccountTitleFetch:@""];
}


-(void) Generate_OTP:(NSString *)strIndustry
{
    
    @try {
        
        
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        //    name = [[NSMutableArray alloc]init];
        //    ttid = [[NSMutableArray alloc]init];
        //    ttacceskey = [[NSMutableArray alloc]init];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,
                                                                gblclass.M3sessionid,
                                                                @"",
                                                                str_account_no,
                                                                strAccessKey,
                                                                @"Generate OTP",
                                                                strTtId,
                                                                tc_access_key, nil]];
        
        NSString* acct_no_chck;
        
        
        if ([gblclass.bill_click isEqualToString:@"Prepaid Vouchers"])
        {
            acct_no_chck = strTtId;
        }
        else
        {
            acct_no_chck = str_account_no;
        }
        
        //    str_account_no
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@""],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", acct_no_chck]],
                                    [encrypt encrypt_Data:strAccessKey],
                                    [encrypt encrypt_Data:@"Generate OTP"],
                                    [encrypt encrypt_Data:[NSString stringWithFormat:@"%@", strTtId]],
                                    [encrypt encrypt_Data:tc_access_key],
                                    [encrypt encrypt_Data:@"SMS"],
                                    [encrypt encrypt_Data:@"ADDITION"],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strBranchCode",
                                                                       @"strAccountNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"M3Key", nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //             NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  responsecode =[dic objectForKey:@"Response"];
                  
                  
                  
                  NSString* resstring = [dic objectForKey:@"strReturnMessage"];
                  //  [self showAlert:resstring :@"Attention"];
                  //NSLog(@"Done");
                  //[self parsearray];
                  [hud hideAnimated:YES];
                  //  [_tableviewcompany reloadData];
                  
                  
                  if ([responsecode isEqualToString:@"0"])
                  {
                      CATransition *transition = [ CATransition animation];
                      transition.duration = 0.3;
                      transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                      transition.type = kCATransitionPush;
                      transition.subtype = kCATransitionFromRight;
                      [self.view.window.layer addAnimation:transition forKey:nil];
                      
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"add_bill_otp"]; //pay_within_acct
                      
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [hud hideAnimated:YES];
                      chk_ssl=@"logout";
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else
                  {
                      
                      
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                      
                      //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                  [alert addAction:ok];
                      //                  [self presentViewController:alert animated:YES completion:nil];
                  }
                  //904440
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Retry" :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later." :@"0"];
    }
    
    
}




- (IBAction)btn_dd_company:(UIButton *)sender
{
    
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtphonenumber resignFirstResponder];
    [_txtnick resignFirstResponder];
    [_billtype resignFirstResponder];
    [txt_phonenumber resignFirstResponder];
    [txt_nick resignFirstResponder];
    
    if([_txtbilltype.text length]==0)
    {
        [self showAlert:@"Please select your Prepaid type" :@"Attention" ];
    }
    else
    {
        vw_table.hidden=NO;
        self.billtype.hidden=true;
        
        if(self.tableviewcompany.isHidden==YES)
        {
            self.tableviewcompany.hidden=NO;
        }
        else
        {
            self.tableviewcompany.hidden=YES;
            vw_table.hidden=YES;
        }
    }
}


- (IBAction)btn_dd_billtype:(UIButton *)sender
{
    //self.billserviceview.hidden=true;
    
    self.company.hidden=true;
    
    if(self.billtype.hidden)
    {
        self.billtype.hidden=false;
        
        //  bill_service = [[NSMutableArray alloc]init];
        
        //  bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    }
    else
    {
        
        self.billtype.hidden=true;
        
        //  bill_service = [[NSMutableArray alloc]init];
        ///
        // bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    self.cardlist.hidden=true;
    //
    // self.billserviceview.hidden=true;
    self.billtype.hidden=true;
    self.company.hidden=true;
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    
    
    if(tableView==_tableviewbilltype)
    {
        [_tableviewbilltype reloadData];
        self.txtcompany.text=@"";
        self.txtnick.text=@"";
        self.txtphonenumber.text=@"";
        //        _txtretypephonenumber.text=@"";
        
        strTtId=@"";
        strAccessKey=@"";
        strType=@"";
        strtxtNick=@"";
        strtxtCustomerID=@"";
        
        self.txtbilltype.text=[bill_type objectAtIndex:indexPath.row];
        NSString* txttypebillvalue = self.txtbilltype.text;
        
        
        
        /////////Mobile BIll
        if([txttypebillvalue isEqualToString:@"Mobile TopUp"])
        {
            //NSLog(@"Mobile Topup");
            [self checkinternet];
            if (netAvailable)
            {
                [self GetBillCompaniesDetail:@""];
            }
        }
        else if ([txttypebillvalue isEqualToString:@"Prepaid Vouchers"])
        {
            //NSLog(@"prepaid voucher");
            [self checkinternet];
            if (netAvailable)
            {
                [self GetPPVBillData:@""];
            }
            
        }
        else if ([txttypebillvalue isEqualToString:@"Wiz Topup"])
        {
            //NSLog(@"Wiz topup");
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    else if(tableView==_tableviewcompany)
    {
        [_tableviewcompany reloadData];
        self.txtcompany.text=[name objectAtIndex:indexPath.row];
        self.txtnick.text=@"";
        self.txtphonenumber.text=@"";
        //        _txtretypephonenumber.text=@"";
        
        strTtId=@"";
        strAccessKey=@"";
        strType=@"";
        strtxtNick=@"";
        strtxtCustomerID=@"";
        
        //strTtId,*strAccessKey,*strType,*strtxtNick,*strtxtCustomerID;
        
        //NSLog(@"%@",self.txtbilltype.text);
        
        if([self.txtbilltype.text isEqualToString:@"Mobile Topup"])
        {
            
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            strTtId=[ttid objectAtIndex:indexPath.row];
            strType=@"TOPUP";
            tc_access_key=[str_TCAccess_Key objectAtIndex:indexPath.row];
            str_account_no=[NSString stringWithFormat:@"%@%@",gblclass.user_id,[name objectAtIndex:indexPath.row]];
            
            split = [[field_condition objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            
            if ([split count]>0)
            {
                Max_Length=[[split objectAtIndex:0] intValue];
                Min_Length=[[split objectAtIndex:1] intValue];
            }
            else
            {
                Max_Length=0;
                Min_Length=0;
            }
            
        }
        else if([self.txtbilltype.text isEqualToString:@"Prepaid Vouchers"])
        {
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            strTtId=[ttid objectAtIndex:indexPath.row];
            strType=@"PPV";
            tc_access_key=[str_TCAccess_Key objectAtIndex:indexPath.row];
            str_account_no=[NSString stringWithFormat:@"%@%@",gblclass.user_id,[name objectAtIndex:indexPath.row]];
            
            //
            //            split = [[field_condition objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            //
            //            if ([split count]>0)
            //            {
            //                Max_Length=[[split objectAtIndex:0] intValue];
            //                Min_Length=[[split objectAtIndex:1] intValue];
            //            }
            //            else
            //            {
            //                Max_Length=0;
            //                Min_Length=0;
            //            }
            
        }
        else if([self.txtbilltype.text isEqualToString:@"Wiz TopUp"])
        {
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            strTtId=[ttid objectAtIndex:indexPath.row];
            strType=@"Wiz";
            tc_access_key=[str_TCAccess_Key objectAtIndex:indexPath.row];
            str_account_no=[NSString stringWithFormat:@"%@%@",gblclass.user_id,[name objectAtIndex:indexPath.row]];
            
            split = [[field_condition objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            
            if ([split count]>0)
            {
                Max_Length=[[split objectAtIndex:0] intValue];
                Min_Length=[[split objectAtIndex:1] intValue];
            }
            else
            {
                Max_Length=0;
                Min_Length=0;
            }
            
        }
        
        vw_table.hidden=YES;
        self.tableviewcompany.hidden=true;
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //    if(tableView== _tableviewbillservice){
    //        return [bill_service count];
    //    }
    //    else
    
    if(tableView== _tableviewbilltype)
    {
        if([bill_type count]==0){
            return 0;
        }
        else
        {
            return [bill_type count];
        }
    }
    else if(tableView== _tableviewcompany)
    {
        if([companytype count]==0)
        {
            return 0;
        }
        else
        {
            return [companytype  count];
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //    if(tableView==_tableviewbillservice){
    //
    //        UILabel* label=(UILabel*)[cell viewWithTag:1];
    //        label.text=[bill_service objectAtIndex:indexPath.row];
    //        label.font=[UIFont systemFontOfSize:14];
    //        label.textColor=[UIColor blackColor];
    //        [cell.contentView addSubview:label];
    //
    //
    //
    //
    //    }else
    
    
    if(tableView==_tableviewbilltype)
    {
        
        UILabel* label=(UILabel*)[cell viewWithTag:2];
        label.text=[bill_type objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        //NSLog(@"%@",self.txtbilltype.text);
        
        NSDictionary *dic = [companytype objectAtIndex:indexPath.row];
        
        if ([dic count]>0)
        {
            
            if([self.txtbilltype.text isEqualToString:@"Mobile TopUp"])
            {
                [name addObject:[dic objectForKey:@"COMPANY_NAME"]];
                [ttid addObject:[dic objectForKey:@"TT_ID"]];
                [ttacceskey addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
                
                
                
                
                
            }
            else if([self.txtbilltype.text isEqualToString:@"Prepaid Vouchers"])
            {
                [name addObject:[dic objectForKey:@"NAME"]];
                [ttid addObject:[dic objectForKey:@"TT_ID"]];
                [ttacceskey addObject:[dic objectForKey:@"ACCESS_KEY"]];
            }
            
        }
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    else if(tableView==_tableviewcompany)
    {
        
        UILabel* label=(UILabel*)[cell viewWithTag:3];
        NSDictionary *dic = [companytype objectAtIndex:indexPath.row];
        
        if([self.txtbilltype.text isEqualToString:@"Mobile Topup"])
        {
            [name addObject:[dic objectForKey:@"COMPANY_NAME"]];
            [ttid addObject:[dic objectForKey:@"TT_ID"]];
            [ttacceskey addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
            [str_TCAccess_Key addObject:[dic objectForKey:@"TC_ACCESS_KEY"]];
            
            NSString* MaxValueField1=[dic objectForKey:@"LENGTH_MAX"];
            [a addObject:MaxValueField1];
            NSString* MinValueField1=[dic objectForKey:@"LENGTH_MIN"];
            [a addObject:MinValueField1];
            
            
            NSString *bbb = [a componentsJoinedByString:@"|"];
            [field_condition addObject:bbb];
            [a removeAllObjects];
            
            
        }
        else if([self.txtbilltype.text isEqualToString:@"Prepaid Vouchers"])
        {
            [name addObject:[dic objectForKey:@"NAME"]];
            [ttid addObject:[dic objectForKey:@"TT_ID"]];
            [ttacceskey addObject:[dic objectForKey:@"ACCESS_KEY"]];
            [str_TCAccess_Key addObject:[dic objectForKey:@"TC_ACCESS_KEY"]];
            
            //            NSString* MaxValueField1=[dic objectForKey:@"LENGTH_MAX"];
            //            [a addObject:MaxValueField1];
            //            NSString* MinValueField1=[dic objectForKey:@"LENGTH_MIN"];
            //            [a addObject:MinValueField1];
            //
            //
            //            NSString *bbb = [a componentsJoinedByString:@"|"];
            //            [field_condition addObject:bbb];
            //            [a removeAllObjects];
        }
        else if([self.txtbilltype.text isEqualToString:@"Wiz TopUp"])
        {
            [name addObject:[dic objectForKey:@"NAME"]];
            [ttid addObject:[dic objectForKey:@"TT_ID"]];
            [ttacceskey addObject:[dic objectForKey:@"ACCESS_KEY"]];
            [str_TCAccess_Key addObject:[dic objectForKey:@"TC_ACCESS_KEY"]];
            
            NSString* MaxValueField1=[dic objectForKey:@"LENGTH_MAX"];
            [a addObject:MaxValueField1];
            NSString* MinValueField1=[dic objectForKey:@"LENGTH_MIN"];
            [a addObject:MinValueField1];
            
            
            NSString *bbb = [a componentsJoinedByString:@"|"];
            [field_condition addObject:bbb];
            [a removeAllObjects];
            
        }
        
        if ([name count]>0)
        {
            label.text=[name objectAtIndex:indexPath.row];
            label.font=[UIFont systemFontOfSize:12];
            label.textColor=[UIColor whiteColor];
            [cell.contentView addSubview:label];
        }
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        _tableviewcompany.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void) GetBillCompaniesDetail:(NSString *)strIndustry{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    name = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"OB"],
                                [encrypt encrypt_Data:@"TOPUP"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccessKey",
                                                                   @"strType",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetBillCompaniesDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  companytype = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table"];
                  //ttid=
                  //NSLog(@"Done");
                  //[self parsearray];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"]  :@"0"];
              }
              
              [hud hideAnimated:YES];
              [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
          }];
}

-(void) GetPPVBillData:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    name = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"PPV"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTcAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetPPVBillData" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  companytype = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbPPVList"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              
              
              //ttid=
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) AddBillCompanies:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],[encrypt encrypt_Data:gblclass.M3sessionid],[encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],[encrypt encrypt_Data:strTtId],[encrypt encrypt_Data:strAccessKey],[encrypt encrypt_Data:strType],[encrypt encrypt_Data:strtxtNick],[encrypt encrypt_Data:strtxtCustomerID],[encrypt encrypt_Data:@"385661"], nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strTtId",@"strAccessKey",@"strType",@"strtxtNick",@"strtxtCustomerID",@"strOTPPIN", nil]];
    //385661
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"AddBillCompanies" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode = [dic objectForKey:@"Response"];
              
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              [self showAlert:resstring :@"Attention"];
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
          }];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert2 show];
                       
                       
                       
                   });
    //[alert release];
}


//** 5 Oct
//- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//
//    if(buttonIndex ==0)
//    {
//        [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
//        // txtbillservice.text=@"";
//        //  _txtbilltype.text=@"";
//
//        if([responsecode isEqualToString:@"0"])
//        {
//            _txtcompany.text=@"";
//            _txtphonenumber .text=@"";
//            _txtnick.text=@"";
////          _txtretypephonenumber.text=@"";
//
//        }
//        else
//        {
//            responsecode=@"";
//        }
//    }
//}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_logout:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtphonenumber resignFirstResponder];
    [_txtnick resignFirstResponder];
    [_billtype resignFirstResponder];
    [txt_phonenumber resignFirstResponder];
    [txt_nick resignFirstResponder];
    
    self.billtype.hidden=true;
    self.company.hidden=true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger minLength=11;
    NSUInteger maxLength = 16;
    //   BOOL stringIsValid;
    NSCharacterSet *myCharSet;
    
    
    if ([textField isEqual:_txtphonenumber])
    {
        
        maxLength=Max_Length;  // 11;
        minLength=Min_Length;
        
        
        //        if (Max_Length==0 || Min_Length==0)
        //        {
        //            if (_txtcompany.text.length==0 || [_txtcompany.text isEqualToString:@""])
        //            {
        //                [self showAlert:@"Select Company" :@"Attention" ];
        //                return 0;
        //            }
        //            else
        //            {
        //                maxLength=12;
        //            }
        //        }
        
        
        if (Max_Length==0 && minLength==0)
        {
            maxLength=11;
        }
        
        if ([self.txtbilltype.text isEqualToString:@"Mobile Topup"])
        {
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        }
        else if ([self.txtbilltype.text isEqualToString:@"Prepaid Vouchers"])
        {
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
            
            maxLength=30;
        }
        else if ([self.txtbilltype.text isEqualToString:@"Wiz TopUp"])
        {
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            maxLength=16;
        }
        
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                //return YES;
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
            
        }
        
        // [theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    //    if ([textField isEqual:_txtphonenumber])
    //    {
    //        maxLength=11;
    //
    //        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    //        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
    //
    //        [textField addTarget:self
    //                         action:@selector(textFieldDidChange:)
    //               forControlEvents:UIControlEventEditingChanged];
    //
    //        stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    //        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    //
    //    }
    
    if ([textField isEqual:txt_phonenumber])
    {
        maxLength=16;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                NSLog(@"%lu",textField.text.length);
                
                NSLog(@"%@",txt_phonenumber);
                
                NSLog(@"%@",[textField.text stringByReplacingCharactersInRange:range withString:string]);
                
                if(textField.text.length==15)
                {
                    [self checkinternet];
                    if (netAvailable)
                    {
                        [txt_phonenumber resignFirstResponder];
                        
                        strtxtCustomerID = [textField.text stringByReplacingCharactersInRange:range withString:string];
                        
                        txt_phonenumber.text = strtxtCustomerID;
                        
                        [hud showAnimated:YES];
                        chk_ssl=@"";   //otp
                        [self SSL_Call];
                    }
                }
                
                
                
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
            
        }
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    return YES;//[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    
    if ([theTextField isEqual:_txtphonenumber])
    {
        _txtphonenumber.text=formattedOutput;
    }
    
}


#define kOFFSET_FOR_KEYBOARD 45.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //   //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
}


-(void)Add_bill_detail:(NSString *)strIndustry
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    // gblclass.is_default_acct_no=@"227536212"; //****
    //strAccessKey
    
    
   //6 txt_phonenumber.text = strtxtCustomerID;
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"147"],
                                [encrypt encrypt_Data:strtxtNick],
                                [encrypt encrypt_Data:@"WizCard"],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:strtxtCustomerID],
                                [encrypt encrypt_Data:@"WIZ_RELOAD"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTT_ID",
                                                                   @"strBillNick",
                                                                   @"strSelectedBillType",
                                                                   @"strBranchSelectedValue",
                                                                   @"strLoanNumber",
                                                                   @"strRepaymentAccNumber",
                                                                   @"strCCNumber",
                                                                   @"strAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //385661
    //NSLog(@"%@",dictparam);
    
    //  281167 478837 281283
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"AddBillDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring =[dic objectForKey:@"outtxtNickCL"];
              
              
              //NSLog(@"Done");
              //[self parsearray];f
              [hud hideAnimated:YES];
              //  [_tableviewcompany reloadData];
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                  
                  chk_ssl = @"";
                  
                  
                  //   storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //   vc = [storyboard instantiateViewControllerWithIdentifier:@"add_bill_otp"]; //pay_within_acct
                  
                  
                  // [self presentModalViewController:vc animated:YES];
                  
                  
                  if ([gblclass.bill_click isEqualToString:@"Wiz TopUp"])
                  {
                      str_account_no=txt_phonenumber.text;
                  }
                  else
                  {
                      str_account_no=_txtphonenumber.text;
                  }
                  
                  
                  strAccessKey=@"WIZ_RELOAD";
                  strTtId=@"147";
                  tc_access_key=@"WIZ";
                  btn_wiz_chk=@"1";
                  
                  
                  txt_nick.text=[dic objectForKey:@"outtxtNickCL"];
                  
                  //[self custom_alert:resstring :@"1"];
                  //[self showAlertwithcancel:resstring :@"Attention"];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic2 objectForKey:@"strReturnMessage"]  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
                  chk_ssl=@"logout";
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  btn_wiz_chk=@"0";
                  [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]  :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Try again later"  :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
          }];
    
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert2 show];
                   });
    //[alert release];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([btn_wiz_chk isEqualToString:@"1"])
    {
        
        //NSLog(@"%ld",(long)buttonIndex);
        if (buttonIndex == 1)
        {
            //Do something
            //NSLog(@"1");
            btn_wiz_chk=@"0";
        }
        else if(buttonIndex == 0)
        {
            
            btn_wiz_chk=@"0";
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"btn_wiz_otp";
                [self SSL_Call];
            }
            
            //            [self Generate_OTP:@""];
            
        }
    }
    else if([chk_ssl isEqualToString:@"logout"])
    {
        
        [hud showAnimated:YES];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
    }
    
}




///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([gblclass.bill_click isEqualToString:@"Wiz TopUp"] && [chk_ssl isEqualToString:@""])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self Add_bill_detail:@""];
        }
        else if ([chk_ssl isEqualToString:@"mobile"])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self GetBillCompaniesDetail:@""];
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"ppv"])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self GetPPVBillData:@""];
            
            return;
        }
        else if ([chk_ssl isEqualToString:@"otp"])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self Generate_OTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        else if ([chk_ssl isEqualToString:@"btn_wiz_otp"])
        {
            [self.connection cancel];
            chk_ssl=@"";
            [self Generate_OTP:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    NSLog(@"%@",chk_ssl);
    [self.connection cancel];
    if ([gblclass.bill_click isEqualToString:@"Wiz TopUp"] && [chk_ssl isEqualToString:@""])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self Add_bill_detail:@""];
    }
    else if ([chk_ssl isEqualToString:@"mobile"])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self GetBillCompaniesDetail:@""];
        
        return;
    }
    else if ([chk_ssl isEqualToString:@"ppv"])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self GetPPVBillData:@""];
        
        return;
    }
    else if ([chk_ssl isEqualToString:@"otp"])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self Generate_OTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    else if ([chk_ssl isEqualToString:@"btn_wiz_otp"])
    {
        [self.connection cancel];
        chk_ssl=@"";
        [self Generate_OTP:@""];
    }
    
    
    //            else
    //            {
    //                [self Generate_OTP:@""];
    //            }
    
    chk_ssl=@"";
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



@end

