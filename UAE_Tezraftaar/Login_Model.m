//
//  Login_Model.m
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Login_Model.h"
#import "OutdtDataset.h"


NSString *const kLogin_ModelOutdtDataset = @"outdtDataset";
NSString *const kLogin_ModelStrDailyLimit = @"strDailyLimit";
NSString *const kLogin_ModelResponse = @"Response";
NSString *const kLogin_ModelOutdtData = @"outdtData";
NSString *const kLogin_ModelPinMessage = @"pinMessage";
NSString *const kLogin_ModelStrReturnMessage = @"strReturnMessage";
NSString *const kLogin_ModelStrMonthlyLimit = @"strMonthlyLimit";
NSString *const kLogin_ModelObjdtUserInfo = @"objdtUserInfo";
NSString *const kLogin_ModelStrWelcomeMesage = @"strWelcomeMesage";
NSString *const kLogin_ModelStrhCode = @"strhCode";


@interface Login_Model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Login_Model

@synthesize outdtDataset = _outdtDataset;
@synthesize strDailyLimit = _strDailyLimit;
@synthesize response = _response;
@synthesize outdtData = _outdtData;
@synthesize pinMessage = _pinMessage;
@synthesize strReturnMessage = _strReturnMessage;
@synthesize strMonthlyLimit = _strMonthlyLimit;
@synthesize objdtUserInfo = _objdtUserInfo;
@synthesize strWelcomeMesage = _strWelcomeMesage;
@synthesize strhCode = _strhCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kLogin_ModelOutdtDataset]];
            self.strDailyLimit = [self objectOrNilForKey:kLogin_ModelStrDailyLimit fromDictionary:dict];
            self.response = [self objectOrNilForKey:kLogin_ModelResponse fromDictionary:dict];
            self.outdtData = [self objectOrNilForKey:kLogin_ModelOutdtData fromDictionary:dict];
            self.pinMessage = [self objectOrNilForKey:kLogin_ModelPinMessage fromDictionary:dict];
            self.strReturnMessage = [self objectOrNilForKey:kLogin_ModelStrReturnMessage fromDictionary:dict];
            self.strMonthlyLimit = [self objectOrNilForKey:kLogin_ModelStrMonthlyLimit fromDictionary:dict];
            self.objdtUserInfo = [self objectOrNilForKey:kLogin_ModelObjdtUserInfo fromDictionary:dict];
            self.strWelcomeMesage = [self objectOrNilForKey:kLogin_ModelStrWelcomeMesage fromDictionary:dict];
            self.strhCode = [self objectOrNilForKey:kLogin_ModelStrhCode fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kLogin_ModelOutdtDataset];
    [mutableDict setValue:self.strDailyLimit forKey:kLogin_ModelStrDailyLimit];
    [mutableDict setValue:self.response forKey:kLogin_ModelResponse];
    [mutableDict setValue:self.outdtData forKey:kLogin_ModelOutdtData];
    [mutableDict setValue:self.pinMessage forKey:kLogin_ModelPinMessage];
    [mutableDict setValue:self.strReturnMessage forKey:kLogin_ModelStrReturnMessage];
    [mutableDict setValue:self.strMonthlyLimit forKey:kLogin_ModelStrMonthlyLimit];
    [mutableDict setValue:self.objdtUserInfo forKey:kLogin_ModelObjdtUserInfo];
    [mutableDict setValue:self.strWelcomeMesage forKey:kLogin_ModelStrWelcomeMesage];
    [mutableDict setValue:self.strhCode forKey:kLogin_ModelStrhCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.outdtDataset = [aDecoder decodeObjectForKey:kLogin_ModelOutdtDataset];
    self.strDailyLimit = [aDecoder decodeObjectForKey:kLogin_ModelStrDailyLimit];
    self.response = [aDecoder decodeObjectForKey:kLogin_ModelResponse];
    self.outdtData = [aDecoder decodeObjectForKey:kLogin_ModelOutdtData];
    self.pinMessage = [aDecoder decodeObjectForKey:kLogin_ModelPinMessage];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kLogin_ModelStrReturnMessage];
    self.strMonthlyLimit = [aDecoder decodeObjectForKey:kLogin_ModelStrMonthlyLimit];
    self.objdtUserInfo = [aDecoder decodeObjectForKey:kLogin_ModelObjdtUserInfo];
    self.strWelcomeMesage = [aDecoder decodeObjectForKey:kLogin_ModelStrWelcomeMesage];
    self.strhCode = [aDecoder decodeObjectForKey:kLogin_ModelStrhCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_outdtDataset forKey:kLogin_ModelOutdtDataset];
    [aCoder encodeObject:_strDailyLimit forKey:kLogin_ModelStrDailyLimit];
    [aCoder encodeObject:_response forKey:kLogin_ModelResponse];
    [aCoder encodeObject:_outdtData forKey:kLogin_ModelOutdtData];
    [aCoder encodeObject:_pinMessage forKey:kLogin_ModelPinMessage];
    [aCoder encodeObject:_strReturnMessage forKey:kLogin_ModelStrReturnMessage];
    [aCoder encodeObject:_strMonthlyLimit forKey:kLogin_ModelStrMonthlyLimit];
    [aCoder encodeObject:_objdtUserInfo forKey:kLogin_ModelObjdtUserInfo];
    [aCoder encodeObject:_strWelcomeMesage forKey:kLogin_ModelStrWelcomeMesage];
    [aCoder encodeObject:_strhCode forKey:kLogin_ModelStrhCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    Login_Model *copy = [[Login_Model alloc] init];
    
    if (copy) {

        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.strDailyLimit = [self.strDailyLimit copyWithZone:zone];
        copy.response = [self.response copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.pinMessage = [self.pinMessage copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
        copy.strMonthlyLimit = [self.strMonthlyLimit copyWithZone:zone];
        copy.objdtUserInfo = [self.objdtUserInfo copyWithZone:zone];
        copy.strWelcomeMesage = [self.strWelcomeMesage copyWithZone:zone];
        copy.strhCode = [self.strhCode copyWithZone:zone];
    }
    
    return copy;
}


@end
