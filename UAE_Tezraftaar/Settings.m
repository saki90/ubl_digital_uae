//
//  AccountStatement_ViewController.m
//  ubltestbanking
//
//  Created by Mehmood on 03/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Settings.h"
//#import "ACT_Statement_DetailViewController.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"


@interface Settings ()
{
    NSArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UILabel* label;
    NSString* chk_ssl;
    NSString* tch_chk;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Settings
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    arr_act_type=[[NSArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    ssl_count = @"0";
    table.delegate=self;
    
    
    
    gblclass=[GlobalStaticClass getInstance];
    
    lbl_ver.text = gblclass.setting_ver;
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
    [self getCredentialsForServer_touch:@"touch_enable"];
    
     //if ([gblclass.story_board isEqualToString:@"iPhone_X"] || [gblclass.story_board isEqualToString:@"iPhone_Xr"])
    if ([tch_chk isEqualToString:@"11"])
    {
        arr_act_type=@[@"Change Daily Transaction Limit",([gblclass.story_board isEqualToString:@"iPhone_X"] || [gblclass.story_board isEqualToString:@"iPhone_Xr"]) ? @"Face ID" : @"Touch ID"];
    }
    else
    {
        arr_act_type=@[@"Change Daily Transaction Limit"];
    }
    
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    [APIdleManager sharedInstance].onTimeout = ^(void){
        //  [self.timeoutLabel  setText:@"YES"];
        
        
        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        
        storyboard = [UIStoryboard storyboardWithName:
                      gblclass.story_board bundle:[NSBundle mainBundle]];
        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        
        [self presentViewController:vc animated:YES completion:nil];
        
        APIdleManager * cc1=[[APIdleManager alloc] init];
        // cc.createTimer;
        
        [cc1 timme_invaletedd];
        
    };
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    
    //Name ::
    label=(UILabel*)[cell viewWithTag:1];
    label.text = [arr_act_type objectAtIndex:indexPath.row];
    label.textColor=[UIColor blackColor];
    // label.font=[UIFont systemFontOfSize:14];
    [cell.contentView addSubview:label];
    
    // cell.textLabel.text = [arr_act_type objectAtIndex:indexPath.row];
    // cell.textLabel.font=[UIFont systemFontOfSize:14];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    
    
    
    if (indexPath.row==0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"change_limit"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else if (indexPath.row==1)
    {
        gblclass.touch_id_frm_menu=@"1";
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard=[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc=[storyboard instantiateViewControllerWithIdentifier:@"touch_id"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(IBAction)btn_back:(id)sender
{
    [self slideLeft];
    
    gblclass.landing = @"1";
    storyboard=[UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc=[storyboard instantiateViewControllerWithIdentifier:@"landing"];
    [self presentViewController:vc animated:NO completion:nil];
    
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    //    [self.delegate dismissSlideMenu];
}

-(IBAction)btn_Pay:(id)sender
{
    [self slideRight];
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slideLeft {
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

-(void)slideRight {
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_more:(id)sender
{
    //    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    //    // vc.view.backgroundColor = [UIColor blueColor];
    //    //   vc.view.userInteractionEnabled = NO; // disables touches on the object
    //    //   vc.view.alpha = .07;
    //    //     vc.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7f];
    //    // [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //    [vc setTransitioningDelegate:transitionController];
    //    vc.modalPresentationStyle= UIModalPresentationCustom;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                    message:Logout
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}



-(IBAction)btn_account:(id)sender
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try {
        
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Touch Id Not Found." :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - Check Internet -
-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            UIAlertView *magalert33 = [[UIAlertView alloc] initWithTitle:@"Notification" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [magalert33 show];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    [self.connection cancel];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //      // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



@end

