#import "ManagePayee.h"
//#import "ACT_Statement_DetailViewController.h"
#import "MBProgressHUD.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface ManagePayee ()<NSURLConnectionDataDelegate>
{
    //  NSArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSMutableArray *user_account,  *  accountname ,    *ttacesskey,
    * bankcode,*alldata;
    UIButton*  editbutton;
    UIButton*  delbutton;
    NSString* delaccount;
    NSString* responsecode;
    UIAlertController* alert;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation ManagePayee
@synthesize transitionController;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
    alldata=[[NSMutableArray alloc]init];
    ssl_count = @"0";
    
    //    [self GetPayDetails:@""];
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
    // arr_act_type=[[NSArray alloc] init];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    // arr_act_type=@[@"Manage Payee",@"Security Tips",@"Disclaimer",@"Privacy",@"Legal"];
    
    
    [APIdleManager sharedInstance].onTimeout = ^(void){
        //  [self.timeoutLabel  setText:@"YES"];
        
        
        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        
        storyboard = [UIStoryboard storyboardWithName:
                      gblclass.story_board bundle:[NSBundle mainBundle]];
        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        
        
        [self presentViewController:myController animated:YES completion:nil];
        
        
        APIdleManager * cc1=[[APIdleManager alloc] init];
        // cc.createTimer;
        
        [cc1 timme_invaletedd];
        
    };
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    if([accountname count]==0){
    ////        return 0;
    //
    //    }
    //    else{
    
    return [accountname count]-1;
    //    }
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    UILabel*  label=(UILabel*)[cell viewWithTag:1];
    label.text=[accountname objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont systemFontOfSize:10];
    
    [cell.contentView addSubview:label];
    
    UILabel*  banklabel=(UILabel*)[cell viewWithTag:2];
    banklabel.text=[bankcode objectAtIndex:indexPath.row];
    
    [cell.contentView addSubview:banklabel];
    
    editbutton=(UIButton*)[cell viewWithTag:100];
    delbutton=(UIButton*)[cell viewWithTag:1000];
    [editbutton addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:editbutton];
    [delbutton addTarget:self action:@selector(DeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:delbutton];
    
    //NSLog(@"Index row %lu",indexPath.row);
    
    //  cell.textLabel.text = [accountname objectAtIndex:indexPath.row];
    //  cell.textLabel.font=[UIFont systemFontOfSize:14];
    
    // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


- (void)DeleteClicked:(UIButton *)sender
{
    
    //    CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:self.payeelisttable];
    //    NSIndexPath *indexPath1 = [self.payeelisttable indexPathForRowAtPoint:buttonPosition1];
    NSIndexPath *indexPath = [self.payeelisttable indexPathForCell:(UITableViewCell *)sender.superview.superview];
    delaccount=[user_account objectAtIndex:indexPath.row];
    NSString* msg= @"Do you want to Delete ";
    
    NSString* labelnam=[accountname objectAtIndex:indexPath.row];
    msg= [msg stringByAppendingString:labelnam];
    NSString* halfmsg= @" from your paylist.";
    msg= [msg stringByAppendingString:halfmsg];
    responsecode=@"1";
    [self checkinternet];
    if (netAvailable)
    {
        [self showAlertwithcancel:msg :@"Attention"];
    }
    //NSLog(@"%ld",(long)indexPath.row);
    //NSLog(@"Index apth %@",[accountname objectAtIndex:indexPath.row]);
    
}


- (void)yourButtonClicked:(UIButton *)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        
        //    CGPoint buttonPosition1 = [sender convertPoint:CGPointZero toView:self.payeelisttable];
        //    NSIndexPath *indexPath1 = [self.payeelisttable indexPathForRowAtPoint:buttonPosition1];
        
        NSIndexPath *indexPath = [self.payeelisttable indexPathForCell:(UITableViewCell *)sender.superview.superview];
        
        gblclass.selectaccount=[user_account objectAtIndex:indexPath.row];
        
        
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"EditPayee"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    //    UIStoryboard *mainStoryboard;
    //    UIViewController *vc;
    
    if(indexPath.row==0){
        
        [self checkinternet];
        if (netAvailable)
        {
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            //vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"Managepayee"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        
    }
    else{
        //        gblclass.settingclick=[arr_act_type objectAtIndex:indexPath.row];
        //
        //        // [self performSegueWithIdentifier:@"act_summary" sender:self];
        //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //        //vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"settingview"];
        //         [self presentViewController:vc animated:NO completion:nil];
    }
    
    
    
}
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"bill_all"]; //pay_within_acct
        
        [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor =[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    //    vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
    
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btn_logout:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(void) GetPayDetails:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    user_account= [[NSMutableArray alloc]init];
    accountname = [[NSMutableArray alloc]init];
    ttacesskey= [[NSMutableArray alloc]init];
    bankcode= [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    //539152
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abc"], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetPayDetails" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              alldata = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbPayAnyOneList"];
              if ([alldata count]>0)
              {
                  
                  for(int i=0;i<[alldata count];i++)
                  {
                      NSDictionary *dict = [alldata objectAtIndex:i];
                      [accountname addObject:[dict objectForKey:@"ACCOUNT_NAME"]];
                      [user_account addObject:[dict objectForKey:@"USER_ACCOUNT_ID"]];
                      [ttacesskey addObject:[dict objectForKey:@"TT_ACCESS_KEY"]];
                      [bankcode addObject:[dict objectForKey:@"BANK_CODE"]];
                      // [branchcode addObject:[dict objectForKey:@"brcode"]];
                      //[searchbranchname addObject:[dict objectForKey:@"brdetail"]];
                  }
                  
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
              
              //ttid=
              //NSLog(@"Done load branch");
              //[self parsearray];
              [_payeelisttable reloadData];
              [hud hideAnimated:YES];
               
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              
              [hud hideAnimated:YES]; 
              [self custom_alert:@"Retry" :@"0"];
              
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
          }
     
     ];
}


-(void) PayeeListDelete:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    user_account= [[NSMutableArray alloc]init];
    //    accountname = [[NSMutableArray alloc]init];
    //    ttacesskey= [[NSMutableArray alloc]init];
    //    bankcode= [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],delaccount,nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"payAnyOneTOAccountID", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"PayeeListDelete" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode = [dic objectForKey:@"Response"];
              NSString* resstring = [dic objectForKey:@"strReturnMessage"];
              [self showAlert:resstring :@"Attention"];
              [hud hideAnimated:YES];
              
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }
     
     ];
}
-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert1 show];
                   });
    //[alert release];
}
-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                       
                   });
    //[alert release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    
    if(buttonIndex ==0)
    {
        if([responsecode isEqualToString:@"0"])
        {
            [self GetPayDetails:@""];
        }
        else
        {
            [self PayeeListDelete:@""];
        }
    }
    else
    {
        
    }
}

-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        [self GetPayDetails:@""];
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self GetPayDetails:@""];
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


@end

