//
//  UIView+UIViewCatagory.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 19/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewCatagory)

-(void)addCircleViewBorderWidth:(CGFloat)borderWidth withColor:(CGColorRef)color;
-(void) animateCircleDuration:(NSTimeInterval)duration divideCircleBy:(CGFloat)circle;

@end
