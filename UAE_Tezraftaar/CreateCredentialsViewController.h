//
//  CreateCredentialsViewController.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface CreateCredentialsViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel* lbl_heading_txt;
    
         IBOutlet UITextField* txt_userName;
         IBOutlet UITextField* txt_password;
         IBOutlet UITextField* txt_reenterPassword;
    
//    d *userName;
//    @property (strong, nonatomic) IBOutlet UITextField *password;
//    @property (strong, nonatomic) IBOutlet UITextField *reenterPassword;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}



@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@end

