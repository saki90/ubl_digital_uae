//
//  OutdtDataset.h
//
//  Created by   on 22/01/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface OutdtDataset : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *gnbRefreshAcct;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
