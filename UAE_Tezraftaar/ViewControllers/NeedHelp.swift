//
//  NeedHelp.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 22/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class NeedHelp: SignUpVideoController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var secondsCounterLabel: UILabel!
    @IBOutlet weak var callUsLabel: UILabel!
    
    let attributedString = NSMutableAttributedString(string: "Call us for more detail 24/7 at 111-825-888(UAN) +92-21-32446949(International) ")
    
    var timer = Timer()
    var seconds = 120
    var isTimerRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        
        secondsCounterLabel.text = String(seconds)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(NeedHelp.startCounter), userInfo: nil, repeats: true)
        
        print(timer)
        
        uiMakeUp()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Custom Functions
    @objc func startCounter(){
        
        if(seconds >= 1){
        seconds -= 1
        secondsCounterLabel.text = String(seconds)
        }else if(seconds == 0){
            activityIndicator.stopAnimating()
            timer.invalidate()
            
            let alert = UIAlertController(title: "Sorry!", message: "Due to technical reasons, our Call Centre could not connect with you at the moment. A UBL representative will call you shortly." , preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
                UIAlertAction in
        
                
                // Moving to next controller when user taps on ok
                self.dismiss(animated: true, completion: nil)
                
            }
            let callUsButton = UIAlertAction(title: "Call Now", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                //Dismissing the controller and moving to previous screeen before moving to dialPad
                self.dismiss(animated: true, completion: nil)
                // Move to dial pad when user taps call now 
                if let url = URL(string: "tel://\(111825888)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            alert.addAction(callUsButton)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func uiMakeUp(){
        
        attributedString.addAttribute(.font, value: UIFont(name: "Aspira-Demi", size: 14.0)!, range: NSRange(location: 26, length: 40))//45
        callUsLabel.attributedText = attributedString
        
    }

}
//MARK: Actions
extension NeedHelp{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}
