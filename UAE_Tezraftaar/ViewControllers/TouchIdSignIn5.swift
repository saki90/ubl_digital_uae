//
//  TouchIdSignIn5.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 12/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn5: SignInVideoController {

    @IBOutlet weak var changeToLoginPasswordButton: UIButton!
    @IBOutlet weak var continueApplicationButton: UIButton!
    @IBOutlet weak var cancelApplicationButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        uiMakeUp()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        // Rounding the Edges
        changeToLoginPasswordButton.resizingFont()
        changeToLoginPasswordButton.layer.cornerRadius = 7
        
        // Resizing Fonts
        continueApplicationButton.resizingFont()
        cancelApplicationButton.resizingFont()
    }
    func continueApplication(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flag = delegate.reading(fromPlist: "ContinueApplication.plist")
        print(flag)
        guard case _? = flag!["NotFound"] else {
            
            let continueApplicationFlag = flag!["ContinueApplication"] as! Int
            
            if (continueApplicationFlag == 1){
                self.performSegue(withIdentifier: "SignUpFormViewController", sender: self)
            }else if(continueApplicationFlag == 2){
                self.performSegue(withIdentifier: "SelectAccountType1ViewController", sender: self)
            }else if(continueApplicationFlag == 3){
                self.performSegue(withIdentifier: "SelectAccountType3ViewController", sender: self)
            }else if(continueApplicationFlag == 4){
                self.performSegue(withIdentifier: "WelcomeToUBLFamily1ViewController", sender: self)
            }else if(continueApplicationFlag == 5){
                self.performSegue(withIdentifier: "CustomizeDebitCardViewController", sender: self)
            }else if(continueApplicationFlag == 6){
                self.performSegue(withIdentifier: "CreatePinViewController", sender: self)
            }
            return
        }
    }
}
extension TouchIdSignIn5{
    
    //MARK: Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - Actions
    @IBAction func changeToLoginPassword(_ sender: UIButton) {
        self.performSegue(withIdentifier: "TouchIdSignIn5ToTouchIdSignIn", sender: sender)
    }
    
    @IBAction func continueApplicationSelected(_ sender: UIButton) {
        print("continue application selected")
        // continueApplication()
    }
    @IBAction func cancelApplication(_ sender: UIButton) {
        print("cancell applicaiton selected")
        self.performSegue(withIdentifier: "TouchIdSignIn5ToSignIn", sender: sender)
    }
    
}
