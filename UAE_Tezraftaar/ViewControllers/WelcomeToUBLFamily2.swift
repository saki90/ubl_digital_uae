//
//  WelcomeToUBLFamily2.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 13/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class WelcomeToUBLFamily2: SignUpVideoController {

    @IBOutlet weak var getYourCardButton: UIButton!
    @IBOutlet weak var needHelpButton: UIButton!
    @IBOutlet weak var iWouldLikeToVisitBranchButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

       uiMakeUp()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: Custom Functions
    func uiMakeUp(){
        iWouldLikeToVisitBranchButton.layer.cornerRadius = 7
        
        getYourCardButton.layer.cornerRadius = self.getYourCardButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = self.needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
    }
}
//MARK: Actions
extension WelcomeToUBLFamily2{
    
    
    @IBAction func wouldLikeTovisitBranch(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "WelcomeToUblFamily2ToYourSelectedBranch", sender: sender)
    }
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "WelcomeToUblFamily2ToNeedHelp", sender: sender)
    }
    @IBAction func getYourCardPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "WelcomeToUBLFamily2ToCustomizeDebitCard", sender: sender)
    }
}
