//
//  TouchIdSignIn.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 25/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn: SignInVideoController {

    @IBOutlet weak var loginPasswordView: UIView!
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var changeToTouchIdButton: UIButton!
    @IBOutlet weak var continueApplicationButton: UIButton!
    @IBOutlet weak var cancelApplicationButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        loginTextField.delegate = self
        passwordTextField.delegate = self
        
       uiMakeUp()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Setting up bottom border to TextFields
        
        self.loginTextField.resizingFont()
        self.loginTextField.layoutIfNeeded()
        loginTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.passwordTextField.resizingFont()
        self.passwordTextField.layoutIfNeeded()
        passwordTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        // Rounding the edges of Buttons and resizing the fonts
        nextButton.resizingFont()
        nextButton.layer.cornerRadius = 9
        
        changeToTouchIdButton.resizingFont()
        changeToTouchIdButton.layer.cornerRadius = 7
        
        // Rounding the Edges of loginPasswordView
        loginPasswordView.layer.cornerRadius = 9
        loginPasswordView.layer.borderWidth = 1
        loginPasswordView.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35).cgColor
        
        continueApplicationButton.resizingFont()
        cancelApplicationButton.resizingFont()
        
    }
    // moving the whole view up with keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 145
        }
    }
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 145
        }
    }
    func continueApplication(){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flag = delegate.reading(fromPlist: "ContinueApplication.plist")
        print(flag)
        guard case _? = flag!["NotFound"] else {
            
            let continueApplicationFlag = flag!["ContinueApplication"] as! Int
            
            
            if (continueApplicationFlag == 1){
                self.performSegue(withIdentifier: "SignUpFormViewController", sender: self)
            }else if(continueApplicationFlag == 2){
                self.performSegue(withIdentifier: "SelectAccountType1ViewController", sender: self)
            }else if(continueApplicationFlag == 3){
                self.performSegue(withIdentifier: "SelectAccountType3ViewController", sender: self)
            }else if(continueApplicationFlag == 4){
                self.performSegue(withIdentifier: "WelcomeToUBLFamily1ViewController", sender: self)
            }else if(continueApplicationFlag == 5){
                self.performSegue(withIdentifier: "CustomizeDebitCardViewController", sender: self)
            }else if(continueApplicationFlag == 6){
                self.performSegue(withIdentifier: "CreatePinViewController", sender: self)
            }
            return
        }
    }
}
//MARK: Actions
extension TouchIdSignIn{
    //MARK: Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func changeToTouchId(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "TouchIdSignInToTouchIdSignIn5", sender: sender)
        
    }
    
    @IBAction func continueApplicationSelected(_ sender: UIButton) {
        print("continue application selected")
        //continueApplication()
    }
    @IBAction func cancelApplication(_ sender: UIButton) {
        print("cancell applicaiton selected")
        self.performSegue(withIdentifier: "TouchIdSignInToSignIn", sender: sender)
    }
}
extension TouchIdSignIn: UITextFieldDelegate{
    // Setting the TextField placeholder to Empty once a user starts typing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // Hiding the placeholder when user starts typing
        if (loginTextField.isEditing == true){
            loginTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(passwordTextField.isEditing == true){
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Displaying placeholder when user end editing or touches everywhere on the screen
        if(loginTextField.isEditing == false){
            //loginTextField.placeholder = "Login"
            loginTextField.attributedPlaceholder = NSAttributedString(string: "Login", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(passwordTextField.isEditing == false){
            //passwordTextField.placeholder = "Password"
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if loginTextField.returnKeyType == UIReturnKeyType.next{
            passwordTextField.becomeFirstResponder()
        }else if passwordTextField.returnKeyType == UIReturnKeyType.done {
            textField.endEditing(true)
        }
        
        return false
    }
}
