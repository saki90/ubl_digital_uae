 //
//  LetsMeet.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 16/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit
import GoogleMaps
import Security
import PKHUD
import SVProgressHUD
import PeekabooConnect
import Reachability

//import GooglePlaces


class LetsMeet: SignUpVideoController, UIPickerViewDataSource{
    
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var chooseLocationView: UIView!
    
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var preferedLocationTextField: UITextField!
    @IBOutlet var selectCityAndPreferedLocationView: UIView!
    @IBOutlet weak var branchIconImage: UIImageView!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet weak var nextButtonArrowImage: UIImageView!
    @IBOutlet weak var findingYourLocation: UIView!
    @IBOutlet weak var changeLocationButton: UIButton!
    
    @IBOutlet weak var previousControllerButton: UIButton!
    @IBOutlet weak var letsMeetLabel: UIView!
    @IBOutlet weak var navigationBar: UIView!
    
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var letsFindBranch: UILabel!
    
    
    @objc var isSelcted : Bool = false
    @objc var slectedLatitude : String = ""
    @objc var slectedLongitude : String = ""
    
    var  gblclass = GlobalStaticClass()
    var firstRunZoom: Bool = true
    var isPopUpViewAppears: Bool!
    var internetStatus:Reachability!
    var internetAvalibility:Bool!
    
    
    var myLocation: CLLocation!
    var locationManager = CLLocationManager()
    let cityPickerView = UIPickerView()
    let preferedLocationPickerView = UIPickerView()
    var didFindMyLocation = false
    var locationDrageed: Bool!
    
    var pickCity : [BranchesModel] = [BranchesModel]()
    var pickBranch : [(name : String , longitude : String , latitude : String , country : String,address : String , distance : String , branch_code : String, contact : String)] = []
    
    var nearestDist : (name: String,adress : String , location : CLLocationCoordinate2D) = ("","",CLLocationCoordinate2D())
    
    var distance_Frm_loc : [Double] = []
    
    var coordinates: [CLLocationCoordinate2D] = []
    
    @IBOutlet var market_Cutom_view: UIView!
    @IBOutlet weak var marker_branch_name: UILabel!
    
    @IBOutlet weak var marker_branhc_address: UILabel!
    
    @IBOutlet weak var marker_done_oult: UIButton!
    
    var markerFlag : Bool = false
    var markerTap : UITapGestureRecognizer = UITapGestureRecognizer()
    
    var pwl_flag : Bool = false
    var rmDisCrption : NSMutableArray = NSMutableArray.init()
    
    @IBAction func marker_done_Action(_ sender: Any) {
        
        self.mapView.settings.setAllGesturesEnabled(true)
        
        markerFlag = false
        UIView.animate(withDuration: 0.3, animations: {
            self.market_Cutom_view.alpha = 0.0
        })
        
    }
    
    
    @objc func markerDissmiss() -> Void {
        
        //        if markerFlag {
        //
        //            markerFlag = false
        //            UIView.animate(withDuration: 0.3, animations: {
        //                self.market_Cutom_view.alpha = 0.0
        //                self.selectCityAndPreferedLocationView.alpha = 0.0
        //                self.cityTextField.alpha = 0.0
        //                self.preferedLocationTextField.alpha = 0.0
        //                self.mapView.settings.setAllGesturesEnabled(true)
        //            })
        //        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        //        HUD.allowsInteraction = false
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setCornerRadius(8.0)
        SVProgressHUD.setContainerView(self.view)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
        
        mapView.isUserInteractionEnabled = false
        
        //        SVProgressHUD.setMinimumDismissTimeInterval(30.0)
        
        //        SVProgressHUD.dismiss(withDelay: 50.0)
        
        //        PKHUDSize.PKHUDSquareBaseViewSize = CGSize(width: 200, height: 200)
        
        //        Branches
        //        https://secure-sdk.peekaboo.guru/kbprosamdmnioblcruahnhdcjhs_ahajlhljlgjhaskjgl4
        
        //        Cities
        //        https://secure-sdk.peekaboo.guru/klaoshcjanaij2mcdoiaodmnsasjd5
        
        self.gblclass = GlobalStaticClass.getInstance()
        self.gblclass.controllers_Refrence_Stack.add(self)
        self.nextButton.alpha = 0.0
        self.changeLocationButton.alpha = 0.0
        
        if gblclass.isLocationSelected {
            self.cityTextField.text = gblclass.selected_city.uppercased()
            self.preferedLocationTextField.text = gblclass.selcted_branch_address
            mapView.isUserInteractionEnabled = true
         //   self.checkInternet()
          //  if internetAvalibility {
                
            self.getBranchesFromPeekaboo(country: gblclass.selcted_country, city: gblclass.selected_city, latitude: gblclass.selctedLatitude, longitude: gblclass.selectedLongitude)
         //   }
        } else {
            self.cityTextField.placeholder = "City"
            self.preferedLocationTextField.placeholder = "Select a prefered location"
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        print(gblclass.isLocationSelected)
        print(gblclass.selctedLatitude)
        print(gblclass.selectedLongitude)
        
        
        do
        {
            gblclass = GlobalStaticClass.getInstance();
//            markerTap = UITapGestureRecognizer(target: self, action: #selector(self.markerDissmiss))
            markerTap.delegate = self
            mapView.addGestureRecognizer(markerTap)
            getCitiesFromPeekaboo()
            
            //            if gblclass.isLocationSelected {
            //
            //                self.view.endEditing(true)
            //                UIView.animate(withDuration: 0.3, animations: {
            //                    self.selectCityAndPreferedLocationView.alpha = 0.0
            //                    self.cityTextField.alpha = 0.0
            //                    self.preferedLocationTextField.alpha = 0.0
            //
            //                })
            //
            //                let zoomCamera = GMSCameraUpdate.zoomIn()
            //                self.mapView.animate(with: zoomCamera)
            //
            //
            //                let location = preferedLocationTextField.text;
            //
            //              //  for loc in pickBranch {
            //
            //                  //  if loc.name == location {
            //
            //                        // Center the camera on UBL Branch with animation after selecting from pickerView
            //
            //                    let nearestBranch = CLLocationCoordinate2D(latitude: Double(gblclass.selctedLatitude!)!, longitude: Double(gblclass.selectedLongitude!)!)
            //                        let nearestBranchCam = GMSCameraUpdate.setTarget(nearestBranch)
            //                        CATransaction.begin()
            //
            //                        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            //                        CATransaction.setValue(2.5, forKey: kCATransactionAnimationDuration)
            //                        CATransaction.setAnimationTimingFunction(timingFunction)
            //
            //                        self.mapView.animate(with: nearestBranchCam)
            //                        self.mapView.animate(toZoom: 15)
            //                        self.mapView.animate(toBearing: 0)
            //                        self.mapView.animate(toViewingAngle: 0)
            //
            //                        let circleCenter = CLLocationCoordinate2DMake(Double(gblclass.selctedLatitude!)!, Double(gblclass.selectedLongitude)!)
            //                        let marker = GMSMarker(position: circleCenter)
            //                        marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
            //                        //marker.title = "\(loc.name)*\(loc.address)"
            //                        marker.map = self.mapView
            //                        marker.appearAnimation = GMSMarkerAnimation.pop
            //
            //                        CATransaction.commit()
            //
            //
            //                   // }
            //
            //              //  }
            //
            //
            //
            //
            //            }
            
            
            //   else {
            
            
            
//            SVProgressHUD.show()
//            let Url = String(format: "https://secure-sdk.peekaboo.guru/klaoshcjanaij2mcdoiaodmnsasjd5")
//            guard let serviceUrl = URL(string: Url) else { return }
//            //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
//
//            let parameterDictionary = [     "mstoaw":"en",
//                                            "mnakls":"100",
//                                            "opmsta":"0",
//                                            "n4ja3s":"Pakistan"  ]
//
//            var request = URLRequest(url: serviceUrl)
//            request.httpMethod = "POST"
//            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//            request.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
//
//            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
//                return
//            }
//
//            request.httpBody = httpBody
//
//            let session = URLSession.shared
//
//            let (data, response, error) = session.synchronousDataTask(with: request)
//            //            session.dataTask(with: request) { (data, response, error) in
//
//            if let response = response {
//                print(response)
//            }
//            if let data = data {
//
//                do {
//                    let json  = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray
//                    self.pickCity = NSMutableArray() as! [BranchesModel]
//                    for jsonObj in json! {
//
//                        print(jsonObj)
//                        print(json)
//
//                        let obj : NSDictionary = (jsonObj as? NSDictionary)!
//
//                        let city = (obj.object(forKey: "city") as! String).uppercased()
//                        let country = (obj.object(forKey: "country") as! String).uppercased()
//                        //let longi = obj.object(forKey: "longitude")
//                        //let lati = obj.object(forKey: "latitude")
//
//                        if country == "PAKISTAN" {
//
//                            let lat : Double
//                            let long : Double
//
//                            if (self.myLocation == nil) {
//                                lat = 24.846279
//                                long = 67.037504
//
//                            } else {
//                                lat = self.myLocation.coordinate.latitude
//                                long = self.myLocation.coordinate.longitude
//                            }
//
//                            let brachObj = BranchesModel(cityname: city , countryName: country as! String, latitude: String(lat) , longitude: String(long))
//                            self.pickCity.append(brachObj)
//
//
//                            //                                if self.pwl_flag {
//                            //                                    let brachObj = BranchesModel(cityname: city as! String, countryName: country as! String, latitude: String(lat) , longitude: String(long))
//                            //                                    self.pickCity.append(brachObj)
//                            //
//                            //                                }  else {
//                            //                                    if let coor = self.myLocation {
//                            //                                        let brachObj = BranchesModel(cityname: city as! String, countryName: country as! String, latitude: String(lat) , longitude: String(long))
//                            //                                        self.pickCity.append(brachObj)
//                            //                                    }
//                            //                                }
//                            //                        self.pickCity.append(obj.object(forKey: "city") as! String)
//
//
//                        } else {
//                            print(obj)
//                        }
//                    }
//
//                    SVProgressHUD.dismiss()
//
//                    //                        DispatchQueue.main.async {
//                    //                            if !self.pwl_flag {
//                    //
//                    //                                if !self.isSelcted || self.isLocationServiceOn() == false {
//                    //                                    self.mapView.addSubview(self.chooseLocationView)
//                    //                                    self.chooseLocationView.addSubview(self.goButton)
//                    //
//                    //                                }
//                    //
//                    //                            }
//                    //                        }
//
//
//                    print(json)
//
//                }catch {
//                    print(error)
//                }
//            }
            //                }.resume()
            
            //      }
            
            self.view.addSubview(mapView)
            
            
            // removing gestureBlocker from google maps for enabling user to input data in UITextFields
            for (index,gesture) in mapView.gestureRecognizers!.enumerated() {
                if index == 0
                {
                    mapView.removeGestureRecognizer(gesture)
                }
            }
            
            mapView.addSubview(navigationBar)
            mapView.addSubview(nextButton)
            nextButton.addSubview(nextButtonArrowImage)
            mapView.addSubview(changeLocationButton)
            
            if !pwl_flag {
                if !self.isSelcted || isLocationServiceOn() == false {
                    //                    mapView.addSubview(chooseLocationView)
                    //                    chooseLocationView.addSubview(goButton)
                    
                }
            }
            
            if isLocationServiceOn() == false  {
                if !gblclass.isLocationSelected {
                    self.nextButton.alpha = 0.0
                }
            }
            
            chooseLocationView.addSubview(branchIconImage)
            cityPickerView.delegate = self
            cityPickerView.dataSource = self
            
            preferedLocationPickerView.delegate = self
            preferedLocationPickerView.dataSource = self
            
            
            cityTextField.delegate = self
            preferedLocationTextField.delegate = self
            mapView.delegate = self
            
            cityPickerView.tag = 1
            preferedLocationPickerView.tag = 2
            //cityTextField.inputView = cityPickerView
            //preferedLocationTextField.inputView = preferedLocationPickerView
            
            //        UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
            //        myTextField.inputView = dummyView; // Hide keyboard, but show blinking cursor
            
            let dummyview = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            cityTextField.inputView = dummyview
            preferedLocationTextField.inputView = dummyview
            
            //            locationManager.delegate = self
            //            locationManager.requestWhenInUseAuthorization()
            
            mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.new, context: nil)
            
            uiMakeUp()
            isPopUpViewAppears = false
            
            if pwl_flag {
                mapView.animate(toZoom: 100)
            }
            
            
            
            do {
                // Set the map style by passing the URL of the local file.
                if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    NSLog("Unable to find style.json")
                }
                
            } catch {
                NSLog("The style definition could not be loaded: \(error)")
            }
            
        }
        catch
        {
            print("Something went wrong!")
        }
        
    }
    
    
    func isLocationServiceOn() -> Bool{
        if(CLLocationManager.locationServicesEnabled()) {
            return true
        } else {
            return false
        }
    }
    
    
    func slideLeft() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    func slideRight() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    
    func toggleMapButtons() {
        DispatchQueue.main.sync {
            UIView.animate(withDuration: 0.3, animations: {
                if self.gblclass.isLocationSelected {
                    self.nextButton.alpha = 1.0
                    self.changeLocationButton.alpha = 1.0
                } else if self.isLocationServiceOn() && CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                    self.nextButton.alpha = 1.0
                    self.changeLocationButton.alpha = 1.0
                } else {
                    self.nextButton.alpha = 0.0
                    self.changeLocationButton.alpha = 1.0
                }
                
            })
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            
            //            DispatchQueue.main.async {
            print("This is run on the main queue, after the previous code in outer block")
            
            
            if self.gblclass.isLocationSelected {
                
                print(self.pickBranch)
                
                //  for loc in pickBranch {
                //  if loc.name == location {
                // Center the camera on UBL Branch with animation after selecting from pickerView
                
                DispatchQueue.main.async {
                    
                    self.view.endEditing(true)
                    let zoomCamera = GMSCameraUpdate.zoomIn()
                    self.mapView.animate(with: zoomCamera)
                    
                    let location = self.preferedLocationTextField.text;
                    
                    NSLog("%@", self.gblclass.selctedLatitude);
                    NSLog("%@", self.gblclass.selectedLongitude);
                    
                    let nearestBranch = CLLocationCoordinate2D(latitude: Double(self.gblclass.selctedLatitude!)!, longitude: Double(self.gblclass.selectedLongitude!)!)
                    let nearestBranchCam = GMSCameraUpdate.setTarget(nearestBranch)
                    CATransaction.begin()
                    
                    let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    CATransaction.setValue(2.5, forKey: kCATransactionAnimationDuration)
                    CATransaction.setAnimationTimingFunction(timingFunction)
                    
                    self.mapView.animate(with: nearestBranchCam)
                    self.mapView.animate(toZoom: 15)
                    self.mapView.animate(toBearing: 0)
                    self.mapView.animate(toViewingAngle: 0)
                    
                    let circleCenter = CLLocationCoordinate2DMake(Double(self.gblclass.selctedLatitude!)!, Double(self.gblclass.selectedLongitude)!)
                    let marker = GMSMarker(position: circleCenter)
                    marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
                    //marker.title = "\(loc.name)*\(loc.address)"
                    marker.map = self.mapView
                    marker.appearAnimation = GMSMarkerAnimation.pop
                    
                    var marker_detail = self.gblclass.selcted_marker_title.components(separatedBy: "*");
                    marker.title = marker_detail[0]
                    marker.userData = marker_detail[1]
                    self.mapView.selectedMarker = marker
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        self.nextButton.alpha = 1.0
                        self.changeLocationButton.alpha = 1
                        
                    })
                    
                    CATransaction.commit()
                }
            } else {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.changeLocationButton.alpha = 1
                        
                    })
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        // self.nextButton.alpha = 0.0
        //        HUD.hide()
        //        SVProgressHUD.dismiss()
        self.gblclass.step_no_onbording = "2"
        self.gblclass.step_no_help_onbording = "5"
        
        
        //        for subview in self.preferedLocationTextField.subviews {
        //            if let label = subview as? UILabel {
        //                label.minimumScaleFactor = 0.3
        //                label.adjustsFontSizeToFitWidth = true
        //            }
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.mapView.endEditing(true)
    }
    
    //MARK: - Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //////////************************** START TABBAR CONTROLS ***************************///////////
    
    @IBAction func btn_feature(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service Unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        //    let first_time_chk = self.defaults.string(forKey: "enable_touch")
        //        if first_time_chk == "1" || first_time_chk! == "11" {
        //            [hud showAnimated:YES];
        //            if (netAvailable) {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        } else {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please register your User." :@"0"];
        //        }
    }
    
    @IBAction func btn_offer(_ sender: UIButton) {
        
        gblclass.tab_bar_login_pw_check="login";
        
        peekabooSDK(type: "deals")
        
//        var jsCodeLocation: URL?
//        jsCodeLocation = Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//        var rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: ["environment": "production",     // Supported are beta, stage, production
//            "pkbc": "app.com.brd",     // Will be provided by Peekaboo Business team
//            "type": "deals"], launchOptions: nil)
//        var vc = UIViewController()
//        vc.view = rootView
//        present(vc, animated: true) {() -> Void in }
        
        
        //        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        //        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
        //            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
        //            "peekabooEnvironment" : "production",
        //            "peekabooSdkId" : "UBL_SDK",
        //            "peekabooTitle" : "UBL Bank Offers",
        //            "peekabooSplashImage" : "true",
        //            "peekabooWelcomeMessage" : "Please select your City to view deals and discounts offered to UBL customers",
        //            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
        //            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
        //            "peekabooPoweredByFooter" : "false",
        //            "peekabooColorPrimary" : "#007DC0",
        //            "peekabooColorPrimaryDark" : "#005481",
        //            "peekabooColorStatus" : "#2d2d2d",
        //            "peekabooColorSplash" : "#004681",
        //            "peekabooType": "deals"], launchOptions: nil)
        //
        //        let vc = UIViewController()
        //        vc.view = rootView
        //        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_find_us(_ sender: UIButton) {
        
        gblclass.tab_bar_login_pw_check="login";
        
    //    [self .peekabooSDK(type: "locator")]
        
        peekabooSDK(type: "locator")
        
        
//        var jsCodeLocation: URL?
//        jsCodeLocation = Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//        var rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: ["environment": "production",     // Supported are beta, stage, production
//            "pkbc": "app.com.brd",     // Will be provided by Peekaboo Business team
//            "type": "locator"], launchOptions: nil)
//        var vc = UIViewController()
//        vc.view = rootView
//        present(vc, animated: true) {() -> Void in }
        
        //        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        //        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
        //            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
        //            "peekabooEnvironment" : "production",
        //            "peekabooSdkId" : "UBL_SDK",
        //            "peekabooTitle" : "UBL Bank Offers",
        //            "peekabooSplashImage" : "true",
        //            "peekabooWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
        //            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
        //            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
        //            "peekabooPoweredByFooter" : "false",
        //            "peekabooColorPrimary" : "#007DC0",
        //            "peekabooColorPrimaryDark" : "#005481",
        //            "peekabooColorStatus" : "#2d2d2d",
        //            "peekabooColorSplash" : "#004681",
        //            "peekabooType": "locator",
        //            "peekabooLocatorWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
        //            "peekabooLocatorTitle" : "Locate us",
        //            ], launchOptions: nil)
        //
        //        let vc = UIViewController()
        //        vc.view = rootView
        //        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_faq(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "faq")
        self.slideLeft()
        self.present(controller, animated: false, completion: nil)
    }
    
    //////////************************** END TABBAR CONTROLS ***************************///////////
    
    @IBAction func previousController(_ sender: Any) {
        // dismiss(animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectedServicesVC")
        self.slideLeft()
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        
        // self.performSegue(withIdentifier: "LetsMeetToWelcomeToUblFamily1", sender: sender)
        if chooseLocationView.alpha != 0 || market_Cutom_view.alpha != 0 || selectCityAndPreferedLocationView.alpha != 0 {
            
            // markerFlag = false
            UIView.animate(withDuration: 0.3, animations: {
                self.market_Cutom_view.alpha = 0.0
                self.selectCityAndPreferedLocationView.alpha = 0.0
                self.cityTextField.alpha = 0.0
                self.preferedLocationTextField.alpha = 0.0
            })
            
            //            if !pwl_flag{
            //                return;
            //            }
        }
        
        self.gblclass.isLocationSelected = true
        gblclass.map_call_service = "1"
        UserDefaults.standard.set(gblclass.selcted_branch_code, forKey: "selected_branch_code")
        UserDefaults.standard.set(gblclass.selected_city, forKey: "selected_branch_city")
        UserDefaults.standard.set(gblclass.selcted_branch_address, forKey: "selected_branch_address")
        UserDefaults.standard.set(gblclass.selcted_branch_contact, forKey: "selected_branch_contact")
        print(gblclass.selcted_branch_code)
        
        let session = URLSession (
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let url = gblclass.mainurl1 as String
        
        //        HUD.show(.progress)
        self.checkInternet()
        SVProgressHUD.show()
        let task = session.dataTask(with: URL.init(string: url)!, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                SVProgressHUD.dismiss()
                print("error: \(error!.localizedDescription): \(error!)")
            }
            else if data != nil
            {
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    print("Received data:\n\(str)")
                    
                    var branch_code : String
                    
                    if let code:NSString = self.gblclass.selcted_branch_code! as NSString {
                        if code.length < 4 {
                            branch_code = "0\(code)"
                        } else {
                            branch_code = code as String
                        }
                    } else {
                        branch_code = "0525"
                    }
                    
                    
                    let encryption = Encrypt()
                    
                    let parameterDictionary2 = [ "strCNIC" : encryption.encrypt_Data(self.gblclass.cnic_onbording),
                                                 "strSessionId" : encryption.encrypt_Data(self.gblclass.m3sessionid),
                                                 "IP" : encryption.encrypt_Data(GlobalStaticClass.getPublicIp()),
                                                 "Device_ID" : encryption.encrypt_Data(self.gblclass.udid),
                                                 "RequestId" : encryption.encrypt_Data(self.gblclass.request_id_onbording),
                                                 "Token" : encryption.encrypt_Data(self.gblclass.token),
                                                 "strBranchCode"  : encryption.encrypt_Data(branch_code)
                        
                        ] as [String : String]
                    
                    
                    
                    
                    
                    
                    
                    //        @try {
                    //            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
                    
                    // let url = gblclass.mainurl1
                    let manger = AFHTTPSessionManager.init(baseURL: URL.init(string: self.gblclass.mainurl1))
                    //            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                    let searlizer = AFHTTPResponseSerializer()
                    manger.responseSerializer = searlizer
                    
                    manger.post("GetOnBoardRMavail", parameters: parameterDictionary2, progress: nil, success: { (task, responseObject) in
                        
                        if let dict = encryption.de_crypt_Data(responseObject as! Data) {
                            
                            //                                [hud hideAnimated:YES];
                            //                                NSError *error=nil;
                            //                                NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                            //                                NSMutableArray* a;
                            //
                            //                                a = [[NSMutableArray alloc] init];
                            //                                data = [[NSMutableArray alloc] init];
                            //                                self.tableData  = [[NSMutableArray alloc] init];
                            
                            let dictionary = dict as NSDictionary
                            let response = dictionary.object(forKey: "Response") as! String
                            
                            switch response {
                            case "-78","-79":
                                // [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                                let alert = UIAlertController.init(title: "Attention", message: (dictionary.object(forKey: "strReturnMessage") as! String), preferredStyle: .alert)
                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                                    (alert: UIAlertAction!) in
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "login_new")
                                    self.slideLeft()
                                    self.present(controller, animated: false, completion: nil)
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                //                                HUD.hide()
                                SVProgressHUD.dismiss()
                                return
                                
                            case "1" :
                                print("reponse ")
                                self.gblclass.mode_of_meeting = "1"
                                self.rmDisCrption = NSMutableArray.init()
                                
                                let test = dictionary.object(forKey: "GetRMTime") as! NSDictionary
                                let data = test.object(forKey: "Table") as! NSArray
                                self.gblclass.rm_avalibilityTimings  = data  as! [Any]
                                
                                //let data = (dictionary.object(forKey: "GetRMTime") as! NSDictionary).object(forKey: "Table") as! NSMutableArray
                                
                                for dic in data {
                                    
                                    let rmdata = RmDataModel()
                                    
                                    if let description = (dic as! NSDictionary).object(forKey: "DESCRIPTION") {
                                        rmdata.rmDiscription = description as! String
                                    } else {
                                        rmdata.rmDiscription = "N/A"
                                    }
                                    
                                    
                                    if let preferTimeID = (dic as! NSDictionary).object(forKey: "PREFERREDTIME_ID") as? String{
                                        rmdata.rmPrefertimeId = preferTimeID as! String
                                        
                                    } else{
                                        rmdata.rmPrefertimeId = "0"
                                        
                                    }
                                    self.rmDisCrption.add(rmdata)
                                }
                                
                                //                                HUD.hide()
                                SVProgressHUD.dismiss()
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller = storyboard.instantiateViewController(withIdentifier: "VisitArrangmentVC") as! VisitArrangmentVC
                                controller.rmDataArray = self.rmDisCrption
                                self.slideRight()
                                self.present(controller, animated: false, completion: nil)
                                
                                
                                
                            case "92" :
                                print("reponse")
                                self.gblclass.mode_of_meeting = "2"
                                self.gblclass.preferred_time = "0"
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller = storyboard.instantiateViewController(withIdentifier: "MeetRepresentativeVC")
                                self.slideRight()
                                self.present(controller, animated: false, completion: nil)
                                
                                //                                HUD.hide()
                                SVProgressHUD.dismiss()
                                
                            default:
                                
                                //                                HUD.hide()
                                SVProgressHUD.dismiss()
                                let alert = UIAlertController.init(title: "Attention", message: dictionary.object(forKey: "strReturnMessage") as! String, preferredStyle: .alert)
                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                return
                            }
                            
                            // let table =  (dictionary.object(forKey: "GetRMTime") as! NSDictionary).object(forKey: "Table")
                            //
                            //                                if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                            //                                    [hud hideAnimated:YES];
                            //                                    [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                            //                                    return ;
                            //
                            //                                }
                            //                                else if ([[dic objectForKey:@"Response"] integerValue]==1)
                            //                                {
                            //
                            //                                    data = [(NSDictionary *)[dic objectForKey:@"GetRMTime"] objectForKey:@"Table"];
                            //
                            //
                            //                                    for (dic in data)
                            //                                    {
                            //
                            //                                        NSString* DESCRIPTION=[dic objectForKey:@"DESCRIPTION"];
                            //                                        if (DESCRIPTION.length==0 || [DESCRIPTION isEqualToString:@" "] || [DESCRIPTION isEqualToString:nil])
                            //                                        {
                            //                                            DESCRIPTION=@"N/A";
                            //                                            [a addObject:DESCRIPTION];
                            //                                        }
                            //                                        else
                            //                                        {
                            //                                            [a addObject:DESCRIPTION];
                            //                                        }
                            //
                            //                                        NSString* PREFERREDTIME_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"PREFERREDTIME_ID"]];
                            //                                        if (PREFERREDTIME_ID.length==0 || [PREFERREDTIME_ID isEqualToString:@" "] || [PREFERREDTIME_ID isEqualToString:nil])
                            //                                        {
                            //                                            PREFERREDTIME_ID=@"0";
                            //                                            [a addObject:PREFERREDTIME_ID];
                            //                                        }
                            //                                        else
                            //                                        {
                            //                                            [a addObject:PREFERREDTIME_ID];
                            //                                        }
                            //
                            //                                        NSString* bbb = [a componentsJoinedByString:@"|"];
                            //                                        [self.tableData addObject:bbb];
                            //                                        [a removeAllObjects];
                            //                                    }
                            //
                            //                                    NSLog(@"%@",self.tableData);
                            //                                    NSLog(@"%lu",(unsigned long)[self.tableData count]);
                            //
                            //                                    [self.tableView reloadData];
                            //                                }
                            //                                else if ([[dic objectForKey:@"Response"] integerValue]==92)
                            //                                {
                            //                                    [self slide_right];
                            //                                    gblclass.mode_of_meeting = @"2";
                            //                                    gblclass.preferred_time = @"0";
                            //                                    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MeetRepresentativeVC"];
                            //                                    [self presentViewController:vc animated:NO completion:nil];
                            //                                }
                            //                                else
                            //                                {
                            //                                    [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                            //                                }
                            //
                            print(dict);
                        }
                        
                    }) { (task, error) in
                        
                        print(error)
                        //                        HUD.hide()
                        SVProgressHUD.dismiss()
                    }
                    
                }
                else {
                    print("Unable to convert data to text")
                }
            }
        })
        
        task.resume()
        
        
        
        
        
        
        
        
        
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "VisitArrangmentVC")
        //        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func changeLocation(_ sender: Any) {
        //print(myLocation.coordinate)
        
        self.mapView.settings.setAllGesturesEnabled(false)
        
        if chooseLocationView.alpha != 0 || market_Cutom_view.alpha != 0 || selectCityAndPreferedLocationView.alpha != 0 {
            
            // markerFlag = false
            UIView.animate(withDuration: 0.3, animations: {
                self.market_Cutom_view.alpha = 0.0
                self.selectCityAndPreferedLocationView.alpha = 0.0
                self.cityTextField.alpha = 0.0
                self.preferedLocationTextField.alpha = 0.0
            })
            
            //            if !pwl_flag{
            //                return;
            //            }
            
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.selectCityAndPreferedLocationView.alpha = 1.0
            self.cityTextField.alpha = 1.0
            self.preferedLocationTextField.alpha = 1.0
            
        })
        
        mapView.addSubview(selectCityAndPreferedLocationView)
        //        chooseLocationView.addSubview(selectCityAndPreferedLocationView)
        selectCityAndPreferedLocationView.addSubview(cityTextField)
        selectCityAndPreferedLocationView.addSubview(preferedLocationTextField)
        selectCityAndPreferedLocationView.center = self.view.center
        markerFlag = true
        
    }
    
    @IBAction func doneSelected(_ sender: UIButton) {
        
        if !(self.cityTextField.text! == "" || self.preferedLocationTextField.text! == "") {
            
            self.mapView.settings.setAllGesturesEnabled(true)
            if isLocationServiceOn() == false  {
                self.nextButton.alpha = 1
            } else {
                self.nextButton.alpha = 1;
            }
            
            
            // if(cityTextField.text == "Karachi" && preferedLocationTextField.text == "Main Abul Hassan Isphani Road"){
            self.view.endEditing(true)
            UIView.animate(withDuration: 0.3, animations: {
                self.selectCityAndPreferedLocationView.alpha = 0.0
                self.cityTextField.alpha = 0.0
                self.preferedLocationTextField.alpha = 0.0
                
            })
            
            let zoomCamera = GMSCameraUpdate.zoomIn()
            self.mapView.animate(with: zoomCamera)
            
            
            let location = preferedLocationTextField.text;
            
            for loc in pickBranch {
                
                if loc.name == location {
                    
                    
                    // Center the camera on UBL Branch with animation after selecting from pickerView
                    
                    self.mapView.clear()
                    
                    let nearestBranch = CLLocationCoordinate2D(latitude: Double(loc.latitude)!, longitude: Double(loc.longitude)!)
                    let nearestBranchCam = GMSCameraUpdate.setTarget(nearestBranch)
                    CATransaction.begin()
                    
                    let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    CATransaction.setValue(2.5, forKey: kCATransactionAnimationDuration)
                    CATransaction.setAnimationTimingFunction(timingFunction)
                    
                    self.mapView.animate(with: nearestBranchCam)
                    self.mapView.animate(toZoom: 15)
                    self.mapView.animate(toBearing: 0)
                    self.mapView.animate(toViewingAngle: 0)
                    
                    let circleCenter = CLLocationCoordinate2DMake(Double(loc.latitude)!, Double(loc.longitude)!);
                    let marker = GMSMarker(position: circleCenter)
                    marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
                    marker.title = "\(loc.name)"
                    marker.userData = loc.address
                    marker.map = self.mapView
                    marker.appearAnimation = GMSMarkerAnimation.pop
                    self.mapView.selectedMarker = marker
                    
                    
                    // Variables to be saved in NSUserDefaults
                    
                    gblclass.isLocationSelected = true
                    gblclass.selctedLatitude = loc.latitude
                    gblclass.selectedLongitude = loc.longitude
                    let title = marker.title!
                    let userdata = marker.userData!
                    gblclass.selcted_marker_title = "\(title)*\(userdata))"
                    gblclass.selected_city = self.cityTextField.text
                    gblclass.selcted_country = loc.country
                    gblclass.selcted_branch_address = loc.address
                    gblclass.selcted_branch_contact = loc.contact
                    
                    //                gblclass.selcted_branch_code = loc.branch_code
                    
                    if (loc.branch_code.count < 4) {
                        gblclass.selcted_branch_code = "0\(loc.branch_code)"
                    } else {
                        gblclass.selcted_branch_code = loc.branch_code
                    }
                    
                    gblclass.branch_name_onbording = loc.name
                    CATransaction.commit()
                    
                }
            }
            
        }
    }
    
    @IBAction func goSelected(_ sender: Any) {
        
        firstRunZoom = false
        
        //User CATransaction completion handler block for
        
        //Removing the view with animation when GO button selected
        UIView.animate(withDuration: 0.3, animations: {
            self.changeLocationButton.alpha = 1
            self.chooseLocationView.alpha = 0
            self.chooseLocationView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        })
        
        //adding layer on top of heirarchy when finding branch to disable the userInteraction with map
        mapView.addSubview(findingYourLocation)
        findingYourLocation.center = self.view.center
        self.nextButton.alpha = 0
        self.changeLocationButton.alpha = 0
        
        
        //Moving around the current location
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        CATransaction.begin()
        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
        CATransaction.setAnimationTimingFunction(timingFunction)
        CATransaction.setCompletionBlock({
            
            //Move the map around current location, first loop
            let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            CATransaction.begin()
            CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
            CATransaction.setAnimationTimingFunction(timingFunction)
            CATransaction.setCompletionBlock({
                
                //Move the map around current location, second loop
                let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                CATransaction.begin()
                CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                CATransaction.setAnimationTimingFunction(timingFunction)
                CATransaction.setCompletionBlock({
                    
                    //Move the map around current location, third loop
                    let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    CATransaction.begin()
                    CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                    CATransaction.setAnimationTimingFunction(timingFunction)
                    CATransaction.setCompletionBlock({
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.findingYourLocation.alpha = 0.0
                            self.nextButton.alpha = 1
                            self.changeLocationButton.alpha = 1
                        })
                        
                        //TODO: Set nearest branch
                        // Zoom in one zoom level
                        let zoomCamera = GMSCameraUpdate.zoomIn()
                        self.mapView.animate(with: zoomCamera)
                        
                        
                        // Center the camera on UBL Branch when animation finished
                        let nearestBranch = CLLocationCoordinate2D(latitude: self.nearestDist.location.latitude, longitude: self.nearestDist.location.longitude)
                        
                        let nearestBranchCam = GMSCameraUpdate.setTarget(nearestBranch)
                        
                        let marker = GMSMarker(position: nearestBranch)
                        marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
                        marker.title = "\(self.nearestDist.name)"
                        marker.userData = self.nearestDist.adress
                        marker.map = self.mapView
                        marker.appearAnimation = GMSMarkerAnimation.pop
                        self.mapView.selectedMarker = marker
                        
                        CATransaction.begin()
                        
                        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                        CATransaction.setAnimationTimingFunction(timingFunction)
                        CATransaction.setCompletionBlock({
                            self.nextButton.alpha = 1.0
                        })
                        self.mapView.animate(with: nearestBranchCam)
                        self.mapView.animate(toZoom: 15)
                        self.mapView.animate(toBearing: 0)
                        self.mapView.animate(toViewingAngle: 0)
                        
                        
                        CATransaction.commit()
                        
                    })
                    self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
                    CATransaction.commit()
                    
                })
                self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
                CATransaction.commit()
                
            })
            self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
            CATransaction.commit()
            
        })
        
        self.mapView.animate(toViewingAngle: 65)
        self.mapView.animate(toZoom: 18.5)
        self.mapView.animate(toBearing: 120)
        CATransaction.commit()
        
    }
    
    func Go_down() {
        
        firstRunZoom = false
        
        //User CATransaction completion handler block for
        
        //Removing the view with animation when GO button selected
        UIView.animate(withDuration: 0.3, animations: {
            self.changeLocationButton.alpha = 1
            self.chooseLocationView.alpha = 0
            self.chooseLocationView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        })
        
        //adding layer on top of heirarchy when finding branch to disable the userInteraction with map
        mapView.addSubview(findingYourLocation)
        findingYourLocation.center = self.view.center
        self.nextButton.alpha = 0
        self.changeLocationButton.alpha = 0
        
        
        //Moving around the current location
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        CATransaction.begin()
        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
        CATransaction.setAnimationTimingFunction(timingFunction)
        CATransaction.setCompletionBlock({
            
            //Move the map around current location, first loop
            let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            CATransaction.begin()
            CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
            CATransaction.setAnimationTimingFunction(timingFunction)
            CATransaction.setCompletionBlock({
                
                //Move the map around current location, second loop
                let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                CATransaction.begin()
                CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                CATransaction.setAnimationTimingFunction(timingFunction)
                CATransaction.setCompletionBlock({
                    
                    //Move the map around current location, third loop
                    let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
                    CATransaction.begin()
                    CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                    CATransaction.setAnimationTimingFunction(timingFunction)
                    CATransaction.setCompletionBlock({
                        
                        UIView.animate(withDuration: 0.5, animations: {
                            self.findingYourLocation.alpha = 0.0
                            self.nextButton.alpha = 1
                            self.changeLocationButton.alpha = 1
                        })
                        
                        //TODO: Set nearest branch
                        // Zoom in one zoom level
                        let zoomCamera = GMSCameraUpdate.zoomIn()
                        self.mapView.animate(with: zoomCamera)
                        
                        
                        // Center the camera on UBL Branch when animation finished
                        let nearestBranch = CLLocationCoordinate2D(latitude: self.nearestDist.location.latitude, longitude: self.nearestDist.location.longitude)
                        
                        let nearestBranchCam = GMSCameraUpdate.setTarget(nearestBranch)
                        
                        let marker = GMSMarker(position: nearestBranch)
                        marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
                        marker.title = "\(self.nearestDist.name)"
                        marker.userData = self.nearestDist.adress
                        marker.map = self.mapView
                        marker.appearAnimation = GMSMarkerAnimation.pop
                        self.mapView.selectedMarker = marker
                        
                        CATransaction.begin()
                        
                        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
                        CATransaction.setAnimationTimingFunction(timingFunction)
                        CATransaction.setCompletionBlock({
                            self.nextButton.alpha = 1.0
                        })
                        self.mapView.animate(with: nearestBranchCam)
                        self.mapView.animate(toZoom: 15)
                        self.mapView.animate(toBearing: 0)
                        self.mapView.animate(toViewingAngle: 0)
                        
                        
                        CATransaction.commit()
                        
                    })
                    self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
                    CATransaction.commit()
                    
                })
                self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
                CATransaction.commit()
                
            })
            self.mapView.animate(toBearing: self.mapView.camera.bearing + 120)
            CATransaction.commit()
            
        })
        self.mapView.animate(toViewingAngle: 65)
        self.mapView.animate(toZoom: 18.5)
        self.mapView.animate(toBearing: 120)
        CATransaction.commit()
        
        
    }
    
    
    
    // MARK: Custom Functions
    func uiMakeUp(){
        
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(LetsMeet.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LetsMeet.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        //self.blurrViewWhenChangeLocationPressed.alpha = 0.5
        //            self.topBorderOnTabBarView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        //            self.tabBarView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        
        if self.isSelcted {
            //            self.changeLocationButton.alpha = 1
        }
        else {
            //            self.changeLocationButton.alpha = 0
        }
        self.branchIconImage.alpha  = 1
        self.goButton.alpha = 1
        self.letsFindBranch.alpha = 1
        self.doneButton.layer.cornerRadius = 7
        self.marker_done_oult.layer.cornerRadius = 7
        self.selectCityAndPreferedLocationView.layer.cornerRadius = 9
        self.market_Cutom_view.layer.cornerRadius = 9
        self.market_Cutom_view.alpha = 1
        self.selectCityAndPreferedLocationView.alpha = 1
        self.selectCityAndPreferedLocationView.layer.shadowOffset = CGSize(width: 2, height: 6)
        self.market_Cutom_view.layer.shadowOffset = CGSize(width: 2, height: 6)
        self.selectCityAndPreferedLocationView.layer.shadowOpacity = 0.35
        self.market_Cutom_view.layer.shadowOpacity = 0.35
        self.selectCityAndPreferedLocationView.layer.shadowRadius = 13
        self.market_Cutom_view.layer.shadowRadius = 13
//        mapMarkers()
        
        // Setting Bottom border to uiTextFields
        cityTextField.setBottomBorder(red: 81/255, green: 83/255, blue: 87/255, alpha: 0.35)
        preferedLocationTextField.setBottomBorder(red: 81/255, green: 83/255, blue: 87/255, alpha: 0.35)
        
        // Rounding the Buttons
        self.nextButton.layer.cornerRadius = self.nextButton.frame.height / 2
        self.nextButton.layer.shadowRadius = 13
        self.nextButton.layer.shadowOpacity = 0.35
        self.nextButton.layer.shadowOffset = CGSize(width: 2, height: 6)
        
        //Adding shadow to view
        changeLocationButton.layer.cornerRadius = 7
        goButton.layer.cornerRadius = 7
    }
    // moving the whole view up with keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 145
        }
    }
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 145
        }
    }
    
    // Setting up the markers on Map
    func mapMarkers(){
        
        for coordinate in coordinates {
            let marker = GMSMarker(position: coordinate)
            marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
            marker.title = "UBL"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
            self.mapView.selectedMarker = marker
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "search" {
            if cityTextField.isEditing {
                let vc = segue.destination as! SearchController
                var cities : [String] = [String]()
                
                for cityobj in pickCity {
                    cities.append(cityobj.cityName)
                }
                
                vc.searchItems = cities
                vc.delegate = self
                vc.type = "city"
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                present(vc, animated: false, completion: nil)
                
            }
                
            else {
                
                
                let vc = segue.destination as! SearchController
                var cities : [String] = [String]()
                
                for cityobj in pickBranch {
                    cities.append(cityobj.name)
                }
                
                vc.searchItems = cities
                vc.delegate = self
                vc.type = "branch"
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromRight
                transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                present(vc, animated: false, completion: nil)
                
            }
            
        }
    }
    
    
    func checkInternet() {
        internetStatus = Reachability.forInternetConnection()!
        internetStatus!.startNotifier()
        let networkstatus:NetworkStatus = internetStatus!.currentReachabilityStatus()
        var statusString:String!
        
        switch networkstatus {
        case .NotReachable:
            statusString = "Internet Access Not Available"
            internetAvalibility = false
            self.customAlert(message: statusString, iconType: "0")
            break
        case .ReachableViaWWAN:
            statusString = "Reachable WWAN"
            internetAvalibility = true
            break
        case .ReachableViaWiFi:
            statusString = "Reachable WiFi"
            internetAvalibility = true
            break
        }
    }
    
    func customAlert(message:String, iconType:String) {
        gblclass.custom_alert_msg = message
        gblclass.custom_alert_img = iconType
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "alert")
        controller.modalPresentationStyle = .overCurrentContext
        controller.view.alpha = 0.9;
        self.present(controller, animated: false, completion: nil)
    }
    
    
}
 
extension LetsMeet: UITextFieldDelegate{
    
    
    
    // Setting the TextField placeholder to Empty once a user starts typing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // Hiding the placeholder when user starts typing
        if (cityTextField.isEditing == true){
            //            cityTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            self.performSegue(withIdentifier: "search", sender: self)
        }else if(preferedLocationTextField.isEditing == true){
            //            preferedLocationTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            self.performSegue(withIdentifier: "search", sender: self)
        }
        
        
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        // Displaying placeholder when user end editing or touches everywhere on the screen
    //        if(cityTextField.isEditing == false){
    //            //cityTextField.placeholder = "City"
    //            cityTextField.attributedPlaceholder = NSAttributedString(string: "City", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    //        }
    //        if(preferedLocationTextField.isEditing == false){
    //            //preferedLocationTextField.placeholder = "Select a prefered location"
    //            preferedLocationTextField.attributedPlaceholder = NSAttributedString(string: "Select a prefered location", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    //        }
    //    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if cityTextField.returnKeyType == UIReturnKeyType.next{
            preferedLocationTextField.becomeFirstResponder()
        }else if cityTextField.returnKeyType == UIReturnKeyType.done {
            textField.endEditing(true)
        }
        
        return false
    }
}
extension LetsMeet: UIPickerViewDelegate{
    //MARK: - UIPickerViewDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.cityPickerView {
            return pickCity.count
        }
        return pickBranch.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView.tag == 1){
            return (pickCity[row]).cityName
        }else if(pickerView.tag == 2){
            return pickBranch[row].name
        }
        return ""
        //return pickBranch[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1)
        {
            cityTextField.text = (pickCity[row]).cityName
            
            
        } else if(pickerView.tag == 2){
            preferedLocationTextField.text = pickBranch[row].name
        }
        
        
        //        let locationCoordinates: [CLLocationCoordinate2D] = [
        //            CLLocationCoordinate2D(latitude: 31.554606, longitude: 74.357158),
        //            CLLocationCoordinate2D(latitude: 33.598394, longitude: 73.044135),
        //            CLLocationCoordinate2D(latitude: 24.861462, longitude: 67.009939),
        //            CLLocationCoordinate2D(latitude: 33.729388, longitude: 73.093146),
        //            CLLocationCoordinate2D(latitude: 33.697576, longitude: 73.050452),
        //        ]
        
        // self.mapView.camera = GMSCameraPosition(target: locationCoordinates[row], zoom: 15, bearing: 17.5, viewingAngle: 40)
        
        
    }
}
extension LetsMeet: CLLocationManagerDelegate{
    // MARK: - CLLocationManagerDelegate functions
    
    func calcuteDistance(distancceArray : [(latitude : String , longitude : String , name : String ,  address : String) ]) -> (name: String , adress: String , location : CLLocationCoordinate2D) {
        
        
        var distanceArr : [Double] = []
        
        
        for dist in distancceArray {
            
            let coordinate₀ = CLLocation(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
            let coordinate₁ = CLLocation(latitude: Double(dist.latitude)!, longitude: Double(dist.longitude)!)
            
            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
            
            distanceArr.append(distanceInMeters)
            
            
        }
        
        
        let minimunDist = distanceArr.min()
        let coordinates = distancceArray[distanceArr.index(of: minimunDist!)!]
        
        let location = CLLocationCoordinate2D(latitude: Double(coordinates.latitude)!, longitude: Double(coordinates.longitude)!)
        return (coordinates.name , coordinates.address , location)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
            mapView.settings.compassButton = true
            mapView.settings.myLocationButton = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if !didFindMyLocation {
            
            locationManager.stopUpdatingLocation()
            var locationArr : [(latitude : String , longitude : String , name : String ,  address : String, contact : String)] = []
            
            print(locationArr);
            
            myLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            // uncomment to animate for start position
            //            CATransaction.begin()
            //            CATransaction.setValue(3.0, forKey: kCATransactionAnimationDuration)
            
            // Users current locaiton
            //Uncomment the following line to start from user's current location
            //mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
            mapView.camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D.init(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude) , zoom: 17, bearing: 0, viewingAngle: 0)
            //            self.mapView.animate(toZoom: 15)
            //            self.mapView.animate(toViewingAngle: 65)
            //            self.mapView.animate(toBearing: 120)
            
            CATransaction.commit()
            
            mapView.settings.myLocationButton = true
            
            didFindMyLocation = true
            print("observeValue working")
            
            self.checkInternet()
            if (internetAvalibility) {
                
                let geoCoder = CLGeocoder()
                let location = CLLocation(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
                geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                    
                    var countryVar : String = ""
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    
                    // Address dictionary
                    print(placeMark.addressDictionary as Any)
                    
                    // Location name
                    if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                        print(locationName)
                    }
                    // Street address
                    if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                        print(street)
                    }
                    
                    //Country
                    if let country = placeMark.addressDictionary!["Country"] as? NSString {
                        print(country)
                        
                        countryVar = country as String
                    }
                    
                    // City
                    if let city = placeMark.addressDictionary!["City"] as? NSString {
                        print(city)
                        
                        let lat : Double
                        let long : Double
                        
                        if (self.myLocation == nil) {
                            lat = 0
                            long = 0
                            
                        } else {
                            lat = self.myLocation.coordinate.latitude
                            long = self.myLocation.coordinate.longitude
                        }
                        
                    }
                    
                })
            }
        }
        
    }
    
    func getCitiesFromPeekaboo() {
        
        self.checkInternet()
        if internetAvalibility {
            let Url = String(format: "https://secure-sdk.peekaboo.guru/klaoshcjanaij2mcdoiaodmnsasjd5")
            guard let serviceUrl = URL(string: Url) else { return }
            //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
            
            let parameterDictionary = [     "mstoaw":"en",
                                            "mnakls":"100",
                                            "opmsta":"0",
                                            "n4ja3s":"Pakistan"  ]
            
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
                return
            }
            
            request.httpBody = httpBody
            
            let session = URLSession.shared
            
            //        let (data, response, error) = session.synchronousDataTask(with: request)
            session.dataTask(with: request) { (data, response, error) in
                
                if let response = response {
                    print(response)
                }
                if let data = data {
                    
                    do {
                        let json  = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray
                        self.pickCity = NSMutableArray() as! [BranchesModel]
                        for jsonObj in json! {
                            
                            print(jsonObj)
                            print(json)
                            
                            let obj : NSDictionary = (jsonObj as? NSDictionary)!
                            
                            let city = (obj.object(forKey: "city") as! String).uppercased()
                            let country = (obj.object(forKey: "country") as! String).uppercased()
                            //let longi = obj.object(forKey: "longitude")
                            //let lati = obj.object(forKey: "latitude")
                            
                            if country == "PAKISTAN" {
                                
                                let lat : Double
                                let long : Double
                                
                                if (self.myLocation == nil) {
                                    lat = 24.846279
                                    long = 67.037504
                                    
                                } else {
                                    lat = self.myLocation.coordinate.latitude
                                    long = self.myLocation.coordinate.longitude
                                }
                                
                                let brachObj = BranchesModel(cityname: city , countryName: country as! String, latitude: String(lat) , longitude: String(long))
                                self.pickCity.append(brachObj)
                                
                                
                                //                                if self.pwl_flag {
                                //                                    let brachObj = BranchesModel(cityname: city as! String, countryName: country as! String, latitude: String(lat) , longitude: String(long))
                                //                                    self.pickCity.append(brachObj)
                                //
                                //                                }  else {
                                //                                    if let coor = self.myLocation {
                                //                                        let brachObj = BranchesModel(cityname: city as! String, countryName: country as! String, latitude: String(lat) , longitude: String(long))
                                //                                        self.pickCity.append(brachObj)
                                //                                    }
                                //                                }
                                //                        self.pickCity.append(obj.object(forKey: "city") as! String)
                                
                                
                            } else {
                                print(obj)
                            }
                        }
                        
                        
                        
                        //                        DispatchQueue.main.async {
                        //                            if !self.pwl_flag {
                        //
                        //                                if !self.isSelcted || self.isLocationServiceOn() == false {
                        //                                    self.mapView.addSubview(self.chooseLocationView)
                        //                                    self.chooseLocationView.addSubview(self.goButton)
                        //
                        //                                }
                        //
                        //                            }
                        //                        }
                        
//                        self.checkInternet()
//                        if self.internetAvalibility {
                        
                        self.getBranchesFromPeekaboo(country: self.pickCity[0].countryNam, city: self.pickCity[0].cityName, latitude: self.pickCity[0].latitude, longitude: self.pickCity[0].longitude)
                        //}
                        
                        DispatchQueue.main.async {
                            if(self.isPopUpViewAppears == false && !self.pwl_flag){
                                if (!self.gblclass.isLocationSelected) {
                                    self.perform(#selector(LetsMeet.popUpView), with: nil, afterDelay: 1)
                                    self.isPopUpViewAppears = true
                                    
                                }
                            }
                        }
                        
                        print(json)
                        
                    } catch {
                        print(error)
                    }
                }
                
                }.resume()
            SVProgressHUD.dismiss()
        }
    }
    
    func getBranchesFromPeekaboo(country:String, city:String, latitude:String, longitude:String) {
        
        self.checkInternet()
        if internetAvalibility {
            SVProgressHUD.show()
            let Url2 = String(format:"https://secure-sdk.peekaboo.guru/kbprosamdmnioblcruahnhdcjhs_ahajlhljlgjhaskjgl4")
            guard let serviceUrl2 = URL(string: Url2) else { return }
            //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
            let parameterDictionary2 = [ "fksyd":city,
                                         
                                         "n4ja3s":country,
                                         
                                         "js6nwf":latitude,
                                         
                                         "pan3ba":longitude,
                                         
                                         "mstoaw":"en",
                                         
                                         "kisu87":"me",
                                         
                                         "matsw":"UBL",
                                         
                                         "mnakls":"500",
                                         
                                         "opmsta":"0",
                                         
                                         "makthya":"distance",
                                         
                                         "9msh":"asc",
                                         "7WdpTO":[98],
                                         "3aqwved":"true"   ] as [String : Any]
            
            
            var request2 = URLRequest(url: serviceUrl2)
            request2.httpMethod = "POST"
            request2.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request2.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
            guard let httpBody2 = try? JSONSerialization.data(withJSONObject: parameterDictionary2, options: []) else {
                return
            }
            request2.httpBody = httpBody2
            
            let session2 = URLSession.shared
            //        let (data, response, error) = session2.synchronousDataTask(with: request2)
            
            
            session2.dataTask(with: request2) { (data, response, error) in
                
                if let error = error {
                    print("Synchronous task ended with error: \(error)")
                }
                else {
                    print("Synchronous task ended without errors.")
                }
                
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    
                    
                    do {
                        let json  = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                        print(json!)
                        
                        let branchData : NSArray = json?.object(forKey: "branches") as! NSArray
                        
                        print(branchData)
                        
                        self.pickBranch = []
                        
                        for branch in branchData {
                            
                            let branchinfo = branch as! NSDictionary
                            
                            let longitude : Double = branchinfo.object(forKey: "longitude") as! Double //String(describing: branchinfo.object(forKey: "longitude")) as! String
                            let latitude : Double = branchinfo.object(forKey: "latitude") as! Double //String(describing: branchinfo.object(forKey: "latitude")) as! String
                            //let cityName : String = branchinfo.object(forKey: "city") as! String
                            let name : String = branchinfo.object(forKey: "name") as! String
                            let country : String = branchinfo.object(forKey: "country") as! String
                            let adress : String = branchinfo.object(forKey: "address") as! String
                            let distance : String = branchinfo.object(forKey: "distance") as! String
                            var contact : String = ""
                            
                            if let number = branchinfo.object(forKey: "contactNumber") as? String, number != "" {
                                contact = number
                            }
                            
                            var code = ""
                            
                            let coo = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                            
                            if let amenties = branchinfo.object(forKey: "amenities") {
                                
                                let amedDc = amenties as? NSDictionary
                                
                                if let branchCode = amedDc?.object(forKey: "Branch Code") {
                                    let brachDc = branchCode as? NSDictionary
                                    code = brachDc?.object(forKey: "value") as! String
                                    
                                } else {
                                    code = "0525"
                                }
                                
                            } else {
                                code = "0525"
                            }
                            
                            //For Only number save ... saki
                            if Int(code) != nil {
                                self.pickBranch.append((name: name, longitude: String(longitude), latitude: String(latitude), country: country,address : adress ,distance: distance , branch_code : code, contact : contact))
                                
                                //                            locationArr.append((latitude: String(latitude), longitude: String(longitude), name: name , address : adress, contact : contact))
                                
                                self.coordinates.append(coo)
                                
                            }
                            
                        }
                        
                        // self.mapMarkers()
                        DispatchQueue.main.async{
                            if !self.gblclass.isLocationSelected {
                                
                                self.nearestDist =  (self.pickBranch[0].name, self.pickBranch[0].address, CLLocationCoordinate2D(latitude: Double(self.pickBranch[0].latitude)!, longitude: Double(self.pickBranch[0].longitude)!))
                                let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: Double(self.pickBranch[0].latitude)!, longitude: Double(self.pickBranch[0].longitude)!))
                                
                                NSLog("%@", marker);
                                
                                marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
                                marker.title = "\(self.pickBranch[0].name)"
                                marker.userData = self.pickBranch[0].address
                                marker.map = self.mapView
                                marker.appearAnimation = GMSMarkerAnimation.pop
                                //self.mapView.selectedMarker = marker
                                
                                //self.gblclass.isLocationSelected = true
                                self.gblclass.selctedLatitude = self.pickBranch[0].latitude
                                self.gblclass.selectedLongitude = self.pickBranch[0].longitude
                                let title = marker.title!
                                let userdata = marker.userData!
                                self.gblclass.selcted_marker_title = "\(title)*\(userdata))"
                                self.gblclass.selected_city = city as String?
                                self.gblclass.selcted_country = self.pickBranch[0].country
                                self.gblclass.selcted_branch_code = self.pickBranch[0].branch_code
                                self.gblclass.branch_name_onbording = self.pickBranch[0].name
                                self.gblclass.selcted_branch_address = self.pickBranch[0].address
                                self.gblclass.selcted_branch_contact = self.pickBranch[0].contact
                                
                                
                                self.cityTextField.text = city as String
                                self.preferedLocationTextField.text = self.self.pickBranch[0].name
                                self.mapView.isUserInteractionEnabled = true
                                
                            }
                        }
                        
                        print()
                        SVProgressHUD.dismiss()
                        
                    } catch {
                        print(error)
                    }
                }
                }.resume()
        }
    }
    
    // executes when the location manager receives new location data.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            //updates the map’s camera to center around the user’s current location
            
            if !gblclass.isLocationSelected {
                self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 40, bearing: 0, viewingAngle: 90)
                mapView.animate(toLocation: location.coordinate)
                mapView.animate(toZoom: 40)
            }
            
            
            //locationManager you’re no longer interested in updates; you don’t want to follow a user around as their initial location is enough for you to work with
            locationManager.stopUpdatingLocation()
        }
    }
}

//MARK: - MapViewDelegate
extension LetsMeet: GMSMapViewDelegate{
    // Animation Code
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.mapView.settings.setAllGesturesEnabled(false)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.market_Cutom_view.alpha = 1.0
            
        })
        
        // let listDetails = marker.title?.components(separatedBy: "*")
        //let name = listDetails![0]
        //let address = listDetails![1]
        
        marker_branch_name.text = marker.title
        marker_branhc_address.text = marker.userData as? String
        
        mapView.addSubview(market_Cutom_view)
        market_Cutom_view.center = self.view.center
        markerFlag = true
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idle at position called")
        
        //uncomment the following code to continiously move arround the current location
        
        //        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        //        CATransaction.begin()
        //        CATransaction.setValue(3.0, forKey: kCATransactionAnimationDuration)
        //        CATransaction.setAnimationTimingFunction(timingFunction)
        //
        //        self.mapView.animate(toBearing: mapView.camera.bearing + 120)
        //        CATransaction.commit()
    }
    
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        print("mapViewDidFinishTileRendering")
        
        //Zooming out the map when tileRendereingFinished
        if(firstRunZoom == true){
            CATransaction.begin()
            let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
            CATransaction.setAnimationTimingFunction(timingFunction)
            
            if !pwl_flag {
                self.mapView.animate(toZoom: 15)
                
            } else  if !gblclass.isLocationSelected {
                self.mapView.animate(toZoom: 2)
            
            }
            firstRunZoom = false
            CATransaction.commit()
            
        }
        //Appears chooseLocationView when tileRenderingFinished
//        if(isPopUpViewAppears == false && !pwl_flag){
//            if (!gblclass.isLocationSelected) {
//
//                self.getBranchesFromPeekaboo(country: self.pickCity[0].countryNam, city: self.pickCity[0].cityName, latitude: self.pickCity[0].latitude, longitude: self.pickCity[0].longitude)
//                self.perform(#selector(LetsMeet.popUpView), with: nil, afterDelay: 1)
//                self.isPopUpViewAppears = true
//
//            }
//        }
    }
    
    //Setting up the popupView with GO button
    @objc func popUpView()
    {
        self.chooseLocationView.layer.cornerRadius = 9
        self.chooseLocationView.backgroundColor = UIColor.white
        
        chooseLocationView.center = self.mapView.center
        self.chooseLocationView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        chooseLocationView.alpha = 0
        
        UIView.animate(withDuration: 0.5, animations: {
            //            self.changeLocationButton.alpha = 1
            self.branchIconImage.alpha  = 1
            self.goButton.alpha = 1
            self.letsFindBranch.alpha = 1
            self.chooseLocationView.alpha = 1
            self.chooseLocationView.transform = .identity
            self.chooseLocationView.layer.shadowOffset = CGSize(width: 2, height: 6)
            self.chooseLocationView.layer.shadowOpacity = 0.35
            self.chooseLocationView.layer.shadowRadius = 13
        })
        
        sleep(1)
        Go_down()
    }
    
}

extension LetsMeet : SearchCustomDelegate {
    
    func didSelectCityOrBranch(_ selectedItem: Int , type: String!) {
        
        if type == "branch" {
            preferedLocationTextField.text = pickBranch[selectedItem].name
            gblclass.branch_name_onbording = self.preferedLocationTextField.text!;
            print(pickBranch[selectedItem].name)
            
            gblclass.selcted_branch_address = pickBranch[selectedItem].address;
            gblclass.selcted_branch_contact = pickBranch[selectedItem].contact;
            
            return
        }
        
        cityTextField.text = pickCity[selectedItem].cityName
        gblclass.selected_city = self.cityTextField.text!;
        
        self.checkInternet()
        SVProgressHUD.show()
        
        // Move to a background thread to do some long running work
        DispatchQueue.global(qos: .userInitiated).async {
            
            let Url2 = String(format: "https://secure-sdk.peekaboo.guru/kbprosamdmnioblcruahnhdcjhs_ahajlhljlgjhaskjgl4")
            guard let serviceUrl2 = URL(string: Url2) else { return }
            //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
            let parameterDictionary2 = [ "fksyd":self.pickCity[selectedItem].cityName,
                                         
                                         "n4ja3s":self.pickCity[selectedItem].countryNam,
                                         
                                         "js6nwf":self.pickCity[selectedItem].latitude,
                                         
                                         "pan3ba":self.pickCity[selectedItem].longitude,
                                         
                                         "mstoaw":"en",
                                         
                                         "kisu87":"me",
                                         
                                         "matsw":"UBL",
                                         
                                         "mnakls":"500",
                                         
                                         "opmsta":"0",
                                         
                                         "makthya":"distance",
                                         
                                         "9msh":"asc",
                                         "3aqwved":"true",
                                         "7WdpTO": [98]
                ] as [String : Any]
            
            
            var request2 = URLRequest(url: serviceUrl2)
            request2.httpMethod = "POST"
            request2.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            request2.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
            guard let httpBody2 = try? JSONSerialization.data(withJSONObject: parameterDictionary2, options: []) else {
                return
            }
            request2.httpBody = httpBody2
            
            let session2 = URLSession.shared
            //        HUD.show(HUDContentType.progress)
            
            session2.dataTask(with: request2) { (data, response, error) in
                
                //            HUD.hide()
                //            SVProgressHUD.dismiss()
                //PKHUD.sharedHUD.hide()
                
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json  = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                        
                        let branchData : NSArray = json?.object(forKey: "branches") as! NSArray
                        
                        self.pickBranch = []
                        
                        print(json)
                        print(branchData)
                        
                        for branch in branchData {
                            
                            let branchinfo = branch as! NSDictionary
                            
                            let longitude : Double = branchinfo.object(forKey: "longitude") as! Double //String(describing: branchinfo.object(forKey: "longitude")) as! String
                            let latitude : Double = branchinfo.object(forKey: "latitude") as! Double //String(describing: branchinfo.object(forKey: "latitude")) as! String
                            //let cityName : String = branchinfo.object(forKey: "city") as! String
                            let name : String = branchinfo.object(forKey: "name") as! String
                            let country : String = branchinfo.object(forKey: "country") as! String
                            let adress : String = branchinfo.object(forKey: "address") as! String
                            let distance : String = branchinfo.object(forKey: "distance") as! String
                            var contact : String = ""
                            
                            if let number = branchinfo.object(forKey: "contactNumber") as? String, number != "" {
                                contact = number
                            }
                            
                            var code = ""
                            
                            let coo = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                            
                            if let amenties = branchinfo.object(forKey: "amenities") {
                                
                                let amedDc = amenties as? NSDictionary
                                
                                if let branchCode = amedDc?.object(forKey: "Branch Code") {
                                    
                                    let brachDc = branchCode as? NSDictionary
                                    
                                    code = brachDc?.object(forKey: "value") as! String
                                    
                                }
                                
                            }
                            
                            //For Only number save ... saki
                            if Int(code) != nil {
                                // is a number
                                
                                self.pickBranch.append((name: name, longitude: String(longitude), latitude: String(latitude), country: country,address : adress ,distance: distance , branch_code : code, contact : contact))
                                
                            }
                        }
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        
                        print()
                        
                    }catch {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                }.resume()
            print()
            
        }
        // Bounce back to the main thread to update the UI
        //            DispatchQueue.main.async {
        //                SVProgressHUD.dismiss()
        //            }
    }
    
    
    
    //        let Url2 = String(format: "https://secure-sdk.peekaboo.guru/kbprosamdmnioblcruahnhdcjhs_ahajlhljlgjhaskjgl4")
    //        guard let serviceUrl2 = URL(string: Url2) else { return }
    //        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
    //        let parameterDictionary2 = [ "fksyd":pickCity[selectedItem].cityName,
    //
    //                                     "n4ja3s":pickCity[selectedItem].countryNam,
    //
    //                                     "js6nwf":pickCity[selectedItem].latitude,
    //
    //                                     "pan3ba":pickCity[selectedItem].longitude,
    //
    //                                     "mstoaw":"en",
    //
    //                                     "kisu87":"me",
    //
    //                                     "matsw":"UBL",
    //
    //                                     "mnakls":"500",
    //
    //                                     "opmsta":"0",
    //
    //                                     "makthya":"distance",
    //
    //                                     "9msh":"asc",
    //                                     "3aqwved":"true",
    //                                     "7WdpTO": [98]
    //            ] as [String : Any]
    //
    //
    //        var request2 = URLRequest(url: serviceUrl2)
    //        request2.httpMethod = "POST"
    //        request2.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    //        request2.setValue("7db159dc932ec461c1a6b9c1778bb2b0", forHTTPHeaderField: "ownerkey")
    //        guard let httpBody2 = try? JSONSerialization.data(withJSONObject: parameterDictionary2, options: []) else {
    //            return
    //        }
    //        request2.httpBody = httpBody2
    //
    //        let session2 = URLSession.shared
    //        //        HUD.show(HUDContentType.progress)
    //
    //        session2.dataTask(with: request2) { (data, response, error) in
    //
    ////            HUD.hide()
    ////            SVProgressHUD.dismiss()
    //            //PKHUD.sharedHUD.hide()
    //
    //            if let response = response {
    //                print(response)
    //            }
    //            if let data = data {
    //                do {
    //                    let json  = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
    //
    //                    let branchData : NSArray = json?.object(forKey: "branches") as! NSArray
    //
    //                    self.pickBranch = []
    //
    //                    print(json)
    //                    print(branchData)
    //
    //                    for branch in branchData {
    //
    //                        let branchinfo = branch as! NSDictionary
    //
    //                        let longitude : Double = branchinfo.object(forKey: "longitude") as! Double //String(describing: branchinfo.object(forKey: "longitude")) as! String
    //                        let latitude : Double = branchinfo.object(forKey: "latitude") as! Double //String(describing: branchinfo.object(forKey: "latitude")) as! String
    //                        //let cityName : String = branchinfo.object(forKey: "city") as! String
    //                        let name : String = branchinfo.object(forKey: "name") as! String
    //                        let country : String = branchinfo.object(forKey: "country") as! String
    //                        let adress : String = branchinfo.object(forKey: "address") as! String
    //                        let distance : String = branchinfo.object(forKey: "distance") as! String
    //
    //                        var code = ""
    //
    //                        let coo = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    //
    //                        if let amenties = branchinfo.object(forKey: "amenities") {
    //
    //                            let amedDc = amenties as? NSDictionary
    //
    //                            if let branchCode = amedDc?.object(forKey: "Branch Code") {
    //
    //                                let brachDc = branchCode as? NSDictionary
    //
    //                                code = brachDc?.object(forKey: "value") as! String
    //
    //                            }
    //
    //                        }
    //
    //
    //
    //                        self.pickBranch.append((name: name, longitude: String(longitude), latitude: String(latitude), country: country,address : adress ,distance: distance , branch_code : code))
    ////                        HUD.hide()
    ////                        SVProgressHUD.dismiss()
    //                    }
    //                    SVProgressHUD.dismiss()
    //                    print()
    //
    //                }catch {
    //
    //                    HUD.hide()
    //                    SVProgressHUD.dismiss()
    ////                    print(error)
    //                }
    //
    ////                HUD.hide()
    //                SVProgressHUD.dismiss()
    //            }
    //            }.resume()
    //        print()
    ////        HUD.hide()
    ////        SVProgressHUD.dismiss()
    //
    //    }
    
    
    func peekabooSDK(type: String)
    {
        self.present(getPeekabooUIViewController(["environment": "production", "pkbc": "app.com.brd", "type": type, "country": "Pakistan"]), animated: true, completion: nil)
        
    }
    
}

extension LetsMeet: UIGestureRecognizerDelegate {
    
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer == self.markerTap {
            if markerFlag {
                let loc = gestureRecognizer.location(in: self.view)
                
                if !self.selectCityAndPreferedLocationView.frame.contains(loc) || self.selectCityAndPreferedLocationView.alpha == 0 {
                    markerFlag = false
                    UIView.animate(withDuration: 0.3, animations: {
                        self.market_Cutom_view.alpha = 0.0
                        self.selectCityAndPreferedLocationView.alpha = 0.0
                        self.cityTextField.alpha = 0.0
                        self.preferedLocationTextField.alpha = 0.0
                        self.mapView.settings.setAllGesturesEnabled(true)
                    })
                }
                return false
            }
        }
        return true
        
    }
    
}

extension URLSession {
    
    func synchronousDataTask(with url: URLRequest) -> (Data?, URLResponse?, Error?) {
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let dataTask = self.dataTask(with: url) {
            data = $0
            response = $1
            error = $2
            
            semaphore.signal()
        }
        dataTask.resume()
        
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (data, response, error)
    }
}


class NSURLSessionPinningDelegate: NSObject, URLSessionDelegate {
    
    let gblclass = GlobalStaticClass.getInstance()
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if(errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        let file_der = Bundle.main.path(forResource: gblclass!.ssl_name, ofType: gblclass!.ssl_type)
                        
                        if let file = file_der {
                            if let cert2 = NSData(contentsOfFile: file) {
                                if cert1.isEqual(to: cert2 as Data) {
                                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
}
 


