//
//  SelectAccountType.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 26/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SelectAccountType: SignUpVideoController {

    @IBOutlet var aPakRupeeLabelHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func currentAccountSelected(_ sender: UIButton) {
        
        changeImageWhenButtonPressed(sender: sender)
        
        if aPakRupeeLabelHeightConstraint.constant == -658 {
           aPakRupeeLabelHeightConstraint.constant = -736
        }else if aPakRupeeLabelHeightConstraint.constant == -736{
           aPakRupeeLabelHeightConstraint.constant = -658
        }
        
        // Chaning the Button Image with Animation when user selects
        UIView.animate(withDuration: 0.8, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        
    }
    
    func changeImageWhenButtonPressed(sender: UIButton){
        
        if sender.currentImage == UIImage(named: "Minus"){
            sender.setImage(UIImage(named: "Plus"), for: .normal)
            sender.backgroundColor = UIColor.white
        }else if sender.currentImage == UIImage(named: "Plus"){
            sender.setImage(UIImage(named: "Minus"), for: .normal)
            sender.backgroundColor = UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 1.0)
        }
        
    }
    

}
