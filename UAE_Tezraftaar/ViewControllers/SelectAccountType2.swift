//
//  SelectAccountType2.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 02/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SelectAccountType2: SignUpVideoController {

    // Setting up Following buttons to rotate in ViewWillAppear Functions
    @IBOutlet weak var creditCardArrowButton: UIButton!
    @IBOutlet weak var personalLoanArrowButton: UIButton!
    @IBOutlet weak var insuranceArrowButton: UIButton!
    @IBOutlet weak var autoLoanArrowButton: UIButton!
    
    //
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    let arrowImageAngle = CGAffineTransform(a: -1.0, b: 1.22464679914735e-16, c: -1.22464679914735e-16, d: -1.0, tx: 0.0, ty: 0.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        uiMakeUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Actions
    
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectedAccountType2ToNeedHelp", sender: sender)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if(creditCardArrowButton.transform != arrowImageAngle){
            creditCardArrowButton.transform = arrowImageAngle
        }
        if(personalLoanArrowButton.transform != arrowImageAngle){
            personalLoanArrowButton.transform = arrowImageAngle
        }
        if(insuranceArrowButton.transform != arrowImageAngle){
            insuranceArrowButton.transform = arrowImageAngle
        }
        if(autoLoanArrowButton.transform != arrowImageAngle){
            autoLoanArrowButton.transform = arrowImageAngle
        }
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
    }
}
//MARK: Actions
extension SelectAccountType2{
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func creditCardButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectAccountType2ToSelectAccountType5", sender: sender)
    }
    @IBAction func personalLoanButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectAccountType2ToPersonalLoan", sender: sender)
    }
    @IBAction func insuranceButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectAccountType2ToInsurance", sender: sender)
    }
    @IBAction func autoLoanButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "SelectAccountType2ToAutoLoan", sender: sender)
    }
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType2ToSelectAccountType3", sender: sender)
    }
    
}
