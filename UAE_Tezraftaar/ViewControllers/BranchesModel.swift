//
//  BranchesModel.swift
//  UblLocationSwift
//
//  Created by Basit on 02/01/2018.
//  Copyright © 2018 M3Tech. All rights reserved.
//

import UIKit

class BranchesModel: NSObject {
    
    var cityName : String
    var countryNam : String
    var latitude : String
    var longitude : String
    
    init(cityname : String , countryName : String , latitude : String , longitude : String) {
        
        self.cityName = cityname
        self.countryNam = countryName
        self.latitude = latitude
        self.longitude = longitude
    }

}
