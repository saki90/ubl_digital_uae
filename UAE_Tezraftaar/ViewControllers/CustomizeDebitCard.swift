//
//  CustomizeDebitCard.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 10/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class CustomizeDebitCard:  SignUpVideoController{

    @IBOutlet var customizableDebitCardView: UIView!
    @IBOutlet var nonCustomizableDebitCardView: UIView!
    @IBOutlet weak var enterYourNameHereTextField: UILabel!
    
    @IBOutlet var callButton: UIButton!
    @IBOutlet var finishButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        uiMakeUp()
        //savingDataToContinueApplicaitonPlist()
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flagRead = delegate.reading(fromPlist: "UserInformation.plist")
        
        
        if flagRead != nil {

        guard case _? = flagRead!["NotFound"] else {
            let userName = flagRead!["Name"]
            enterYourNameHereTextField.text! = userName! as! String
            return
        }
        }
            
            
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        //Rounding the Top Right and Top Left Corners of  customizbaleDebitCardView
        let maskPath1 = UIBezierPath(roundedRect: customizableDebitCardView.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 9.0, height: 9.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = customizableDebitCardView.bounds
        maskLayer1.path = maskPath1.cgPath
        customizableDebitCardView.layer.mask = maskLayer1
        
        //Rounding the Top Right and Top Left Corners of  nonCustomizbaleDebitCardView
        let maskPath2 = UIBezierPath(roundedRect: nonCustomizableDebitCardView.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomLeft, .bottomRight], cornerRadii: CGSize(width: 9.0, height: 9.0))
        let maskLayer2 = CAShapeLayer()
        maskLayer2.frame = nonCustomizableDebitCardView.bounds
        maskLayer2.path = maskPath2.cgPath
        nonCustomizableDebitCardView.layer.mask = maskLayer2
        
        //Corner radius to UIButtons
        finishButton.layer.cornerRadius = finishButton.frame.height / 2
        callButton.layer.borderWidth = 1.5
        callButton.layer.borderColor = UIColor.white.cgColor
        callButton.layer.cornerRadius = callButton.frame.height / 2
        
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["continueApplication": 5] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }

    
}

//MARK: - Actions
extension CustomizeDebitCard{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        //Popup aniamtion on TabBar buttons
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "CustomizeDebitCardToNeedhelp", sender: sender)
        
    }
    @IBAction func finishPressed(_ sender: Any) {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["isSignUpCompleted": 1,"hasAccount": 1, "isNetBankingOn": 0] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
        self.performSegue(withIdentifier: "CustomizeYourDebitCardToCreatePin", sender: sender)
        
    }
}
