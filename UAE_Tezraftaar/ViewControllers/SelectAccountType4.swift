//
//  SelectAccountType4.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 02/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SelectAccountType4: SignUpVideoController {
    
    var delegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var addButtonImage: UIImageView!
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    let addButtonImageAngle = CGAffineTransform(a: 0.707106781186548, b: 0.707106781186547, c: -0.707106781186547, d: 0.707106781186548, tx: 0.0, ty: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        
        uiMakeUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if(self.addButtonImage.transform != addButtonImageAngle){
            self.addButtonImage.transform = addButtonImageAngle
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func uiMakeUp(){
        addButton.layer.cornerRadius = addButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        
    }
    
}
//MARK: Actions
extension SelectAccountType4{
    //MARK: Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectedAccountType4ToNeedHelp", sender: sender)
    }
    @IBAction func addButtonPressed(_ sender: UIButton) {
        delegate = UIApplication.shared.delegate as! AppDelegate
        let currentAccount = ["CurrentAccount": 1, "SavingAccount": 0]
        delegate.write(toPlist: currentAccount as [String: Any], plistName: "AccountType.plist")
        let accountType = delegate.reading(fromPlist: "AccountType.plist")
        print(accountType)
        
        self.performSegue(withIdentifier: "SelectAccountType4ToSelectAccountType3", sender: sender)
    }
}
