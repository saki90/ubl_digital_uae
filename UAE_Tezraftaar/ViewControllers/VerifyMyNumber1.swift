//
//  VerifyMyNumber.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 10/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class VerifyMyNumber1: SignUpVideoController {
    
    @IBOutlet weak var statusBarFirstStepView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txtOneOTP: UITextField!
    @IBOutlet weak var txtTwoOTP: UITextField!
    @IBOutlet weak var txtThreeOTP: UITextField!
    @IBOutlet weak var txtFourOTP: UITextField!
    @IBOutlet weak var txtFiveOTP: UITextField!
    @IBOutlet weak var txtSixOTP: UITextField!
    
    @IBOutlet weak var shakeTheDeviceLabel: UILabel!
    @IBOutlet weak var resendOTPButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.txtOneOTP.delegate = self
        self.txtTwoOTP.delegate = self
        self.txtThreeOTP.delegate = self
        self.txtFourOTP.delegate = self
        self.txtFiveOTP.delegate = self
        self.txtSixOTP.delegate = self
        
        uiMakeUp()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //Removing Text from all TextFields on viewAppears
        
        
        
        //animateProgressBar()
        
        txtOneOTP.text = " "
        txtTwoOTP.text = " "
        txtThreeOTP.text = " "
        txtFourOTP.text = " "
        txtFiveOTP.text = " "
        txtSixOTP.text = " "
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        activityIndicator.stopAnimating()
        shakeTheDeviceLabel.alpha = 1
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        txtOneOTP.becomeFirstResponder()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //self.view.endEditing(true)
        
    }
    
    // MARK: - Actions
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func resendOTPButtonPressed(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "VerifyMyNumber1ToVerifyMyNumber2", sender: sender)
        
    }
    
    
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        self.txtOneOTP.resizingFont()
        self.txtOneOTP.layoutIfNeeded()
        self.txtOneOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtTwoOTP.resizingFont()
        self.txtTwoOTP.layoutIfNeeded()
        self.txtTwoOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtThreeOTP.resizingFont()
        self.txtThreeOTP.layoutIfNeeded()
        self.txtThreeOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFourOTP.resizingFont()
        self.txtFourOTP.layoutIfNeeded()
        self.txtFourOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFiveOTP.resizingFont()
        self.txtFiveOTP.layoutIfNeeded()
        self.txtFiveOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtSixOTP.resizingFont()
        self.txtSixOTP.layoutIfNeeded()
        self.txtSixOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        resendOTPButton.resizingFont()
        
    }
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    func animateProgressBar(){
        self.statusBarFirstStepView.alpha = 1.0
        //        self.statusBarFirstStepView.frame = CGRect(x: self.statusBarFirstStepView.frame.origin.x, y: self.statusBarFirstStepView.frame.origin.y, width: 0, height: self.statusBarFirstStepView.frame.height)
        UIView.animate(withDuration: 1.5) {
            self.statusBarFirstStepView.frame = CGRect(x: self.statusBarFirstStepView.frame.origin.x, y: self.statusBarFirstStepView.frame.origin.y, width: self.statusBarFirstStepView.frame.width + 40, height: self.statusBarFirstStepView.frame.height)
            
        }
    }
    
}
extension VerifyMyNumber1: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Hiding the cursor of uitextfields
        txtOneOTP.tintColor = UIColor.clear
        txtTwoOTP.tintColor = UIColor.clear
        txtThreeOTP.tintColor = UIColor.clear
        txtFourOTP.tintColor = UIColor.clear
        txtFiveOTP.tintColor = UIColor.clear
        txtSixOTP.tintColor = UIColor.clear
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print(string.count)
        print(textField.tag)
        
        if (string.count == 1){
            print("first if")
            
            textField.text = ""
            textField.text = string
            
            let nextTag = textField.tag + 1;
            
            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag);
            
            if (nextResponder == nil){
                view.endEditing(true)
                UIView.animate(withDuration: 0.5, animations: {
                    self.activityIndicator.alpha = 1
                    self.activityIndicator.startAnimating();
                    self.shakeTheDeviceLabel.alpha = 0.0
                    
                })
                
                //self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
            }
            
            nextResponder?.becomeFirstResponder();
            
            print("//")
            print(string.count)
            print(nextTag)
            return false;
        }else if (textField.tag == 0){
            print("second else")
            
            textField.text = " "
            
            return false;
        }else if(string.count == 0 && textField.tag != 1){
            print("Third else")
            
            textField.text = " "
            let previousTag = textField.tag - 1;
            
            // get previous responder
            let previousResponder = textField.superview?.viewWithTag(previousTag);
            
            
            previousResponder?.becomeFirstResponder()
            
            let textField = previousResponder?.viewWithTag(previousTag) as! UITextField
            textField.text = " "
            
            print("//")
            print(string.count)
            print(previousTag)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.tag == 6 && textField.text != " "){
            
            UIView.animate(withDuration: 0.5, animations: {
                self.activityIndicator.alpha = 1
                self.activityIndicator.startAnimating();
                self.shakeTheDeviceLabel.alpha = 0.0
                
            })
            self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
        }
    }
    
    @objc func nextController(){
        performSegue(withIdentifier: "VerifyMyNumber1ToSelectAccountType1", sender: self)
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if (motion == .motionShake){
            txtOneOTP.text = " "
            txtTwoOTP.text = " "
            txtThreeOTP.text = " "
            txtFourOTP.text = " "
            txtFiveOTP.text = " "
            txtSixOTP.text = " "
            
            txtOneOTP.becomeFirstResponder()
            shakeAnimation()
        }
    }
    func shakeAnimation(){
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x - 10, y: shakeTheDeviceLabel.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x + 10, y: shakeTheDeviceLabel.center.y))
        shakeTheDeviceLabel.layer.add(animation, forKey: "position")
        
    }
    
    
    
}
