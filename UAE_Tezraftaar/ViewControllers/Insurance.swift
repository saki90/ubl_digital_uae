//
//  Insurance.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 21/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class Insurance: SignUpVideoController {

    
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    @IBOutlet weak var addButtonImage: UIImageView!
    let addButtonImageAngle = CGAffineTransform(a: 0.707106781186548, b: 0.707106781186547, c: -0.707106781186547, d: 0.707106781186548, tx: 0.0, ty: 0.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        uiMakeUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if(self.addButtonImage.transform != addButtonImageAngle){
            self.addButtonImage.transform = addButtonImageAngle
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Custom Functions
    func uiMakeUp(){
        
        addButton.layer.cornerRadius = addButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        
        
    }

}
extension Insurance{
    // Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func addButtonSelected(_ sender: UIButton) {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.globarVariableForAdditionalServices.updateValue("Bancassurance", forKey: "Insurance")
        print(delegate.globarVariableForAdditionalServices)
        self.performSegue(withIdentifier: "InsuranceToSelectAccountType3", sender: sender)
    }
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "InsuranceToNeedHelp", sender: sender)
        
    }
}
