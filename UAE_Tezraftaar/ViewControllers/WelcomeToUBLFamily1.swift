//
//  WelcomeToUBLFamily1.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 06/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class WelcomeToUBLFamily1: SignUpVideoController {

    @IBOutlet weak var morningButton: UIButton!
    @IBOutlet weak var afternoonButton: UIButton!
    @IBOutlet weak var eveningButton: UIButton!
    
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var needHelpButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        uiMakeUp()
        //savingDataToContinueApplicaitonPlist()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    

    // Custom Function
    func uiMakeUp(){
        
        finishButton.layer.cornerRadius = self.finishButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = self.needHelpButton.frame.height / 2
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        needHelpButton.layer.borderWidth = 1.5
        
        morningButton.layer.cornerRadius = self.morningButton.frame.height / 2
        morningButton.layer.borderColor = UIColor.white.cgColor
        morningButton.layer.borderWidth = 1.5
        
        afternoonButton.layer.cornerRadius = self.afternoonButton.frame.height / 2
        afternoonButton.layer.borderColor = UIColor.white.cgColor
        afternoonButton.layer.borderWidth = 1.5
        
        eveningButton.layer.cornerRadius = self.eveningButton.frame.height / 2
        eveningButton.layer.borderColor = UIColor.white.cgColor
        eveningButton.layer.borderWidth = 1.5
        
    }
    
    func changingButtonImageWhenPressed(sender: UIButton){
        
        if sender.currentImage == UIImage.init(named: "Tick-Small") {
            
            sender.setImage(nil, for: .normal)
            sender.backgroundColor = UIColor.clear
            
        }else if (sender.currentImage == nil){
            
            sender.setImage( UIImage(named:"Tick-Small"), for: .normal)
            sender.backgroundColor = UIColor.white
            
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["continueApplication": 4] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
    }
}
//MARK: Action
extension WelcomeToUBLFamily1{
    //MARK: - Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "WelcomeToUblFamily1ToNeedHelp", sender: sender)
    }
    @IBAction func morningSelected(_ sender: UIButton) {
        changingButtonImageWhenPressed(sender: sender)
        
        afternoonButton.setImage(nil, for: .normal)
        afternoonButton.backgroundColor = UIColor.clear
        
        eveningButton.setImage(nil, for: .normal)
        eveningButton.backgroundColor = UIColor.clear
    }
    @IBAction func afternoonSelected(_ sender: UIButton) {
        changingButtonImageWhenPressed(sender: sender)
        
        morningButton.setImage(nil, for: .normal)
        morningButton.backgroundColor = UIColor.clear
        
        eveningButton.setImage(nil, for: .normal)
        eveningButton.backgroundColor = UIColor.clear
    }
    @IBAction func eveningSelected(_ sender: UIButton) {
        changingButtonImageWhenPressed(sender: sender)
        
        morningButton.setImage(nil, for: .normal)
        morningButton.backgroundColor = UIColor.clear
        
        afternoonButton.setImage(nil, for: .normal)
        afternoonButton.backgroundColor = UIColor.clear
    }
    @IBAction func finishButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "WelcomeToUBLFamily1ToWelcomeToUBLFamily2", sender: sender)
    }
}
