//
//  NeedHelp.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 26/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn3: SignInVideoController {

    
    @IBOutlet weak var changeToLoginPasswordButton: UIButton!
    @IBOutlet weak var activateYourUblDigitalAppButton: UIButton!
    @IBOutlet weak var applyForAnAccountButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiMakeUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Custom Functions
    func uiMakeUp(){
        // Rounding the edges of buttons and resizing their fonts
        changeToLoginPasswordButton.layer.cornerRadius = 7
        applyForAnAccountButton.layer.cornerRadius = applyForAnAccountButton.frame.height / 2
        
        changeToLoginPasswordButton.resizingFont()
        activateYourUblDigitalAppButton.resizingFont()
        applyForAnAccountButton.resizingFont()
    }
}
// MARK: - Actions
extension TouchIdSignIn3{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func applyForAnAccountButtonPressed(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "TouchIdSignIn3ToSignUpForm", sender: sender)
        
    }
    
}
