//
//  TouchIdSignIn2.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 27/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn2: SignInVideoController {
    
    @IBOutlet weak var loginPasswordView: UIView!
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet weak var changeToTouchIdButton: UIButton!
    @IBOutlet weak var trackMyApplicationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        loginTextField.delegate  = self
        passwordTextField.delegate  = self
        
        uiMakeUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn2.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn2.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Setting up bottom border to TextFields
        self.loginTextField.resizingFont()
        self.loginTextField.layoutIfNeeded()
        loginTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.passwordTextField.resizingFont()
        self.passwordTextField.layoutIfNeeded()
        passwordTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        // Rounding the edges of Buttons and resizing the font
        nextButton.resizingFont()
        nextButton.layer.cornerRadius = 9
        
        changeToTouchIdButton.resizingFont()
        changeToTouchIdButton.layer.cornerRadius = 7
        
        loginPasswordView.layer.cornerRadius = 9
        loginPasswordView.layer.borderWidth = 1
        loginPasswordView.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35).cgColor
        
        // Underlining the Button Text
        let definedAttributes : [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        //.styleDouble.rawValue, .styleThick.rawValue, .styleNone.rawValue
  
        
       // forgotPasswordButton.resizingFont()
        let forgotPasswordButtonAttributedString = NSMutableAttributedString(string: "Track my Application", attributes: definedAttributes)
        trackMyApplicationButton.setAttributedTitle(forgotPasswordButtonAttributedString, for: .normal)
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 145
        }
        
    }
    
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 145
        }
    }
}
//MARK: - Actions
extension TouchIdSignIn2{
    
    // MARK: Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func trackMyApplication(_ sender: UIButton) {
        self.performSegue(withIdentifier: "TouchIdSignInToViewProgress1", sender: sender)
    }
    
    @IBAction func changeToTouchIdSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "TouchIdSignIn2ToTouchIdSignIn4", sender: sender)
    }
}

extension TouchIdSignIn2: UITextFieldDelegate{
    // Setting the TextField placeholder to Empty once a user starts typing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // Hiding the placeholder when user starts typing
        if (loginTextField.isEditing == true){
            loginTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(passwordTextField.isEditing == true){
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Displaying placeholder when user end editing or touches everywhere on the screen
        if(loginTextField.isEditing == false){
            //loginTextField.placeholder = "Login"
            loginTextField.attributedPlaceholder = NSAttributedString(string: "Login", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(passwordTextField.isEditing == false){
            //passwordTextField.placeholder = "Password"
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if loginTextField.returnKeyType == UIReturnKeyType.next{
            passwordTextField.becomeFirstResponder()
        }else if passwordTextField.returnKeyType == UIReturnKeyType.done {
            textField.endEditing(true)
        }
        
        return false
    }
}
