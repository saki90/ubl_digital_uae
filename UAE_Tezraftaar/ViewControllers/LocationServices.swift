//
//  LocationServices.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 08/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit
import CoreLocation

class LocationServices: SignUpVideoController {
    
    @IBOutlet weak var locationServicesAllowsMapsLabel: UILabel!
    @IBOutlet weak var proceedWithoutLocation: UIButton!
    @IBOutlet weak var enableLocationServices: UIButton!
    @IBOutlet var EnableLocationView: UIView!
    var locationManager = CLLocationManager()
    var locationFlag : Bool = false
    var gbclass : GlobalStaticClass = GlobalStaticClass.getInstance()
    var timer = Timer()
    
    
    @IBAction func hideEnableLocationView(_ sender: Any) {
        self.EnableLocationView.isHidden = true
    }
    
    @IBAction func without_loc(_ sender: Any) {
        self.performSegue(withIdentifier: "LocationServicesToLetsMeet", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "LocationServicesToLetsMeet" {
            let vc:LetsMeet = segue.destination as! LetsMeet
            
            if !locationFlag{
                vc.pwl_flag = true
            }
            
        }
    }
    
    fileprivate func enableLocation() {
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .authorizedWhenInUse  {
            
            if let url = URL(string: UIApplicationOpenSettingsURLString){
                if #available(iOS 10.0, *){
                    UIApplication.shared.open(url, completionHandler: { (success) in
                        print(" Profile Settings opened: \(success)")
                    })
                } else{
                    UIApplication.shared.openURL(url)
                    print("\n--- in ios other than 10 ")
                }
            }
        } else if !CLLocationManager.locationServicesEnabled() {
            
            //Do something if no location services are not enabled
            self.fadeInAnimate(view: self.EnableLocationView)
            self.EnableLocationView.isHidden = false
        }
    }
    
    @IBAction func enable_Action(_ sender: Any) {
        enableLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gbclass.controllers_Refrence_Stack.add(self)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        uiMakeUp()
        //        timer =  Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(LocationServices.isLocationServiceOn), userInfo: nil, repeats: true)
        
        
    }
    
    fileprivate func fadeInAnimate(view:UIView) {
        let animation = CATransition()
        animation.type = kCATransitionFade;
        animation.duration = 0.4;
        view.layer.add(animation, forKey: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: Custom Functions
    func uiMakeUp(){
        
        // Setting attributed Text to label
        
        //    let attributedString = NSMutableAttributedString(string: "Location Services allows Maps and other apps and services like Find My iPhone to gather and use data indicating your approximate location.\n\nAbout Location Services")
        //        attributedString.addAttribute(.font, value: UIFont(name: "Aspira-Bold", size: 16.0)!, range: NSRange(location: 140, length: 23))
        //        locationServicesAllowsMapsLabel.attributedText = attributedString
        
        enableLocationServices.resizingFont()
        proceedWithoutLocation.resizingFont()
        
    }
    
    func slideLeft() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    func slideRight() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    
    @objc func isLocationServiceOn() {
        
        if(CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == .authorizedWhenInUse){
            locationFlag = true
            self.performSegue(withIdentifier: "LocationServicesToLetsMeet", sender: self)
            
        }  else {
            //Do something if no location services are not enabled
            let alert = UIAlertController(title: "Location Services", message: "Please turn on location services so we can recommend you the nearest branch or select your preferred branch." , preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                
            }
            
            //            let selectBranchAction = UIAlertAction(title: "Select Branch", style: UIAlertActionStyle.default) {
            //                UIAlertAction in
            //                NSLog("selectBranchPressed")
            //            }
            //
            //
            //            alert.addAction(selectBranchAction)
            //            alert.addAction(okAction)
            //            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func stopTimerFunction(){
        timer.invalidate()
        timer = Timer()
    }
    
}
//MARK: - Actions
extension LocationServices {
    
    
    // MARK: Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //////////************************** START TABBAR CONTROLS ***************************///////////
    
    @IBAction func btn_feature(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service Unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        //    let first_time_chk = self.defaults.string(forKey: "enable_touch")
        //        if first_time_chk == "1" || first_time_chk! == "11" {
        //            [hud showAnimated:YES];
        //            if (netAvailable) {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        } else {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please register your User." :@"0"];
        //        }
    }
    
    @IBAction func btn_offer(_ sender: UIButton) {
        
        gbclass.tab_bar_login_pw_check="login";
        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
            "peekabooEnvironment" : "production",
            "peekabooSdkId" : "UBL_SDK",
            "peekabooTitle" : "UBL Bank Offers",
            "peekabooSplashImage" : "true",
            "peekabooWelcomeMessage" : "Please select your City to view deals and discounts offered to UBL customers",
            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
            "peekabooPoweredByFooter" : "false",
            "peekabooColorPrimary" : "#007DC0",
            "peekabooColorPrimaryDark" : "#005481",
            "peekabooColorStatus" : "#2d2d2d",
            "peekabooColorSplash" : "#004681",
            "peekabooType": "deals"], launchOptions: nil)
        
        let vc = UIViewController()
        vc.view = rootView
        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_find_us(_ sender: UIButton) {
        
        gbclass.tab_bar_login_pw_check="login";
        gbclass.tab_bar_login_pw_check="login";
        
        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
            "peekabooEnvironment" : "production",
            "peekabooSdkId" : "UBL_SDK",
            "peekabooTitle" : "UBL Bank Offers",
            "peekabooSplashImage" : "true",
            "peekabooWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
            "peekabooPoweredByFooter" : "false",
            "peekabooColorPrimary" : "#007DC0",
            "peekabooColorPrimaryDark" : "#005481",
            "peekabooColorStatus" : "#2d2d2d",
            "peekabooColorSplash" : "#004681",
            "peekabooType": "locator",
            "peekabooLocatorWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
            "peekabooLocatorTitle" : "Locate us",
            ], launchOptions: nil)
        
        let vc = UIViewController()
        vc.view = rootView
        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_faq(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "faq")
        self.slideLeft()
        self.present(controller, animated: false, completion: nil)
    }
    
    //////////************************** END TABBAR CONTROLS ***************************///////////
    
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension LocationServices: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        print("didChangeAuthorizationCalled\(CLLocationManager.authorizationStatus())")
        
        if(CLLocationManager.locationServicesEnabled() && status == .authorizedWhenInUse) {
            locationFlag = true
            self.performSegue(withIdentifier: "LocationServicesToLetsMeet", sender: self)
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
}


