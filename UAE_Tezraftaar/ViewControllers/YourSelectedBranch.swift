//
//  YourSelectedBranch.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 24/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit
import GoogleMaps

class YourSelectedBranch: SignUpVideoController {

    @IBOutlet weak var getDirectionsButton: UIButton!
    
    var myLocation: CLLocation!
    var locationManager = CLLocationManager()
     var didFindMyLocation = false
    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self as? GMSMapViewDelegate
        mapView.addSubview(getDirectionsButton)
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.new, context: nil)
        
        mapMarkers()
        uiMakeUp()
        
        
        do {

            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("The style definition could not be loaded: \(error)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mapMarkers(){
        let coordinates: [CLLocationCoordinate2D] = [
            CLLocationCoordinate2D.init(latitude: 24.970329, longitude: 67.067173)
        ]
        for coordinate in coordinates {
            let marker = GMSMarker(position: coordinate)
            marker.iconView = UIImageView(image: #imageLiteral(resourceName: "MarkerIcon"))
            marker.title = "UBL"
            marker.map = self.mapView
            marker.appearAnimation = GMSMarkerAnimation.pop
        }
    }
    func uiMakeUp(){
        getDirectionsButton.layer.cornerRadius = 7
    }
}
//MARK: - Actions
extension YourSelectedBranch{
    
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func getDirectionSelected(_ sender: UIButton) {
        
        //        UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)
        //
        //        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
        //            UIApplication.shared.openURL(URL(string:
        //                "comgooglemaps://?center=40.765819,-73.975866&zoom=14&views=traffic")!)
        //        } else {
        //            print("Can't use comgooglemaps://");
        //        }
        let testURL = URL(string: "comgooglemaps://")!
        if UIApplication.shared.canOpenURL(testURL) {
            let directionsRequest = "comgooglemaps://" + "?daddr=24.970329,67.067173"
            
            let directionsURL = URL(string: directionsRequest)!
            UIApplication.shared.openURL(directionsURL)
        } else {
            let alert = UIAlertController(title: "Oops!", message: "Having difficulty connecting with Google maps services", preferredStyle: UIAlertControllerStyle.alert)
            let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
            }
            
            alert.addAction(dismissAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        //        let testURL = URL(string: "http://maps.google.com/maps?f=d&daddr=Tokyo+Tower,+Tokyo,+Japan&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1")!
        //        if UIApplication.shared.canOpenURL(testURL) {
        //            let directionsRequest = "https://www.google.com/maps/dir/?api=1&origin=Space+Needle+Seattle+WA&destination=Pike+Place+Market+Seattle+WA&travelmode=bicycling"
        //
        //            let directionsURL = URL(string: directionsRequest)!
        //            UIApplication.shared.openURL(directionsURL)
        //        } else {
        //            NSLog("Can't use comgooglemaps-x-callback:// on this device.")
        //        }
        
    }
}
extension YourSelectedBranch: CLLocationManagerDelegate{
    // MARK: - CLLocationManagerDelegate functions
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse{
            mapView.isMyLocationEnabled = true
            
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation{
            
            
            
            
            //myLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            // uncomment to animate for start position
            //            CATransaction.begin()
            //            CATransaction.setValue(3.0, forKey: kCATransactionAnimationDuration)
            
            // Users current locaiton
            //mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
            mapView.camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D.init(latitude: 24.970329, longitude: 67.067173) , zoom: 15, bearing: 0, viewingAngle: 0)
            //            self.mapView.animate(toZoom: 15)
            //            self.mapView.animate(toViewingAngle: 65)
            //            self.mapView.animate(toBearing: 120)
            
            CATransaction.commit()
            
            
            
            didFindMyLocation = true
            print("observeValue working")
        }
    }
    
}
