//
//  AccountType.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 25/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SelectAccountType1: SignUpVideoController {

    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    @IBOutlet weak var currentAccountArrowButton: UIButton!
    @IBOutlet weak var savingAccountArrowButton: UIButton!
    
    
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    let arrowImageAngle = CGAffineTransform(a: -1.0, b: 1.22464679914735e-16, c: -1.22464679914735e-16, d: -1.0, tx: 0.0, ty: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
       
        uiMakeUp()
        //savingDataToContinueApplicaitonPlist()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        // Rotating the buttons
        if(currentAccountArrowButton.transform != arrowImageAngle){
            currentAccountArrowButton.transform = arrowImageAngle
        }
        if(savingAccountArrowButton.transform != arrowImageAngle){
            savingAccountArrowButton.transform = arrowImageAngle
        }
    }
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        
    }
    func changeImageWhenButtonPressed(sender: UIButton){
        
        if sender.currentImage == UIImage(named: "Tick-Small-White"){
            sender.setImage(UIImage(named: "Plus"), for: .normal)
            sender.backgroundColor = UIColor.white
        }else if sender.currentImage == UIImage(named: "Plus"){
            sender.setImage(UIImage(named: "Tick-Small-White"), for: .normal)
            sender.backgroundColor = UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 1.0)
        }
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["continueApplication": 2] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }
}
extension SelectAccountType1{
    
    // MARK: - Actions
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func currentAccountSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType1ToSelectAccountType4", sender: sender)
    }
    @IBAction func savingAccountSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType1ToSavingAccount", sender: sender)
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType1ToFindPlace", sender: sender)
    }
    
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType1ToNeedHelp", sender: sender)
    }
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
}
