//
//  TouchIdSignIn4.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 12/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn4: SignInVideoController {
    
    @IBOutlet weak var changeToLoginPasswordButton: UIButton!
    @IBOutlet weak var trackMyApplication: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        uiMakeUp()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Custom Functions
    func uiMakeUp(){
        // Rounding the edges
        changeToLoginPasswordButton.layer.cornerRadius = 7
        
        // Resizing font
        trackMyApplication.resizingFont()
        changeToLoginPasswordButton.resizingFont()
    }
}
// MARK: Actions
extension TouchIdSignIn4{
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func changeToLoginPasswordSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "TouchIdSignIn4ToTouchIdSignIn2", sender: sender)
    }
    @IBAction func trackMyApplication(_ sender: UIButton) {
        self.performSegue(withIdentifier: "TouchIdSignIn4ToViewProgress1", sender: sender)
    }
}
