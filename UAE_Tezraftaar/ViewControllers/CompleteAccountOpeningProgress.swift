//
//  CompleteAccountOpeningProgress.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 06/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class CompleteAccountOpeningProgress: SignInVideoController {

    @IBOutlet weak var youHaveBeenAssignedAUBLRepresentative: UIView!
    @IBOutlet weak var meetingWithTheRepresentativeSetup: UIView!
    @IBOutlet weak var CongratsAccountNumberIsCreated: UIView!
    @IBOutlet weak var AccountActivated: UIView!
    @IBOutlet weak var welcomePackDispatched: UIView!
    @IBOutlet weak var welcomePackDelivered: UIView!
    @IBOutlet weak var callUBLToActivateYourDebitCard: UIView!
    @IBOutlet weak var accountFullyActivated: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        uiMakeUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: Custom Functions
    func uiMakeUp(){
        
        youHaveBeenAssignedAUBLRepresentative.layer.cornerRadius = youHaveBeenAssignedAUBLRepresentative.frame.height / 2
        youHaveBeenAssignedAUBLRepresentative.layer.borderWidth = 2
        youHaveBeenAssignedAUBLRepresentative.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        youHaveBeenAssignedAUBLRepresentative.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        youHaveBeenAssignedAUBLRepresentative.animateCircle(duration: 0.5, divideCircleBy: 8)

        
        meetingWithTheRepresentativeSetup.layer.cornerRadius = meetingWithTheRepresentativeSetup.frame.height / 2
        meetingWithTheRepresentativeSetup.layer.borderWidth = 2
        meetingWithTheRepresentativeSetup.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        meetingWithTheRepresentativeSetup.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        meetingWithTheRepresentativeSetup.animateCircle(duration: 0.5, divideCircleBy: 7)
        
        CongratsAccountNumberIsCreated.layer.cornerRadius = CongratsAccountNumberIsCreated.frame.height / 2
        CongratsAccountNumberIsCreated.layer.borderWidth = 2
        CongratsAccountNumberIsCreated.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        CongratsAccountNumberIsCreated.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        CongratsAccountNumberIsCreated.animateCircle(duration: 0.5, divideCircleBy: 6)
        
        AccountActivated.layer.cornerRadius = accountFullyActivated.frame.height / 2
        AccountActivated.layer.borderWidth = 2
        AccountActivated.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        AccountActivated.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        AccountActivated.animateCircle(duration: 0.5, divideCircleBy: 5)
        
        welcomePackDispatched.layer.cornerRadius = welcomePackDispatched.frame.height / 2
        welcomePackDispatched.layer.borderWidth = 2
        welcomePackDispatched.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        welcomePackDispatched.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        welcomePackDispatched.animateCircle(duration: 0.5, divideCircleBy: 4)
        
        welcomePackDelivered.layer.cornerRadius = welcomePackDelivered.frame.height / 2
        welcomePackDelivered.layer.borderWidth = 2
        welcomePackDelivered.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        welcomePackDelivered.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        welcomePackDelivered.animateCircle(duration: 0.5, divideCircleBy: 3)
        
        callUBLToActivateYourDebitCard.layer.cornerRadius = callUBLToActivateYourDebitCard.frame.height / 2
        callUBLToActivateYourDebitCard.layer.borderWidth = 2
        callUBLToActivateYourDebitCard.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        callUBLToActivateYourDebitCard.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        callUBLToActivateYourDebitCard.animateCircle(duration: 0.5, divideCircleBy: 2)
        
        
        
        accountFullyActivated.addCircleView(borderWidth: 1, color: UIColor.white.cgColor)
        accountFullyActivated.animateCircle(duration: 0.5, divideCircleBy: 1)
        
        accountFullyActivated.layer.cornerRadius = accountFullyActivated.frame.height / 2
        accountFullyActivated.layer.borderWidth = 2
        accountFullyActivated.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
    }
}
//MARK: - Actions
extension CompleteAccountOpeningProgress{
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
