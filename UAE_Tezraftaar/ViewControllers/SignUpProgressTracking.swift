//
//  SignUpProgressTracking.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 11/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SignUpProgressTracking: SignInVideoController {
    
    @IBOutlet var outerCircleView: UIView!
    @IBOutlet var innerCircleView: UIView!
    @IBOutlet weak var progressTrackingLabel: UILabel!
    
    @IBOutlet weak var haveAnAccountLabel: UIButton!
    @IBOutlet weak var documentsRequiredButton: UIButton!
    @IBOutlet var callButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        uiMakeUp()
        innerCircleView.addSubview(progressTrackingLabel)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom functions
    func uiMakeUp(){
        
        innerCircleView.addCircleView(borderWidth: 7, color: UIColor(white: 1, alpha: 0.5).cgColor)
        innerCircleView.animateCircle(duration: 1.5, divideCircleBy: 1)
        innerCircleView.addCircleView(borderWidth: 7, color: UIColor.white.cgColor)
        innerCircleView.animateCircle(duration: 1.5, divideCircleBy: 2)
        
        outerCircleView.layer.cornerRadius = outerCircleView.frame.height / 2
        outerCircleView.layer.borderWidth = 1
        outerCircleView.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        
        haveAnAccountLabel.resizingFont()
        
        callButton.layer.cornerRadius = callButton.frame.height / 2
        callButton.layer.borderColor = UIColor.white.cgColor
        callButton.layer.borderWidth = 1.5
        
        documentsRequiredButton.layer.cornerRadius = self.documentsRequiredButton.frame.height / 2
        documentsRequiredButton.layer.borderColor = UIColor.white.cgColor
        documentsRequiredButton.layer.borderWidth = 1.5
        
    }
}
//MARK: Actions
extension SignUpProgressTracking{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        //Popup animation on TabBar buttons
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func haveAnAccountSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "SignUpProgressTrackingToToucIdSignIn2", sender: sender)
        
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "SignUpProgressTrackingToNeedHelp", sender: sender)
        
    }
    
    @IBAction func documentsRequiredSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "SignUpProgressTrackingToRequiredDocuments", sender: sender)
        
    }
    @IBAction func progressTrackingLabelGesture(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "SignUpProgressTrackingToCompleteAccountOpeningProgress", sender: sender)
    }
    
}
