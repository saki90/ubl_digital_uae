//
//  SignUpForm.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 12/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SignUpForm: SignUpVideoController {
    
    
    @IBOutlet weak var statusBarFirstStepView: UIView!
    
    
    // UITextField Outlets
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var cnicTextField: UITextField!
    @IBOutlet var mobileTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    
    let initialString = "my question to be answered"
    var inputString = NSString()
    
    
    // UIButton Outlets
    @IBOutlet var yesButton: UIButton!
    @IBOutlet var noButton: UIButton!
    
    
    @IBOutlet var nextButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layoutIfNeeded()
        
        self.nameTextField.delegate = self
        self.cnicTextField.delegate = self
        self.mobileTextField.delegate = self
        self.emailTextField.delegate = self
        
        uiMakeUp()
        
        //savingDataToAppLaunchStatusPlist()
        //savingDataToContinueApplicaitonPlist()
        
        //Saving flag for appLaunchStatu
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flag = ["isSignUpCompleted": 0,"hasAccount": 0, "isNetBankingOn": 0] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        //animateProgressBar()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    
    // MARK: - Acitons
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func previousController(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flag = ["Name": nameTextField.text!,"CNIC": cnicTextField.text!, "Mobile": mobileTextField.text!,"Email": emailTextField.text!] as Any!
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "UserInformation.plist")
        let flagRead = delegate.reading(fromPlist: "UserInformation.plist")
        print(flagRead)
        performSegue(withIdentifier: "SignUpFormToVerifyMyNumber", sender: sender)
        
    }
    @IBAction func yesSelected(_ sender: UIButton) {
        changingButtonImageWhenPressed(sender: sender)
        noButton.backgroundColor = UIColor.clear
        noButton.setImage(nil, for: .normal)
        
        
    }
    @IBAction func noSelected(_ sender: UIButton) {
        
        changingButtonImageWhenPressed(sender: sender)
        yesButton.backgroundColor = UIColor.clear
        yesButton.setImage(nil, for: .normal)
    }
    
    // MARK: - Customization
    
    func uiMakeUp(){
        
        // Resizing font according to Screen size and adding bottom border to UITextFields
        self.nameTextField.resizingFont()
        self.nameTextField.layoutIfNeeded()
        self.nameTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.cnicTextField.resizingFont()
        self.cnicTextField.layoutIfNeeded()
        self.cnicTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.mobileTextField.resizingFont()
        self.mobileTextField.layoutIfNeeded()
        self.mobileTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.emailTextField.resizingFont()
        self.emailTextField.layoutIfNeeded()
        self.emailTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpForm.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpForm.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Rounding the Next Button
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        
        //Rounding the Yes and No Button
        yesButton.layer.cornerRadius = yesButton.frame.height / 2
        noButton.layer.cornerRadius = noButton.frame.height / 2
        
        yesButton.layer.borderWidth = 1.5
        noButton.layer.borderWidth = 1.5
        
        yesButton.layer.borderColor = UIColor.white.cgColor
        noButton.layer.borderColor = UIColor.white.cgColor
        
        
        
        
        // Adding Bottom line to UITextFields
        //        nameTextField.ty
        
        
        
        
        
    }
    // moving the whole view up with keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 145
        }
        
        
    }
    
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 145
        }
        
        
    }
    func changingButtonImageWhenPressed(sender: UIButton){
        
        if sender.currentImage == UIImage.init(named: "Tick-Small") {
            
            sender.setImage(nil, for: .normal)
            sender.backgroundColor = UIColor.clear
            
        }else if (sender.currentImage == nil){
            
            sender.setImage( UIImage(named:"Tick-Small"), for: .normal)
            sender.backgroundColor = UIColor.white
            
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        
        
    }
    func animateProgressBar(){
        self.statusBarFirstStepView.alpha = 1.0
        self.statusBarFirstStepView.frame = CGRect(x: self.statusBarFirstStepView.frame.origin.x, y: self.statusBarFirstStepView.frame.origin.y, width: 0, height: self.statusBarFirstStepView.frame.height)
        UIView.animate(withDuration: 1.5) {
            self.statusBarFirstStepView.frame = CGRect(x: self.statusBarFirstStepView.frame.origin.x, y: self.statusBarFirstStepView.frame.origin.y, width: self.statusBarFirstStepView.frame.width + 40, height: self.statusBarFirstStepView.frame.height)
            
        }
    }
    
    func savingDataToAppLaunchStatusPlist(){
        
        
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["continueApplication": 1] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }
    
    
}
extension SignUpForm: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField.tag == 2) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            
            if(textField.text?.count == 5 && string.count == 1){
                textField.text = textField.text! + "-" + string
                
                
                return false
            }else if(textField.text?.count == 13 && string.count == 1){
                textField.text = textField.text! + "-" + string
                
                return false
            }
            
            if(textField.text?.count == 7 && string.count == 0){
                
                let firstIndex = textField.text?.index((textField.text?.startIndex)!, offsetBy: 6)
                
                textField.text?.remove(at: firstIndex!)
                
            }else if(textField.text?.count == 15 && string.count == 0){
                
                let firstIndex = textField.text?.index((textField.text?.startIndex)!, offsetBy: 14)
                
                textField.text?.remove(at: firstIndex!)
                
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 15
        }
        
        return true
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        nameTextField.tintColor = UIColor.white
        cnicTextField.tintColor = UIColor.white
        mobileTextField.tintColor = UIColor.white
        emailTextField.tintColor = UIColor.white
        
        // Hiding the placeholder when use starts typing
        if(nameTextField.isEditing == true){
            
            // writeToPlist(data: "", plistName: "UserInformation.plist")
            nameTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(cnicTextField.isEditing == true){
            cnicTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(mobileTextField.isEditing == true){
            mobileTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(emailTextField.isEditing == true){
            emailTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        
    }
    // Setting the TextField placeholder when user end editing
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Displaying placeholder when user end editing or touches everywhere on the screen
        if(nameTextField.isEditing == false){
            //nameTextField.placeholder = "NAME"
            nameTextField.attributedPlaceholder = NSAttributedString(string: "NAME", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(cnicTextField.isEditing == false){
            //cnicTextField.placeholder = "CNIC/SNIC"
            cnicTextField.attributedPlaceholder = NSAttributedString(string: "CNIC/SNIC", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(mobileTextField.isEditing == false){
            //cnicTextField.placeholder = "MOBILE"
            mobileTextField.attributedPlaceholder = NSAttributedString(string: "MOBILE", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(emailTextField.isEditing == false){
            //emailTextField.placeholder = "EMAIL"
            emailTextField.attributedPlaceholder = NSAttributedString(string: "EMAIL", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if nameTextField.returnKeyType == UIReturnKeyType.next{
            cnicTextField.becomeFirstResponder()
        }else if(cnicTextField.returnKeyType == UIReturnKeyType.next){
            mobileTextField.becomeFirstResponder()
        }else if(mobileTextField.returnKeyType ==  UIReturnKeyType.next){
            emailTextField.becomeFirstResponder()
        }else if textField.returnKeyType == UIReturnKeyType.done{
            textField.endEditing(true)
        }
        
        return false
    }
    
    
}
