//
//  CreatePin.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 26/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit
import UserNotifications

class CreatePin: SignUpVideoController {
    
    @IBOutlet var txtOneOTP: UITextField!
    @IBOutlet var txtTwoOTP: UITextField!
    @IBOutlet var txtThreeOTP: UITextField!
    @IBOutlet var txtFourOTP: UITextField!
    @IBOutlet var txtFiveOTP: UITextField!
    @IBOutlet var txtSixOTP: UITextField!
    @IBOutlet weak var invisibleLayerOnTextFields: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var shakeTheDeviceLabel: UILabel!
    
    @IBOutlet var confirmTxtOneOTP: UITextField!
    @IBOutlet var confirmTxtTwoOTP: UITextField!
    @IBOutlet var confirmTxtThreeOTP: UITextField!
    @IBOutlet var confirmTxtFourOTP: UITextField!
    @IBOutlet var confirmTxtFiveOTP: UITextField!
    @IBOutlet var confirmTxtSixOTP: UITextField!
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtOneOTP.delegate = self
        self.txtTwoOTP.delegate = self
        self.txtThreeOTP.delegate = self
        self.txtFourOTP.delegate = self
        self.txtFiveOTP.delegate = self
        self.txtSixOTP.delegate = self
        
        self.confirmTxtOneOTP.delegate = self
        self.confirmTxtTwoOTP.delegate = self
        self.confirmTxtThreeOTP.delegate = self
        self.confirmTxtFourOTP.delegate = self
        self.confirmTxtFiveOTP.delegate = self
        self.confirmTxtSixOTP.delegate = self
        
        
        uiMakeUp()
        //savingDataToContinueApplicaitonPlist()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        txtOneOTP.text = " "
        txtTwoOTP.text = " "
        txtThreeOTP.text = " "
        txtFourOTP.text = " "
        txtFiveOTP.text = " "
        txtSixOTP.text = " "
        
        
        confirmTxtOneOTP.text = " "
        confirmTxtTwoOTP.text = " "
        confirmTxtThreeOTP.text = " "
        confirmTxtFourOTP.text = " "
        confirmTxtFiveOTP.text = " "
        confirmTxtSixOTP.text = " "
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        shakeTheDeviceLabel.alpha = 1.0
        activityIndicator.stopAnimating()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtOneOTP.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //self.view.endEditing(true)
    }
   
    
    //MARK: - Custom Functions
    func uiMakeUp(){
        
        
        
        self.txtOneOTP.resizingFont()
        self.txtOneOTP.layoutIfNeeded()
        self.txtOneOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtTwoOTP.resizingFont()
        self.txtTwoOTP.layoutIfNeeded()
        self.txtTwoOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtThreeOTP.resizingFont()
        self.txtThreeOTP.layoutIfNeeded()
        self.txtThreeOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFourOTP.resizingFont()
        self.txtFourOTP.layoutIfNeeded()
        self.txtFourOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFiveOTP.resizingFont()
        self.txtFiveOTP.layoutIfNeeded()
        self.txtFiveOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtSixOTP.resizingFont()
        self.txtSixOTP.layoutIfNeeded()
        self.txtSixOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtOneOTP.resizingFont()
        self.confirmTxtOneOTP.layoutIfNeeded()
        self.confirmTxtOneOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtTwoOTP.resizingFont()
        self.confirmTxtTwoOTP.layoutIfNeeded()
        self.confirmTxtTwoOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtThreeOTP.resizingFont()
        self.confirmTxtThreeOTP.layoutIfNeeded()
        self.confirmTxtThreeOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtFourOTP.resizingFont()
        self.confirmTxtFourOTP.layoutIfNeeded()
        self.confirmTxtFourOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtFiveOTP.resizingFont()
        self.confirmTxtFiveOTP.layoutIfNeeded()
        self.confirmTxtFiveOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.confirmTxtSixOTP.resizingFont()
        self.confirmTxtSixOTP.layoutIfNeeded()
        self.confirmTxtSixOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
    }
    func notificationAuthorization(){
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound], completionHandler: {
                (granted, error) in
                
                if granted{
                    print("You will receive notification of your pin")
                }else{
                    print(error?.localizedDescription as Any)
                }
                
            })
        } else {
            // Fallback on earlier versions
        }
        
    }
    func generateNotification() {
        
        //let userInformation = delegate.readingFromPlist(plistName: "UserInformation.plist")
        let userPin = delegate.reading(fromPlist: "UserPin.plist")
        //let name = userInformation["Name"] as! String
        
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = "Dear<Awais>"
            content.subtitle = ""
            content.body = "Your new account request#<Reference no.> has been received. The 6 digits PIN to track your account opening progress is <\(userPin)>"
            content.categoryIdentifier = "newCuddlePix"
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest(identifier:  "identifier1", content: content, trigger: trigger)
            
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
        } else {
            // Fallback on earlier versions
        }
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let flag = ["continueApplication": 6] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }
    
    
    
}
//MARK: - Actions
extension CreatePin{
    //MARK: - Actions
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
}
extension CreatePin: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txtOneOTP.tintColor = UIColor.clear
        txtTwoOTP.tintColor = UIColor.clear
        txtThreeOTP.tintColor = UIColor.clear
        txtFourOTP.tintColor = UIColor.clear
        txtFiveOTP.tintColor = UIColor.clear
        txtSixOTP.tintColor = UIColor.clear
        
        
        confirmTxtOneOTP.tintColor = UIColor.clear
        confirmTxtTwoOTP.tintColor = UIColor.clear
        confirmTxtThreeOTP.tintColor = UIColor.clear
        confirmTxtFourOTP.tintColor = UIColor.clear
        confirmTxtFiveOTP.tintColor = UIColor.clear
        confirmTxtSixOTP.tintColor = UIColor.clear
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(string.count)
        print(textField.tag)
    
        if (string.count == 1){
            print("first if")

            textField.text = ""
            textField.text = string

            let nextTag = textField.tag + 1;

            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag);

            if (nextResponder == nil){
                view.endEditing(true)
                UIView.animate(withDuration: 0.5, animations: {
                    self.activityIndicator.alpha = 1
                    self.activityIndicator.startAnimating();
                    self.shakeTheDeviceLabel.alpha = 0.0
                })
                //self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
            }
            nextResponder?.becomeFirstResponder();

            print("//")
            print(string.count)
            print(nextTag)
            return false;
        }else if (textField.tag == 0){
            print("second else")

            textField.text = " "

            return false;
        }else if(string.count == 0 && textField.tag != 1){
            print("Third else")

            textField.text = " "
            let previousTag = textField.tag - 1;

            // get previous responder
            let previousResponder = textField.superview?.viewWithTag(previousTag);


            previousResponder?.becomeFirstResponder()

            let textField = previousResponder?.viewWithTag(previousTag) as! UITextField
            textField.text = " "

            print("//")
            print(string.count)
            print(previousTag)
            return false
        }
        return true
    }
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if(textField.tag == 12){
            if(txtOneOTP.text! == confirmTxtOneOTP.text! && txtTwoOTP.text! == confirmTxtTwoOTP.text! && txtThreeOTP.text! == confirmTxtThreeOTP.text! && txtFourOTP.text! == confirmTxtFourOTP.text! && txtFiveOTP.text! == confirmTxtFiveOTP.text! && txtSixOTP.text! == confirmTxtSixOTP.text!){
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.activityIndicator.alpha = 1.0
                    self.activityIndicator.startAnimating()
                    self.shakeTheDeviceLabel.alpha = 0.0
                })
                // Saving the PIN to pList if all user types right confirm textFields
                
                let flag = ["Pin1": confirmTxtOneOTP.text!,"Pin2": confirmTxtTwoOTP.text!, "Pin3": confirmTxtThreeOTP.text!,"Pin4": confirmTxtFourOTP.text!,"Pin5": confirmTxtFiveOTP.text!,"Pin6": confirmTxtSixOTP.text!] as Any
                
                delegate.write(toPlist: flag as! [String : Any], plistName: "UserPin.plist")
                let flagRead = delegate.reading(fromPlist: "UserPin.plist")
                print(flagRead)
                
                notificationAuthorization()
                generateNotification()
                self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
                
            }else{
                
                activityIndicator.stopAnimating()
                let alert = UIAlertController(title: "Error!", message: "Your PIN and confirmation PIN do not match.", preferredStyle: UIAlertControllerStyle.alert)
                let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                    
                    self.txtOneOTP.text = " "
                    self.txtTwoOTP.text = " "
                    self.txtThreeOTP.text = " "
                    self.txtFourOTP.text = " "
                    self.txtFiveOTP.text = " "
                    self.txtSixOTP.text = " "
                    
                    self.confirmTxtOneOTP.text = " "
                    self.confirmTxtTwoOTP.text = " "
                    self.confirmTxtThreeOTP.text = " "
                    self.confirmTxtFourOTP.text = " "
                    self.confirmTxtFiveOTP.text = " "
                    self.confirmTxtSixOTP.text = " "
                    
                    self.activityIndicator.stopAnimating()
                    self.txtOneOTP.becomeFirstResponder()
                    self.shakeTheDeviceLabel.alpha = 1.0
                }
                
                alert.addAction(dismissAction)
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
    }
    @objc func nextController(){
        performSegue(withIdentifier: "CreatePinToRequiredDocuments", sender: self)
    }
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if (motion == .motionShake){
            txtOneOTP.text = " "
            txtTwoOTP.text = " "
            txtThreeOTP.text = " "
            txtFourOTP.text = " "
            txtFiveOTP.text = " "
            txtSixOTP.text = " "
            
            confirmTxtOneOTP.text = " "
            confirmTxtTwoOTP.text = " "
            confirmTxtThreeOTP.text = " "
            confirmTxtFourOTP.text = " "
            confirmTxtFiveOTP.text = " "
            confirmTxtSixOTP.text = " "
            
            
            txtOneOTP.becomeFirstResponder()
            shakeAnimation()
        }
    }
    func shakeAnimation(){
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x - 10, y: shakeTheDeviceLabel.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x + 10, y: shakeTheDeviceLabel.center.y))
        shakeTheDeviceLabel.layer.add(animation, forKey: "position")
        
    }
    
    
}
