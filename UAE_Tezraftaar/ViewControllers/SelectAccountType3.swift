//
//  SelectAccountType3.swift
//  UBLDigitalSignUp
//
//  Created by awais khalid on 02/11/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class SelectAccountType3: SignUpVideoController {

    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var currentAccountOrSavingAccountLabel: UILabel!
    @IBOutlet weak var currentAccountRadioButton: UIButton!
    @IBOutlet weak var changeAccountType: UIButton!
    
    
    @IBOutlet weak var additionalServicesButton: UIButton!
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    
    
    
     let arrowImageAngle = CGAffineTransform(a: -1.0, b: 1.22464679914735e-16, c: -1.22464679914735e-16, d: -1.0, tx: 0.0, ty: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        uiMakeUp()
        //savingDataToContinueApplicaitonPlist()
        
        //Setting Label on the basis on addtional services
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let services = delegate.globarVariableForAdditionalServices
        var bottomMargin = 0
        for i in services!{
            
            
            
            bottomMargin += 36
            print("BottomMargin\(bottomMargin)")
            let label = i.value
            addLabel(string: label as! String , contraintConstant: CGFloat(bottomMargin))
            addButton(contraintConstant: CGFloat(bottomMargin))
            
            if(services?.count == 4){
                additionalServicesButton.alpha = 0.0
            }
            
            
            
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Setting up Current or saving account
        let flagRead = delegate.reading(fromPlist: "AccountType.plist")
        guard case _? = flagRead!["NotFound"] else {
            
            let currentAccount = flagRead!["CurrentAccount"] as! Int
            let savingAccount = flagRead!["SavingAccount"] as! Int

            if (currentAccount == 0 && savingAccount == 1){
                self.currentAccountOrSavingAccountLabel.text! = "Saving Account"
            }else if(currentAccount == 1 && savingAccount == 0){
                self.currentAccountOrSavingAccountLabel.text! = "Current Account"
            }
            return
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func addLabel(string: String, contraintConstant: CGFloat){
        
        let lblNew = UILabel()
        lblNew.font = UIFont(name: "Aspira-Regular", size: 16)
        lblNew.backgroundColor = UIColor.clear
        lblNew.text = string
        lblNew.textColor = UIColor.white
        lblNew.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblNew)

       
        lblNew.leadingAnchor.constraint(equalTo: currentAccountOrSavingAccountLabel.leadingAnchor).isActive = true
        lblNew.bottomAnchor.constraint(equalTo: currentAccountOrSavingAccountLabel.bottomAnchor, constant: contraintConstant).isActive = true

    }
    func addButton(contraintConstant: CGFloat){
        let button = UIButton()
        
        
        button.setImage(UIImage(named: "Tick-Extra-Small"), for: .normal)
        button.backgroundColor = UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 1.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        print(button.frame.height)
        button.layer.cornerRadius = 10
        
        
        button.centerXAnchor.constraint(equalTo: currentAccountRadioButton.centerXAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: currentAccountOrSavingAccountLabel.bottomAnchor, constant: contraintConstant).isActive = true
    }
    
    //MARK: - Custom Functions
    func uiMakeUp(){
        
        additionalServicesButton.layer.cornerRadius = self.additionalServicesButton.frame.height / 2
        currentAccountRadioButton.layer.cornerRadius = self.currentAccountRadioButton.frame.height / 2
        changeAccountType.layer.cornerRadius = self.changeAccountType.frame.height / 2
        
        
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        
    }
    func savingDataToContinueApplicaitonPlist(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let flag = ["continueApplication": 3] as Any
        
        delegate.write(toPlist: flag as! [String : Any], plistName: "AppLaunchStatus.plist")
        let flagRead = delegate.reading(fromPlist: "AppLaunchStatus.plist")
        print(flagRead)
        
    }
}
// MARK: - Actions
extension SelectAccountType3{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        //Alert
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func previousController(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType3ToSignUpForm", sender: sender)
    }
    @IBAction func nextButttonPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType3ToFindPlace", sender: sender)
    }
    @IBAction func needHelpSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectedAccountType3ToNeedHelp", sender: sender)
    }
    @IBAction func changeAccountSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType3ToSelectAccountType1", sender: sender)
    }
    @IBAction func additionalServicesSelected(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectAccountType3ToSelectAccount2", sender: sender)
    }
}
