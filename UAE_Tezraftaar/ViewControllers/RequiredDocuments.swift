//
//  RequiredDocuments.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 20/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class RequiredDocuments: SignUpVideoController {

    
    @IBOutlet var currentAccountButton: UIButton!
    @IBOutlet var savingAccountButton: UIButton!
    @IBOutlet var creditCardButton: UIButton!
    @IBOutlet var personalLoanButton: UIButton!
    @IBOutlet var insuranceButton: UIButton!
    @IBOutlet var autoLoanButton: UIButton!
    
    
    @IBOutlet var needHelpButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet var currentAccountHeightContraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()

        
        uiMakeUp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Custom Functions
    func uiMakeUp(){
        // Rounding the Buttons
        currentAccountButton.layer.cornerRadius = currentAccountButton.frame.height / 2
        savingAccountButton.layer.cornerRadius = savingAccountButton.frame.height / 2
        creditCardButton.layer.cornerRadius = creditCardButton.frame.height / 2
        personalLoanButton.layer.cornerRadius = personalLoanButton.frame.height / 2
        insuranceButton.layer.cornerRadius = insuranceButton.frame.height / 2
        autoLoanButton.layer.cornerRadius = autoLoanButton.frame.height / 2
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        needHelpButton.layer.borderWidth = 1.5
      
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
        
    }
    
    
    func changingButtonImageWhenPressed(sender: UIButton){
        
        if sender.currentImage == UIImage.init(named: "Minus") {
            
            sender.setImage(UIImage(named:"Plus"), for: .normal)
            sender.backgroundColor = UIColor.white
            
        }else if (sender.currentImage == UIImage.init(named: "Plus")){
            
            sender.setImage( UIImage(named:"Minus"), for: .normal)
            sender.backgroundColor = UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 1.0)
            
        }
        
    }
    
}
//MARK: - Actions
extension RequiredDocuments{
    
    // MARK: - Actions
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func needHelpSelected(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "RequiredDocumentsToNeedHelp", sender: sender)
        
    }
    @IBAction func closeButton(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "RequiredDocumentsToTouchIdSignIn2", sender: sender)
        
    }
    
    
    @IBAction func minuPlusButtonsPressed(_ sender: UIButton) {
        
        
        if currentAccountHeightContraint.constant == 86 {
            
            currentAccountHeightContraint.constant = 0
            
            sender.setImage(UIImage.init(named: "Plus"), for: .normal)
            sender.backgroundColor = UIColor.white
            
        }else{
            
            currentAccountHeightContraint.constant = 86
            sender.setImage(UIImage.init(named: "Minus"), for: .normal)
            sender.backgroundColor = UIColor(red: 0/255, green: 131/255, blue: 202/255, alpha: 1.0)
            
            
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        
    }
    
}
