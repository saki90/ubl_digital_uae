//
//  MobileVerification.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 13/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class VerifyMyNumber2: SignUpVideoController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var txtOneOTP: UITextField!
    @IBOutlet var txtTwoOTP: UITextField!
    @IBOutlet var txtThreeOTP: UITextField!
    @IBOutlet var txtFourOTP: UITextField!
    @IBOutlet var txtFiveOTP: UITextField!
    @IBOutlet var txtSixOTP: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.txtOneOTP.delegate = self
        self.txtTwoOTP.delegate = self
        self.txtThreeOTP.delegate = self
        self.txtFourOTP.delegate = self
        self.txtFiveOTP.delegate = self
        self.txtSixOTP.delegate = self
        
        
        uiMakeUp()
    }
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicator.stopAnimating()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        txtOneOTP.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        //Removing Text from all TextFields on viewAppears
        
        //Hiding the cursor of uitextfields
        txtOneOTP.tintColor = UIColor.clear
        txtTwoOTP.tintColor = UIColor.clear
        txtThreeOTP.tintColor = UIColor.clear
        txtFourOTP.tintColor = UIColor.clear
        txtFiveOTP.tintColor = UIColor.clear
        txtSixOTP.tintColor = UIColor.clear
        
        txtOneOTP.text = " "
        txtTwoOTP.text = " "
        txtThreeOTP.text = " "
        txtFourOTP.text = " "
        txtFiveOTP.text = " "
        txtSixOTP.text = " "
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Customization
    func uiMakeUp(){
        
        
        self.txtOneOTP.resizingFont()
        self.txtOneOTP.layoutIfNeeded()
        self.txtOneOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtTwoOTP.resizingFont()
        self.txtTwoOTP.layoutIfNeeded()
        self.txtTwoOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtThreeOTP.resizingFont()
        self.txtThreeOTP.layoutIfNeeded()
        self.txtThreeOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFourOTP.resizingFont()
        self.txtFourOTP.layoutIfNeeded()
        self.txtFourOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFiveOTP.resizingFont()
        self.txtFiveOTP.layoutIfNeeded()
        self.txtFiveOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtSixOTP.resizingFont()
        self.txtSixOTP.layoutIfNeeded()
        self.txtSixOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        
        
    }
    
    
}
//MARK: - Actions
extension VerifyMyNumber2{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func previousController(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
extension VerifyMyNumber2: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Hiding the cursor of uitextfields
        txtOneOTP.tintColor = UIColor.clear
        txtTwoOTP.tintColor = UIColor.clear
        txtThreeOTP.tintColor = UIColor.clear
        txtFourOTP.tintColor = UIColor.clear
        txtFiveOTP.tintColor = UIColor.clear
        txtSixOTP.tintColor = UIColor.clear
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print(string.count)
        print(textField.tag)
        if (string.count == 1){
            print("first if")
            
            textField.text = ""
            textField.text = string
            
            let nextTag = textField.tag + 1;
            
            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag);
            
            if (nextResponder == nil){
                view.endEditing(true)
                UIView.animate(withDuration: 0.5, animations: {
                    self.activityIndicator.alpha = 1
                    self.activityIndicator.startAnimating();
                    //self.shakeTheDeviceLabel.alpha = 0.0
                })
                //self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
            }
            nextResponder?.becomeFirstResponder();
            print("//")
            print(string.count)
            print(nextTag)
            return false;
        }else if (textField.tag == 0){
            print("second else")
            
            textField.text = " "
            
            return false;
        }else if(string.count == 0 && textField.tag != 1){
            print("Third else")
            
            textField.text = " "
            let previousTag = textField.tag - 1;
            
            // get previous responder
            let previousResponder = textField.superview?.viewWithTag(previousTag);
            previousResponder?.becomeFirstResponder()

            let textField = previousResponder?.viewWithTag(previousTag) as! UITextField
            textField.text = " "
            
            print("//")
            print(string.count)
            print(previousTag)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag == 6 && textField.text != ""){
            activityIndicator.startAnimating(); self.perform(#selector(VerifyMyNumber2.nextController), with: nil, afterDelay: 2.0)
        }
    }
    
    @objc func nextController(){
        performSegue(withIdentifier: "VerifyMyNumber2ToSelectAccountType1", sender: self)
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if (motion == .motionShake){
            txtOneOTP.text = " "
            txtTwoOTP.text = " "
            txtThreeOTP.text = " "
            txtFourOTP.text = " "
            txtFiveOTP.text = " "
            txtSixOTP.text = " "
            
            txtOneOTP.becomeFirstResponder()
        }
    }
}
