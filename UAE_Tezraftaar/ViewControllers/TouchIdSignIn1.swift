//
//  TouchIdSignIn1.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 27/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class TouchIdSignIn1: SignUpVideoController {
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var ublLogoImage: UIImageView!
    @IBOutlet var touchIdImage: UIImageView!
    @IBOutlet var arrowImage: UIImageView!
    
    @IBOutlet var pleaseUseTouchIdLabel: UILabel!
    @IBOutlet var forgotPassword: UILabel!
    @IBOutlet var nextLabel: UILabel!
    
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var nextButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        loginTextField.delegate  = self
        passwordTextField.delegate  = self
        
        uiMakeUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - Animations
    override func viewWillAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.8, animations: {
            
            self.backButton.center.x -= self.view.bounds.width
            self.ublLogoImage.center.x -= self.view.bounds.width
            self.touchIdImage.center.x -= self.view.bounds.width
            self.pleaseUseTouchIdLabel.center.x -= self.view.bounds.width
            self.loginTextField.center.x -= self.view.bounds.width
            self.passwordTextField.center.x -= self.view.bounds.width
            self.forgotPassword.center.x -= self.view.bounds.width
            self.nextButton.center.x -= self.view.bounds.width
            self.arrowImage.center.x -= self.view.bounds.width
            self.nextLabel.center.x -= self.view.bounds.width
            
        })
    }
    
    
    
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        // Setting up bottom border to TextFields
        self.loginTextField.resizingFont()
        self.loginTextField.layoutIfNeeded()
        loginTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.passwordTextField.resizingFont()
        self.passwordTextField.layoutIfNeeded()
        passwordTextField.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        // Rounding the edges of Buttons
        nextButton.resizingFont()
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn1.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TouchIdSignIn1.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 145
        }
    }
    
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 145
        }
    }
}
// MARK: - Actions
extension TouchIdSignIn1{
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func previousController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
    }
    
}

extension TouchIdSignIn1: UITextFieldDelegate{
    
    
    // Setting the TextField placeholder to Empty once a user starts typing
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // Hiding the placeholder when user starts typing
        if (loginTextField.isEditing == true){
            loginTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }else if(passwordTextField.isEditing == true){
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Displaying placeholder when user end editing or touches everywhere on the screen
        if(loginTextField.isEditing == false){
            //loginTextField.placeholder = "Login"
            loginTextField.attributedPlaceholder = NSAttributedString(string: "Login", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        if(passwordTextField.isEditing == false){
            //passwordTextField.placeholder = "Password"
            passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if loginTextField.returnKeyType == UIReturnKeyType.next{
            passwordTextField.becomeFirstResponder()
        }else if passwordTextField.returnKeyType == UIReturnKeyType.done {
            textField.endEditing(true)
        }
        
        return false
    }
}

