//
//  WelcomeController.swift
//  UBL Digital
//
//  Created by Awais Khalid on 17/08/2017.
//  Copyright © 2017 Awais Khalid. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class WelcomeViewController: SignInVideoController {
    
    
    var gradientLayer: CAGradientLayer!
    
    @IBOutlet weak var ublLogoImage: UIImageView!
    @IBOutlet weak var tabBar: UIView!
    @IBOutlet weak var nextButtonArrowImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var getStartedLabel: UILabel!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var qrLabel: UILabel!
    @IBOutlet weak var offersLabel: UILabel!
    @IBOutlet weak var locateUsLabel: UILabel!
    @IBOutlet weak var faqsLabel: UILabel!
    
    @IBOutlet weak var tabBarView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        uiMakeUp()
        tabBarView.insertTabBar()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        animateViews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Custom Functions
    func uiMakeUp(){
        self.nextButton.layer.cornerRadius = self.nextButton.bounds.height/2
    }
    func animateViews(){
        
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseInOut, animations: {
            self.ublLogoImage.center.x -= self.view.bounds.width
        }, completion: {finished in
            
        })
        
        UIView.animate(withDuration: 1.2, delay: 0.2, options: .curveEaseInOut, animations: {
            self.getStartedLabel.center.x -= self.view.bounds.width
        }, completion: {finished in
            
        })
        UIView.animate(withDuration: 1.3, delay: 0.3, options: .curveEaseInOut, animations: {
            self.nextButton.center.x -= self.view.bounds.width
        }, completion: {finished in
            
        })
        UIView.animate(withDuration: 1.3, delay: 0.3, options: .curveEaseInOut, animations: {
            self.nextButtonArrowImage.center.x -= self.view.bounds.width
        }, completion: {finished in
            
        })
        UIView.animate(withDuration: 1.3, delay: 0.3, options: .curveEaseInOut, animations: {
            self.nextLabel.center.x -= self.view.bounds.width
        }, completion: {finished in
            
        })
        
    }
}
//Actions
extension WelcomeViewController{
    
    //MARK: - Actions
    @IBAction func nextPressed(_ sender: Any) {
        //animateViews()
        self.perform(#selector(self.nextController), with: nil, afterDelay: 0)
        
    }
    @objc func nextController(){
        performSegue(withIdentifier: "WelcomeToSignIn", sender: self)
    }
    
}



