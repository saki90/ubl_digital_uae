 //
 //  FindPlace.swift
 //  UBLDigitalSignUp
 //
 //  Created by Awais Khalid on 20/10/2017.
 //  Copyright © 2017 O3Interfaces. All rights reserved.
 //
 
 import UIKit
 import CoreLocation
 import SVProgressHUD
 import PeekabooConnect
 
 
 class FindPlace: SignUpVideoController  {
    
    
    @objc var isSelcted : Bool = false
    
    @IBOutlet var mainSubView: UIView!
    @IBOutlet var outerCircle: UIView!
    @IBOutlet var innerCircle: UIView!
    @IBOutlet var mostInnerCircle: UIView!
    
    
    
    
    @IBOutlet var needHelpButton: UIButton!
    
    
    @IBOutlet weak var faq: UILabel!
    @IBOutlet weak var Locate: UILabel!
    @IBOutlet weak var offers: UILabel!
    @IBOutlet weak var qr: UILabel!
    @IBOutlet weak var lets: UILabel!
    @IBOutlet weak var just: UILabel!
    @IBOutlet weak var in_order: UILabel!
    var backFlag : Bool = false
    var  isHelpSelected = false
    // Variables
    var locationManager = CLLocationManager()
    var gbclass = GlobalStaticClass.getInstance()
    var defaults = UserDefaults()
    
    override func viewWillAppear(_ animated: Bool) {
//        in_order.text = "In order for use to complete your application we need to meet";
//        just.text = "Just a few seconds"
//        lets.text = "LET'S MEET!"
//        qr.text = "QR"
//        offers.text = "Offers"
//        Locate.text = "Locate Us"
//        faq.text = "FAQs"
        gbclass!.step_no_onbording = "1"
//        gbclass!.step_no_help_onbording = "5"
        gbclass!.step_no_help_onbording = "DOB04"
    } 
    
    
    
    override func viewWillLayoutSubviews() {
//        in_order.text = "In order for use to complete your application we need to meet";
//        just.text = "Just a few seconds"
//        lets.text = "LET'S MEET!"
//        qr.text = "QR"
//        offers.text = "Offers"
//        Locate.text = "Locate Us"
//        faq.text = "FAQs"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setCornerRadius(8.0)
        SVProgressHUD.setContainerView(self.view)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        
//        in_order.text = "In order for use to complete your application we need to meet";
//        just.text = "Just a few seconds"
//        lets.text = "LET'S MEET!"
//        qr.text = "QR"
//        offers.text = "Offers"
//        Locate.text = "Locate Us"
//        faq.text = "FAQs"
        
        
        uiMakeUp()
        
        print(gbclass!.isLocationSelected)
        print(gbclass!.selctedLatitude)
        print(gbclass!.selectedLongitude)
        
        gbclass?.controllers_Refrence_Stack.add(self)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(FindPlace.pulseAnimation))
        tapGesture.numberOfTapsRequired = 1
        outerCircle.addGestureRecognizer(tapGesture)
        gbclass!.noOfPerformedAttempts = 0;
        gbclass!.parent_vc_onbording = "FindPlace";
        
        defaults = .standard
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        pulseAnimation()
        isHelpSelected = false
        
//        in_order.text = "In order for use to complete your application we need to meet";
//        just.text = "Just a few seconds"
//        lets.text = "LET'S MEET!"
//        qr.text = "QR"
//        offers.text = "Offers"
//        Locate.text = "Locate Us"
//        faq.text = "FAQs"
        
        if !backFlag {
            if(isLocationServiceOn()) {
                self.perform(#selector(FindPlace.letsMeetController), with: nil, afterDelay: 2.0)
            } else {
                //self.perform(#selector(FindPlace.letsMeetController), with: nil, afterDelay: 2.0)
                self.perform(#selector(FindPlace.locationServicesController), with: nil, afterDelay: 2.0)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom Functions
    func uiMakeUp(){
        
        outerCircle.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        outerCircle.layer.cornerRadius = outerCircle.frame.height / 2
        outerCircle.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6).cgColor
        outerCircle.layer.borderWidth = 1.6
        
        innerCircle.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        innerCircle.layer.cornerRadius = innerCircle.frame.height / 2
        innerCircle.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6).cgColor
        innerCircle.layer.borderWidth = 1.6
        
        mostInnerCircle.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2)
        mostInnerCircle.layer.cornerRadius = mostInnerCircle.frame.height / 2
        mostInnerCircle.layer.borderColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6).cgColor
        mostInnerCircle.layer.borderWidth = 1.6
        
        needHelpButton.layer.cornerRadius = needHelpButton.frame.height / 2
        needHelpButton.layer.borderWidth = 1.5
        needHelpButton.layer.borderColor = UIColor.white.cgColor
        
    }
    
    func isLocationServiceOn() -> Bool{
        if(CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            return true
        } else {
            return false
        }
    }
    
    func slideLeft() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    func slideRight() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
    }
    
    @objc func pulseAnimation()
    {
        print("Tapped")
        let pulse = Pulsing(numberOfPulses:.infinity, radius: self.view.frame.height / 2, position: outerCircle!.center)
        pulse.animationDuration = 0.6
        pulse.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor
        self.view.layer.insertSublayer(pulse, above: self.view.layer)
        
//        Sublayer(pulse, below: mainSubView.layer)
        
    }
    
    @objc func letsMeetController() {
        if !backFlag {
            if !isHelpSelected {
                self.performSegue(withIdentifier: "FindPlaceToLetsMeet", sender: self)
            }
            
        }
    }
    
    @objc func locationServicesController()
    {
        if !backFlag {
            if !isHelpSelected {
                self.performSegue(withIdentifier: "FindPlaceToLocationServices", sender: self)
            }
            
        }
    }
    
    
    //    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    //        if isHelpSelected {
    //            return false
    //        } else {
    //            return true
    //        }
    //    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FindPlaceToLetsMeet" {
            if isSelcted {
                let vc : LetsMeet = segue.destination as! LetsMeet
                vc.isSelcted = true
            }
            
        }
    }
    
    func custom_alert(msg1: String, icons: String) {
        
        self.gbclass!.custom_alert_msg = msg1;
        self.gbclass!.custom_alert_img = icons;
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "alert")
        controller.modalPresentationStyle = .overCurrentContext;
        self.present(controller, animated: false, completion: nil)
    }
    
    
    
    
 }
 
 //MARK: - Actions
 extension FindPlace{
    //MARK: - Actions
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //////////************************** START TABBAR CONTROLS ***************************///////////
    
    @IBAction func btn_feature(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
            
        })
        
        let alert = UIAlertController(title: "", message: "Service Unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
        //    let first_time_chk = self.defaults.string(forKey: "enable_touch")
        //        if first_time_chk == "1" || first_time_chk! == "11" {
        //            [hud showAnimated:YES];
        //            if (netAvailable) {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        } else {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please register your User." :@"0"];
        //        }
    }
    
    @IBAction func btn_offer(_ sender: UIButton)
    {
        
        gbclass!.tab_bar_login_pw_check="login";
        
       // [self .peekabooSDK(type: "deals")]
        
         peekabooSDK(type: "deals")
        
//        var jsCodeLocation: URL?
//        jsCodeLocation = Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//        var rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: ["environment": "production",     // Supported are beta, stage, production
//            "pkbc": "app.com.brd",     // Will be provided by Peekaboo Business team
//            "type": "deals"], launchOptions: nil)
//        var vc = UIViewController()
//        vc.view = rootView
//        present(vc, animated: true) {() -> Void in }
        
        
        //        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        //        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
        //            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
        //            "peekabooEnvironment" : "production",
        //            "peekabooSdkId" : "UBL_SDK",
        //            "peekabooTitle" : "UBL Bank Offers",
        //            "peekabooSplashImage" : "true",
        //            "peekabooWelcomeMessage" : "Please select your City to view deals and discounts offered to UBL customers",
        //            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
        //            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
        //            "peekabooPoweredByFooter" : "false",
        //            "peekabooColorPrimary" : "#007DC0",
        //            "peekabooColorPrimaryDark" : "#005481",
        //            "peekabooColorStatus" : "#2d2d2d",
        //            "peekabooColorSplash" : "#004681",
        //            "peekabooType": "deals"], launchOptions: nil)
        //
        //        let vc = UIViewController()
        //        vc.view = rootView
        //        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_find_us(_ sender: UIButton) {
        
        gbclass!.tab_bar_login_pw_check="login";
        
       // [self .peekabooSDK(type: "locator")]
        peekabooSDK(type: "locator")
        
        
        
        
        // gbclass!.tab_bar_login_pw_check="login";
        
//        var jsCodeLocation: URL?
//        jsCodeLocation = Bundle.main.url(forResource: "main", withExtension: "jsbundle")
//        var rootView = RCTRootView(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: ["environment": "production",     // Supported are beta, stage, production
//            "pkbc": "app.com.brd",     // Will be provided by Peekaboo Business team
//            "type": "locator"], launchOptions: nil)
//        var vc = UIViewController()
//        vc.view = rootView
//        present(vc, animated: true) {() -> Void in }
        
        
        //        let jsCodeLocation = Bundle.main.url(forResource:"main", withExtension: "jsbundle")
        //        let rootView = RCTRootView.init(bundleURL: jsCodeLocation, moduleName: "peekaboo", initialProperties: [
        //            "peekabooOwnerKey" : "7db159dc932ec461c1a6b9c1778bb2b0",
        //            "peekabooEnvironment" : "production",
        //            "peekabooSdkId" : "UBL_SDK",
        //            "peekabooTitle" : "UBL Bank Offers",
        //            "peekabooSplashImage" : "true",
        //            "peekabooWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
        //            "peekabooGooglePlacesApiKey" : "AIzaSyDL8H2kty2e7T_8hNZX4UW2S3-4syXO0xE",
        //            "peekabooContactUsWidget" : "customer.services@ubl.com.pk",
        //            "peekabooPoweredByFooter" : "false",
        //            "peekabooColorPrimary" : "#007DC0",
        //            "peekabooColorPrimaryDark" : "#005481",
        //            "peekabooColorStatus" : "#2d2d2d",
        //            "peekabooColorSplash" : "#004681",
        //            "peekabooType": "locator",
        //            "peekabooLocatorWelcomeMessage" : "Please select your City to easily search & navigate to the UBL Branch / ATM near you",
        //            "peekabooLocatorTitle" : "Locate us",
        //            ], launchOptions: nil)
        //
        //        let vc = UIViewController()
        //        vc.view = rootView
        //        self.present(vc, animated: true, completion: nil);
        
    }
    
    @IBAction func btn_faq(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "faq")
        self.slideLeft()
        self.present(controller, animated: false, completion: nil)
    }
    
    //////////************************** END TABBAR CONTROLS ***************************///////////
    
    @IBAction func previousController(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectedServicesVC")
        self.slideLeft()
        self.present(controller, animated: false, completion: nil)
        
        
        //        backFlag = true
        //
        //        //        for vc in (gbclass?.controllers_Refrence_Stack)! {
        //        //
        //        //            if let fvc: LetsMeet = vc as? LetsMeet {
        //        //
        //        //                fvc.dismiss(animated: false, completion: nil);
        //        //
        //        //            }
        //        //
        //        //            if let fvc: FindPlace = vc as? FindPlace {
        //        //
        //        //                fvc.dismiss(animated: false, completion: nil);
        //        //
        //        //            }
        //        //
        //        //            if let fvc: LocationServices = vc as? LocationServices {
        //        //
        //        //                fvc.dismiss(animated: false, completion: nil);
        //        //
        //        //            }
        //        //
        //        //            if let fvc: VisitArrangmentVC = vc as? VisitArrangmentVC {
        //        //
        //        //                fvc.dismiss(animated: false, completion: nil);
        //        //
        //        //            }
        //        //
        //        //            if let fvc: MeetRepresentativeVC = vc as? MeetRepresentativeVC {
        //        //
        //        //                fvc.dismiss(animated: false, completion: nil);
        //        //
        //        //            }
        //        //
        //        //
        //        //        }
        //
        //
        //        let tst = gbclass?.isRedirected
        //        if (gbclass?.isRedirected)! {
        //
        //            let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        //            let vc = storyBoard.instantiateViewController(withIdentifier: "SelectAccountTypeVC")
        //            present(vc, animated: true, completion: nil)
        //            //self.dismiss(animated: false, completion: nil)
        //
        //        }
        //        else {
        //            self.dismiss(animated: true, completion: nil)
        //
        //        }
        //
        //        gbclass?.controllers_Refrence_Stack = NSMutableArray()
        //
    }
    
    @IBAction func needHelpSelected(_ sender: UIButton) {
        
        isHelpSelected = true
        
        let session = URLSession (
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let url = gbclass!.mainurl1 as String
        SVProgressHUD.show()
        
        let task = session.dataTask(with: URL.init(string: url)!, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print("error: \(error!.localizedDescription): \(error!)")
            } else if data != nil {
                
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    print("Received data:\n\(str)")
                    
                    let encryption = Encrypt()
                    
                    let parameterDictionary2 = ["strSessionId" : encryption.encrypt_Data(self.gbclass!.m3sessionid),
                                                "IP": encryption.encrypt_Data(GlobalStaticClass.getPublicIp()),
                                                "Device_ID" : encryption.encrypt_Data(self.gbclass!.udid),
                                                "Token" :encryption.encrypt_Data(self.gbclass!.token),
                                                "strMobileNo": encryption.encrypt_Data(self.gbclass!.mobile_number_onbording),
                                                "strEmailAddress" : encryption.encrypt_Data(self.gbclass!.email_onbording),
                                                "RequestId": encryption.encrypt_Data(self.gbclass!.request_id_onbording),
                                                "strCNIC": encryption.encrypt_Data(self.gbclass!.cnic_onbording),
                                                "Name": encryption.encrypt_Data(self.gbclass!.user_name_onbording),
                                                "StepNo" : encryption.encrypt_Data(self.gbclass!.step_no_help_onbording)
                        
                        ] as [String : String]
                    
                    
                    let manger = AFHTTPSessionManager.init(baseURL: URL.init(string: self.gbclass!.mainurl1))
                    let searlizer = AFHTTPResponseSerializer()
                    manger.responseSerializer = searlizer
                    
                    manger.post("NeedOnBoardHelp", parameters: parameterDictionary2, progress: nil, success: { (task, responseObject) in
                        
                        if let dict = encryption.de_crypt_Data(responseObject as! Data) {
                            
                            let dictionary = dict as NSDictionary
                            let response = dictionary.object(forKey: "Response") as! String
                            SVProgressHUD.dismiss()
                            
                            switch response {
                            case "-78","-79":
                                // [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                                let alert = UIAlertController.init(title: "Attention", message: (dictionary.object(forKey: "strReturnMessage") as! String), preferredStyle: .alert)
                                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.cancel, handler: {
                                    (alert: UIAlertAction!) in
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "login_new")
                                    self.slideLeft()
                                    self.present(controller, animated: false, completion: nil)
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                return
                                
                            case "0","-1" :
                                print("reponse ")
                                self.gbclass!.noOfPerformedAttempts += 1;
                                self.gbclass!.noOfAvailableAttempts = ((dictionary.object(forKey: "NoOfAttempts") as! NSString).intValue - 1)
                                self.gbclass!.timmerLimitOnBording = ((dictionary.object(forKey: "TimerLimit") as! NSString).intValue)
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller = storyboard.instantiateViewController(withIdentifier: "NeedHelpVC") as! NeedHelpVC
                                self.present(controller, animated: false, completion: nil)
                                self.slideRight()
                                
                                
                            case "1", "2", "4" :
                                print("reponse")
                                self.gbclass!.noOfPerformedAttempts += 1;
                                self.gbclass!.noOfAvailableAttempts = ((dictionary.object(forKey: "NoOfAttempts") as! NSString).intValue - 1)
                                self.custom_alert(msg1: dictionary.object(forKey: "strReturnMessage") as! String, icons: "0")
                                if(self.isLocationServiceOn()) {
                                    self.performSegue(withIdentifier: "FindPlaceToLetsMeet", sender: self)
                                } else {
                                    self.performSegue(withIdentifier: "FindPlaceToLocationServices", sender: self)
                                }
                                
                                
                            default:
                                self.custom_alert(msg1: dictionary.object(forKey: "strReturnMessage") as! String, icons: "0")
                                if(self.isLocationServiceOn()) {
                                    self.performSegue(withIdentifier: "FindPlaceToLetsMeet", sender: self)
                                } else {
                                    self.performSegue(withIdentifier: "FindPlaceToLocationServices", sender: self)
                                }
                                
                            }
                        }
                        
                    }) { (task, error) in
                        
                        print(error)
                        SVProgressHUD.dismiss()
                    }
                    
                }
                else {
                    print("Unable to convert data to text")
                }
            }
        })
        
        task.resume()
    }
    
    
    func peekabooSDK(type: String) {
        self.present(getPeekabooUIViewController(["environment": "production", "pkbc": "app.com.brd", "type": type, "country": "Pakistan"]), animated: true, completion: nil)
        
    }
    
 }
 


 
