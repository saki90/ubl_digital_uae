//
//  ViewProgress1.swift
//  UBLDigitalSignUp
//
//  Created by Awais Khalid on 30/10/2017.
//  Copyright © 2017 O3Interfaces. All rights reserved.
//

import UIKit

class ViewProgress1: SignInVideoController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var txtOneOTP: UITextField!
    @IBOutlet var txtTwoOTP: UITextField!
    @IBOutlet var txtThreeOTP: UITextField!
    @IBOutlet var txtFourOTP: UITextField!
    @IBOutlet var txtFiveOTP: UITextField!
    @IBOutlet var txtSixOTP: UITextField!
    
    
    @IBOutlet weak var shakeTheDeviceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        
        self.txtOneOTP.delegate = self
        self.txtTwoOTP.delegate = self
        self.txtThreeOTP.delegate = self
        self.txtFourOTP.delegate = self
        self.txtFiveOTP.delegate = self
        self.txtSixOTP.delegate = self
        
        uiMakeUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        txtOneOTP.text = " "
        txtTwoOTP.text = " "
        txtThreeOTP.text = " "
        txtFourOTP.text = " "
        txtFiveOTP.text = " "
        txtSixOTP.text = " "
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtOneOTP.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //self.view.endEditing(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicator.stopAnimating()
        shakeTheDeviceLabel.alpha = 1.0
    }
    
 
    // MARK: Custom Functions
    func uiMakeUp(){
        // UITextFields moving up and down with keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(ViewProgress1.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewProgress1.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Bottom Border and resizing the font of UITextFields
        
        self.txtOneOTP.resizingFont()
        self.txtOneOTP.layoutIfNeeded()
        self.txtOneOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtTwoOTP.resizingFont()
        self.txtTwoOTP.layoutIfNeeded()
        self.txtTwoOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtThreeOTP.resizingFont()
        self.txtThreeOTP.layoutIfNeeded()
        self.txtThreeOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFourOTP.resizingFont()
        self.txtFourOTP.layoutIfNeeded()
        self.txtFourOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtFiveOTP.resizingFont()
        self.txtFiveOTP.layoutIfNeeded()
        self.txtFiveOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
        
        self.txtSixOTP.resizingFont()
        self.txtSixOTP.layoutIfNeeded()
        self.txtSixOTP.setBottomBorder(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.35)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if view.frame.origin.y == 0{
            self.view.frame.origin.y -= 100
        }
    }
    // moving the whole view to its defualt place as the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y += 100
        }
    }
}
//MARK: - Actions
extension ViewProgress1{
    
    
    @IBAction func alertOnTabBarSelected(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {finished in
            UIView.animate(withDuration: 0.5, animations: {
                sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        })
        let alert = UIAlertController(title: "", message: "Service unavailable", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
        }
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewProgress1: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txtOneOTP.tintColor = UIColor.clear
        txtTwoOTP.tintColor = UIColor.clear
        txtThreeOTP.tintColor = UIColor.clear
        txtFourOTP.tintColor = UIColor.clear
        txtFiveOTP.tintColor = UIColor.clear
        txtSixOTP.tintColor = UIColor.clear
        
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        
        print(string.count)
        print(textField.tag)
        
        
        if (string.count == 1){
            print("first if")
            
            textField.text = ""
            textField.text = string
            
            let nextTag = textField.tag + 1;
            
            // get next responder
            let nextResponder = textField.superview?.viewWithTag(nextTag);
            
            if (nextResponder == nil){
                view.endEditing(true)
                UIView.animate(withDuration: 0.5, animations: {
                    self.activityIndicator.alpha = 1
                    self.activityIndicator.startAnimating();
                    self.shakeTheDeviceLabel.alpha = 0.0
                    
                })
                
                //self.perform(#selector(VerifyMyNumber1.nextController), with: nil, afterDelay: 2.0)
            }
            
            nextResponder?.becomeFirstResponder();
            
            print("//")
            print(string.count)
            print(nextTag)
            return false;
        }else if (textField.tag == 0){
            print("second else")
            
            textField.text = " "
            
            return false;
        }else if(string.count == 0 && textField.tag != 1){
            print("Third else")
            
            textField.text = " "
            let previousTag = textField.tag - 1;
            
            // get previous responder
            let previousResponder = textField.superview?.viewWithTag(previousTag);
            
            
            previousResponder?.becomeFirstResponder()
            
            let textField = previousResponder?.viewWithTag(previousTag) as! UITextField
            textField.text = " "
            
            print("//")
            print(string.count)
            print(previousTag)
            return false
        }
    
        
        
        return true
    }
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {

        let delegate = UIApplication.shared.delegate as! AppDelegate
        let pinData = delegate.reading(fromPlist: "UserPin.plist")
       // let userInfo = delegate.readingFromPlist(plistName: "UserInformation.plist")

        let pin1 = pinData!["Pin1"] as! String
        let pin2 = pinData!["Pin2"] as! String
        let pin3 = pinData!["Pin3"] as! String
        let pin4 = pinData!["Pin4"] as! String
        let pin5 = pinData!["Pin5"] as! String
        let pin6 = pinData!["Pin6"] as! String

        //TODO: - Also validate with Mobile number
        //let mobileNumber = userInfo["Mobile"] as! String

        if(textField.tag == 6){
            if(txtOneOTP.text! == pin1 && txtTwoOTP.text! == pin2 && txtThreeOTP.text! == pin3 && txtFourOTP.text! == pin4 && txtFiveOTP.text! == pin5 && txtSixOTP.text! == pin6 /*&& mobileNumberTextField.text! == mobileNumber*/){
                self.view.endEditing(true)
                UIView.animate(withDuration: 0.5, animations: {

                    self.activityIndicator.alpha = 1
                    self.activityIndicator.startAnimating()
                    self.shakeTheDeviceLabel.alpha = 0.0

                })


                self.perform(#selector(ViewProgress1.nextController), with: nil, afterDelay: 2.0)

            }else {
                activityIndicator.stopAnimating()

                let alert = UIAlertController(title: "Sorry!", message: "The Mobile Number or PIN you entered is not assosiated with this device", preferredStyle: UIAlertControllerStyle.alert)
                let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in

                    self.txtOneOTP.text = " "
                    self.txtTwoOTP.text = " "
                    self.txtThreeOTP.text = " "
                    self.txtFourOTP.text = " "
                    self.txtFiveOTP.text = " "
                    self.txtSixOTP.text = " "

                    self.txtOneOTP.becomeFirstResponder()
                    self.activityIndicator.stopAnimating()


                }

                alert.addAction(dismissAction)
                self.present(alert, animated: true, completion: nil)

            }

        }
    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        if(textField.tag == 6){
//            return true
//        }
//        return false
//    }
 
    @objc func nextController(){
        self.performSegue(withIdentifier: "ViewProgress1ToSignUpProgressTracking", sender: self)
    }
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if (motion == .motionShake){
            
            txtOneOTP.text = " "
            txtTwoOTP.text = " "
            txtThreeOTP.text = " "
            txtFourOTP.text = " "
            txtFiveOTP.text = " "
            txtSixOTP.text = " "
            
            txtOneOTP.becomeFirstResponder()
            shakeAnimation()
        }
    }
    func shakeAnimation(){
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x - 10, y: shakeTheDeviceLabel.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: shakeTheDeviceLabel.center.x + 10, y: shakeTheDeviceLabel.center.y))
        shakeTheDeviceLabel.layer.add(animation, forKey: "position")
        
    }
    
}
