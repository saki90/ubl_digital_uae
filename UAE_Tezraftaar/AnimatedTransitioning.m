//
//  AnimatedTransitioning.m
//  CustomTransitionExample
//
//  Created by Blanche Faur on 10/24/13.
//  Copyright (c) 2013 Blanche Faur. All rights reserved.
//

#import "AnimatedTransitioning.h"
//#import "MainViewController.h"
#import "Login_VC.h"
//#import "SecondViewController.h"
#import "Slide_menu_VC.h"

@implementation AnimatedTransitioning

//===================================================================
// - UIViewControllerAnimatedTransitioning
//===================================================================


//**************************************** Form Bottom-Up ****************************************

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.39f;
}


//- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
//    
//    UIView *inView = [transitionContext containerView];
//  
//    Slide_menu_VC *toVC = (Slide_menu_VC *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//  
//    Login_VC *fromVC = (Login_VC *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    
//    [inView addSubview:toVC.view];
//    
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    [toVC.view setFrame:CGRectMake(0, screenRect.size.height, fromVC.view.frame.size.width, fromVC.view.frame.size.height)];
//    
//    [UIView animateWithDuration:0.25f
//                     animations:^{
//                         
//                         [toVC.view setFrame:CGRectMake(0, 0, fromVC.view.frame.size.width, fromVC.view.frame.size.height)];
//                     }
//                     completion:^(BOOL finished) {
//                         [transitionContext completeTransition:YES];
//                     }];
//}


//**************************************** Form right-side ****************************************


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CGRect fromVCFrame =fromViewController.view.frame;
    
    CGFloat width = toViewController.view.frame.size.width;
   [[transitionContext containerView] addSubview:toViewController.view];
    toViewController.view.frame = CGRectOffset(fromVCFrame, -width, 0); // CGRectOffset(fromVCFrame, width, 0);
    
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromViewController.view.frame = CGRectOffset(fromVCFrame, width, 0);
        toViewController.view.frame = fromVCFrame;
    } completion:^(BOOL finished)
     {
         fromViewController.view.frame = fromVCFrame;
         [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
         
     }];
    
}




    
 //   UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
 //   UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
 //   CGRect fromVCFrame =fromViewController.view.frame;
    
//    CGFloat width = 240;//toViewController.view.frame.size.width;
//    [[transitionContext containerView] addSubview:toViewController.view];
//    toViewController.view.frame = CGRectOffset(fromVCFrame, width, 0); // CGRectOffset(fromVCFrame, width, 0);
    
    
    
//    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
//        fromViewController.view.frame = CGRectOffset(fromVCFrame, -width, 0);
//        toViewController.view.frame = fromVCFrame;
//    } completion:^(BOOL finished)
//     {
//         fromViewController.view.frame = fromVCFrame;
// /        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
         
//     }];
    






@end
