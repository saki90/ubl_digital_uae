//
//  Login_Model.h
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdtDataset;

@interface Login_Model : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, strong) NSString *strDailyLimit;
@property (nonatomic, strong) NSString *response;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) NSString *pinMessage;
@property (nonatomic, strong) NSString *strReturnMessage;
@property (nonatomic, strong) NSString *strMonthlyLimit;
@property (nonatomic, assign) id objdtUserInfo;
@property (nonatomic, strong) NSString *strWelcomeMesage;
@property (nonatomic, strong) NSString *strhCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
