//
//  Features_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "SettingView.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "APIdleManager.h"


@interface SettingView ()
{
    //    UIStoryboard *mainStoryboard;
    //    UIViewController *vc;
    //    GlobalStaticClass* gblclass;
    //    NSDictionary *dic;
    //    MBProgressHUD* hud;
    //    NSMutableArray  *a;
    //    NSMutableArray* arr_offer;
    //    NSMutableArray* gblCls_Offer;
    //    UILabel* label;
    //    UIImageView* img;
//    UIWebView* web;
    NSString* data;
    NSURL* url;
    MBProgressHUD *hud;
    GlobalStaticClass*   gblclass;
}
@end

@implementation SettingView
//@synthesize webView;
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    
    if([gblclass.settingclick isEqualToString:@"Security Tips"]){
        url = [[NSBundle mainBundle] URLForResource:@"securityTips" withExtension:@"htm"];
        
    }
    else if([gblclass.settingclick isEqualToString:@"Disclaimer"]){
        url = [[NSBundle mainBundle] URLForResource:@"disclaimer" withExtension:@"htm"];
        
        
    }
    else if([gblclass.settingclick isEqualToString:@"Privacy"]){
        url = [[NSBundle mainBundle] URLForResource:@"privacy" withExtension:@"htm"];
        
        
    }
    else if([gblclass.settingclick isEqualToString:@"Terms & Conditions"]){
        url = [[NSBundle mainBundle] URLForResource:@"legal" withExtension:@"htm"];
        
        
    }
    
    
    
   // [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
   // self.webView.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    
    
    [APIdleManager sharedInstance].onTimeout = ^(void){
        //  [self.timeoutLabel  setText:@"YES"];
        
        
        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    gblclass.story_board bundle:[NSBundle mainBundle]];
        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        
        
        [self presentViewController:myController animated:YES completion:nil];
        
        
        APIdleManager * cc1=[[APIdleManager alloc] init];
        // cc.createTimer;
        
        [cc1 timme_invaletedd];
        
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
//#pragma mark - WebView -
//- (void)webViewDidStartLoad:(UIWebView *)webView
//{
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//    [hud showAnimated:YES];
//}
//
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [hud hideAnimated:YES];
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    [hud hideAnimated:YES];
//}

-(IBAction)btn_more:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor =[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}



@end

