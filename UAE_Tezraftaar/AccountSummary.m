//
//  AccountSummary.m
//
//  Created by   on 22/01/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AccountSummary.h"
#import "OutdtDataset.h"


NSString *const kAccountSummaryResponse = @"Response";
NSString *const kAccountSummaryOutdtDataset = @"outdtDataset";
NSString *const kAccountSummaryOutdtData = @"outdtData";
NSString *const kAccountSummaryStrReturnMessage = @"strReturnMessage";


@interface AccountSummary ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AccountSummary

@synthesize response = _response;
@synthesize outdtDataset = _outdtDataset;
@synthesize outdtData = _outdtData;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kAccountSummaryResponse fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kAccountSummaryOutdtDataset]];
            self.outdtData = [self objectOrNilForKey:kAccountSummaryOutdtData fromDictionary:dict];
            self.strReturnMessage = [self objectOrNilForKey:kAccountSummaryStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kAccountSummaryResponse];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kAccountSummaryOutdtDataset];
    [mutableDict setValue:self.outdtData forKey:kAccountSummaryOutdtData];
    [mutableDict setValue:self.strReturnMessage forKey:kAccountSummaryStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kAccountSummaryResponse];
    self.outdtDataset = [aDecoder decodeObjectForKey:kAccountSummaryOutdtDataset];
    self.outdtData = [aDecoder decodeObjectForKey:kAccountSummaryOutdtData];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kAccountSummaryStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kAccountSummaryResponse];
    [aCoder encodeObject:_outdtDataset forKey:kAccountSummaryOutdtDataset];
    [aCoder encodeObject:_outdtData forKey:kAccountSummaryOutdtData];
    [aCoder encodeObject:_strReturnMessage forKey:kAccountSummaryStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    AccountSummary *copy = [[AccountSummary alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
