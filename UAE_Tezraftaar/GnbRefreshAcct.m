//
//  GnbRefreshAcct.m
//
//  Created by   on 11/04/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbRefreshAcct.h"


NSString *const kGnbRefreshAcctProductId = @"product_id";
NSString *const kGnbRefreshAcctCustAcopDt = @"Cust_Acop_Dt";
NSString *const kGnbRefreshAcctAccountTypeDesc = @"account_type_desc";
NSString *const kGnbRefreshAcctDefaultOrder = @"default_order";
NSString *const kGnbRefreshAcctRegisteredAccountId = @"registered_account_id";
NSString *const kGnbRefreshAcctCardLimit = @"card_limit";
NSString *const kGnbRefreshAcctBrCode = @"br_code";
NSString *const kGnbRefreshAcctCcyCode = @"ccy_code";
NSString *const kGnbRefreshAcctAccountType = @"AccountType";
NSString *const kGnbRefreshAcctCustName = @"Cust_Name";
NSString *const kGnbRefreshAcctAccountNo = @"account_no";
NSString *const kGnbRefreshAcctAccountCurrency = @"AccountCurrency";
NSString *const kGnbRefreshAcctAddr4 = @"Addr4";
NSString *const kGnbRefreshAcctBeneficiaryId = @"beneficiary_id";
NSString *const kGnbRefreshAcctBrName = @"Br_Name";
NSString *const kGnbRefreshAcctStatus = @"status";
NSString *const kGnbRefreshAcctAccountNature = @"account_nature";
NSString *const kGnbRefreshAcctCcBalanceDate = @"cc_balance_date";
NSString *const kGnbRefreshAcctChkDigit = @"Chk_Digit";
NSString *const kGnbRefreshAcctCustNo = @"Cust_No";
NSString *const kGnbRefreshAcctCcBalance = @"cc_balance";
NSString *const kGnbRefreshAcctSortOrderCCY = @"Sort_Order_CCY";
NSString *const kGnbRefreshAcctCustBlockAmt = @"Cust_Block_Amt";
NSString *const kGnbRefreshAcctIsDefault = @"is_default";
NSString *const kGnbRefreshAcctBillDate = @"bill_date";
//NSString *const kGnbRefreshAcctAccountType = @"account_type";
NSString *const kGnbRefreshAcctAppCode = @"App_Code";
NSString *const kGnbRefreshAcctDescr = @"descr";
NSString *const kGnbRefreshAcctCurrencyDescr = @"currencyDescr";
NSString *const kGnbRefreshAcctStatementDate = @"statement_date";
NSString *const kGnbRefreshAcctM3Balance = @"m3_balance";
NSString *const kGnbRefreshAcctAddr1 = @"Addr1";
NSString *const kGnbRefreshAcctBranchUpdate = @"branch_update";
NSString *const kGnbRefreshAcctAvailableBalance = @"available_balance";
NSString *const kGnbRefreshAcctCcy = @"ccy";
NSString *const kGnbRefreshAcctCardType = @"card_type";
NSString *const kGnbRefreshAcctBalance = @"balance";
NSString *const kGnbRefreshAcctPrivileges = @"privileges";
NSString *const kGnbRefreshAcctAvailableBalanceTime = @"available_balance_time";
NSString *const kGnbRefreshAcctSortOrderACC = @"Sort_Order_ACC";
NSString *const kGnbRefreshAcctCustBal = @"Cust_Bal";
NSString *const kGnbRefreshAcctCreditLimit = @"credit_limit";
NSString *const kGnbRefreshAcctAddr2 = @"Addr2";
NSString *const kGnbRefreshAcctCustZkt = @"Cust_Zkt";
NSString *const kGnbRefreshAcctCustBalDt = @"Cust_Bal_Dt";
NSString *const kGnbRefreshAcctExpiryDate = @"expiry_date";
NSString *const kGnbRefreshAcctCurrencyPrefix = @"CurrencyPrefix";
NSString *const kGnbRefreshAcctBranchName = @"branch_name";
NSString *const kGnbRefreshAcctAccountName = @"account_name";
NSString *const kGnbRefreshAcctCountryCode = @"Country_Code";
NSString *const kGnbRefreshAcctEmbossedName = @"embossed_name";
NSString *const kGnbRefreshAcctIsOfflineBr = @"is_Offline_br";
NSString *const kGnbRefreshAcctCustOpBal = @"Cust_Op_Bal";
NSString *const kGnbRefreshAcctCardNumber = @"card_number";
NSString *const kGnbRefreshAcctDefaultRange = @"default_range";
NSString *const kGnbRefreshAcctAddr3 = @"Addr3";


@interface GnbRefreshAcct ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbRefreshAcct

@synthesize productId = _productId;
@synthesize custAcopDt = _custAcopDt;
@synthesize accountTypeDesc = _accountTypeDesc;
@synthesize defaultOrder = _defaultOrder;
@synthesize registeredAccountId = _registeredAccountId;
@synthesize cardLimit = _cardLimit;
@synthesize brCode = _brCode;
@synthesize ccyCode = _ccyCode;
@synthesize accountType = _accountType;
@synthesize custName = _custName;
@synthesize accountNo = _accountNo;
@synthesize accountCurrency = _accountCurrency;
@synthesize addr4 = _addr4;
@synthesize beneficiaryId = _beneficiaryId;
@synthesize brName = _brName;
@synthesize status = _status;
@synthesize accountNature = _accountNature;
@synthesize ccBalanceDate = _ccBalanceDate;
@synthesize chkDigit = _chkDigit;
@synthesize custNo = _custNo;
@synthesize ccBalance = _ccBalance;
@synthesize sortOrderCCY = _sortOrderCCY;
@synthesize custBlockAmt = _custBlockAmt;
@synthesize isDefault = _isDefault;
@synthesize billDate = _billDate;
//@synthesize accountType = _accountType;
@synthesize appCode = _appCode;
@synthesize descr = _descr;
@synthesize currencyDescr = _currencyDescr;
@synthesize statementDate = _statementDate;
@synthesize m3Balance = _m3Balance;
@synthesize addr1 = _addr1;
@synthesize branchUpdate = _branchUpdate;
@synthesize availableBalance = _availableBalance;
@synthesize ccy = _ccy;
@synthesize cardType = _cardType;
@synthesize balance = _balance;
@synthesize privileges = _privileges;
@synthesize availableBalanceTime = _availableBalanceTime;
@synthesize sortOrderACC = _sortOrderACC;
@synthesize custBal = _custBal;
@synthesize creditLimit = _creditLimit;
@synthesize addr2 = _addr2;
@synthesize custZkt = _custZkt;
@synthesize custBalDt = _custBalDt;
@synthesize expiryDate = _expiryDate;
@synthesize currencyPrefix = _currencyPrefix;
@synthesize branchName = _branchName;
@synthesize accountName = _accountName;
@synthesize countryCode = _countryCode;
@synthesize embossedName = _embossedName;
@synthesize isOfflineBr = _isOfflineBr;
@synthesize custOpBal = _custOpBal;
@synthesize cardNumber = _cardNumber;
@synthesize defaultRange = _defaultRange;
@synthesize addr3 = _addr3;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.productId = [self objectOrNilForKey:kGnbRefreshAcctProductId fromDictionary:dict];
        self.custAcopDt = [self objectOrNilForKey:kGnbRefreshAcctCustAcopDt fromDictionary:dict];
        self.accountTypeDesc = [self objectOrNilForKey:kGnbRefreshAcctAccountTypeDesc fromDictionary:dict];
        self.defaultOrder = [self objectOrNilForKey:kGnbRefreshAcctDefaultOrder fromDictionary:dict];
        self.registeredAccountId = [self objectOrNilForKey:kGnbRefreshAcctRegisteredAccountId fromDictionary:dict];
        self.cardLimit = [self objectOrNilForKey:kGnbRefreshAcctCardLimit fromDictionary:dict];
        self.brCode = [self objectOrNilForKey:kGnbRefreshAcctBrCode fromDictionary:dict];
        self.ccyCode = [self objectOrNilForKey:kGnbRefreshAcctCcyCode fromDictionary:dict];
        self.accountType = [self objectOrNilForKey:kGnbRefreshAcctAccountType fromDictionary:dict];
        self.custName = [self objectOrNilForKey:kGnbRefreshAcctCustName fromDictionary:dict];
        self.accountNo = [self objectOrNilForKey:kGnbRefreshAcctAccountNo fromDictionary:dict];
        self.accountCurrency = [self objectOrNilForKey:kGnbRefreshAcctAccountCurrency fromDictionary:dict];
        self.addr4 = [self objectOrNilForKey:kGnbRefreshAcctAddr4 fromDictionary:dict];
        self.beneficiaryId = [self objectOrNilForKey:kGnbRefreshAcctBeneficiaryId fromDictionary:dict];
        self.brName = [self objectOrNilForKey:kGnbRefreshAcctBrName fromDictionary:dict];
        self.status = [self objectOrNilForKey:kGnbRefreshAcctStatus fromDictionary:dict];
        self.accountNature = [self objectOrNilForKey:kGnbRefreshAcctAccountNature fromDictionary:dict];
        self.ccBalanceDate = [self objectOrNilForKey:kGnbRefreshAcctCcBalanceDate fromDictionary:dict];
        self.chkDigit = [self objectOrNilForKey:kGnbRefreshAcctChkDigit fromDictionary:dict];
        self.custNo = [self objectOrNilForKey:kGnbRefreshAcctCustNo fromDictionary:dict];
        self.ccBalance = [self objectOrNilForKey:kGnbRefreshAcctCcBalance fromDictionary:dict];
        self.sortOrderCCY = [self objectOrNilForKey:kGnbRefreshAcctSortOrderCCY fromDictionary:dict];
        self.custBlockAmt = [self objectOrNilForKey:kGnbRefreshAcctCustBlockAmt fromDictionary:dict];
        self.isDefault = [self objectOrNilForKey:kGnbRefreshAcctIsDefault fromDictionary:dict];
        self.billDate = [self objectOrNilForKey:kGnbRefreshAcctBillDate fromDictionary:dict];
        self.accountType = [self objectOrNilForKey:kGnbRefreshAcctAccountType fromDictionary:dict];
        self.appCode = [self objectOrNilForKey:kGnbRefreshAcctAppCode fromDictionary:dict];
        self.descr = [self objectOrNilForKey:kGnbRefreshAcctDescr fromDictionary:dict];
        self.currencyDescr = [self objectOrNilForKey:kGnbRefreshAcctCurrencyDescr fromDictionary:dict];
        self.statementDate = [self objectOrNilForKey:kGnbRefreshAcctStatementDate fromDictionary:dict];
        self.m3Balance = [self objectOrNilForKey:kGnbRefreshAcctM3Balance fromDictionary:dict];
        self.addr1 = [self objectOrNilForKey:kGnbRefreshAcctAddr1 fromDictionary:dict];
        self.branchUpdate = [self objectOrNilForKey:kGnbRefreshAcctBranchUpdate fromDictionary:dict];
        self.availableBalance = [self objectOrNilForKey:kGnbRefreshAcctAvailableBalance fromDictionary:dict];
        self.ccy = [self objectOrNilForKey:kGnbRefreshAcctCcy fromDictionary:dict];
        self.cardType = [self objectOrNilForKey:kGnbRefreshAcctCardType fromDictionary:dict];
        self.balance = [self objectOrNilForKey:kGnbRefreshAcctBalance fromDictionary:dict];
        self.privileges = [self objectOrNilForKey:kGnbRefreshAcctPrivileges fromDictionary:dict];
        self.availableBalanceTime = [self objectOrNilForKey:kGnbRefreshAcctAvailableBalanceTime fromDictionary:dict];
        self.sortOrderACC = [self objectOrNilForKey:kGnbRefreshAcctSortOrderACC fromDictionary:dict];
        self.custBal = [self objectOrNilForKey:kGnbRefreshAcctCustBal fromDictionary:dict];
        self.creditLimit = [self objectOrNilForKey:kGnbRefreshAcctCreditLimit fromDictionary:dict];
        self.addr2 = [self objectOrNilForKey:kGnbRefreshAcctAddr2 fromDictionary:dict];
        self.custZkt = [self objectOrNilForKey:kGnbRefreshAcctCustZkt fromDictionary:dict];
        self.custBalDt = [self objectOrNilForKey:kGnbRefreshAcctCustBalDt fromDictionary:dict];
        self.expiryDate = [self objectOrNilForKey:kGnbRefreshAcctExpiryDate fromDictionary:dict];
        self.currencyPrefix = [self objectOrNilForKey:kGnbRefreshAcctCurrencyPrefix fromDictionary:dict];
        self.branchName = [self objectOrNilForKey:kGnbRefreshAcctBranchName fromDictionary:dict];
        self.accountName = [self objectOrNilForKey:kGnbRefreshAcctAccountName fromDictionary:dict];
        self.countryCode = [self objectOrNilForKey:kGnbRefreshAcctCountryCode fromDictionary:dict];
        self.embossedName = [self objectOrNilForKey:kGnbRefreshAcctEmbossedName fromDictionary:dict];
        self.isOfflineBr = [self objectOrNilForKey:kGnbRefreshAcctIsOfflineBr fromDictionary:dict];
        self.custOpBal = [self objectOrNilForKey:kGnbRefreshAcctCustOpBal fromDictionary:dict];
        self.cardNumber = [self objectOrNilForKey:kGnbRefreshAcctCardNumber fromDictionary:dict];
        self.defaultRange = [self objectOrNilForKey:kGnbRefreshAcctDefaultRange fromDictionary:dict];
        self.addr3 = [self objectOrNilForKey:kGnbRefreshAcctAddr3 fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.productId forKey:kGnbRefreshAcctProductId];
    [mutableDict setValue:self.custAcopDt forKey:kGnbRefreshAcctCustAcopDt];
    [mutableDict setValue:self.accountTypeDesc forKey:kGnbRefreshAcctAccountTypeDesc];
    [mutableDict setValue:self.defaultOrder forKey:kGnbRefreshAcctDefaultOrder];
    [mutableDict setValue:self.registeredAccountId forKey:kGnbRefreshAcctRegisteredAccountId];
    [mutableDict setValue:self.cardLimit forKey:kGnbRefreshAcctCardLimit];
    [mutableDict setValue:self.brCode forKey:kGnbRefreshAcctBrCode];
    [mutableDict setValue:self.ccyCode forKey:kGnbRefreshAcctCcyCode];
    [mutableDict setValue:self.accountType forKey:kGnbRefreshAcctAccountType];
    [mutableDict setValue:self.custName forKey:kGnbRefreshAcctCustName];
    [mutableDict setValue:self.accountNo forKey:kGnbRefreshAcctAccountNo];
    [mutableDict setValue:self.accountCurrency forKey:kGnbRefreshAcctAccountCurrency];
    [mutableDict setValue:self.addr4 forKey:kGnbRefreshAcctAddr4];
    [mutableDict setValue:self.beneficiaryId forKey:kGnbRefreshAcctBeneficiaryId];
    [mutableDict setValue:self.brName forKey:kGnbRefreshAcctBrName];
    [mutableDict setValue:self.status forKey:kGnbRefreshAcctStatus];
    [mutableDict setValue:self.accountNature forKey:kGnbRefreshAcctAccountNature];
    [mutableDict setValue:self.ccBalanceDate forKey:kGnbRefreshAcctCcBalanceDate];
    [mutableDict setValue:self.chkDigit forKey:kGnbRefreshAcctChkDigit];
    [mutableDict setValue:self.custNo forKey:kGnbRefreshAcctCustNo];
    [mutableDict setValue:self.ccBalance forKey:kGnbRefreshAcctCcBalance];
    [mutableDict setValue:self.sortOrderCCY forKey:kGnbRefreshAcctSortOrderCCY];
    [mutableDict setValue:self.custBlockAmt forKey:kGnbRefreshAcctCustBlockAmt];
    [mutableDict setValue:self.isDefault forKey:kGnbRefreshAcctIsDefault];
    [mutableDict setValue:self.billDate forKey:kGnbRefreshAcctBillDate];
    [mutableDict setValue:self.accountType forKey:kGnbRefreshAcctAccountType];
    [mutableDict setValue:self.appCode forKey:kGnbRefreshAcctAppCode];
    [mutableDict setValue:self.descr forKey:kGnbRefreshAcctDescr];
    [mutableDict setValue:self.currencyDescr forKey:kGnbRefreshAcctCurrencyDescr];
    [mutableDict setValue:self.statementDate forKey:kGnbRefreshAcctStatementDate];
    [mutableDict setValue:self.m3Balance forKey:kGnbRefreshAcctM3Balance];
    [mutableDict setValue:self.addr1 forKey:kGnbRefreshAcctAddr1];
    [mutableDict setValue:self.branchUpdate forKey:kGnbRefreshAcctBranchUpdate];
    [mutableDict setValue:self.availableBalance forKey:kGnbRefreshAcctAvailableBalance];
    [mutableDict setValue:self.ccy forKey:kGnbRefreshAcctCcy];
    [mutableDict setValue:self.cardType forKey:kGnbRefreshAcctCardType];
    [mutableDict setValue:self.balance forKey:kGnbRefreshAcctBalance];
    [mutableDict setValue:self.privileges forKey:kGnbRefreshAcctPrivileges];
    [mutableDict setValue:self.availableBalanceTime forKey:kGnbRefreshAcctAvailableBalanceTime];
    [mutableDict setValue:self.sortOrderACC forKey:kGnbRefreshAcctSortOrderACC];
    [mutableDict setValue:self.custBal forKey:kGnbRefreshAcctCustBal];
    [mutableDict setValue:self.creditLimit forKey:kGnbRefreshAcctCreditLimit];
    [mutableDict setValue:self.addr2 forKey:kGnbRefreshAcctAddr2];
    [mutableDict setValue:self.custZkt forKey:kGnbRefreshAcctCustZkt];
    [mutableDict setValue:self.custBalDt forKey:kGnbRefreshAcctCustBalDt];
    [mutableDict setValue:self.expiryDate forKey:kGnbRefreshAcctExpiryDate];
    [mutableDict setValue:self.currencyPrefix forKey:kGnbRefreshAcctCurrencyPrefix];
    [mutableDict setValue:self.branchName forKey:kGnbRefreshAcctBranchName];
    [mutableDict setValue:self.accountName forKey:kGnbRefreshAcctAccountName];
    [mutableDict setValue:self.countryCode forKey:kGnbRefreshAcctCountryCode];
    [mutableDict setValue:self.embossedName forKey:kGnbRefreshAcctEmbossedName];
    [mutableDict setValue:self.isOfflineBr forKey:kGnbRefreshAcctIsOfflineBr];
    [mutableDict setValue:self.custOpBal forKey:kGnbRefreshAcctCustOpBal];
    [mutableDict setValue:self.cardNumber forKey:kGnbRefreshAcctCardNumber];
    [mutableDict setValue:self.defaultRange forKey:kGnbRefreshAcctDefaultRange];
    [mutableDict setValue:self.addr3 forKey:kGnbRefreshAcctAddr3];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.productId = [aDecoder decodeObjectForKey:kGnbRefreshAcctProductId];
    self.custAcopDt = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustAcopDt];
    self.accountTypeDesc = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountTypeDesc];
    self.defaultOrder = [aDecoder decodeObjectForKey:kGnbRefreshAcctDefaultOrder];
    self.registeredAccountId = [aDecoder decodeObjectForKey:kGnbRefreshAcctRegisteredAccountId];
    self.cardLimit = [aDecoder decodeObjectForKey:kGnbRefreshAcctCardLimit];
    self.brCode = [aDecoder decodeObjectForKey:kGnbRefreshAcctBrCode];
    self.ccyCode = [aDecoder decodeObjectForKey:kGnbRefreshAcctCcyCode];
    self.accountType = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountType];
    self.custName = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustName];
    self.accountNo = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountNo];
    self.accountCurrency = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountCurrency];
    self.addr4 = [aDecoder decodeObjectForKey:kGnbRefreshAcctAddr4];
    self.beneficiaryId = [aDecoder decodeObjectForKey:kGnbRefreshAcctBeneficiaryId];
    self.brName = [aDecoder decodeObjectForKey:kGnbRefreshAcctBrName];
    self.status = [aDecoder decodeObjectForKey:kGnbRefreshAcctStatus];
    self.accountNature = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountNature];
    self.ccBalanceDate = [aDecoder decodeObjectForKey:kGnbRefreshAcctCcBalanceDate];
    self.chkDigit = [aDecoder decodeObjectForKey:kGnbRefreshAcctChkDigit];
    self.custNo = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustNo];
    self.ccBalance = [aDecoder decodeObjectForKey:kGnbRefreshAcctCcBalance];
    self.sortOrderCCY = [aDecoder decodeObjectForKey:kGnbRefreshAcctSortOrderCCY];
    self.custBlockAmt = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustBlockAmt];
    self.isDefault = [aDecoder decodeObjectForKey:kGnbRefreshAcctIsDefault];
    self.billDate = [aDecoder decodeObjectForKey:kGnbRefreshAcctBillDate];
    self.accountType = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountType];
    self.appCode = [aDecoder decodeObjectForKey:kGnbRefreshAcctAppCode];
    self.descr = [aDecoder decodeObjectForKey:kGnbRefreshAcctDescr];
    self.currencyDescr = [aDecoder decodeObjectForKey:kGnbRefreshAcctCurrencyDescr];
    self.statementDate = [aDecoder decodeObjectForKey:kGnbRefreshAcctStatementDate];
    self.m3Balance = [aDecoder decodeObjectForKey:kGnbRefreshAcctM3Balance];
    self.addr1 = [aDecoder decodeObjectForKey:kGnbRefreshAcctAddr1];
    self.branchUpdate = [aDecoder decodeObjectForKey:kGnbRefreshAcctBranchUpdate];
    self.availableBalance = [aDecoder decodeObjectForKey:kGnbRefreshAcctAvailableBalance];
    self.ccy = [aDecoder decodeObjectForKey:kGnbRefreshAcctCcy];
    self.cardType = [aDecoder decodeObjectForKey:kGnbRefreshAcctCardType];
    self.balance = [aDecoder decodeObjectForKey:kGnbRefreshAcctBalance];
    self.privileges = [aDecoder decodeObjectForKey:kGnbRefreshAcctPrivileges];
    self.availableBalanceTime = [aDecoder decodeObjectForKey:kGnbRefreshAcctAvailableBalanceTime];
    self.sortOrderACC = [aDecoder decodeObjectForKey:kGnbRefreshAcctSortOrderACC];
    self.custBal = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustBal];
    self.creditLimit = [aDecoder decodeObjectForKey:kGnbRefreshAcctCreditLimit];
    self.addr2 = [aDecoder decodeObjectForKey:kGnbRefreshAcctAddr2];
    self.custZkt = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustZkt];
    self.custBalDt = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustBalDt];
    self.expiryDate = [aDecoder decodeObjectForKey:kGnbRefreshAcctExpiryDate];
    self.currencyPrefix = [aDecoder decodeObjectForKey:kGnbRefreshAcctCurrencyPrefix];
    self.branchName = [aDecoder decodeObjectForKey:kGnbRefreshAcctBranchName];
    self.accountName = [aDecoder decodeObjectForKey:kGnbRefreshAcctAccountName];
    self.countryCode = [aDecoder decodeObjectForKey:kGnbRefreshAcctCountryCode];
    self.embossedName = [aDecoder decodeObjectForKey:kGnbRefreshAcctEmbossedName];
    self.isOfflineBr = [aDecoder decodeObjectForKey:kGnbRefreshAcctIsOfflineBr];
    self.custOpBal = [aDecoder decodeObjectForKey:kGnbRefreshAcctCustOpBal];
    self.cardNumber = [aDecoder decodeObjectForKey:kGnbRefreshAcctCardNumber];
    self.defaultRange = [aDecoder decodeObjectForKey:kGnbRefreshAcctDefaultRange];
    self.addr3 = [aDecoder decodeObjectForKey:kGnbRefreshAcctAddr3];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_productId forKey:kGnbRefreshAcctProductId];
    [aCoder encodeObject:_custAcopDt forKey:kGnbRefreshAcctCustAcopDt];
    [aCoder encodeObject:_accountTypeDesc forKey:kGnbRefreshAcctAccountTypeDesc];
    [aCoder encodeObject:_defaultOrder forKey:kGnbRefreshAcctDefaultOrder];
    [aCoder encodeObject:_registeredAccountId forKey:kGnbRefreshAcctRegisteredAccountId];
    [aCoder encodeObject:_cardLimit forKey:kGnbRefreshAcctCardLimit];
    [aCoder encodeObject:_brCode forKey:kGnbRefreshAcctBrCode];
    [aCoder encodeObject:_ccyCode forKey:kGnbRefreshAcctCcyCode];
    [aCoder encodeObject:_accountType forKey:kGnbRefreshAcctAccountType];
    [aCoder encodeObject:_custName forKey:kGnbRefreshAcctCustName];
    [aCoder encodeObject:_accountNo forKey:kGnbRefreshAcctAccountNo];
    [aCoder encodeObject:_accountCurrency forKey:kGnbRefreshAcctAccountCurrency];
    [aCoder encodeObject:_addr4 forKey:kGnbRefreshAcctAddr4];
    [aCoder encodeObject:_beneficiaryId forKey:kGnbRefreshAcctBeneficiaryId];
    [aCoder encodeObject:_brName forKey:kGnbRefreshAcctBrName];
    [aCoder encodeObject:_status forKey:kGnbRefreshAcctStatus];
    [aCoder encodeObject:_accountNature forKey:kGnbRefreshAcctAccountNature];
    [aCoder encodeObject:_ccBalanceDate forKey:kGnbRefreshAcctCcBalanceDate];
    [aCoder encodeObject:_chkDigit forKey:kGnbRefreshAcctChkDigit];
    [aCoder encodeObject:_custNo forKey:kGnbRefreshAcctCustNo];
    [aCoder encodeObject:_ccBalance forKey:kGnbRefreshAcctCcBalance];
    [aCoder encodeObject:_sortOrderCCY forKey:kGnbRefreshAcctSortOrderCCY];
    [aCoder encodeObject:_custBlockAmt forKey:kGnbRefreshAcctCustBlockAmt];
    [aCoder encodeObject:_isDefault forKey:kGnbRefreshAcctIsDefault];
    [aCoder encodeObject:_billDate forKey:kGnbRefreshAcctBillDate];
    [aCoder encodeObject:_accountType forKey:kGnbRefreshAcctAccountType];
    [aCoder encodeObject:_appCode forKey:kGnbRefreshAcctAppCode];
    [aCoder encodeObject:_descr forKey:kGnbRefreshAcctDescr];
    [aCoder encodeObject:_currencyDescr forKey:kGnbRefreshAcctCurrencyDescr];
    [aCoder encodeObject:_statementDate forKey:kGnbRefreshAcctStatementDate];
    [aCoder encodeObject:_m3Balance forKey:kGnbRefreshAcctM3Balance];
    [aCoder encodeObject:_addr1 forKey:kGnbRefreshAcctAddr1];
    [aCoder encodeObject:_branchUpdate forKey:kGnbRefreshAcctBranchUpdate];
    [aCoder encodeObject:_availableBalance forKey:kGnbRefreshAcctAvailableBalance];
    [aCoder encodeObject:_ccy forKey:kGnbRefreshAcctCcy];
    [aCoder encodeObject:_cardType forKey:kGnbRefreshAcctCardType];
    [aCoder encodeObject:_balance forKey:kGnbRefreshAcctBalance];
    [aCoder encodeObject:_privileges forKey:kGnbRefreshAcctPrivileges];
    [aCoder encodeObject:_availableBalanceTime forKey:kGnbRefreshAcctAvailableBalanceTime];
    [aCoder encodeObject:_sortOrderACC forKey:kGnbRefreshAcctSortOrderACC];
    [aCoder encodeObject:_custBal forKey:kGnbRefreshAcctCustBal];
    [aCoder encodeObject:_creditLimit forKey:kGnbRefreshAcctCreditLimit];
    [aCoder encodeObject:_addr2 forKey:kGnbRefreshAcctAddr2];
    [aCoder encodeObject:_custZkt forKey:kGnbRefreshAcctCustZkt];
    [aCoder encodeObject:_custBalDt forKey:kGnbRefreshAcctCustBalDt];
    [aCoder encodeObject:_expiryDate forKey:kGnbRefreshAcctExpiryDate];
    [aCoder encodeObject:_currencyPrefix forKey:kGnbRefreshAcctCurrencyPrefix];
    [aCoder encodeObject:_branchName forKey:kGnbRefreshAcctBranchName];
    [aCoder encodeObject:_accountName forKey:kGnbRefreshAcctAccountName];
    [aCoder encodeObject:_countryCode forKey:kGnbRefreshAcctCountryCode];
    [aCoder encodeObject:_embossedName forKey:kGnbRefreshAcctEmbossedName];
    [aCoder encodeObject:_isOfflineBr forKey:kGnbRefreshAcctIsOfflineBr];
    [aCoder encodeObject:_custOpBal forKey:kGnbRefreshAcctCustOpBal];
    [aCoder encodeObject:_cardNumber forKey:kGnbRefreshAcctCardNumber];
    [aCoder encodeObject:_defaultRange forKey:kGnbRefreshAcctDefaultRange];
    [aCoder encodeObject:_addr3 forKey:kGnbRefreshAcctAddr3];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbRefreshAcct *copy = [[GnbRefreshAcct alloc] init];
    
    if (copy) {
        
        copy.productId = [self.productId copyWithZone:zone];
        copy.custAcopDt = [self.custAcopDt copyWithZone:zone];
        copy.accountTypeDesc = [self.accountTypeDesc copyWithZone:zone];
        copy.defaultOrder = [self.defaultOrder copyWithZone:zone];
        copy.registeredAccountId = [self.registeredAccountId copyWithZone:zone];
        copy.cardLimit = [self.cardLimit copyWithZone:zone];
        copy.brCode = [self.brCode copyWithZone:zone];
        copy.ccyCode = [self.ccyCode copyWithZone:zone];
        copy.accountType = [self.accountType copyWithZone:zone];
        copy.custName = [self.custName copyWithZone:zone];
        copy.accountNo = [self.accountNo copyWithZone:zone];
        copy.accountCurrency = [self.accountCurrency copyWithZone:zone];
        copy.addr4 = [self.addr4 copyWithZone:zone];
        copy.beneficiaryId = [self.beneficiaryId copyWithZone:zone];
        copy.brName = [self.brName copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.accountNature = [self.accountNature copyWithZone:zone];
        copy.ccBalanceDate = [self.ccBalanceDate copyWithZone:zone];
        copy.chkDigit = [self.chkDigit copyWithZone:zone];
        copy.custNo = [self.custNo copyWithZone:zone];
        copy.ccBalance = [self.ccBalance copyWithZone:zone];
        copy.sortOrderCCY = [self.sortOrderCCY copyWithZone:zone];
        copy.custBlockAmt = [self.custBlockAmt copyWithZone:zone];
        copy.isDefault = [self.isDefault copyWithZone:zone];
        copy.billDate = [self.billDate copyWithZone:zone];
        copy.accountType = [self.accountType copyWithZone:zone];
        copy.appCode = [self.appCode copyWithZone:zone];
        copy.descr = [self.descr copyWithZone:zone];
        copy.currencyDescr = [self.currencyDescr copyWithZone:zone];
        copy.statementDate = [self.statementDate copyWithZone:zone];
        copy.m3Balance = [self.m3Balance copyWithZone:zone];
        copy.addr1 = [self.addr1 copyWithZone:zone];
        copy.branchUpdate = [self.branchUpdate copyWithZone:zone];
        copy.availableBalance = [self.availableBalance copyWithZone:zone];
        copy.ccy = [self.ccy copyWithZone:zone];
        copy.cardType = [self.cardType copyWithZone:zone];
        copy.balance = [self.balance copyWithZone:zone];
        copy.privileges = [self.privileges copyWithZone:zone];
        copy.availableBalanceTime = [self.availableBalanceTime copyWithZone:zone];
        copy.sortOrderACC = [self.sortOrderACC copyWithZone:zone];
        copy.custBal = [self.custBal copyWithZone:zone];
        copy.creditLimit = [self.creditLimit copyWithZone:zone];
        copy.addr2 = [self.addr2 copyWithZone:zone];
        copy.custZkt = [self.custZkt copyWithZone:zone];
        copy.custBalDt = [self.custBalDt copyWithZone:zone];
        copy.expiryDate = [self.expiryDate copyWithZone:zone];
        copy.currencyPrefix = [self.currencyPrefix copyWithZone:zone];
        copy.branchName = [self.branchName copyWithZone:zone];
        copy.accountName = [self.accountName copyWithZone:zone];
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.embossedName = [self.embossedName copyWithZone:zone];
        copy.isOfflineBr = [self.isOfflineBr copyWithZone:zone];
        copy.custOpBal = [self.custOpBal copyWithZone:zone];
        copy.cardNumber = [self.cardNumber copyWithZone:zone];
        copy.defaultRange = [self.defaultRange copyWithZone:zone];
        copy.addr3 = [self.addr3 copyWithZone:zone];
    }
    
    return copy;
}


@end
