//
//  Load_payee_list_Model.h
//
//  Created by   on 18/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdsIBFTRelation, OutdtDataset, OutdsIBFTPOT;

@interface Load_payee_list_Model : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) OutdsIBFTRelation *outdsIBFTRelation;
@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, strong) NSString *strReturnMessage;
@property (nonatomic, strong) NSString *response;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) OutdsIBFTPOT *outdsIBFTPOT;
@property (nonatomic, strong) NSString *outStrIBFTRelation;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
