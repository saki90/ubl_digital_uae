//
//  Change_limit_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Change_limit_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField* txt_amount;
    IBOutlet UITextField* txt_atm_card;
    IBOutlet UITextField* txt_atm_pin;
    IBOutlet UILabel* lbl_current_daily_limit;
    IBOutlet UILabel* lbl_current_monthly_limit;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

