//
//  VeriftTrackPinVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 21/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "VerifyTrackPinVC.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"
#import <AVFoundation/AVFoundation.h>


@interface VerifyTrackPinVC ()
{
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* t_n_c;
    NSMutableArray* a;
    NSUserDefaults *defaults;
    NSString* ssl_count;
    NSMutableArray *arr_pin_pattern;
    NSMutableArray *arr_pw_pin;
    NSString *str_pw;
    NSString *str_Otp;
    int txt_nam;
    NSString*  first_time_chk;
    NSString *stepedVC;
}


@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) AVPlayer *avplayer;

- (IBAction)btn_back:(id)sender;
- (IBAction)btn_resend_otp:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;


@end

@implementation VerifyTrackPinVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self Play_bg_video];
    [self playerStartPlaying];
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    ssl_count = @"0";
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    txt_1.OtpDelegate = self;
    txt_2.OtpDelegate = self;
    txt_3.OtpDelegate = self;
    txt_4.OtpDelegate = self;
    txt_5.OtpDelegate = self;
    txt_6.OtpDelegate = self;
    
    txt_1.borderStyle = UITextBorderStyleNone;
    txt_2.borderStyle = UITextBorderStyleNone;
    txt_3.borderStyle = UITextBorderStyleNone;
    txt_4.borderStyle = UITextBorderStyleNone;
    txt_5.borderStyle = UITextBorderStyleNone;
    txt_6.borderStyle = UITextBorderStyleNone;
    
    [txt_1 setBackgroundColor:[UIColor clearColor]];
    [txt_2 setBackgroundColor:[UIColor clearColor]];
    [txt_3 setBackgroundColor:[UIColor clearColor]];
    [txt_4 setBackgroundColor:[UIColor clearColor]];
    [txt_5 setBackgroundColor:[UIColor clearColor]];
    [txt_6 setBackgroundColor:[UIColor clearColor]];
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
}

-(void)viewDidAppear:(BOOL)animated {
    [txt_1 becomeFirstResponder];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Play_bg_video {
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [self.avplayer play];
}


-(IBAction)btn_next:(id)sender {
    
    if ([txt_1.text isEqualToString:@""] ||
        [txt_2.text isEqualToString:@""] ||
        [txt_3.text isEqualToString:@""] ||
        [txt_4.text isEqualToString:@""] ||
        [txt_5.text isEqualToString:@""] ||
        [txt_6.text isEqualToString:@""]) {
        [self custom_alert:@"Enter All OTP Pins" :@"0"];
    }
    else {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"verifyOnBoardTrackPin";
            [self SSL_Call];
        }
    }
    
}


-(void) clearOTP {
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}



- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(event.type == UIEventSubtypeMotionShake) {
        
        txt_1.text=@"";
        txt_2.text=@"";
        txt_3.text=@"";
        txt_4.text=@"";
        txt_5.text=@"";
        txt_6.text=@"";
        
        [self.view resignFirstResponder];
        [txt_1 becomeFirstResponder];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)textFieldDidDelete:(UITextField *)textField {
    OtpTextField *otpField = (OtpTextField *)[self.view viewWithTag:textField.tag-1];
    [otpField becomeFirstResponder];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //NSUInteger MAX_DIGITS = 1;
    
    NSUInteger maxLength = 1;
    
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text leng`th :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    
    NSString *str1 = @"hello ";
    str1 = [str1 stringByAppendingString:str];
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            
            
            //            if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
            //            {
            //                btn_next.enabled=YES;
            //            }
            //            else if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
            //            {
            //                [hud hideAnimated:YES];
            //
            //                txt_1.text=@"";
            //                txt_2.text=@"";
            //                txt_3.text=@"";
            //                txt_4.text=@"";
            //                txt_5.text=@"";
            //                txt_6.text=@"";
            //
            //                [txt_1 becomeFirstResponder];
            //
            //                [self custom_alert:@"Please Accept Term&Conditition" :@"0"];
            //                return 0;
            //            }
            //            else if ([t_n_c isEqualToString:@"0"])
            //            {
            //                [hud hideAnimated:YES];
            //
            //                txt_1.text=@"";
            //                txt_2.text=@"";
            //                txt_3.text=@"";
            //                txt_4.text=@"";
            //                txt_5.text=@"";
            //                txt_6.text=@"";
            //
            //                [txt_1 becomeFirstResponder];
            //
            //                [self custom_alert:@"Please Accept Term&Conditition" :@"0"];
            //                return 0;
            //            }
            //            else if ([t_n_c isEqualToString:@"1"])
            //            {
            //                btn_next.enabled=YES;
            //            }
            
            
            
            if (textField.tag == 6)
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                if ([str_Otp length] < 6) {
                    [self clearOTP];
                    [hud hideAnimated:YES];
                    [txt_1 becomeFirstResponder];
                    [self custom_alert:@"please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
                } else {
                    //                    gblclass.step_no_onbording = @"2";
                    [self checkinternet];
                    if (netAvailable){
                        chk_ssl=@"verifyOnBoardTrackPin";
                        [self SSL_Call];
                    }
                }
                return 0;
            }
            
            
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


//////////************************** KEYBOARD CONTROLS ***************************///////////

#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //  //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillShow)
    //                                                 name:UIKeyboardWillShowNotification
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillHide)
    //                                                 name:UIKeyboardWillHideNotification
    //                                               object:nil];
    //    dispatch_async(dispatch_get_main_queue(),
    //                   ^{
    //                       //  self.fundtextfield.delegate = self;
    //
    //
    //                       //         [mine showmyhud];
    //                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
    //
    //                       //                       [self getFolionumber];
    //                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

//////////************************** END KEYBOARD CONTROLS ***************************///////////


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]) {
        
        if ([chk_ssl isEqualToString:@"verifyOnBoardTrackPin"])
        {
            chk_ssl=@"";
            [self VerifyOnBoardTrackPin:@""];
            //[self Get_Token_Request:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
        
    }
    else {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    
    
    [connection cancel];
    
    if ([chk_ssl isEqualToString:@"verifyOnBoardTrackPin"])
    {
        chk_ssl=@"";
        [self VerifyOnBoardTrackPin:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [connection cancel];
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (responseData == nil) {
        responseData = [NSMutableData dataWithData:data];
    } else {
        [responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    }
    
    // NSLog(@"%@", error.localizedDescription);
}

- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message {
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



-(void)Get_Token_Request:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        // CNIC Load ::
        NSUserDefaults *cnic = [NSUserDefaults standardUserDefaults];
        cnic =[cnic valueForKey:@"cnic"];
        gblclass.cnic_onbording = (NSString*) [encrypt de_crypt_url:cnic];
        NSLog(@"%@",gblclass.cnic_onbording);
        
        
        defaults = [NSUserDefaults standardUserDefaults];
        NSString* req_id = [defaults valueForKey:@"req_id"];
        gblclass.request_id_onbording =[encrypt de_crypt_url: req_id];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                    [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.request_id_onbording],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",   nil]];
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetTokenRequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"]){
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:@"Your request has been successfuly completed!"
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      
                      [self removeUserdata];
                      [defaults removeObjectForKey:@"apply_acct"];
                      [defaults synchronize];
                      
                  } else if ([[dic2 objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic2 objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic2 objectForKey:@"strReturnMessage"] :@"Attention"];
                      return ;
                      
                  }
                  else if ([[dic2 objectForKey:@"Response"] isEqualToString:@"1"])
                  {
                      //[hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      
                      gblclass.token = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"Token"]];
                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic2 objectForKey:@"SessionId"]];
                      
                      [self VerifyOnBoardTrackPin:@""];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      // vw_JB.hidden=YES;
                      [self custom_alert:[dic2 objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:@"Try again later." :@"0"];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Try again later." :@"0"];
        
    }
    
}




-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}


-(void)VerifyOnBoardTrackPin:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:str_Otp], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token",
                                                                       @"TrackingPin", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"VerifyOnBoardTrackPin" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return;
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                      //call getonboardstatus
                      [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccountProgressVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [self clearOTP];
        [hud hideAnimated:YES];
        [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void)GetOnBoardData:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:@"1234"],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"RequestId",
                                                                       @"strCNIC",
                                                                       @"strSessionId",
                                                                       @"Token",
                                                                       @"IP",
                                                                       @"Device_ID", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [gblclass.selected_services_onbording addObject:[dic objectForKey:@"Accounts"]];
                      [gblclass.selected_services_onbording addObject:[dic objectForKey:@"SelectedProducts"]];
                      
                      
                      NSDictionary *otherInfo = [[dic objectForKey:@"OtherInfo"] firstObject];
                      
                      gblclass.branch_code_onbording = [otherInfo objectForKey:@"strBranchCode"];
                      gblclass.mode_of_meeting = [otherInfo objectForKey:@"modeofmeeting"];
                      gblclass.preferred_time = [otherInfo objectForKey:@"preferredtime"];
                      gblclass.debitcard_name_onbording = [otherInfo objectForKey:@"debitcardname"];
                      gblclass.user_name_onbording = [otherInfo objectForKey:@"fullname"];
                      gblclass.cnic_onbording = [otherInfo objectForKey:@"cnic"];
                      gblclass.email_onbording = [otherInfo objectForKey:@"emailaddress"];
                      gblclass.mobile_number_onbording = [otherInfo objectForKey:@"mobileno"];
                      gblclass.step_no_onbording = [otherInfo objectForKey:@"stepno"];
                      
                      
                      //                      All the data provided by customer till the stage including stepno as follows:
                      //                      {
                      //                          "Accounts":[
                      //                          {"id":"1","Name":"Current","Description":"Current Account","Document":"NIC^Passport Size Picture"}],
                      //                          "selectedproducts":[
                      //                          {"id":"1","Name":"Credit Card","Description":"Credit Card","Document":"NIC^Passport Size Picture"},
                      //                          {"id":"10","Name":"Personal Loan","Description":"Personal Loan","Document":"NIC^Passport Size Picture"}],
                      //                          "otherinfo":
                      //                          [{"strBranchCode":"08090",
                      //                              "modeofmeeting":"R",
                      //                              "preferredtime":"M",
                      //                              "debitcardname":"Customer Name",
                      //                              "stepno":"5"
                      //                              "fullname":"Customer Name",
                      //                              "cnic":"4201019874561",
                      //                              "emailaddress":"a@b.com",
                      //                              "mobileno":"03213343344"
                      //                          }]
                      //                      }
                      
                      NSString *stepedVC = @"";
                      
                      switch ([gblclass.step_no_onbording integerValue]) {
                          case 1:
                              stepedVC = @"GoogleMapVC";
                              break;
                          case 2:
                              stepedVC = @"CustomizeDebitCardVC";
                              break;
                              
                              //                          case 3:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              //                          case 4:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              //                          case 5:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              //                          case 6:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              //                          case 7:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              //                          case 8:
                              //                              stepedVC = @"GoogleMapVC";
                              //                              break;
                              
                          default:
                              stepedVC = @"SelectAccountTypeVC";
                              break;
                      }
                      
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void) removeUserdata {
    [defaults removeObjectForKey:@"name"];
    [defaults removeObjectForKey:@"cnic"];
    [defaults removeObjectForKey:@"mobile_num"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"nationality"];
    [defaults removeObjectForKey:@"req_id"];
    [defaults synchronize];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_back:(id)sender
{
    [self slide_left];
    
    
    if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
    {
        stepedVC = @"login_new";
    }
    else
    {
        stepedVC = @"login";
    }
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_resend_otp:(id)sender
{
    [self slide_right];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ResetPinVC"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_feature:(id)sender
{
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"deals"];  
 
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end

