//
//  GetForgotPassClick_Model.h
//
//  Created by   on 08/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdtDataset;

@interface GetForgotPassClick_Model : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *strUserNameOut;
@property (nonatomic, strong) NSString *response;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, strong) NSString *strReturnMessage;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
