//
//  CreateCredentialsViewController.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "CreateCredentialsViewController.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GnbRefreshAcct.h"
#import "GlobalStaticClass.h"
 
#import "SWRevealViewController.h"
#import "Slide_menu_VC.h"
#import "TransitionDelegate.h"
#import "APIdleManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
 
#import "Reachability.h"
#import "Encrypt.h"


#import "Globals.h"
//saki #import <PeekabooConnect/PeekabooConnect.h>



@interface CreateCredentialsViewController ()
{
    GlobalStaticClass* gblclass;
    TransitionDelegate *transitionController;
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    UIAlertController *alert;
    MBProgressHUD* hud;
    AVPlayer *avplayer;
    NSString* chk_ssl;
    NSMutableArray  *a;
    NSString* Pw_status_chck;
    NSString* user,*pass;
    NSString* check_user;
    Encrypt* encrypt;
    NSString* ssl_count;
    NSString* t_n_c;
    NSString* debit_lock_chck;
    NSString* first_time_chk;
    
}
//@property (strong, nonatomic) IBOutlet UITextField *userName;
//@property (strong, nonatomic) IBOutlet UITextField *password;
//@property (strong, nonatomic) IBOutlet UITextField *reenterPassword;

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

- (IBAction)btn_next:(id)sender;


@end

@implementation CreateCredentialsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    encrypt = [[Encrypt alloc] init];
     [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    a = [[NSMutableArray alloc] init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    
    ssl_count = @"0";
    txt_userName.delegate=self;
    txt_password.delegate=self;
    txt_reenterPassword.delegate=self;
    
    // NSLog(@"%@",gblclass.forgot_pw_click);
    debit_lock_chck = @"no";
    
    
    
    if ([gblclass.change_pw_txt isEqualToString:@"1"] || [gblclass.forgot_pw_click isEqualToString:@"1"]) {
        lbl_heading_txt.text = @"Please change your password. This password will be used for your Digital banking accessibility";
    } else {
        lbl_heading_txt.text = @"Please create a Login ID and Password. This Login ID and Password will be used for your Digital Banking accessibility.";
    }
    
    
    if (!([gblclass.sign_up_user length] == 0)) {
        txt_userName.text=gblclass.sign_up_user;
        txt_userName.userInteractionEnabled=NO;
    } else {
        txt_userName.userInteractionEnabled=YES;
    }
    
    [self Play_bg_video];
    
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    if ([avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [avplayer seekToTime:kCMTimeZero];
    [avplayer setVolume:0.0f];
    [avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
}



- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:NO];
    
    
    //    txt_pw1.text=@"";
    
    //    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults] stringForKey:@"enable_touch"];
    //
    //
    //    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    //    {
    //        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
    //        [self presentViewController:vc animated:NO completion:nil];
    //    }
    //    else
    //    {
    //
    //    }
    
    
    //    if (runOnce == YES)
    //    {
    //
    //        self.lbl_heading.frame = CGRectMake(-(self.view.frame.size.width + self.lbl_heading.frame.size.width) ,self.lbl_heading.frame.origin.y, self.lbl_heading.frame.size.width, self.lbl_heading.frame.size.height);
    //
    //        self.cnicNumber.frame = CGRectMake(-(self.view.frame.size.width + self.cnicNumber.frame.size.width), self.cnicNumber.frame.origin.y, self.cnicNumber.frame.size.width, self.cnicNumber.frame.size.height);
    //
    //        //        emailView.frame = CGRectMake(-(self.view.frame.size.width + emailView.frame.size.width), emailView.frame.origin.y, emailView.frame.size.width, emailView.frame.size.height);
    //
    //        self.atmCardPin.frame = CGRectMake(-(self.view.frame.size.width + self.atmCardPin.frame.size.width), self.atmCardPin.frame.origin.y, self.atmCardPin.frame.size.width, self.atmCardPin.frame.size.height);
    //
    //        //        passView.frame = CGRectMake(-(self.view.frame.size.width + passView.frame.size.width), passView.frame.origin.y, passView.frame.size.width, passView.frame.size.height);
    //
    //        self.btn_next_outlet.frame = CGRectMake(-(self.view.frame.size.width + self.btn_next_outlet.frame.size.width), self.btn_next_outlet.frame.origin.y, self.btn_next_outlet.frame.size.width, self.btn_next_outlet.frame.size.height);
    //
    //        [self showAnimation];
    //
    //    }
    
    
    
    //    [UIView animateWithDuration:10.f animations:^{
    //        self.view.frame = CGRectMake(0.f, 0.f, 320.f, 568.f);
    //    }];
    
    [avplayer play];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [avplayer play];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self.view endEditing:YES];
    
    [txt_userName resignFirstResponder];
    [txt_password resignFirstResponder];
    [txt_reenterPassword resignFirstResponder];
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //777    BOOL stringIsValid;
    NSInteger MAX_DIGITS=30;
    
    
    if ([theTextField isEqual:txt_userName])
    {
        MAX_DIGITS = 15;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-._"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:txt_password])
    {
        MAX_DIGITS = 20;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-._"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    
    if ([theTextField isEqual:txt_reenterPassword])
    {
        MAX_DIGITS = 20;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789-._"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    
    
    return YES;
}

//////////************************** KEYBOARD CONTROLS ***************************///////////



//#define kOFFSET_FOR_KEYBOARD 0.0
//
//-(void)keyboardWillShow
//{
//    // Animate the current view out of the way
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)keyboardWillHide
//{
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:sender.text])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}
//
////method to move the view up/down whenever the keyboard is shown/dismissed
//-(void)setViewMovedUp:(BOOL)movedUp
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
//
//    CGRect rect = self.view.frame;
//
//    //   //NSLog(@"%d",rect );
//
//    if (movedUp)
//    {
//        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
//        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//
//
//        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
//
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//
//    }
//    else
//    {
//        // revert back to the normal state.
//        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
//        [self.view endEditing:YES];
//    }
//    self.view.frame = rect;
//
//    [UIView commitAnimations];
//}
//
//
//- (void)viewWillAppear:(BOOL)animated
//{
//    [hud hideAnimated:YES];
//    [super viewWillAppear:animated];
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    dispatch_async(dispatch_get_main_queue(),
//                   ^{
//                       //  self.fundtextfield.delegate = self;
//
//
//                       //         [mine showmyhud];
//                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
//
//                       //                       [self getFolionumber];
//                   });
//
//
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//
//    return YES;
//}











//#define kOFFSET_FOR_KEYBOARD 55.0
//
//-(void)keyboardWillShow
//{
//    // Animate the current view out of the way
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)keyboardWillHide
//{
//    if (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
//    else if (self.view.frame.origin.y < 0)
//    {
//        [self setViewMovedUp:NO];
//    }
//}
//
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if ([sender isEqual:sender.text])
//    {
//        //move the main view, so that the keyboard does not hide it.
//        if  (self.view.frame.origin.y >= 0)
//        {
//            [self setViewMovedUp:YES];
//        }
//    }
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)textField {
//    
//    [textField resignFirstResponder];
//    
//    if (textField == txt_userName)
//    {
//       // [txt_password becomeFirstResponder];
//    }
//    else if (textField == txt_password)
//    {
//      //  [txt_reenterPassword becomeFirstResponder];
//    }
//    
//    return NO;
//}
//
////method to move the view up/down whenever the keyboard is shown/dismissed
//-(void)setViewMovedUp:(BOOL)movedUp
//{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
//    
//    CGRect rect = self.view.frame;
//    
//    //  //NSLog(@"%d",rect );
//    
//    if (movedUp)
//    {
//        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
//        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//        
//        
//        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//        
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
//        
//        
//        //NSLog(@"%f", rect.origin.y );
//        //NSLog(@"%f",rect.size.height);
//        
//    }
//    else
//    {
//        // revert back to the normal state.
//        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
//        [self.view endEditing:YES];
//    }
//    self.view.frame = rect;
//    
//    [UIView commitAnimations];
//}
//
//
//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    // register for keyboard notifications
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    dispatch_async(dispatch_get_main_queue(),
//                   ^{
//                       //  self.fundtextfield.delegate = self;
//                       
//                       
//                       //         [mine showmyhud];
//                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
//                       
//                       //                       [self getFolionumber];
//                   });
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    // unregister for keyboard notifications while not visible.
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillShowNotification
//                                                  object:nil];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//}

//////////************************** END KEYBOARD CONTROLS ***************************///////////


-(void) User_Signup_Step4:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //// [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        UIDevice *deviceInfo = [UIDevice currentDevice];
        
        NSLog(@"%@",gblclass.M3sessionid);
        if (![gblclass.sign_up_user isEqualToString:@""])
        {
            txt_userName.text=gblclass.sign_up_user;
            //        _userName.enabled=NO;
            check_user = @"";
        }
        else
        {
            gblclass.sign_up_user = @"";
            check_user = txt_userName.text;
            
            
            //        _userName.enabled=YES;
        }
        
        
        NSLog(@"%@",gblclass.token);
        NSLog(@"%@",gblclass.Exis_relation_id);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abc"],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.relation_id],
                                    [encrypt encrypt_Data:check_user],
                                    [encrypt encrypt_Data:gblclass.sign_up_user],
                                    [encrypt encrypt_Data:txt_password.text],
                                    [encrypt encrypt_Data:txt_reenterPassword.text], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"LogBrowserInfo",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"relationshipID",
                                                                       @"newUserName",
                                                                       @"existingUserName",
                                                                       @"password",
                                                                       @"retypepassword", nil]];
        
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"UserSignupStep4" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject)
         {
             NSError *error1=nil;
             //NSArray *arr = (NSArray *)responseObject;
             NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
             
             
             if ([[dic objectForKey:@"Response"] integerValue]==0)
             {
                 
                 [avplayer pause];
                 gblclass.chk_new_signup_back=@"1";
                 [self UserMobMpinReg:@""];
                 
             } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                 [hud hideAnimated:YES];
                 [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                 //                 [self slide_left];
                 //                 mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                 //                 vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
                 //                 [self presentViewController:vc animated:NO completion:nil];
             }
             else
             {
                 [hud hideAnimated:YES];
                 [self custom_alert:[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]] :@"0"];
             }
             
         }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Retry" :@"0"];
                  
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
}

-(void) UserMobMpinReg:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        NSDictionary *dictparam;
        
        //        NSLog(@"%@",gblclass.chk_device_jb);
        //        NSLog(@"%@",gblclass.chk_termNCondition);
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        dictparam = [NSDictionary dictionaryWithObjects:
                     [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                      [encrypt encrypt_Data:@"1.1.1.1"],
                      [encrypt encrypt_Data:@"abv"],
                      [encrypt encrypt_Data:gblclass.user_id],
                      [encrypt encrypt_Data:gblclass.relation_id],
                      [encrypt encrypt_Data:gblclass.Udid],
                      [encrypt encrypt_Data:@"0"],
                      [encrypt encrypt_Data:gblclass.token],
                      [encrypt encrypt_Data:@"newSignUp"],
                      [encrypt encrypt_Data:@"1234"],
                      [encrypt encrypt_Data:gblclass.chk_device_jb],
                      [encrypt encrypt_Data:t_n_c], nil]
                                                forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                         @"IP",
                                                         @"hCode",
                                                         @"strUserId",
                                                         @"StrRelationShipId",
                                                         @"StrDeviceId",
                                                         @"IsTouchLoign",
                                                         @"Token",
                                                         @"calling_source",
                                                         @"strLogBrowserInfo",
                                                         @"isRooted",
                                                         @"isTnCAccepted",  nil]];
        
        
        //        NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"UserMobMpinRegRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //             NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  NSString* responsecode =[NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
                  
                  if ([responsecode isEqualToString:@"-78"] || [responsecode isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else if([responsecode isEqualToString:@"1"])
                  {
                      
                      //  [self showAlert:@"Thank you for joining UBL DIGITAL, Welcome to UBL." :@"Attention"];
                      
                      [self mob_Sign_In:@""];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      NSString* responsemsg =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                      [self showAlert:responsemsg :@"Attention"];
                  }
                  
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  
                  [hud hideAnimated:YES];
                  
                  alert = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) mob_Sign_In:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];   //baseURL];
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    gblclass.arr_act_statmnt_name=[[NSMutableArray alloc] init];
    gblclass.arr_transfer_within_acct=[[NSMutableArray alloc] init];  //Arry transfer within my acct
    gblclass.arr_transfer_within_acct_to=[[NSMutableArray alloc] init];
    
    
    
    NSDictionary *dictparam;
    
    //    dictparam = [NSDictionary dictionaryWithObjects:
    //                 [NSArray arrayWithObjects:[encrypt encrypt_Data:self.userName.text],
    //                  [encrypt encrypt_Data:self.password.text],
    //                  [encrypt encrypt_Data:gblclass.Udid],
    //                  [encrypt encrypt_Data:@"1234"],
    //                  [encrypt encrypt_Data:@"1.1.1.1"],
    //                  [encrypt encrypt_Data:@"abcd"],
    //                  [encrypt encrypt_Data:@"0"], nil]
    //
    //                                            forKeys:[NSArray arrayWithObjects:@"LoginName",
    //                                                     @"strPassword",
    //                                                     @"strDeviceID",
    //                                                     @"strSessionId",
    //                                                     @"IP",
    //                                                     @"hCode",
    //                                                     @"isTouchLogin", nil]];
    
    //    NSLog(@"%@",gblclass.isDeviceJailBreaked);
    //    NSLog(@"%@",gblclass.chk_termNCondition);
    
    dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                     [encrypt encrypt_Data:txt_userName.text],
                                                     [encrypt encrypt_Data:txt_password.text],
                                                     [encrypt encrypt_Data:gblclass.Udid],
                                                     [encrypt encrypt_Data:@"1234"],
                                                     [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                     [encrypt encrypt_Data:@"abcd"],
                                                     [encrypt encrypt_Data:@"0"],
                                                     [encrypt encrypt_Data:gblclass.chk_device_jb],
                                                     [encrypt encrypt_Data:t_n_c], nil]
                                            forKeys:[NSArray arrayWithObjects:@"LoginName",@"strPassword",@"strDeviceID",@"strSessionId",@"IP",@"hCode",@"isTouchLogin",@"isRooted",@"isTnCAccepted", nil]];
    
    //    NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"mobSignInRooted" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              @try {
                  
                  
                  NSDictionary *dic = (NSDictionary *) [encrypt de_crypt_Data:responseObject];
                  NSDictionary *dic1 = dic;
                  
                  a=[[NSMutableArray alloc] init];
                  NSMutableArray*  aa=[[NSMutableArray alloc] init];
                  //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  
                  
                  [self removeAllCredentialsForServer:@"finger_print"];
                  
                  //for finger_print enable/disable ::
                  [self finger_print_enable:@"0" forServer:@"finger_print"];
                  
                  
                  //                  gblclass.token=[dic objectForKey:@"Token"];
                  //                  Pw_status_chck=[dic objectForKey:@"Response"];
                  //                  gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  //                  gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  //                  gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                  //                  gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  //                  gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  //                  gblclass.user_id=[dic objectForKey:@"strUserId"];
                  //                  gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                  //                  gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  //                  gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                  
                  //NSLog(@"%@",Pw_status_chck);
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==-2)
                  {
                      [hud hideAnimated:YES];
                      // [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return;
                  }
                  else  if ([[dic objectForKey:@"Response"] integerValue] == 0)
                  {
                      
                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                      [defaults setValue:@"1" forKey:@"udid_chk"];
                      [defaults synchronize];
                      
                      
                      gblclass.token=[dic objectForKey:@"Token"];
                      Pw_status_chck=[dic objectForKey:@"Response"];
                      gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                      gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                      gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                      gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                      gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                      gblclass.user_id=[dic objectForKey:@"strUserId"];
                      gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                      gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                      gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                      
                      
                      
                      //                      // Remove Credential from Keychain ::
                      //                      [self removeAllCredentialsForServer:@"enable_touch"];
                      //
                      //                      // Save Credential from Keychain ::
                      //                      [self saveUsername:gblclass.sign_in_username withPassword:gblclass.sign_in_pw forServer:@"enable_touch"];
                      
                      
                      //gblclass.actsummaryarr =
                      NSArray* array=    [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                      
                      gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                      gblclass.relation_id=[[array objectAtIndex:0]objectForKey:@"RelationshipID"];
                      gblclass.franchise_btn_chck=[[array objectAtIndex:0]objectForKey:@"isFA"];
                      
                      gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                      //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                      gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                      
                      NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      if(mobileno==(NSString *)[NSNull null])
                      {
                          gblclass.Mobile_No =@"";
                      }
                      else
                      {
                          gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                      }
                      
                      
                      gblclass.arr_user_pw_table2 =
                      [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                      
                      gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                      
                      
                      NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                      
                      if (Email ==(NSString *)[NSNull null])
                      {
                          Email=@"N/A";
                          [a addObject:Email];
                      }
                      else
                      {
                          // [a addObject:Email];
                          gblclass.user_email=Email;
                      }
                      
                      NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                      
                      if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                      {
                          FundsTransferEnabled=@"N/A";
                          [a addObject:FundsTransferEnabled];
                      }
                      else
                      {
                          // [a addObject:FundsTransferEnabled];
                      }
                      
                      NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                      
                      if (Mobile_No == (NSString *)[NSNull null])
                      {
                          Mobile_No=@"N/A";
                          [a addObject:Mobile_No];
                      }
                      else
                      {
                          // [a addObject:Mobile_No];
                      }
                      
                      //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //              [a removeAllObjects];
                      //              bbb=@"";
                      
                      
                      //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                      
                      gblclass.actsummaryarr =
                      [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                      
                      
                  }
                  
                  
                  //NSLog(@"%@",gblclass.actsummaryarr);
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
                  {
                      
                      [hud hideAnimated:YES];
                      // [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
                  {
                      // [self clearOTP];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
                  {
                      [hud hideAnimated:YES];
                      //  [self clearOTP];
                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                      return ;
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]== 0  || [[dic objectForKey:@"Response"] integerValue]==101)
                  {
                      //  [self clearOTP];
                      [a removeAllObjects];
                      
                      //NSLog(@"%@",gblclass.actsummaryarr);
                      
                      
                      //                      NSUInteger *set;
                      //                      for (dic in gblclass.actsummaryarr)
                      //                      {
                      //
                      //                          NSString* acct_name=[dic objectForKey:@"account_name"];
                      //
                      //                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                      //                          {
                      //                              acct_name=@"N/A";
                      //                              [a addObject:acct_name];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_name];
                      //                          }
                      //
                      //
                      //                          NSString* acct_no=[dic objectForKey:@"account_no"];
                      //                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                      //                          {
                      //                              acct_no=@"N/A";
                      //                              [a addObject:acct_no];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_no];
                      //                          }
                      //
                      //
                      //                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                      //                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                      //                          {
                      //                              acct_id=@"N/A";
                      //                              [a addObject:acct_id];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:acct_id];
                      //                          }
                      //
                      //                          NSString* is_default=[dic objectForKey:@"is_default"];
                      //                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                      //                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                      //                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                      //                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                      //
                      //                          if ([is_default isEqualToString:@"1"])
                      //                          {
                      //
                      //                              gblclass.is_default_acct_id=acct_id;
                      //                              gblclass.is_default_acct_id_name=is_default_name;
                      //                              gblclass.is_default_acct_no=is_default_acct_no;
                      //                              gblclass.is_default_m3_balc=is_default_balc;
                      //                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                      //                          }
                      //
                      //                          //privileges
                      //                          NSString* privileges=[dic objectForKey:@"privileges"];
                      //                          NSString* pri=[dic objectForKey:@"privileges"];
                      //                          privileges=[privileges substringWithRange:NSMakeRange(2,1)]; //Second bit 1 for withDraw ..
                      //
                      //
                      //                          if ([privileges isEqualToString:@"0"])
                      //                          {
                      //                              privileges=@"0";
                      //                              [a addObject:privileges];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:privileges];
                      //                          }
                      //
                      //                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                      //                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                      //                          {
                      //                              account_type_desc=@"N/A";
                      //                              [a addObject:account_type_desc];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:account_type_desc];
                      //                          }
                      //                          //account_type
                      //
                      //                          NSString* account_type=[dic objectForKey:@"account_type"];
                      //                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                      //                          {
                      //                              account_type=@"N/A";
                      //                              [a addObject:account_type];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:account_type];
                      //                          }
                      //
                      //                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                      //
                      //                          //((NSString *)[NSNull null] == m3_balc)
                      //                          //[m3_balc isEqualToString:@" "] ||
                      //
                      //
                      //                          //27 OCt New Sign IN Method :::
                      //
                      //                            if (m3_balc == [NSNull null])
                      //                            {
                      //                                m3_balc=@"0";
                      //                                [a addObject:m3_balc];
                      //                            }
                      //                            else
                      //                            {
                      //                                [a addObject:m3_balc];
                      //                            }
                      //
                      //
                      //
                      //
                      //                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                      //                          if ( branch_name == [NSNull null])
                      //                          {
                      //                              branch_name=@"N/A";
                      //                              [a addObject:branch_name];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:branch_name];
                      //                          }
                      //
                      //                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                      //                          if ( available_balance == [NSNull null])
                      //                          {
                      //                              available_balance=@"N/A";
                      //                              [a addObject:available_balance];
                      //                          }
                      //                          else
                      //                          {
                      //                              [a addObject:available_balance];
                      //                          }
                      //
                      //
                      //
                      //                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                      //                          [gblclass.arr_act_statmnt_name addObject:bbb];
                      //
                      //                          if ([privileges isEqualToString:@"1"])
                      //                          {
                      //                              [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //                          }
                      //
                      //                          //                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                      //
                      //
                      //
                      //
                      //                          //NSLog(@"%@", bbb);
                      //                          [a removeAllObjects];
                      //                          [aa removeAllObjects];
                      //
                      //                      }
                      //
                      //                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      //
                      //
                      //                      [hud hideAnimated:YES];
                      //                      [self segue1];
                      
                      
                      //                    NSUInteger *set;
                      for (dic in gblclass.actsummaryarr)
                      {
                          
                          NSString* acct_name=[dic objectForKey:@"account_name"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          
                          NSString* acct_no=[dic objectForKey:@"account_no"];
                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                          {
                              acct_no=@"N/A";
                              [a addObject:acct_no];
                          }
                          else
                          {
                              [a addObject:acct_no];
                          }
                          
                          
                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                          {
                              acct_id=@"N/A";
                              [a addObject:acct_id];
                          }
                          else
                          {
                              [a addObject:acct_id];
                          }
                          
                          NSString* is_default=[dic objectForKey:@"is_default"];
                          NSString* is_default_curr=[dic objectForKey:@"ccy"];
                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                          NSString* is_account_type=[dic objectForKey:@"account_type"];
                          NSString* is_br_code=[dic objectForKey:@"br_code"];
                          
                          
                          //privileges
                          NSString* privileges=[dic objectForKey:@"privileges"];
                          //777         NSString* pri=[dic objectForKey:@"privileges"];
                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //(1,1) //Second bit 1 for withDraw ..
                          
                          
                          if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"1"])
                          {
                              gblclass.is_default_acct_id=acct_id;
                              gblclass.is_default_acct_id_name=is_default_name;
                              gblclass.is_default_acct_no=is_default_acct_no;
                              gblclass.is_default_m3_balc=is_default_balc;
                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                              gblclass.is_default_currency=is_default_curr;
                              gblclass.is_default_acct_type=is_account_type;
                              gblclass.is_default_br_code=is_br_code;
                              
                              
                              //                              NSLog(@"%@",acct_id);
                              //                              NSLog(@"%@",is_default_name);
                              //                              NSLog(@"%@",is_default_acct_no);
                              //                              NSLog(@"%@",is_default_balc);
                              //                              NSLog(@"%@",is_default_regstd_acct_id);
                              //                              NSLog(@"%@",is_default_curr);
                              //                              NSLog(@"%@",is_account_type);
                              //                              NSLog(@"%@",is_br_code);
                              
                          }
                          else if ([is_default isEqualToString:@"1"] && [privileges isEqualToString:@"0"])
                          {
                              debit_lock_chck = @"yes";
                          }
                          
                          
                          //                          //privileges
                          //                          NSString* privileges=[dic objectForKey:@"privileges"];
                          //                          NSString* pri=[dic objectForKey:@"privileges"];
                          //                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //Second bit 1 for withDraw ..
                          
                          
                          if ([privileges isEqualToString:@"0"])
                          {
                              privileges=@"0";
                              [a addObject:privileges];
                          }
                          else
                          {
                              [a addObject:privileges];
                          }
                          
                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                          {
                              account_type_desc=@"N/A";
                              [a addObject:account_type_desc];
                          }
                          else
                          {
                              [a addObject:account_type_desc];
                          }
                          //account_type
                          
                          NSString* account_type=[dic objectForKey:@"account_type"];
                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                          {
                              account_type=@"N/A";
                              [a addObject:account_type];
                          }
                          else
                          {
                              [a addObject:account_type];
                          }
                          
                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                          
                          
                          
                          //((NSString *)[NSNull null] == m3_balc)
                          //[m3_balc isEqualToString:@" "] ||
                          
                          
                          //27 OCt New Sign IN Method :::
                          
                          if (m3_balc == (NSString *)[NSNull null])
                          {
                              m3_balc=@"0";
                              [a addObject:m3_balc];
                          }
                          else
                          {
                              [a addObject:m3_balc];
                          }
                          
                          
                          
                          
                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                          if ( branch_name == (NSString *)[NSNull null])
                          {
                              branch_name=@"N/A";
                              [a addObject:branch_name];
                          }
                          else
                          {
                              [a addObject:branch_name];
                          }
                          
                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                          if ( available_balance == (NSString *)[NSNull null])
                          {
                              available_balance=@"N/A";
                              [a addObject:available_balance];
                          }
                          else
                          {
                              [a addObject:available_balance];
                          }
                          
                          NSString* br_code=[dic objectForKey:@"br_code"];
                          if ( br_code == (NSString *)[NSNull null])
                          {
                              br_code=@"0";
                              [a addObject:br_code];
                          }
                          else
                          {
                              [a addObject:br_code];
                          }
                          
                          NSString* ccy=[dic objectForKey:@"ccy"];
                          if ( ccy == (NSString *)[NSNull null])
                          {
                              ccy=@"0";
                              [a addObject:ccy];
                          }
                          else
                          {
                              [a addObject:ccy];
                          }
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_act_statmnt_name addObject:bbb];
                          
                          if ([privileges isEqualToString:@"1"])
                          {
                              if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                              {
                                  
                                  [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                                  
                              }
                          }
                          
                          
                          NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
                          privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
                          
                          
                          if ([privileges_to isEqualToString:@"1"])
                          {
                              [gblclass.arr_transfer_within_acct_to addObject:bbb];
                          }
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                          [aa removeAllObjects];
                          
                      }
                      
                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                      
                      
                      if ([debit_lock_chck isEqualToString:@"yes"])
                      {
                          
                          //                          NSLog(@"%lu", (unsigned long)[gblclass.arr_transfer_within_acct count]);
                          //                          NSLog(@"%@",gblclass.arr_transfer_within_acct);
                          
                          if ([gblclass.arr_transfer_within_acct count]>0)
                          {
                              NSArray*  split_debit = [[gblclass.arr_transfer_within_acct objectAtIndex:0] componentsSeparatedByString: @"|"];
                              
                              NSLog(@"%@",[split_debit objectAtIndex:0]);
                              
                              gblclass.is_default_acct_id = [split_debit objectAtIndex:2];
                              gblclass.is_default_acct_id_name = [split_debit objectAtIndex:0];
                              gblclass.is_default_acct_no = [split_debit objectAtIndex:1];
                              gblclass.is_default_m3_balc = [split_debit objectAtIndex:6];
                              gblclass.registd_acct_id_trans = [split_debit objectAtIndex:2];
                              gblclass.is_default_currency = [split_debit objectAtIndex:10];
                              gblclass.is_default_acct_type = [split_debit objectAtIndex:5];
                              gblclass.is_default_br_code = [split_debit objectAtIndex:9];
                              
                              //                              NSLog(@"%@",gblclass.is_default_acct_id);
                              //                              NSLog(@"%@",gblclass.is_default_acct_id_name);
                              //                              NSLog(@"%@",gblclass.is_default_acct_no);
                              //                              NSLog(@"%@",gblclass.is_default_m3_balc);
                              //                              NSLog(@"%@",gblclass.registd_acct_id_trans);
                              //                              NSLog(@"%@",gblclass.is_default_currency);
                              //                              NSLog(@"%@",gblclass.is_default_acct_type);
                              //                              NSLog(@"%@",gblclass.is_default_br_code);
                          }
                          
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                      [self segue1];
                      
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      if ( [[dic objectForKey:@"Response"] isEqualToString:@"0"])
                      {
                          //NSLog(@" response 0");
                      }
                      else
                      {
                          //NSLog(@" response 1");
                      }
                      
                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Retry" preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alert addAction:ok];
                      
                      [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  //                  //NSError *error;
                  //                  //NSArray *arr = (NSArray *)responseObject;
                  //                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  //                  NSDictionary *dic1 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  //
                  //                  a=[[NSMutableArray alloc] init];
                  //                  NSMutableArray*  aa=[[NSMutableArray alloc] init];
                  //                  //LoginMethod *classObj = [[LoginMethod alloc] initWithDictionary:dic];
                  //
                  //
                  //                  [self removeAllCredentialsForServer:@"finger_print"];
                  //
                  //                  //for finger_print enable/disable ::
                  //                  [self finger_print_enable:@"0" forServer:@"finger_print"];
                  //
                  //
                  //
                  //                  //                  gblclass.token=[dic objectForKey:@"Token"];
                  //                  //                  Pw_status_chck=[dic objectForKey:@"Response"];
                  //                  //                  gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  //                  //                  gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  //                  //                  gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                  //                  //                  gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  //                  //                  gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  //                  //                  gblclass.user_id=[dic objectForKey:@"strUserId"];
                  //                  //                  gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                  //                  //                  gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  //                  //                  gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                  //
                  //                  //NSLog(@"%@",Pw_status_chck);
                  //
                  //                  if ([[dic objectForKey:@"Response"] integerValue]==-2)
                  //                  {
                  //                      [hud hideAnimated:YES];
                  //
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                      [alert addAction:ok];
                  //
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                      return ;
                  //                  }
                  //                  else  if ([[dic objectForKey:@"Response"] integerValue] == 0)
                  //                  {
                  //
                  //
                  //                      gblclass.token=[dic objectForKey:@"Token"];
                  //                      Pw_status_chck=[dic objectForKey:@"Response"];
                  //                      gblclass.T_Pin=[dic objectForKey:@"pinMessage"];
                  //                      gblclass.welcome_msg=[dic objectForKey:@"strWelcomeMesage"];
                  //                      gblclass.user_login_name=[dic objectForKey:@"strWelcomeMesage"];
                  //                      gblclass.daily_limit=[dic objectForKey:@"strDailyLimit"];
                  //                      gblclass.monthly_limit=[dic objectForKey:@"strMonthlyLimit"];
                  //                      gblclass.user_id=[dic objectForKey:@"strUserId"];
                  //                      gblclass.M3sessionid=[dic objectForKey:@"M3Session"];
                  //                      gblclass.is_qr_payment=[dic objectForKey:@"is_QRPayment"];
                  //                      gblclass.is_isntant_allow=[dic objectForKey:@"isInstantAllow"];
                  //
                  //
                  //
                  //                      //                      // Remove Credential from Keychain ::
                  //                      //                      [self removeAllCredentialsForServer:@"enable_touch"];
                  //                      //
                  //                      //                      // Save Credential from Keychain ::
                  //                      //                      [self saveUsername:gblclass.sign_in_username withPassword:gblclass.sign_in_pw forServer:@"enable_touch"];
                  //
                  //
                  //                      //gblclass.actsummaryarr =
                  //                      NSArray* array=    [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  //
                  //                      gblclass.user_id=[[array objectAtIndex:0]objectForKey:@"userId"];
                  //                      gblclass.franchise_btn_chck=[[array objectAtIndex:0]objectForKey:@"isFA"];
                  //
                  //                      gblclass.base_currency = [[array objectAtIndex:0]objectForKey:@"baseCCY"];
                  //                      //[(NSString *)[[[dic objectForKey:@"outdtDataset"] objectForKey:@"Table2"]objectForKey:@"baseCCY"]];
                  //                      gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  //                      gblclass.UserType = [[array objectAtIndex:0]objectForKey:@"UserType"];
                  //
                  //                      NSString *mobileno=[[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  //                      if(mobileno==(NSString *)[NSNull null])
                  //                      {
                  //                          gblclass.Mobile_No =@"";
                  //                      }
                  //                      else
                  //                      {
                  //                          gblclass.Mobile_No = [[array objectAtIndex:0]objectForKey:@"Mobile_No"];
                  //                      }
                  //
                  //
                  //                      gblclass.arr_user_pw_table2 =
                  //                      [(NSDictionary *)[dic1 objectForKey:@"outdtDataset"] objectForKey:@"Table2"];
                  //
                  //                      gblclass.base_currency = [[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"baseCCY"];
                  //
                  //
                  //                      NSString* Email=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Email"];
                  //
                  //                      if (Email ==(NSString *)[NSNull null])
                  //                      {
                  //                          Email=@"N/A";
                  //                          [a addObject:Email];
                  //                      }
                  //                      else
                  //                      {
                  //                          // [a addObject:Email];
                  //                          gblclass.user_email=Email;
                  //                      }
                  //
                  //                      NSString* FundsTransferEnabled=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"FundsTransferEnabled"];
                  //
                  //                      if (FundsTransferEnabled.length==0 || [FundsTransferEnabled isEqualToString:@"<nil>"] || [FundsTransferEnabled isEqualToString:NULL])
                  //                      {
                  //                          FundsTransferEnabled=@"N/A";
                  //                          [a addObject:FundsTransferEnabled];
                  //                      }
                  //                      else
                  //                      {
                  //                          // [a addObject:FundsTransferEnabled];
                  //                      }
                  //
                  //                      NSString* Mobile_No=[[gblclass.arr_user_pw_table2 objectAtIndex:0]objectForKey:@"Mobile_No"];
                  //
                  //                      if (Mobile_No == (NSString *)[NSNull null])
                  //                      {
                  //                          Mobile_No=@"N/A";
                  //                          [a addObject:Mobile_No];
                  //                      }
                  //                      else
                  //                      {
                  //                          // [a addObject:Mobile_No];
                  //                      }
                  //
                  //                      //              NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                  //                      //             [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //                      //              [a removeAllObjects];
                  //                      //              bbb=@"";
                  //
                  //
                  //                      //NSLog(@"%@",[[gblclass.arr_transfer_within_acct_table2 objectAtIndex:0]objectForKey:@"baseCCY"]);
                  //
                  //                      gblclass.actsummaryarr =
                  //                      [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table1"];
                  //
                  //
                  //                  }
                  //
                  //
                  //                  //NSLog(@"%@",gblclass.actsummaryarr);
                  //
                  //                  if ([[dic objectForKey:@"Response"] integerValue]==102) //change password
                  //                  {
                  //
                  //                      [hud hideAnimated:YES];
                  //
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"ReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                      [alert addAction:ok];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //
                  //                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                  //
                  //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"change_pw"];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                      return ;
                  //                  }
                  //                  else if ([[dic objectForKey:@"Response"] integerValue]==103) //Forget Password
                  //                  {
                  //
                  //                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //                      //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                  //
                  //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"forgot_password"];
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                      return ;
                  //                  }
                  //                  else if ([[dic objectForKey:@"Response"] integerValue]==-5 || [[dic objectForKey:@"Response"] integerValue]==-6 || [[dic objectForKey:@"Response"] integerValue]==-1) //PACKAGE IS NOT ACTIVATED. OR PASSWORD IS INCORRECT
                  //                  {
                  //                      [hud hideAnimated:YES];
                  //
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Error" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                      [alert addAction:ok];
                  //
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                      return ;
                  //                  }
                  //                  else if ([[dic objectForKey:@"Response"] integerValue]== 0  || [[dic objectForKey:@"Response"] integerValue]==101)
                  //                  {
                  //                      [a removeAllObjects];
                  //
                  //                      //NSLog(@"%@",gblclass.actsummaryarr);
                  //
                  //
                  //                      //                      NSUInteger *set;
                  //                      //                      for (dic in gblclass.actsummaryarr)
                  //                      //                      {
                  //                      //
                  //                      //                          NSString* acct_name=[dic objectForKey:@"account_name"];
                  //                      //
                  //                      //                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                  //                      //                          {
                  //                      //                              acct_name=@"N/A";
                  //                      //                              [a addObject:acct_name];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:acct_name];
                  //                      //                          }
                  //                      //
                  //                      //
                  //                      //                          NSString* acct_no=[dic objectForKey:@"account_no"];
                  //                      //                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                  //                      //                          {
                  //                      //                              acct_no=@"N/A";
                  //                      //                              [a addObject:acct_no];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:acct_no];
                  //                      //                          }
                  //                      //
                  //                      //
                  //                      //                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                  //                      //                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                  //                      //                          {
                  //                      //                              acct_id=@"N/A";
                  //                      //                              [a addObject:acct_id];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:acct_id];
                  //                      //                          }
                  //                      //
                  //                      //                          NSString* is_default=[dic objectForKey:@"is_default"];
                  //                      //                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                  //                      //                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                  //                      //                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                  //                      //                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                  //                      //
                  //                      //                          if ([is_default isEqualToString:@"1"])
                  //                      //                          {
                  //                      //
                  //                      //                              gblclass.is_default_acct_id=acct_id;
                  //                      //                              gblclass.is_default_acct_id_name=is_default_name;
                  //                      //                              gblclass.is_default_acct_no=is_default_acct_no;
                  //                      //                              gblclass.is_default_m3_balc=is_default_balc;
                  //                      //                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                  //                      //                          }
                  //                      //
                  //                      //                          //privileges
                  //                      //                          NSString* privileges=[dic objectForKey:@"privileges"];
                  //                      //                          NSString* pri=[dic objectForKey:@"privileges"];
                  //                      //                          privileges=[privileges substringWithRange:NSMakeRange(2,1)]; //Second bit 1 for withDraw ..
                  //                      //
                  //                      //
                  //                      //                          if ([privileges isEqualToString:@"0"])
                  //                      //                          {
                  //                      //                              privileges=@"0";
                  //                      //                              [a addObject:privileges];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:privileges];
                  //                      //                          }
                  //                      //
                  //                      //                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                  //                      //                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                  //                      //                          {
                  //                      //                              account_type_desc=@"N/A";
                  //                      //                              [a addObject:account_type_desc];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:account_type_desc];
                  //                      //                          }
                  //                      //                          //account_type
                  //                      //
                  //                      //                          NSString* account_type=[dic objectForKey:@"account_type"];
                  //                      //                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                  //                      //                          {
                  //                      //                              account_type=@"N/A";
                  //                      //                              [a addObject:account_type];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:account_type];
                  //                      //                          }
                  //                      //
                  //                      //                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                  //                      //
                  //                      //                          //((NSString *)[NSNull null] == m3_balc)
                  //                      //                          //[m3_balc isEqualToString:@" "] ||
                  //                      //
                  //                      //
                  //                      //                          //27 OCt New Sign IN Method :::
                  //                      //
                  //                      //                            if (m3_balc == [NSNull null])
                  //                      //                            {
                  //                      //                                m3_balc=@"0";
                  //                      //                                [a addObject:m3_balc];
                  //                      //                            }
                  //                      //                            else
                  //                      //                            {
                  //                      //                                [a addObject:m3_balc];
                  //                      //                            }
                  //                      //
                  //                      //
                  //                      //
                  //                      //
                  //                      //                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                  //                      //                          if ( branch_name == [NSNull null])
                  //                      //                          {
                  //                      //                              branch_name=@"N/A";
                  //                      //                              [a addObject:branch_name];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:branch_name];
                  //                      //                          }
                  //                      //
                  //                      //                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                  //                      //                          if ( available_balance == [NSNull null])
                  //                      //                          {
                  //                      //                              available_balance=@"N/A";
                  //                      //                              [a addObject:available_balance];
                  //                      //                          }
                  //                      //                          else
                  //                      //                          {
                  //                      //                              [a addObject:available_balance];
                  //                      //                          }
                  //                      //
                  //                      //
                  //                      //
                  //                      //                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                  //                      //                          [gblclass.arr_act_statmnt_name addObject:bbb];
                  //                      //
                  //                      //                          if ([privileges isEqualToString:@"1"])
                  //                      //                          {
                  //                      //                              [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //                      //                          }
                  //                      //
                  //                      //                          //                    [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //                      //
                  //                      //
                  //                      //
                  //                      //
                  //                      //                          //NSLog(@"%@", bbb);
                  //                      //                          [a removeAllObjects];
                  //                      //                          [aa removeAllObjects];
                  //                      //
                  //                      //                      }
                  //                      //
                  //                      //                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                  //                      //                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                  //                      //                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                  //                      //
                  //                      //
                  //                      //                      [hud hideAnimated:YES];
                  //                      //                      [self segue1];
                  //
                  //
                  //                      //                    NSUInteger *set;
                  //                      for (dic in gblclass.actsummaryarr)
                  //                      {
                  //
                  //                          NSString* acct_name=[dic objectForKey:@"account_name"];
                  //
                  //                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                  //                          {
                  //                              acct_name=@"N/A";
                  //                              [a addObject:acct_name];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:acct_name];
                  //                          }
                  //
                  //
                  //                          NSString* acct_no=[dic objectForKey:@"account_no"];
                  //                          if (acct_no.length==0 || [acct_no isEqualToString:@" "] || [acct_no isEqualToString:nil])
                  //                          {
                  //                              acct_no=@"N/A";
                  //                              [a addObject:acct_no];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:acct_no];
                  //                          }
                  //
                  //
                  //                          NSString* acct_id=[dic objectForKey:@"registered_account_id"];
                  //                          if (acct_id.length==0 || [acct_id isEqualToString:@" "] || [acct_id isEqualToString:nil])
                  //                          {
                  //                              acct_id=@"N/A";
                  //                              [a addObject:acct_id];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:acct_id];
                  //                          }
                  //
                  //                          NSString* is_default=[dic objectForKey:@"is_default"];
                  //                          NSString* is_default_curr=[dic objectForKey:@"ccy"];
                  //                          NSString* is_default_name=[dic objectForKey:@"account_name"];
                  //                          NSString* is_default_acct_no=[dic objectForKey:@"account_no"];
                  //                          NSString* is_default_balc=[dic objectForKey:@"m3_balance"];
                  //                          NSString* is_default_regstd_acct_id=[dic objectForKey:@"registered_account_id"];
                  //                          NSString* is_account_type=[dic objectForKey:@"account_type"];
                  //
                  //
                  //                          if ([is_default isEqualToString:@"1"])
                  //                          {
                  //                              gblclass.is_default_acct_id=acct_id;
                  //                              gblclass.is_default_acct_id_name=is_default_name;
                  //                              gblclass.is_default_acct_no=is_default_acct_no;
                  //                              gblclass.is_default_m3_balc=is_default_balc;
                  //                              gblclass.registd_acct_id_trans=is_default_regstd_acct_id;
                  //                              gblclass.is_default_currency=is_default_curr;
                  //                              gblclass.is_default_acct_type=is_account_type;
                  //                          }
                  //
                  //
                  //                          //privileges
                  //                          NSString* privileges=[dic objectForKey:@"privileges"];
                  //                          NSString* pri=[dic objectForKey:@"privileges"];
                  //                          privileges=[privileges substringWithRange:NSMakeRange(1,1)]; //Second bit 1 for withDraw ..
                  //
                  //
                  //                          if ([privileges isEqualToString:@"0"])
                  //                          {
                  //                              privileges=@"0";
                  //                              [a addObject:privileges];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:privileges];
                  //                          }
                  //
                  //                          NSString* account_type_desc=[dic objectForKey:@"account_type_desc"];
                  //                          if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
                  //                          {
                  //                              account_type_desc=@"N/A";
                  //                              [a addObject:account_type_desc];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:account_type_desc];
                  //                          }
                  //                          //account_type
                  //
                  //                          NSString* account_type=[dic objectForKey:@"account_type"];
                  //                          if (account_type.length==0 || [account_type isEqualToString:@" "] || [account_type isEqualToString:nil])
                  //                          {
                  //                              account_type=@"N/A";
                  //                              [a addObject:account_type];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:account_type];
                  //                          }
                  //
                  //                          NSString* m3_balc=[dic objectForKey:@"m3_balance"];
                  //
                  //
                  //
                  //                          //((NSString *)[NSNull null] == m3_balc)
                  //                          //[m3_balc isEqualToString:@" "] ||
                  //
                  //
                  //                          //27 OCt New Sign IN Method :::
                  //
                  //                          if (m3_balc == (NSString *)[NSNull null])
                  //                          {
                  //                              m3_balc=@"0";
                  //                              [a addObject:m3_balc];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:m3_balc];
                  //                          }
                  //
                  //
                  //
                  //
                  //                          NSString* branch_name=[dic objectForKey:@"branch_name"];
                  //                          if ( branch_name == (NSString *)[NSNull null])
                  //                          {
                  //                              branch_name=@"N/A";
                  //                              [a addObject:branch_name];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:branch_name];
                  //                          }
                  //
                  //                          NSString* available_balance=[dic objectForKey:@"available_balance"];
                  //                          if ( available_balance == (NSString *)[NSNull null])
                  //                          {
                  //                              available_balance=@"N/A";
                  //                              [a addObject:available_balance];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:available_balance];
                  //                          }
                  //
                  //                          NSString* br_code=[dic objectForKey:@"br_code"];
                  //                          if ( br_code == (NSString *)[NSNull null])
                  //                          {
                  //                              br_code=@"0";
                  //                              [a addObject:br_code];
                  //                          }
                  //                          else
                  //                          {
                  //                              [a addObject:br_code];
                  //                          }
                  //
                  //                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                  //                          [gblclass.arr_act_statmnt_name addObject:bbb];
                  //
                  //                          if ([privileges isEqualToString:@"1"])
                  //                          {
                  //                              if ([account_type isEqualToString:@"SY"] || [account_type isEqualToString:@"CM"] || [account_type isEqualToString:@"OR"] || [account_type isEqualToString:@"RF"])
                  //                              {
                  //
                  //                                  [gblclass.arr_transfer_within_acct addObject:bbb]; ////Arry transfer within my acct
                  //
                  //                              }
                  //                          }
                  //
                  //
                  //                          NSString* privileges_to=[dic objectForKey:@"privileges"]; //for third bit 1
                  //                          privileges_to=[privileges_to substringWithRange:NSMakeRange(2,1)];
                  //
                  //
                  //                          if ([privileges_to isEqualToString:@"1"])
                  //                          {
                  //                              [gblclass.arr_transfer_within_acct_to addObject:bbb];
                  //                          }
                  //
                  //
                  //                          //NSLog(@"%@", bbb);
                  //                          [a removeAllObjects];
                  //                          [aa removeAllObjects];
                  //
                  //                      }
                  //
                  //                      //NSLog(@"arr :  %lu",(unsigned long)[gblclass.arr_transfer_within_acct count]);
                  //                      //NSLog(@"PW response :  %@",gblclass.arr_act_statmnt_name);
                  //                      //NSLog(@"PW response :  %@",[dic objectForKey:@"Response"]);
                  //
                  //
                  //                      [hud hideAnimated:YES];
                  //                      [self segue1];
                  //
                  //                  }
                  //                  else
                  //                  {
                  //                      [hud hideAnimated:YES];
                  //
                  //
                  //                      //NSLog(@" response :  %@",[dic objectForKey:@"Response"]);
                  //
                  //
                  //                      if ( [[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  //                      {
                  //                          //NSLog(@" response 0");
                  //                      }
                  //                      else
                  //                      {
                  //                          //NSLog(@" response 1");
                  //                      }
                  //
                  //                      alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Retry" preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                      UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                      [alert addAction:ok];
                  //
                  //                      [self presentViewController:alert animated:YES completion:nil];
                  //
                  //                  }
                  
              }
              @catch (NSException *exception)
              {
                  [hud hideAnimated:YES];
                  //NSLog(@"%@",exception.description);
                  //NSLog(@"%@",exception);
                  
                  //NSLog(@"%@",[exception debugDescription]);
                  
                  //exception.reason
                  
                  
                  [self custom_alert:@"Unable to Signup, Please try again later." :@"0"];
                  
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to Signup, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              //NSLog(@"%@",[error localizedDescription]);
              //NSLog(@"%@",[error localizedFailureReason]);
              //NSLog(@"%@",[error localizedRecoverySuggestion]);
              //NSLog(@"%@",task);
              
              
              [self custom_alert:@"Unable to Signup, Please try again later." :@"0"];
              
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Unable to Signup, Please try again later." preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
              
          }];
    
}




-(void) removeAllCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"2"])
        {
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        else
        {
            mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"next"];
            [self presentViewController:vc animated:NO completion:nil];
        }
        
        
        
    }
    else if(buttonIndex == 1)
    {
        
    }
    
    
}


-(void) getCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Look up server in the keychain
    NSDictionary* found = nil;
    CFDictionaryRef foundCF;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
    
    // Check if found
    found = (__bridge NSDictionary*)(foundCF);
    if (!found)
        return;
    
    // Found
    
    //17 march 2017
    // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
    pass = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    
    gblclass.sign_in_username = user;
    gblclass.sign_in_pw = pass;
    
}

-(void) finger_print_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


-(void)segue1
{
    
    //NSLog(@"%@",gblclass.ssl_pin_check);
    
    if ([Pw_status_chck isEqualToString:@"101"] || [Pw_status_chck isEqualToString:@"0"])
    {
        //act_summary
        
        gblclass.storyboardidentifer=@"password";
        
        
        //first_time
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"enable_touch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        //[self removeAllCredentialsForServer:@"touch_enable"];
        
        //NSLog(@"%@",gblclass.signup_username);
        //NSLog(@"%@",gblclass.signup_pw);
        
        
        [self with_touch_enable:@"1" forServer:@"touch_enable"];
        
        
        [self getCredentialsForServer:@"touch_enable"];
        
        
        
        //     [self saveUsername:gblclass.signup_username withPassword:gblclass.signup_pw forServer:@"touch_enable"];
        
        
        NSString *msg = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"enable_touch"]; // first_time
        
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];  //act_summary pie_chart
        [self presentViewController:vc animated:NO completion:nil];
        
        CATransition *transition = [ CATransition animation ];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [ self.view.window. layer addAnimation:transition forKey:nil];
        
    }
    
}

-(void) with_touch_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}






///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"User_Signup_Step4"])
        {
            chk_ssl=@"";
            [self User_Signup_Step4:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"User_Signup_Step4"])
    {
        chk_ssl=@"";
        [self User_Signup_Step4:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    //NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
    
}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    //    NSRange r;
    //    NSString *s = txt; //[self copy];
    //    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
    //        s = [s stringByReplacingCharactersInRange:r withString:@""];
    //    }
    //
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithData:[txt dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    NSString *s = attributedStr.string;
    
    return s;
    
}

-(IBAction)btn_feature:(id)sender
{
    if ([t_n_c isEqualToString:@"1"] || [t_n_c isEqualToString:@"2"])
    {
        [hud showAnimated:YES];
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
    }
    else
    {
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    
//    gblclass.tab_bar_login_pw_check=@"login";
//    [self peekabooSDK:@"deals"];
    
    //saki    [self custom_alert:Offer_msg :@""];
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:gblclass.story_board withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    //saki    [self peekabooSDK:@"locator"];
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_back:(id)sender
{
    // gblclass.user_login_name=@"";
    
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btn_next:(id)sender
{
    
    [self.view endEditing:YES];
    
    if([txt_userName.text isEqualToString:@""]) {
        [self custom_alert:@"Please enter username" :@"0"];
        return;
        
    } else if([txt_userName.text length] < 5) {
        [self custom_alert:@"Username should be minimum 5 and maximum 15 characters" :@"0"];
        return;
        
    } else if([txt_password.text isEqualToString:@""]) {
        [self custom_alert:@"Please enter password" :@"0"];
        return;
        
    } else if([txt_reenterPassword.text isEqualToString:@""]) {
        [self custom_alert:@"Please re-enter password" :@"0"];
        return;
        
    } else if(![txt_password.text isEqualToString:txt_reenterPassword.text]) {
        [self custom_alert:@"Re-enter password should be same as new password" :@"0"];
        return;
    }
    else
    {
        [self checkinternet];
        if (netAvailable)
        {
            
            chk_ssl=@"User_Signup_Step4";
            [self SSL_Call];
        }
    }
}


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            [hud hideAnimated:YES];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //        NSLog(@"%@",gblclass.chk_device_jb);
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.chk_device_jb],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          
                          gblclass.instant_credit_chk=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"isCredit"]];
                          gblclass.instant_debit_chk=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"isDebit"]];
                          gblclass.M3Session_ID_instant=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"strRetSessionId"]];
                          gblclass.token_instant=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"Token"]];
                          gblclass.user_id=[NSString stringWithFormat:@"%@",[dic2 objectForKey:@"strUserId"]];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}

- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
}


@end

