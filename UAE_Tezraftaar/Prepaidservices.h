//
//  AddPayee.h
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface Prepaidservices: UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    //    IBOutlet UIPickerView* picker;
    //    IBOutlet UIView* picker_view;
    
    IBOutlet UIView* vw_table;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIButton* btn_combo_box;
    IBOutlet UIImageView* img_arrow;
    IBOutlet UITextField* txt_phonenumber;
    IBOutlet UITextField* txt_nick;
    IBOutlet UIView* vw_wiz_topup;
    IBOutlet UILabel* lbl_header_tble_name;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


@property (nonatomic, strong) IBOutlet UITextField* txtbillservice;
@property (nonatomic, strong) IBOutlet UITextField* txtbilltype;
@property (nonatomic, strong) IBOutlet UITextField* txtcompany;
@property (nonatomic, strong) IBOutlet UITextField* txtphonenumber;

//@property (nonatomic, strong) IBOutlet UITextField* txtretypephonenumber;


@property (nonatomic, strong) IBOutlet UITextField* txtnick;

//@property (nonatomic, strong) IBOutlet UIView* billserviceview;
@property (nonatomic, strong) IBOutlet UIView* billtype;
@property (nonatomic, strong) IBOutlet UIView* company;

//@property (nonatomic, strong) IBOutlet UITableView* tableviewbillservice;
@property (nonatomic, strong) IBOutlet UITableView* tableviewbilltype;
@property (nonatomic, strong) IBOutlet UITableView* tableviewcompany;



@property (nonatomic, strong) IBOutlet UIButton* dd3;



@end

