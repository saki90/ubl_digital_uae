//
//  UIViewController+TextFieldDelegate.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TextFieldDelegate) <UITextFieldDelegate>

@end
