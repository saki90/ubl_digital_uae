//
//  SecureCodeOrProfileInfoViewController.h
//  ubltestbanking
//
//  Created by Asim Khan on 7/18/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface SecureCodeOrProfileInfoViewController : UIViewController {
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;


@end

