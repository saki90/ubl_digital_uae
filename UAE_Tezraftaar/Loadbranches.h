//
//  Table1.h
//
//  Created by   on 16/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Loadbranches : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *brcode;
@property (nonatomic, strong) NSString *brdetail;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
