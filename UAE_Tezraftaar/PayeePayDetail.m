//
//  PayeePayDetail.m
//
//  Created by   on 18/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "PayeePayDetail.h"


NSString *const kPayeePayDetailACCOUNTNAME = @"ACCOUNT_NAME";
NSString *const kPayeePayDetailTTNAME = @"TT_NAME";
NSString *const kPayeePayDetailLASTPAYDATE = @"LAST_PAY_DATE";
NSString *const kPayeePayDetailDEBITABLEACCTYPES = @"DEBITABLE_ACC_TYPES";
NSString *const kPayeePayDetailBANKIMD = @"BANK_IMD";
NSString *const kPayeePayDetailACCOUNTNO = @"ACCOUNT_NO";
NSString *const kPayeePayDetailRELATIONSHIPBENEFICIARY = @"RELATIONSHIPBENEFICIARY";
NSString *const kPayeePayDetailBRANCHCODE = @"BRANCH_CODE";
NSString *const kPayeePayDetailEMAIL = @"EMAIL";
NSString *const kPayeePayDetailBENEFICIARYID = @"BENEFICIARY_ID";
NSString *const kPayeePayDetailBANKNAME = @"BANK_NAME";
NSString *const kPayeePayDetailUSERACCOUNTID = @"USER_ACCOUNT_ID";
NSString *const kPayeePayDetailCDG = @"CDG";


@interface PayeePayDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PayeePayDetail

@synthesize aCCOUNTNAME = _aCCOUNTNAME;
@synthesize tTNAME = _tTNAME;
@synthesize lASTPAYDATE = _lASTPAYDATE;
@synthesize dEBITABLEACCTYPES = _dEBITABLEACCTYPES;
@synthesize bANKIMD = _bANKIMD;
@synthesize aCCOUNTNO = _aCCOUNTNO;
@synthesize rELATIONSHIPBENEFICIARY = _rELATIONSHIPBENEFICIARY;
@synthesize bRANCHCODE = _bRANCHCODE;
@synthesize eMAIL = _eMAIL;
@synthesize bENEFICIARYID = _bENEFICIARYID;
@synthesize bANKNAME = _bANKNAME;
@synthesize uSERACCOUNTID = _uSERACCOUNTID;
@synthesize cDG = _cDG;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.aCCOUNTNAME = [self objectOrNilForKey:kPayeePayDetailACCOUNTNAME fromDictionary:dict];
            self.tTNAME = [self objectOrNilForKey:kPayeePayDetailTTNAME fromDictionary:dict];
            self.lASTPAYDATE = [self objectOrNilForKey:kPayeePayDetailLASTPAYDATE fromDictionary:dict];
            self.dEBITABLEACCTYPES = [self objectOrNilForKey:kPayeePayDetailDEBITABLEACCTYPES fromDictionary:dict];
            self.bANKIMD = [self objectOrNilForKey:kPayeePayDetailBANKIMD fromDictionary:dict];
            self.aCCOUNTNO = [self objectOrNilForKey:kPayeePayDetailACCOUNTNO fromDictionary:dict];
            self.rELATIONSHIPBENEFICIARY = [self objectOrNilForKey:kPayeePayDetailRELATIONSHIPBENEFICIARY fromDictionary:dict];
            self.bRANCHCODE = [[self objectOrNilForKey:kPayeePayDetailBRANCHCODE fromDictionary:dict] doubleValue];
            self.eMAIL = [self objectOrNilForKey:kPayeePayDetailEMAIL fromDictionary:dict];
            self.bENEFICIARYID = [self objectOrNilForKey:kPayeePayDetailBENEFICIARYID fromDictionary:dict];
            self.bANKNAME = [self objectOrNilForKey:kPayeePayDetailBANKNAME fromDictionary:dict];
            self.uSERACCOUNTID = [[self objectOrNilForKey:kPayeePayDetailUSERACCOUNTID fromDictionary:dict] doubleValue];
            self.cDG = [self objectOrNilForKey:kPayeePayDetailCDG fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.aCCOUNTNAME forKey:kPayeePayDetailACCOUNTNAME];
    [mutableDict setValue:self.tTNAME forKey:kPayeePayDetailTTNAME];
    [mutableDict setValue:self.lASTPAYDATE forKey:kPayeePayDetailLASTPAYDATE];
    [mutableDict setValue:self.dEBITABLEACCTYPES forKey:kPayeePayDetailDEBITABLEACCTYPES];
    [mutableDict setValue:self.bANKIMD forKey:kPayeePayDetailBANKIMD];
    [mutableDict setValue:self.aCCOUNTNO forKey:kPayeePayDetailACCOUNTNO];
    [mutableDict setValue:self.rELATIONSHIPBENEFICIARY forKey:kPayeePayDetailRELATIONSHIPBENEFICIARY];
    [mutableDict setValue:[NSNumber numberWithDouble:self.bRANCHCODE] forKey:kPayeePayDetailBRANCHCODE];
    [mutableDict setValue:self.eMAIL forKey:kPayeePayDetailEMAIL];
    [mutableDict setValue:self.bENEFICIARYID forKey:kPayeePayDetailBENEFICIARYID];
    [mutableDict setValue:self.bANKNAME forKey:kPayeePayDetailBANKNAME];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uSERACCOUNTID] forKey:kPayeePayDetailUSERACCOUNTID];
    [mutableDict setValue:self.cDG forKey:kPayeePayDetailCDG];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.aCCOUNTNAME = [aDecoder decodeObjectForKey:kPayeePayDetailACCOUNTNAME];
    self.tTNAME = [aDecoder decodeObjectForKey:kPayeePayDetailTTNAME];
    self.lASTPAYDATE = [aDecoder decodeObjectForKey:kPayeePayDetailLASTPAYDATE];
    self.dEBITABLEACCTYPES = [aDecoder decodeObjectForKey:kPayeePayDetailDEBITABLEACCTYPES];
    self.bANKIMD = [aDecoder decodeObjectForKey:kPayeePayDetailBANKIMD];
    self.aCCOUNTNO = [aDecoder decodeObjectForKey:kPayeePayDetailACCOUNTNO];
    self.rELATIONSHIPBENEFICIARY = [aDecoder decodeObjectForKey:kPayeePayDetailRELATIONSHIPBENEFICIARY];
    self.bRANCHCODE = [aDecoder decodeDoubleForKey:kPayeePayDetailBRANCHCODE];
    self.eMAIL = [aDecoder decodeObjectForKey:kPayeePayDetailEMAIL];
    self.bENEFICIARYID = [aDecoder decodeObjectForKey:kPayeePayDetailBENEFICIARYID];
    self.bANKNAME = [aDecoder decodeObjectForKey:kPayeePayDetailBANKNAME];
    self.uSERACCOUNTID = [aDecoder decodeDoubleForKey:kPayeePayDetailUSERACCOUNTID];
    self.cDG = [aDecoder decodeObjectForKey:kPayeePayDetailCDG];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_aCCOUNTNAME forKey:kPayeePayDetailACCOUNTNAME];
    [aCoder encodeObject:_tTNAME forKey:kPayeePayDetailTTNAME];
    [aCoder encodeObject:_lASTPAYDATE forKey:kPayeePayDetailLASTPAYDATE];
    [aCoder encodeObject:_dEBITABLEACCTYPES forKey:kPayeePayDetailDEBITABLEACCTYPES];
    [aCoder encodeObject:_bANKIMD forKey:kPayeePayDetailBANKIMD];
    [aCoder encodeObject:_aCCOUNTNO forKey:kPayeePayDetailACCOUNTNO];
    [aCoder encodeObject:_rELATIONSHIPBENEFICIARY forKey:kPayeePayDetailRELATIONSHIPBENEFICIARY];
    [aCoder encodeDouble:_bRANCHCODE forKey:kPayeePayDetailBRANCHCODE];
    [aCoder encodeObject:_eMAIL forKey:kPayeePayDetailEMAIL];
    [aCoder encodeObject:_bENEFICIARYID forKey:kPayeePayDetailBENEFICIARYID];
    [aCoder encodeObject:_bANKNAME forKey:kPayeePayDetailBANKNAME];
    [aCoder encodeDouble:_uSERACCOUNTID forKey:kPayeePayDetailUSERACCOUNTID];
    [aCoder encodeObject:_cDG forKey:kPayeePayDetailCDG];
}

- (id)copyWithZone:(NSZone *)zone
{
    PayeePayDetail *copy = [[PayeePayDetail alloc] init];
    
    if (copy) {

        copy.aCCOUNTNAME = [self.aCCOUNTNAME copyWithZone:zone];
        copy.tTNAME = [self.tTNAME copyWithZone:zone];
        copy.lASTPAYDATE = [self.lASTPAYDATE copyWithZone:zone];
        copy.dEBITABLEACCTYPES = [self.dEBITABLEACCTYPES copyWithZone:zone];
        copy.bANKIMD = [self.bANKIMD copyWithZone:zone];
        copy.aCCOUNTNO = [self.aCCOUNTNO copyWithZone:zone];
        copy.rELATIONSHIPBENEFICIARY = [self.rELATIONSHIPBENEFICIARY copyWithZone:zone];
        copy.bRANCHCODE = self.bRANCHCODE;
        copy.eMAIL = [self.eMAIL copyWithZone:zone];
        copy.bENEFICIARYID = [self.bENEFICIARYID copyWithZone:zone];
        copy.bANKNAME = [self.bANKNAME copyWithZone:zone];
        copy.uSERACCOUNTID = self.uSERACCOUNTID;
        copy.cDG = [self.cDG copyWithZone:zone];
    }
    
    return copy;
}


@end
