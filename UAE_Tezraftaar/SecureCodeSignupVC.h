//
//  SecureCodeSignupVC.h
//  ubltestbanking
//
//  Created by Asim Khan on 7/10/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SecureCodeSignupVC : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *cnicNumber;
@property (strong, nonatomic) IBOutlet UITextField *secureCode;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (nonatomic, strong) AVPlayer *avplayer;

- (IBAction)btn_next:(id)sender;

@end
