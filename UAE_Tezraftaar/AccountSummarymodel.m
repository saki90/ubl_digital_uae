//
//  AccountSummarymodel.m
//
//  Created by   on 03/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AccountSummarymodel.h"
#import "OutdtDataset.h"


NSString *const kAccountSummarymodelResponse = @"Response";
NSString *const kAccountSummarymodelStrTotal = @"strTotal";
NSString *const kAccountSummarymodelOutdtData = @"outdtData";
NSString *const kAccountSummarymodelOutdtDataset = @"outdtDataset";
NSString *const kAccountSummarymodelStrReturnMessage = @"strReturnMessage";


@interface AccountSummarymodel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AccountSummarymodel

@synthesize response = _response;
@synthesize strTotal = _strTotal;
@synthesize outdtData = _outdtData;
@synthesize outdtDataset = _outdtDataset;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kAccountSummarymodelResponse fromDictionary:dict];
            self.strTotal = [self objectOrNilForKey:kAccountSummarymodelStrTotal fromDictionary:dict];
            self.outdtData = [self objectOrNilForKey:kAccountSummarymodelOutdtData fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kAccountSummarymodelOutdtDataset]];
            self.strReturnMessage = [self objectOrNilForKey:kAccountSummarymodelStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kAccountSummarymodelResponse];
    [mutableDict setValue:self.strTotal forKey:kAccountSummarymodelStrTotal];
    [mutableDict setValue:self.outdtData forKey:kAccountSummarymodelOutdtData];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kAccountSummarymodelOutdtDataset];
    [mutableDict setValue:self.strReturnMessage forKey:kAccountSummarymodelStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kAccountSummarymodelResponse];
    self.strTotal = [aDecoder decodeObjectForKey:kAccountSummarymodelStrTotal];
    self.outdtData = [aDecoder decodeObjectForKey:kAccountSummarymodelOutdtData];
    self.outdtDataset = [aDecoder decodeObjectForKey:kAccountSummarymodelOutdtDataset];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kAccountSummarymodelStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kAccountSummarymodelResponse];
    [aCoder encodeObject:_strTotal forKey:kAccountSummarymodelStrTotal];
    [aCoder encodeObject:_outdtData forKey:kAccountSummarymodelOutdtData];
    [aCoder encodeObject:_outdtDataset forKey:kAccountSummarymodelOutdtDataset];
    [aCoder encodeObject:_strReturnMessage forKey:kAccountSummarymodelStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    AccountSummarymodel *copy = [[AccountSummarymodel alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.strTotal = [self.strTotal copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
