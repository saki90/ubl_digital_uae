//
//  AccountSummarymodel.h
//
//  Created by   on 03/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdtDataset;

@interface AccountSummarymodel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *response;
@property (nonatomic, strong) NSString *strTotal;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, strong) NSString *strReturnMessage;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
