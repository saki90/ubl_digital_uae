//
//  TransitionDelegate.h
//  CustomTransitionExample
//
//  Created by Blanche Faur on 10/24/13.
//  Copyright (c) 2013 Blanche Faur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnimatedTransitioning.h"


@interface TransitionDelegate : NSObject <UIViewControllerTransitioningDelegate>//<UIViewControllerTransitioningDelegate>

@end
