//
//  Table1.m
//
//  Created by   on 11/04/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Table_de.h"


NSString *const kTable1ProductId = @"product_id";
NSString *const kTable1CustAcopDt = @"Cust_Acop_Dt";
NSString *const kTable1AccountTypeDesc = @"account_type_desc";
NSString *const kTable1DefaultOrder = @"default_order";
NSString *const kTable1RegisteredAccountId = @"registered_account_id";
NSString *const kTable1CardLimit = @"card_limit";
NSString *const kTable1BrCode = @"br_code";
NSString *const kTable1CcyCode = @"ccy_code";
NSString *const kTable1AccountType = @"AccountType";
NSString *const kTable1CustName = @"Cust_Name";
NSString *const kTable1AccountNo = @"account_no";
NSString *const kTable1AccountCurrency = @"AccountCurrency";
NSString *const kTable1Addr4 = @"Addr4";
NSString *const kTable1BeneficiaryId = @"beneficiary_id";
NSString *const kTable1BrName = @"Br_Name";
NSString *const kTable1Status = @"status";
NSString *const kTable1AccountNature = @"account_nature";
NSString *const kTable1CcBalanceDate = @"cc_balance_date";
NSString *const kTable1ChkDigit = @"Chk_Digit";
NSString *const kTable1CustNo = @"Cust_No";
NSString *const kTable1CcBalance = @"cc_balance";
NSString *const kTable1SortOrderCCY = @"Sort_Order_CCY";
NSString *const kTable1CustBlockAmt = @"Cust_Block_Amt";
NSString *const kTable1IsDefault = @"is_default";
NSString *const kTable1BillDate = @"bill_date";
//NSString *const kTable1AccountType = @"account_type";
NSString *const kTable1AppCode = @"App_Code";
NSString *const kTable1Descr = @"descr";
NSString *const kTable1CurrencyDescr = @"currencyDescr";
NSString *const kTable1StatementDate = @"statement_date";
NSString *const kTable1M3Balance = @"m3_balance";
NSString *const kTable1Addr1 = @"Addr1";
NSString *const kTable1BranchUpdate = @"branch_update";
NSString *const kTable1AvailableBalance = @"available_balance";
NSString *const kTable1Ccy = @"ccy";
NSString *const kTable1CardType = @"card_type";
NSString *const kTable1Balance = @"balance";
NSString *const kTable1Privileges = @"privileges";
NSString *const kTable1AvailableBalanceTime = @"available_balance_time";
NSString *const kTable1SortOrderACC = @"Sort_Order_ACC";
NSString *const kTable1CustBal = @"Cust_Bal";
NSString *const kTable1CreditLimit = @"credit_limit";
NSString *const kTable1Addr2 = @"Addr2";
NSString *const kTable1CustZkt = @"Cust_Zkt";
NSString *const kTable1CustBalDt = @"Cust_Bal_Dt";
NSString *const kTable1ExpiryDate = @"expiry_date";
NSString *const kTable1CurrencyPrefix = @"CurrencyPrefix";
NSString *const kTable1BranchName = @"branch_name";
NSString *const kTable1AccountName = @"account_name";
NSString *const kTable1CountryCode = @"Country_Code";
NSString *const kTable1EmbossedName = @"embossed_name";
NSString *const kTable1IsOfflineBr = @"is_Offline_br";
NSString *const kTable1CustOpBal = @"Cust_Op_Bal";
NSString *const kTable1CardNumber = @"card_number";
NSString *const kTable1DefaultRange = @"default_range";
NSString *const kTable1Addr3 = @"Addr3";


@interface Table_de ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Table_de

@synthesize productId = _productId;
@synthesize custAcopDt = _custAcopDt;
@synthesize accountTypeDesc = _accountTypeDesc;
@synthesize defaultOrder = _defaultOrder;
@synthesize registeredAccountId = _registeredAccountId;
@synthesize cardLimit = _cardLimit;
@synthesize brCode = _brCode;
@synthesize ccyCode = _ccyCode;
@synthesize accountType = _accountType;
@synthesize custName = _custName;
@synthesize accountNo = _accountNo;
@synthesize accountCurrency = _accountCurrency;
@synthesize addr4 = _addr4;
@synthesize beneficiaryId = _beneficiaryId;
@synthesize brName = _brName;
@synthesize status = _status;
@synthesize accountNature = _accountNature;
@synthesize ccBalanceDate = _ccBalanceDate;
@synthesize chkDigit = _chkDigit;
@synthesize custNo = _custNo;
@synthesize ccBalance = _ccBalance;
@synthesize sortOrderCCY = _sortOrderCCY;
@synthesize custBlockAmt = _custBlockAmt;
@synthesize isDefault = _isDefault;
@synthesize billDate = _billDate;
//@synthesize accountType = _accountType;
@synthesize appCode = _appCode;
@synthesize descr = _descr;
@synthesize currencyDescr = _currencyDescr;
@synthesize statementDate = _statementDate;
@synthesize m3Balance = _m3Balance;
@synthesize addr1 = _addr1;
@synthesize branchUpdate = _branchUpdate;
@synthesize availableBalance = _availableBalance;
@synthesize ccy = _ccy;
@synthesize cardType = _cardType;
@synthesize balance = _balance;
@synthesize privileges = _privileges;
@synthesize availableBalanceTime = _availableBalanceTime;
@synthesize sortOrderACC = _sortOrderACC;
@synthesize custBal = _custBal;
@synthesize creditLimit = _creditLimit;
@synthesize addr2 = _addr2;
@synthesize custZkt = _custZkt;
@synthesize custBalDt = _custBalDt;
@synthesize expiryDate = _expiryDate;
@synthesize currencyPrefix = _currencyPrefix;
@synthesize branchName = _branchName;
@synthesize accountName = _accountName;
@synthesize countryCode = _countryCode;
@synthesize embossedName = _embossedName;
@synthesize isOfflineBr = _isOfflineBr;
@synthesize custOpBal = _custOpBal;
@synthesize cardNumber = _cardNumber;
@synthesize defaultRange = _defaultRange;
@synthesize addr3 = _addr3;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    if(self && [dict isKindOfClass:[NSDictionary class]])
    {
        self.productId = [self objectOrNilForKey:kTable1ProductId fromDictionary:dict];
        self.custAcopDt = [self objectOrNilForKey:kTable1CustAcopDt fromDictionary:dict];
        self.accountTypeDesc = [self objectOrNilForKey:kTable1AccountTypeDesc fromDictionary:dict];
        self.defaultOrder = [self objectOrNilForKey:kTable1DefaultOrder fromDictionary:dict];
        self.registeredAccountId = [self objectOrNilForKey:kTable1RegisteredAccountId fromDictionary:dict];
        self.cardLimit = [self objectOrNilForKey:kTable1CardLimit fromDictionary:dict];
        self.brCode = [self objectOrNilForKey:kTable1BrCode fromDictionary:dict];
        self.ccyCode = [self objectOrNilForKey:kTable1CcyCode fromDictionary:dict];
        self.accountType = [self objectOrNilForKey:kTable1AccountType fromDictionary:dict];
        self.custName = [self objectOrNilForKey:kTable1CustName fromDictionary:dict];
        self.accountNo = [self objectOrNilForKey:kTable1AccountNo fromDictionary:dict];
        self.accountCurrency = [self objectOrNilForKey:kTable1AccountCurrency fromDictionary:dict];
        self.addr4 = [self objectOrNilForKey:kTable1Addr4 fromDictionary:dict];
        self.beneficiaryId = [self objectOrNilForKey:kTable1BeneficiaryId fromDictionary:dict];
        self.brName = [self objectOrNilForKey:kTable1BrName fromDictionary:dict];
        self.status = [self objectOrNilForKey:kTable1Status fromDictionary:dict];
        self.accountNature = [self objectOrNilForKey:kTable1AccountNature fromDictionary:dict];
        self.ccBalanceDate = [self objectOrNilForKey:kTable1CcBalanceDate fromDictionary:dict];
        self.chkDigit = [self objectOrNilForKey:kTable1ChkDigit fromDictionary:dict];
        self.custNo = [self objectOrNilForKey:kTable1CustNo fromDictionary:dict];
        self.ccBalance = [self objectOrNilForKey:kTable1CcBalance fromDictionary:dict];
        self.sortOrderCCY = [self objectOrNilForKey:kTable1SortOrderCCY fromDictionary:dict];
        self.custBlockAmt = [self objectOrNilForKey:kTable1CustBlockAmt fromDictionary:dict];
        self.isDefault = [self objectOrNilForKey:kTable1IsDefault fromDictionary:dict];
        self.billDate = [self objectOrNilForKey:kTable1BillDate fromDictionary:dict];
        self.accountType = [self objectOrNilForKey:kTable1AccountType fromDictionary:dict];
        self.appCode = [self objectOrNilForKey:kTable1AppCode fromDictionary:dict];
        self.descr = [self objectOrNilForKey:kTable1Descr fromDictionary:dict];
        self.currencyDescr = [self objectOrNilForKey:kTable1CurrencyDescr fromDictionary:dict];
        self.statementDate = [self objectOrNilForKey:kTable1StatementDate fromDictionary:dict];
        self.m3Balance = [self objectOrNilForKey:kTable1M3Balance fromDictionary:dict];
        self.addr1 = [self objectOrNilForKey:kTable1Addr1 fromDictionary:dict];
        self.branchUpdate = [self objectOrNilForKey:kTable1BranchUpdate fromDictionary:dict];
        self.availableBalance = [self objectOrNilForKey:kTable1AvailableBalance fromDictionary:dict];
        self.ccy = [self objectOrNilForKey:kTable1Ccy fromDictionary:dict];
        self.cardType = [self objectOrNilForKey:kTable1CardType fromDictionary:dict];
        self.balance = [self objectOrNilForKey:kTable1Balance fromDictionary:dict];
        self.privileges = [self objectOrNilForKey:kTable1Privileges fromDictionary:dict];
        self.availableBalanceTime = [self objectOrNilForKey:kTable1AvailableBalanceTime fromDictionary:dict];
        self.sortOrderACC = [self objectOrNilForKey:kTable1SortOrderACC fromDictionary:dict];
        self.custBal = [self objectOrNilForKey:kTable1CustBal fromDictionary:dict];
        self.creditLimit = [self objectOrNilForKey:kTable1CreditLimit fromDictionary:dict];
        self.addr2 = [self objectOrNilForKey:kTable1Addr2 fromDictionary:dict];
        self.custZkt = [self objectOrNilForKey:kTable1CustZkt fromDictionary:dict];
        self.custBalDt = [self objectOrNilForKey:kTable1CustBalDt fromDictionary:dict];
        self.expiryDate = [self objectOrNilForKey:kTable1ExpiryDate fromDictionary:dict];
        self.currencyPrefix = [self objectOrNilForKey:kTable1CurrencyPrefix fromDictionary:dict];
        self.branchName = [self objectOrNilForKey:kTable1BranchName fromDictionary:dict];
        self.accountName = [self objectOrNilForKey:kTable1AccountName fromDictionary:dict];
        self.countryCode = [self objectOrNilForKey:kTable1CountryCode fromDictionary:dict];
        self.embossedName = [self objectOrNilForKey:kTable1EmbossedName fromDictionary:dict];
        self.isOfflineBr = [self objectOrNilForKey:kTable1IsOfflineBr fromDictionary:dict];
        self.custOpBal = [self objectOrNilForKey:kTable1CustOpBal fromDictionary:dict];
        self.cardNumber = [self objectOrNilForKey:kTable1CardNumber fromDictionary:dict];
        self.defaultRange = [self objectOrNilForKey:kTable1DefaultRange fromDictionary:dict];
        self.addr3 = [self objectOrNilForKey:kTable1Addr3 fromDictionary:dict];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.productId forKey:kTable1ProductId];
    [mutableDict setValue:self.custAcopDt forKey:kTable1CustAcopDt];
    [mutableDict setValue:self.accountTypeDesc forKey:kTable1AccountTypeDesc];
    [mutableDict setValue:self.defaultOrder forKey:kTable1DefaultOrder];
    [mutableDict setValue:self.registeredAccountId forKey:kTable1RegisteredAccountId];
    [mutableDict setValue:self.cardLimit forKey:kTable1CardLimit];
    [mutableDict setValue:self.brCode forKey:kTable1BrCode];
    [mutableDict setValue:self.ccyCode forKey:kTable1CcyCode];
    [mutableDict setValue:self.accountType forKey:kTable1AccountType];
    [mutableDict setValue:self.custName forKey:kTable1CustName];
    [mutableDict setValue:self.accountNo forKey:kTable1AccountNo];
    [mutableDict setValue:self.accountCurrency forKey:kTable1AccountCurrency];
    [mutableDict setValue:self.addr4 forKey:kTable1Addr4];
    [mutableDict setValue:self.beneficiaryId forKey:kTable1BeneficiaryId];
    [mutableDict setValue:self.brName forKey:kTable1BrName];
    [mutableDict setValue:self.status forKey:kTable1Status];
    [mutableDict setValue:self.accountNature forKey:kTable1AccountNature];
    [mutableDict setValue:self.ccBalanceDate forKey:kTable1CcBalanceDate];
    [mutableDict setValue:self.chkDigit forKey:kTable1ChkDigit];
    [mutableDict setValue:self.custNo forKey:kTable1CustNo];
    [mutableDict setValue:self.ccBalance forKey:kTable1CcBalance];
    [mutableDict setValue:self.sortOrderCCY forKey:kTable1SortOrderCCY];
    [mutableDict setValue:self.custBlockAmt forKey:kTable1CustBlockAmt];
    [mutableDict setValue:self.isDefault forKey:kTable1IsDefault];
    [mutableDict setValue:self.billDate forKey:kTable1BillDate];
    [mutableDict setValue:self.accountType forKey:kTable1AccountType];
    [mutableDict setValue:self.appCode forKey:kTable1AppCode];
    [mutableDict setValue:self.descr forKey:kTable1Descr];
    [mutableDict setValue:self.currencyDescr forKey:kTable1CurrencyDescr];
    [mutableDict setValue:self.statementDate forKey:kTable1StatementDate];
    [mutableDict setValue:self.m3Balance forKey:kTable1M3Balance];
    [mutableDict setValue:self.addr1 forKey:kTable1Addr1];
    [mutableDict setValue:self.branchUpdate forKey:kTable1BranchUpdate];
    [mutableDict setValue:self.availableBalance forKey:kTable1AvailableBalance];
    [mutableDict setValue:self.ccy forKey:kTable1Ccy];
    [mutableDict setValue:self.cardType forKey:kTable1CardType];
    [mutableDict setValue:self.balance forKey:kTable1Balance];
    [mutableDict setValue:self.privileges forKey:kTable1Privileges];
    [mutableDict setValue:self.availableBalanceTime forKey:kTable1AvailableBalanceTime];
    [mutableDict setValue:self.sortOrderACC forKey:kTable1SortOrderACC];
    [mutableDict setValue:self.custBal forKey:kTable1CustBal];
    [mutableDict setValue:self.creditLimit forKey:kTable1CreditLimit];
    [mutableDict setValue:self.addr2 forKey:kTable1Addr2];
    [mutableDict setValue:self.custZkt forKey:kTable1CustZkt];
    [mutableDict setValue:self.custBalDt forKey:kTable1CustBalDt];
    [mutableDict setValue:self.expiryDate forKey:kTable1ExpiryDate];
    [mutableDict setValue:self.currencyPrefix forKey:kTable1CurrencyPrefix];
    [mutableDict setValue:self.branchName forKey:kTable1BranchName];
    [mutableDict setValue:self.accountName forKey:kTable1AccountName];
    [mutableDict setValue:self.countryCode forKey:kTable1CountryCode];
    [mutableDict setValue:self.embossedName forKey:kTable1EmbossedName];
    [mutableDict setValue:self.isOfflineBr forKey:kTable1IsOfflineBr];
    [mutableDict setValue:self.custOpBal forKey:kTable1CustOpBal];
    [mutableDict setValue:self.cardNumber forKey:kTable1CardNumber];
    [mutableDict setValue:self.defaultRange forKey:kTable1DefaultRange];
    [mutableDict setValue:self.addr3 forKey:kTable1Addr3];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.productId = [aDecoder decodeObjectForKey:kTable1ProductId];
    self.custAcopDt = [aDecoder decodeObjectForKey:kTable1CustAcopDt];
    self.accountTypeDesc = [aDecoder decodeObjectForKey:kTable1AccountTypeDesc];
    self.defaultOrder = [aDecoder decodeObjectForKey:kTable1DefaultOrder];
    self.registeredAccountId = [aDecoder decodeObjectForKey:kTable1RegisteredAccountId];
    self.cardLimit = [aDecoder decodeObjectForKey:kTable1CardLimit];
    self.brCode = [aDecoder decodeObjectForKey:kTable1BrCode];
    self.ccyCode = [aDecoder decodeObjectForKey:kTable1CcyCode];
    self.accountType = [aDecoder decodeObjectForKey:kTable1AccountType];
    self.custName = [aDecoder decodeObjectForKey:kTable1CustName];
    self.accountNo = [aDecoder decodeObjectForKey:kTable1AccountNo];
    self.accountCurrency = [aDecoder decodeObjectForKey:kTable1AccountCurrency];
    self.addr4 = [aDecoder decodeObjectForKey:kTable1Addr4];
    self.beneficiaryId = [aDecoder decodeObjectForKey:kTable1BeneficiaryId];
    self.brName = [aDecoder decodeObjectForKey:kTable1BrName];
    self.status = [aDecoder decodeObjectForKey:kTable1Status];
    self.accountNature = [aDecoder decodeObjectForKey:kTable1AccountNature];
    self.ccBalanceDate = [aDecoder decodeObjectForKey:kTable1CcBalanceDate];
    self.chkDigit = [aDecoder decodeObjectForKey:kTable1ChkDigit];
    self.custNo = [aDecoder decodeObjectForKey:kTable1CustNo];
    self.ccBalance = [aDecoder decodeObjectForKey:kTable1CcBalance];
    self.sortOrderCCY = [aDecoder decodeObjectForKey:kTable1SortOrderCCY];
    self.custBlockAmt = [aDecoder decodeObjectForKey:kTable1CustBlockAmt];
    self.isDefault = [aDecoder decodeObjectForKey:kTable1IsDefault];
    self.billDate = [aDecoder decodeObjectForKey:kTable1BillDate];
    self.accountType = [aDecoder decodeObjectForKey:kTable1AccountType];
    self.appCode = [aDecoder decodeObjectForKey:kTable1AppCode];
    self.descr = [aDecoder decodeObjectForKey:kTable1Descr];
    self.currencyDescr = [aDecoder decodeObjectForKey:kTable1CurrencyDescr];
    self.statementDate = [aDecoder decodeObjectForKey:kTable1StatementDate];
    self.m3Balance = [aDecoder decodeObjectForKey:kTable1M3Balance];
    self.addr1 = [aDecoder decodeObjectForKey:kTable1Addr1];
    self.branchUpdate = [aDecoder decodeObjectForKey:kTable1BranchUpdate];
    self.availableBalance = [aDecoder decodeObjectForKey:kTable1AvailableBalance];
    self.ccy = [aDecoder decodeObjectForKey:kTable1Ccy];
    self.cardType = [aDecoder decodeObjectForKey:kTable1CardType];
    self.balance = [aDecoder decodeObjectForKey:kTable1Balance];
    self.privileges = [aDecoder decodeObjectForKey:kTable1Privileges];
    self.availableBalanceTime = [aDecoder decodeObjectForKey:kTable1AvailableBalanceTime];
    self.sortOrderACC = [aDecoder decodeObjectForKey:kTable1SortOrderACC];
    self.custBal = [aDecoder decodeObjectForKey:kTable1CustBal];
    self.creditLimit = [aDecoder decodeObjectForKey:kTable1CreditLimit];
    self.addr2 = [aDecoder decodeObjectForKey:kTable1Addr2];
    self.custZkt = [aDecoder decodeObjectForKey:kTable1CustZkt];
    self.custBalDt = [aDecoder decodeObjectForKey:kTable1CustBalDt];
    self.expiryDate = [aDecoder decodeObjectForKey:kTable1ExpiryDate];
    self.currencyPrefix = [aDecoder decodeObjectForKey:kTable1CurrencyPrefix];
    self.branchName = [aDecoder decodeObjectForKey:kTable1BranchName];
    self.accountName = [aDecoder decodeObjectForKey:kTable1AccountName];
    self.countryCode = [aDecoder decodeObjectForKey:kTable1CountryCode];
    self.embossedName = [aDecoder decodeObjectForKey:kTable1EmbossedName];
    self.isOfflineBr = [aDecoder decodeObjectForKey:kTable1IsOfflineBr];
    self.custOpBal = [aDecoder decodeObjectForKey:kTable1CustOpBal];
    self.cardNumber = [aDecoder decodeObjectForKey:kTable1CardNumber];
    self.defaultRange = [aDecoder decodeObjectForKey:kTable1DefaultRange];
    self.addr3 = [aDecoder decodeObjectForKey:kTable1Addr3];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_productId forKey:kTable1ProductId];
    [aCoder encodeObject:_custAcopDt forKey:kTable1CustAcopDt];
    [aCoder encodeObject:_accountTypeDesc forKey:kTable1AccountTypeDesc];
    [aCoder encodeObject:_defaultOrder forKey:kTable1DefaultOrder];
    [aCoder encodeObject:_registeredAccountId forKey:kTable1RegisteredAccountId];
    [aCoder encodeObject:_cardLimit forKey:kTable1CardLimit];
    [aCoder encodeObject:_brCode forKey:kTable1BrCode];
    [aCoder encodeObject:_ccyCode forKey:kTable1CcyCode];
    [aCoder encodeObject:_accountType forKey:kTable1AccountType];
    [aCoder encodeObject:_custName forKey:kTable1CustName];
    [aCoder encodeObject:_accountNo forKey:kTable1AccountNo];
    [aCoder encodeObject:_accountCurrency forKey:kTable1AccountCurrency];
    [aCoder encodeObject:_addr4 forKey:kTable1Addr4];
    [aCoder encodeObject:_beneficiaryId forKey:kTable1BeneficiaryId];
    [aCoder encodeObject:_brName forKey:kTable1BrName];
    [aCoder encodeObject:_status forKey:kTable1Status];
    [aCoder encodeObject:_accountNature forKey:kTable1AccountNature];
    [aCoder encodeObject:_ccBalanceDate forKey:kTable1CcBalanceDate];
    [aCoder encodeObject:_chkDigit forKey:kTable1ChkDigit];
    [aCoder encodeObject:_custNo forKey:kTable1CustNo];
    [aCoder encodeObject:_ccBalance forKey:kTable1CcBalance];
    [aCoder encodeObject:_sortOrderCCY forKey:kTable1SortOrderCCY];
    [aCoder encodeObject:_custBlockAmt forKey:kTable1CustBlockAmt];
    [aCoder encodeObject:_isDefault forKey:kTable1IsDefault];
    [aCoder encodeObject:_billDate forKey:kTable1BillDate];
    [aCoder encodeObject:_accountType forKey:kTable1AccountType];
    [aCoder encodeObject:_appCode forKey:kTable1AppCode];
    [aCoder encodeObject:_descr forKey:kTable1Descr];
    [aCoder encodeObject:_currencyDescr forKey:kTable1CurrencyDescr];
    [aCoder encodeObject:_statementDate forKey:kTable1StatementDate];
    [aCoder encodeObject:_m3Balance forKey:kTable1M3Balance];
    [aCoder encodeObject:_addr1 forKey:kTable1Addr1];
    [aCoder encodeObject:_branchUpdate forKey:kTable1BranchUpdate];
    [aCoder encodeObject:_availableBalance forKey:kTable1AvailableBalance];
    [aCoder encodeObject:_ccy forKey:kTable1Ccy];
    [aCoder encodeObject:_cardType forKey:kTable1CardType];
    [aCoder encodeObject:_balance forKey:kTable1Balance];
    [aCoder encodeObject:_privileges forKey:kTable1Privileges];
    [aCoder encodeObject:_availableBalanceTime forKey:kTable1AvailableBalanceTime];
    [aCoder encodeObject:_sortOrderACC forKey:kTable1SortOrderACC];
    [aCoder encodeObject:_custBal forKey:kTable1CustBal];
    [aCoder encodeObject:_creditLimit forKey:kTable1CreditLimit];
    [aCoder encodeObject:_addr2 forKey:kTable1Addr2];
    [aCoder encodeObject:_custZkt forKey:kTable1CustZkt];
    [aCoder encodeObject:_custBalDt forKey:kTable1CustBalDt];
    [aCoder encodeObject:_expiryDate forKey:kTable1ExpiryDate];
    [aCoder encodeObject:_currencyPrefix forKey:kTable1CurrencyPrefix];
    [aCoder encodeObject:_branchName forKey:kTable1BranchName];
    [aCoder encodeObject:_accountName forKey:kTable1AccountName];
    [aCoder encodeObject:_countryCode forKey:kTable1CountryCode];
    [aCoder encodeObject:_embossedName forKey:kTable1EmbossedName];
    [aCoder encodeObject:_isOfflineBr forKey:kTable1IsOfflineBr];
    [aCoder encodeObject:_custOpBal forKey:kTable1CustOpBal];
    [aCoder encodeObject:_cardNumber forKey:kTable1CardNumber];
    [aCoder encodeObject:_defaultRange forKey:kTable1DefaultRange];
    [aCoder encodeObject:_addr3 forKey:kTable1Addr3];
}

- (id)copyWithZone:(NSZone *)zone
{
    Table_de *copy = [[Table_de alloc] init];
    
    if (copy) {
        
        copy.productId = [self.productId copyWithZone:zone];
        copy.custAcopDt = [self.custAcopDt copyWithZone:zone];
        copy.accountTypeDesc = [self.accountTypeDesc copyWithZone:zone];
        copy.defaultOrder = [self.defaultOrder copyWithZone:zone];
        copy.registeredAccountId = [self.registeredAccountId copyWithZone:zone];
        copy.cardLimit = [self.cardLimit copyWithZone:zone];
        copy.brCode = [self.brCode copyWithZone:zone];
        copy.ccyCode = [self.ccyCode copyWithZone:zone];
        copy.accountType = [self.accountType copyWithZone:zone];
        copy.custName = [self.custName copyWithZone:zone];
        copy.accountNo = [self.accountNo copyWithZone:zone];
        copy.accountCurrency = [self.accountCurrency copyWithZone:zone];
        copy.addr4 = [self.addr4 copyWithZone:zone];
        copy.beneficiaryId = [self.beneficiaryId copyWithZone:zone];
        copy.brName = [self.brName copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.accountNature = [self.accountNature copyWithZone:zone];
        copy.ccBalanceDate = [self.ccBalanceDate copyWithZone:zone];
        copy.chkDigit = [self.chkDigit copyWithZone:zone];
        copy.custNo = [self.custNo copyWithZone:zone];
        copy.ccBalance = [self.ccBalance copyWithZone:zone];
        copy.sortOrderCCY = [self.sortOrderCCY copyWithZone:zone];
        copy.custBlockAmt = [self.custBlockAmt copyWithZone:zone];
        copy.isDefault = [self.isDefault copyWithZone:zone];
        copy.billDate = [self.billDate copyWithZone:zone];
        copy.accountType = [self.accountType copyWithZone:zone];
        copy.appCode = [self.appCode copyWithZone:zone];
        copy.descr = [self.descr copyWithZone:zone];
        copy.currencyDescr = [self.currencyDescr copyWithZone:zone];
        copy.statementDate = [self.statementDate copyWithZone:zone];
        copy.m3Balance = [self.m3Balance copyWithZone:zone];
        copy.addr1 = [self.addr1 copyWithZone:zone];
        copy.branchUpdate = [self.branchUpdate copyWithZone:zone];
        copy.availableBalance = [self.availableBalance copyWithZone:zone];
        copy.ccy = [self.ccy copyWithZone:zone];
        copy.cardType = [self.cardType copyWithZone:zone];
        copy.balance = [self.balance copyWithZone:zone];
        copy.privileges = [self.privileges copyWithZone:zone];
        copy.availableBalanceTime = [self.availableBalanceTime copyWithZone:zone];
        copy.sortOrderACC = [self.sortOrderACC copyWithZone:zone];
        copy.custBal = [self.custBal copyWithZone:zone];
        copy.creditLimit = [self.creditLimit copyWithZone:zone];
        copy.addr2 = [self.addr2 copyWithZone:zone];
        copy.custZkt = [self.custZkt copyWithZone:zone];
        copy.custBalDt = [self.custBalDt copyWithZone:zone];
        copy.expiryDate = [self.expiryDate copyWithZone:zone];
        copy.currencyPrefix = [self.currencyPrefix copyWithZone:zone];
        copy.branchName = [self.branchName copyWithZone:zone];
        copy.accountName = [self.accountName copyWithZone:zone];
        copy.countryCode = [self.countryCode copyWithZone:zone];
        copy.embossedName = [self.embossedName copyWithZone:zone];
        copy.isOfflineBr = [self.isOfflineBr copyWithZone:zone];
        copy.custOpBal = [self.custOpBal copyWithZone:zone];
        copy.cardNumber = [self.cardNumber copyWithZone:zone];
        copy.defaultRange = [self.defaultRange copyWithZone:zone];
        copy.addr3 = [self.addr3 copyWithZone:zone];
    }
    
    return copy;
}


@end
