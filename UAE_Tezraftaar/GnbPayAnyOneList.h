//
//  GnbPayAnyOneList.h
//
//  Created by   on 17/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbPayAnyOneList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *tYPE;
@property (nonatomic, strong) NSString *aCCOUNTNAME;
@property (nonatomic, strong) NSString *lASTPAYDATE;
@property (nonatomic, assign) double bANKIMD;
@property (nonatomic, strong) NSString *aCCOUNTNO;
@property (nonatomic, assign) id cDGCODE;
@property (nonatomic, assign) double bRANCHCODE;
@property (nonatomic, strong) NSString *bANKCODE;
@property (nonatomic, assign) double bENEFICIARYID;
@property (nonatomic, strong) NSString *tTACCESSKEY;
@property (nonatomic, assign) double uSERACCOUNTID;
@property (nonatomic, strong) NSString *lastPayDate1;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
