//
//  Get_Pay_Detail_Model.h
//
//  Created by   on 17/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdtDataset;

@interface Get_Pay_Detail_Model : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *response;
@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) NSString *strReturnMessage;
@property (nonatomic, strong) NSString *strhCode;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
