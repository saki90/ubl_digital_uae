//
//  CustomizeDebitCardVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 06/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "CustomizeDebitCardVC.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"

@interface CustomizeDebitCardVC () {
    
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    NSUserDefaults *defaults;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSMutableArray *tableData;
    NSMutableArray *tableDataTitle;
    NSString* ssl_count;
    NSString* t_n_c;
    NSMutableArray* a;
    NSString *selectedAccount;
    NSString *selectedProducts;
    NSString *cardName;
    NSString*  first_time_chk;
    NSString *stepedVC;
}

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
//@property (strong, nonatomic) IBOutlet UITextField *cardName;

- (IBAction)btn_back:(id)sender;
- (IBAction)btn_help:(id)sender;
- (IBAction)btn_next:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;

@end

@implementation CustomizeDebitCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    ssl_count = @"0";
    gblclass.noOfPerformedAttempts = 0;
    
    selectedAccount = @"";
    selectedProducts = @"";
    NSMutableArray *idNumber = [[NSMutableArray alloc] init];
    NSArray *userData;
    
    // Initilizing TableView Data.
    tableDataTitle = [[NSMutableArray alloc] init];
    tableData = [[NSMutableArray alloc] init];
    self.tableView.alwaysBounceVertical = NO;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 24.0;
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    [gblclass.selected_services_onbording enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger index, BOOL *stop) {
        if ([[item objectForKey:@"NAME"] containsString:@"Account"]) {
            selectedAccount = [NSString stringWithFormat:@"%@", [item objectForKey:@"NAME"]];
        } else {
            [idNumber addObject:(NSString *)[item objectForKey:@"NAME"]];
        }
    }];
    
    //    [gblclass.selected_services_onbording enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger index, BOOL *stop) {
    //        if (index == 0) {
    //            if (gblclass.isAccountSelected) {
    //                selectedAccount = [NSString stringWithFormat:@"%@", [item objectForKey:@"NAME"]];
    //            } else {
    //                [idNumber addObject:(NSString *)[item objectForKey:@"NAME"]];
    //            }
    //        } else {
    //            [idNumber addObject:(NSString *)[item objectForKey:@"NAME"]];
    //        }
    //    }];
    
    if ([idNumber count] != 0) {
        selectedProducts = [NSString stringWithFormat:@"%@",[idNumber componentsJoinedByString:@" \n"]];
    }
    
    idNumber = nil;
    
//    if (!([[dic objectForKey:@"SelectedProducts"]  isKindOfClass:[NSNull class]] || [[dic objectForKey:@"SelectedProducts"] count] == 0)) {
//        [[dic objectForKey:@"SelectedProducts"] enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger index, BOOL *stop) {
//            NSDictionary *itemDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[item objectForKey:@"PRODUCT_ID"], @"PRODUCT_ID", [item objectForKey:@"DESCRIPTION"], @"DESCRIPTION",[item objectForKey:@"NAME"],@"NAME" ,[item objectForKey:@"REQUIREMENTS"], @"REQUIREMENTS" , nil];
//            [gblclass.selected_services_onbording addObject:itemDictionary];
//        }];
//    }
    
    
    if ([gblclass.selcted_branch_contact length] ==0) {
        gblclass.selcted_branch_contact = @"";
    }

    
    tableDataTitle = [NSMutableArray arrayWithObjects:@"Full name",
                      @"NADRA CNIC number",
                      @"Mobile",
                      @"Email",
                      @"Account Type",
                      @"Additional Services",
                      @"Time to call",
                      @"Branch", nil];
    
    userData = [NSMutableArray arrayWithObjects:gblclass.user_name_onbording,
                gblclass.cnic_onbording,
                gblclass.mobile_number_onbording,
                gblclass.email_onbording,
                selectedAccount,
                selectedProducts,
                gblclass.preferred_time,
                [NSString stringWithFormat:@"%@ \n%@ \n%@",gblclass.branch_name_onbording, gblclass.selcted_branch_address, gblclass.selcted_branch_contact], nil];
    
    
    [userData enumerateObjectsUsingBlock:^(NSString *item, NSUInteger index, BOOL * _Nonnull stop) {
        
        if (![item isEqualToString:@""]) {
            if ([item isEqual:gblclass.preferred_time]) {
                if (![gblclass.mode_of_meeting isEqualToString:@"2"]) {
                    
                    switch ([item intValue]) {
                        case 1:
                            [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:[tableDataTitle objectAtIndex:index], @"NAME" ,@"Morning" ,@"VALUE", nil]];
                            break;
                        case 2:
                            [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:[tableDataTitle objectAtIndex:index], @"NAME" ,@"Afternoon" ,@"VALUE", nil]];
                            break;
                        case 3:
                            [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:[tableDataTitle objectAtIndex:index], @"NAME" ,@"Evening" ,@"VALUE", nil]];
                            break;
                        default:
                            break;
                    }
                }
                
            }
            else
            {
                [tableData addObject:[NSDictionary dictionaryWithObjectsAndKeys:[tableDataTitle objectAtIndex:index], @"NAME" ,item ,@"VALUE", nil]];
            }
        }
    }];
    
    //    cardName.borderStyle = UITextBorderStyleNone;
    //    [cardName setBackgroundColor:[UIColor clearColor]];
    cardName = [gblclass.user_name_onbording substringToIndex: MIN(19, [gblclass.user_name_onbording length])];
    //    [cardName setPlaceholder:gblclass.user_name_onbording];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    gblclass.step_no_onbording = @"4";
//    gblclass.step_no_help_onbording = @"8";
    gblclass.step_no_help_onbording = @"DOB07";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}

/////****************** TABLEVIEW DELEGATE ******************/////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableData.count;
}

//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    NSLog(@"%lu",(unsigned long)arr_product_list.count);
//    return arr_product_list.count ;
//}

//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
//
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//    //NSLog(@"%@",(NSString *)[(NSDictionary *)[gblclass.getOnBoardProducts objectAtIndex:indexPath.row] objectForKey:@"NAME"]);
//
//    UILabel *cellLabel = (UILabel *)[cell viewWithTag:1];
//
//
////    int count = 0;
////    if(self.editing && indexPath.row != 0)
////        count = 1;
////
////    if(indexPath.row == ([gblclass.getOnBoardProducts count]) && self.editing)
////    {
////        cell.textLabel.text = @"Append a new row";
////        return cell;
////    }
////    else
////    {
//    //arr_product_list
//
//        NSArray*   split = [[arr_product_list objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
//        cellLabel.text = [split objectAtIndex:0];
////    }
//
//    return cell;
//
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *cellTitleLabel = (UILabel *)[cell viewWithTag:1];
    cellTitleLabel.text = [[tableData objectAtIndex:indexPath.row] objectForKey:@"NAME"];
    
    UILabel *cellTextLabel = (UILabel *)[cell viewWithTag:2];
    cellTextLabel.text = [[tableData objectAtIndex:indexPath.row] objectForKey:@"VALUE"];
    
    
    //    CGSize labelSize = [cellTextLabel.text sizeWithFont:cellTextLabel.font
    //                              constrainedToSize:cellTextLabel.frame.size
    //                                  lineBreakMode:cellTextLabel.lineBreakMode];
    //    cellTextLabel.frame = CGRectMake(
    //                             cellTextLabel.frame.origin.x, cellTextLabel.frame.origin.y,
    //                             cellTextLabel.frame.size.width, labelSize.height);
    return cell;
    
}


/////****************** END TABLEVIEW DELEGATE ******************/////


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"setOnBoardData"])
        {
            chk_ssl=@"";
            [self SetOnBoardData:@""];
        }
        else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
        {
            chk_ssl=@"";
            [self NeedOnBoardHelp:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    if ([chk_ssl isEqualToString:@"setOnBoardData"])
    {
        chk_ssl=@"";
        [self SetOnBoardData:@""];
    }
    else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
    {
        chk_ssl=@"";
        [self NeedOnBoardHelp:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [self.connection cancel];
    }
    
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    // NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}


-(void)NeedOnBoardHelp:(NSString *)strIndustryOmai
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        gblclass.parent_vc_onbording = @"CustomizeDebitCardVC";
        
        NSLog(@"%@",gblclass.step_no_help_onbording);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                    [encrypt encrypt_Data:gblclass.email_onbording],
                                    [encrypt encrypt_Data:gblclass.request_id_onbording],
                                    [encrypt encrypt_Data:gblclass.cnic_onbording],
                                    [encrypt encrypt_Data:gblclass.user_name_onbording],
                                    [encrypt encrypt_Data:gblclass.step_no_help_onbording],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strEmailAddress",
                                                                       @"RequestId",
                                                                       @"strCNIC",
                                                                       @"Name",
                                                                       @"StepNo",
                                                                       nil]];
        
        
        //        {
        //            NoOfAttempts = "10";
        //            Response = "0";
        //            TimerLimit = "120";
        //            strReturnMessage = "message to be on alertiew";
        //        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"NeedOnBoardHelp" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                      
                      // Changed 4 Apr.
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"-1"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      gblclass.timmerLimitOnBording = [[dic objectForKey:@"TimerLimit"] intValue];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      //                      [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      
                      //                                        [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      //                                        gblclass.helpidentifier = [dic objectForKey:@"HelpIdentifier"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"] || [[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  //                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  //                      [alert show];
                  [self slide_right];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  //                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];//                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //                      [alert show];
        [self slide_right];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        //        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void)SetOnBoardData:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.selcted_branch_code],
                                                                       [encrypt encrypt_Data:gblclass.mode_of_meeting],
                                                                       [encrypt encrypt_Data:gblclass.preferred_time],
                                                                       [encrypt encrypt_Data:gblclass.debitcard_name_onbording],
                                                                       [encrypt encrypt_Data:gblclass.selected_city],// @"karachi"
                                                                       [encrypt encrypt_Data:gblclass.step_no_onbording],
                                                                       [encrypt encrypt_Data:@"Remarks"],
                                                                       [encrypt encrypt_Data:@"0"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",
                                                                       @"strBranchCode",
                                                                       @"ModeOfMeetingId",
                                                                       @"PreferredTimeId",
                                                                       @"DebitCardName",
                                                                       @"City",
                                                                       @"StepNo",
                                                                       @"Remarks",
                                                                       @"ChannelId", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"SetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                      [self FinishOnBoarding:@""];
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)FinishOnBoarding:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
        //                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
        //                                                                       [encrypt encrypt_Data:@"1234"],
        //                                                                       [encrypt encrypt_Data:gblclass.token],
        //                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],nil]
        
        
        //   NSLog(@"%@",gblclass.request_id_onbording);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording]   ,nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"FinishOnBoarding" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      [defaults setValue:@"1" forKey:@"apply_acct"];
                      [defaults setValue:[encrypt encrypt_Data:[dic objectForKey:@"SRNumber"]] forKey:@"sr_num"];
                      [defaults synchronize];
                      
                      gblclass.sr_number = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SRNumber"]];
                      
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"CreatePinVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              }
              failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             [hud hideAnimated:YES];
             [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
         }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}



- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS=13;
    
    if ([theTextField isEqual:cardName])
    {
        MAX_DIGITS = 30;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return  YES;
}



//////////************************** START TABBAR CONTROLS ***************************///////////

-(IBAction)btn_back:(id)sender {
    [self slide_left];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"FindingLocationVC"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"lets_meet_Swift"]; // EnableGpsVC FindingLocationVC FindPlace_swift
    [self presentViewController:vc animated:NO completion:nil];
}

//-(IBAction)btn_help:(id)sender {
//    if (gblclass.noOfPerformedAttempts <= gblclass.noOfAvailableAttempts) {
//        [self checkinternet];
//        if (netAvailable) {
//            gblclass.selected_account_onbording = @"";
//            chk_ssl = @"needOnBoardHelp";
//            [self SSL_Call];
//        } else {
//            [hud hideAnimated:YES];
//            [self custom_alert:@"Please make sure you have connected to wifi" :@"0"];
//        }
//
//        //        [self slide_right];
//        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
//        //        [self presentViewController:vc animated:NO completion:nil];
//    } else {
//        [self custom_alert:[NSString stringWithFormat:@"You have made %d allowed attempts. Please contact us on 111-825-888 (UAN) or  in order to help you.", gblclass.noOfPerformedAttempts] :@"0"];
//    }
//}



-(void)btn_help:(id)sender
{
    if (gblclass.noOfPerformedAttempts <= gblclass.noOfAvailableAttempts) {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self checkinternet];
        if (netAvailable) {
            gblclass.selected_account_onbording = @"";
            chk_ssl = @"needOnBoardHelp";
            [self SSL_Call];
        } else {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please make sure you have connected to wifi" :@"0"];
        }
        
        //        [self slide_right];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
        //        [self presentViewController:vc animated:NO completion:nil];
    } else {
        [self custom_alert:[NSString stringWithFormat:@"You have made %d attempts. Please contact us on 111-825-888 (UAN) or  in order to help you.", gblclass.noOfPerformedAttempts] :@"0"];
    }
}


-(IBAction)btn_next:(id)sender {
    
    gblclass.debitcard_name_onbording = cardName;
    gblclass.parent_vc_onbording = @"CustomizeDebitCardVC";
    
    if ([cardName isEqualToString:@""]) {
        [self custom_alert:@"Please enter valid card name" :@"0"];
        
    } else {
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"setOnBoardData";
            [self SSL_Call];
        }
    }
}


-(IBAction)btn_feature:(id)sender
{
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

//////////************************** END TABBAR CONTROLS ***************************///////////

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}
-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end

