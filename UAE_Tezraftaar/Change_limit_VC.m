//
//  Change_limit_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Change_limit_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Change_limit_VC ()
{
    UIAlertController* alert;
    
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString *responsecode;
    NSString* old_pw,*new_pw,*retype_pw;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* chk_ssl;
    NSString * vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Change_limit_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    ssl_count = @"0";
    txt_atm_pin.delegate=self;
    txt_atm_card.delegate=self;
    txt_amount.delegate=self;
    vw_down_chck = @"";
    
//    NSLog(@"%@",gblclass.daily_limit);
//    NSLog(@"%@",gblclass.monthly_limit);
    
    lbl_current_daily_limit.text = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,@"0"]; //gblclass.daily_limit;
    lbl_current_monthly_limit.text = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,@"0"]; //gblclass.monthly_limit;
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"load";
        [self SSL_Call];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_submit:(id)sender
{
    
    [txt_amount resignFirstResponder];
    [txt_atm_pin resignFirstResponder];
    [txt_atm_card resignFirstResponder];
    
    
    if ([txt_amount.text isEqualToString:@""])
    {
        [self custom_alert:@"Enter Daily Amount"  :@"0"];
    }
    else if ([txt_amount.text isEqualToString:@"0"])
    {
        [self custom_alert:@"Enter Valid Daily Amount" :@"0"];
    }
    else if([txt_atm_card.text isEqualToString:@""])
    {
        [self custom_alert:@"Enter Monthly Amount" :@"0"];
    }
    else if([txt_atm_card.text isEqualToString:@"0"])
    {
        [self custom_alert:@"Enter Valid Monthly Amount" :@"0"];
    }
//    else if(txt_atm_card.text.length <16)
//    {
//
//        [self custom_alert:@"Enter 16 Digits Debit Card Number" :@"0"];
//
//    }
    else if([txt_atm_pin.text isEqualToString:@""])
    {
        [self custom_alert:@"Enter ATM PIN" :@"0"];
    }
    else
    {
        gblclass.arr_daily_limit = [[NSMutableArray alloc] init];
        
//        [gblclass.arr_daily_limit addObject:txt_amount.text];
//        [gblclass.arr_daily_limit addObject:txt_atm_card.text];
        
       // [gblclass.arr_daily_limit addObject:txt_amount.text];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"submit";
            [self SSL_Call];
        }
    }
    
}


-(void) GenerateOTP:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"CHANGELIMIT",@"Generate OTP",@"0",@"CHANGELIMIT",@"MOB", nil]];
    
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.is_default_acct_no);
    
    gblclass.fetchtitlebranchcode = @"0";
    gblclass.acc_number = @"0";
    //[gblclass.acc_number uppercaseString]
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.fetchtitlebranchcode],
                                [encrypt encrypt_Data:gblclass.acc_number],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:@"0"],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  gblclass.add_bill_type=@"Add_payee_omni";
                  
                  CATransition *transition = [CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];

                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"monthly_limit_otp"];
                  [self presentViewController:vc animated:NO completion:nil];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
         {
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription]  :@"0"];
              
          }];
}


-(void) Check_Max_Limit:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"CHANGELIMIT",@"Generate OTP",@"0",@"CHANGELIMIT",@"MOB", nil]];
    
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.is_default_acct_no);
    
    gblclass.fetchtitlebranchcode = @"0";
    gblclass.acc_number = @"0";
    //[gblclass.acc_number uppercaseString]
    
    NSString *daily = [txt_amount.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSString *monthly = [txt_atm_card.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    gblclass.arr_daily_limit = [[NSMutableArray alloc] init];
    
    [gblclass.arr_daily_limit addObject:daily];
    [gblclass.arr_daily_limit addObject:monthly];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:daily],
                                [encrypt encrypt_Data:monthly],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"DailyLimit",
                                                                   @"MonthlyLimit",
                                                                   @"strSessionId",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager POST:@"CheckMaxLimit" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [self GenerateOTP:@""];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
         {
         [hud hideAnimated:YES];\
         [self custom_alert:[error localizedDescription]  :@"0"];
         
     }];
}



-(void) Get_Limits:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.setting_ver],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"LogBrowserInfo",
                                                                   @"hCode",
                                                                   @"Device_ID",
                                                                   @"Token",   nil]];
    
    [manager POST:@"GetLimits" parameters:dictparam progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
//                  strDailyLimit = "AED 50,000";
//                  strMonthlyLimit = "AED 59,980";
                  
                  NSString* Daily_Limit=[dic objectForKey:@"strDailyLimit"];
                  
                  if (Daily_Limit.length==0 || [Daily_Limit isEqualToString:@"<nil>"] || [Daily_Limit isEqualToString:NULL])
                  {
                      Daily_Limit=@"0";
                      lbl_current_daily_limit.text = Daily_Limit;
                  }
                  else
                  {
                      lbl_current_daily_limit.text = Daily_Limit;
                  }
                  
                  NSString* Monthly_Limit=[dic objectForKey:@"strMonthlyLimit"];
                  
                  if (Monthly_Limit.length==0 || [Monthly_Limit isEqualToString:@"<nil>"] || [Monthly_Limit isEqualToString:NULL])
                  {
                      Monthly_Limit=@"0";
                      lbl_current_monthly_limit.text = Monthly_Limit;
                  }
                  else
                  {
                      lbl_current_monthly_limit.text = Monthly_Limit;
                  }
                  
                  [hud hideAnimated:YES];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [hud hideAnimated:YES];\
         [self custom_alert:[error localizedDescription]  :@"0"];
         
     }];
}



-(void) Change_daily_limit:(NSString *)strIndustry
{
    
    @try {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.signup_relationshipid);
        
        //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"7c4d5f254b9e6edd",txt_pin.text,@"1234",[GlobalStaticClass getPublicIp],@"asdf", nil]
        //                                                          forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
        
        new_pw  = [txt_amount.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        retype_pw  = [txt_atm_card.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        old_pw  = [txt_atm_pin.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        //NSLog(@"%@",gblclass.user_id);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:txt_atm_card.text],
                                    [encrypt encrypt_Data:txt_atm_pin.text],
                                    [encrypt encrypt_Data:[txt_amount.text stringByReplacingOccurrencesOfString:@"," withString:@""]],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strCardno",
                                                                       @"ATMPin",
                                                                       @"Limit",
                                                                       @"strAtmInvalidAttempts",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        
        [manager POST:@"ChangeLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //           NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  //  [arr_que addObject:(NSDictionary *)[dic objectForKey:@"dt2"]];
                  //for(dic in generalarray){
                  
                  responsecode=[dic objectForKey:@"Response"];
                  
                  NSString* exception =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  //[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
                  
                  if ([responsecode isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
                      // gblclass.daily_limit=txt_amount.text;
                      
                      //  [self showAlert:exception:@"Attention"];
                      
                      gblclass.daily_limit = txt_amount.text;
                      lbl_current_daily_limit.text = txt_amount.text;
                      
                      txt_amount.text=@"";
                      txt_atm_pin.text=@"";
                      txt_atm_card.text=@"";
                      
                      [self custom_alert:exception :@"1"];
                      
                      //                    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                    [alert addAction:ok];
                      //                    [self presentViewController:alert animated:YES completion:nil];
                      
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [hud hideAnimated:YES];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return;
                      
                  }
                  else
                  {
                      
                      [hud hideAnimated:YES];
                      [self custom_alert:exception :@"0"];
                      
                      //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:exception preferredStyle:UIAlertControllerStyleAlert];
                      //
                      //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      //                  [alert addAction:ok];
                      //                  [self presentViewController:alert animated:YES completion:nil];
                      
                  }
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[error localizedDescription] :@"0"];
                  
                  //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //              [alert addAction:ok];
                  //              [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window. layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //   BOOL stringIsValid;
    NSInteger MAX_DIGITS=4;
    
    if ([theTextField isEqual:txt_amount])
    {
        MAX_DIGITS=13;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_atm_card])
    {
        MAX_DIGITS=13;
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        [theTextField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
        
        return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_atm_pin])
    {
        MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_amount resignFirstResponder];
    [txt_atm_pin resignFirstResponder];
    [txt_atm_card resignFirstResponder];
    
    vw_down_chck = @"1";
    
    [self keyboardWillHide];
}

#define kOFFSET_FOR_KEYBOARD 45.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
       
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    if ([theTextField isEqual:txt_amount])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_amount.text = [nf stringFromNumber:myNumber];
    }
    else if ([theTextField isEqual:txt_atm_card])
    {
        NSString *textFieldText1 = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        NSNumber *myNumber = [NSNumber numberWithInteger:[textFieldText1 integerValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
//        NSLog(@"NSNumberFormatterDecimalStyle  %@", [nf stringFromNumber:myNumber]);
        
        txt_atm_card.text = [nf stringFromNumber:myNumber];
    }
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:YES completion:nil];
}



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


///////////************************** SSL PINNING START ******************************////////////////



- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"load"])
        {
            [self Get_Limits:@""];
        }
        else if ([chk_ssl isEqualToString:@"submit"])
        {
            [self Check_Max_Limit:@""];
        }
         // [self Check_Max_Limit:@""];
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        [self Get_Limits:@""];
    }
    else if ([chk_ssl isEqualToString:@"submit"])
    {
        [self Check_Max_Limit:@""];
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}

///////////************************** SSL PINNING END ******************************////////////////


@end

