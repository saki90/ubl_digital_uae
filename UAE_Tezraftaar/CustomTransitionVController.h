//
//  CustomTransitionVController.h
//  CustomTransition
//
//  Created by Mehmood on 22/11/2016.
//  Copyright © 2016 M3Tech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface CustomTransitionVController : NSObject<UIViewControllerAnimatedTransitioning>

@property CGRect frameOfSmallRect;
@property CGRect frameOfScreenRect;
@property CGRect center;
@property CGPoint centerPoint;
@property (nonatomic,strong) NSString* type;



@end
