//
//  AddPayee.m
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "FundTransfer.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
//#import "Loadbranches.h"
//#import "LoadIBFTBanks.h"
#import "APIdleManager.h"
#import "Encrypt.h"

//
//#import "DescriptionDetailsVC.h"

#define ACCEPTABLE_CHARECTERS @"0123456789"


@interface FundTransfer () <NSURLConnectionDataDelegate>
{
    //NSMutableArray* bill_service;
    NSMutableArray* bill_type;
    NSMutableArray* companytype;
    NSInteger selectedrowpicker;
    NSMutableArray* ttid,*ttacceskey;
    NSMutableArray* branchname,*branchcode,*acct_format;
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    
    //fet title
    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* responsecode;
    NSString* countrybranch;
    UIAlertController* alert;
    NSString* txttypebillvalue;
    
    NSArray *searchResults;
    NSMutableArray* arr_search,*arr_search_brcode;
    NSArray* split,*split_select;
    NSString* brcode;
     NSString* chck_ibft;
    NSUInteger maxLength,minLength;
    NSString* str_validation;
    NSMutableArray* arr_length;
    NSMutableArray* a;
    NSMutableArray* arr_format;
    NSMutableArray* arr_bank_name;
    NSMutableArray* arr_ibft_relation;
    NSMutableArray* arr_ibft_relation_load;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSMutableArray* arr_choose_type;
    NSNumber* iban_length;
    NSString* choose_type;
    NSString * vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;
@end

@implementation FundTransfer


@synthesize txtbillservice;
@synthesize transitionController;
@synthesize searchResult;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
 //   NSLog(@"%@",@"saki Add payee load");
    
    // Do any additional setup after loading the view.
    // bill_service = [[NSMutableArray alloc]init];
    encrypt = [[Encrypt alloc] init];
    arr_bank_name = [[NSMutableArray alloc]init];
    arr_choose_type = [[NSMutableArray alloc] init];
    bill_type = [[NSMutableArray alloc]init];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    vw_fetch_title.hidden=YES;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    gblclass.arr_re_genrte_OTP_additn=[[NSMutableArray alloc] init];
    arr_format=[[NSMutableArray alloc] init];
    a=[[NSMutableArray alloc] init];
    arr_length=[[NSMutableArray alloc] init];
    arr_search_brcode=[[NSMutableArray alloc] init];
    searchResult=[[NSMutableArray alloc] init];
    arr_search=[[NSMutableArray alloc] init];
    arr_ibft_relation=[[NSMutableArray alloc] init];
    arr_ibft_relation_load=[[NSMutableArray alloc] init];
    
    search.barTintColor=[UIColor colorWithRed:44/255.0 green:128/255.0 blue:197/255.0 alpha:1.0];
    search.tintColor=[UIColor whiteColor];
    //search.placeholder = @"Search";
    self.definesPresentationContext=true;
    search.delegate=self;
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
  //crash  search.searchTextField.textColor = [UIColor whiteColor];
    txt_iban.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_accnt_email_address.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_accnt_nick.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_accnt_title.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_accnt_number.autocorrectionType = UITextAutocorrectionTypeNo;

    
//    [[UITextField appearanceWhenContainedInInstancesOfClasses:[UISearchBar class], nil] setTintColor:[UIColor darkGrayColor]];
    
    vw_down_chck = @"";
    isFiltered = NO;
    vw_IBFT.hidden=YES;
    [self.view endEditing:YES];
    
    lbl_h1_1.hidden = YES;
    txt_accnt_title_2.hidden = YES;
    lbl_h1.hidden = YES;
    txt_accnt_title.hidden = YES;
    img_arrow.hidden = NO;
    choose_type = @"";
    iban_length = @"";
    chck_ibft=@"";
    ssl_count = @"0";
    maxLength=0;
    minLength=0;
    str_validation=@"";
    btn_combo.enabled=YES;
    img_combo.hidden=NO;
    _txtaccountnumber.enabled=YES;
    txt_iban.hidden = YES;
    
    txt_choose_type.delegate = self;
    txt_acct_number_ibft.delegate=self;
    txtbillservice.delegate=self;
    _txtbilltype.delegate=self;
    _txtcompany.delegate=self;
    _txtaccountnumber.delegate=self;
    txt_accnt_email_address.delegate=self;
    txt_accnt_nick.delegate=self;
    txt_iban.delegate=self;
    txt_accnt_title.delegate = self;
    
    _receiverEmail.delegate = self;
    _receiverMobile.delegate = self;
    _receiverCnic.delegate = self;
    _confirmReceiverCnic.delegate = self;
    _receiverName.delegate = self;
    vw_iban.hidden = YES;
    
    _txtaccountnumber.delegate=self;
    
    lbl_acct_numb_brnch.hidden = YES;
    lbl_acct_num_txt_brnch.hidden = YES;
    
    btn_submit.hidden=NO;
    btn_add_record.hidden=YES;
    vw_ibft_relation.hidden=YES;
    
    lbl_acct_number.hidden = YES;
    lbl_acct_num_txt.hidden = YES;
    
    lbl_h1.text = @"Account Title";
    lbl_h2.text = @"Account Number";
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textViewTapped:)];
     tap.delegate = self;
     tap.numberOfTapsRequired = 1;
    [txt_IBFT_formated addGestureRecognizer:tap];
    [txt_iban addGestureRecognizer:tap];
    
    
    arr_choose_type = [[NSMutableArray alloc] init];
    [arr_choose_type addObject:@"IBAN"];
    [arr_choose_type addObject:@"Credit Card"];
    
    if([gblclass.UserType isEqualToString:@"1000"])
    {
        bill_type=[[NSMutableArray alloc] initWithArray:@[@"UBL Branch Account",@"UBL Omni Account",@"Other Bank Account (IBFT)"]];
        
        countrybranch=@"1";
    }
    else
    {
        if([gblclass.UserType isEqualToString:@"4000"])
        {
            countrybranch=@"2";
        }
        else if([gblclass.UserType isEqualToString:@"4096"])
        {
            countrybranch=@"4";
        }
        else if([gblclass.UserType isEqualToString:@"4112"])
        {
            countrybranch=@"10";
        }
        
        //   bill_type=@[@"UBL Branch Account"];
        
        bill_type=[[NSMutableArray alloc] initWithArray:@[@"UBL Branch Account"]];
    }
    
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    
  //bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    self.txtaccountnumber.delegate=self;
    
    //NSLog(@"Base Currency .%@",gblclass.base_currency);
    
    
    
       //gblclass.acct_add_header_type;
    self.txtbilltype.text=@"Add Beneficiary Account";   //[bill_type objectAtIndex:indexPath.row];
    txttypebillvalue = gblclass.acct_add_type;
    
    if ([txttypebillvalue isEqualToString:@"IBAN"])
    {
        txt_iban.keyboardType = UIKeyboardTypeAlphabet;
        gblclass.bill_type = @"Add IBAN";
        //NSLog(@"ither bank ac");
        self.dd3.hidden = false;
        btn_combo.hidden=YES;
        img_dd3.hidden=YES;
        _txtcompany.hidden=YES;
        acc_type =@"IBAN";
//        txt_iban.hidden=NO;
        txt_iban.placeholder=@"Enter IBAN";
        _txtaccountnumber.hidden=YES;
        
        lbl_h1.text = @"Account Title";
        lbl_h2.text = @"Bank Name";
        
    }
    
    if ([txttypebillvalue isEqualToString:@"TRANSFER FUNDS TO CNIC"]) {
        lbl_heading.text = txttypebillvalue;
        self.fundTransferThroughCnicView.hidden = NO;
    }
    else
    {
        
        if ([gblclass.acct_add_type isEqualToString:@"IBAN"])
        {
            lbl_heading.text = [NSString stringWithFormat:@"ADD OTHER BANK ACCOUNT"];
            self.fundTransferThroughCnicView.hidden = YES;
        }
        else
        {
            lbl_heading.text = [NSString stringWithFormat:@"ADD %@",gblclass.acct_add_type.uppercaseString];
            self.fundTransferThroughCnicView.hidden = YES;
        }
       
        
        [self checkinternet];
        if (netAvailable)
        {
           //  NSLog(@"%@",@"saki Add payee load service");
            chk_ssl=@"load";
            [self SSL_Call];
        }
        
    }
    
    ///UBL branch account
    //
    //    if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
    //    {
    //        self.dd3.hidden = false;
    //        //NSLog(@"ubl branch acc");
    //        acc_type =@"UBL";
    //        bankname =@"United Bank Limited";
    //        bankimd=@"588974";
    //
    //        self.txtcompany.placeholder=@"Branch Name";
    //        [self LoadBranches:@""];
    //
    //        img_dd3.hidden=NO;
    //    }
    //    else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
    //    {
    //        self.txtcompany.text = @"Ubl Omni Branch - 1256";
    //        self.dd3.hidden = true;
    //        bankname =@"United Bank Limited";
    //        acc_type =@"OMNI";
    //        bankimd=@"588974";
    //        fetchtitlebranchcode=@"1256";
    //
    //        self.txtcompany.placeholder=@"Branch Name";
    //        // fetchtitlebranchcode=
    //        self.company.hidden = true;
    //        img_dd3.hidden=YES;
    //
    //    }
    //    else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
    //    {
    //        //NSLog(@"ither bank ac");
    //        self.dd3.hidden = false;
    //        acc_type =@"IBFT";
    //        [self LoadIBFTbanks:@""];
    //        [self SSL_Call];
    //        self.txtcompany.placeholder=@"Select Bank";
    //        img_dd3.hidden=NO;
    //    }
    
    
    table_ibft_relation.hidden=YES;
    self.company.hidden=YES;
    self.tableviewcompany.hidden=YES;
    vw_table.hidden=YES;
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    self.fundTransferThroughCnicView.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //        //  [self.timeoutLabel  setText:@"YES"];
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //        storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //        vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //
    //        [self presentViewController:vc animated:YES completion:nil];
    //
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
    
    txtbillservice.text=@"Add Beneficiary Account";
    txtbillservice.hidden=YES;
    
   //  NSLog(@"%@",@"saki Add payee load End");
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (IBAction)btn_ddbillservice:(UIButton *)sender
 {
 self.billtype.hidden=true;
 self.company.hidden=true;
 
 if(self.billserviceview.hidden){
 self.billserviceview.hidden=false;
 bill_service = [[NSMutableArray alloc]init];
 
 bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
 }else{
 
 self.billserviceview.hidden=true;
 bill_service = [[NSMutableArray alloc]init];
 
 bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
 }
 
 }*/

- (IBAction)btn_submitfetchtitle:(UIButton *)sender
{
    
    //NSLog(@"%@",txttypebillvalue);
    [txt_iban resignFirstResponder];
    
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtaccountnumber resignFirstResponder];
    [txt_accnt_email_address resignFirstResponder];
    [txt_accnt_nick resignFirstResponder];
    [txt_acct_number_ibft resignFirstResponder];
    
    if ([txttypebillvalue isEqualToString:@"IBAN"])
    {
        if ([txt_iban.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            
      //      NSLog(@"%@",branchname);
      //      NSLog(@"%@",arr_length);
            if ([txt_bank_choose.text isEqualToString:@""])
            {
                [self custom_alert:@"Please select bank name" :@"0"];
            }
            else if ([txt_choose_type.text isEqualToString:@""])
            {
                [self custom_alert:@"Please choose type" :@"0"];
            }
            else if ([txt_choose_type.text isEqualToString:@"IBAN"])
            {
                [self custom_alert:@"Please enter IBAN" :@"0"];
            }
            else
            {
                [self custom_alert:@"Please enter Credit Card" :@"0"];
            }
            
            return;
        }
//        else if (txt_iban.text.length<[iban_length integerValue])
//        {
           else if ([txt_choose_type.text isEqualToString:@"IBAN"])
            {
                
                if (txt_iban.text.length<[iban_length integerValue])
                {
                    [self custom_alert:@"IBAN length should be 23 characters" :@"0"]; //Please enter valid IBAN
                }
                else
                {
                    gblclass.acc_type=@"IBAN";
                    [self checkinternet];
                    if (netAvailable)
                    {
                        chk_ssl=@"review";
                        [self SSL_Call];
                    }
                }
            }
           else if ([txt_choose_type.text isEqualToString:@"Credit Card"])
            {
      //          NSLog(@"%lu",(unsigned long)txt_iban.text.length);
                if (txt_iban.text.length <16)
                {
                    [self custom_alert:@"Incomplete card number, please enter 16 digits card number" :@"0"];
//                    Please enter valid Credit card number
                }
                else
                {
                    gblclass.acc_type=@"CreditCard";
                    [self checkinternet];
                    if (netAvailable)
                    {
                        chk_ssl=@"review";
                        [self SSL_Call];
                    }
                }
            }
        
        
            
//            else
//            {
//                [self custom_alert:@"Please enter valid Credit card number" :@"0"];
//            }
           
      //      return;
     //   }
//        else
//        {
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl=@"review";
//                [self SSL_Call];
//            }
//        }
        
    }
    else if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
    {
        if ([_txtcompany.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Select Branch Name" :@"0"];
            return;
        }
        else if ([_txtaccountnumber.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter Account Number" :@"0"];
            return;
        }
        else if ([_txtaccountnumber.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter account number" :@"0"];
            return;
        }
        else if (_txtaccountnumber.text.length<minLength)
        {
            [hud hideAnimated:YES];
            [self custom_alert:str_validation :@"0"];
            return;
        }
        else
        {
            
            gblclass.acct_nick = txt_accnt_nick.text;
            gblclass.acctitle = txt_accnt_title_2.text;
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"review";
                [self SSL_Call];
            }
        }
        
    }
    else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
    {
        
        if (_txtaccountnumber.text.length<maxLength)
        {
            [hud hideAnimated:YES];
            [self custom_alert:str_validation :@"0"];
            return;
        }
        else
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"review";
                [self SSL_Call];
            }
        }
        
    }
    else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
    {
        
        if ([_txtcompany.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please select bank" :@"0"];
            return;
        }
        else if ([txt_acct_number_ibft.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter account number" :@"0"];
            return;
        }
        
        if (txt_acct_number_ibft.text.length<minLength)
        {
            [hud hideAnimated:YES];
            [self custom_alert:str_validation :@"0"];
            return;
        }
        
        else
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"review";
                self.txtaccountnumber.text=txt_acct_number_ibft.text;
                [self SSL_Call];
            }
        }
        
        
    } else if ([txttypebillvalue isEqualToString:@"TRANSFER FUNDS TO CNIC"]) {
        
//        receiverName;
//        receiverCnic;
//        confirmReceiverCnic;
//        receiverMobile;
//        receiverEmail;
        
//        NSLog(@"%@",[_receiverMobile.text substringFromIndex:0]);
//        NSLog(@"%@",[_receiverMobile.text substringWithRange:NSMakeRange(0, 2)]);
        
        
     //   NSString *string1 = [string substringWithRange:NSMakeRange(0, 32)];
        
        if ([_receiverName.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Invalid name" :@"0"];
        }
        else if ([_receiverCnic.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please enter CNIC Number" :@"0"];
        }
        else if (_receiverCnic.text.length <13)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Invalid CNIC Number" :@"0"];
        }
        else if ([_confirmReceiverCnic.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please enter confirm CNIC Number" :@"0"];
        }
        else if (_confirmReceiverCnic.text.length <13)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Invalid CNIC Number" :@"0"];
        }
        else if (![_receiverCnic.text isEqualToString:_confirmReceiverCnic.text])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"CNIC Mismatched" :@"0"];
        }
        else if ([_receiverMobile.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please enter mobile number" :@"0"];
        }
        else if (_receiverMobile.text.length <11)
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Invalid mobile number" :@"0"];
        }
        else if ([[_receiverMobile.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"02"])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Invalid mobile number" :@"0"];
        }
        else if (![_receiverEmail.text isEqualToString:@""])
        {
            if (![self validateEmail:[_receiverEmail text]])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Invalid email address" :@"0"];
            }
//            [hud hideAnimated:YES];
//            [self custom_alert:@"Please enter receiver email" :@"0"];
        }
//        else if (![self validateEmail:[_receiverEmail text]])
//        {
//            [hud hideAnimated:YES];
//            [self custom_alert:@"Please enter valid email" :@"0"];
//        }
        
     
        
        
        [self slideRight];
        gblclass.descriptionDetailModel = [[NSMutableArray alloc] init];
        [gblclass.descriptionDetailModel addObjectsFromArray:@[self.receiverName.text, self.receiverCnic.text, self.receiverMobile.text, self.receiverEmail.text]];
        
        vc = [storyboard instantiateViewControllerWithIdentifier:@"descriptionDetailsVC"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
    //    if([self.txtbilltype.text length]==0 || [self.txtcompany.text length]==0 || [self.txtaccountnumber.text length]==0)
    //    {
    //        [self showAlert:@"All field are mandtory" :@"Attention" ];
    //    }
    //    else
    //    {
    //        [self checkinternet];
    //        if (netAvailable)
    //        {
    //            [self AddAccountTitleFetch:@""];
    //        }
    //
    //    }
}


- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (IBAction)btn_dd_company:(UIButton *)sender
{
    // self.billserviceview.hidden=true;
    [self.view endEditing:YES];
    search.hidden=NO;
    
    if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
    {
        lbl_table_header.text=@"Select Bank";
        
    } else if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
    {
        lbl_table_header.text=@"Select Branch";
    } else {
        lbl_table_header.text=@"Select Beneficiary";
    }
    
    if([txttypebillvalue length]==0)
    {
        [hud hideAnimated:YES];
        
        [self custom_alert:@"Please select your Account type" :@"0"];
      //[self showAlert:@"Please select your Account type" :@"Attention" ];
    }
    else
    {
        self.billtype.hidden=YES;
        if(self.tableviewcompany.isHidden==YES)
        {
            self.company.hidden=NO;
            self.tableviewcompany.hidden=NO;
            [self.tableviewcompany reloadData];
            table_choose_type.hidden =YES;
            vw_table.hidden=NO;
            isFiltered = NO;
        }
        else
        {
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
            vw_table.hidden=YES;
        }
    }
}


- (IBAction)btn_dd_billtype:(UIButton *)sender
{
    self.company.hidden=true;
    if(self.billtype.hidden)
    {
        self.billtype.hidden=false;
    }
    else
    {
        self.billtype.hidden=true;
    }
    
}

//UISearchDisplayController
- (void)searchDisplayController:(UISearchController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    
    //tableView.backgroundColor =[UIColor redColor];
    //[UIColor colorWithPatternImage:[UIImage imageNamed:@"default.png"]];
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSAttributedString *attributedStr = [[NSAttributedString alloc] initWithData:[txt dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    NSString *s = attributedStr.string;
    return s;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.billtype.hidden = true;
    self.company.hidden = true;
    self.tableviewcompany.hidden = YES;
    
    [tableView cellForRowAtIndexPath:indexPath].selected = false;
    
 //   NSLog(@"%@",tableView);
    //if (tableView == self.searchDisplayController.searchResultsTableView)
    if([searchResult count]>0)    //(isFiltered)
    {
        
        if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
          //saki   [_tableviewcompany reloadData];
            split_select = [[NSArray alloc] init];
      //      NSLog(@"%@", searchResult);
            //NSLog(@"%@",[arr_length objectAtIndex:indexPath.row]);
            if ([searchResult count] >0)
            {
                iban_length = [arr_length objectAtIndex:indexPath.row];
                split_select = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            }
            else
            {
                iban_length = [arr_length objectAtIndex:indexPath.row];
                split_select = [[branchname objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            }
            
            
            txt_bank_choose.text = [split_select objectAtIndex:0]; //[searchResult objectAtIndex:indexPath.row];  //branchname
            gblclass.bankname = [split_select objectAtIndex:0];
            fetchtitlebranchcode=[split_select objectAtIndex:1];// [branchcode objectAtIndex:indexPath.row];
            gblclass.bankimd = fetchtitlebranchcode;
            txt_format.text =  [self stringByStrippingHTML:[acct_format objectAtIndex:indexPath.row]];
            
            vw_IBFT.hidden = YES;
            txt_choose_type.text = @"";
            txt_iban.text = @"";
            gblclass.ibft_relation = @"";
            
//            vw_IBFT.hidden = YES;
//            txt_choose_type.text = @"";
//            txt_iban.text = @"";
            txt_iban.hidden = YES;
            
         //saki   isFiltered = NO;
            vw_table.hidden = YES;
            self.company.hidden = YES;
            self.tableviewcompany.hidden = YES;
            _txtaccountnumber.hidden = YES; //saki 5 april 2019
            [_searchController resignFirstResponder];
            
//            NSLog(@"%@",txt_bank_choose.text);
//            NSLog(@"%@",fetchtitlebranchcode);
//            NSLog(@"%@",txt_format.text);
//            NSLog(@"%@",_txtaccountnumber.text);
//            [searchResult removeAllObjects];
        }
        
        if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
        {
            self.txtcompany.text = [searchResult objectAtIndex:indexPath.row];
            NSString *branch_code = [searchResult objectAtIndex:indexPath.row];
            fetchtitlebranchcode = [branch_code substringToIndex:4];
            gblclass.ibft_relation = @"";
            
            // self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            // fetchtitlebranchcode=[branchcode objectAtIndex:indexPath.row];
             
         //saki   isFiltered = NO;
            _txtaccountnumber.text = @"";
            _txtaccountnumber.hidden = NO;
            vw_table.hidden=YES;
            self.company.hidden=YES;
            self.tableviewcompany.hidden = YES;
//            [searchResult removeAllObjects];
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
        {
            fetchtitlebranchcode=@"1256";
            gblclass.ibft_relation=@"";
        }
        else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
        {
            //  self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            //   bankimd=[branchcode objectAtIndex:indexPath.row];
            //  bankname=[branchname objectAtIndex:indexPath.row];
            
            _txtaccountnumber.keyboardType = UIKeyboardTypeAlphabet;
            
            split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            bankimd=[split objectAtIndex:3];
            bankname=[split objectAtIndex:4];
            self.txtcompany.text =bankname;
            fetchtitlebranchcode=@"";
            vw_table.hidden=YES;
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
            
            // split = [[arr_length objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            minLength=[[split objectAtIndex:0] integerValue];
            maxLength=[[split objectAtIndex:1] integerValue];
            //            txt_IBFT_formated.text=[split objectAtIndex:2];
            
            NSAttributedString *htmlToString = [[NSAttributedString alloc] initWithData:[[split objectAtIndex:2] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
            
            txt_IBFT_formated.text=[htmlToString string];
            lbl_formatted.text=[NSString stringWithFormat:@"Please enter %@ account number in the following format:",bankname];
            str_validation=[NSString stringWithFormat:@"Account number must be %lu - %lu",(unsigned long)minLength,(unsigned long)maxLength];
            
            vw_IBFT.hidden=NO;
            _txtaccountnumber.hidden=YES;
            
        }
        
        vw_down_chck = @"1";
        search.hidden=YES;
  //      self.searchDisplayController.active=false;
        search.text = @"";
        [search resignFirstResponder];
        search.hidden=YES;
        
    }
    
    if(tableView==_tableviewcompany && [searchResult count]==0) //&& isFiltered == NO
    {
    //saki    [_tableviewcompany reloadData];
    //saki    [table_choose_type reloadData];
        
        if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
            split_select = [[NSArray alloc] init];
            //NSLog(@"%@",[arr_length objectAtIndex:indexPath.row]);
            split_select = [[branchname objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            iban_length = [arr_length objectAtIndex:indexPath.row];
            txt_bank_choose.text = [split_select objectAtIndex:0]; //[branchname objectAtIndex:indexPath.row];
            gblclass.bankname = [split_select objectAtIndex:0];
            fetchtitlebranchcode = [split_select objectAtIndex:1]; //[branchcode objectAtIndex:indexPath.row];
            gblclass.bankimd = fetchtitlebranchcode;//saki 24 june 2019
            txt_format.text =  [self stringByStrippingHTML:[acct_format objectAtIndex:indexPath.row]];
            
            vw_IBFT.hidden = YES;
            txt_choose_type.text = @"";
            txt_iban.text = @"";
            txt_iban.hidden = YES;
            
            gblclass.ibft_relation=@"";
            vw_table.hidden=YES;
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
         //_txtaccountnumber.hidden=NO; //saki 4 aprl 2019
            
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
        {
            self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            fetchtitlebranchcode=[branchcode objectAtIndex:indexPath.row];
            gblclass.ibft_relation=@"";
            
            _txtaccountnumber.text = @"";
            vw_table.hidden=YES;
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
            _txtaccountnumber.hidden=NO;
            
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
        {
            fetchtitlebranchcode=@"1256";
            gblclass.ibft_relation=@"";
            _txtaccountnumber.hidden=NO;
        }
        else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
        {
            self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            bankimd=[branchcode objectAtIndex:indexPath.row];
            bankname=[branchname objectAtIndex:indexPath.row];
            fetchtitlebranchcode=@"";
            vw_table.hidden=YES;
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
            split = [[arr_length objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            minLength=[[split objectAtIndex:0] integerValue];
            maxLength=[[split objectAtIndex:1] integerValue];
            //            txt_IBFT_formated.text=[split objectAtIndex:2];
            NSAttributedString *htmlToString = [[NSAttributedString alloc] initWithData:[[split objectAtIndex:2] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
            
            txt_IBFT_formated.text=[htmlToString string];
            
            lbl_formatted.text=[NSString stringWithFormat:@"Please enter %@ account number in the following format:",bankname];
            
            str_validation=[NSString stringWithFormat:@"Account number must be %lu - %lu",(unsigned long)minLength,(unsigned long)maxLength];
            
            vw_IBFT.hidden=NO;
            _txtaccountnumber.hidden=YES;
            
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
    }
    else if (tableView==table_choose_type)
    {
        [table_choose_type reloadData];
        split = [[arr_choose_type objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
            txt_choose_type.text = [split objectAtIndex:0];
            
            if ([[split objectAtIndex:0] isEqualToString:@"IBAN"])
            {
                txt_iban.keyboardType = UIKeyboardTypeDefault;
                txt_iban.hidden = NO;
                txt_iban.placeholder = @"Enter IBAN";
                txt_iban.text = @"";
                vw_IBFT.hidden = NO;
                choose_type = @"2";
            }
            else
            {
//                txt_iban.keyboardType = UIKeyboardTypeNumberPad;
                txt_iban.hidden = NO;
                txt_iban.placeholder = @"Enter Credit Card";
                txt_iban.text = @"";
                vw_IBFT.hidden = YES;
                choose_type = @"3";
            }
            
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        vw_table.hidden=YES;
        table_choose_type.hidden=YES;
    }
    else if(tableView==table_ibft_relation)
    {
        [table_ibft_relation reloadData];
        split = [[arr_ibft_relation objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
        
            txt_ibft_relation.text=[split objectAtIndex:0];
            gblclass.ibft_relation=[split objectAtIndex:1];
        
         
        vw_table.hidden=YES;
        table_ibft_relation.hidden=YES;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
    }
    
   // vw_down_chck = @"1";
    isFiltered = NO;
    [search resignFirstResponder];
    [searchResult removeAllObjects];
}


 - (void)textViewTapped:(UITapGestureRecognizer *)tap {
     //DO SOMTHING
     
//     NSLog(@"TAPPP");
//     NSLog(@"%@",tap);
     vw_down_chck = @"1";
 }

 #pragma mark - Gesture recognizer delegate

 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
     return YES;
 }


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    if(tableView == _searchController.searchResultsUpdater)
//    if (tableView == !self.searchController.searchResultsUpdater.self)
//    {
//        return [searchResult count];
//    }
    
    if(isFiltered)
    {
        //searchResult   arrsearchresult
        return [searchResult count];
    }
    else if(tableView== _tableviewbilltype)
    {
        if([bill_type count]==0)
        {
            return 0;}
        else
        {
            return [bill_type count];
        }
    }
    else if(tableView== _tableviewcompany)
    {
        if([branchname count]==0)// companytype
        {
            return 0;
        }
        else
        {
            return [branchname  count];
        }
    }
    else if (tableView==table_ibft_relation)
    {
        return [arr_ibft_relation count];
    }
    else if (tableView==table_choose_type)
    {
        return [arr_choose_type count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    if (tableView == self.searchDisplayController.searchResultsTableView)
//    {
//        if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
//        {
//            split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
//            cell.textLabel.text=[split objectAtIndex:4];  //4
//            cell.textLabel.font=[UIFont systemFontOfSize:12];
//        }
//        else
//        {
//            split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
//            cell.textLabel.text=[split objectAtIndex:0];
//            cell.textLabel.font=[UIFont systemFontOfSize:12];
//        }
//
//    }
    
   
    
    if(isFiltered)
    {
       // searchResult  arrsearchname
        if ([searchResult count]>0)
        {
             split = [[searchResult objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        }
        else
        {
             split = [[branchname objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        }
       
//        cell.textLabel.text=[split objectAtIndex:0];  //4
//        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
         UILabel* label=(UILabel*)[cell viewWithTag:3];
        label.text=[split objectAtIndex:0];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
        //cell.lblFullname.text = [arrsearchname objectAtIndex:indexPath.row];
    }
    else if(tableView==_tableviewbilltype)
    {
        
        UILabel* label=(UILabel*)[cell viewWithTag:2];
        label.text=[bill_type objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if(tableView==_tableviewcompany && isFiltered == NO)
    {
        
        UILabel* label=(UILabel*)[cell viewWithTag:3];
       // NSDictionary *dic = [companytype objectAtIndex:indexPath.row];
        //        if([self.txtbilltype.text isEqualToString:@"Other Bank Account (IBFT)"]){
        //            [branch name addObject:[dic objectForKey:@"bank_name"]];
        //            [branchcode addObject:[dic objectForKey:@"bank_imd"]];
        //        }
        //        else{
        //            [branchname addObject:[dic objectForKey:@"brdetail"]];
        //            [branchcode addObject:[dic objectForKey:@"brcode"]];
        //        }
        
        split = [[branchname objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        //[branchname objectAtIndex:indexPath.row];
        label.text=[split objectAtIndex:0];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if (tableView==table_ibft_relation)
    {
        split = [[arr_ibft_relation objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        UILabel* label=(UILabel*)[cell viewWithTag:3];
        label.text=[split objectAtIndex:0];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        //        cell.textLabel.text=[split objectAtIndex:0];
        //        cell.textColor=[UIColor whiteColor];
        //        cell.textLabel.font=[UIFont systemFontOfSize:12];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if (tableView==table_choose_type)
    {
        
        split = [[arr_choose_type objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
        UILabel* label=(UILabel*)[cell viewWithTag:3];
        label.text=[split objectAtIndex:0];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    
     table_ibft_relation.separatorStyle = UITableViewCellSeparatorStyleNone;
     table_choose_type.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableviewbilltype.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableviewcompany.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isFiltered = YES;
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
 
            if(searchText.length==0)
            {
                isFiltered=NO;
            }

            else
            {
                isFiltered=YES;
                arrsearchresult = [[NSMutableArray alloc]init];
               // arrsearchdate=[[NSMutableArray alloc]init];
                arrsearchname = [[NSMutableArray alloc]init];
                searchResult = [[NSMutableArray alloc] init];
                
                for ( NSString * str in branchname)
                {
                    NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];

                    NSInteger index;
                    if(stringRange.location != NSNotFound)
                    {
                     //   [arrsearchresult addObject:str];
                        index = [arrfullname indexOfObject:str];
                       // [arrsearchdate addObject:[arrleadCreatedDate objectAtIndex:index]];
                        [searchResult addObject:str];
                        //arrsearchname
                    }
                }
            }

    [self.tableviewcompany reloadData];
}

//-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//    [self.tableviewcompany resignFirstResponder];
//}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    isFiltered=NO;
    [self.tableviewcompany reloadData];

}

//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
//{
//    [searchBar resignFirstResponder];
//}

 
 
//#pragma mark - Search Results Updater
// 
//- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
//    
//    // filter the search
//    self.searchResult = nil;
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains [cd] %@", self.searchController.searchBar.text];
//    //searchResult = [branchname filteredArrayUsingPredicate:predicate];
//    searchResult = [NSMutableArray arrayWithArray: [branchname filteredArrayUsingPredicate:predicate]];
//    
//    // just checking if this works
//    NSLog(@"Array contains: %@", [self.searchResult description]);
//}

//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
//{
//    [searchResult removeAllObjects];
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
//
//    searchResult = [NSMutableArray arrayWithArray: [branchname filteredArrayUsingPredicate:resultPredicate]];
//    //NSLog(@"Search ::  %@",searchResult);
//}
//
////UISearchDisplayController
//-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
//
//    return YES;
//}







//-(void) LoadUtilityBilla:(NSString *)strIndustry{
//
//    [hud showAnimated:YES];
//    [self.view addSubview:hud];
//   [hud showAnimated:YES];
//
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
//                                                                                   ]];//baseURL];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    //680801
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"539152",@"12345",[GlobalStaticClass getPublicIp],@"OB", nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"strAccessKey", nil]];
//
//    //NSLog(@"%@",dictparam);
//
//    [manager POST:@"LoadUtilityCompanies" parameters:dictparam
//
//
//
//          success:^(NSURLSessionDataTask *task, id responseObject) {
//              NSError *error;
//              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
////              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
////              gblclass.actsummaryarr =
//              companytype = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table"];
//              //ttid=
//              //NSLog(@"Done");
//              //[self parsearray];

//              [hud hideAnimated:YES];
//
//          }
//
//          failure:^(NSURLSessionDataTask *task, NSError *error) {
//              // [mine myfaildata];
//
//          }];
//}


-(void) Get_IBFT_BankName:(NSString *)strIndustry{
    
    
    
    companytype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    acct_format = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //GetIBFTBankName
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data: gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.Udid], nil]

                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"Device_ID", nil]];
    
    //LoadBranches
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                               [NSArray arrayWithObjects:[encrypt encrypt_Data:countrybranch],
//                                [encrypt encrypt_Data:gblclass.UserType],
//                                [encrypt encrypt_Data:gblclass.user_id],
//                                [encrypt encrypt_Data:gblclass.M3sessionid],
//                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                [encrypt encrypt_Data:gblclass.Udid],
//                                [encrypt encrypt_Data:gblclass.token], nil]
//
//                                                          forKeys:[NSArray arrayWithObjects:@"strCountry",
//                                                                   @"StrUserType",
//                                                                   @"strUserId",
//                                                                   @"strSessionId",
//                                                                   @"IP",
//                                                                   @"Device_ID",
//                                                                   @"Token", nil]];
    
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    //LoadBranches  GetIBFTBankName
    [manager POST:@"GetIBFTBankName" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                 
              if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
              {
                  companytype = [dic objectForKey:@"dtIBFTBankList"];
                  
                  for(int i=0;i<[companytype count];i++)
                  {
                      NSDictionary *dict = [companytype objectAtIndex:i];
                      if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
                      {
                          [branchname addObject:[dict objectForKey:@"bank_name"]];
                          [branchcode addObject:[dict objectForKey:@"bank_imd"]];
                      }
                      else
                      {
                          NSString* branch_name = [NSString stringWithFormat:@"%@|%@",[dict objectForKey:@"BANK_NAME"],[dict objectForKey:@"BANK_IMD"]];
                           [branchname addObject:branch_name];
                         // [branchname addObject:[dict objectForKey:@"BANK_NAME"]];
                          //[branchcode addObject:[dict objectForKey:@"BANK_IMD"]];
                          [acct_format addObject:[dict objectForKey:@"ACCOUNT_FORMAT_TEXT"]];
                      }
                  }
                  
                  arr_length=[[NSMutableArray alloc] init];
                  for (dic in companytype)
                  {
                      //NSLog(@"%@",[dic objectForKey:@"brdetail"]);
                      
//                      NSString*brdetail =[dic objectForKey:@"MIN_ACC_NUMBER_LEN"];
//
//                      [a addObject:brdetail];
                      
//                      NSString*brcode =[dic objectForKey:@"brcode"];
//                      [a addObject:brcode];
                      
//                      NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
//                      [arr_search addObject:bbb];
                      
                      
                      [arr_length addObject:[dic objectForKey:@"MIN_ACC_NUMBER_LEN"]];
                     // [arr_search_brcode addObject:[dic objectForKey:@"brcode"]];
                      
                  }
                   
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              //NSLog(@"Done load branch");
              //[self parsearray];
              [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}

-(void) Load_Branches:(NSString *)strIndustry
{
    
    companytype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //GetIBFTBankName
//    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
//                                [encrypt encrypt_Data: gblclass.M3sessionid],
//                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                [encrypt encrypt_Data:gblclass.Udid], nil]
//
//                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
//                                                                   @"strSessionId",
//                                                                   @"IP",
//                                                                   @"Device_ID", nil]];
    
    //LoadBranches
      //  countrybranch = @"1";
//    fttitle
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:countrybranch],
                                    [encrypt encrypt_Data:gblclass.UserType], //gblclass.UserType
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
    
                                                              forKeys:[NSArray arrayWithObjects:@"strCountry",
                                                                       @"StrUserType",
                                                                       @"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
    
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    //LoadBranches  GetIBFTBankName
    [manager POST:@"LoadBranches" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  companytype = [[dic objectForKey:@"dtUBLBankList"] objectForKey:@"Table1"];
                  
                  for(int i=0;i<[companytype count];i++)
                  {
                      NSDictionary *dict = [companytype objectAtIndex:i];
                      if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
                      {
                          [branchname addObject:[dict objectForKey:@"bank_name"]];
                          [branchcode addObject:[dict objectForKey:@"bank_imd"]];
                      }
                      else
                      {
                          [branchname addObject:[dict objectForKey:@"brdetail"]];
                          [branchcode addObject:[dict objectForKey:@"brcode"]];
                      }
                      
                  }
                  
//                  arr_length=[[NSMutableArray alloc] init];
//                  for (dic in companytype)
//                  {
//                      //NSLog(@"%@",[dic objectForKey:@"brdetail"]);
//                      
//                      //                      NSString*brdetail =[dic objectForKey:@"MIN_ACC_NUMBER_LEN"];
//                      //
//                      //                      [a addObject:brdetail];
//                      
//                      //                      NSString*brcode =[dic objectForKey:@"brcode"];
//                      //                      [a addObject:brcode];
//                      
//                      //                      NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
//                      //                      [arr_search addObject:bbb];
//                      
//                      
//                      [arr_length addObject:[dic objectForKey:@"MIN_ACC_NUMBER_LEN"]];
//                      // [arr_search_brcode addObject:[dic objectForKey:@"brcode"]];
//                      
//                  }
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
               
              //NSLog(@"Done load branch");
              //[self parsearray];
              [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}



-(void) Verify_IBAN_CreditCard:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
 //   NSLog(@"%@",gblclass.bankimd);
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], //2 iban / 3 credit card.
                                [encrypt encrypt_Data:choose_type],
                                [encrypt encrypt_Data:@"2"],
                                [encrypt encrypt_Data:fetchtitlebranchcode],
                                [encrypt encrypt_Data:[txt_iban.text uppercaseString]], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"AccountType",
                                                                   @"country",
                                                                   @"BankId",
                                                                   @"Account",  nil]];

    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"VerifyIBANCreditCard" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSString* response = [dic objectForKey:@"Response"];
              NSString* msg = [dic objectForKey:@"strReturnMessage"];
              
              if (response == (NSString *)[NSNull null])
              {
                  response = @"";
              }
              
              if (msg == (NSString *)[NSNull null])
              {
                  msg = @"";
              }
              
              if ([response isEqualToString:@"1"])
              {
                  [self Get_IBAN_Title:@""];
              }
              else if ([response isEqualToString:@"-78"] || [response isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:msg :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:msg :@"0"];
              }
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
          }];
}


-(void) Get_IBAN_Title:(NSString *)strIndustry{
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:[txt_iban.text uppercaseString]],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],  nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccountNumber",
                                                                   @"Device_ID",
                                                                   @"Token",  nil]];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"getIBANTitle" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"1"])
              {
                  
                  btn_bank_choose.enabled = NO;
//                  btn_choose_type.enabled = NO;
                  
                  vw_IBFT.hidden = YES;
                  //NSLog(@"Done IBFT load branch");
                  
           //       NSLog(@"%@", gblclass.bankimd);
           //       NSLog(@"%@", gblclass.bankname);
                  
                //saki 22 march 2019  gblclass.acc_type=@"IBAN"; //acc_type
//saki 2 dec 2019  gblclass.bankname = [dic objectForKey:@"strlblBank"];
//                  gblclass.bankimd=@"0"; //saki 24 2019
                  gblclass.fetchtitlebranchcode = @""; //fetchtitlebranchcode //saki 24 2019
                  gblclass.acc_number = txt_iban.text;
                  gblclass.acctitle = txt_accnt_nick.text; //[dic objectForKey:@"strlblAccountTitle"];
                  
                  
//                  if ([gblclass.acct_nick isEqualToString:@""] || gblclass.acct_nick.length ==0)
//                  {
//                      txt_accnt_nick.enabled = YES;
//                  }
//                  else
//                  {
//                      txt_accnt_nick.enabled = NO;
//                  }
//
//                  if ([gblclass.acctitle isEqualToString:@""] || gblclass.acctitle.length ==0)
//                  {
//                      txt_accnt_title.enabled = YES;
//                  }
//                  else
//                  {
//                      txt_accnt_title.enabled = NO;
//                  }
                  
                  vw_fetch_title.hidden=NO;
                  btn_combo.enabled=NO;
                  img_combo.hidden=YES;
                  txt_iban.enabled=NO;
                  
                  txt_iban.hidden = YES;
                  lbl_acct_number.hidden = NO;
                  lbl_acct_num_txt.hidden = NO;
                  lbl_acct_num_txt.text = txt_iban.text;
                  
                  
                  btn_choose_type.hidden = YES;
                  txt_choose_type.hidden = YES;
                  
                  lbl_acct_num_txt_brnch.hidden = YES;
                  lbl_acct_numb_brnch.hidden = YES;
                  img_arrow.hidden = YES;
                  
                  lbl_h1.hidden = NO;
                  txt_accnt_title.hidden = NO;
                  
                  txt_accnt_number.text = [dic objectForKey:@"strlblBank"];
                  txt_accnt_title.text = [dic objectForKey:@"strlblAccountTitle"];
                  txt_accnt_nick.text = [dic objectForKey:@"strtxtAccountNick"];
                  
                  
                  if ([txt_accnt_nick.text isEqualToString:@""] || txt_accnt_nick.text.length ==0)
                  {
                      txt_accnt_nick.enabled = YES;
                  }
                  else
                  {
                      txt_accnt_nick.enabled = NO;
                  }
                  
                  if ([txt_accnt_title.text isEqualToString:@""] || txt_accnt_title.text.length ==0)
                  {
                      txt_accnt_title.enabled = YES;
                  }
                  else
                  {
                      txt_accnt_title.enabled = NO;
                  }
                  
                  
                  btn_cancel.hidden=YES;
                  btn_submit.hidden=YES;
                  btn_add_record.hidden=NO;
                  
                  lbl_h1.text = @"Account Title";
                  lbl_h2.text = @"Bank Name";
                  
                  [txt_iban setBackgroundColor:[UIColor clearColor]];
                  [txt_iban setBorderStyle:UITextBorderStyleNone];
                  NSString *ibft_relation = [dic objectForKey:@"outdsIBFTRelation"];
                  if(ibft_relation == (NSString *)[NSNull null])
                  {
                      chck_ibft = @"0";
                      vw_ibft_relation.hidden=YES;
                  }
                  else
                  {
                      chck_ibft = @"1";
                      vw_ibft_relation.hidden=NO;
                  }
 
                  arr_ibft_relation = [[NSMutableArray alloc] init];
                  
                  if ([chck_ibft isEqualToString:@"1"])
                  {
 
                      arr_ibft_relation_load = [(NSDictionary *)[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
 
                      for(dic in arr_ibft_relation_load)
                      {
                          
                          NSString* IBFT_RELATION_DESC=[dic objectForKey:@"IBFT_RELATION_DESC"];
                          if ([IBFT_RELATION_DESC isEqual:@""])
                          {
                              IBFT_RELATION_DESC=@"N/A";
                              [a addObject:IBFT_RELATION_DESC];
                          }
                          else
                          {
                              [a addObject:IBFT_RELATION_DESC];
                          }
                          
                          NSString* IBFT_RELATION_ID=[dic objectForKey:@"IBFT_RELATION_ID"];
                          if ([IBFT_RELATION_ID isEqual:@""])
                          {
                              IBFT_RELATION_ID=@"N/A";
                              [a addObject:IBFT_RELATION_ID];
                          }
                          else
                          {
                              [a addObject:IBFT_RELATION_ID];
                          }
                          
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [arr_ibft_relation addObject:bbb];
                          
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                      }
                      
                      [table_ibft_relation reloadData];
                      
                  }
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES]; 
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              
              //NSLog(@"Done load branch");
              //[self parsearray];
              [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry" :@"0"];
          }];
}


-(void) LoadIBFTbanks:(NSString *)strIndustry{
    
    //    [hud showAnimated:YES];
    //    [self.view addSubview:hud];
    //   [hud showAnimated:YES];
    
    companytype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"LoadIBFTBanks" parameters:nil progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  companytype = [[dic objectForKey:@"dtIBFTBankList"] objectForKey:@"Table1"];
                  for(int i=0;i<[companytype count];i++)
                  {
                      
                      NSDictionary *dict = [companytype objectAtIndex:i];
                      if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
                      {
                          [branchname addObject:[dict objectForKey:@"bank_name"]];
                          [branchcode addObject:[dict objectForKey:@"bank_imd"]];
                          
                          
                          NSString* bank_imd=[dict objectForKey:@"bank_imd"];
                          if ([bank_imd isEqualToString:@""])
                          {
                              
                              [a addObject:@"N/A"];
                          }
                          else
                          {
                              [a addObject:bank_imd];
                          }
                          
                          NSString* bank_name=[dict objectForKey:@"bank_name"];
                          if ([bank_name isEqualToString:@""])
                          {
                              
                              [a addObject:@"N/A"];
                          }
                          else
                          {
                              [a addObject:bank_name];
                          }
                          
                          
                          NSString *bbb1 = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                          [arr_bank_name addObject:bbb1];
                          [a removeAllObjects];
                          
                      }
                      else
                      {
                          [branchname addObject:[dict objectForKey:@"brdetail"]];
                          [branchcode addObject:[dict objectForKey:@"brcode"]];
                      }
                      
                  }
                  
                  
                  for (dic in companytype)
                  {
                      
                      NSString* min=[dic objectForKey:@"minaccountNumberLength"];
                      if ([min isEqualToString:@""])
                      {
                          
                          [a addObject:@"0"];
                      }
                      else
                      {
                          [a addObject:min];
                      }
                      
                      NSString* max=[dic objectForKey:@"accountNumberLength"];
                      if ([max isEqualToString:@""])
                      {
                          
                          [a addObject:@"0"];
                      }
                      else
                      {
                          [a addObject:max];
                      }
                      
                      NSString* format=[dic objectForKey:@"format"];
                      if ([format isEqualToString:@""])
                      {
                          
                          [a addObject:@"N/A"];
                      }
                      else
                      {
                          [a addObject:format];
                      }
                      
                      NSString* bank_imd=[dic objectForKey:@"bank_imd"];
                      if ([bank_imd isEqualToString:@""])
                      {
                          
                          [a addObject:@"N/A"];
                      }
                      else
                      {
                          [a addObject:bank_imd];
                      }
                      
                      NSString* bank_name=[dic objectForKey:@"bank_name"];
                      if ([bank_name isEqualToString:@""])
                      {
                          
                          [a addObject:@"N/A"];
                      }
                      else
                      {
                          [a addObject:bank_name];
                      }
                      
                      
                      NSString *bbb = [a componentsJoinedByString:@"|"];   //returns a pointer to NSString
                      [arr_length addObject:bbb];
                      [a removeAllObjects];
                      
                  }
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry" :@"0"];
              
              //              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              //
              //              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              //              [alert addAction:ok];
              //              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
}

-(void) AddAccountTitleFetch:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    
    
    gblclass.strAccessKey_for_add=acc_type;
    //fetchtitlebranchcode
    
    //yourTexField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:acc_type],
                                [encrypt encrypt_Data:bankname],
                                [encrypt encrypt_Data:bankimd],
                                [encrypt encrypt_Data:fetchtitlebranchcode],
                                [encrypt encrypt_Data:[self.txtaccountnumber.text uppercaseString]],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccType",
                                                                   @"strBankName",
                                                                   @"strBankImd",
                                                                   @"strBranchCode",
                                                                   @"strAccountNumber",
                                                                   @"strIBFTFormat",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager POST:@"AddAccountTitleFetch" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              a=[[NSMutableArray alloc] init];
              
              responsecode =[dic objectForKey:@"Response"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  //NSLog(@"Done IBFT load branch");
                  gblclass.acc_type=acc_type;
                  gblclass.bankname=bankname;
                  gblclass.bankimd=bankimd;
                  gblclass.fetchtitlebranchcode=fetchtitlebranchcode;
                  gblclass.acc_number=self.txtaccountnumber.text;
                  gblclass.acctitle=[dic objectForKey:@"strlblAccountTitle"];
                  
                  vw_fetch_title.hidden = NO;
                  btn_combo.enabled = NO;
                  img_combo.hidden = YES;
                  _txtaccountnumber.enabled = NO;
                  
                  txt_accnt_title_2.text=[dic objectForKey:@"strlblAccountTitle"];
                  txt_accnt_number.text=[dic objectForKey:@"strlblAccountNumber"];
                  txt_accnt_nick.text=[dic objectForKey:@"strtxtAccountNick"];
                  
  
                  if ([txt_accnt_title_2.text isEqualToString:@""] || txt_accnt_title_2.text.length ==0)
                  {
                      txt_accnt_title_2.enabled = YES;
                  }
                  else
                  {
                      txt_accnt_title_2.enabled = NO;
                      txt_accnt_title_2.borderStyle = UITextBorderStyleNone;
                      [txt_accnt_title_2 setBackgroundColor:[UIColor clearColor]];
                  }
                  
                  [txt_accnt_title_2 adjustsFontSizeToFitWidth];
                  [txt_accnt_nick adjustsFontSizeToFitWidth];
 
                  lbl_h1_1.hidden = NO;
                  txt_accnt_title_2.hidden = NO;
                  
                  vw_ibft_relation.hidden = YES;
                  btn_cancel.hidden = YES;
                  btn_submit.hidden = YES;
                  btn_add_record.hidden = NO;
                  
                  _txtaccountnumber.hidden = YES;
                  lbl_acct_numb_brnch.hidden = NO;
                  lbl_acct_num_txt_brnch.hidden = NO;
                  lbl_acct_num_txt_brnch.text = @"Account No.";
                  lbl_acct_numb_brnch.text = _txtaccountnumber.text;
                  
                  
                  vw_fetch_title.frame = CGRectMake(0, 178, vw_fetch_title.frame.size.width, vw_fetch_title.frame.size.height);
                  
                  if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
                  {
                      arr_ibft_relation_load=[[dic objectForKey:@"outdsIBFTRelation"] objectForKey:@"Table"];
                      
                      vw_ibft_relation.hidden = NO;
                      vw_IBFT.hidden = YES;
                      _txtaccountnumber.text = txt_acct_number_ibft.text;
                      _txtaccountnumber.hidden = NO;
                      
                      for(dic in arr_ibft_relation_load)
                      {
                          
                          NSString* IBFT_RELATION_DESC=[dic objectForKey:@"IBFT_RELATION_DESC"];
                          if ([IBFT_RELATION_DESC isEqual:@""])
                          {
                              IBFT_RELATION_DESC=@"N/A";
                              [a addObject:IBFT_RELATION_DESC];
                          }
                          else
                          {
                              [a addObject:IBFT_RELATION_DESC];
                          }
                          
                          NSString* IBFT_RELATION_ID=[dic objectForKey:@"IBFT_RELATION_ID"];
                          if ([IBFT_RELATION_ID isEqual:@""])
                          {
                              IBFT_RELATION_ID=@"N/A";
                              [a addObject:IBFT_RELATION_ID];
                          }
                          else
                          {
                              [a addObject:IBFT_RELATION_ID];
                          }
                          
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];
                          [arr_ibft_relation addObject:bbb];
                          
                          
                          //NSLog(@"%@", bbb);
                          [a removeAllObjects];
                      }
                      
                      [table_ibft_relation reloadData];
                      
                  }
                  
                  
                  //                  txtbillservice.text=@"";
                  
                  //** saki 21 sept
                  //                  _txtbilltype.text=@"";
                  //                  _txtcompany.text=@"";
                  //                  _txtaccountnumber.text=@"";
                  
                  
                  //                  UIStoryboard*  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  //                  //vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"accounts"];
                  //                  UIViewController* vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"fetchtitle"];
                  //                   [self presentViewController:vc animated:NO completion:nil];
                  
                  
              }
              else if ([responsecode isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              }
              else
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self custom_alert:resstring :@"0"];
              }
              
              //NSLog(@"Done IBFT load branch");
              //  [self parsearray];
              //  [_tableviewcompany reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}


- (void)slideRight
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
}

-(void)addA2pBeneficiaryInquiry:(NSString *)strIndustry {
    
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //public string AddA2pBeneficiaryInquiry(string strSessionId, string IP, Int64 strUserId, string txtReceiverCNICText, string txtReConfirmReceiverCNICText, string txtReceiverNameText, string txtReceiverMobileNumberText, string txtReceiverEmailAddressText, string strCallingOTPType, out string strRelationshipId, out string strEmail, out string lblReceiversNameText, out string lblReceiversCNICText, out string lblReceiversMobilePhoneNumberText, out string lblReceiverEmailAddressText, out string strReturnMessage)
        
        
        //    txtReceiverCNICText:
        //    txtReConfirmReceiverCNICText:
        //    txtReceiverNameText:
        //    txtReceiverMobileNumberText:
        //    txtReceiverEmailAddressText:
        //    strCallingOTPType:
        
        //    strSessionId:
        //    IP:
        //    strUserId:
        //    Token:
        //    Device_ID:
        //    txtReceiverCNICText:
        //    txtReConfirmReceiverCNICText:
        //    txtReceiverNameText:
        //    txtReceiverMobileNumberText:
        //    txtReceiverEmailAddressText:
        //    strCallingOTPType:
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:self.receiverName.text],
                                    [encrypt encrypt_Data:self.receiverCnic.text],
                                    [encrypt encrypt_Data:self.confirmReceiverCnic.text],
                                    [encrypt encrypt_Data:self.receiverMobile.text],
                                    [encrypt encrypt_Data:self.receiverEmail.text],
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:@"ADDITION"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"txtReceiverNameText",
                                                                       @"txtReceiverCNICText",
                                                                       @"txtReConfirmReceiverCNICText",
                                                                       @"txtReceiverMobileNumberText",
                                                                       @"txtReceiverEmailAddressText",
                                                                       @"strUserId",
                                                                       @"strCallingOTPType",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"AddA2pBeneficiaryInquiry" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [self slideRight];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"school_otp"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription]  preferredStyle:UIAlertControllerStyleAlert];
                  
                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alert addAction:ok];
                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please Try again later." :@"0"];
    }
}

-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}






-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertController * alert=   [UIAlertController
                                                     alertControllerWithTitle:Title
                                                     message:exception
                                                     preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:UIAlertActionStyleDefault
                                                                        handler:nil]; //You can use a block here to handle a press on this button
                       [alert addAction:actionOk];
                       
                       [self presentViewController:alert animated:YES completion:nil];
                   });
    
    //[alert release];
}

//- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//
//    if(buttonIndex ==0){
//        [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
//        // txtbillservice.text=@"";
//        //  _txtbilltype.text=@"";
//        if([responsecode isEqualToString:@"0"]){
//            _txtcompany.text=@"";
//            _txtaccountnumber .text=@"";
//          //  _txtnick.text=@"";
//        }
//        else{
//            responsecode=@"";
//        }
//    }
//
//
//
//}

-(IBAction)btn_back:(id)sender
{
    // [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
    
    
    [self dismissViewControllerAnimated:NO completion:nil];
}



//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//    return YES;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//
//    [self.view endEditing:YES];
//    return YES;
//}
//
//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    NSLog(@"%@",@"Show");
//    // Assign new frame to your view
// //   [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
//
//}
//
//-(void)keyboardDidHide:(NSNotification *)notification
//{
//    NSLog(@"%@",@"Show");
////    [self.view setFrame:CGRectMake(0,0,320,460)];
//}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // NSUInteger maxLength = 20;
    
    if (textField==_txtaccountnumber)
    {
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        
        //        if (textField.text.length <=maxLength)
        //        {
        //
        //            //ACCEPTABLE_CHARECTERS
        //            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        //            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        //
        //         //   return [string isEqualToString:filtered];
        //
        //            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        //        }
        //        else
        //        {
        //
        //            if (range.length==1 && string.length==0)
        //            {
        //                //NSLog(@"backspace tapped");
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //
        //        }
    }
    
    
    if (textField==_receiverCnic)
    {
        
        maxLength=13;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                [self custom_alert:@"CNIC number should not start with zero." :@"0"];
                return NO;
            } else {
                if (![myCharSet characterIsMember:c]) {
                    return NO;
                } else {
                    return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
                }
            }
            
        }
        
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
//            }
//
//        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    if (textField==_confirmReceiverCnic)
    {
        
        maxLength=13;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                [self custom_alert:@"CNIC number should not start with zero." :@"0"];
                return NO;
            } else {
                if (![myCharSet characterIsMember:c]) {
                    return NO;
                } else {
                    return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
                }
            }
            
        }
        
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
//            }
//
//        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if (textField==_receiverMobile)
    {
        
        maxLength=11;

        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && ![string isEqualToString:@"0"]) {
                [self custom_alert:@"Mobile number should start with zero." :@"0"];
                return NO;
            } else {
                if (![myCharSet characterIsMember:c]) {
                    return NO;
                } else {
                    return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
                }
            }
            
        }
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
//                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
//            }
//
//        }
        
        
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    if (textField==_receiverEmail)
    {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_.@"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
    }
    
    if ([textField isEqual:txt_iban ])
    {
        NSCharacterSet *myCharSet;
        
        if ([choose_type isEqualToString:@"2"])
        {
            maxLength = [iban_length integerValue];
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
        }
        else
        {
            maxLength = 16;
            myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        }
        
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    
    if ([textField isEqual:txt_acct_number_ibft ])
    {
        
        maxLength=30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    if ([textField isEqual:txt_accnt_email_address])
    {
        
        
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_.@"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        
    }
    
    if([textField isEqual:txt_accnt_title]) {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        
        for (int i=0; i<[string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c]) {
                return NO;
            } else {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 60;
            }
        }
    }
    
    if([textField isEqual:txt_accnt_nick]) {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        
        for (int i=0; i<[string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c]) {
                return NO;
            } else {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= 30;
            }
        }
    }
   
    
    if ([textField isEqual:self.receiverName]) {
        maxLength = 60;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
        }
    }
    
    if ([textField isEqual:self.receiverCnic] || [textField isEqual:self.confirmReceiverCnic]) {
        maxLength = 13;//13
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                [self custom_alert:@"NADRA CNIC number should not start with zero." :@"0"];
                return NO;
                
            } else if (![myCharSet characterIsMember:c]) {
                return NO;
            } else {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
        }
    }
    
    if ([textField isEqual:self.receiverMobile]) {
        maxLength = 11;
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && ![string isEqualToString:@"0"]) {
                [self custom_alert:@"Mobile number should start with zero." :@"0"];
                return NO;
            } else {
                if (![myCharSet characterIsMember:c]) {
                    return NO;
                } else {
                    return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
                }
            }
            
        }
    }
    
    if ([textField isEqual:self.receiverEmail])
    {
        maxLength = 100;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@._-"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
            }
        }
    }
    return YES;
    //[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert2 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        // [self mob_App_Logout:@""];
    }
    else if(buttonIndex == 0)
    {
        CATransition *transition = [ CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}






-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
        
        [self presentViewController:vc animated:NO completion:nil];
    }
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    vw_down_chck = @"1";
    
   // [self.view endEditing:YES];
    
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [_txtaccountnumber resignFirstResponder];
    [txt_accnt_email_address resignFirstResponder];
    [txt_accnt_nick resignFirstResponder];
    [txt_acct_number_ibft resignFirstResponder];
    [txt_ibft_relation resignFirstResponder];
    [txt_iban resignFirstResponder];
    [txt_accnt_title resignFirstResponder];
    
//    txt_choose_type
    
   // [self keyboardWillHide];
    
//    if ([vw_down_chck isEqualToString:@"1"])
//    {
//        [self setViewMovedUp:NO];
//        vw_down_chck = @"0";
//    }
    
    self.billtype.hidden=true;
    self.company.hidden=true;
    //[_txtnick resignFirstResponder];
}

#define kOFFSET_FOR_KEYBOARD 55.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        
        //[self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        //[self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
    self.company.hidden=YES;
    self.tableviewcompany.hidden=YES;
    table_ibft_relation.hidden=YES;
}



///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"load"])
        {
            chk_ssl=@"";
            
            if ([txttypebillvalue isEqualToString:@"IBAN"])
            {
                txt_iban.keyboardType = UIKeyboardTypeAlphabet;
                gblclass.bill_type = @"Add IBAN";
                //NSLog(@"ither bank ac");
                self.dd3.hidden = false;
                btn_combo.hidden=YES;
                img_dd3.hidden=YES;
                _txtcompany.hidden=YES;
                acc_type =@"IBAN";
//                txt_iban.hidden=NO;
                txt_iban.placeholder=@"Enter IBAN";
                _txtaccountnumber.hidden=YES;
                
                //txt_iban.text=@"PK78UNIL0109000212549568";
                //txt_iban.text=@"PK67FAYS0118006000003633";
                
                [hud hideAnimated:YES];
                
            }
            else if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
            {
                [hud showAnimated:YES];
                
                gblclass.bill_type = @"Add UBL Branch Account";
                
                self.dd3.hidden = false;
                //NSLog(@"ubl branch acc");
                acc_type =@"UBL";
                bankname =@"United Bank Limited";
                bankimd=@"588974";
                
                self.txtcompany.placeholder=@"Select Branch";
                [self Get_IBFT_BankName:@""];
                img_dd3.hidden=NO;
                txt_iban.hidden=YES;
                // [hud hideAnimated:YES];
                
                minLength=8;
                maxLength=12;
                str_validation=@"Account number length must be 8-12 digit";
                
            }
            else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
            {
                // self.txtcompany.text = @"Ubl Omni Branch - 1256";
                
                gblclass.bill_type = @"Add UBL Omni Account";
                self.txtcompany.text = @"UBL Omni Branch";
                self.txtcompany.enabled = NO;
                self.dd3.hidden = true;
                bankname =@"United Bank Limited";
                acc_type =@"OMNI";
                bankimd=@"588974";
                fetchtitlebranchcode=@"";  //1256
                
                self.txtcompany.placeholder=@"Branch Name";
                // fetchtitlebranchcode=
                self.company.hidden = true;
                img_dd3.hidden=YES;
                maxLength=11;
                str_validation=@"Account number length must be 11 digit";
                txt_iban.hidden=YES;
                [hud hideAnimated:YES];
                
            }
            else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
            {
                [hud showAnimated:YES];
                txt_acct_number_ibft.keyboardType = UIKeyboardTypeAlphabet;
                gblclass.bill_type = @"Add UBL Other Bank Account (IBFT)";
                //NSLog(@"ither bank ac");
                self.dd3.hidden = false;
                acc_type =@"IBFT";
                [self LoadIBFTbanks:@""];
                //  [self SSL_Call];
                self.txtcompany.placeholder=@"Select Bank";
                img_dd3.hidden=NO;
                txt_iban.hidden=YES;
                // [hud hideAnimated:YES];
                
            }
            
            
        } //load End ...
        else if ([chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            
            if ([txttypebillvalue isEqualToString:@"IBAN"])
            {
                [self Get_IBAN_Title:@""];
                
            }
            else if ([txttypebillvalue isEqualToString:@"TRANSFER FUNDS TO CNIC"])
            {
                [self addA2pBeneficiaryInquiry:@""];
            }
            
        }
        else if ([chk_ssl isEqualToString:@"otp"])
        {
            
            chk_ssl=@"";
            [self GenerateOTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"load"])
    {
        chk_ssl=@"";
        if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
            txt_iban.keyboardType = UIKeyboardTypeAlphabet;
            
            vw_iban.hidden = NO;
            gblclass.bill_type = @"Add IBAN";
            //NSLog(@"ither bank ac");
            self.dd3.hidden = false;
            btn_combo.hidden=YES;
            img_dd3.hidden=YES;
            _txtcompany.hidden=YES;
            acc_type =@"IBAN";
//          txt_iban.hidden=NO;
            txt_iban.placeholder=@"Enter IBAN";
            _txtaccountnumber.hidden=YES;
            [self Get_IBFT_BankName:@""];
            
           // txt_iban.text=@"PK78UNIL0109000212549568";
            // txt_iban.text=@"PK67FAYS0118006000003633";
            
           // [hud hideAnimated:YES];
            
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
        {
            [hud showAnimated:YES];
            
            gblclass.bill_type = @"Add UBL Branch Account";
            
            self.dd3.hidden = false;
            //NSLog(@"ubl branch acc");
            acc_type =@"UBL";
            bankname =@"United Bank Limited";
            bankimd=@"588974";
            self.txtcompany.placeholder=@"Select Branch Name";
            [self Load_Branches:@""];
            img_dd3.hidden=NO;
            // [hud hideAnimated:YES];
            minLength=8;
            maxLength=12;
            str_validation=@"Account number length must be 8-12 digit";
            
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Omni Account"])
        {
            // self.txtcompany.text = @"Ubl Omni Branch - 1256";
            
            gblclass.bill_type = @"Add UBL Omni Account";
            self.txtcompany.text = @"UBL Omni Branch";
            self.txtcompany.enabled = NO;
            txt_iban.hidden = YES;
            self.dd3.hidden = true;
            bankname =@"United Bank Limited";
            acc_type =@"OMNI";
            bankimd=@"588974";
            fetchtitlebranchcode=@"";  //1256
            self.txtcompany.placeholder=@"Branch Name";
            // fetchtitlebranchcode=
            self.company.hidden = true;
            img_dd3.hidden=YES;
            maxLength=11;
            str_validation=@"Account number length must be 11 digit";
            
            [hud hideAnimated:YES];
            
        }
        else if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
        {
            [hud showAnimated:YES];
            txt_acct_number_ibft.keyboardType = UIKeyboardTypeAlphabet;
            gblclass.bill_type = @"Add UBL Other Bank Account (IBFT)";
            //NSLog(@"ither bank ac");
            self.dd3.hidden = false;
            acc_type =@"IBFT";
            [self LoadIBFTbanks:@""];
            //  [self SSL_Call];
            self.txtcompany.placeholder=@"Select Bank";
            img_dd3.hidden=NO;
            // [hud hideAnimated:YES];
            
        }
    } //load End ...
    else if ([chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
//            [self Get_IBAN_Title:@""];
              [self Verify_IBAN_CreditCard:@""];
        } else if ([txttypebillvalue isEqualToString:@"TRANSFER FUNDS TO CNIC"])
        {
            [self addA2pBeneficiaryInquiry:@""];
        }
        else
        {
            [self AddAccountTitleFetch:@""];
        }
        
        //        chk_ssl=@"";
        //
        //        [self AddAccountTitleFetch:@""];
    }
    else if ( [chk_ssl isEqualToString:@"otp"])
    {
        chk_ssl=@"";
        [self GenerateOTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    chk_ssl=@"";
    [self.connection cancel];
    //    [hud hideAnimated:YES];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
   // NSLog(@"%@", gblclass.SSL_name);
    //NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
  //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    @try
    {
        
    
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }

    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void) GenerateOTP:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"MOB", nil]];
    
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.acc_number);
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
//    NSLog(@"%@",gblclass.fetchtitlebranchcode);
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.fetchtitlebranchcode],
                                [encrypt encrypt_Data:[gblclass.acc_number uppercaseString]],
                                [encrypt encrypt_Data:@"Add Payee List"],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:@"3"],
                                [encrypt encrypt_Data:@"FT"],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"ADDITION"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
                  
                  gblclass.add_bill_type=@"Add_payee_omni";
                  gblclass.add_payee_email=txt_accnt_email_address.text;
                  
                  //gblclass.add_bill_type=self.txtbilltype.text;
                  
                  CATransition *transition = [CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"add_bill_otp"];
                  
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
                  
                  //  [self showAlert:responsemsg :@"Attention"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              [self custom_alert:[error localizedDescription]  :@"0"]; 
              
          }];
    //   //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}

-(BOOL)isValidEmail:(NSString *)emailchk
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailchk];
}

-(IBAction)btn_ibft_relation:(id)sender
{
    vw_down_chck = @"1";
    vw_table.hidden=NO;
    _tableviewcompany.hidden=YES;
    _tableviewbilltype.hidden=YES;
    search.hidden=YES;
    
    lbl_table_header.text=@"Select Beneficiary Relation";
    
    table_ibft_relation.hidden=NO;
    
    [self.view endEditing:YES];
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtaccountnumber resignFirstResponder];
    [txt_accnt_email_address resignFirstResponder];
    [txt_accnt_nick resignFirstResponder];
    [txt_acct_number_ibft resignFirstResponder];
}


- (IBAction)btn_generateotp:(UIButton *)sender
{
    
  //  NSLog(@"%@",txttypebillvalue);
    
    [txt_iban resignFirstResponder];
    
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [ _txtaccountnumber resignFirstResponder];
    [txt_accnt_email_address resignFirstResponder];
    [txt_accnt_nick resignFirstResponder];
    [txt_acct_number_ibft resignFirstResponder];
    
    if([txt_accnt_nick.text length] >0 && [txt_accnt_nick.text length] <5 )
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Enter nick between 5 to 30 characters"  :@"0"];
        return ;
    }
    
    if ([txttypebillvalue isEqualToString:@"IBAN"])
    {
        if ([txt_accnt_title.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please enter account title" :@"0"];
        }
    }
    else if ([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
    {
        if ([txt_accnt_title_2.text isEqualToString:@""])
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please enter account title" :@"0"];
        }
    }
//    if ([txt_accnt_title.text isEqualToString:@""])
//    {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Please enter account title" :@"0"];
//    }
    
    if([txt_accnt_title.text length] >0 && [txt_accnt_title.text length] <5 )
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Enter account title between 5 to 60 characters"  :@"0"];
        return ;
    }
    else if ([txt_accnt_nick.text isEqualToString:@""])
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please enter account nick" :@"0"];
    }
    else if (txt_accnt_email_address.text.length>0)
    {
        
        if([self isValidEmail:txt_accnt_email_address.text])
        {
            
            if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
            {
                if([txt_ibft_relation.text isEqualToString:@""])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Please select beneficiary relation" :@"0"];
                    return;
                }
            }
            
            if ([txttypebillvalue isEqualToString:@"IBAN"])
            {
                if ([txt_ibft_relation.text isEqualToString:@""] && [chck_ibft isEqualToString:@"1"])
                {
                    [hud hideAnimated:YES];
                    [self custom_alert:@"Please select beneficiary relation" :@"0"];
                    
                    // [self showAlert:@"Select Ibft Relation" :@"Attention"];
                    return;
                }
            }
            
            if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
            {
                gblclass.acctitle =  txt_accnt_title_2.text;
            }
            else
            {
                gblclass.acctitle =  txt_accnt_title.text;
            }
            gblclass.acct_nick = txt_accnt_nick.text;
            
       //     NSLog(@"%@",gblclass.acctitle);
            
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"otp";
                [self SSL_Call];
                
                //[self GenerateOTP:@""];
            }
            
        }
        else
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Enter a valid email address" :@"0"];
            // [self showAlert:@"Enter a valid email address" :@"Attention"];
        }
        
    }
    else if ([txttypebillvalue isEqualToString:@"IBAN"])
    {
        if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
        {
            if([txt_ibft_relation.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Select Ibft Relation" :@"0"];
                
                // [self showAlert:@"Select Ibft Relation" :@"Attention"];
                return;
            }
        }
        else if ([txttypebillvalue isEqualToString:@"IBAN"])
        {
            if ([txt_ibft_relation.text isEqualToString:@""] && [chck_ibft isEqualToString:@"1"])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please select relation" :@"0"];
                return;
            }
        }
        
//        NSLog(@"%@",gblclass.acctitle);
//        NSLog(@"%@",txt_accnt_nick.text);
        
        gblclass.acctitle = txt_accnt_title.text;
        gblclass.acct_nick = txt_accnt_nick.text;
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"otp";
            [self SSL_Call];
        }
        
    }
    else
    {
        if([txttypebillvalue isEqualToString:@"Other Bank Account (IBFT)"])
        {
            if([txt_ibft_relation.text isEqualToString:@""])
            {
                [hud hideAnimated:YES];
                [self custom_alert:@"Please select beneficiary relation" :@"0"];
                return;
            }
        }
        
        //ibft
        
        if ([txttypebillvalue isEqualToString:@"UBL Branch Account"])
        {
            gblclass.acctitle =  txt_accnt_title_2.text;
        }
        else
        {
            gblclass.acctitle =  txt_accnt_title.text;
        }
        
        gblclass.acct_nick = txt_accnt_nick.text;
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"otp";
            [self SSL_Call];
            //[self GenerateOTP:@""];
        }
        
    }
    
    
    //    if([txt_accnt_email_address.text isEqualToString:@""])
    //    {
    //        //NSLog(@"sdf");
    //        [self checkinternet];
    //        if (netAvailable)
    //        {
    //            [self GenerateOTP:@""];
    //        }
    //    }
    //    else if ([self isValidEmail:txt_accnt_email_address.text])
    //    {
    //        //NSLog(@"sdf");
    //        [self checkinternet];
    //        if (netAvailable)
    //        {
    //            [self GenerateOTP:@""];
    //        }
    //    }
    //    else
    //    {
    //        [self showAlert:@"Please enter correct email address" :@"Attention"];
    //    }
    
    
    //     if([txt_accnt_email_address.text isValidEmail])
    //     {
    //         //NSLog(@"yes");
    //     }
    
    
    // if([self isValidEmail:self.txtemail.text]){
    
    
    //    }else{
    //        [self showAlert:@"Please enter correct email address" :@"Attention"];
    //    }
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_choose_type:(id)sender
{
//    arr_choose_type = [NSArray arrayWithObjects:@"IBAN",@"Credit Card", nil];
    
    lbl_table_header.text = @"Choose Type";
    [txt_iban resignFirstResponder];
    search.hidden = YES;
    
   // [table_choose_type reloadData];
    
//    arr_choose_type = [[NSMutableArray alloc] init];
//    [arr_choose_type addObject:@"IBAN"];
//    [arr_choose_type addObject:@"Credit Card"];
    
  //  [table_choose_type reloadData];
    
    if(table_choose_type.isHidden==YES)
    {
        self.company.hidden=NO;
        self.tableviewcompany.hidden=YES;
        table_choose_type.hidden = NO;
        vw_table.hidden=NO;
       // [table_choose_type reloadData];
        
    }
    else
    {
        self.company.hidden=YES;
        self.tableviewcompany.hidden=YES;
        table_choose_type.hidden = NO;
        vw_table.hidden=YES;
    }
}


- (IBAction)btn_bank_choose:(UIButton *)sender
{
        [txt_iban resignFirstResponder];
    
        lbl_table_header.text = @"Select Bank";
        self.billtype.hidden=YES;
        search.hidden = NO;
        if(self.tableviewcompany.isHidden==YES)
        {
            self.company.hidden=NO;
            self.tableviewcompany.hidden=NO;
            table_choose_type.hidden = YES;
            vw_table.hidden=NO;
            [_tableviewcompany reloadData];
        }
        else
        {
            self.company.hidden=YES;
            self.tableviewcompany.hidden=YES;
            table_choose_type.hidden = YES;
            vw_table.hidden=YES;
        }
}


@end

