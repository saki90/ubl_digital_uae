//
//  Verify_Otp_Mobile_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 24/05/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import "OtpTextField.h"

@interface Verify_Otp_Mobile_VC : UIViewController <OtpTextFieldDelegate, UITextFieldDelegate, CLLocationManagerDelegate>
{
    IBOutlet OtpTextField* txt_1;
    IBOutlet OtpTextField* txt_2;
    IBOutlet OtpTextField* txt_3;
    IBOutlet OtpTextField* txt_4;
    IBOutlet OtpTextField* txt_5;
    IBOutlet OtpTextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    IBOutlet UIButton* btn_resend_otp;
    
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
