//
//  OutdtDataset.m
//
//  Created by   on 22/01/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "OutdtDataset.h"
#import "GnbRefreshAcct.h"


NSString *const kOutdtDatasetGnbRefreshAcct = @"GnbRefreshAcct";


@interface OutdtDataset ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation OutdtDataset

@synthesize gnbRefreshAcct = _gnbRefreshAcct;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedGnbRefreshAcct = [dict objectForKey:kOutdtDatasetGnbRefreshAcct];
    NSMutableArray *parsedGnbRefreshAcct = [NSMutableArray array];
    if ([receivedGnbRefreshAcct isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedGnbRefreshAcct) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedGnbRefreshAcct addObject:[GnbRefreshAcct modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedGnbRefreshAcct isKindOfClass:[NSDictionary class]]) {
       [parsedGnbRefreshAcct addObject:[GnbRefreshAcct modelObjectWithDictionary:(NSDictionary *)receivedGnbRefreshAcct]];
    }

    self.gnbRefreshAcct = [NSArray arrayWithArray:parsedGnbRefreshAcct];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForGnbRefreshAcct = [NSMutableArray array];
    for (NSObject *subArrayObject in self.gnbRefreshAcct) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForGnbRefreshAcct addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForGnbRefreshAcct addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForGnbRefreshAcct] forKey:kOutdtDatasetGnbRefreshAcct];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.gnbRefreshAcct = [aDecoder decodeObjectForKey:kOutdtDatasetGnbRefreshAcct];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_gnbRefreshAcct forKey:kOutdtDatasetGnbRefreshAcct];
}

- (id)copyWithZone:(NSZone *)zone
{
    OutdtDataset *copy = [[OutdtDataset alloc] init];
    
    if (copy) {

        copy.gnbRefreshAcct = [self.gnbRefreshAcct copyWithZone:zone];
    }
    
    return copy;
}


@end
