//
//  MasterPass_limit_VC.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 08/11/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface MasterPass_limit_VC : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITextField* txt_amount;
    IBOutlet UITextField* txt_master_card;
    IBOutlet UITextField* txt_atm_pin;
    IBOutlet UILabel* lbl_current_daily_limit;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}


@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

