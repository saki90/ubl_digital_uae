//
//  Signup_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface Signup_VC : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    IBOutlet UITableView* table;
    IBOutlet UILabel* lbl_heading;

    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********

}



//@property(nonatomic,strong) IBOutlet UIWebView* web;

@property (nonatomic, strong) IBOutlet UITextField* txtcountryname;

//@property (nonatomic, strong) IBOutlet UIView* billserviceview;
@property (nonatomic, strong) IBOutlet UIView* countryview;
@property (nonatomic, strong) IBOutlet UIView* company;

//@property (nonatomic, strong) IBOutlet UITableView* tableviewbillservice;
@property (nonatomic, strong) IBOutlet UITableView* countrytableview;
//@property (nonatomic, strong) IBOutlet UITableView* tableviewcompany;


@end
