//
//  Question_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 15/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Question_VC : UIViewController<UITextFieldDelegate>
{
    IBOutlet UIView* vw_table;
}

@property (nonatomic, strong) IBOutlet UIView* question1view;
@property (nonatomic, strong) IBOutlet UIView* question2view;
@property (nonatomic, strong) IBOutlet UIView* generalview;



@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;



@property (nonatomic, strong) IBOutlet UITableView* question1tableview;

@property (nonatomic, strong) IBOutlet UITableView* question2tableview;

@property (nonatomic, strong) IBOutlet UITableView* generaltableview;

@property (nonatomic, strong) IBOutlet UITextField* txtlogin;
@property (nonatomic, strong) IBOutlet UITextField* txtpassword;
@property (nonatomic, strong) IBOutlet UITextField* txtretypepassword;
@property (nonatomic, strong) IBOutlet UITextField* txtgeneralquestion;
@property (nonatomic, strong) IBOutlet UITextField* txtsecretquestion;
@property (nonatomic, strong) IBOutlet UITextField* txtanssecret1;
@property (nonatomic, strong) IBOutlet UITextField* txtsecretquestion2;
@property (nonatomic, strong) IBOutlet UITextField* txtanssecret2;



@end
