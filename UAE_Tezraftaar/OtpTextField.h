//
//  OtpTextField.h
//  ubltestbanking
//
//  Created by Asim Khan on 7/2/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OtpTextFieldDelegate <NSObject>
@optional -(void)textFieldDidDelete:(UITextField *)textField;
@end

@interface OtpTextField : UITextField
@property (nonatomic, assign) id<OtpTextFieldDelegate> OtpDelegate;
@end
