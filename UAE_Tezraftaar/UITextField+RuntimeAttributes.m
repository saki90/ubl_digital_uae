//
//  UITextField+RuntimeAttribute.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 09/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <objc/runtime.h>
#import "UITextField+RuntimeAttributes.h"
#import "GlobalStaticClass.h"

static void *MaxLengthKey;
static void *IsEMailAddressKey;
static void *IsDecimalFieldKey;
static void *IsCnicNumberKey;

@implementation UITextField (RuntimeAttribute)

__attribute__((constructor)) static void initializeUITextFieldRuntimeAttributes(void)
{
    // do stuff
}

-(instancetype)init {
    self = [super init];
    
    if (self) {
        [self setAutocorrectionType:UITextAutocorrectionTypeNo];
        [self setSpellCheckingType:UITextSpellCheckingTypeNo];
//        self.autocorrectionType = UITextAutocorrectionTypeNo;
//        self.spellCheckingType = UITextSpellCheckingTypeNo;
    }
    
    return self;
}

// Locally named pastboard
- (void)copy:(id)sender {
    [[GlobalStaticClass getInstance].pasteBoard setString:self.text];
}

-(void)cut:(id)sender {
    [[GlobalStaticClass getInstance].pasteBoard setString:self.text];
    self.text = @"";
}

-(void)paste:(id)sender {
    if ([[GlobalStaticClass getInstance].pasteBoard hasStrings]) {
        self.text = [NSString stringWithFormat:@"%@%@", self.text,[GlobalStaticClass getInstance].pasteBoard.string];
    } else if ([[UIPasteboard generalPasteboard] hasStrings]) {
        self.text = [NSString stringWithFormat:@"%@%@", self.text,[UIPasteboard generalPasteboard].string];;
    }
}

-(void)setMaxLength:(NSNumber *)maxLength{
    objc_setAssociatedObject(self, &MaxLengthKey, maxLength, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber*)maxLength{
    return objc_getAssociatedObject(self, &MaxLengthKey);
}

-(void)setIsEmailAddress:(BOOL)isEMailAddress{
    objc_setAssociatedObject(self, &IsEMailAddressKey, [NSNumber numberWithBool:isEMailAddress], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isEmailAddress{
    NSNumber* isEmailAddressNumber = objc_getAssociatedObject(self, &IsEMailAddressKey);
    return [isEmailAddressNumber boolValue];
}

-(void)setIsCnicNumber:(BOOL)isCnicNumber{
    objc_setAssociatedObject(self, &IsCnicNumberKey, [NSNumber numberWithBool:isCnicNumber], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isCnicNumber{
    NSNumber* IsCnicNumberNumber = objc_getAssociatedObject(self, &IsCnicNumberKey);
    return [IsCnicNumberNumber boolValue];
}

-(void)setIsDecimalField:(BOOL)isDecimalField{
    objc_setAssociatedObject(self, &IsDecimalFieldKey, [NSNumber numberWithBool:isDecimalField], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isDecimalField{
    NSNumber* IsDecimalFieldNumber = objc_getAssociatedObject(self, &IsDecimalFieldKey);
    return [IsDecimalFieldNumber boolValue];
}


@end
