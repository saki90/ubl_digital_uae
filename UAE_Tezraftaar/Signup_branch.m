//
//  Signup_detail.m
//  ubltestbanking
//
//  Created by ammar on 04/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Signup_branch.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "APIdleManager.h"
#import "Encrypt.h"


@interface Signup_branch ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    NSString*  strCountry,*strTxtEmail,*strTxtKeyword,*strBranchCode,*strTxtAccountNumber,*strCardPin,*strAccType;
    
    NSMutableArray* banktype;
    NSMutableArray* branchname;
    NSMutableArray* searchbranchname;
    NSMutableArray* branchcode;
    NSString* atmavailable;
    UIAlertController* alert;
    
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    Encrypt *encrypt;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Signup_branch

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    strCountry=gblclass.signup_countrycode;
    //**[self LoadBranches:@""];
    atmavailable=@"N";
    
    
    self.acc_number.delegate=self;
    self.branchname.delegate=self;
    self.emailaddress.delegate=self;
    self.atmpin.delegate=self;
    self.atmcard.delegate=self;
    self.dntatmemail.delegate=self;
    self.maidenname.delegate=self;
    
    
    self.haveview.hidden=NO;
    atmavailable=@"Y";
    
    //**     [self Play_bg_video];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
    self.atmcard.text=@"5327090949000026";
    self.atmpin.text=@"1111";
    
    
    [APIdleManager sharedInstance].onTimeout = ^(void){
        //  [self.timeoutLabel  setText:@"YES"];
        
        
        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    gblclass.story_board bundle:[NSBundle mainBundle]];
        UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
        
        
        [self presentViewController:myController animated:YES completion:nil];
        
        
        APIdleManager * cc1=[[APIdleManager alloc] init];
        // cc.createTimer;
        
        [cc1 timme_invaletedd];
        
    };
    
    
}


-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    //**  [super viewDidAppear:animated];
    //**  [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    //**   AVPlayerItem *p = [notification object];
    //**   [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    //**  [self.avplayer play];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_dnthaveatm:(id)sender
{
    self.haveview.hidden=true;
    self.dnthaveview.hidden=false;
    atmavailable=@"N";
    
}

-(IBAction)btn_haveatm:(id)sender
{
    self.dnthaveview.hidden=true;
    self.haveview.hidden=false;
    
    atmavailable=@"Y";
}

- (IBAction)btn_selectbank:(UIButton *)sender
{
    
    if(self.bankview.hidden){
        self.bankview.hidden=false;
    }
    else{
        self.bankview.hidden=true;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if([searchbranchname count]==0)
    {
        return 0;
    }
    else
    {
        return[searchbranchname count];
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    if(tableView==self.banktableview){
        
        cell.textLabel.text=[searchbranchname objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        
    }
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    Yourstring=[arr_act_type objectAtIndex:indexPath.row];
    //
    //NSLog(@"Selected item %ld ",(long)indexPath.row);
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    self.bankview.hidden=true;
    NSString* selectdbranchname=[searchbranchname objectAtIndex:indexPath.row];
    self.branchname.text=selectdbranchname;
    NSUInteger* set = [branchname indexOfObject:selectdbranchname];
    //    strBranchCode=[branchcode objectAtIndex:indexPath.row];
    strBranchCode=[branchcode objectAtIndex:set];
    
}


-(IBAction)btn_next:(id)sender
{
    
    if([atmavailable isEqualToString:@"Y"])
    {
        
        //        if ([self.emailaddress.text isEqualToString:@""])
        //        {
        //            NSString* exe=@"Enter Email Address";
        //            [self showAlert:exe:@"Attention"];
        //            return ;
        //        }
        if (![self validateEmail:[self.emailaddress text]])
            //        {
            //            NSString* exe=@"Please enter valid email address";
            //            [self showAlert:exe:@"Attention"];
            //            return ;
            //        }
            if ([self.atmcard.text isEqualToString:@""])
            {
                NSString* exe=@"Enter ATM Card";
                [self showAlert:exe:@"Attention"];
                return ;
            }
            else if ([self.atmpin.text isEqualToString:@""])
            {
                NSString* exe=@"Enter ATM Pin";
                [self showAlert:exe:@"Attention"];
                return ;
            }
        
        
        
        strTxtEmail=self.emailaddress.text;
        
        strTxtKeyword=@"";
        strBranchCode=@"0";
        strTxtAccountNumber=self.atmcard.text;
        
        strCardPin=self.atmpin.text;
        gblclass.signup_acctype=@"WalletCard";
        
    }
    else
    {
        
        if ([self.dntatmemail.text isEqualToString:@""])
        {
            NSString* exe=@"Enter Register Email Address";
            [self showAlert:exe:@"Attention"];
            return ;
        }
        else if(![self validateEmail:[self.dntatmemail text]])
        {
            NSString* exe=@"Please enter valid email address";
            [self showAlert:exe:@"Attention"];
            return ;
        }
        else if ([self.maidenname.text isEqualToString:@""])
        {
            NSString* exe=@"Keyword/Mother's Maiden name";
            [self showAlert:exe:@"Attention"];
            return ;
        }
        else if ([self.branchname.text isEqualToString:@""])
        {
            NSString* exe=@"Select Branch Name";
            [self showAlert:exe:@"Attention"];
            return ;
        }
        else if ([self.acc_number.text isEqualToString:@""])
        {
            NSString* exe=@"Enter Account Number";
            [self showAlert:exe:@"Attention"];
            return ;
        }
        
        strTxtKeyword=self.maidenname.text;
        
        // strTxtAccountNumber=self.atmcard.text;
        
        strTxtAccountNumber=self.acc_number.text;
        strTxtEmail=self.dntatmemail.text;
        strCardPin=self.atmpin.text;
        gblclass.signup_acctype=@"AccInfo";
    }
    
    strAccType=gblclass.signup_acctype;
    
    //    [self UserMobReg:@""];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
}

-(void) UserMobReg:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    
    //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    //NSLog(@"strCountry%@",strCountry);
    //NSLog(@"strTxtEmail%@",strTxtEmail);
    //NSLog(@"strTxtKeyword%@",strTxtKeyword);
    //NSLog(@"strBranchCode%@",strBranchCode);
    //NSLog(@"strTxtAccountNumber%@",strTxtAccountNumber);
    //NSLog(@"strCardPin%@",strCardPin);
    //NSLog(@"strAccType%@",strAccType);
    //WalletCard,0
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"0"],
                                [encrypt encrypt_Data:@"1234"],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abv"],
                                [encrypt encrypt_Data:@"1"],
                                [encrypt encrypt_Data:strTxtEmail],
                                [encrypt encrypt_Data:strTxtKeyword],
                                [encrypt encrypt_Data:strBranchCode],
                                [encrypt encrypt_Data:strTxtAccountNumber],
                                [encrypt encrypt_Data:strCardPin],
                                [encrypt encrypt_Data:strAccType],
                                [encrypt encrypt_Data:gblclass.Udid], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode",
                                                                   @"strCountry",
                                                                   @"strTxtEmail",
                                                                   @"strTxtKeyword",
                                                                   @"strBranchCode",
                                                                   @"strTxtAccountNumber",
                                                                   @"strCardPin",
                                                                   @"strAccType",
                                                                   @"strDeviceID", nil]];
    
    //NSLog(@"%@",dictparam);
    
    
    //    alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:gblclass.Udid preferredStyle:UIAlertControllerStyleAlert];
    //
    //    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //    [alert addAction:ok];
    //    [self presentViewController:alert animated:YES completion:nil];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"UserMobReg" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              // companytype = [(NSDictionary *)[dic objectForKey:@"Response"] objectForKey:@"Table1"];
              //ttid=
              
              NSString* responsecode = [dic objectForKey:@"Response"];
              if ([responsecode isEqualToString:@"0" ])
              {
                  [hud hideAnimated:YES];
                  
                  gblclass.From_Exis_login=@"0";
                  gblclass.Otp_mobile_verifi=[dic objectForKey:@"strMobileNumber"];
                  
                  gblclass.signup_relationshipid=[dic objectForKey:@"strRelationShipId"];
                  gblclass.signup_mobile=[dic objectForKey:@"strMobileNumber"];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  
                  NSString* exe=[GlobalStaticClass stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  [self showAlert:exe:@"Attention"];
              }
              //NSLog(@"Done IBFT load branch");
              //[self parsearray];
              //  [_countrytableview reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              [hud hideAnimated:YES];
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                       [alert1 show];
                   });
    //[alert release];
}


-(void) LoadBranches:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    banktype = [[NSMutableArray alloc]init];
    branchname = [[NSMutableArray alloc]init];
    branchcode = [[NSMutableArray alloc]init];
    searchbranchname= [[NSMutableArray alloc]init];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1"],
                                [encrypt encrypt_Data:@""], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strCountry",
                                                                   @"StrUserType", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"LoadBranches" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              
              //NSLog(@"%@",[dic objectForKey:@"Response"]);
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  banktype = [(NSDictionary *)[dic objectForKey:@"dtUBLBankList"] objectForKey:@"Table1"];
                  for(int i=0;i<[banktype count];i++)
                  {
                      NSDictionary *dict = [banktype objectAtIndex:i];
                      [branchname addObject:[dict objectForKey:@"brdetail"]];
                      [branchcode addObject:[dict objectForKey:@"brcode"]];
                      [searchbranchname addObject:[dict objectForKey:@"brdetail"]];
                  }
                  
                  //NSLog(@"Done load branch");
                  //[self parsearray];
                  [_banktableview reloadData];
                  
              }
              else
              {
                  [ self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention" ];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"" withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    
    if ([theTextField isEqual:self.acc_number])
    {
        self.acc_number.text=formattedOutput;
    }
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //   autocompleteTableView.hidden = NO;
    // if([_branchname.text length]>3){
    
    //}
    
    //    NSInteger MAX_DIGITS;
    //    BOOL stringIsValid;
    
    if ([textField isEqual:self.acc_number])
    {
        NSInteger MAX_DIGITS=12;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
    }
    else if ([textField isEqual:self.atmcard])
    {
        NSInteger MAX_DIGITS=16;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
    }
    else if ([textField isEqual:self.atmpin])
    {
        NSInteger MAX_DIGITS=4;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                
                
                if (_atmpin.text.length==3)
                {
                    [hud showAnimated:YES];
                    [self.view addSubview:hud];
                    [hud showAnimated:YES];
                    
                    _atmpin.text=[_atmpin.text stringByAppendingString:string];
                    
                    strTxtEmail=self.emailaddress.text;
                    
                    strTxtKeyword=@"";
                    strBranchCode=@"0";
                    strTxtAccountNumber=self.atmcard.text;
                    
                    strCardPin=self.atmpin.text;
                    gblclass.signup_acctype=@"WalletCard";
                    strAccType=gblclass.signup_acctype;
                    
                    
                    
                    [self SSL_Call];
                    
                    
                    return 0;
                }
                
                
                
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
    }
    else if ([textField isEqual:self.branchname])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                [self  searchAutocompleteEntriesWithSubstring:self.branchname.text];
                
                //return YES;
            }
        }
        [self  searchAutocompleteEntriesWithSubstring:self.branchname.text];
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([textField isEqual:self.dntatmemail])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@_."];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
                
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([textField isEqual:self.maidenname])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                
                return YES;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    
    
    //    if([searchbranchname count]==0){
    //        searchbranchname=branchname;
    //    }
    //    else{
    
    [searchbranchname removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", substring];
    
    searchbranchname= [NSMutableArray arrayWithArray: [branchname filteredArrayUsingPredicate:resultPredicate]];
    
    //    }
    
    _banktableview.hidden=NO;
    [_banktableview reloadData];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self.branchname resignFirstResponder];
    [self.acc_number resignFirstResponder];
    
    [self.emailaddress resignFirstResponder];
    [self.atmcard resignFirstResponder];
    [self.atmpin resignFirstResponder];
    [self.dntatmemail resignFirstResponder];
    [self.maidenname resignFirstResponder];
}

- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)btn_feature:(id)sender
{
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
    [self presentViewController:alert animated:YES completion:nil];
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    {
        
        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            [self UserMobReg:@""];
            
            [self.connection cancel];
        }
        else
        {
            [hud hideAnimated:YES];
            [self.connection cancel];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Authentication Failed."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        [hud hideAnimated:YES];
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [hud hideAnimated:YES];
        [self.connection cancel];
        
        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        
        gblclass.ssl_pin_check=@"0";
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"SSL Not Verified." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        return;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}




@end

