//
//  Touch_ID_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 19/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Touch_ID_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <Security/Security.h>
#import <LocalAuthentication/LocalAuthentication.h>
//saki #import <PeekabooConnect/PeekabooConnect.h>
#import "Encrypt.h"

@interface Touch_ID_VC ()
{
    UIAlertController* alert;
    
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UIStoryboard *mainStoryboard;
    NSString *touch_id;
    NSString* user,*pass;
    NSString* chk_ssl;
    NSString* tch_chk;
    NSString* chk_tch_change;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* tch_ID_name;
}


- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@property (weak, nonatomic) IBOutlet UIView *yesNoView;

@end

@implementation Touch_ID_VC{
    
    CGRect yesFrame;
    CGRect yesBtnFrame;
    CGRect noFrame;
    CGRect noBtnFrame;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    tch_chk=@"0";
    
    [img_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
    [img_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
    
    touch_id=@"2";
    ssl_count = @"0";
    
    [self getCredentialsForServer_touch:@"finger_print"];
    
    
    //**********************************
    //By-Default select No ::
    
    [btn_chck_no setImage:[UIImage imageNamed:@"Check_touch_id.png"] forState:UIControlStateNormal];
    [btn_chck_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"] forState:UIControlStateNormal];
    
    [img_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
    [img_no setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
    
    
    touch_id = @"0";
    
    
    
    gblclass.touch_id_enable=@"0";
    
    //**********************************
    
    //touch_iddd
    if([tch_chk isEqualToString:@"1"]) {
        
        chk_tch_change=@"1";
        
        [btn_chck_yes setImage:[UIImage imageNamed:@"Check_touch_id.png"] forState:UIControlStateNormal];
        [img_yes setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
        touch_id=@"1";

        
        //UNCheck ...
        [img_yes setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
        [img_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
        
        
        // Get Credential from Keychain ::
        [self getCredentialsForServer:@"login"];
        
    } //touch_iddd
    else if([tch_chk isEqualToString:@"2"])
    {
        
    } //touch_iddd
    else if([tch_chk isEqualToString:@"0"])
    {
        
        // [btn_chck_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"] forState:UIControlStateNormal];
        // [img_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
        
        [btn_chck_no setImage:[UIImage imageNamed:@"Check_touch_id.png"] forState:UIControlStateNormal];
        [btn_chck_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"] forState:UIControlStateNormal];
        
        [img_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
        [img_no setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
        touch_id=@"0";
        
    }
    
    
//    NSLog(@"%@",gblclass.isIphoneX);
//    NSLog(@"%@",gblclass.story_board);
    
    if ([gblclass.story_board isEqualToString:@"iPhone_X"] || [gblclass.story_board isEqualToString:@"iPhone_Xr"])
    {
        [_touchLogo setImage:[UIImage imageNamed:@"faceID"]];
        _heading.text = @"Do you want to enable Face ID for UBL Digital App?";
        tch_ID_name = @"Face ID";
    }
    else
    {
        [_touchLogo setImage:[UIImage imageNamed:@"touchID"]];
        _heading.text = @"Do you want to enable Touch ID for UBL Digital App?";
        tch_ID_name = @"Touch ID";
    }
    
    
    if ([gblclass.setting_chck_Tch_ID isEqualToString:@"1"])
    {
        img_back_check.hidden = NO;
        btn_back_check.enabled=YES;
    }
    else
    {
        img_back_check.hidden = YES;
        btn_back_check.enabled=NO;
    }
    
//    if (gblclass.isIphoneX) {
//        [_touchLogo setImage:[UIImage imageNamed:@"faceID"]];
//        _heading.text = @"Do you want to enable Face ID for UBL Digital App?";
//
//    } else {
//        [_touchLogo setImage:[UIImage imageNamed:@"touchID"]];
//        _heading.text = @"Do you want to enable Touch ID for UBL Digital App?";
//    }
    
//    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
//
//        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
//
//            case 2436:
//                printf("iPhone X");
//                [_touchLogo setImage:[UIImage imageNamed:@"faceID"]];
//                _heading.text = @"Do you want to enable Face ID for UBL Digital App?";
//
//                break;
//            default:
//                [_touchLogo setImage:[UIImage imageNamed:@"touchID"]];
//                _heading.text = @"Do you want to enable Touch ID for UBL Digital App?";
//                printf("unknown");
//        }
//    }
    
  //  [self Toch_chk];
    
    [self Play_bg_video];
}

-(void)animateViews {
    
    [UIView animateWithDuration:0.4 animations:^{
        
        _touchLogo.frame = CGRectMake((self.view.frame.size.width/2) - (_touchLogo.frame.size.width/2), _touchLogo.frame.origin.y, _touchLogo.frame.size.width, _touchLogo.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        
        _heading.frame = CGRectMake((self.view.frame.size.width/2) - (_heading.frame.size.width/2), _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        
        _yesNoView.frame = CGRectMake((self.view.frame.size.width/2) - (_yesNoView.frame.size.width/2), _yesNoView.frame.origin.y, _yesNoView.frame.size.width, _yesNoView.frame.size.height);
        
    }];
    
    //    [UIView animateWithDuration:0.8 animations:^{
    //
    //
    //        _yes.frame = yesFrame;
    //        _yesBtn.frame = yesBtnFrame;
    //        _no.frame = noFrame;
    //        _noBtn.frame = noBtnFrame;
    //
    //    }];
    
    
    [UIView animateWithDuration:0.9 animations:^{
        
        
        _nxt_btn.frame = CGRectMake((self.view.frame.size.width/2) - (_nxt_btn.frame.size.width/2), _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
        
    }];
    
}

-(void)Toch_chk
{
    
    LAContext *myContext = [[LAContext alloc] init];
    
    if (@available(iOS 11.0, *))
    {
//        NSLog(@"%ld",(long)myContext.biometryType);
    }
    else
    {
        // Fallback on earlier versions
    }
    
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"Touch ID ";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                
                                if (success) {
                                    
                                    
                                    
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        // [self performSegueWithIdentifier:@"Success" sender:nil];
                                        
                                        
                                        [self.view addSubview:hud];
                                        [hud showAnimated:YES];
                                        
                                        //                                                [self checkinternet];
                                        //                                                if (netAvailable)
                                        //                                                {
                                        
                                        //                                            btn_next_press=@"0";
                                        //
                                        //                                            [self SSL_Call];
                                        //                                                }
                                        
                                    });
                                    
                                }
                                else
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        //                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                        //                                                                                            message:error.localizedDescription
                                        //                                                                                           delegate:self
                                        //                                                                                  cancelButtonTitle:@"OK"
                                        //                                                                                  otherButtonTitles:nil, nil];
                                        //  [alertView show];
                                        
                                        //NSLog(@"Switch to fall back authentication - ie, display a keypad or password entry box");
                                    });
                                }
                            }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ATTENTION"
                                                                message:authError.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            
            
            // btn_touch_id.hidden=YES;
            
        });
    }
    
}


-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.avplayer play];
    
    
    [super viewDidAppear:animated];
    [self.avplayer play];
    
    //    yesFrame = CGRectMake(self.yes.frame.origin.x, self.yes.frame.origin.y, self.yes.frame.size.width,  self.yes.frame.size.height);
    //    yesBtnFrame =  CGRectMake(self.yesBtn.frame.origin.x, self.yesBtn.frame.origin.y, self.yesBtn.frame.size.width,  self.yesBtn.frame.size.height);
    //
    //    noFrame = CGRectMake(self.no.frame.origin.x, self.no.frame.origin.y, self.yes.frame.size.width,  self.no.frame.size.height);
    //    noBtnFrame =  CGRectMake(self.noBtn.frame.origin.x, self.noBtn.frame.origin.y, self.noBtn.frame.size.width,  self.noBtn.frame.size.height);
    
    
    
    _touchLogo.frame = CGRectMake(self.view.frame.size.width + _touchLogo.frame.size.width, _touchLogo.frame.origin.y, _touchLogo.frame.size.width, _touchLogo.frame.size.height);
    _heading.frame = CGRectMake(self.view.frame.size.width + _heading.frame.size.width, _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
    
    _yesNoView.frame = CGRectMake(self.view.frame.size.width + _yesNoView.frame.size.width, _yesNoView.frame.origin.y, _yesNoView.frame.size.width, _yesNoView.frame.size.height);
    
    //    _yes.frame = CGRectMake(self.view.frame.size.width + _yes.frame.size.width, _yes.frame.origin.y, _yes.frame.size.width, _yes.frame.size.height);
    //    _yesBtn.frame = CGRectMake(self.view.frame.size.width + _yesBtn.frame.size.width, _yesBtn.frame.origin.y, _yesBtn.frame.size.width, _yesBtn.frame.size.height);
    //    _no.frame = CGRectMake(self.view.frame.size.width + _no.frame.size.width, _no.frame.origin.y, _no.frame.size.width, _no.frame.size.height);
    //    _noBtn.frame = CGRectMake(self.view.frame.size.width + _noBtn.frame.size.width, _noBtn.frame.origin.y, _noBtn.frame.size.width, _noBtn.frame.size.height);
    
    _nxt_btn.frame = CGRectMake(self.view.frame.size.width + _nxt_btn.frame.size.width, _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
    
    
    //_nxt_btn.frame = CGRectMake(self.view.frame.size.width + _nxt_btn.frame.size.width, _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
    
    
    [self animateViews];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)slide_right
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}



-(IBAction)btn_yes:(id)sender
{
    
    [btn_chck_yes setImage:[UIImage imageNamed:@"Check_touch_id.png"] forState:UIControlStateNormal];
    [btn_chck_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"] forState:UIControlStateNormal];
    
    [img_yes setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
    [img_no setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
    
    
   // tch_chk = @"1";
    touch_id = @"1";
    
    gblclass.touch_id_enable=@"1";
}


-(IBAction)btn_next:(id)sender
{
    
    if ([chk_tch_change isEqualToString:@"1"])
    {
        NSString* yy = [NSString stringWithFormat:@"%@ %@",tch_ID_name,@"is already enabled"];
        [self custom_alert:yy :@"0"];
        return;
    }
    
    
    if ([touch_id isEqualToString:@"2"])
    {
        [self custom_alert:@"Select Yes OR No" :@"0"];
        return;
    }
    else
    {
        
        [self removeAllCredentialsForServer:@"touch_enable"];
        [self with_touch_enable:@"11" forServer:@"touch_enable"]; //second_time
        
        
        if ([touch_id isEqualToString:@"1"])
        {
            
            //            [self touch_Yes];
            
            
            //            touch_id=@"1";
            //            gblclass.touch_id_enable=@"1";
            //
            //
            //
            //            // Remove Credential from Keychain ::
            //          //27 march 2017
            //        //    [self removeAllCredentialsForServer:@"login"];
            //            [self removeAllCredentialsForServer:@"finger_print"];
            //
            //
            //
            //
            //            //for finger_print enable/disable ::
            //            [self finger_print_enable:@"1" forServer:@"finger_print"];
            
            
        }
        else
        {
            
            //           [self touch_No];
            
            
            //            touch_id=@"0";
            //            gblclass.touch_id_enable=@"0";
            //
            //
            //            [self removeAllCredentialsForServer:@"finger_print"];
            //            [self removeAllCredentialsForServer:@"login"];
            //
            //
            //            //for finger_print enable/disable ::
            //            [self finger_print_enable:@"2" forServer:@"finger_print"];
            
        }
        
        
        
        //** 22 nov       gblclass.touch_id_enable=@"1";
        
        [hud showAnimated:YES];
        [hud hideAnimated:YES afterDelay:130];
        [self.view addSubview:hud];
        
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"review";
            [self SSL_Call];
        }
        
        //[self Reg_Biometric_User:@""];
    }
}



-(void)touch_Yes
{
    touch_id=@"1";
    gblclass.touch_id_enable=@"1";
    
    
    
    // Remove Credential from Keychain ::
    //27 march 2017
    //    [self removeAllCredentialsForServer:@"login"];
    [self removeAllCredentialsForServer:@"finger_print"];
    
    
    
    
    //for finger_print enable/disable ::
    [self finger_print_enable:@"1" forServer:@"finger_print"];
}

-(void)touch_No
{
    touch_id=@"0";
    gblclass.touch_id_enable=@"0";
    
    
    [self removeAllCredentialsForServer:@"finger_print"];
    [self removeAllCredentialsForServer:@"login"];
    
    
    //for finger_print enable/disable ::
    [self finger_print_enable:@"2" forServer:@"finger_print"];
}



-(void) with_touch_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}



-(void) finger_print_enable:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    //  dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    
    
    dict=[NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),kSecClass,server,kSecAttrServer,passwordData,kSecValueData, nil];
    
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


//*********************** Login credential Start ******************************



-(void) saveUsername:(NSString*)user withPassword:(NSString*)pass forServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [pass dataUsingEncoding:NSUTF8StringEncoding];
    NSData* UserData = [user dataUsingEncoding:NSUTF8StringEncoding];
    
    dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, server, kSecAttrServer, passwordData, kSecValueData, UserData, kSecAttrAccount, nil];
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
    
}


-(void) removeAllCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
}


-(void) getCredentialsForServer:(NSString*)server
{
    
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Look up server in the keychain
    NSDictionary* found = nil;
    CFDictionaryRef foundCF;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
    
    // Check if found
    found = (__bridge NSDictionary*)(foundCF);
    if (!found)
        return;
    
    //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
    
    // Found
    user = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
    
    pass = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    
    
    gblclass.sign_in_username = user;
    gblclass.sign_in_pw = pass;
    
}


//*********************** Login credential End ******************************



-(IBAction)btn_no:(id)sender
{
    
    chk_tch_change=@"";
    
    [btn_chck_no setImage:[UIImage imageNamed:@"Check_touch_id.png"] forState:UIControlStateNormal];
    [btn_chck_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"] forState:UIControlStateNormal];
    
    [img_yes setImage:[UIImage imageNamed:@"touch_id_uncheck.png"]];
    [img_no setImage:[UIImage imageNamed:@"Check_touch_id.png"]];
    
    
    touch_id = @"0";
    
    gblclass.touch_id_enable=@"0";
    
}

-(IBAction)btn_back:(id)sender
{
    //    CATransition *transition = [ CATransition animation ];
    //    transition.duration = 0.3;
    //    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    //    transition.type = kCATransitionPush;
    //    transition.subtype = kCATransitionFromLeft;
    //    [ self.view.window. layer addAnimation:transition forKey:nil];
    //
    //    [self dismissViewControllerAnimated:YES completion:nil];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        _touchLogo.frame = CGRectMake((self.view.frame.size.width + _touchLogo.frame.size.width), _touchLogo.frame.origin.y, _touchLogo.frame.size.width, _touchLogo.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        
        _heading.frame = CGRectMake((self.view.frame.size.width + _heading.frame.size.width), _heading.frame.origin.y, _heading.frame.size.width, _heading.frame.size.height);
        
    }];
    
    [UIView animateWithDuration:0.8 animations:^{
        
        _yesNoView.frame = CGRectMake((self.view.frame.size.width + _yesNoView.frame.size.width), _yesNoView.frame.origin.y, _yesNoView.frame.size.width, _yesNoView.frame.size.height);
        
    }];
    
    //    [UIView animateWithDuration:0.8 animations:^{
    //
    //
    //        _yes.frame = yesFrame;
    //        _yesBtn.frame = yesBtnFrame;
    //        _no.frame = noFrame;
    //        _noBtn.frame = noBtnFrame;
    //
    //    }];
    
    
    [UIView animateWithDuration:0.9 animations:^{
        
        
        _nxt_btn.frame = CGRectMake((self.view.frame.size.width + _nxt_btn.frame.size.width), _nxt_btn.frame.origin.y, _nxt_btn.frame.size.width, _nxt_btn.frame.size.height);
        
        
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:0.4
                                     target:self
                                   selector:@selector(dissmiss)
                                   userInfo:nil
                                    repeats:NO];
    
    
}

-(void)dissmiss
{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(IBAction)btn_feature:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    //saki     [self custom_alert:Offer_msg :@""];
    
// [self peekabooSDK:@"deals"];  
    
}

-(IBAction)btn_find_us:(id)sender
{
    //saki     [self peekabooSDK:@"locator"];
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) Reg_Biometric_User:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    manager.securityPolicy = policy;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy = policy;
    
    UIDevice *deviceInfo = [UIDevice currentDevice];
    
    //NSLog(@"Device name:  %@", deviceInfo.name);
    
    
    NSString*  is_touch_login;
    NSString* user_name,* pass_word;
    
    if ([touch_id isEqualToString:@"1"])
    {
        is_touch_login=@"1";
    }
    else
    {
        is_touch_login=@"0";
    }
    
    
    if ([gblclass.sign_Up_chck isEqualToString:@"yes"])
    {
        user_name = gblclass.signup_username;
        pass_word = gblclass.signup_pw;
    }
    else
    {
        user_name = gblclass.sign_in_username;
        pass_word = gblclass.sign_in_pw;
    }
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"asdf"],
                                [encrypt encrypt_Data:@"abcd"],
                                [encrypt encrypt_Data:user_name],
                                [encrypt encrypt_Data:pass_word],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:is_touch_login],nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"LogBrowserInfo",
                                                                   @"hCode",
                                                                   @"strLoginName",
                                                                   @"strPassword",
                                                                   @"strDeviceID",
                                                                   @"Token",
                                                                   @"IsTouchLoign",nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"RegBiometricUser" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //              NSError *error;
              //              NSArray *arr = (NSArray *)responseObject;
              NSDictionary*  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              //login_status_chck=[dic objectForKey:@"Response"];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              
              if ([[dic objectForKey:@"Response"] integerValue]==0)
              {
                  
                  //gblclass.storyboardidentifer=@"password";
                  //***    [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  
                  // Remove Credential from Keychain ::
                  [self removeAllCredentialsForServer:@"login"];
                  
                  
                  //save in keychain ::
                  [self saveUsername:gblclass.sign_in_username withPassword:[dic objectForKey:@"Touch_token"] forServer:@"login"];
                  
                  
                  if (gblclass.touch_id_frm_menu)
                  {
                      
                      if ([touch_id isEqualToString:@"1"])
                      {
                          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                          [defaults setValue:@"1" forKey:@"tch_enable_chk"];
                          [defaults synchronize];
                          
                          [self touch_Yes];
                          NSString* yyy = [NSString stringWithFormat:@"%@ %@",tch_ID_name,@"has been enabled, Please login again."];
                         
                          [self showAlert:yyy :@"Attention!"];
                      }
                      else 
                      {
                          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                          [defaults setValue:@"0" forKey:@"tch_enable_chk"];
                          [defaults synchronize];
                          
                          [self touch_No];
                          NSString* yyy1 = [NSString stringWithFormat:@"%@ %@",tch_ID_name,@"has been disabled, Please login again."];
                          
                          [self showAlert:yyy1 :@"Attention!"];
                      }
                      
                      [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      
                      if ([self presentedViewController]) {
                          [[self presentedViewController] dismissViewControllerAnimated:NO completion:^{
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [self presentViewController:vc animated:NO completion:nil];
                              });
                          }];
                      } else {
                          [self presentViewController:vc animated:NO completion:nil];
                          
                      }
                      
                  }
                  else
                  {
                      
                      if ([touch_id isEqualToString:@"1"])
                      {
                          [self touch_Yes];
                        //  [self showAlert:@"Touch ID has been enabled, Please login again." :@"Attention!"];
                      }
                      else
                      {
                          [self touch_No];
                        //  [self showAlert:@"Touch ID has been disabled, Please login again." :@"Attention!"];
                      }
                      
                      [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"landing"];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
              else
              {
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        //[self mob_App_Logout:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"logout";
            [self SSL_Call];
        }
        
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ( [chk_ssl isEqualToString:@"review"])
        {
            chk_ssl=@"";
            [self Reg_Biometric_User:@""];
            
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self.connection cancel];
            [self mob_App_Logout:@""];
            
        }
        
        chk_ssl=@"";
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    
    gblclass.ssl_pin_check=@"1";
    
    if ( [chk_ssl isEqualToString:@"review"])
    {
        chk_ssl=@"";
        [self Reg_Biometric_User:@""];
        
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self.connection cancel];
        [self mob_App_Logout:@""];
        
    }
    
    chk_ssl=@"";
    [self.connection cancel];
}


-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //     // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  // NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
   // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString  :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}



-(void) getCredentialsForServer_touch:(NSString*)server
{
    @try {
        
        // Create dictionary of search parameters
        NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, server, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
        
        // Look up server in the keychain
        NSDictionary* found = nil;
        CFDictionaryRef foundCF;
        OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
        
        // Check if found
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return;
        
        // Found
        //NSLog(@"%@",[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding]);
        
        // user = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
        
        //   user=[[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecAttrAccount)] encoding:NSUTF8StringEncoding];
        
        tch_chk = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        NSString* gg = [NSString stringWithFormat:@"%@ Not Found.",tch_ID_name];
        [self custom_alert:gg :@"0"];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Touch Id Not Found." preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //
        //        [self presentViewController:alert animated:YES completion:nil];
        
        return ;
    }
    
}


- (void)peekabooSDK:(NSString *)type {
//    [self presentViewController:getPeekabooUIViewController(@{
//                                                              @"environment" : @"production",
//                                                              @"pkbc" : @"app.com.brd",
//                                                              @"type": type,
//                                                              @"country": @"Pakistan",
//                                                              @"userId": gblclass.peekabo_kfc_userid
//                                                              }) animated:YES completion:nil];
}



@end
