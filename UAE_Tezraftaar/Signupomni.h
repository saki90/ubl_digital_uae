//
//  Signupomni.h
//  ubltestbanking
//
//  Created by Yahya Zaki on 18/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface Signupomni : UIViewController<UITextFieldDelegate>
{
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) IBOutlet UITextField* txtemailaddress;
@property (nonatomic, strong) IBOutlet UITextField* txtomninumber;
@property (nonatomic, strong) IBOutlet UITextField* txtomnipin;


@end
