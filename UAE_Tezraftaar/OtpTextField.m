//
//  OtpTextField.m
//  ubltestbanking
//
//  Created by Asim Khan on 7/2/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "OtpTextField.h"


@implementation OtpTextField

-(void)deleteBackward {
    [super deleteBackward];
    
    if ([_OtpDelegate respondsToSelector:@selector(textFieldDidDelete:)]){
        [_OtpDelegate textFieldDidDelete:self];
    }
}



@end
