//
//  Transaction_detail.m
//
//  Created by   on 23/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Transaction_detail.h"
#import "OutdtDataset.h"


NSString *const kTransaction_detailResponse = @"Response";
NSString *const kTransaction_detailOutdtDataset = @"outdtDataset";
NSString *const kTransaction_detailOutdtData = @"outdtData";
NSString *const kTransaction_detailStrReturnMessage = @"strReturnMessage";


@interface Transaction_detail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Transaction_detail

@synthesize response = _response;
@synthesize outdtDataset = _outdtDataset;
@synthesize outdtData = _outdtData;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kTransaction_detailResponse fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kTransaction_detailOutdtDataset]];
            self.outdtData = [self objectOrNilForKey:kTransaction_detailOutdtData fromDictionary:dict];
            self.strReturnMessage = [self objectOrNilForKey:kTransaction_detailStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kTransaction_detailResponse];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kTransaction_detailOutdtDataset];
    [mutableDict setValue:self.outdtData forKey:kTransaction_detailOutdtData];
    [mutableDict setValue:self.strReturnMessage forKey:kTransaction_detailStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kTransaction_detailResponse];
    self.outdtDataset = [aDecoder decodeObjectForKey:kTransaction_detailOutdtDataset];
    self.outdtData = [aDecoder decodeObjectForKey:kTransaction_detailOutdtData];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kTransaction_detailStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kTransaction_detailResponse];
    [aCoder encodeObject:_outdtDataset forKey:kTransaction_detailOutdtDataset];
    [aCoder encodeObject:_outdtData forKey:kTransaction_detailOutdtData];
    [aCoder encodeObject:_strReturnMessage forKey:kTransaction_detailStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    Transaction_detail *copy = [[Transaction_detail alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
