//
//  Signup_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Signup_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Signup_VC ()<NSURLConnectionDataDelegate>
{
    NSArray* arr_act_type;
    NSString* Yourstring;
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    //   UIStoryboard *storyboard;
    //   UIViewController *vc;
    
    NSMutableArray* array_country;
    NSMutableArray* pro_countryid,*countryname;
    NSString* countrycode;
    NSArray *acctype;
    UIAlertController* alert;
    
    UIStoryboard *mainStoryboard;
    UIViewController *vc;
    Encrypt *encrypt;
    
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end


@implementation Signup_VC
//@synthesize web;

- (void)viewDidLoad
{
    arr_act_type=[[NSArray alloc] init];
    //self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    table.delegate=self;
    
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    array_country =  [[NSMutableArray alloc]init];
    countryname =  [[NSMutableArray alloc]init];
    
    lbl_heading.hidden=YES;
    
    pro_countryid =  [[NSMutableArray alloc]init];
    arr_act_type= [[NSMutableArray alloc]init];
    acctype = @[@"AccInfo",@"ORION",@"CreditCard",@"AccInfo"];
    // [self GetCountry: @""];
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView==_countrytableview){
        if([array_country count]==0){
            return 0;
        }
        return[array_country count];
    }
    else{
        if([arr_act_type count]==0){
            return 0;
        }
        else{
            return [arr_act_type count];    //count number of row from counting array hear cataGorry is An Array
        }
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    if(tableView==_countrytableview){
        NSDictionary *dic = [array_country objectAtIndex:indexPath.row];
        
        
        //cell.textLabel.text =
        [countryname addObject:[dic objectForKey:@"PROD_COUNTRY_DESC"]];
        [pro_countryid addObject:[dic objectForKey:@"PROD_COUNTRY_ID"]];
        cell.textLabel.text=[countryname objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        
        NSDictionary *dic = [arr_act_type objectAtIndex:indexPath.row];
        
        
        cell.textLabel.text = [dic objectForKey:@"BANK_TYPE_DESC"];
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        
        // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    _countryview.hidden=true;
    if(tableView==self.countrytableview)
    {
        gblclass.signup_countrycode=[pro_countryid objectAtIndex:indexPath.row];
        
        lbl_heading.hidden=NO;
        
        countrycode=[pro_countryid objectAtIndex:indexPath.row];
        self.txtcountryname.text=[countryname objectAtIndex:indexPath.row];
        [self GetCountryProduct:@""];
    }
    else
    {
        gblclass.signup_acctype=[acctype objectAtIndex:indexPath.row];
        
        
        //NSLog(@"%@",[acctype objectAtIndex:indexPath.row]);
        
        switch (indexPath.row)
        {
            case 0:
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"signupdetail"];
                [self presentViewController:vc animated:NO completion:nil];
                break;
            case 1:
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"signupomni"];
                [self presentViewController:vc animated:NO completion:nil];
                break;
            case 2:
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"signupcc"];
                [self presentViewController:vc animated:NO completion:nil];                break;
            case 3:
                
                mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"signupdetail"];
                [self presentViewController:vc animated:NO completion:nil];
                break;
            default:
                break;
        }
        
        
    }
    
    
    
}
- (IBAction)btn_dd_country:(UIButton *)sender
{
    
    if(self.countryview.hidden)
    {
        self.countryview.hidden=false;
    }
    else
    {
        self.countryview.hidden=true;
    }
    
    
}

//-(IBAction)btn_next:(id)sender
//{
//
//    // [self performSegueWithIdentifier:@"act_summary" sender:self];
//    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//    //  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Signup_detail"];
//    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"signupdetail"];
//    [self presentModalViewController:vc animated:YES];
//}



-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) GetCountry:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:@"0"],
                                [encrypt encrypt_Data:@"1234"],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"skjdaf"], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"GetCountry" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              // companytype = [(NSDictionary *)[dic objectForKey:@"Response"] objectForKey:@"Table1"];
              //ttid=
              NSString* responsecode = [dic objectForKey:@"Response"];
              if ([responsecode isEqualToString:@"0" ]){
                  
                  array_country =
                  [(NSDictionary *)[dic objectForKey:@"dtCountryList"] objectForKey:@"Table"];
                  
                  
              }else{
                  //                  NSString* resstring =(NSDictionary *)[dic objectForKey:@"strReturnMessage"];
                  //                  [self showAlert:resstring :@"Attention"];
              }
              //NSLog(@"Done IBFT load branch");
              //[self parsearray];
              [_countrytableview reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}


-(void) GetCountryProduct:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    
    //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"0",@"1234",[GlobalStaticClass getPublicIp],@"skjdaf",countrycode, nil] forKeys:[NSArray arrayWithObjects:@"strUserId",@"strSessionId",@"IP",@"hCode",@"strCountry", nil]];
    
    //NSLog(@"%@",dictparam);
    
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetCountryProduct" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              // companytype = [(NSDictionary *)[dic objectForKey:@"Response"] objectForKey:@"Table1"];
              //ttid=
              NSString* responsecode = [dic objectForKey:@"Response"];
              if ([responsecode isEqualToString:@"0" ]){
                  
                  arr_act_type =
                  [(NSDictionary *)[dic objectForKey:@"dtCountryProductList"] objectForKey:@"Table"];
                  
                  
              }else
              {
                  //                  NSString* resstring =(NSDictionary *)[dic objectForKey:@"strReturnMessage"];
                  //                  [self showAlert:resstring :@"Attention"];
              }
              
              //NSLog(@"Done IBFT load branch");
              //[self parsearray];
              [table reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}

-(IBAction)btn_feature:(id)sender
{
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
    [self presentViewController:vc animated:NO completion:nil];
}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    {
        
        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            [self GetCountry: @""];
            
            [self.connection cancel];
        }
        else
        {
            [self.connection cancel];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Authentication Failed."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self.connection cancel];
        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        
        gblclass.ssl_pin_check=@"0";
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"SSL Not Verified." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        return;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    return [envValue boolValue];
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}



@end
