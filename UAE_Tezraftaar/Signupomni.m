//
//  Signupomni.m
//  ubltestbanking
//
//  Created by Yahya Zaki on 18/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Signupomni.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import "Encrypt.h"

@interface Signupomni ()<NSURLConnectionDataDelegate>
{
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
    NSString*  strCountry,*strTxtEmail,*strTxtKeyword,*strBranchCode,*strTxtAccountNumber,*strCardPin,*strAccType;
    
    NSMutableArray* banktype;
    NSMutableArray* branchname;
    NSMutableArray* searchbranchname;
    NSMutableArray* branchcode;
    NSString* atmavailable;
    UIAlertController* alert;
    Encrypt *encrypt;
    
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Signupomni

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    
    
    strCountry=gblclass.signup_countrycode;
    strAccType=gblclass.signup_acctype;
    strBranchCode=@"0";
    strCardPin=@"";
    
    self.txtomnipin.delegate=self;
    self.txtomninumber.delegate=self;
    self.txtemailaddress.delegate=self;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) UserMobReg:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
   [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    //680801
    //    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    //NSLog(@"strCountry%@",strCountry);
    //NSLog(@"strTxtEmail%@",strTxtEmail);
    //NSLog(@"strTxtKeyword%@",strTxtKeyword);
    //NSLog(@"strBranchCode%@",strBranchCode);
    //NSLog(@"strTxtAccountNumber%@",strTxtAccountNumber);
    //NSLog(@"strCardPin%@",strCardPin);
    //NSLog(@"strAccType%@",strAccType);
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:@"0",
                                @"1234",
                                [GlobalStaticClass getPublicIp],
                                @"abv",
                                strCountry,
                                strTxtEmail,
                                strTxtKeyword,
                                strBranchCode,
                                strTxtAccountNumber,
                                strCardPin,
                                strAccType, nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode",
                                                                   @"strCountry",
                                                                   @"strTxtEmail",
                                                                   @"strTxtKeyword",
                                                                   @"strBranchCode",
                                                                   @"strTxtAccountNumber",
                                                                   @"strCardPin",
                                                                   @"strAccType", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"UserMobReg" parameters:dictparam progress:nil
 
          success:^(NSURLSessionDataTask *task, id responseObject) {
 //             NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              // companytype = [(NSDictionary *)[dic objectForKey:@"Response"] objectForKey:@"Table1"];
              //ttid=
  
              NSString* responsecode =(NSDictionary *)[dic objectForKey:@"Response"];
              
              if ([responsecode isEqualToString:@"0" ]){
                  
                  gblclass.signup_relationshipid=(NSDictionary *)[dic objectForKey:@"strRelationShipId"];
                  
                  UIStoryboard *mainStoryboard;
                  UIViewController *vc;
                  
                  // [self performSegueWithIdentifier:@"act_summary" sender:self];
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"verification_acc"];
                  [self presentViewController:vc animated:NO completion:nil];
              }
              else{
                  NSString* exe= (NSDictionary *)[dic objectForKey:@"strReturnMessage"];
                  [self showAlert:exe:@"Attention"];
              }
              
              //[self parsearray];
              //  [_countrytableview reloadData];
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}
-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                       message:exception
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                       [alert2 show];
                   });
    //[alert release];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)textFieldDidChange:(UITextField *)theTextField
{
    
    //NSLog(@"text changed: %@", theTextField.text);
    NSString *textFieldText = [theTextField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //  [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *formattedOutput = [formatter stringFromNumber:[NSNumber numberWithInt:[textFieldText integerValue]]];
    
    
    if ([theTextField isEqual:self.txtomnipin])
    {
        self.txtomnipin.text=formattedOutput;
    }
    else if ([theTextField isEqual:self.txtomninumber])
    {
        self.txtomninumber.text=formattedOutput;
    }
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //   autocompleteTableView.hidden = NO;
    // if([_branchname.text length]>3){
    
    //}
    
//    NSInteger MAX_DIGITS;
//    BOOL stringIsValid;

    
    
   if ([textField isEqual:self.txtomnipin])
    {
        NSInteger MAX_DIGITS=12;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
    }
    else if ([textField isEqual:self.txtomninumber])
    {
        NSInteger MAX_DIGITS=12;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        
    }
    else if ([textField isEqual:self.txtemailaddress])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789._@"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    return YES;
}



-(IBAction)btn_next:(id)sender
{
    
    if ([self.txtemailaddress.text isEqualToString:@""] || [self.txtemailaddress.text isEqualToString:@" "])
    {
        NSString* exe=@"Enter Email Address";
        [self showAlert:exe:@"Attention"];
        return ;
    }
    else  if (![self validateEmail:[self.txtemailaddress text]])
    {
        NSString* exe=@"Please enter valid email address";
        [self showAlert:exe:@"Attention"];
        return ;
    }
    else if ([self.txtomninumber.text isEqualToString:@""] || [self.txtomninumber.text isEqualToString:@" "])
    {
        NSString* exe=@"Enter Omni Number";
        [self showAlert:exe:@"Attention"];
        return ;
    }
    else if ([self.txtomnipin.text isEqualToString:@""] || [self.txtomnipin.text isEqualToString:@" "])
    {
        NSString* exe=@"Enter Omni Number";
        [self showAlert:exe:@"Attention"];
        return ;
    }
//    else if ([self.txtomninumber.text isEqualToString:@""])
//    {
//        NSString* exe=@"Enter Omni Number";
//        [self showAlert:exe:@"Attention"];
//        return ;
//    }
    
    
    strTxtEmail=self.txtemailaddress.text;
    strTxtKeyword=@"";
    strTxtAccountNumber=self.txtomninumber.text;
    strCardPin = self.txtomnipin.text;
    
//    [self UserMobReg:@""];
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
}


#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self.txtemailaddress resignFirstResponder];
    [self.txtomninumber resignFirstResponder];
    [self.txtomnipin resignFirstResponder];
    
}

- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    {
        
        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            [self UserMobReg:@""];
            
             [self.connection cancel];
        }
        else
        {
            
             [self.connection cancel];
            
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                            message:@"Authentication Failed."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                        [alertView show];
            
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self.connection cancel];
        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        
        gblclass.ssl_pin_check=@"0";
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"SSL Not Verified." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];

        
        
        return;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


@end
