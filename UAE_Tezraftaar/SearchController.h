//
//  SearchController.h
//  Nafa
//
//  Created by Abdul Basit on 06/09/2017.
//  Copyright © 2017 Abdul Basit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchCustomDelegate <NSObject>

-(void)didSelectCityOrBranch: (NSInteger) selectedItem type: (NSString*)type;

@end

@interface SearchController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate>

@property (nonatomic,assign) id delegate;
@property (nonatomic,strong) NSArray* searchItems;
@property (nonatomic,strong) NSString *type;

@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
- (IBAction)back_Action:(id)sender;
- (IBAction)cancel_Action:(id)sender;

@end

