//
//  GnbTranansactionHistory.h
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbTranansactionHistory : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *transactionId;
@property (nonatomic, strong) NSString *transactionDate;
@property (nonatomic, strong) NSString *tCACCESSKEY;
@property (nonatomic, strong) NSString *trandisplayId;
@property (nonatomic, strong) NSString *ttName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
