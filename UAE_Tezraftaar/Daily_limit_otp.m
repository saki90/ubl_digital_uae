//
//  Daily_limit_otp.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/04/2019.
//  Copyright © 2019 ammar. All rights reserved.
//

#import "Daily_limit_otp.h"
#import "MBProgressHUD.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Daily_limit_otp ()<NSURLConnectionDataDelegate>
{
    MBProgressHUD *hud;
    GlobalStaticClass* gblclass;
    NSString* responsecode;
    UIAlertController* alert;
    NSString* str_Otp;
    NSString* alert_chck;
    NSMutableArray* arr_pin_pattern,*arr_pw_pin;
    NSString* str_pw;
    int txt_nam;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    NSString* chk_service_call;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation Daily_limit_otp
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    gblclass =  [GlobalStaticClass getInstance];
    self.transitionController = [[TransitionDelegate alloc] init];
    encrypt = [[Encrypt alloc] init];
    lbl_heading.text = @"OTP AUTHENTICATION"; //gblclass.bill_type;
    
    //[NSString stringWithFormat:@"ADD %@",gblclass.acct_add_type]; //
    //gblclass.Mobile_No
    
    chk_service_call = @"0";
    ssl_count = @"0";
    txt_1.delegate=self;
    txt_2.delegate=self;
    txt_3.delegate=self;
    txt_4.delegate=self;
    txt_5.delegate=self;
    txt_6.delegate=self;
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    // Set textfield cursor color ::
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
  
    //To add a new payee via smartphone a four digits One Time Passcode is sent to your mobile number *******3456 via SMS. Please enter the sent code below.
    
    NSString *subStr ;//= [str substringWithRange:NSMakeRange(0, 20)];
    
//    NSLog(@"%@", gblclass.Mobile_No);
    alert_chck=@"";
    
    if ([gblclass.Mobile_No length]>0)
    {
        subStr = [gblclass.Mobile_No substringWithRange:NSMakeRange(8,4)];
    }
    
    lbl_text.text=[NSString stringWithFormat:@"A six digit One Time Password (OTP) is sent to your mobile number *******%@ via SMS. Please enter the code below.",subStr];
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    
}


-(IBAction)btn_resend_otp:(id)sender
{
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"re_send_otp";
        [self SSL_Call];
    }
}

-(IBAction)btn_submit:(id)sender
{
    
    if ([txt_1.text isEqualToString:@""] || [txt_2.text isEqualToString:@""] || [txt_3.text isEqualToString:@""] || [txt_4.text isEqualToString:@""] || [txt_5.text isEqualToString:@""] || [txt_6.text isEqualToString:@""])
    {
        
        [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
        return;
    }
    
    str_Otp=@"";
    str_Otp = [str_Otp stringByAppendingString:txt_1.text];
    str_Otp = [str_Otp stringByAppendingString:txt_2.text];
    str_Otp = [str_Otp stringByAppendingString:txt_3.text];
    str_Otp = [str_Otp stringByAppendingString:txt_4.text];
    str_Otp = [str_Otp stringByAppendingString:txt_5.text];
    str_Otp = [str_Otp stringByAppendingString:txt_6.text];
    
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl = @"submit";
        [self SSL_Call];
    }
    
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
        //NSLog(@"%ld",(long)buttonIndex);
        if (buttonIndex == 0 && [alert_chck isEqualToString:@"1"])
        {
    
            alert_chck = @"0";
//            NSLog(@"%@",gblclass.story_board);
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:vc animated:YES completion:nil];
            
            //[self mob_App_Logout:@""];
    
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl=@"logout";
//                [self SSL_Call];
//            }
    
    
    
            //Do something
            //NSLog(@"1");
        }
        else if(buttonIndex == 1)
        {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
            [self presentViewController:alert animated:YES completion:nil];
        }
    
    
//    if ([alert_chck isEqualToString:@"0"])
//    {
//
//        [ alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
//
//        if(buttonIndex ==0)
//        {
//
//            if([responsecode isEqualToString:@"0"])
//            {
//
//                if([gblclass.add_bill_type isEqualToString:@"UBL Cashline"]||([gblclass.add_bill_type isEqualToString:@"UBL Credit Card"]))
//                {
//
//        //            [self AddConfirmBillDetail:@""];
//
//                }
//                else
//                {
//
//                }
//            }
//            else
//            {
//                responsecode=@"";
//            }
//        }
//
//    }
//    else if ([alert_chck isEqualToString:@"1"])
//    {
//        if (buttonIndex == 0)
//        {
//            alert_chck = @"";
//            [self checkinternet];
//            if (netAvailable)
//            {
//                chk_ssl=@"logout";
//                [self SSL_Call];
//            }
//        }
//        else if(buttonIndex == 1)
//        {
//            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//            vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
//            [self presentViewController:vc animated:NO completion:nil];
//        }
//    }
//    else
//    {
//
//    }
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    //[alert release];
}

-(void) showAlertwithcancel:(NSString *)exception : (NSString *) Title{
    
    alert_chck=@"0";
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:Title
                                                                        message:exception
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:@"Cancel",nil];
                       [alert2 show];
                   });
    //[alert release];
}



//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//
//    //NSLog(@"%ld",(long)buttonIndex);
//    if (buttonIndex == 0)
//    {
//
//        //[self mob_App_Logout:@""];
//
//        [self checkinternet];
//        if (netAvailable)
//        {
//            chk_ssl=@"logout";
//            [self SSL_Call];
//        }
//
//
//
//        //Do something
//        //NSLog(@"1");
//    }
//    else if(buttonIndex == 1)
//    {
//        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
//}


-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //   // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  [hud hideAnimated:YES];
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
    }
}


-(void) clearOTP
{
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}



-(void)slideLeft
{
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
}

-(void)slideRight
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
}


-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger MAX_DIGITS = 1;
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
            }
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
//            NSLog(@"%@",nextResponder);
//            NSLog(@"%@",[textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]);
            
//            if (nextResponder)
//            {
//                [textField resignFirstResponder];
//                [nextResponder becomeFirstResponder];
//            }
            
            
            if (textField.tag == 6)
            {
//                [hud showAnimated:YES];
//                [self.view addSubview:hud];
//                [hud showAnimated:YES];
//
//                //  textField.tag = [@"7" integerValue];
//
//
//                txt_6.enabled = NO;
//                txt_6.text=string;
//
//                str_Otp=@"";
//                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
//                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
//                NSLog(@"**** ALERT ***** SIX");
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                [hud showAnimated:YES];
                [hud hideAnimated:YES afterDelay:130];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
//                NSLog(@"**** ALERT ***** SERVICE CALL");
                
                
                chk_ssl = @"submit";
                [self SSL_Call];
                
               // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                 return 0;
            }
            
            textField = nil;
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
    return YES;
}




-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}


-(void) Change_daily_limit:(NSString *)strIndustry
{
    
    @try {
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.signup_relationshipid);
        
        //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"7c4d5f254b9e6edd",txt_pin.text,@"1234",[GlobalStaticClass getPublicIp],@"asdf", nil]
        //                                                          forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
        
//        new_pw  = [txt_amount.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        retype_pw  = [txt_atm_card.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        old_pw  = [txt_atm_pin.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
//        NSLog(@"%@",gblclass.arr_daily_limit);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:[gblclass.arr_daily_limit objectAtIndex:1]],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:[gblclass.arr_daily_limit objectAtIndex:0]],
                                    [encrypt encrypt_Data:@"0"],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strCardno",//monthly
                                                                       @"ATMPin",
                                                                       @"Limit",//daily
                                                                       @"strAtmInvalidAttempts",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"ChangeLimit" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //           NSError *error;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  responsecode=[dic objectForKey:@"Response"];
                  
                 // NSString* exception =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
                  
                  NSString* exception = [dic objectForKey:@"strReturnMessage"];
                  if (exception ==(NSString *)[NSNull null])
                  {
                      exception = @"";
                  }
                  
                  if ([responsecode isEqualToString:@"0"])
                  {
                      [hud hideAnimated:YES];
                      
//                      gblclass.daily_limit=txt_amount.text;
//                      lbl_current_daily_limit.text=txt_amount.text;
//
//                      txt_amount.text=@"";
//                      txt_atm_pin.text=@"";
//                      txt_atm_card.text=@"";
                      
                      gblclass.daily_limit = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_daily_limit objectAtIndex:0]];
                      gblclass.monthly_limit = [NSString stringWithFormat:@"%@ %@",gblclass.base_currency,[gblclass.arr_daily_limit objectAtIndex:1]];
                      
                    //  [self slide_right];
                      [self clearOTP];
                      storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                      vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"]; //landing
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                       message:exception
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                      [alert1 show];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      alert_chck = @"1";
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                      return;
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      txt_6.enabled = YES;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  [hud hideAnimated:YES];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error)
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[error localizedDescription] :@"0"];
              }];
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
}


-(void) Re_GenerateOTP:(NSString *)strIndustry
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1//otpurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
//    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,gblclass.fetchtitlebranchcode,gblclass.acc_number,@"CHANGELIMIT",@"Generate OTP",@"0",@"CHANGELIMIT",@"MOB", nil]];
    
    
//    NSLog(@"%@", gblclass.arr_re_genrte_OTP_additn);
//    NSLog(@"%@",[gblclass.arr_re_genrte_OTP_additn objectAtIndex:4]);
    
    gblclass.fetchtitlebranchcode = @"0";
    gblclass.acc_number = @"0";
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:gblclass.fetchtitlebranchcode],
                                [encrypt encrypt_Data:gblclass.acc_number],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:@"0"],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"CHANGELIMIT"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key",nil]];
    
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //           NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //NSLog(@"Done IBFT load branch");
              responsecode = [dic objectForKey:@"Response"];
              NSString* responsemsg = [dic objectForKey:@"strReturnMessage"];
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                   [self clearOTP];
                  [self custom_alert:responsemsg :@"1"];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  alert_chck = @"1";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:responsemsg :@"0"];
              }
              
              [hud hideAnimated:YES];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [hud hideAnimated:YES];\
         [self custom_alert:[error localizedDescription]  :@"0"];
         
     }];
}



///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"re_send_otp"])
        {
            chk_ssl=@"";
            [self Re_GenerateOTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"submit"])
        {
            chk_ssl=@"";
            [self Change_daily_limit:@""];
        }
    }
    else
    {
        //
        //        if ([chk_service_call isEqualToString:@"1"])
        //        {
        
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
        //        }
        
    }
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    chk_service_call = @"0";
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    [self.connection cancel];
    
    if ([chk_ssl isEqualToString:@"re_send_otp"])
    {
        chk_ssl=@"";
        [self Re_GenerateOTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"submit"])
    {
        chk_ssl=@"";
        [self Change_daily_limit:@""];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
//    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end
