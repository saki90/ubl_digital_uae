//
//  Atm_Card_Management.h
//  ubltestbanking
//
//  Created by ammar on 26/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"


@interface Atm_Card_Management  : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) IBOutlet UITextField* txtcardname;
@property (nonatomic, strong)   IBOutlet UITextField* txtcardnumber;
@property (nonatomic, strong)  IBOutlet UITextField* txtcardtype;
@property (nonatomic, strong)  IBOutlet UITextField* txtcardstatus;

@property (nonatomic, strong)  IBOutlet UIView* cardlist;
@property (nonatomic, strong)  IBOutlet UITableView* cardtableview;
@property (nonatomic, strong) TransitionDelegate *transitionController;


-(IBAction)btn_req_atm:(id)sender;

@end
