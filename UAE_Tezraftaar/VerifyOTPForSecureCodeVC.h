//
//  VerifyOTPForSecureCodeVC.h
//  ubltestbanking
//
//  Created by Asim Khan on 7/20/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "OtpTextField.h"

@interface VerifyOTPForSecureCodeVC : UIViewController <OtpTextFieldDelegate,UITextFieldDelegate>
{
    IBOutlet OtpTextField* txt_1;
    IBOutlet OtpTextField* txt_2;
    IBOutlet OtpTextField* txt_3;
    IBOutlet OtpTextField* txt_4;
    IBOutlet OtpTextField* txt_5;
    IBOutlet OtpTextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    IBOutlet UIButton* btn_resend_otp;
    
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end
