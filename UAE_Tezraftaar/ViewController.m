 
#import "ViewController.h"
#import "NSString+Encryption.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *cypherString = [NSString encryptString:@"Hello World" withKey:@"SkDd9k88Ss7D9S9DSkDd9k88Ss7D9S9D"];
    
    NSLog(@"cypherString :  %@", cypherString);
    
    NSString *decrptedstring = [NSString decryptData:cypherString withKey:@"SkDd9k88Ss7D9S9DSkDd9k88Ss7D9S9D"];
    
    NSLog(@"Decrypted String : %@",decrptedstring);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
