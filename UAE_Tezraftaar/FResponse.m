//
//  FResponse.m
//
//  Created by   on 14/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "FResponse.h"


NSString *const kFResponseResponseCode = @"ResponseCode";


@interface FResponse ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FResponse

@synthesize responseCode = _responseCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.responseCode = [self objectOrNilForKey:kFResponseResponseCode fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.responseCode forKey:kFResponseResponseCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.responseCode = [aDecoder decodeObjectForKey:kFResponseResponseCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_responseCode forKey:kFResponseResponseCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    FResponse *copy = [[FResponse alloc] init];
    
    if (copy) {

        copy.responseCode = [self.responseCode copyWithZone:zone];
    }
    
    return copy;
}


@end
