//
//  Daily_limit_otp.h
//  ubltestbanking
//
//  Created by Jahangir Mirza on 09/04/2019.
//  Copyright © 2019 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

NS_ASSUME_NONNULL_BEGIN

@interface Daily_limit_otp : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel* lbl_heading;
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    IBOutlet UILabel* lbl_text;
    
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
 
@end

NS_ASSUME_NONNULL_END
