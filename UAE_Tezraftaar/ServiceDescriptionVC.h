//
//  CurrentAccountVC.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 20/11/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDescriptionVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *serviceTitle;
@property (strong, nonatomic) NSDictionary *serviceData;
@property (strong, nonatomic) IBOutlet UILabel *serviceDescription;
@property (strong, nonatomic) IBOutlet UILabel *serviceKeyBenefits;

@end

