//
//  VisitArrangmentVC.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 06/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VisitArrangmentVC : UIViewController

@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) NSMutableArray *rmTimings;
@property (strong,nonatomic) NSMutableArray *rmDataArray;

@end


