//
//  SecureCodeSignupVC.m
//  ubltestbanking
//
//  Created by Asim Khan on 7/10/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "SecureCodeSignupVC.h"
#import "GlobalStaticClass.h"
#import "GlobalClass.h"
#import "AFHTTPSessionManager.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"

@interface SecureCodeSignupVC () {
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD *hud;
    NSString *chk_ssl;
    Encrypt *encrypt;
    NSString *ssl_count;
    NSUserDefaults *defaults;
    NSString* t_n_c,* device_jb_chk;
    int viewStackCount;
    NSString* first_time_chk;
}

@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation SecureCodeSignupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    encrypt = [[Encrypt alloc] init];
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    // Cheks the jail break status.
    [self checkJailbreakStatus];
    
    //
    [self Play_bg_video];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    viewStackCount = 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    _avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:_avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    if ([_avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [_avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [_avplayer seekToTime:kCMTimeZero];
    [_avplayer setVolume:0.0f];
    [_avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [_avplayer play];
}


- (IBAction)btn_next:(id)sender {
    
    gblclass.parent_vc_onbording = @"SecureCodeSignupVC";
    
    if ([self.cnicNumber.text isEqualToString:@""]) {
        [self custom_alert:@"Please enter NADRA CNIC number" :@"0"];
        return ;
        
    } else if ([self.cnicNumber.text length] < 13) {
        [self custom_alert:@"Invalid NADRA CNIC number" :@"0"];
        return ;
        
    } else if ([self.secureCode.text isEqualToString:@""]) {
        [self custom_alert:@"Please enter secure code" :@"0"];
        return ;
        
    } else if ([self.secureCode.text length] < 12) {
        [self custom_alert:@"Invalid secure code" :@"0"];
        return ;
        
    } else {
        
        gblclass.cnic = self.cnicNumber.text;
        gblclass.secureCode = self.secureCode.text;

//        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
//        [self presentViewController:vc animated:NO completion:nil];
//        [self slide_right];
        
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"MobileAppSecureCodeVerify";
            [self SSL_Call];
        }
        
    }
    
}

-(IBAction)btn_back:(id)sender
{
    [self slide_left];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SecureCodeOrProfileInfoViewController"];
    [self presentViewController:vc animated:NO completion:nil];
    
//    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification {
    NSLog(@"%@",notification);
}

- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger MAX_DIGITS=13;
    
    if ([theTextField  isEqual:self.cnicNumber]) {
        MAX_DIGITS = 13;//13
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                [self custom_alert:@"NADRA CNIC number should not start with zero." :@"0"];
                return NO;
                
            } else if (![myCharSet characterIsMember:c]) {
                return NO;
            } else {
                
                //                if(textField.tag == 2) {
                //                    let currentCharacterCount = textField.text?.count ?? 0
                //                    if (range.length + range.location > currentCharacterCount){
                //                        return false
                //                    }
                //
                //                    if(textField.text?.count == 5 && string.count == 1){
                //                        textField.text = textField.text! + "-" + string
                //
                //
                //                        return false
                //                    }else if(textField.text?.count == 13 && string.count == 1){
                //                        textField.text = textField.text! + "-" + string
                //
                //                        return false
                //                    }
                //
                //                    if(textField.text?.count == 7 && string.count == 0){
                //
                //                        let firstIndex = textField.text?.index((textField.text?.startIndex)!, offsetBy: 6)
                //
                //                        textField.text?.remove(at: firstIndex!)
                //
                //                    }else if(textField.text?.count == 15 && string.count == 0){
                //
                //                        let firstIndex = textField.text?.index((textField.text?.startIndex)!, offsetBy: 14)
                //
                //                        textField.text?.remove(at: firstIndex!)
                //
                //                    }
                //                    let newLength = currentCharacterCount + string.count - range.length
                //                    return newLength <= 15
                //                }
                
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
    }
    
    if ([theTextField isEqual:self.secureCode])
    {
        MAX_DIGITS = 12;
        
//        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
//        for (int i = 0; i < [string length]; i++)
//        {
//            unichar c = [string characterAtIndex:i];
//            if (![myCharSet characterIsMember:c])
//            {
//                return NO;
//            }
//            else
//            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
//            }
//        }
    }
    
    return YES;
}



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            [hud hideAnimated:YES];
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [self custom_alert:statusString :@"0"];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach {
    [self setdelegfornet:curReach];
}


//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


///////////************************** SSL PINNING START ******************************////////////////


- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call {
    
    NSLog(@"%@",gblclass.mainurl1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]) {
        
        if ([chk_ssl isEqualToString:@"MobileAppSecureCodeVerify"]) {
            chk_ssl=@"";
            [self mobileAppSecureCodeVerify:@""];
        }
        
    } else {
       
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    gblclass.ssl_pin_check=@"1";
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"MobileAppSecureCodeVerify"]) {
        chk_ssl=@"";
        [self mobileAppSecureCodeVerify:@""];
    }
    
    
}


- (BOOL)isSSLPinning
{
    
    //    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (self.responseData == nil) {
        self.responseData = [NSMutableData dataWithData:data];
    } else {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    }
//    else if (error.code == NSURLErrorNotConnectedToInternet) {
//        [self custom_alert:@"Internet Access Not Available" :@"0"];
//    }
    //    NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
    NSLog(@"%@", message);
}


- (void)checkJailbreakStatus {
    t_n_c = [defaults valueForKey:@"t&c"];
    device_jb_chk = [defaults valueForKey:@"isDeviceJailBreaked"];
    
    if ([t_n_c isEqualToString:@"2"]) {
        t_n_c = @"1";
    }
    
    if ([device_jb_chk isEqualToString:@"2"]) {
        device_jb_chk = @"1";
    }
}



- (void)mobileAppSecureCodeVerify:(NSString *)strIndustry {
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects: [encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:@"iOS"],
                                    [encrypt encrypt_Data:device_jb_chk],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:self.cnicNumber.text],
                                    [encrypt encrypt_Data:self.secureCode.text],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects: @"strSessionId",
                                                                       @"IP",
                                                                       @"strDeviceID",
                                                                       @"LogBrowserInfo",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"strCNIC",
                                                                       @"strSecureCode", nil]];
        
        // Input
//        @"strSessionId"
//        @"IP"
//        @"LogBrowserInfo"
//        @"isRooted"
//        @"isTncAccepted"
//        @"strDeviceID"
//        @"strCNIC"
//        @"strSecureCode"
//        
//        // output
//        @"struserId"
//        @"strRetVal"
//        @"strReturnMessage"
//        @"Token"
//        @"strParaSessionID"
//        @"relationshipID"
        
//        Service Response:
//         0    Successful
//        -1    Secure code could not be verified
//        -2    UserException
//        -3    Exception
//        -4    Invalid CNIC
//        -77   Token generation failed
        
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"MobileAppSecureCodeVerify" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      
                      NSString* stepedVC;
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue] == 0) {
                      
//                      [hud hideAnimated:YES];
                      gblclass.cnic = self.cnicNumber.text;
                      gblclass.user_id = [dic objectForKey:@"struserId"];
                      //gblclass.retValue = [dic objectForKey:@"strRetVal"];
                      gblclass.token = [dic objectForKey:@"Token"];
                      gblclass.M3sessionid = [dic objectForKey:@"strParaSessionID"];
                      gblclass.relation_id = [dic objectForKey:@"relationshipID"];

                      [self ShowTermCondition:@""];
                      
//                      [self generateOTPForAddition:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue] == -1 ||
                             [[dic objectForKey:@"Response"] integerValue] == -2 ||
                             [[dic objectForKey:@"Response"] integerValue] == -3 ||
                             [[dic objectForKey:@"Response"] integerValue] == -4 ||
                             [[dic objectForKey:@"Response"] integerValue] == -77 ) {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

- (void)ShowTermCondition:(NSString *)strIndustry
{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:device_jb_chk], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                   @"IP",
                                                                   @"isRooted", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"ShowTermCondition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              NSString* responsecode =[NSString stringWithFormat:@"%@",[dic objectForKey:@"Response"]];
              [hud hideAnimated:YES];
              
              if([responsecode isEqualToString:@"0"]) {
                  gblclass.jb_chk_login_back = @"2";
                  gblclass.termsAndconditions = [dic objectForKey:@"TermNConditionText"];
                  [self slide_right];
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"JB_TermNCondition"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }  else {
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              [hud hideAnimated:YES];
              [self custom_alert:[error localizedDescription] :@"0"];
              
          }];
}

-(void)generateOTPForAddition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:gblclass.cnic],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"NEWSIGNUP"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userId",
                                                                       @"strSessionId",
                                                                       @"M3Key",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strBranchCode",
                                                                       @"strAccountNo",
                                                                       @"strAccessKey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        
        [manager.requestSerializer setTimeoutInterval: time_out];
        [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifySecureCodeOTP"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

//- (void)mobileAppSecureCodeVerifyStep2:(NSString *)strIndustry {
//
//    @try {
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.M3sessionid],
//                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                    [encrypt encrypt_Data:@"iOS"],
//                                    [encrypt encrypt_Data:device_jb_chk],
//                                    [encrypt encrypt_Data:t_n_c],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:gblclass.user_id],
//                                    [encrypt encrypt_Data:gblclass.token],
//                                    [encrypt encrypt_Data:self.otp.text],
//                                    [encrypt encrypt_Data:gblclass.cnic],
//                                    [encrypt encrypt_Data:gblclass.relation_id],  nil]
//
//                                                              forKeys:[NSArray arrayWithObjects: @"strSessionId",
//                                                                       @"IP",
//                                                                       @"LogBrowserInfo",
//                                                                       @"isRooted",
//                                                                       @"isTncAccepted",
//                                                                       @"strDeviceID",
//                                                                       @"StrUserId",
//                                                                       @"Token",
//                                                                       @"strOTPPIN",
//                                                                       @"strCNIC",
//                                                                       @"strRelationshipId", nil]];
//
//        // Input
//        //        @"strSessionId"
//        //        @"IP"
//        //        @"LogBrowserInfo"
//        //        @"isRooted"
//        //        @"isTncAccepted"
//        //        @"strDeviceID"
//        //        @"StrUserId"
//        //        @"Token"
//        //        @"strOTPPIN"
//        //        @"strCNIC"
//        //        @"strRelationshipId"
//
//        // output
//        //        @"strReturnMessage
//        //        @"UserName"
//
////        Service Response:
////        0/1           Successful
////        -1            Token not verified for this device/ Enter Valid OTP
////        -3
////        -1/-78/-79    UserException
////        -1/-12/-78    Exception
////        303           limit Downgrade due wrong OTP-PIN provide
////        304           OTP is incorrect
//
//
//
//
//        [manager.requestSerializer setTimeoutInterval:time_out];
//        [manager POST:@"MobileAppSecureCodeVerifyStep2" parameters:dictparam progress:nil
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//
//                  NSError *error=nil;
//                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
//
//                      [hud hideAnimated:YES];
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:[dic objectForKey:@"strReturnMessage"]
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
//                      [self slide_right];
//                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
//                      [self presentViewController:vc animated:NO completion:nil];
//
//
//                  } else if ([[dic objectForKey:@"Response"] integerValue] == 0) {
//
//                      gblclass.user_id = [dic objectForKey:@"Response"];
//                      //                      gblclass.retValue = [dic objectForKey:@"strRetVal"];
//                      gblclass.token = [dic objectForKey:@"Token"];
//                      gblclass.M3sessionid = [dic objectForKey:@"strParaSessionID"];
//                      gblclass.relation_id = [dic objectForKey:@"relationshipID"];
//
//                  } else if ([[dic objectForKey:@"Response"] integerValue] == -1 ||
//                             [[dic objectForKey:@"Response"] integerValue] == -2 ||
//                             [[dic objectForKey:@"Response"] integerValue] == -3 ||
//                             [[dic objectForKey:@"Response"] integerValue] == -4 ||
//                             [[dic objectForKey:@"Response"] integerValue] == -77 ) {
//
//                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
//                  } else {
//                      [self custom_alert:@"Case not documented": @"0"];
//                  }
//
//              } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  [hud hideAnimated:YES];
//                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
//              }];
//
//    } @catch (NSException *exception) {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Try again later." :@"0"];
//    }
//
//}






@end
