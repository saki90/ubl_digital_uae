//
//  RequestHandler.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 22/11/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "RequestHandler.h"

@implementation RequestHandler


/////////////************************** SSL PINNING START ******************************////////////////
//
//
////- (IBAction)MakeHTTPRequestTapped
////{
////    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
////
////    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
////
////    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
////    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
////    [self.connection start];
////
////    if ([self isSSLPinning])
////    {
////        [self printMessage:@"Making pinned request"];
////    }
////    else
////    {
////        [self printMessage:@"Making non-pinned request"];
////    }
////
////}
//
//-(void)SSL_Call
//{
//
//    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
//    {
//
//        if ([chk_ssl isEqualToString:@"signup"])
//        {
//            chk_ssl=@"";
//            [self Existing_UserLogin:@""];
//        }
//        else if ([chk_ssl isEqualToString:@"qr"])
//        {
//            chk_ssl=@"";
//            [self Is_Instant_Pay_Allow:@""];
//        }
//    }
//    else
//    {
//        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//        [request setHTTPMethod:@"POST"];
//        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//        [self.connection start];
//    }
//
//}
//
//
//- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//
//    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
//    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
//    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
//    NSData *skabberCertData = [self skabberCert];
//
//    if ([self isSSLPinning])
//    {
//        if ([remoteCertificateData isEqualToData:skabberCertData])
//        {
//            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
//            gblclass.ssl_pin_check=@"1";
//
//            //[self Existing_UserLogin:@""];
//
//        }
//        else
//        {
//            [self.connection cancel];
//            [self custom_alert:@"Authentication Failed." :@"0"];
//            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
//
//            gblclass.ssl_pin_check=@"0";
//            [hud hideAnimated:YES];
//
//            return;
//        }
//
//        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
//        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
//    }
//
//
//    if ([chk_ssl isEqualToString:@"signup"])
//    {
//        chk_ssl=@"";
//        [self Existing_UserLogin:@""];
//    }
//    else if ([chk_ssl isEqualToString:@"qr"])
//    {
//        chk_ssl=@"";
//        [self Is_Instant_Pay_Allow:@""];
//    }
//
//    [self.connection cancel];
//
//}
//
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
//{
//    if (self.responseData == nil)
//    {
//        self.responseData = [NSMutableData dataWithData:data];
//    }
//    else
//    {
//        [self.responseData appendData:data];
//    }
//
//}
//
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection
//{
//    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
//    [self printMessage:response];
//    self.responseData = nil;
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
//{
//    [hud hideAnimated:YES];
//    if(error.code == NSURLErrorTimedOut) {
//        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
//    }
//    NSLog(@"%@", error.localizedDescription);
//}
//
//
//- (NSData *)skabberCert
//{
//    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
//
//    return [NSData dataWithContentsOfFile:cerPath];
//}
//
//
//- (void)printMessage:(NSString *)message
//{
//    NSString *existingMessage = self.textOutput.text;
//    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
//}
//
//
//- (BOOL)isSSLPinning
//{
//
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
//
//    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
//    {
//        return false;
//    }
//    else if (gblclass.ssl_pinning_url1 == nil)
//    {
//        return false;
//    }
//    else
//    {
//        return true;
//    }
//
//    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
//    //if (envValue==nil){
//    //    return false;}
//    //else{
//    //    return [envValue boolValue];
//    //}
//
//}
//
//
/////////////************************** SSL PINNING END ******************************////////////////

@end

