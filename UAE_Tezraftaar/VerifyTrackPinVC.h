//
//  VerifyTrackPinVC.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 21/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtpTextField.h"

@interface VerifyTrackPinVC : UIViewController<UITextFieldDelegate,OtpTextFieldDelegate> {
    
    IBOutlet OtpTextField* txt_1;
    IBOutlet OtpTextField* txt_2;
    IBOutlet OtpTextField* txt_3;
    IBOutlet OtpTextField* txt_4;
    IBOutlet OtpTextField* txt_5;
    IBOutlet OtpTextField* txt_6;
}

@end
