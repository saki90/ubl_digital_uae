//
//  EditPayee.h
//  ubltestbanking
//
//  Created by ammar on 13/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface EditPayee : UIViewController<UITextFieldDelegate>
{
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}
@property (weak, nonatomic)  IBOutlet UITextField* txtbank;
@property (weak, nonatomic)  IBOutlet UITextField* txtaccountname;
@property (weak, nonatomic)  IBOutlet UITextField* txtLastpayment;
@property (weak, nonatomic)  IBOutlet UITextField* txtname;
@property (weak, nonatomic)  IBOutlet UITextField* txtemail;
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

