//
//  Transaction_history.m
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Transaction_history.h"
#import "OutdtDataset.h"


NSString *const kTransaction_historyResponse = @"Response";
NSString *const kTransaction_historyOutdtDataset = @"outdtDataset";
NSString *const kTransaction_historyOutdtData = @"outdtData";
NSString *const kTransaction_historyStrReturnMessage = @"strReturnMessage";


@interface Transaction_history ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Transaction_history

@synthesize response = _response;
@synthesize outdtDataset = _outdtDataset;
@synthesize outdtData = _outdtData;
@synthesize strReturnMessage = _strReturnMessage;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kTransaction_historyResponse fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kTransaction_historyOutdtDataset]];
            self.outdtData = [self objectOrNilForKey:kTransaction_historyOutdtData fromDictionary:dict];
            self.strReturnMessage = [self objectOrNilForKey:kTransaction_historyStrReturnMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kTransaction_historyResponse];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kTransaction_historyOutdtDataset];
    [mutableDict setValue:self.outdtData forKey:kTransaction_historyOutdtData];
    [mutableDict setValue:self.strReturnMessage forKey:kTransaction_historyStrReturnMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kTransaction_historyResponse];
    self.outdtDataset = [aDecoder decodeObjectForKey:kTransaction_historyOutdtDataset];
    self.outdtData = [aDecoder decodeObjectForKey:kTransaction_historyOutdtData];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kTransaction_historyStrReturnMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kTransaction_historyResponse];
    [aCoder encodeObject:_outdtDataset forKey:kTransaction_historyOutdtDataset];
    [aCoder encodeObject:_outdtData forKey:kTransaction_historyOutdtData];
    [aCoder encodeObject:_strReturnMessage forKey:kTransaction_historyStrReturnMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    Transaction_history *copy = [[Transaction_history alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
    }
    
    return copy;
}


@end
