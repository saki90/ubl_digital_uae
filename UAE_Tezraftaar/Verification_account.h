//
//  Verification_account.h
//  ubltestbanking
//
//  Created by ammar on 04/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface Verification_account : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel* lbl_text;
    
    IBOutlet UITextField* txt_1;
    IBOutlet UITextField* txt_2;
    IBOutlet UITextField* txt_3;
    IBOutlet UITextField* txt_4;
    IBOutlet UITextField* txt_5;
    IBOutlet UITextField* txt_6;
    
    
    IBOutlet UIView* vw_JB;
    IBOutlet UIView* vw_tNc;
    IBOutlet UIButton* btn_check_JB;
    IBOutlet UIButton* btn_next;
    IBOutlet UIButton* btn_resend_otp;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}


@property (nonatomic, strong) IBOutlet UITextField* txtcode;


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;




@end

