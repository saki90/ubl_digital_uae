//
//  Signup_detail.h
//  ubltestbanking
//
//  Created by ammar on 04/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface Signup_branch : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{

    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********

}


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (nonatomic, strong) IBOutlet UIView* haveview;
@property (nonatomic, strong) IBOutlet UIView* dnthaveview;
@property (nonatomic, strong) IBOutlet UIView* bankview;
@property (nonatomic, strong) IBOutlet UITableView* banktableview;

@property (nonatomic, strong) IBOutlet UITextField* emailaddress;
@property (nonatomic, strong) IBOutlet UITextField* atmcard;
@property (nonatomic, strong) IBOutlet UITextField* atmpin;



@property (nonatomic, strong) IBOutlet UITextField* dntatmemail;
@property (nonatomic, strong) IBOutlet UITextField* maidenname;
@property (nonatomic, strong) IBOutlet UITextField* branchname;
@property (nonatomic,strong) IBOutlet UITextField* acc_number;

@end
