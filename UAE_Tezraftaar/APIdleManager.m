//
//  APIdleManager.m
//
//  Created by Malleo, Mitch on 12/8/14.
//

#import "APIdleManager.h"


@interface APIdleManager()

@property (strong, nonatomic) NSTimer *idleTimer;
@property (strong, nonatomic) IBOutlet NSTimer *idleTimer_saki;
@property (strong, nonatomic) NSDate *lastInteraction;
@property (assign, nonatomic) BOOL didTimeout;

@end

static APIdleManager *_sharedInstance = nil;

#pragma mark - APIdleManager

@implementation APIdleManager

#pragma mark - Lifecycle

- (id)init
{
    self = [super init];
    
    if (self)
    {
//        [self createTimer];
//        self.willCreateNewTimerOnTimeout = YES;
    }
    
    return self;
}

#pragma mark - Public Methods

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;
    
    static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (void)createTimer
{
    
    //NSLog(@"***** createTimer *****");
    
    self.lastInteraction = [NSDate date];
    
    if (self.idleTimer)
    {
        [self.idleTimer invalidate];
    }
    
    self.didTimeout = NO;
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval: 1000.0
                                                      target:self
                                                    selector:@selector(checkDidTimeout)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)checkAndReset
{
    if (![self checkDidTimeout])
    {
        [self createTimer];
    }
}

- (void)didReceiveInput
{
    self.lastInteraction = [NSDate date];
}

#pragma mark - Private Methods

- (BOOL)checkDidTimeout
{
    
    //NSLog(@"***** checkDidTimeout *****");
    NSDate *currentDate = [NSDate date];
    NSTimeInterval interactionTime = [currentDate timeIntervalSinceDate: self.lastInteraction];
    
    if (interactionTime > kMaxIdleTimeSeconds - 1 && !self.didTimeout)
    {
        [self timedOut];
        return YES;
    }
    
    return NO;
}


-(void)timme_invaletedd
{
    [self.idleTimer invalidate];
}




- (void)timedOut
{
    //NSLog(@"***** timedOutOut *****");
    
    if(self.onTimeout)
    {
        //NSLog(@"***** timedOut In *****");
        
        [self.idleTimer invalidate];
        self.idleTimer = nil;
        self.didTimeout = YES;
        self.onTimeout();
        
        if(self.willCreateNewTimerOnTimeout)
        {
               [self createTimer];
           
            
            //NSLog(@"**** Session Time Out Successfully ****");
            
            //            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
            //                                        gblclass.story_board bundle:[NSBundle mainBundle]];
            //            UIViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            
            
            //   [self presentViewController:myController animated:YES completion:nil];
            
        }
    }
}


@end
