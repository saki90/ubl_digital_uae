//
//  GnbRefreshAcct.h
//
//  Created by   on 11/04/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbRefreshAcct : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *custAcopDt;
@property (nonatomic, strong) NSString *accountTypeDesc;
@property (nonatomic, strong) NSString *defaultOrder;
@property (nonatomic, strong) NSString *registeredAccountId;
@property (nonatomic, assign) id cardLimit;
@property (nonatomic, strong) NSString *brCode;
@property (nonatomic, strong) NSString *ccyCode;
@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSString *custName;
@property (nonatomic, strong) NSString *accountNo;
@property (nonatomic, strong) NSString *accountCurrency;
@property (nonatomic, strong) NSString *addr4;
@property (nonatomic, strong) NSString *beneficiaryId;
@property (nonatomic, strong) NSString *brName;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *accountNature;
@property (nonatomic, assign) id ccBalanceDate;
@property (nonatomic, strong) NSString *chkDigit;
@property (nonatomic, strong) NSString *custNo;
@property (nonatomic, assign) id ccBalance;
@property (nonatomic, strong) NSString *sortOrderCCY;
@property (nonatomic, strong) NSString *custBlockAmt;
@property (nonatomic, strong) NSString *isDefault;
@property (nonatomic, strong) NSString *billDate;
//@property (nonatomic, strong) NSString *accountType;
@property (nonatomic, strong) NSString *appCode;
@property (nonatomic, strong) NSString *descr;
@property (nonatomic, strong) NSString *currencyDescr;
@property (nonatomic, strong) NSString *statementDate;
@property (nonatomic, strong) NSString *m3Balance;
@property (nonatomic, strong) NSString *addr1;
@property (nonatomic, strong) NSString *branchUpdate;
@property (nonatomic, strong) NSString *availableBalance;
@property (nonatomic, strong) NSString *ccy;
@property (nonatomic, assign) id cardType;
@property (nonatomic, strong) NSString *balance;
@property (nonatomic, strong) NSString *privileges;
@property (nonatomic, strong) NSString *availableBalanceTime;
@property (nonatomic, strong) NSString *sortOrderACC;
@property (nonatomic, strong) NSString *custBal;
@property (nonatomic, strong) NSString *creditLimit;
@property (nonatomic, strong) NSString *addr2;
@property (nonatomic, strong) NSString *custZkt;
@property (nonatomic, strong) NSString *custBalDt;
@property (nonatomic, assign) id expiryDate;
@property (nonatomic, strong) NSString *currencyPrefix;
@property (nonatomic, strong) NSString *branchName;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, assign) id embossedName;
@property (nonatomic, assign) id isOfflineBr;
@property (nonatomic, strong) NSString *custOpBal;
@property (nonatomic, assign) id cardNumber;
@property (nonatomic, strong) NSString *defaultRange;
@property (nonatomic, strong) NSString *addr3;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
