//
//  CnicRegistrationTypeViewController.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 15/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface CnicRegistrationTypeViewController : UIViewController

{
    IBOutlet UIButton *btn_registerEmail;
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}




- (IBAction)btn_registerEmail:(id)sender;
- (IBAction)btn_enterDob:(id)sender;


@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

