//
//  AddPayee.h
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface FundTransfer : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UISearchResultsUpdating,UIGestureRecognizerDelegate>
{
    //    IBOutlet UIPickerView* picker;
    //    IBOutlet UIView* picker_view;
    
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_table;
    IBOutlet UIImageView* img_dd3;
    
    IBOutlet UILabel* lbl_h1;
    IBOutlet UILabel* lbl_h1_1;
    IBOutlet UILabel* lbl_h2;
    
    IBOutlet UITextField* txt_accnt_title;
    IBOutlet UITextField* txt_accnt_title_2;
    IBOutlet UITextField* txt_accnt_number;
    IBOutlet UITextField* txt_accnt_nick;
    IBOutlet UITextField* txt_accnt_email_address;
    IBOutlet UIView* vw_fetch_title;
    IBOutlet UITextField* txt_iban;
    
    IBOutlet UIButton* btn_submit;
    IBOutlet UIButton* btn_cancel;
    IBOutlet UIButton* btn_add_record;
    
    
    IBOutlet UISearchBar* search;
    
    IBOutlet UIButton* btn_combo;
    IBOutlet UIImageView* img_combo;
    IBOutlet UILabel* lbl_IBFT_format;
    IBOutlet UILabel* lbl_formatted;
    IBOutlet UITextView* txt_IBFT_formated;
    IBOutlet UIView* vw_IBFT;
    IBOutlet UITextField* txt_acct_number_ibft;
    IBOutlet UITextField* txt_ibft_relation;
    IBOutlet UITableView* table_ibft_relation;
    IBOutlet UIView* vw_ibft_relation;
    IBOutlet UILabel* lbl_table_header;
    
    IBOutlet UILabel* lbl_acct_number;
    IBOutlet UILabel* lbl_acct_num_txt;
    IBOutlet UILabel* lbl_acct_numb_brnch;
    IBOutlet UILabel* lbl_acct_num_txt_brnch;
    IBOutlet UIImageView* img_arrow;
    
    IBOutlet UIView* vw_iban;
    IBOutlet UIButton* btn_choose_type;
    IBOutlet UIButton* btn_bank_choose;
    IBOutlet UITextField* txt_choose_type;
    IBOutlet UITextField* txt_bank_choose;
    IBOutlet UITableView* table_choose_type;
    IBOutlet UITextView* txt_format;
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
       BOOL isFiltered;
       NSMutableArray *arrsearchresult ,*arrsearchname;
       NSMutableArray *arrfullname;
    
    
}

@property (strong, nonatomic) IBOutlet UITableView *tblAppoinment;


@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResult;



@property (nonatomic, strong) TransitionDelegate *transitionController;


@property (nonatomic, strong) IBOutlet UITextField* txtbillservice;
@property (nonatomic, strong) IBOutlet UITextField* txtbilltype;
@property (nonatomic, strong) IBOutlet UITextField* txtcompany;
@property (nonatomic, strong) IBOutlet UITextField* txtaccountnumber;

//@property (nonatomic, strong) IBOutlet UIView* billserviceview;
@property (nonatomic, strong) IBOutlet UIView* billtype;
@property (nonatomic, strong) IBOutlet UIView* company;

//@property (nonatomic, strong) IBOutlet UITableView* tableviewbillservice;
@property (nonatomic, strong) IBOutlet UITableView* tableviewbilltype;
@property (nonatomic, strong) IBOutlet UITableView* tableviewcompany;

@property (nonatomic, strong) IBOutlet UIButton* dd3;


// Fund Transfer using Cnic
@property (strong, nonatomic) IBOutlet UIView *fundTransferThroughCnicView;
@property (strong, nonatomic) IBOutlet UITextField *receiverName;
@property (strong, nonatomic) IBOutlet UITextField *receiverCnic;
@property (strong, nonatomic) IBOutlet UITextField *confirmReceiverCnic;
@property (strong, nonatomic) IBOutlet UITextField *receiverMobile;
@property (strong, nonatomic) IBOutlet UITextField *receiverEmail;


@end

