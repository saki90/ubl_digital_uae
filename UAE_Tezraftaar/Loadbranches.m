//
//  Table1.m
//
//  Created by   on 16/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Loadbranches.h"


NSString *const kTable1Brcode = @"brcode";
NSString *const kTable1Brdetail = @"brdetail";


@interface Loadbranches ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Loadbranches

@synthesize brcode = _brcode;
@synthesize brdetail = _brdetail;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.brcode = [self objectOrNilForKey:kTable1Brcode fromDictionary:dict];
            self.brdetail = [self objectOrNilForKey:kTable1Brdetail fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.brcode forKey:kTable1Brcode];
    [mutableDict setValue:self.brdetail forKey:kTable1Brdetail];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.brcode = [aDecoder decodeObjectForKey:kTable1Brcode];
    self.brdetail = [aDecoder decodeObjectForKey:kTable1Brdetail];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_brcode forKey:kTable1Brcode];
    [aCoder encodeObject:_brdetail forKey:kTable1Brdetail];
}

- (id)copyWithZone:(NSZone *)zone
{
    Loadbranches *copy = [[Loadbranches alloc] init];
    
    if (copy) {

        copy.brcode = [self.brcode copyWithZone:zone];
        copy.brdetail = [self.brdetail copyWithZone:zone];
    }
    
    return copy;
}


@end
