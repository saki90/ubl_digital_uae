//
//  GnbAccountTransactionDetail.h
//
//  Created by   on 03/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbAccountTransactionDetail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *credit;
@property (nonatomic, assign) id valueDate;
@property (nonatomic, strong) NSString *balance;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *debit;
@property (nonatomic, strong) NSString *gnbAccountTransactionDetailDescription;
@property (nonatomic, assign) id cardNo;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
