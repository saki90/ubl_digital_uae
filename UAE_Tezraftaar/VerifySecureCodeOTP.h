//
//  VerifySecureCodeOTP.h
//  ubltestbanking
//
//  Created by Asim Khan on 7/23/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import "OtpTextField.h"

@interface VerifySecureCodeOTP : UIViewController<OtpTextFieldDelegate, UITextFieldDelegate> {
    
    IBOutlet OtpTextField *txt_1;
    IBOutlet OtpTextField *txt_2;
    IBOutlet OtpTextField *txt_3;
    IBOutlet OtpTextField *txt_4;
    IBOutlet OtpTextField *txt_5;
    IBOutlet OtpTextField *txt_6;
    
}

@property (nonatomic, strong) AVPlayer *avplayer;
@property (weak, nonatomic)  UITextView *textOutput;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIView *navBar;
@property (weak, nonatomic) IBOutlet UIView *tabBar;
@property (weak, nonatomic) IBOutlet UILabel *cntr_lbl_heading;

- (IBAction)btn_back:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;
-(IBAction)btn_resend_otp:(id)sender;


@end
