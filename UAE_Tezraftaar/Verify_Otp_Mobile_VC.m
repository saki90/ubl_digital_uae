//
//  Verify_Otp_Mobile_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 24/05/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "Verify_Otp_Mobile_VC.h"
#import "UIViewController+TextFieldDelegate.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"

@interface Verify_Otp_Mobile_VC ()
{
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSUserDefaults *defaults;
    
    NSMutableArray *arr_pin_pattern;
    NSMutableArray *arr_pw_pin;
    NSString *str_pw;
    NSString *str_Otp;
    int txt_nam;
    BOOL isContinued;
    NSString *callingResource;
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    NSString* ssl_count;
    NSString* t_n_c;
    NSMutableArray* a;
    NSString*  first_time_chk;
    NSString *stepedVC;
}

@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIView *navBar;
@property (weak, nonatomic) IBOutlet UIView *tabBar;
@property (weak, nonatomic) IBOutlet UILabel *cntr_lbl_heading;

- (IBAction)btn_back:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;

@end

@implementation Verify_Otp_Mobile_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    ssl_count = @"0";
    
    NSLog(@"%@",gblclass.email_onbording);
    
    NSArray* email_split = [gblclass.email_onbording componentsSeparatedByString: @"@"];
    NSString *subStr = [gblclass.email_onbording substringWithRange:NSMakeRange(0,1)];
    
    self.cntr_lbl_heading.text = [NSString stringWithFormat:@"Please enter the Verification Code sent to your email %@******@%@",subStr,[email_split objectAtIndex:1]];
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    txt_1.OtpDelegate = self;
    txt_2.OtpDelegate = self;
    txt_3.OtpDelegate = self;
    txt_4.OtpDelegate = self;
    txt_5.OtpDelegate = self;
    txt_6.OtpDelegate = self;
    
    txt_1.borderStyle = UITextBorderStyleNone;
    txt_2.borderStyle = UITextBorderStyleNone;
    txt_3.borderStyle = UITextBorderStyleNone;
    txt_4.borderStyle = UITextBorderStyleNone;
    txt_5.borderStyle = UITextBorderStyleNone;
    txt_6.borderStyle = UITextBorderStyleNone;
    
    [txt_1 setBackgroundColor:[UIColor clearColor]];
    [txt_2 setBackgroundColor:[UIColor clearColor]];
    [txt_3 setBackgroundColor:[UIColor clearColor]];
    [txt_4 setBackgroundColor:[UIColor clearColor]];
    [txt_5 setBackgroundColor:[UIColor clearColor]];
    [txt_6 setBackgroundColor:[UIColor clearColor]];
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"]) {
        [self Play_bg_video];
        [self playerStartPlaying];
        self.navBar.backgroundColor = [UIColor clearColor];
        self.tabBar.backgroundColor = [UIColor clearColor];
        self.progressBar.hidden = YES;
        callingResource = @"reset";
    } else if ([gblclass.parent_vc_onbording isEqualToString:@"RegisterAccountVC"]) {
        self.progressBar.hidden = NO;
        callingResource = @"request";
    }
    
    //    locationManager.delegate = self;
    //    [locationManager startUpdatingLocation];
    //    gblclass.controllers_Refrence_Stack = [[NSMutableArray alloc] init];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [txt_1 becomeFirstResponder];
    
    [self clearOTP];
}


-(void)Play_bg_video {
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [self.avplayer play];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_resend_otp:(id)sender
{
    [self checkinternet];
    if (netAvailable) {
        //        if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"]) {
        //            chk_ssl = @"generateOTPForDARequest";
        //        } else if ([gblclass.parent_vc_onbording isEqualToString:@"RegisterAccountVC"]) {
        //            chk_ssl=@"resend";
        //        }
        chk_ssl = @"generateOTPForDARequest";
        [self SSL_Call];
    }
}

-(void) Verify_OTP_Email:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //     NSLog(@"%@",gblclass.request_id_onbording);
        
        gblclass.otpPin = str_Otp;
        
        // NSLog(@"%@",gblclass.email_onbording);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.email_onbording],
                                                                       [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.otpPin],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"EMAIL"],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strBranchCode",
                                                                       @"strAccountNumber",
                                                                       @"strOTPPIN",
                                                                       @"strTTAccessKey",
                                                                       @"Channel",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //        NSLog(@"%@",gblclass.mobile_number_onbording);
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
        //                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
        //                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
        //                                                                       [encrypt encrypt_Data:@"343"],
        //                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
        //                                                                       [encrypt encrypt_Data:str_Otp],
        //                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
        //                                                                       [encrypt encrypt_Data:@"SMS"],
        //                                                                       [encrypt encrypt_Data:gblclass.Udid],
        //                                                                       [encrypt encrypt_Data:gblclass.token], nil]
        //
        //                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
        //                                                                       @"strSessionId",
        //                                                                       @"IP",
        //                                                                       @"strBranchCode",
        //                                                                       @"strAccountNumber",
        //                                                                       @"strOTPPIN",
        //                                                                       @"strTTAccessKey",
        //                                                                       @"Channel",
        //                                                                       @"Device_ID",
        //                                                                       @"Token", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"VerifyOTPForDigitalAccountReq" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  //                  [self clearOTP];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      
                      //Generate OTP FOR Mobile ::
                      
                      [self Generate_OTP_For_DA_Request_Mobile:@""];
                      
                      
                      //Apply acct check
                      // [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      // [defaults setValue:@"0" forKey:@"apply_acct"];
                      // [defaults synchronize];
                      //[self GetOnBoardProducts:@""];
                      
                  }
                  //                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"2"])
                  //                  {
                  //                      //Apply acct check
                  //
                  //                      //                      {
                  //                      //                          RequestId = 1087;
                  //                      //                          Response = 2;
                  //                      //                          SRNumber = "";
                  //                      //                          strReturnMessage = InProcess;
                  //                      //                      }
                  //
                  //                      isContinued = YES;
                  //                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                  //                      [defaults setValue:@"0" forKey:@"apply_acct"];
                  //                      [defaults synchronize];
                  //                      [self GetOnBoardProducts:@""];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"3"])
                  //                  {
                  //                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                  //                      [defaults setValue:[encrypt encrypt_Data:[dic objectForKey:@"SRNumber"]] forKey:@"sr_num"];
                  //                      [defaults setValue:@"1" forKey:@"apply_acct"];
                  //                      [defaults synchronize];
                  //
                  //                      [hud hideAnimated:YES];
                  //                      [self slide_right];
                  //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                  //                      [self presentViewController:vc animated:NO completion:nil];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"4"])
                  //                  {
                  //                      [self clearOTP];
                  //                      [hud hideAnimated:YES];
                  //                      [txt_1 becomeFirstResponder];
                  //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  //
                  //                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"5"])
                  //                  {
                  //                      [self clearOTP];
                  //                      [hud hideAnimated:YES];
                  //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  //
                  //                  }
                  //                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-3"] || [[dic objectForKey:@"Response"] isEqualToString:@"-4"]) {
                  //                      [hud hideAnimated:YES];
                  //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                  //                                                                          message:[dic objectForKey:@"strReturnMessage"]
                  //                                                                         delegate:self
                  //                                                                cancelButtonTitle:@"Ok"
                  //                                                                otherButtonTitles:nil];
                  //
                  //                      [alertView show];
                  //                      [self slide_left];
                  //                      [self dismissViewControllerAnimated:NO completion:nil];
                  //
                  //                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    }
    @catch (NSException *exception)
    {
        [self clearOTP];
        [hud hideAnimated:YES];
        [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
}



-(void)GenerateOTPForDARequest:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.email_onbording],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"], //
                                                                       [encrypt encrypt_Data:@"EMAIL"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        [manager.requestSerializer setTimeoutInterval:                                                                                                                                            time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      [self clearOTP];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                      
                      //                      gblclass.token = [dic objectForKey:@"Token"];
                      //                      gblclass.request_id_onbording = [dic objectForKey:@"RequestId"];
                      //
                      //                      [self slide_right];
                      //                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SelectAccountTypeVC"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}




-(void) Generate_OTP_For_DA_Request_Mobile:(NSString *)strIndustry
{
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //        NSLog(@"%@",gblclass.cnic_onbording);
        //        NSLog(@"%@",gblclass.token);
        //        NSLog(@"%@",gblclass.mobile_number_onbording);
        
        // gblclass.mobile_number_onbording = @"saqib.ahmed@m3tech.com.pk";
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"], //
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],   nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccesskey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  }
                  else  if ([[dic objectForKey:@"Response"] integerValue]==0)
                  {
                      
                      //                      gblclass.token = @"12345";  // [dic objectForKey:@"Token"];
                      //                      gblclass.request_id_onbording = [dic objectForKey:@"RequestId"];
                      
                      gblclass.arr_reset_otp_onbarding = [[NSMutableArray alloc] init];
                      
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.cnic_onbording];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.M3sessionid];
                      [gblclass.arr_reset_otp_onbarding addObject:[GlobalStaticClass getPublicIp]];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.Udid];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.token];
                      [gblclass.arr_reset_otp_onbarding addObject:gblclass.mobile_number_onbording];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"Generate OTP"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"2"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"SMS"];
                      [gblclass.arr_reset_otp_onbarding addObject:@"DIGITALACCOUNTREQ"];
                      
                      
                      [hud hideAnimated:YES];
                      [self slide_right];
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyOtpVC"]; // VerifyOtpVC
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  }  else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




-(void) clearOTP
{
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"otp"])
        {
            chk_ssl=@"";
            [self Verify_OTP_Email:@""];
        }
        else if ([chk_ssl isEqualToString:@"generateOTPForDARequest"])
        {
            chk_ssl=@"";
            [self GenerateOTPForDARequest:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    
    
    [connection cancel];
    
    if ([chk_ssl isEqualToString:@"otp"]) {
        chk_ssl=@"";
        [self Verify_OTP_Email:@""];
    }
    else if ([chk_ssl isEqualToString:@"generateOTPForDARequest"])
    {
        chk_ssl=@"";
        [self GenerateOTPForDARequest:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (responseData == nil) {
        responseData = [NSMutableData dataWithData:data];
    } else {
        [responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    }
    
    //NSLog(@"%@", error.localizedDescription);
}

- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message {
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}

-(void)textFieldDidDelete:(UITextField *)textField {
    OtpTextField *otpField = (OtpTextField *)[self.view viewWithTag:textField.tag-1];
    [otpField becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSUInteger MAX_DIGITS = 1;
    
    NSUInteger maxLength = 1;
    
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text length :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    
    NSString *str1 = @"hello ";
    str1 = [str1 stringByAppendingString:str];
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 )
        {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            
            
            //            if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"1"])
            //            {
            //                btn_next.enabled=YES;
            //            }
            //            else if ([gblclass.device_chck isEqualToString:@"1"] && [t_n_c isEqualToString:@"0"])
            //            {
            //                [hud hideAnimated:YES];
            //
            //                txt_1.text=@"";
            //                txt_2.text=@"";
            //                txt_3.text=@"";
            //                txt_4.text=@"";
            //                txt_5.text=@"";
            //                txt_6.text=@"";
            //
            //                [txt_1 becomeFirstResponder];
            //
            //                [self custom_alert:@"Please Accept Term&Conditition" :@"0"];
            //                return 0;
            //            }
            //            else if ([t_n_c isEqualToString:@"0"])
            //            {
            //                [hud hideAnimated:YES];
            //
            //                txt_1.text=@"";
            //                txt_2.text=@"";
            //                txt_3.text=@"";
            //                txt_4.text=@"";
            //                txt_5.text=@"";
            //                txt_6.text=@"";
            //
            //                [txt_1 becomeFirstResponder];
            //
            //                [self custom_alert:@"Please Accept Term&Conditition" :@"0"];
            //                return 0;
            //            }
            //            else if ([t_n_c isEqualToString:@"1"])
            //            {
            //                btn_next.enabled=YES;
            //            }
            
            
            
            if (textField.tag == 6)
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                if ([str_Otp length] < 6) {
                    [self clearOTP];
                    [hud hideAnimated:YES];
                    [txt_1 becomeFirstResponder];
                    [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
                    [self.view resignFirstResponder];
                } else {
                    [self checkinternet];
                    if (netAvailable){
                        
                        if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"]) {
                            chk_ssl=@"insertUpdateOnboardPin";
                            [self SSL_Call];
                        } else {
                            chk_ssl=@"otp";
                            [self SSL_Call];
                        }
                    }
                }
                return 0;
            }
            
            
            
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}




-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_back:(id)sender
{
    // gblclass.user_login_name=@"";
    [self slide_left];
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)btn_feature:(id)sender
{
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(event.type == UIEventSubtypeMotionShake)
    {
        txt_1.text=@"";
        txt_2.text=@"";
        txt_3.text=@"";
        txt_4.text=@"";
        txt_5.text=@"";
        txt_6.text=@"";
        
        [self.view resignFirstResponder];
        [txt_1 becomeFirstResponder];
        
    }
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end
