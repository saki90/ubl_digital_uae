//
//  GnbForgotPassword.h
//
//  Created by   on 08/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GnbForgotPassword : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iDProperty;
@property (nonatomic, strong) NSString *tEXT;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
