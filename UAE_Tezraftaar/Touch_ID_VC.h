//
//  Touch_ID_VC.h
//  ubltestbanking
//
//  Created by Mehmood on 19/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"

@interface Touch_ID_VC : UIViewController
{
    IBOutlet UIButton* btn_chck_yes;
    IBOutlet UIButton* btn_chck_no;
    IBOutlet UIImageView* img_yes;
    IBOutlet UIImageView* img_no;
    
    IBOutlet UIButton* btn_back_check;
    IBOutlet UIImageView* img_back_check;

    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}


@property (weak, nonatomic) IBOutlet UIImageView *touchLogo;
@property (weak, nonatomic) IBOutlet UILabel *heading;
@property (weak, nonatomic) IBOutlet UILabel *yes;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UILabel *no;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *nxt_btn;

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

-(IBAction)btn_next:(id)sender;

@end

