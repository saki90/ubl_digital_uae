//
//  UblBillManagement.h
//  ubltestbanking
//
//  Created by ammar on 21/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface UblBillManagement : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchDisplayDelegate>{
    
    //    IBOutlet UIPickerView* picker;
    //    IBOutlet UIView* picker_view;
    
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_table;
    IBOutlet UIImageView* img_dd3;
    IBOutlet UISearchBar* search;
    IBOutlet UITextField *txt_cashline_nick;
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (strong, nonatomic) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResult;
@property (nonatomic, strong) TransitionDelegate *transitionController;

@property (nonatomic, strong) IBOutlet UITextField* txtbillservice;
@property (nonatomic, strong) IBOutlet UITextField* txtbilltype;
@property (nonatomic, strong) IBOutlet UITextField* txtcompany;
@property (nonatomic, strong) IBOutlet UITextField* txtphonenumber;
@property (nonatomic, strong) IBOutlet UITextField* txtnick;

//@property (nonatomic, strong) IBOutlet UIView* billserviceview;
@property (nonatomic, strong) IBOutlet UIView* billtype;
@property (nonatomic, strong) IBOutlet UIView* company;

//@property (nonatomic, strong) IBOutlet UITableView* tableviewbillservice;
@property (nonatomic, strong) IBOutlet UITableView* tableviewbilltype;
@property (nonatomic, strong) IBOutlet UITableView* tableviewcompany;
@property (nonatomic, strong) IBOutlet UIButton* dd3;


-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_more:(id)sender;
-(IBAction)btn_account:(id)sender;




@end

