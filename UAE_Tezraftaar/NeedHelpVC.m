//
//  NeedHelpVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 20/11/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "NeedHelpVC.h"
#import "RegisterAccountVC.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"


@interface NeedHelpVC ()<NSURLConnectionDataDelegate, MBProgressHUDDelegate> {
    
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSTimer *timer;
    int time;
    int count;
    NSString* ssl_count;
    NSDictionary *dic;
    NSString* t_n_c;
    NSMutableArray* a;
    NSUserDefaults *defaults;
    UIBackgroundTaskIdentifier backgroundTaskIdentifier;
}



@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@property (strong, nonatomic) IBOutlet UIView *timerView;
@property (strong, nonatomic) IBOutlet UIView *loaderView;
@property (strong, nonatomic) IBOutlet UILabel *secondsLabel;

- (IBAction)restartTimer:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *restartTimer;

- (IBAction)btn_back:(id)sender;
- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;
- (IBAction)callCenterCall:(id)sender;

@end

@implementation NeedHelpVC

//static NSTimer *timer = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    hud = [MBProgressHUD showHUDAddedTo:self.loaderView animated:YES];
    hud.contentColor = [UIColor whiteColor];
    hud.bezelView.color = [UIColor clearColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(suspended:) name:@"UIApplicationSuspendedNotification" object:nil];
    ssl_count = @"0";
    self.restartTimer.hidden = YES;
    self.timerView.hidden = NO;
    
    //    [self createTimer:gblclass.timmerLimitOnBording];
    
    //
    //    [self.view addSubview:hud];
    //    [hud showAnimated:YES];
    
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    //        chk_ssl=@"needOnBoardHelp";
    //        [self SSL_Call];
    //    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self createTimer:gblclass.timmerLimitOnBording];
    
    //    [NeedHelpVC createTimer:gblclass.timmerLimitOnBording];
    hud = [MBProgressHUD showHUDAddedTo:self.loaderView animated:YES];
    hud.contentColor = [UIColor whiteColor];
    hud.bezelView.color = [UIColor clearColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    
    self.timerView.hidden = NO;
    self.restartTimer.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)suspended:(NSNotification *) notification {
    NSLog(@"Suspended");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIApplicationSuspendedNotification" object:nil];
    
}

-(void) createTimer:(int)limit {
    
    // Total time of timer 97 sec
    gblclass.remainingTimeOnBording = limit;
    
    //    hud = [[MBProgressHUD alloc] init];
    //    [self.loaderView addSubview:hud];
    //    [self.view addSubview:self.loaderView];
    //    [hud showAnimated:YES];
    //
    //    hud = [MBProgressHUD showHUDAddedTo:self.loaderView animated:YES];
    // //   [hud.bezelView setBackgroundColor:[UIColor clearColor]];//
    //    hud.contentColor = [UIColor whiteColor];
    //    hud.bezelView.color = [UIColor clearColor];
    //    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //
    backgroundTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
    }];
    
    // Make sure you end the background task when you no longer need background execution:
    // [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
    
    //    [NSTimer scheduledTimerWithTimeInterval:1
    //                                     target:self
    //                                   selector:@selector(timerDidFire:)
    //                                   userInfo:nil
    //                                    repeats:YES];
    
    
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //        //Run your loop here
    //        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick) userInfo:nil repeats:YES];
    //        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    //        [[NSRunLoop currentRunLoop] run];
    ////        dispatch_async(dispatch_get_main_queue(), ^(void) {
    ////            //stop your HUD here
    ////            //This is run on the main thread
    ////
    ////
    ////        });
    //    });
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick) userInfo:nil repeats:YES];
    //    self.timerView.hidden = NO;
    //    self.restartTimer.hidden = YES;
    
    //     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(suspended:) name:@"UIApplicationDidEnterBackgroundNotification" object:nil];
}

//+(void) invalidateTimer {
//    [timer invalidate];
//    timer = nil;
//}

- (void) timerDidFire:(NSTimer *)timer {
    // This method might be called when the application is in the background.
    // Ensure you do not do anything that will trigger the GPU (e.g. animations)
    // See: http://developer.apple.com/library/ios/DOCUMENTATION/iPhone/Conceptual/iPhoneOSProgrammingGuide/ManagingYourApplicationsFlow/ManagingYourApplicationsFlow.html#//apple_ref/doc/uid/TP40007072-CH4-SW47
    NSLog(@"%i", gblclass.remainingTimeOnBording);
}

-(void)tick {
    
    NSLog(@"%i", gblclass.remainingTimeOnBording);
    gblclass.remainingTimeOnBording --;
    
    if (gblclass.remainingTimeOnBording >= 0) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            //stop your HUD here
            //This is run on the main thread
            self.secondsLabel.text = [NSString stringWithFormat:@"%i", gblclass.remainingTimeOnBording];
        });
        self.secondsLabel.text = [NSString stringWithFormat:@"%i", gblclass.remainingTimeOnBording];
    } else {
        [self.view addSubview:hud];
        [hud showAnimated:YES];
        //        [self OnBoardHelpProvided:@""];
        
        gblclass.remainingTimeOnBording = 0;
        self.timerView.hidden = YES;
        //        self.restartTimer.hidden = NO;
        [hud hideAnimated:YES];
        [timer invalidate];
        timer = nil;
        
        // Make sure you end the background task when you no longer need background execution:
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
        
        [self slide_left];
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}


///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])  {
        
        if ([chk_ssl isEqualToString:@"needOnBoardHelp"]) {
            chk_ssl=@"";
            [self NeedOnBoardHelp:@""];
            
        } else if ([chk_ssl isEqualToString:@"qr"]) {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
            
        }
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
    {
        chk_ssl=@"";
        [self NeedOnBoardHelp:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [connection cancel];
    }
    
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    //  NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////

-(void)OnBoardHelpProvided:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects: [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.Udid],
                                                                        [encrypt encrypt_Data:gblclass.user_name_onbording],
                                                                        [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                                                        [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                        [encrypt encrypt_Data:gblclass.step_no_onbording],
                                                                        [encrypt encrypt_Data:gblclass.helpidentifier],
                                                                        [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                        [encrypt encrypt_Data:@"1234"],
                                                                        [encrypt encrypt_Data:gblclass.token],
                                                                        [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"Device_ID",
                                                                       @"name",
                                                                       @"strMobileNo",
                                                                       @"strCnic",
                                                                       @"stepno",
                                                                       @"HelpIdentifier",
                                                                       @"RequestId",
                                                                       @"strSessionId",
                                                                       @"Token",
                                                                       @"IP", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"OnBoardHelpProvided" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      NSLog(@"Success");
                  } if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Sorry!" message:[dic objectForKey:@"strReturnMessage "]  preferredStyle:UIAlertControllerStyleAlert];
                      
                      UIAlertAction *callNow = [UIAlertAction actionWithTitle:@"Call Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                          
                          //                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[dic objectForKey:@"CalCentreNo"]]]];
                          
                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",[dic objectForKey:@"CalCentreNo"]]] options:@{} completionHandler:^(BOOL success) {
                              [self slide_left];
                              [self dismissViewControllerAnimated:NO completion:nil];
                          }];
                      }];
                      
                      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                          [self slide_left];
                          [self dismissViewControllerAnimated:NO completion:nil];
                      }];
                      
                      [alertController addAction:callNow];
                      [alertController addAction:ok];
                      [self presentViewController:alertController animated:YES completion:nil];
                      
                      
                      //                      CalCentreNo = "021-35302789";
                      //                      Response = 1;
                      //                      strReturnMessage = "Eleventh API";
                      
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}


-(void)NeedOnBoardHelp:(NSString *)strIndustryOmai
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                    [encrypt encrypt_Data:gblclass.email_onbording],
                                    [encrypt encrypt_Data:gblclass.request_id_onbording],
                                    [encrypt encrypt_Data:gblclass.cnic_onbording],
                                    [encrypt encrypt_Data:gblclass.user_name_onbording],
                                    [encrypt encrypt_Data:gblclass.step_no_help_onbording],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strEmailAddress",
                                                                       @"RequestId",
                                                                       @"strCNIC",
                                                                       @"Name",
                                                                       @"StepNo",
                                                                       nil]];
        
        
        //        public void NeedOnBoardHelp(
        //                                    string strSessionId,
        //                                    string IP,
        //                                    string Device_ID,
        //                                    string Token,
        //                                    string strMobileNo,
        //                                    string strEmailAddress,
        //                                    string RequestId,
        //                                    string strCNIC,
        //                                    string Name,
        //                                    string StepNo
        //                                    )
        //        {
        //            NoOfAttempts = "10";
        //            Response = "0";
        //            TimerLimit = "120";
        //            strReturnMessage = "message to be on alertiew";
        //        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"NeedOnBoardHelp" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                      
                      // Changed 4 Apr.
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"-1"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      //                      [NeedHelpVC createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      
                      //                                        [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      //                                        gblclass.helpidentifier = [dic objectForKey:@"HelpIdentifier"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"] || [[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      [self slide_left];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      [alertView show];
                      [self dismissViewControllerAnimated:NO completion:nil];
                  } else {
                      [self slide_left];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      [alertView show];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  //                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  //                      [alert show];
                  [self slide_right];
                  [self dismissViewControllerAnimated:NO completion:nil];
                  
                  //                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];//                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //                      [alert show];
        [self slide_right];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        //        [self custom_alert:@"Try again later." :@"0"];
    }
    
}




//////////************************** START TABBAR CONTROLS ***************************///////////

- (IBAction)restartTimer:(id)sender {
    //    [self createTimer:time];
}

-(IBAction)btn_back:(id)sender {
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_feature:(id)sender
{
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

- (IBAction)callCenterCall:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:021-111825888"]] options:@{} completionHandler:^(BOOL success) {
        //        [self slide_left];
        //        [self dismissViewControllerAnimated:NO completion:nil];
    }];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(suspended:) name:@"UIApplicationDidEnterBackgroundNotification" object:nil];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:021-35302789"]]];
    
}

//////////************************** END TABBAR CONTROLS ***************************///////////


#pragma mark - Check Internet -
-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            UIAlertView *magalert33 = [[UIAlertView alloc] initWithTitle:@"Notification" message:statusString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [magalert33 show];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    
}


- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end

