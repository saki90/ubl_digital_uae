//
//  MPin_change_VC.m
//  ubltestbanking
//
//  Created by Mehmood on 18/07/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "MPin_change_VC.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "Encrypt.h"

@interface MPin_change_VC ()<NSURLConnectionDataDelegate>
{
    UIAlertController* alert;
    
    GlobalStaticClass* gblclass;
    MBProgressHUD *hud;
   // UIStoryboard *storyboard;
    UIViewController *vc;
    NSString *responsecode;
    NSString* old_pw,*new_pw,*retype_pw;
    
    UIStoryboard *mainStoryboard;
   // UIViewController *vc;
    Encrypt *encrypt;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation MPin_change_VC
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    encrypt = [[Encrypt alloc] init];
    gblclass=[GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.transitionController = [[TransitionDelegate alloc] init];
    
    txt_new_pw.delegate=self;
    txt_retype_pw.delegate=self;
    txt_old_pw.delegate=self;
    
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
 //**   [self Play_bg_video];
}

-(void)Play_bg_video
{
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)viewDidAppear:(BOOL)animated
{
 //**   [super viewDidAppear:animated];
 //**   [self.avplayer play];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
  //**  AVPlayerItem *p = [notification object];
  //**  [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying
{
  //**  [self.avplayer play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_submit:(id)sender
{
    if ([txt_old_pw.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Old Password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if([txt_new_pw.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter New Password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if([txt_retype_pw.text isEqualToString:@""])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Enter Retype Password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(![txt_new_pw.text isEqualToString:txt_retype_pw.text])
    {
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Password Not Match" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
//        [self Change_MPIN:@""];
        
        [self checkinternet];
        if (netAvailable)
        {
            [self SSL_Call];
        }
    }
    
}

-(void) Change_MPIN:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
   [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:mainurl
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:ubl_header_user password:ubl_header_pw];
    
    //NSLog(@"%@",gblclass.signup_relationshipid);
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"7c4d5f254b9e6edd",txt_pin.text,@"1234",[GlobalStaticClass getPublicIp],@"asdf", nil]
    //                                                          forKeys:[NSArray arrayWithObjects:@"strDeviceID",@"strMpinPassword",@"strSessionId",@"IP",@"hCode", nil]];
    
        new_pw  = [txt_new_pw.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        retype_pw  = [txt_retype_pw.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        old_pw  = [txt_old_pw.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSLog(@"%@",gblclass.user_id);
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:new_pw],
                                [encrypt encrypt_Data:retype_pw],
                                [encrypt encrypt_Data:old_pw],
                                [encrypt encrypt_Data:@"1234"],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"userid",
                                                                   @"strDeviceid",
                                                                   @"strPassword",
                                                                   @"strRetypePassword",
                                                                   @"strOldPassword",
                                                                   @"strSessionId",
                                                                   @"IP", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"ChangeMPIN" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
  //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //  [arr_que addObject:(NSDictionary *)[dic objectForKey:@"dt2"]];
              //for(dic in generalarray){
              responsecode=[dic objectForKey:@"Response"];
              
              NSString* exception =[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]];
              
              //[self stringByStrippingHTML:[dic objectForKey:@"strReturnMessage"]]
              
              if ([responsecode isEqualToString:@"0"])
              {
                  [hud hideAnimated:YES];
                  
//                  NSString *valueToSave = @"1";
//                  [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"Mpin"];
//                  [[NSUserDefaults standardUserDefaults] synchronize];
//                  
//                  gblclass.first_time_login_chck=@"1";
                  
                  
                  [self showAlert:exception:@"Attention"];
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                   vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Mpin_login"];
                   [self presentViewController:vc animated:NO completion:nil];
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
              }
              else
              {
                  [self showAlert:exception :@"Attention"];
              }
              
              
              //NSLog(@"Response %@",[dic objectForKey:@"ID"]);
              
              [hud hideAnimated:YES];
              // [_generaltableview reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              [hud hideAnimated:YES];
              
              //NSLog(@"failed load branch");
              
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
    // //NSLog(@"xompanytype cpount %lu",[companytype count]);
    
}

-(NSString *) stringByStrippingHTML:(NSString*)txt
{
    NSRange r;
    NSString *s = txt; //[self copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}


-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:Title
                                                                       message:exception
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                       [alert1 show];
                   });
    //[alert release];
}



- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
 //   BOOL stringIsValid;
    NSInteger MAX_DIGITS=4;
    
    if ([theTextField isEqual:txt_new_pw])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_retype_pw])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:txt_old_pw])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    
    return YES;
}




- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
     [txt_new_pw resignFirstResponder];
     [txt_retype_pw resignFirstResponder];
     [txt_old_pw resignFirstResponder];
}

#define kOFFSET_FOR_KEYBOARD 45.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_more:(id)sender
{
     mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
     vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_feature:(id)sender
{
    
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"feature"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_offer:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"offer"];
    
   [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_find_us:(id)sender
{
    // gblclass.tab_bar_login_pw_check=@"login";
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_faq:(id)sender
{
    mainStoryboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    
    [self presentViewController:vc animated:YES completion:nil];
}



///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([remoteCertificateData isEqualToData:skabberCertData] || [self isSSLPinning] == NO)
    {
        
        if ([self isSSLPinning] || [remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
              [self Change_MPIN:@""];
            
             [self.connection cancel];
        }
        else
        {
             [self.connection cancel];
            
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                            message:@"Authentication Failed."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
                        [alertView show];
            
            
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self.connection cancel];
        [self printMessage:@"The server's certificate does not match SSL PINNING Canceling the request."];
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        
        gblclass.ssl_pin_check=@"0";
        [hud hideAnimated:YES];
        
        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"SSL Not Verified." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    // NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"secure.skabber.com" ofType:@"cer"];
    
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:ssl_pinning_name ofType:ssl_pinning_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////



#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


@end
