//
//  Atm_Card_Management.m
//  ubltestbanking
//
//  Created by ammar on 26/02/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "Atm_Card_Management.h"
#import "GlobalStaticClass.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "GnbATMCardList.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface Atm_Card_Management ()<NSURLConnectionDataDelegate>
{
    MBProgressHUD *hud;
    NSMutableArray* cardnumber,*cardname,*cardstatus,*cardtype;
    GlobalStaticClass *gblclass ;
    NSDictionary *dic;
    UILabel* label;
    UIStoryboard *storyboard;
    UIViewController *vc;
    UIAlertController* alert;
    NSMutableArray* a;
    NSMutableArray* arr_atm_card;
    NSArray* split;
    Encrypt *encrypt;
    NSString* ssl_count;
}

- (IBAction)MakeHTTPRequestTapped;

@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;


@end

@implementation Atm_Card_Management
@synthesize transitionController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    encrypt = [[Encrypt alloc] init];
    a=[[NSMutableArray alloc] init];
    arr_atm_card=[[NSMutableArray alloc] init];
    gblclass =  [GlobalStaticClass getInstance];
    //NSLog(@"view did load user id  %@",gblclass.user_id);
    
    ssl_count = @"0";
    NSLog(@"%@",gblclass.actsummaryarr);
    
    [self checkinternet];
    if (netAvailable)
    {
        [self SSL_Call];
    }
    
    for (dic in gblclass.actsummaryarr)
    {
        
        NSString* acct_name=[dic objectForKey:@"account_type"];
        
        if ([acct_name isEqualToString:@"CC"])
        {
            NSString* account_type_desc=[dic objectForKey:@"card_number"];
            if (account_type_desc.length==0 || [account_type_desc isEqualToString:@" "] || [account_type_desc isEqualToString:nil])
            {
                account_type_desc=@"N/A";
                [a addObject:account_type_desc];
            }
            else
            {
                [a addObject:account_type_desc];
            }
            
            NSString* embossed_name=[dic objectForKey:@"embossed_name"];
            if (embossed_name.length==0 || [embossed_name isEqualToString:@" "] || [embossed_name isEqualToString:nil])
            {
                embossed_name=@"N/A";
                [a addObject:embossed_name];
            }
            else
            {
                [a addObject:embossed_name];
            }
            
            NSString* card_number=@"Credit Card";   //[dic objectForKey:@"account_type_desc"];
            if (card_number.length==0 || [card_number isEqualToString:@" "] || [card_number isEqualToString:nil])
            {
                card_number=@"N/A";
                [a addObject:card_number];
            }
            else
            {
                [a addObject:card_number];
            }
            
            
            [a addObject:@""];
            
            
            NSString* expiry_date=[dic objectForKey:@"expiry_date"];
            if (expiry_date.length==0 || [expiry_date isEqualToString:@" "] || [expiry_date isEqualToString:nil])
            {
                expiry_date=@"";
                [a addObject:expiry_date];
            }
            else
            {
                [a addObject:expiry_date];
            }
            
            
            NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
            [arr_atm_card addObject:bbb];
            
            [a removeAllObjects];
        }
        
    }
    
    self.transitionController = [[TransitionDelegate alloc] init];
    self.view.backgroundColor=[UIColor colorWithRed:229/255.0 green:233/255.0 blue:242/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cardtypeddbtntapped:(UIButton *)sender
{
    if(self.cardlist.hidden)
    {
        self.cardlist.hidden=false;
    }
    else
    {
        self.cardlist.hidden=true;
    }
}

-(void) getAtmCardList:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //NSLog(@"user id  %@",gblclass.user_id);
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"abc"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"hCode",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    
    [manager POST:@"ATMCardList" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return;
                  
              }
              else  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  
                  gblclass.atmcardlist =[(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"GnbATMCardList"];
                  
                  
                  for (dic in gblclass.atmcardlist)
                  {
                      
                      NSString* ACCOUNT_NAME=[dic objectForKey:@"ATM_CARD_NO"];
                      if (ACCOUNT_NAME.length==0 || [ACCOUNT_NAME isEqualToString:@" "] || [ACCOUNT_NAME isEqualToString:nil])
                      {
                          ACCOUNT_NAME=@"N/A";
                          [a addObject:ACCOUNT_NAME];
                      }
                      else
                      {
                          [a addObject:ACCOUNT_NAME];
                      }
                      
                      NSString* TT_ACCESS_KEY=[dic objectForKey:@"ATM_CUST_NAME"];
                      if (TT_ACCESS_KEY.length==0 || [TT_ACCESS_KEY isEqualToString:@" "] || [TT_ACCESS_KEY isEqualToString:nil])
                      {
                          TT_ACCESS_KEY=@"N/A";
                          [a addObject:TT_ACCESS_KEY];
                      }
                      else
                      {
                          [a addObject:TT_ACCESS_KEY];
                      }
                      
                      NSString* ATM_TYPE=[dic objectForKey:@"ATM_TYPE"];
                      if ([ATM_TYPE isEqualToString:@""])
                      {
                          ATM_TYPE=@"N/A";
                          [a addObject:ATM_TYPE];
                      }
                      else
                      {
                          [a addObject:ATM_TYPE];
                      }
                      
                      
                      NSString* ACCOUNT_NO=[dic objectForKey:@"ACCOUNT_NO"];
                      if (ACCOUNT_NO == (NSString *)[NSNull null])
                      {
                          ACCOUNT_NO=@"";
                          [a addObject:ACCOUNT_NO];
                      }
                      else
                      {
                          [a addObject:ACCOUNT_NO];
                      }
                      
                      NSString* EXPIRY_DATE=[dic objectForKey:@"EXPIRY_DATE"];
                      if (EXPIRY_DATE == (NSString *)[NSNull null])
                      {
                          EXPIRY_DATE=@"";
                          [a addObject:EXPIRY_DATE];
                      }
                      else
                      {
                          [a addObject:EXPIRY_DATE];
                      }
                      
                      NSString *bbb = [a componentsJoinedByString:@"|"];    //returns a pointer to NSString
                      [arr_atm_card addObject:bbb];
                      
                      //NSLog(@"%@", bbb);
                      [a removeAllObjects];
                      
                  }
                  
                  
                  //NSLog(@"Done");
                  //   [self parsearray];
                  [hud hideAnimated:YES];
                  
                  [self.cardtableview reloadData];
                  
              }
              else
              {
                  
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  
//                  gblclass.custom_alert_msg=@"Retry";
//                  gblclass.custom_alert_img=@"0";
//                  
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              
          }];
}



-(void) mob_App_Logout:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        
        //NSLog(@"%@",gblclass.user_id);
        //NSLog(@"%@",gblclass.Udid);
        //NSLog(@"%@",gblclass.M3sessionid);
        //NSLog(@"%@",gblclass.token);
        //NSLog(@"%@",gblclass.header_user);
        //NSLog(@"%@",gblclass.header_pw);
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  [self custom_alert:@"Please try again later" :@"0"];
                  
//                  gblclass.custom_alert_msg=@"Please try again later.";
//                  gblclass.custom_alert_img=@"0";
//
//                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                  vc.view.alpha = alpha1;
//                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please try again later" :@"0"];
        
//        gblclass.custom_alert_msg=@"Please try again later.";
//        gblclass.custom_alert_img=@"0";
//
//        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
//        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//        vc.view.alpha = alpha1;
//        [self presentViewController:vc animated:NO completion:nil];
        
    }
}



-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
}

 

-(void)parsearray
{
    
    cardname = [[NSMutableArray alloc]init];
    cardnumber= [[NSMutableArray alloc]init];
    cardtype= [[NSMutableArray alloc]init];
    cardstatus = [[NSMutableArray alloc]init];
    
    NSUInteger *set;
    //NSLog(@"this is count of global array %lu",(unsigned long)[ gblclass.atmcardlist count]);
    
    for (dic in gblclass.atmcardlist)
    {
        set = [gblclass.atmcardlist indexOfObject:dic];
        //NSLog(@"%d",set);
        GnbATMCardList*  classObj = [[GnbATMCardList alloc] initWithDictionary:dic];
        
        
        [cardname addObject:classObj.aTMCUSTNAME];
        [cardnumber addObject:classObj.aTMCARDNO];
        
        [cardstatus addObject:classObj.status];
        
        
        [cardtype addObject:classObj.aTMTYPE];
        
        
    }
    self.txtcardname.text = [cardname objectAtIndex:0];
    self.txtcardnumber.text = [cardnumber objectAtIndex:0];
    self.txtcardstatus.text = [cardstatus objectAtIndex:0];
    self.txtcardtype.text = [cardtype objectAtIndex:0];
    
    [self.cardtableview reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        
        
        self.cardlist.hidden=true;
        
        [tableView cellForRowAtIndexPath:indexPath].selected=false;
        
        if ([arr_atm_card count] >0)
        {
            split = [[arr_atm_card objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            if ([[split objectAtIndex:2] isEqualToString:@"Credit Card"])
            {
                [self checkinternet];
                if (netAvailable)
                {
                    
                    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                    vc = [storyboard instantiateViewControllerWithIdentifier:@"acct_summary_new"];
                    [self presentViewController:vc animated:NO completion:nil];
                    
                    CATransition *transition = [CATransition animation];
                    transition.duration = 0.3;
                    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                    transition.type = kCATransitionPush;
                    transition.subtype = kCATransitionFromRight;
                    [ self.view.window. layer addAnimation:transition forKey:nil];
                    
                }
            }
            
        }
        
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
    
    //    self.txtcardname.text = [cardname objectAtIndex:indexPath.row];
    //    self.txtcardnumber.text = [cardnumber objectAtIndex:indexPath.row];;
    //    self.txtcardstatus.text = [cardstatus objectAtIndex:indexPath.row];;
    //    self.txtcardtype.text = [cardtype objectAtIndex:indexPath.row];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_atm_card count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    split = [[arr_atm_card objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
    
    
    //for Image ::
    
    //UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 48, 46)];
    UIImageView* imv=(UIImageView*)[cell viewWithTag:6];
    
    //imv.image=[UIImage imageNamed:@""];
    
    // imv.layer.cornerRadius=20;
    //imv.layer.borderWidth=1.0;
    imv.layer.masksToBounds = YES;
    if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
    {
        //imv.backgroundColor=[UIColor colorWithRed:168/255.0 green:211/255.0 blue:255/255.0 alpha:1.0];
        [imv setImage:[UIImage imageNamed:@"Credit-Card-1.png"]];
    }
    else //if([[split objectAtIndex:2] isEqualToString:@"Primary"])
    {
        // imv.backgroundColor=[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        [imv setImage:[UIImage imageNamed:@"Primary-Card-1.png"]];
    }
    //    else
    //    {
    //        [imv setImage:[UIImage imageNamed:@""]];
    //    }
    
    [cell.contentView addSubview:imv];
    
    
    //for Image ::
    
  //UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 48, 46)];
    UIImageView* imv_visa=(UIImageView*)[cell viewWithTag:7];
    
    //imv.image=[UIImage imageNamed:@""];
    
    // imv.layer.cornerRadius=20;
    //imv.layer.borderWidth=1.0;
    imv_visa.layer.masksToBounds = YES;
    
    //NSLog(@" as %@",[[split objectAtIndex:0]substringToIndex:1]);
    
    
    if ([[[split objectAtIndex:0]substringToIndex:1] isEqualToString:@"5"])
    {
        [imv_visa setImage:[UIImage imageNamed:@"Master-Card.png"]];
    }
    else
    {
        [imv_visa setImage:[UIImage imageNamed:@"Visa.png"]];
    }
    
    [cell.contentView addSubview:imv_visa];
    
    
    //  tick selected ....
    
    //    if (indexPath.row==1)
    //    {
    //        //for Image ::
    //
    //        //UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 48, 46)];
    //        UIImageView* imv_check=(UIImageView*)[cell viewWithTag:8];
    //
    //        //imv.image=[UIImage imageNamed:@""];
    //
    //        // imv.layer.cornerRadius=20;
    //        //imv.layer.borderWidth=1.0;
    //        imv_check.layer.masksToBounds = YES;
    //
    //        [imv_check setImage:[UIImage imageNamed:@"Check_atm.png"]];
    //
    //         [cell.contentView addSubview:imv_check];
    //    }
    
    
    
    label=(UILabel*)[cell viewWithTag:2];
    label.text=[split objectAtIndex:2];
    //label.font=[UIFont systemFontOfSize:12];
    if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
    {
        label.textColor=[UIColor colorWithRed:2/255.0 green:124/255.0 blue:193/255.0 alpha:1.0];
    }
    else
    {
        label.textColor=[UIColor colorWithRed:25/255.0 green:23/255.0 blue:26/255.0 alpha:1.0];
    }
    [cell.contentView addSubview:label];
    
    //acct no
    label=(UILabel*)[cell viewWithTag:3];
    label.text=[split objectAtIndex:0];
    
    NSMutableString *atm_num = [NSMutableString new];
    [atm_num setString:[split objectAtIndex:0]];
    
    for (int p = 0; p < [atm_num length]; p++)
    {
        if (p%5 == 0)
        {
            [atm_num insertString:@" " atIndex:p];
        }
    }
    
    //NSLog(@"%@", atm_num);
    
    
    label.text=atm_num;
    if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
    {
        label.textColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:199/255.0 alpha:1.0];
    }
    else
    {
        label.textColor=[UIColor colorWithRed:25/255.0 green:23/255.0 blue:26/255.0 alpha:1.0];
    }
    [cell.contentView addSubview:label];
    
    //name
    label=(UILabel*)[cell viewWithTag:4];
    label.text=[split objectAtIndex:1];
    if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
    {
        label.textColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:199/255.0 alpha:1.0];
    }
    else
    {
        label.textColor=[UIColor colorWithRed:25/255.0 green:23/255.0 blue:26/255.0 alpha:1.0];
    }
    //label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    //Acct No.
    label=(UILabel*)[cell viewWithTag:10];
    if ([[split objectAtIndex:4] isEqualToString:@""])
    {
        
        if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
        {
            label.textColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:199/255.0 alpha:1.0];
        }
        else
        {
            label.textColor=[UIColor colorWithRed:25/255.0 green:23/255.0 blue:26/255.0 alpha:1.0];
        }
        
        label.text=@"";
    }
    else
    {
        
        if ([[split objectAtIndex:2] isEqualToString:@"Supplementary"])
        {
            label.textColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:199/255.0 alpha:1.0];
        }
        else
        {
            label.textColor=[UIColor colorWithRed:25/255.0 green:23/255.0 blue:26/255.0 alpha:1.0];
        }
        
        label.text=[NSString stringWithFormat:@"Expiry : %@",[split objectAtIndex:4]];
    }
    
    //label.font=[UIFont systemFontOfSize:12];
    [cell.contentView addSubview:label];
    
    
    //Acct No.
    //    label=(UILabel*)[cell viewWithTag:11];
    //    label.text=[split objectAtIndex:3];
    //    //label.font=[UIFont systemFontOfSize:12];
    //    [cell.contentView addSubview:label];
    
    return cell;
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}


-(IBAction)btn_more:(id)sender
{
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}


-(IBAction)btn_addpayee:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
    [self presentViewController:vc animated:YES completion:nil];
}


-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(IBAction)btn_logout:(id)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                     message:Logout
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
    
    [alert1 show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //NSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        
        
        [self mob_App_Logout:@""];
        
        //Do something
        //NSLog(@"1");
    }
    else if(buttonIndex == 1)
    {
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    
}


-(IBAction)btn_Pay:(id)sender
{
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_account:(id)sender
{
    
    //    [self checkinternet];
    //    if (netAvailable)
    //    {
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
    //  }
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_txtcardname resignFirstResponder];
    [_txtcardnumber resignFirstResponder];
    [_txtcardtype resignFirstResponder];
    [_txtcardstatus resignFirstResponder];
    
    _cardlist .hidden=true;
    
}

-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_req_atm:(id)sender
{
    
    CATransition *transition = [ CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window. layer addAnimation:transition forKey:nil];
    
    
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"req_atm_card"];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        [self getAtmCardList:@""];
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    
     [self.connection cancel];
    
    [self getAtmCardList:@""];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}


- (NSData *)skabberCert
{
    NSLog(@"%@", gblclass.SSL_name);
    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
//    NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////




#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            gblclass.custom_alert_msg=statusString;
            gblclass.custom_alert_img=@"0";
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.view.alpha = alpha1;
            [self presentViewController:vc animated:NO completion:nil];
            
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}


-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

