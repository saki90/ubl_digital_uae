//
//  AddPayee.h
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"
#import "Reachability.h"

@interface BillManagement: UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    //    IBOutlet UIPickerView* picker;
    //    IBOutlet UIView* picker_view;
    IBOutlet UILabel* lbl_heading;
    IBOutlet UIView* vw_table;
    IBOutlet UILabel* lbl_table_heading;
    

    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}

@property (nonatomic, strong) TransitionDelegate *transitionController;


@property (nonatomic, strong) IBOutlet UITextField* txtbillservice;
@property (nonatomic, strong) IBOutlet UITextField* txtbilltype;
@property (nonatomic, strong) IBOutlet UITextField* txtcompany;
@property (nonatomic, strong) IBOutlet UITextField* txt_service;
@property (nonatomic, strong) IBOutlet UITextField* txtphonenumber;

@property (nonatomic, strong) IBOutlet UITextField* normaltxtnick;

@property (nonatomic, strong) IBOutlet UITextField* mobiletxtnick;
@property (nonatomic, strong) IBOutlet UITextField* retypemobilenumbernick;

//@property (nonatomic, strong) IBOutlet UIView* billserviceview;
@property (nonatomic, strong) IBOutlet UIView* billtype;
@property (nonatomic, strong) IBOutlet UIView* company;

//@property (nonatomic, strong) IBOutlet UITableView* tableviewbillservice;
@property (nonatomic, strong) IBOutlet UITableView* tableviewbilltype;
@property (nonatomic, strong) IBOutlet UITableView* tableviewcompany;
@property (nonatomic, strong) IBOutlet UITableView* table_service;

@property (nonatomic, strong) IBOutlet UIView* normalview;
@property (nonatomic, strong) IBOutlet UIView* mobileview;

@property (nonatomic, strong) IBOutlet UIButton* dd3;


@end

