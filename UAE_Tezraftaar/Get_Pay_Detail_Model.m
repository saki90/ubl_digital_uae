//
//  Get_Pay_Detail_Model.m
//
//  Created by   on 17/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Get_Pay_Detail_Model.h"
#import "OutdtDataset.h"


NSString *const kGet_Pay_Detail_ModelResponse = @"Response";
NSString *const kGet_Pay_Detail_ModelOutdtDataset = @"outdtDataset";
NSString *const kGet_Pay_Detail_ModelOutdtData = @"outdtData";
NSString *const kGet_Pay_Detail_ModelStrReturnMessage = @"strReturnMessage";
NSString *const kGet_Pay_Detail_ModelStrhCode = @"strhCode";


@interface Get_Pay_Detail_Model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Get_Pay_Detail_Model

@synthesize response = _response;
@synthesize outdtDataset = _outdtDataset;
@synthesize outdtData = _outdtData;
@synthesize strReturnMessage = _strReturnMessage;
@synthesize strhCode = _strhCode;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.response = [self objectOrNilForKey:kGet_Pay_Detail_ModelResponse fromDictionary:dict];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kGet_Pay_Detail_ModelOutdtDataset]];
            self.outdtData = [self objectOrNilForKey:kGet_Pay_Detail_ModelOutdtData fromDictionary:dict];
            self.strReturnMessage = [self objectOrNilForKey:kGet_Pay_Detail_ModelStrReturnMessage fromDictionary:dict];
            self.strhCode = [self objectOrNilForKey:kGet_Pay_Detail_ModelStrhCode fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.response forKey:kGet_Pay_Detail_ModelResponse];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kGet_Pay_Detail_ModelOutdtDataset];
    [mutableDict setValue:self.outdtData forKey:kGet_Pay_Detail_ModelOutdtData];
    [mutableDict setValue:self.strReturnMessage forKey:kGet_Pay_Detail_ModelStrReturnMessage];
    [mutableDict setValue:self.strhCode forKey:kGet_Pay_Detail_ModelStrhCode];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.response = [aDecoder decodeObjectForKey:kGet_Pay_Detail_ModelResponse];
    self.outdtDataset = [aDecoder decodeObjectForKey:kGet_Pay_Detail_ModelOutdtDataset];
    self.outdtData = [aDecoder decodeObjectForKey:kGet_Pay_Detail_ModelOutdtData];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kGet_Pay_Detail_ModelStrReturnMessage];
    self.strhCode = [aDecoder decodeObjectForKey:kGet_Pay_Detail_ModelStrhCode];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_response forKey:kGet_Pay_Detail_ModelResponse];
    [aCoder encodeObject:_outdtDataset forKey:kGet_Pay_Detail_ModelOutdtDataset];
    [aCoder encodeObject:_outdtData forKey:kGet_Pay_Detail_ModelOutdtData];
    [aCoder encodeObject:_strReturnMessage forKey:kGet_Pay_Detail_ModelStrReturnMessage];
    [aCoder encodeObject:_strhCode forKey:kGet_Pay_Detail_ModelStrhCode];
}

- (id)copyWithZone:(NSZone *)zone
{
    Get_Pay_Detail_Model *copy = [[Get_Pay_Detail_Model alloc] init];
    
    if (copy) {

        copy.response = [self.response copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
        copy.strhCode = [self.strhCode copyWithZone:zone];
    }
    
    return copy;
}


@end
