//
//  SettingView.h
//  ubltestbanking
//
//  Created by ammar on 30/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitionDelegate.h"


@interface SettingView : UIViewController


@property (nonatomic, strong) TransitionDelegate *transitionController;

//@property (weak, nonatomic) IBOutlet UIWebView *webView;

-(IBAction)btn_back:(id)sender;

@end

