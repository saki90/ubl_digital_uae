//
//  Transaction_detail.h
//
//  Created by   on 23/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OutdtDataset;

@interface Transaction_detail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *response;
@property (nonatomic, strong) OutdtDataset *outdtDataset;
@property (nonatomic, assign) id outdtData;
@property (nonatomic, strong) NSString *strReturnMessage;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
