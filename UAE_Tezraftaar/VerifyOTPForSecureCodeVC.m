//
//  VerifyOTPForSecureCodeVC.m
//  ubltestbanking
//
//  Created by Asim Khan on 7/20/18.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "VerifyOTPForSecureCodeVC.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"
#import "ubltestbanking-Swift.h"
#import "API_Handler.h"
#import "OtpTextField.h"


@interface VerifyOTPForSecureCodeVC () {
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSUserDefaults *defaults;
    
    NSMutableArray *arr_pin_pattern;
    NSMutableArray *arr_pw_pin;
    NSString *str_pw;
    NSString *str_Otp;
    int txt_nam;
    BOOL isContinued;
    NSString *callingResource;
    NSString *latitude;
    NSString *longitude;
    NSString* ssl_count;
    NSMutableArray* a;
    NSString*  first_time_chk;
    NSString *stepedVC;
    NSString *t_n_c,* device_jb_chk;
}

@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIView *navBar;
@property (weak, nonatomic) IBOutlet UIView *tabBar;
@property (weak, nonatomic) IBOutlet UILabel *cntr_lbl_heading;

- (IBAction)btn_back:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;


@end

@implementation VerifyOTPForSecureCodeVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    ssl_count = @"0";
    
    // NSLog(@"%@",gblclass.mobile_number_onbording);
    NSString *subStr = [gblclass.mobile_number_onbording substringWithRange:NSMakeRange(7,4)];
    
    self.cntr_lbl_heading.text = [NSString stringWithFormat:@"Please enter the One Time Password (OTP) sent to your mobile number *******%@ via SMS.",subStr];
    
    arr_pin_pattern=[[NSMutableArray alloc] init];
    arr_pw_pin=[[NSMutableArray alloc] init];
    
    arr_pin_pattern=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    arr_pw_pin=[[NSMutableArray alloc] initWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6"]];
    
    
    txt_1.OtpDelegate = self;
    txt_2.OtpDelegate = self;
    txt_3.OtpDelegate = self;
    txt_4.OtpDelegate = self;
    txt_5.OtpDelegate = self;
    txt_6.OtpDelegate = self;
    
    
    txt_1.borderStyle = UITextBorderStyleNone;
    txt_2.borderStyle = UITextBorderStyleNone;
    txt_3.borderStyle = UITextBorderStyleNone;
    txt_4.borderStyle = UITextBorderStyleNone;
    txt_5.borderStyle = UITextBorderStyleNone;
    txt_6.borderStyle = UITextBorderStyleNone;
    
    [txt_1 setBackgroundColor:[UIColor clearColor]];
    [txt_2 setBackgroundColor:[UIColor clearColor]];
    [txt_3 setBackgroundColor:[UIColor clearColor]];
    [txt_4 setBackgroundColor:[UIColor clearColor]];
    [txt_5 setBackgroundColor:[UIColor clearColor]];
    [txt_6 setBackgroundColor:[UIColor clearColor]];
    
    // Cheks the jail break status.
    [self checkJailbreakStatus];
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"]) {
        [self Play_bg_video];
        [self playerStartPlaying];
        self.navBar.backgroundColor = [UIColor clearColor];
        self.tabBar.backgroundColor = [UIColor clearColor];
        self.progressBar.hidden = YES;
        callingResource = @"reset";
    } else if ([gblclass.parent_vc_onbording isEqualToString:@"RegisterAccountVC"]) {
        self.progressBar.hidden = NO;
        callingResource = @"request";
    } else if([gblclass.parent_vc_onbording isEqualToString:@"SecureCodeSignupVC"]) {
         self.cntr_lbl_heading.text = @"A one time password (OTP) has been sent to your phone. Please enter the 6 digits for confirmation.";
    }
    
//    locationManager.delegate = self;
//    [locationManager startUpdatingLocation];
    gblclass.controllers_Refrence_Stack = [[NSMutableArray alloc] init];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [txt_1 becomeFirstResponder];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkJailbreakStatus {
    t_n_c = [defaults valueForKey:@"t&c"];
    device_jb_chk = [defaults valueForKey:@"isDeviceJailBreaked"];
    
    if ([t_n_c isEqualToString:@"2"]) {
        t_n_c = @"1";
    }
    
    if ([device_jb_chk isEqualToString:@"2"]) {
        device_jb_chk = @"1";
    }
}

-(void)Play_bg_video {
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [self.avplayer play];
}


//- (void)locationManager:(CLLocationManager *)manager
//     didUpdateLocations:(NSArray<CLLocation *> *)locations API_AVAILABLE(ios(6.0), macos(10.9)) {
//    
//    [locationManager stopUpdatingLocation];
//    latitude = [NSString stringWithFormat:@"%f",locations.lastObject.coordinate.latitude];
//    longitude = [NSString stringWithFormat:@"%f",locations.lastObject.coordinate.longitude];
//    
//    
//}


-(IBAction)btn_next:(id)sender {
    
    if ([txt_1.text isEqualToString:@""] ||
        [txt_2.text isEqualToString:@""] ||
        [txt_3.text isEqualToString:@""] ||
        [txt_4.text isEqualToString:@""] ||
        [txt_5.text isEqualToString:@""] ||
        [txt_6.text isEqualToString:@""]) {
        [self custom_alert:@"Enter All OTP Pins" :@"0"];
    }
    else {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"otp";
            [self SSL_Call];
        }
    }
    
}




-(void) VerifyOTP_last:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        //   NSLog(@"%@",gblclass.request_id_onbording);
        
        gblclass.otpPin = str_Otp;
        NSLog(@"%@",gblclass.conti_session_id);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:[NSString stringWithFormat:@"%@|%@",gblclass.M3sessionid,gblclass.conti_session_id]],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:str_Otp],
                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"Temp_RequestId",
                                                                       @"strOTPPIN",
                                                                       @"strMobileNo", nil]];
        
        
        
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"VerifyOTP" parameters:dictparam progress:nil
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  //                  [self clearOTP];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                      
                      //Apply acct check
                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      [defaults setValue:@"0" forKey:@"apply_acct"];
                      [defaults synchronize];
                      [self GetOnBoardProducts:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"2"]) {
                      //Apply acct check
                      
                      //                      {
                      //                          RequestId = 1087;
                      //                          Response = 2;
                      //                          SRNumber = "";
                      //                          strReturnMessage = InProcess;
                      //                      }
                      
                      isContinued = YES;
                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      [defaults setValue:@"0" forKey:@"apply_acct"];
                      [defaults synchronize];
                      [self GetOnBoardProducts:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"3"]) {
                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      [defaults setValue:[encrypt encrypt_Data:[dic objectForKey:@"SRNumber"]] forKey:@"sr_num"];
                      [defaults setValue:@"1" forKey:@"apply_acct"];
                      [defaults synchronize];
                      
                      [hud hideAnimated:YES];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"5"]) {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-3"] || [[dic objectForKey:@"Response"] isEqualToString:@"-4"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"Ok"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_left];
                      
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"RegisterAccountVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      
                      //                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [self clearOTP];
        [hud hideAnimated:YES];
        [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
}



-(void) VerifyOTP:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSLog(@"%@",gblclass.request_id_onbording);
        
        gblclass.otpPin = str_Otp;
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                                                       [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:str_Otp],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"strBranchCode",
                                                                       @"strAccountNumber",
                                                                       @"strOTPPIN",
                                                                       @"strTTAccessKey",
                                                                       @"Channel",
                                                                       @"Device_ID",
                                                                       @"Token", nil]];
        
        //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
        //                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
        //                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
        //                                                                       [encrypt encrypt_Data:gblclass.Udid],
        //                                                                       [encrypt encrypt_Data:gblclass.token],
        //                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
        //                                                                       [encrypt encrypt_Data:str_Otp],
        //                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording], nil]
        //
        //                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
        //                                                                       @"strSessionId",
        //                                                                       @"IP",
        //                                                                       @"Device_ID",
        //                                                                       @"Token",
        //                                                                       @"Temp_RequestId",
        //                                                                       @"strOTPPIN",
        //                                                                       @"strMobileNo", nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"VerifyOTPForDigitalAccountReq" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  //                  [self clearOTP];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      
                      [self VerifyOTP_last:@""];
                      
                      //                      //Apply acct check
                      //                      [self saveUserdata:gblclass.request_id_onbording]; //[dic objectForKey:@"RequestId"]];
                      //                      [defaults setValue:@"0" forKey:@"apply_acct"];
                      //                      [defaults synchronize];
                      //                      [self GetOnBoardProducts:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"2"])
                  {
                      //Apply acct check
                      
                      //                      {
                      //                          RequestId = 1087;
                      //                          Response = 2;
                      //                          SRNumber = "";
                      //                          strReturnMessage = InProcess;
                      //                      }
                      
                      isContinued = YES;
                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      [defaults setValue:@"0" forKey:@"apply_acct"];
                      [defaults synchronize];
                      [self GetOnBoardProducts:@""];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"3"]) {
                      [self saveUserdata:[dic objectForKey:@"RequestId"]];
                      [defaults setValue:[encrypt encrypt_Data:[dic objectForKey:@"SRNumber"]] forKey:@"sr_num"];
                      [defaults setValue:@"1" forKey:@"apply_acct"];
                      [defaults synchronize];
                      
                      [hud hideAnimated:YES];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"5"]) {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-3"] || [[dic objectForKey:@"Response"] isEqualToString:@"-4"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"Ok"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_left];
                      [self dismissViewControllerAnimated:NO completion:nil];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  }
                  else
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [self clearOTP];
        [hud hideAnimated:YES];
        [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
}



-(void)GetOnBoardProducts:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        NSLog(@"%@",gblclass.request_id_onbording);
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token",nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardProducts" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  //                  [refreshController endRefreshing];
                  //                  [hud hideAnimated:YES];
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                      gblclass.getOnBoardProducts = dic;
                      
                      NSString *stepedVC = @"";
                      if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"])
                      {
                          stepedVC = @"CreatePinVC";
                          [hud hideAnimated:YES];
                          [self slide_right];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      else if (isContinued)
                      {
                          [self GetOnBoardData:@""];
                      }
                      else
                      {
                          stepedVC = @"SelectAccountTypeVC";
                          [hud hideAnimated:YES];
                          [self slide_right];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      
                      //                      [txt_1 becomeFirstResponder];
                      
                      //                      [hud hideAnimated:YES];
                      //                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }
                  else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
                  {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      return ;
                      
                  } else {
                      [self clearOTP];
                      [hud hideAnimated:YES];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  [txt_1 becomeFirstResponder];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [self clearOTP];
        [hud hideAnimated:YES];
        [txt_1 becomeFirstResponder];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void)GetOnBoardData:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token", nil]];
        
        
        //    strSessionId:
        //    IP:
        //    Device_ID:
        //    RequestId:
        //    Token:
        //    strCNIC:
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
                      
                      if (!([[dic objectForKey:@"SelectedAccount"]  isKindOfClass:[NSNull class]] || [[dic objectForKey:@"SelectedAccount"] count] == 0)) {
                          [[dic objectForKey:@"SelectedAccount"] enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger index, BOOL *stop) {
                              
                              NSDictionary *itemDictionary = @{
                                                               @"ACCOUNTTYPE_ID" : [item objectForKey:@"ACCOUNTTYPE_ID"],
                                                               @"NAME" : [item objectForKey:@"NAME"],
                                                               @"DESCRIPTION" : [item objectForKey:@"DESCRIPTION"],
                                                               @"REQUIREMENTS" : [item objectForKey:@"REQUIREMENTS"]
                                                               };
                              
                              [gblclass.selected_services_onbording addObject:itemDictionary];
                          }];
                      }
                      
                      if (!([[dic objectForKey:@"SelectedProducts"]  isKindOfClass:[NSNull class]] || [[dic objectForKey:@"SelectedProducts"] count] == 0)) {
                          
                          [[dic objectForKey:@"SelectedProducts"] enumerateObjectsUsingBlock:^(NSDictionary *item, NSUInteger index, BOOL *stop) {
                              NSDictionary *itemDictionary = @{
                                                               @"PRODUCT_ID" : [item objectForKey:@"PRODUCT_ID"],
                                                               @"NAME" : [item objectForKey:@"NAME"],
                                                               @"DESCRIPTION" : [item objectForKey:@"DESCRIPTION"],
                                                               @"REQUIREMENTS" : [item objectForKey:@"REQUIREMENTS"]
                                                               };
                              
                              [gblclass.selected_services_onbording addObject:itemDictionary];
                          }];
                      }
                      
                      
                      NSDictionary *otherInfo = [[dic objectForKey:@"Request"] firstObject];
                      
                      gblclass.user_name_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"NAME"]];
                      gblclass.cnic_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"CNIC"]];
                      gblclass.email_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"EMAIL"]];
                      gblclass.mobile_number_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"MOBILE"]];
                      gblclass.step_no_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"SCREEN_ID"]];
                      gblclass.branch_code_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"BRANCH"]];
                      gblclass.mode_of_meeting = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"MODE_OF_MEETING_ID"]];
                      gblclass.preferred_time = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"PREFERRED_TIME_ID"]];
                      gblclass.debitcard_name_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"NAME_ON_CARD"]]; //
                      gblclass.nationality_onbording = [NSString stringWithFormat:@"%@",[otherInfo objectForKey:@"PK_CITIZEN"]];
                      gblclass.request_id_onbording = [NSString stringWithFormat:@"%@", [otherInfo objectForKey:@"REQUEST_ID"]];
                      gblclass.selcted_branch_address = [NSString stringWithFormat:@"%@", [otherInfo objectForKey:@"Branch_Address"]];
                      
//                      gblclass.selcted_branch_contact = [NSString stringWithFormat:@"%@", [otherInfo objectForKey:@"REQUEST_ID"]];
                      gblclass.isRedirected = YES;
                      //                      gblclass. = [otherInfo objectForKey:@"CITY"];
                      
                      
                      //                      {
                      //                          Request =     (
                      //                                         {
                      //                                             BRANCH = "<null>";
                      //                                             CHANNELID = 1;
                      //                                             CITY = "<null>";
                      //                                             CNIC = 4240169592915;
                      //                                             EMAIL = "email@email.com";
                      //                                             MOBILE = 03333104534;
                      //                                             "MODE_OF_MEETING_ID" = "<null>";
                      //                                             "ModeofMeeting_Name" = "<null>";
                      //                                             NAME = asim;
                      //                                             "NAME_ON_CARD" = "<null>";
                      //                                             OTPVERIFIED = 0;
                      //                                             "PK_CITIZEN" = 0;
                      //                                             "PREFERRED_TIME_ID" = "<null>";
                      //                                             "Preferred_Name" = "<null>";
                      //                                             "REQUEST_ID" = 544;
                      //                                             "SCREEN_ID" = 1;
                      //                                         }
                      //                                         );
                      //                          Response = 1;
                      //                          SelectedAccount =     (
                      //                                                 {
                      //                                                     "ACCOUNTTYPE_ID" = 1;
                      //                                                     "AccountType_Name" = "Current Account";
                      //                                                     "COMPLETION_STATUS" = 0;
                      //                                                     DESCRIPTION = "A Pak Rupee non-remunerative checking account that offers a range of benefits and free services allowing transactional flexibility to the modern day customer.\n\nKey Benefits\n* Checking account with no limits on transfers\n* No profit payout\n* Unlimited transactions without transaction costs ";
                      //                                                     REMARKS = "<null>";
                      //                                                     "REQUEST_ACCOUNTTYPE_ID" = 255;
                      //                                                     REQUIREMENTS = "* CNIC Copy\n* proof of income\n* Deposit (Rs. !00, 500 or 1000)";
                      //                                                     "TRACKING_ID" = 0;
                      //                                                 }
                      //                                                 );
                      //                          SelectedProducts =     (
                      //                          );
                      //                          strReturnMessage = "Executed Successful";
                      //                      }
                      
                      
                      
                      //                      All the data provided by customer till the stage including stepno as follows:
                      //                      {
                      //                          "Accounts":[
                      //                          {"id":"1","Name":"Current","Description":"Current Account","Document":"NIC^Passport Size Picture"}],
                      //                          "selectedproducts":[
                      //                          {"id":"1","Name":"Credit Card","Description":"Credit Card","Document":"NIC^Passport Size Picture"},
                      //                          {"id":"10","Name":"Personal Loan","Description":"Personal Loan","Document":"NIC^Passport Size Picture"}],
                      //                          "otherinfo":
                      //                          [{"strBranchCode":"08090",
                      //                              "modeofmeeting":"R",
                      //                              "preferredtime":"M",
                      //                              "debitcardname":"Customer Name",
                      //                              "stepno":"5"
                      //                              "fullname":"Customer Name",
                      //                              "cnic":"4201019874561",
                      //                              "emailaddress":"a@b.com",
                      //                              "mobileno":"03213343344"
                      //                          }]
                      //                      }
                      
                      
                      NSString *stepedVC = @"";
                      switch ([gblclass.step_no_onbording integerValue]) {
                          case 0:
                              stepedVC = @"SelectAccountTypeVC";
                              break;
                          case 1:
                              //                              stepedVC = @"FindingLocationVC";
                              stepedVC = @"FindPlace_swift";
                          case 2:
                              stepedVC = @"FindPlace_swift";
                              mainStoryboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                              vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"FindPlace_swift"];
                              ((FindPlace *)vc).isSelcted = YES;
                              break;
                          case 3:
                              stepedVC = @"CustomizeDebitCardVC";
                              break;
                          default:
                              stepedVC = @"SelectAccountTypeVC";
                              break;
                      }
                      
                      //[hud hideAnimated:YES];
                      // [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      
                      if (gblclass.isLocationSelected) {
                          
                          
                          //                          UIStoryboard *main2 = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
                          //                          FindPlace * vc34 = ((FindPlace*)[main2 instantiateViewControllerWithIdentifier:@"FindPlace_swift"]);
                          //                          vc34.isSelcted = YES;
                          
                          //                              var isSelcted : Bool = false
                          //                              var slectedLatitude : String = ""
                          //                              var slectedLongitude : String = ""
                          [self presentViewController:vc animated:NO completion:nil];
                          
                      }
                      
                      //                      NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"selected_branch_code"]);
                      //                      NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"selected_branch_city"]);
                      
                      NSString *branch_code = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"selected_branch_code"];
                      NSString *branch_city = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"selected_branch_city"];
                      
                      if(![gblclass.branch_code_onbording isEqual:@""] && gblclass.branch_code_onbording != nil && ![gblclass.branch_code_onbording isKindOfClass:[NSNull class]]) {
                          
                          //  NSArray *arr =[[NSArray alloc] initWithObjects:[NSNumber numberWithInteger:98], nil];
                          
                          
                          NSDictionary *params =  @{@"fksyd":branch_city,
                                                    
                                                    @"n4ja3s":@"PAKISTAN",
                                                    
                                                    @"js6nwf":@"0",
                                                    
                                                    @"pan3ba":@"0",
                                                    
                                                    @"mstoaw":@"en",
                                                    
                                                    @"kisu87":@"me",
                                                    
                                                    @"matsw":@"UBL",
                                                    
                                                    @"mnakls":@"500",
                                                    
                                                    @"opmsta":@"0",
                                                    
                                                    @"makthya":@"distance",
                                                    
                                                    @"9msh":@"asc",
                                                    
                                                    @"3aqwved":@"true",
                                                    
                                                    @"7WdpTO": @"[98]"
                                                    
                                                    };
                          // [hud showAnimated:YES];
                          
                          [API_Handler get_branches:params onCompletionBlock:^(id responseObject) {
                              
                              
                              // NSLog(@"asdas");
                              
                              NSArray *branchData = [responseObject objectForKey:@"branches"];
                              
                              for (NSDictionary* branch in branchData) {
                                  
                                  
                                  NSString *longitude =  [[branch objectForKey:@"longitude"] stringValue];
                                  NSString *latitude = [[branch objectForKey:@"latitude"] stringValue];
                                  NSString *name = [branch objectForKey:@"name"];
                                  NSString *city = [branch objectForKey:@"city"] ;
                                  NSString *country = [branch objectForKey:@"country"];
                                  NSString *adress =[branch objectForKey:@"address"];
                                  NSString *distance = [branch objectForKey:@"distance"];
                                  
                                  if ([branch objectForKey:@"amenities"] != nil && ![[branch objectForKey:@"amenities"] isKindOfClass:[NSNull class]]) {
                                      
                                      NSDictionary *amenties = [branch objectForKey:@"amenities"];
                                      
                                      if ([amenties objectForKey:@"Branch Code"] != nil && ![[amenties objectForKey:@"Branch Code"] isKindOfClass:[NSNull class]]) {
                                          
                                          NSDictionary *branchDC = [amenties objectForKey:@"Branch Code"];
                                          
                                          if ([branchDC objectForKey:@"value"] != nil && ![[branchDC objectForKey:@"value"] isKindOfClass:[NSNull class]] ) {
                                              
                                              NSString *value = [branchDC objectForKey:@"value"];
                                              
                                              if (value.length < 4) {
                                                  value = [NSString stringWithFormat:@"0%@",value];
                                              }
                                              
                                              if ([branch_code intValue] == [value intValue]) {
                                                  
                                                  //                                                  UIStoryboard *main2 = [UIStoryboard storyboardWithName:@"Main2" bundle:[NSBundle mainBundle]];
                                                  //                                                  FindPlace * vc34 = ((FindPlace*)[main2 instantiateViewControllerWithIdentifier:@"FindPlace_swift"]);
                                                  //                                                  vc34.isSelcted = YES;
                                                  
                                                  
                                                  gblclass.selected_city = city;
                                                  gblclass.selcted_country = country;
                                                  gblclass.selctedLatitude = latitude;
                                                  gblclass.selectedLongitude = longitude;
                                                  gblclass.selcted_branch_code = value;
                                                  gblclass.selcted_marker_title = [NSString stringWithFormat:@"%@*%@",name,adress];
                                                  gblclass.isLocationSelected = YES;
                                                  gblclass.branch_name_onbording = name;
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      //your main thread task here
                                                      [self presentViewController:vc animated:NO completion:nil];
                                                  });
                                                  
                                                  //                                                  [self presentViewController:vc animated:NO completion:nil];
                                                  //[hud hideAnimated:YES];
                                                  return;
                                                  break;
                                                  
                                                  
                                              }
                                          }
                                      }
                                  }
                              }
                              // [hud hideAnimated:YES];
                              [self presentViewController:vc animated:NO completion:nil];
                              
                              
                          } onerror:^(NSError *error) {
                              [hud hideAnimated:YES];
                          }];
                          
                      } else {
                          [hud showAnimated:YES];
                          [self presentViewController:vc animated:NO completion:nil];
                      }
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      [self clearOTP];
                      
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  } else {
                      // [hud hideAnimated:YES];
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self clearOTP];
        [self custom_alert:@"Try again later." :@"0"];
    }
}

-(void)GenerateOTPForDARequest:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:@"1234"],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"Generate OTP"],
                                                                       [encrypt encrypt_Data:@"2"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"DIGITALACCOUNTREQ"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strAccessKey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:                                                                                                                                            time_out];
        [manager POST:@"GenerateOTPForDARequest" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      [self clearOTP];
                      [txt_1 becomeFirstResponder];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"1"];
                      
                      //                      gblclass.token = [dic objectForKey:@"Token"];
                      //                      gblclass.request_id_onbording = [dic objectForKey:@"RequestId"];
                      //
                      //                      [self slide_right];
                      //                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"SelectAccountTypeVC"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)generateOTPForAddition:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.user_id],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:gblclass.cnic],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@""],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:@"SECURE_CODE"],
                                                                       [encrypt encrypt_Data:@"SMS"],
                                                                       [encrypt encrypt_Data:@"NEWSIGNUP"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"userId",
                                                                       @"strSessionId",
                                                                       @"M3Key",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strBranchCode",
                                                                       @"strAccountNo",
                                                                       @"strAccessKey",
                                                                       @"strCallingOTPType",
                                                                       @"TTID",
                                                                       @"TC_AccessKey",
                                                                       @"Channel",
                                                                       @"strMessageType", nil]];
        
        
        
        
        [manager.requestSerializer setTimeoutInterval: time_out];
        [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [self slide_right];
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"VerifyOtpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  }  else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    }
    @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)InsertUpdateOnboardPin:(NSString *)strIndustry {
    
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.cnic_onbording],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.tracking_pin_onbording],
                                                                       [encrypt encrypt_Data:str_Otp],
                                                                       [encrypt encrypt_Data:@"0"],
                                                                       [encrypt encrypt_Data:callingResource], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",
                                                                       @"TrackingPin",
                                                                       @"strOTPPin",
                                                                       @"InvalidAttempts",
                                                                       @"CallingResource",  nil]];
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"InsertUpdateOnBoardPin" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  
                  [self clearOTP];
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"]) {
                      [self slide_right];
                      //                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                          [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [self clearOTP];
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

- (void)mobileAppSecureCodeVerifyStep2:(NSString *)strIndustry {

    @try {

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects: [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"iOS"],
                                    [encrypt encrypt_Data:device_jb_chk],
                                    [encrypt encrypt_Data:t_n_c],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:str_Otp],
                                    [encrypt encrypt_Data:gblclass.cnic],
                                    [encrypt encrypt_Data:gblclass.relation_id],  nil]

                                                              forKeys:[NSArray arrayWithObjects: @"strSessionId",
                                                                       @"IP",
                                                                       @"LogBrowserInfo",
                                                                       @"isRooted",
                                                                       @"isTncAccepted",
                                                                       @"strDeviceID",
                                                                       @"StrUserId",
                                                                       @"Token",
                                                                       @"strOTPPIN",
                                                                       @"strCNIC",
                                                                       @"strRelationshipId", nil]];

        // Input
        //        @"strSessionId"
        //        @"IP"
        //        @"LogBrowserInfo"
        //        @"isRooted"
        //        @"isTncAccepted"
        //        @"strDeviceID"
        //        @"StrUserId"
        //        @"Token"
        //        @"strOTPPIN"
        //        @"strCNIC"
        //        @"strRelationshipId"

        // output
        //        @"strReturnMessage
        //        @"UserName"

//        Service Response:
//        0/1           Successful
//        -1            Token not verified for this device/ Enter Valid OTP
//        -3
//        -1/-78/-79    UserException
//        -1/-12/-78    Exception
//        303           limit Downgrade due wrong OTP-PIN provide
//        304           OTP is incorrect




        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"MobileAppSecureCodeVerifyStep2" parameters:dictparam progress:nil

              success:^(NSURLSessionDataTask *task, id responseObject) {

                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];

                  if ([[dic objectForKey:@"Response"] integerValue] == -78 || [[dic objectForKey:@"Response"] integerValue] == -79) {

                      [hud hideAnimated:YES];
                      [self clearOTP];
//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
//                                                                          message:[dic objectForKey:@"strReturnMessage"]
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles:nil];
//
//                      [alertView show];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      [self presentViewController:vc animated:NO completion:nil];


                  } else if ([[dic objectForKey:@"Response"] integerValue] == 0 || [[dic objectForKey:@"Response"] integerValue] == 1) {
                      
                      [self clearOTP];
                      [self slide_right];
                      gblclass.sign_up_user = [dic objectForKey:@"UserName"];
                      
                      if ([gblclass.sign_up_user isEqualToString:@""] || gblclass.sign_up_user == (NSString *)[NSNull null]) {
                          
                          //new_signup_credential
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"new_signup_credential"];
                          [self presentViewController:vc animated:NO completion:nil];
                          
                      } else {
                          
                          //create_option
                          // [self slide_right];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"create_option"];
                          [self presentViewController:vc animated:NO completion:nil];
                      }

                  } else if ([[dic objectForKey:@"Response"] integerValue] == -1 ||
                             [[dic objectForKey:@"Response"] integerValue] == -3 ||
                             [[dic objectForKey:@"Response"] integerValue] == -3 ||
                             [[dic objectForKey:@"Response"] integerValue] == -12 ||
                             [[dic objectForKey:@"Response"] integerValue] == 303 ||
                             [[dic objectForKey:@"Response"] integerValue] == 304) {

                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @"0"];
                  } else {
                      [self clearOTP];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"]: @""];
                  }

              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self clearOTP];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];

    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self clearOTP];
        [self custom_alert:@"Try again later." :@"0"];
    }

}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
}


-(void)saveUserdata:(NSString *)requestId
{
    
    gblclass.request_id_onbording = requestId;
    
    [defaults setValue:[encrypt encrypt_Data:gblclass.user_name_onbording] forKey:@"name"];
    [defaults setValue:[encrypt encrypt_Data:gblclass.cnic_onbording] forKey:@"cnic"];
    [defaults setValue:[encrypt encrypt_Data:gblclass.mobile_number_onbording] forKey:@"mobile_num"];
    [defaults setValue:[encrypt encrypt_Data:gblclass.email_onbording] forKey:@"email"];
    [defaults setValue:[encrypt encrypt_Data:gblclass.nationality_onbording] forKey:@"nationality"];
    [defaults setValue:[encrypt encrypt_Data:requestId] forKey:@"req_id"];
    [defaults synchronize];
}

-(void) clearOTP {
    txt_1.text=@"";
    txt_2.text=@"";
    txt_3.text=@"";
    txt_4.text=@"";
    txt_5.text=@"";
    txt_6.text=@"";
}


- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(event.type == UIEventSubtypeMotionShake)
    {
        txt_1.text=@"";
        txt_2.text=@"";
        txt_3.text=@"";
        txt_4.text=@"";
        txt_5.text=@"";
        txt_6.text=@"";
        
        [self.view resignFirstResponder];
        [txt_1 becomeFirstResponder];
        
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txt_1 resignFirstResponder];
    [txt_2 resignFirstResponder];
    [txt_3 resignFirstResponder];
    [txt_4 resignFirstResponder];
    [txt_5 resignFirstResponder];
    [txt_6 resignFirstResponder];
}

-(void)textFieldDidDelete:(UITextField *)textField {
    OtpTextField *otpField = (OtpTextField *)[self.view viewWithTag:textField.tag-1];
    [otpField becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSUInteger MAX_DIGITS = 1;
    
    NSUInteger maxLength = 1;
    
    // return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    
    //   UITextField* t1,*t2,*t3;
    
    //NSLog(@"tag :: %ld",(long)textField.tag);
    //NSLog(@"text length :: %ld",(long)textField.text.length);
    
    
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"Responder :: %@",str);
    
    
    NSString *str1 = @"hello ";
    str1 = [str1 stringByAppendingString:str];
    
    if ([arr_pin_pattern count]>1)
    {
        
        str_pw =[NSString stringWithFormat:@"%d",[textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength ];
        
        //NSLog(@"sre pw :: %@",str_pw);
        textField.text = string;
        
        NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
        
        if ([textField.text rangeOfCharacterFromSet:set].location == NSNotFound)
        {
            //NSLog(@"NO SPECIAL CHARACTER");
        }
        else
        {
            //NSLog(@"HAS SPECIAL CHARACTER");
            textField.text=@"";
            
            return 0;
        }
        
        if( [str length] > 0 ) {
            //   [arr_pin_pattern removeObjectAtIndex:0];
            
            int j;
            int i;
            
            for (i=1; i<arr_pin_pattern.count-1; i++) //txt_enable.count-1
            {
                j=[[arr_pw_pin objectAtIndex:i] integerValue];
                
                if (j>textField.tag)
                {
                    j=i;
                    break;
                }
                
            }
            
            
            textField.text = string;
            UIResponder* nextResponder = [textField.superview viewWithTag:([[arr_pin_pattern objectAtIndex:i] integerValue])]; //viewWithTag:(textField.tag + 5)
            
            //NSLog(@"Responder :: %@",str);
            //NSLog(@"tag %ld",(long)textField.tag);
            
            if (nextResponder)
            {
                [textField resignFirstResponder];
                [nextResponder becomeFirstResponder];
            }
            
            
            if (textField.tag == 6)
            {
                [hud showAnimated:YES];
                [self.view addSubview:hud];
                [hud showAnimated:YES];
                
                txt_6.text=string;
                
                str_Otp=@"";
                str_Otp = [str_Otp stringByAppendingString:txt_1.text];
                str_Otp = [str_Otp stringByAppendingString:txt_2.text];
                str_Otp = [str_Otp stringByAppendingString:txt_3.text];
                str_Otp = [str_Otp stringByAppendingString:txt_4.text];
                str_Otp = [str_Otp stringByAppendingString:txt_5.text];
                str_Otp = [str_Otp stringByAppendingString:txt_6.text];
                
                [txt_1 resignFirstResponder];
                [txt_2 resignFirstResponder];
                [txt_3 resignFirstResponder];
                [txt_4 resignFirstResponder];
                [txt_5 resignFirstResponder];
                [txt_6 resignFirstResponder];
                
                if ([str_Otp length] < 6) {
                    [self clearOTP];
                    [hud hideAnimated:YES];
                    [txt_1 becomeFirstResponder];
                    [self custom_alert:@"Please enter 6 digit OTP that has been sent to your mobile number" :@"0"];
                    [self.view resignFirstResponder];
                } else {
                    [self checkinternet];
                    if (netAvailable){
                        if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"]) {
                            chk_ssl=@"insertUpdateOnboardPin";
                            [self SSL_Call];
                        } else if ([gblclass.parent_vc_onbording isEqualToString:@"RegisterAccountVC"]) {
                            chk_ssl=@"otp";
                            [self SSL_Call];
                        } else if([gblclass.parent_vc_onbording isEqualToString:@"SecureCodeSignupVC"]) {
                            chk_ssl=@"mobileAppSecureCodeVerifyStep2";
                            [self SSL_Call];
                        }
                    
                    }
                }
                return 0;
            }
            return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
        }
        
    }
    
    //NSLog(@"%ld",(long)textField.tag);
    
    if( [str length] > 0 )
    {
        return [textField.text stringByReplacingCharactersInRange:range withString:string].length <= maxLength;
    }
    else
    {
        //  txt_nam=[NSString stringWithFormat:@"%ld",(long)textField.tag];
        
        txt_nam=textField.tag;
        [self txt_field_back_move];
        UIResponder* nextResponder = [textField.superview viewWithTag:txt_nam]; //viewWithTag:(textField.tag + 5)
        
        //NSLog(@"Responder :: %@",nextResponder);
        
        if (nextResponder)
        {
            [textField resignFirstResponder];
            [nextResponder becomeFirstResponder];
        }
        
        //textField.text=@"";
        return 0;
    }
    
    
    
    
    
    
    
}


-(void)txt_field_back_move
{
    
    //NSLog(@"%d",txt_nam);
    
    //int numberOfRows = [arr_pw_pin count-1];
    
    int j;
    for (int i=arr_pw_pin.count-1; i>=0 ; i--) //txt_enable.count-1
    {
        j=[[arr_pw_pin objectAtIndex:i] integerValue];
        
        
        //NSLog(@"%d",txt_nam);
        //NSLog(@"%d",j);
        if (txt_nam>j)
        {
            //NSLog(@"MINI");
            txt_nam=j;
            return;
        }
        else
        {
            //NSLog(@"MAXI");
        }
    }
    
}


//////////************************** KEYBOARD CONTROLS ***************************///////////

#define kOFFSET_FOR_KEYBOARD 50.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //  //NSLog(@"%d",rect );
    
    if (movedUp) {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillShow)
    //                                                 name:UIKeyboardWillShowNotification
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillHide)
    //                                                 name:UIKeyboardWillHideNotification
    //                                               object:nil];
    //    dispatch_async(dispatch_get_main_queue(),
    //                   ^{
    //                       //  self.fundtextfield.delegate = self;
    //
    //
    //                       //         [mine showmyhud];
    //                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
    //
    //                       //                       [self getFolionumber];
    //                   });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

//////////************************** END KEYBOARD CONTROLS ***************************///////////


///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        if ([chk_ssl isEqualToString:@"otp"])
        {
            chk_ssl=@"";
            [self VerifyOTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"generateOTPForDARequest"])
        {
            chk_ssl=@"";
            [self GenerateOTPForDARequest:@""];
        }
        else if ([chk_ssl isEqualToString:@"insertUpdateOnboardPin"])
        {
            chk_ssl=@"";
            [self InsertUpdateOnboardPin:@""];
        }
        else if ([chk_ssl isEqualToString:@"mobileAppSecureCodeVerifyStep2"])
        {
            chk_ssl=@"";
            [self mobileAppSecureCodeVerifyStep2:@""];
        }
        else if ([chk_ssl isEqualToString:@"generateOTPForAddition"])
        {
            chk_ssl=@"";
            [self generateOTPForAddition:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    gblclass.ssl_pin_check=@"1";
    
    
    [connection cancel];
    
    if ([chk_ssl isEqualToString:@"otp"]) {
        chk_ssl=@"";
        [self VerifyOTP_last:@""];
        
    } else if ([chk_ssl isEqualToString:@"generateOTPForDARequest"]) {
        chk_ssl=@"";
        [self GenerateOTPForDARequest:@""];
    } else if ([chk_ssl isEqualToString:@"insertUpdateOnboardPin"]) {
        chk_ssl=@"";
        [self InsertUpdateOnboardPin:@""];
    } else if ([chk_ssl isEqualToString:@"mobileAppSecureCodeVerifyStep2"]) {
        chk_ssl=@"";
        [self mobileAppSecureCodeVerifyStep2:@""];
    } else if ([chk_ssl isEqualToString:@"generateOTPForAddition"]) {
        chk_ssl=@"";
        [self generateOTPForAddition:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [connection cancel];
    }
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if (responseData == nil) {
        responseData = [NSMutableData dataWithData:data];
    } else {
        [responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    }
    
    //  NSLog(@"%@", error.localizedDescription);
}

- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message {
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    // NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_back:(id)sender
{
    // gblclass.user_login_name=@"";
    [self slide_left];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_resend_otp:(id)sender
{
    [self checkinternet];
    if (netAvailable) {
        
        if ([gblclass.parent_vc_onbording isEqualToString:@"ResetPinVC"] || [gblclass.parent_vc_onbording isEqualToString:@"RegisterAccountVC"]) {
            chk_ssl = @"generateOTPForDARequest";
            [self SSL_Call];
        } else if([gblclass.parent_vc_onbording isEqualToString:@"SecureCodeSignupVC"]) {
            chk_ssl = @"generateOTPForAddition";
            [self SSL_Call];
        }
        
    }
}

-(IBAction)btn_feature:(id)sender
{
    
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
    
    
}

-(IBAction)btn_offer:(id)sender
{
    
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0)
    {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
}

- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}



@end


