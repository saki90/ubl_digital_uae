//
//  GnbAccountTransactionDetail.m
//
//  Created by   on 03/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbAccountTransactionDetail.h"


NSString *const kGnbAccountTransactionDetailCredit = @"Credit";
NSString *const kGnbAccountTransactionDetailValueDate = @"Value_Date";
NSString *const kGnbAccountTransactionDetailBalance = @"Balance";
NSString *const kGnbAccountTransactionDetailDate = @"Date";
NSString *const kGnbAccountTransactionDetailDebit = @"Debit";
NSString *const kGnbAccountTransactionDetailDescription = @"Description";
NSString *const kGnbAccountTransactionDetailCardNo = @"Card_No";


@interface GnbAccountTransactionDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbAccountTransactionDetail

@synthesize credit = _credit;
@synthesize valueDate = _valueDate;
@synthesize balance = _balance;
@synthesize date = _date;
@synthesize debit = _debit;
@synthesize gnbAccountTransactionDetailDescription = _gnbAccountTransactionDetailDescription;
@synthesize cardNo = _cardNo;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.credit = [self objectOrNilForKey:kGnbAccountTransactionDetailCredit fromDictionary:dict];
            self.valueDate = [self objectOrNilForKey:kGnbAccountTransactionDetailValueDate fromDictionary:dict];
            self.balance = [self objectOrNilForKey:kGnbAccountTransactionDetailBalance fromDictionary:dict];
            self.date = [self objectOrNilForKey:kGnbAccountTransactionDetailDate fromDictionary:dict];
            self.debit = [self objectOrNilForKey:kGnbAccountTransactionDetailDebit fromDictionary:dict];
            self.gnbAccountTransactionDetailDescription = [self objectOrNilForKey:kGnbAccountTransactionDetailDescription fromDictionary:dict];
            self.cardNo = [self objectOrNilForKey:kGnbAccountTransactionDetailCardNo fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.credit forKey:kGnbAccountTransactionDetailCredit];
    [mutableDict setValue:self.valueDate forKey:kGnbAccountTransactionDetailValueDate];
    [mutableDict setValue:self.balance forKey:kGnbAccountTransactionDetailBalance];
    [mutableDict setValue:self.date forKey:kGnbAccountTransactionDetailDate];
    [mutableDict setValue:self.debit forKey:kGnbAccountTransactionDetailDebit];
    [mutableDict setValue:self.gnbAccountTransactionDetailDescription forKey:kGnbAccountTransactionDetailDescription];
    [mutableDict setValue:self.cardNo forKey:kGnbAccountTransactionDetailCardNo];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.credit = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailCredit];
    self.valueDate = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailValueDate];
    self.balance = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailBalance];
    self.date = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailDate];
    self.debit = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailDebit];
    self.gnbAccountTransactionDetailDescription = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailDescription];
    self.cardNo = [aDecoder decodeObjectForKey:kGnbAccountTransactionDetailCardNo];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_credit forKey:kGnbAccountTransactionDetailCredit];
    [aCoder encodeObject:_valueDate forKey:kGnbAccountTransactionDetailValueDate];
    [aCoder encodeObject:_balance forKey:kGnbAccountTransactionDetailBalance];
    [aCoder encodeObject:_date forKey:kGnbAccountTransactionDetailDate];
    [aCoder encodeObject:_debit forKey:kGnbAccountTransactionDetailDebit];
    [aCoder encodeObject:_gnbAccountTransactionDetailDescription forKey:kGnbAccountTransactionDetailDescription];
    [aCoder encodeObject:_cardNo forKey:kGnbAccountTransactionDetailCardNo];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbAccountTransactionDetail *copy = [[GnbAccountTransactionDetail alloc] init];
    
    if (copy) {

        copy.credit = [self.credit copyWithZone:zone];
        copy.valueDate = [self.valueDate copyWithZone:zone];
        copy.balance = [self.balance copyWithZone:zone];
        copy.date = [self.date copyWithZone:zone];
        copy.debit = [self.debit copyWithZone:zone];
        copy.gnbAccountTransactionDetailDescription = [self.gnbAccountTransactionDetailDescription copyWithZone:zone];
        copy.cardNo = [self.cardNo copyWithZone:zone];
    }
    
    return copy;
}


@end
