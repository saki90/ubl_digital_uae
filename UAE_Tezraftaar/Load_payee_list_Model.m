//
//  Load_payee_list_Model.m
//
//  Created by   on 18/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Load_payee_list_Model.h"
#import "OutdsIBFTRelation.h"
#import "OutdtDataset.h"
#import "OutdsIBFTPOT.h"


NSString *const kLoad_payee_list_ModelOutdsIBFTRelation = @"outdsIBFTRelation";
NSString *const kLoad_payee_list_ModelOutdtDataset = @"outdtDataset";
NSString *const kLoad_payee_list_ModelStrReturnMessage = @"strReturnMessage";
NSString *const kLoad_payee_list_ModelResponse = @"Response";
NSString *const kLoad_payee_list_ModelOutdtData = @"outdtData";
NSString *const kLoad_payee_list_ModelOutdsIBFTPOT = @"outdsIBFTPOT";
NSString *const kLoad_payee_list_ModelOutStrIBFTRelation = @"outStrIBFTRelation";


@interface Load_payee_list_Model ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Load_payee_list_Model

@synthesize outdsIBFTRelation = _outdsIBFTRelation;
@synthesize outdtDataset = _outdtDataset;
@synthesize strReturnMessage = _strReturnMessage;
@synthesize response = _response;
@synthesize outdtData = _outdtData;
@synthesize outdsIBFTPOT = _outdsIBFTPOT;
@synthesize outStrIBFTRelation = _outStrIBFTRelation;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.outdsIBFTRelation = [OutdsIBFTRelation modelObjectWithDictionary:[dict objectForKey:kLoad_payee_list_ModelOutdsIBFTRelation]];
            self.outdtDataset = [OutdtDataset modelObjectWithDictionary:[dict objectForKey:kLoad_payee_list_ModelOutdtDataset]];
            self.strReturnMessage = [self objectOrNilForKey:kLoad_payee_list_ModelStrReturnMessage fromDictionary:dict];
            self.response = [self objectOrNilForKey:kLoad_payee_list_ModelResponse fromDictionary:dict];
            self.outdtData = [self objectOrNilForKey:kLoad_payee_list_ModelOutdtData fromDictionary:dict];
            self.outdsIBFTPOT = [OutdsIBFTPOT modelObjectWithDictionary:[dict objectForKey:kLoad_payee_list_ModelOutdsIBFTPOT]];
            self.outStrIBFTRelation = [self objectOrNilForKey:kLoad_payee_list_ModelOutStrIBFTRelation fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.outdsIBFTRelation dictionaryRepresentation] forKey:kLoad_payee_list_ModelOutdsIBFTRelation];
    [mutableDict setValue:[self.outdtDataset dictionaryRepresentation] forKey:kLoad_payee_list_ModelOutdtDataset];
    [mutableDict setValue:self.strReturnMessage forKey:kLoad_payee_list_ModelStrReturnMessage];
    [mutableDict setValue:self.response forKey:kLoad_payee_list_ModelResponse];
    [mutableDict setValue:self.outdtData forKey:kLoad_payee_list_ModelOutdtData];
    [mutableDict setValue:[self.outdsIBFTPOT dictionaryRepresentation] forKey:kLoad_payee_list_ModelOutdsIBFTPOT];
    [mutableDict setValue:self.outStrIBFTRelation forKey:kLoad_payee_list_ModelOutStrIBFTRelation];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.outdsIBFTRelation = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelOutdsIBFTRelation];
    self.outdtDataset = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelOutdtDataset];
    self.strReturnMessage = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelStrReturnMessage];
    self.response = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelResponse];
    self.outdtData = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelOutdtData];
    self.outdsIBFTPOT = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelOutdsIBFTPOT];
    self.outStrIBFTRelation = [aDecoder decodeObjectForKey:kLoad_payee_list_ModelOutStrIBFTRelation];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_outdsIBFTRelation forKey:kLoad_payee_list_ModelOutdsIBFTRelation];
    [aCoder encodeObject:_outdtDataset forKey:kLoad_payee_list_ModelOutdtDataset];
    [aCoder encodeObject:_strReturnMessage forKey:kLoad_payee_list_ModelStrReturnMessage];
    [aCoder encodeObject:_response forKey:kLoad_payee_list_ModelResponse];
    [aCoder encodeObject:_outdtData forKey:kLoad_payee_list_ModelOutdtData];
    [aCoder encodeObject:_outdsIBFTPOT forKey:kLoad_payee_list_ModelOutdsIBFTPOT];
    [aCoder encodeObject:_outStrIBFTRelation forKey:kLoad_payee_list_ModelOutStrIBFTRelation];
}

- (id)copyWithZone:(NSZone *)zone
{
    Load_payee_list_Model *copy = [[Load_payee_list_Model alloc] init];
    
    if (copy) {

        copy.outdsIBFTRelation = [self.outdsIBFTRelation copyWithZone:zone];
        copy.outdtDataset = [self.outdtDataset copyWithZone:zone];
        copy.strReturnMessage = [self.strReturnMessage copyWithZone:zone];
        copy.response = [self.response copyWithZone:zone];
        copy.outdtData = [self.outdtData copyWithZone:zone];
        copy.outdsIBFTPOT = [self.outdsIBFTPOT copyWithZone:zone];
        copy.outStrIBFTRelation = [self.outStrIBFTRelation copyWithZone:zone];
    }
    
    return copy;
}


@end
