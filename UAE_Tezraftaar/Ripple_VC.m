//
//  Ripple_VC.m
//  ubltestbanking
//
//  Created by Jahangir Mirza on 10/08/2018.
//  Copyright © 2018 ammar. All rights reserved.
//

#import "Ripple_VC.h"

@interface Ripple_VC ()
{
    UIStoryboard *storyboard;
    UIViewController *vc;
}
@end

@implementation Ripple_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Ripple Screen");
   // [self performSelector:@selector(Call_Ripple) withObject:nil afterDelay:0.3];
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"FindPlace_swift"]; //utility_bill   //bill_all
        [self presentViewController:vc animated:NO completion:nil];
        
    });
}

-(void)Call_Ripple
{
    storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"FindPlace_swift"]; //utility_bill   //bill_all
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
