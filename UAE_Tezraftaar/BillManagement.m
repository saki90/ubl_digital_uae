//
//  AddPayee.m
//  ubltestbanking
//
//  Created by ammar on 02/03/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import "BillManagement.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "GlobalClass.h"
#import "GlobalStaticClass.h"
//#import "Loadbranches.h"
//#import "LoadIBFTBanks.h"
#import "APIdleManager.h"
#import "Encrypt.h"

@interface BillManagement ()<NSURLConnectionDataDelegate>
{
    // NSMutableArray* bill_service;
    NSMutableArray* bill_type;
    NSMutableArray* companytype;
    NSMutableArray* arr_select_service;
    NSInteger selectedrowpicker;
    NSMutableArray* ttid,*ttacceskey,*placeholdername;
    // NSMutableArray* branchname,*branchcode;
    MBProgressHUD *hud;
    NSMutableArray* name;
    
    GlobalStaticClass* gblclass;
    
    //fet title
    NSString* acc_type,*bankname,*bankimd,*fetchtitlebranchcode;
    NSString*  strTtId,*strAccessKey,*strType,*strtxtNick,*strtxtCustomerID,*merchant_id;
    UIStoryboard *storyboard;
    UIViewController *vc;
    NSString* responsecode;
    APIdleManager* timer_class;
    UIAlertController* alert;
    NSString* txttypebillvalue;
    int LENGTH_MAX, LENGTH_MIN;
    NSMutableArray* a;
    NSArray* split;
    NSString* str_TcAccess_key;
    NSMutableArray* tc_access_key;
    NSString* chk_confirm_add_isp;
    
    NSString* selected_bill_type,*branch_selected_value,*bIsConfirmation_required;
    NSString* chck_isp_nick;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSString* ssl_count;
    UILabel* label;
    NSString *vw_down_chck;
}

- (IBAction)MakeHTTPRequestTapped;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation BillManagement
@synthesize txtbillservice;
@synthesize transitionController;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // bill_service = [[NSMutableArray alloc]init];
    encrypt = [[Encrypt alloc] init];
    gblclass =  [GlobalStaticClass getInstance];
    bill_type = [[NSMutableArray alloc]init];
    self.transitionController = [[TransitionDelegate alloc] init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    tc_access_key=[[NSMutableArray alloc] init];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    //   bill_type= @[@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"];
    
    arr_select_service = [[NSMutableArray alloc] init];
    bill_type=[[NSMutableArray alloc] initWithArray:@[@"Utility Bills",@"Mobile Bills",@"UBL Bills",@"Broadband Internet Bills"]];
    
    vw_down_chck = @"";
    ssl_count = @"0";
    chk_confirm_add_isp=@"0";
    
    //bill_service=@[@"Bill Management",@"Prepaid Services",@"Fund Transfer"];
    
    gblclass.arr_re_genrte_OTP_additn=[[NSMutableArray alloc] init];
    a=[[NSMutableArray alloc] init];
    //NSLog(@"Base Currency .%@",gblclass.base_currency);
    
    // bill_service=@[@"Bill Management",@" Prepaid Services",@"Fund Transfer"];
    
    // [tapGesture release];
    
    
    LENGTH_MAX=0;
    LENGTH_MIN=0;
    
    _mobiletxtnick.delegate=self;
    _normaltxtnick.delegate=self;
    _txtphonenumber.delegate=self;
    _retypemobilenumbernick.delegate=self;
    _txtcompany.delegate=self;
    
    _mobiletxtnick.autocorrectionType = UITextAutocorrectionTypeNo;
    _normaltxtnick.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtphonenumber.autocorrectionType = UITextAutocorrectionTypeNo;
    _retypemobilenumbernick.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtcompany.autocorrectionType = UITextAutocorrectionTypeNo;
    
    gblclass.arr_values=[[NSMutableArray alloc] init];
    //NSLog(@"%@",gblclass.bill_type);
    
    
    //    [APIdleManager sharedInstance].onTimeout = ^(void){
    //        //  [self.timeoutLabel  setText:@"YES"];
    //
    //
    //        //NSLog(@"*********************** Time Out Yes Successfully *********************** ");
    //
    //       storyboard = [UIStoryboard storyboardWithName:
    //                                    gblclass.story_board bundle:[NSBundle mainBundle]];
    //       vc = [storyboard instantiateViewControllerWithIdentifier:session_logout_VC];
    //
    //
    //        [self presentViewController:vc animated:YES completion:nil];
    //
    //
    //        APIdleManager * cc1=[[APIdleManager alloc] init];
    //        // cc.createTimer;
    //
    //        [cc1 timme_invaletedd];
    //
    //    };
    
    
    NSLog(@"%@",gblclass.bill_type.uppercaseString);
    vw_table.backgroundColor=[UIColor colorWithRed:0/255.0 green:96/255.0 blue:198/255.0 alpha:1.0];
    self.view.backgroundColor = [UIColor colorWithRed:bg_r/255.0 green:bg_g/255.0 blue:bg_b/255.0 alpha:1.0];
    lbl_heading.text=gblclass.bill_type.uppercaseString;
    vw_table.hidden=YES;
    _tableviewcompany.hidden=YES;
    _tableviewbilltype.hidden=YES;
    
     
    self.billtype.hidden=true;
    self.company.hidden=true;
    self.txtcompany.text=@"";
    self.normaltxtnick.text=@"";
    self.mobiletxtnick.text=@"";
    self.retypemobilenumbernick.text=@"";
    
    self.txtphonenumber.text=@"";
    strTtId=@"";
    strAccessKey=@"";
    strType=@"";
    strtxtNick=@"";
    strtxtCustomerID=@"";
    
    self.txtbilltype.text = gblclass.bill_click;        //@"Utility Bills";
    txttypebillvalue = gblclass.bill_click;             //@"Utility Bills";
    
    _txtbilltype.hidden=YES;
    
    
    // NSLog(@"%@",gblclass.bill_click);
    
    
    //if([txttypebillvalue isEqualToString:@"Utility Bills"])
    
    //        //NSLog(@"utilty bill");
    //        gblclass.bill_type=@"Utility Bills";
    //
    //        [self LoadUtilityBills:@""];
    //        _normaltxtnick.hidden=false;
    //        _normaltxtnick. placeholder=@"Enter nick";
    //        _txtphonenumber. placeholder=@"Enter consumer number";
    //        _normalview.hidden=false;
    //        _mobileview.hidden=true;
    //        vw_table.hidden=YES;
    
    
    //    self.txtbilltype.text=[bill_type objectAtIndex:indexPath.row];
    //    txttypebillvalue = self.txtbilltype.text;
    
    if([txttypebillvalue isEqualToString:@"Utility Bills"])
    {
        //NSLog(@"utilty bill");
        gblclass.bill_type=@"Utility Bills";
        
        [self checkinternet];
        if (netAvailable)
        {
            [self LoadUtilityBills:@""];
        }
        
        _txt_service.placeholder = @"Select Service";
//        lbl_table_heading.text=@"Select Bill Company";
        _normaltxtnick.hidden=false;
        _normaltxtnick.placeholder=@"Enter nick";
        _normaltxtnick.enabled=NO;
        _txtphonenumber.placeholder=@"Enter consumer or account number";
        _txtphonenumber.enabled=NO;
     //   [self.txtphonenumber setKeyboardType:UIKeyboardTypeDefault];
        
        _normalview.hidden=NO;
        _mobileview.hidden=YES;
        vw_table.hidden=YES;
        
    }
    else if ([txttypebillvalue isEqualToString:@"Mobile Bills"])
    {
        //NSLog(@"Mobile bill");
        gblclass.bill_type=@"Mobile Bills";
        
        
        _txtcompany.placeholder = @"Please select mobile company";
        
        [self checkinternet];
        if (netAvailable)
        {
            [self GetBillCompaniesDetail:@"OB"];
        }
        
        lbl_table_heading.text=@"Select Your Mobile Company";
        
        //    _txtnick. placeholder=@"Confirm mobile number";
        _txtphonenumber.placeholder=@"Mobile number";
        
        
        _mobiletxtnick.hidden=NO;
        _normalview.hidden=NO; //true
        _mobileview.hidden=NO;
        
        vw_table.hidden=YES;
        
    }
    else if ([txttypebillvalue isEqualToString:@"UBL Bills"])
    {
        //NSLog(@"UBL bill");
        gblclass.bill_type=@"UBL Bills";
        
        self.txtbilltype.text=@"";
        vw_table.hidden=YES;
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        //    vc = [storyboard instantiateViewControllerWithIdentifier:@"addpayee"];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if ([txttypebillvalue isEqualToString:@"Broadband Internet Bills"] || [txttypebillvalue isEqualToString:@"ISP Bill"])
    {
        //NSLog(@"Internet Bill");
        gblclass.bill_type=@"Broadband Internet Bills";
        
        _mobiletxtnick.hidden=YES;
        _txtphonenumber. placeholder=@"Enter consumer number";
      //  [self.txtphonenumber setKeyboardType:UIKeyboardTypeDefault];
        [self GetBillCompaniesDetail:@"ISP"];
        
        _normalview.hidden=YES;
        _normaltxtnick.enabled=NO;
        
        _mobileview.hidden=true;
        vw_table.hidden=YES;
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textField:(UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    BOOL stringIsValid;
    NSInteger MAX_DIGITS=12;
    NSInteger MIN_DIGITS=12;
    
    
    
    if([txttypebillvalue isEqualToString:@"Utility Bills"])
    {
        if ([theTextField isEqual:_txtphonenumber])
        {
//            MAX_DIGITS=LENGTH_MAX;  // 11;
//            MIN_DIGITS=LENGTH_MIN;
            //  MAX_DIGITS=12;
            
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
                else
                {
                    return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                }
            }
        }
    }
    else
    {
        
        
        if ([theTextField isEqual:_txtphonenumber])
        {
            MAX_DIGITS=LENGTH_MAX;  // 11;
            MIN_DIGITS=LENGTH_MIN;
            
            if (MAX_DIGITS==0 || MIN_DIGITS==0)
            {
                if (_txtcompany.text.length==0 || [_txtcompany.text isEqualToString:@""])
                {
                    [self showAlert:@"Select Company" :@"Attention" ];
                    return 0;
                }
                else
                {
                    MAX_DIGITS=11;
                }
            }
            
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
            
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
                else
                {
                    //NSLog(@"%lu",theTextField.text.length);
                    
                    return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
                }
                
            }
            
            //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
        }
    }
    
    
    
    if ([theTextField isEqual:_normaltxtnick])
    {
        
        MAX_DIGITS = 30;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 "];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:_retypemobilenumbernick])
    {
        MAX_DIGITS=12;
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return [theTextField.text stringByReplacingCharactersInRange:range withString:string].length <= MAX_DIGITS;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:_mobiletxtnick])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_. "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    else if ([theTextField isEqual:_txtcompany])
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_. "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
            else
            {
                return YES;
            }
            
        }
        //[theTextField.text stringByReplacingCharactersInRange:range withString:string];//.length <= MAX_DIGITS;
    }
    
    return YES;
}


- (IBAction)btn_submitfetchtitle:(UIButton *)sender
{
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [_txtphonenumber resignFirstResponder];
    [_normaltxtnick resignFirstResponder];
    [_retypemobilenumbernick resignFirstResponder];
    
    
    [gblclass.arr_values removeAllObjects];
    
    NSString *nick_name = [self.normaltxtnick.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(_mobileview.isHidden==NO)
    {
        strtxtNick = nick_name; //self.normaltxtnick.text;
        self.normaltxtnick.text = nick_name;
        
    }
    else if(_normalview.isHidden==NO)
    {
        if ([_normaltxtnick.text length] < 5) {
            [self custom_alert:@"Nickname should be between 5 to 30 characters" :@"0"];
            return;
            
        } else {
            strtxtNick = nick_name;     //self.normaltxtnick.text;
        }
    }
    
    strtxtCustomerID=self.txtphonenumber.text;
    
    if([self.txtbilltype.text length]==0)
    {
        [self custom_alert:@"Please select your Bill type" :@"0"];
        return;
    }
    else
    {
        
        if([self.txtbilltype.text isEqualToString:@"Utility Bills"])
        {
            
            if([self.txtcompany.text length]==0)
            {
                [self custom_alert:@"Please select your company" :@"0"];
                return;
            }
            else if([self.txt_service.text length]==0)
            {
                [self custom_alert:@"Please select your service" :@"0"];
                return;
            }
            else if([strtxtCustomerID length]==0)
            {
                [self custom_alert:[NSString stringWithFormat:@"Please %@",_txtphonenumber.placeholder]  :@"0"];
                return;
            }
            else if([strtxtNick length]==0)
            {
                //[self showAlert:@"Please enter your nick" :@"Attention" ];
                [self custom_alert:@"Please enter nick name" :@"0"];
                self.normaltxtnick.text = @"";
                return;
                
            } else if ([strtxtNick length] < 5) {
                
                [self custom_alert:@"Nickname should be between 5 to 30 characters" :@"0"];
                return;
            }
            else
            {
                
                if (_txtphonenumber.text.length < LENGTH_MIN)
                {
                    //NSLog(@"MIN");
                    
                    //  [self showAlert:[NSString stringWithFormat:@"Length of %@ is not less then %d", _txtphonenumber.placeholder,LENGTH_MIN] :@"Attention"];
                    
                    [self custom_alert:[NSString stringWithFormat:@"Length of %@ is not less then %d", _txtphonenumber.placeholder,LENGTH_MIN] :@"0"];
                    return;
                }
                
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:strTtId];
                [gblclass.arr_values addObject:strAccessKey];
                [gblclass.arr_values addObject:merchant_id];
                [gblclass.arr_values addObject:strtxtNick];
                [gblclass.arr_values addObject:strtxtCustomerID];
                
                if ([vw_down_chck isEqualToString:@""])
                {
                    [self keyboardWillHide];
                }
                
                //  [self Generate_OTP:@""];
                
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"otp";
                    [self SSL_Call];
                }
                
                //  [self AddUtilityCompanies:@""];
                
            }
        }
        else if ([self.txtbilltype.text isEqualToString:@"Mobile Bills"])
        {
            if([self.txtcompany.text length]==0)
            {
                //  [self showAlert:@"Please select your Company" :@"Attention"];
                [self custom_alert:@"Please select your Mobile Company" :@"0"];
                return;
            }
            else if([strtxtCustomerID length]==0)
            {
                //  [self showAlert:@"Please enter your mobile number" :@"Attention"];
                [self custom_alert:@"Please provide valid postpaid mobile number" :@"0"];
                return;
            }
            else if([strtxtNick length]==0)
            {
                // [self showAlert:@"Please enter your nick" :@"Attention"];
                [self custom_alert:@"Please enter mobile bill nick" :@"0"];
                return;
            }
            else if([strtxtNick length]<5)
            {
                // [self showAlert:@"Please enter your nick" :@"Attention"];
                [self custom_alert:@"Mobile bill nick should be between 5 to 30 charactors" :@"0"];
                return;
            }
            //            else if([_retypemobilenumbernick.text isEqualToString:@""])
            //            {
            //                [self showAlert:@"Please enter confirm mobile number" :@"Attention"];
            //            }
            //            else if(![strtxtCustomerID isEqualToString:_retypemobilenumbernick.text])
            //            {
            //                [self showAlert:@"Mobile number does not match" :@"Attention"];
            //            }
            else
            {
                
                if (_txtphonenumber.text.length < LENGTH_MIN)
                {
                    //NSLog(@"MIN");
                    
                    //    [self showAlert:[NSString stringWithFormat:@"Length of %@ is not less then %d", _txtphonenumber.placeholder,LENGTH_MIN] :@"Attention"];
                    
                    
                    [self custom_alert:[NSString stringWithFormat:@"Length of %@ is not less then %d", _txtphonenumber.placeholder,LENGTH_MIN] :@"0"];
                    return;
                }
                
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:strTtId];
                [gblclass.arr_values addObject:strAccessKey];
                [gblclass.arr_values addObject:strType];
                [gblclass.arr_values addObject:strtxtNick];
                [gblclass.arr_values addObject:strtxtCustomerID];
                
                //  [self Generate_OTP:@""];
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"otp";
                    [self SSL_Call];
                }
                
                //strTtId,strAccessKey,strType,strtxtNick,strtxtCustomerID,
                // [self AddBillCompanies:@""];
            }
        }
        else if ([self.txtbilltype.text isEqualToString:@"Broadband Internet Bills"] || [txttypebillvalue isEqualToString:@"ISP Bill"])
        {
            
            if([self.txtcompany.text length]==0)
            {
                //[self showAlert:@"Please select your Company" :@"Attention"];
                
                //  [self showAlert:[NSString stringWithFormat:@"Please %@",_txtcompany.placeholder] :@"Attention"];
                
                [self custom_alert:[NSString stringWithFormat:@"Please %@",_txtcompany.placeholder] :@"0"];
                return;
                
            }
            else if([strtxtCustomerID length]==0)
            {
                //[self showAlert:@"Please enter your consumer/customer number" :@"Attention"];
                
                //[self showAlert:[NSString stringWithFormat:@"Please Enter %@",_txtphonenumber.placeholder] :@"Attention"];
                
                [self custom_alert:[NSString stringWithFormat:@"Please enter %@",_txtphonenumber.placeholder] :@"0"];
                return;
            }
            //            else if ([chck_isp_nick isEqualToString:@"1"])
            //            {
            //                if([strtxtNick length]==0)
            //                {
            //                    [self showAlert:@"Please enter your nick" :@"Attention" ];
            //                    return;
            //                }
            //
            //            }
            else if ([chk_confirm_add_isp isEqualToString:@"0"])
            {
                if (_txtphonenumber.text.length < LENGTH_MIN)
                {
                    //NSLog(@"MIN");
                    
                    //   [self showAlert:[NSString stringWithFormat:@"Length of %@ is not less then %d",_txtphonenumber.placeholder,LENGTH_MIN] :@"Attention"];
                    
                    [self custom_alert:[NSString stringWithFormat:@"Length of %@ is not less then %d",_txtphonenumber.placeholder,LENGTH_MIN] :@"0"];
                    return;
                }
                
                //                [gblclass.arr_values addObject:gblclass.user_id];
                //                [gblclass.arr_values addObject:gblclass.M3sessionid];
                //                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                //                [gblclass.arr_values addObject:strTtId];
                //                [gblclass.arr_values addObject:strAccessKey];
                //                [gblclass.arr_values addObject:strtxtCustomerID];
                
                
                
                //    [self Generate_OTP:@""];
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"isp";
                    [self SSL_Call];
                }
            }
            else if ([chk_confirm_add_isp isEqualToString:@"1"])
            {
                //  [self Add_Confirm_bill_detail:@""];
                
                if([strtxtNick length]==0)
                {
                    //  [self showAlert:@"Please enter your nick" :@"Attention" ];
                    [self custom_alert:@"Please enter ISP bill nick" :@"0"];
                    return;
                }
                else if([strtxtNick length]<5)
                {
                    // [self showAlert:@"Please enter your nick" :@"Attention"];
                    [self custom_alert:@"ISP bill nick should be between 5 to 30 charactors" :@"0"];
                    return;
                }
                
                [gblclass.arr_values addObject:gblclass.user_id];
                [gblclass.arr_values addObject:gblclass.M3sessionid];
                [gblclass.arr_values addObject:[GlobalStaticClass getPublicIp]];
                [gblclass.arr_values addObject:strTtId];
                [gblclass.arr_values addObject:@"ISP"];
                [gblclass.arr_values addObject:strtxtCustomerID];
                [gblclass.arr_values addObject:_normaltxtnick.text];
                [gblclass.arr_values addObject:@""];
                [gblclass.arr_values addObject:@"ISP"]; //strAccessKey
                [gblclass.arr_values addObject:@"false"];
                
                
                strAccessKey=@"ISP";
                // str_TcAccess_key=
                
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"otp";
                    [self SSL_Call];
                }
                
                //**   [self Generate_OTP:@""];
            }
            else
            {
                // [self ConfirmAddISPBill:@""];
                [self checkinternet];
                if (netAvailable)
                {
                    chk_ssl=@"add_bill";
                    [self SSL_Call];
                    
                    //[self AddBillCompanies:@""];
                }
            }
        }
    }
}

-(IBAction)btn_select_service:(id)sender
{
    lbl_table_heading.text=@"Select Services";
    
    [txtbillservice resignFirstResponder];;
    [_txtbilltype resignFirstResponder];;
    [_txtcompany resignFirstResponder];;
    [_txtphonenumber resignFirstResponder];;
    [_normaltxtnick resignFirstResponder];
    [_retypemobilenumbernick resignFirstResponder];
    
      self.billtype.hidden=true;
      _tableviewbilltype.hidden=YES;
      if(_table_service.hidden)  //self.company.hidden
      {
          self.company.hidden=false;
          _table_service.hidden = NO;
          _tableviewcompany.hidden=YES;
          _tableviewbilltype.hidden=YES;
          vw_table.hidden=NO;
      }
      else
      {
          self.company.hidden=true;
          _tableviewcompany.hidden=YES;
          _table_service.hidden = YES;
          vw_table.hidden=YES;
      }
    
//    [self checkinternet];
//    if (netAvailable)
//    {
//        chk_ssl=@"select_service";
//        [self SSL_Call];
//    }
//
  
}

- (IBAction)btn_dd_company:(UIButton *)sender
{
    
    //self.billserviceview.hidden=true;
    lbl_table_heading.text=@"Select Bill Company";
    [txtbillservice resignFirstResponder];
    [_txtbilltype resignFirstResponder];
    [_txtcompany resignFirstResponder];
    [_txtphonenumber resignFirstResponder];
    [_normaltxtnick resignFirstResponder];
    [_retypemobilenumbernick resignFirstResponder];
    
    
    if([self.txtbilltype.text length]==0)
    {
        [self showAlert:@"Please select your Bill type" :@"Attention" ];
    }
    else
    {
        self.billtype.hidden=true;
        _tableviewbilltype.hidden=YES;
        if(_tableviewcompany.hidden)  //self.company.hidden
        {
            self.company.hidden=false;
            _table_service.hidden = YES;
            _tableviewcompany.hidden=NO;
            _tableviewbilltype.hidden=YES;
            vw_table.hidden=NO;
           // [_tableviewcompany reloadData];
        }
        else
        {
            self.company.hidden=true;
            _tableviewcompany.hidden=YES;
            _table_service.hidden = NO;
            vw_table.hidden=YES;
        }
    }
}

- (IBAction)btn_dd_billtype:(UIButton *)sender
{
    //self.billserviceview.hidden=true;
    self.company.hidden=true;
    
    if(_tableviewbilltype.hidden)  //self.billtype.hidden
    {
        self.billtype.hidden=false;
        _tableviewbilltype.hidden=NO;
        _tableviewcompany.hidden=YES;
        vw_table.hidden=NO;
    }
    else
    {
        self.billtype.hidden=true;
        _tableviewbilltype.hidden=YES;
        vw_table.hidden=YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    self.cardlist.hidden=true;
    //
    // self.billserviceview.hidden=true;
    
    
    self.billtype.hidden=true;
    self.company.hidden=true;
 //saki 30 jan   self.txtcompany.text=@"";
    //   self.normaltxtnick.text=@"";
    //   self.mobiletxtnick.text=@"";
    self.retypemobilenumbernick.text=@"";
    
    self.txtphonenumber.text=@"";
    strTtId=@"";
    strAccessKey=@"";
    strType=@"";
    strtxtNick=@"";
    strtxtCustomerID=@"";
    
    
    [tableView cellForRowAtIndexPath:indexPath].selected=false;
    if(tableView==_tableviewbilltype)
    {
        [_tableviewbilltype reloadData];
        //NSLog(@"%@",[bill_type objectAtIndex:indexPath.row]);
        
        self.txtbilltype.text=[bill_type objectAtIndex:indexPath.row];
        txttypebillvalue = self.txtbilltype.text;
        
        if([txttypebillvalue isEqualToString:@"Utility Bills"])
        {
            //NSLog(@"utilty bill");
            gblclass.bill_type=@"Utility Bills";
            
            [self checkinternet];
            if (netAvailable)
            {
                [self LoadUtilityBills:@""];
            }
            _normaltxtnick.hidden=false;
            _normaltxtnick.placeholder=@"Enter nick";
            _txtphonenumber.placeholder=@"Enter consumer number";
            
            _normalview.hidden=false;
            _mobileview.hidden=true;
            vw_table.hidden=YES;
            
        }
        else if ([txttypebillvalue isEqualToString:@"Mobile Bills"])
        {
            //NSLog(@"Mobile bill");
            gblclass.bill_type=@"Mobile Bills";
            
            [self checkinternet];
            if (netAvailable)
            {
                [self GetBillCompaniesDetail:@"OB"];
            }
            
            //    _txtnick. placeholder=@"Confirm mobile number";
            _txtphonenumber. placeholder=@"Mobile number";
            
            _mobiletxtnick.hidden=false;
            _normalview.hidden=true;
            _mobileview.hidden=false;
            vw_table.hidden=YES;
            
        }
        else if ([txttypebillvalue isEqualToString:@"UBL Bills"])
        {
            //NSLog(@"UBL bill");
            gblclass.bill_type=@"UBL Bills";
            
            self.txtbilltype.text=@"";
            vw_table.hidden=YES;
            
            storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
            vc = [storyboard instantiateViewControllerWithIdentifier:@"ublbillmanagement"];
            [self presentViewController:vc animated:NO completion:nil];
            
        }
        else if ([txttypebillvalue isEqualToString:@"Broadband Internet Bills"])
        {
            //NSLog(@"Internet Bill");
            gblclass.bill_type=@"Broadband Internet Bills";
            
            _mobiletxtnick.hidden=true;
            _txtphonenumber.placeholder=@"Enter consumer number";
            _txtphonenumber.keyboardType = UIKeyboardTypeDefault;
            [self GetBillCompaniesDetail:@"ISP"];
            _normalview.hidden=NO;
            _normaltxtnick.enabled=NO;
            
            _mobileview.hidden=true;
            vw_table.hidden=YES;
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
    }
    
    else if(tableView==_tableviewcompany)
    {
        [_tableviewcompany reloadData];
         split = [[companytype objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        self.txtcompany.text=[split objectAtIndex:0];
        _txtphonenumber.enabled=YES;
        _normaltxtnick.enabled=YES;
        //self.txtcompany.text=[name objectAtIndex:indexPath.row];
        //      self.mobiletxtnick.text=@"";
        //      self.normaltxtnick.text=@"";
        self.txtphonenumber.text=@"";
        _normaltxtnick.text = @"";
        txtbillservice.text = @"";
        _txt_service.text = @"";
        
        strTtId=@"";
        strAccessKey=@"";
        strType=@"";
        strtxtNick=@"";
        strtxtCustomerID=@"";
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
        myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
        
        
        vw_table.hidden=YES;
        
        [hud showAnimated:YES];
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"select_service";
            [self SSL_Call];
            
            return;
        }
         
        
        if ([self.txtbilltype.text isEqualToString:@"Utility Bills"])
        {
//            split = [[placeholdername objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
//
//            gblclass.bill_type=@"Utility Bills";
//            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
//            strTtId=[ttid objectAtIndex:indexPath.row];
//
//
//            _txtphonenumber.placeholder=[split objectAtIndex:0];
//            LENGTH_MAX=[[split objectAtIndex:1] intValue];
//            LENGTH_MIN=[[split objectAtIndex:2] intValue];
//            str_TcAccess_key=[tc_access_key objectAtIndex:indexPath.row];
            
            //strType=@"MOBILEBILL";
            
        }
        else if ([self.txtbilltype.text isEqualToString:@"Mobile Bills"])
        {
            //            self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            //            fetchtitlebranchcode=[branchcode objectAtIndex:indexPath.row];
            
            gblclass.bill_type=@"Mobile Bills";
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            strTtId=[ttid objectAtIndex:indexPath.row];
            strType=@"MOBILEBILL";
            str_TcAccess_key=[tc_access_key objectAtIndex:indexPath.row];
        }
        else if ([self.txtbilltype.text isEqualToString:@"Broadband Internet Bills"] || [self.txtbilltype.text isEqualToString:@"ISP Bill"])
        {
            // fetchtitlebranchcode=@"1256";
            
            split = [[placeholdername objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            gblclass.bill_type=@"Broadband Internet Bills";
            strAccessKey=[ttacceskey objectAtIndex:indexPath.row];
            strTtId=[ttid objectAtIndex:indexPath.row];
            strType=@"ISPP";
            str_TcAccess_key=[tc_access_key objectAtIndex:indexPath.row];
            
            _normaltxtnick.hidden=NO;
            _normalview.hidden=YES;
            
            
            _txtphonenumber.placeholder=[split objectAtIndex:0];
            LENGTH_MAX=[[split objectAtIndex:1] intValue];
            LENGTH_MIN=[[split objectAtIndex:2] intValue];
            
        }
        else if ([self.txtbilltype.text isEqualToString:@"UBL Bills"])
        {
            //            self.txtcompany.text = [branchname objectAtIndex:indexPath.row];
            //            bankimd=[branchcode objectAtIndex:indexPath.row];
            //            bankname=[branchname objectAtIndex:indexPath.row];
            //            fetchtitlebranchcode=@"";
        }
    }
    
    if (tableView == _table_service)
    {
               [_table_service reloadData];
        
                split = [[arr_select_service objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
               _txt_service.text=[split objectAtIndex:2];
                merchant_id = [split objectAtIndex:1];
               _txtphonenumber.enabled=YES;
               _normaltxtnick.enabled=YES;
               //self.txtcompany.text=[name objectAtIndex:indexPath.row];
               //      self.mobiletxtnick.text=@"";
               //      self.normaltxtnick.text=@"";
               self.txtphonenumber.text=@"";
              _normaltxtnick.text = @"";
               strTtId=[split objectAtIndex:5];
               strAccessKey = [split objectAtIndex:0]; //1
               str_TcAccess_key = [split objectAtIndex:3]; //1
               strType=@"";
               strtxtNick=@"";
               strtxtCustomerID=@"";
               
               UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
               UIImageView *myImg = (UIImageView *)[cell viewWithTag:1];
               myImg.image = [UIImage imageNamed:@"check_select_acct.png"];
    }
    
    vw_table.hidden=YES;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if(tableView== _tableviewbillservice){
    //        return [bill_service count];
    //    }
    //    else
    
    if(tableView== _tableviewbilltype)
    {
        if([bill_type count]==0)
        {
            return 0;
        }
        else
        {
            return [bill_type count];
        }
    }
    else if(tableView== _tableviewcompany)
    {
        if([companytype count]==0)
        {
            return 0;
        }
        else
        {
            return [companytype  count];
        }
    }
    else if(tableView== _table_service)
    {
        if([arr_select_service count]==0)
        {
            return 0;
        }
        else
        {
            return [arr_select_service count];
        }
        
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(tableView==_tableviewbilltype)
    {
        
        label=(UILabel*)[cell viewWithTag:2];
        label.text=[bill_type objectAtIndex:indexPath.row];
        label.font=[UIFont systemFontOfSize:12];
        label.textColor=[UIColor whiteColor];
        [cell.contentView addSubview:label];
        
        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
        // [img setImage:@"accounts.png"];
        
        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        else
        {
            img.image=[UIImage imageNamed:@"Non.png"];
        }
        
        [cell.contentView addSubview:img];
        
    }
    else if(tableView==_tableviewcompany)
    {
        
        
       // NSDictionary *dic = [companytype objectAtIndex:indexPath.row];
        
        //NSLog(@"%@",self.txtbilltype.text);
        if([self.txtbilltype.text isEqualToString:@"Utility Bills"])
        {
            split = [[companytype objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
            
            label=(UILabel*)[cell viewWithTag:3];
            label.text=[split objectAtIndex:0];
            label.font=[UIFont systemFontOfSize:12];
            label.textColor=[UIColor whiteColor];
            [cell.contentView addSubview:label];
            
            UIImageView* img=(UIImageView*)[cell viewWithTag:1];
            // [img setImage:@"accounts.png"];
            
            if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            else
            {
                img.image=[UIImage imageNamed:@"Non.png"];
            }
            
            [cell.contentView addSubview:img];
            
            
     //saki 29 jan       [name addObject:[dic objectForKey:@"NAME"]];
            //[ttid addObject:[dic objectForKey:@"TT_ID"]];
           // [ttacceskey addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
      //saki 29 jan      [tc_access_key addObject:[dic objectForKey:@"TC_ACCESS_KEY"]];
            
            // [ placeholdername addObject:[dic objectForKey:@"SHOW_CONSUMER_NO"]];
            
//            NSString* SHOW_CONSUMER_NO=[dic objectForKey:@"SHOW_CONSUMER_NO"];
//            [a addObject:SHOW_CONSUMER_NO];
//            NSString* LENGTHS_MAX=[dic objectForKey:@"LENGTH_MAX"];
//            [a addObject:LENGTHS_MAX];
//            NSString* LENGTHS_MIN=[dic objectForKey:@"LENGTH_MIN"];
//            [a addObject:LENGTHS_MIN];
            
//            NSString *bbb = [a componentsJoinedByString:@"|"];
//            [placeholdername addObject:bbb];
//            [a removeAllObjects];
            
            // self.txtphonenumber.placeholder = [placeholdername objectAtIndex:indexPath.row];
            
        }
        else  if([self.txtbilltype.text isEqualToString:@"Mobile Bills"])
        {
            
            //            [branchname addObject:[dic objectForKey:@"brdetail"]];
            //            [branchcode addObject:[dic objectForKey:@"brcode"]];
            
//            [name addObject:[dic objectForKey:@"NAME"]];
//            [ttid addObject:[dic objectForKey:@"TT_ID"]];
//            [ttacceskey addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
//            [tc_access_key addObject:[dic objectForKey:@"TC_ACCESS_KEY"]];
        }
        else  if([self.txtbilltype.text isEqualToString:@"Broadband Internet Bills"] || [self.txtbilltype.text isEqualToString:@"ISP Bill"])
        {
            
//            //            [branchname addObject:[dic objectForKey:@"brdetail"]];
//            //            [branchcode addObject:[dic objectForKey:@"brcode"]];
//            [name addObject:[dic objectForKey:@"NAME"]];
//            [ttid addObject:[dic objectForKey:@"TT_ID"]];
//            [ttacceskey addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
//            [tc_access_key addObject:[dic objectForKey:@"TT_ACCESS_KEY"]];
//
//            //  [ placeholdername addObject:[dic objectForKey:@"SHOW_CONSUMER_NO"]];
//
//
//            NSString* SHOW_CONSUMER_NO=[dic objectForKey:@"SHOW_CONSUMER_NO"];
//            [a addObject:SHOW_CONSUMER_NO];
//            NSString* LENGTHS_MAX=[dic objectForKey:@"LENGTH_MAX"];
//            [a addObject:LENGTHS_MAX];
//            NSString* LENGTHS_MIN=[dic objectForKey:@"LENGTH_MIN"];
//            [a addObject:LENGTHS_MIN];
//
//            NSString *bbb = [a componentsJoinedByString:@"|"];
//            [placeholdername addObject:bbb];
//            [a removeAllObjects];
//
//
//            //  self.txtphonenumber.placeholder = [placeholdername objectAtIndex:indexPath.row];
        
            
            
        }
        
        
        
//        UIImageView* img=(UIImageView*)[cell viewWithTag:1];
//        // [img setImage:@"accounts.png"];
//
//        if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
//        {
//            img.image=[UIImage imageNamed:@"Non.png"];
//        }
//        else
//        {
//            img.image=[UIImage imageNamed:@"Non.png"];
//        }
//
//        [cell.contentView addSubview:img];
//
//        label.text=[name objectAtIndex:indexPath.row];
//        label.font=[UIFont systemFontOfSize:12];
//        label.textColor=[UIColor whiteColor];
//        [cell.contentView addSubview:label];
        
        
    }
    else if(tableView==_table_service)
    {
             split = [[arr_select_service objectAtIndex:indexPath.row] componentsSeparatedByString: @"|"];
        
              label=(UILabel*)[cell viewWithTag:2];
              label.text=[split objectAtIndex:2];
              label.font=[UIFont systemFontOfSize:12];
              label.textColor=[UIColor whiteColor];
              [cell.contentView addSubview:label];
              
              UIImageView* img=(UIImageView*)[cell viewWithTag:1];
              // [img setImage:@"accounts.png"];
              
              if ([gblclass.atm_req_gender_flag isEqualToString:@"gender"])
              {
                  img.image=[UIImage imageNamed:@"Non.png"];
              }
              else
              {
                  img.image=[UIImage imageNamed:@"Non.png"];
              }
              
              [cell.contentView addSubview:img];
    }
    
    _tableviewbilltype.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableviewcompany.separatorStyle = UITableViewCellSeparatorStyleNone;
     _table_service.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return cell;
}


-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void) LoadUtilityBills:(NSString *)strIndustry
{
    name = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    placeholdername = [[NSMutableArray alloc]init];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"ME_TELCO"],
                                [encrypt encrypt_Data:@"0"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccessKey",
                                                                   @"strType",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetBillCompaniesServiceProvidersUAE" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];

              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  companytype = [[NSMutableArray alloc] init];
                  NSMutableArray* company_name = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table"];
                  
                  for (dic in company_name)
                  {
                      NSString* COMPANY_NAME=[dic objectForKey:@"NAME"];
                      if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || COMPANY_NAME==(NSString *)[NSNull null])
                      {
                          COMPANY_NAME=@"N/A";
                          [a addObject:COMPANY_NAME];
                      }
                      else
                      {
                          [a addObject:COMPANY_NAME];
                      }
                      
                      NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                      if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || TC_ACCESS_KEY==(NSString *)[NSNull null])
                      {
                          TC_ACCESS_KEY=@"N/A";
                          [a addObject:TC_ACCESS_KEY];
                      }
                      else
                      {
                          [a addObject:TC_ACCESS_KEY];
                      }
                      
                      NSString* bbb = [a componentsJoinedByString:@"|"];
                      [companytype addObject:bbb];
                      [a removeAllObjects];
                  }
                  
                    [_tableviewcompany reloadData];
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
          }];
}

-(void) GetBillCompanies_ServiceTypes_UAE:(NSString *)strIndustry
{
    name = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    placeholdername = [[NSMutableArray alloc]init];
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@"ME_TELCO"],
                                [encrypt encrypt_Data:self.txtcompany.text],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccessKey",
                                                                   @"strSubAccessKey",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetBillCompaniesServiceTypesUAE" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //             NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              

              arr_select_service = [[NSMutableArray alloc] init];
        
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                 NSMutableArray* arr_select = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table"];
                   
                  
                  for (dic in arr_select)
                  {
                      NSString* COMPANY_NAME=[dic objectForKey:@"COMPANY_NAME"];
                      if (COMPANY_NAME.length==0 || [COMPANY_NAME isEqualToString:@" "] || COMPANY_NAME==(NSString *)[NSNull null])
                      {
                          COMPANY_NAME=@"N/A";
                          [a addObject:COMPANY_NAME];
                      }
                      else
                      {
                          [a addObject:COMPANY_NAME];
                      }
                      
                      NSString* MERCHANT_ID=[dic objectForKey:@"MERCHANT_ID"];
                      if (MERCHANT_ID.length==0 || [MERCHANT_ID isEqualToString:@" "] || MERCHANT_ID==(NSString *)[NSNull null])
                      {
                          MERCHANT_ID=@"N/A";
                          [a addObject:MERCHANT_ID];
                      }
                      else
                      {
                          [a addObject:MERCHANT_ID];
                      }
                      
                      NSString* NAME=[dic objectForKey:@"NAME"];
                      if (NAME.length==0 || [NAME isEqualToString:@" "] || NAME==(NSString *)[NSNull null])
                      {
                          NAME=@"N/A";
                          [a addObject:NAME];
                      }
                      else
                      {
                          [a addObject:NAME];
                      }
                      
                      NSString* TC_ACCESS_KEY=[dic objectForKey:@"TC_ACCESS_KEY"];
                      if (TC_ACCESS_KEY.length==0 || [TC_ACCESS_KEY isEqualToString:@" "] || TC_ACCESS_KEY==(NSString *)[NSNull null])
                      {
                          TC_ACCESS_KEY=@"N/A";
                          [a addObject:TC_ACCESS_KEY];
                      }
                      else
                      {
                          [a addObject:TC_ACCESS_KEY];
                      }
                      
                      NSString* TT_ACCESS_KEY=[dic objectForKey:@"TT_ACCESS_KEY"];
                      if (TT_ACCESS_KEY.length==0 || [TT_ACCESS_KEY isEqualToString:@" "] || TT_ACCESS_KEY==(NSString *)[NSNull null])
                      {
                          TT_ACCESS_KEY=@"N/A";
                          [a addObject:TT_ACCESS_KEY];
                      }
                      else
                      {
                          [a addObject:TT_ACCESS_KEY];
                      }
                      
                      NSString* TT_ID= [NSString stringWithFormat:@"%@",[dic objectForKey:@"TT_ID"]];
                      if (TT_ID.length==0 || [TT_ID isEqualToString:@" "] || TT_ID==(NSString *)[NSNull null])
                      {
                          TT_ID=@"N/A";
                          [a addObject:TT_ID];
                      }
                      else
                      {
                          [a addObject:TT_ID];
                      }
                      
                      NSString* bbb = [a componentsJoinedByString:@"|"];
                      [arr_select_service addObject:bbb];
                      [a removeAllObjects];
                  }
                  
                     [_table_service reloadData];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              [_table_service reloadData];
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              
              [self custom_alert:@"Retry" :@"0"];
              
        }];
}


-(void) GetBillCompaniesDetail:(NSString *)methodaccesskey
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    
    
    name = [[NSMutableArray alloc]init];
    ttid = [[NSMutableArray alloc]init];
    ttacceskey = [[NSMutableArray alloc]init];
    placeholdername  = [[NSMutableArray alloc]init];
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:methodaccesskey],
                                [encrypt encrypt_Data:@"MOBILEBILL"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strAccessKey",
                                                                   @"strType",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GetBillCompaniesDetail" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              
              //              GlobalStaticClass *gblclass =  [GlobalStaticClass getInstance];
              //              gblclass.actsummaryarr =
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  companytype = [(NSDictionary *)[dic objectForKey:@"outdtDataset"] objectForKey:@"Table"];
                  //ttid=
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              [_tableviewcompany reloadData];
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
          }];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                                         message:exception
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                       
                       [message show];
                   });
    
    //[alert release];
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex ==0)
    {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        // txtbillservice.text=@"";
        //  _txtbilltype.text=@"";
        
        if ([chk_ssl isEqualToString:@"logout"])
        {
            [self checkinternet];
            if (netAvailable)
            {
                chk_ssl=@"logout";
                [self SSL_Call];
                return;
            }
        }
        
        if([responsecode isEqualToString:@"0"])
        {
            if ([self.txtbilltype.text isEqualToString:@"Broadband Internet Bills"] || [self.txtbilltype.text isEqualToString:@"ISP Bill"])
            {
                _normaltxtnick.hidden=false;
            }
            
            _txtcompany.text=@"";
            _txtphonenumber .text=@"";
            _mobiletxtnick.text=@"";
            _retypemobilenumbernick.text=@"";
            
        }
        else
        {
            responsecode=@"";
        }
    }
}

-(void) AddBillCompanies:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:strTtId],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:strType],
                                [encrypt encrypt_Data:strtxtNick],
                                [encrypt encrypt_Data:strtxtCustomerID], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTtId",
                                                                   @"strAccessKey",
                                                                   @"strMerchanId",
                                                                   @"strtxtNick", @"strtxtCustomerID",
                                                                   @"strtxtNick",nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"AddBillCompaniesUAE" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  [self custom_alert:resstring :@"0"];
                //[self parsearray];
                  [hud hideAnimated:YES];
                //[_tableviewcompany reloadData];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return;
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
              
          }
          failure:^(NSURLSessionDataTask *task, NSError *error)
          {
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
          }];
}


-(void) Generate_OTP:(NSString *)strIndustry
{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    [gblclass.arr_re_genrte_OTP_additn addObjectsFromArray:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,@"",_txtphonenumber.text,strAccessKey,@"Generate OTP",strTtId,str_TcAccess_key,@"MOB", nil]];
    
    
    //    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:gblclass.user_id,gblclass.M3sessionid,[GlobalStaticClass getPublicIp],gblclass.fetchtitlebranchcode,gblclass.acc_number,@"Add Payee List",@"Generate OTP",@"3",@"FT",@"SMS",@"ADDITION",@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo=", nil]
    //                                                          forKeys:[NSArray arrayWithObjects:@"userId",@"strSessionId",@"IP",@"strBranchCode",@"strAccountNo",@"strAccesskey",@"strCallingOTPType",@"TTID",@"TC_AccessKey",@"Channel",@"strMessageType",@"M3Key",nil]];
    
   // strAccessKey = @"ME_TELCO";
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:_txtphonenumber.text],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:@"Generate OTP"],
                                [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",strTtId]],
                                [encrypt encrypt_Data:str_TcAccess_key],
                                [encrypt encrypt_Data:@"SMS"],
                                [encrypt encrypt_Data:@"ADDITION"],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token],
                                [encrypt encrypt_Data:@"JFLaptbW4Ox83hUX1TZGLHdUPQQ0PXtWkQP47Zjcmo="], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"UserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strBranchCode",
                                                                   @"strAccountNo",
                                                                   @"strAccesskey",
                                                                   @"strCallingOTPType",
                                                                   @"TTID",
                                                                   @"TC_AccessKey",
                                                                   @"Channel",
                                                                   @"strMessageType",
                                                                   @"Device_ID",
                                                                   @"Token",
                                                                   @"M3Key", nil]];
    
    //385661
    //NSLog(@"%@",dictparam);
    
    //281167 478837 281283
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"GenerateOTPForAddition" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode =[dic objectForKey:@"Response"];
              
              NSString* resstring =[dic objectForKey:@"strReturnMessage"];
              
              //NSLog(@"Done");
              //[self parsearray];
              [hud hideAnimated:YES];
              //[_tableviewcompany reloadData];
              
              
              if ([responsecode isEqualToString:@"0"])
              {
                //[self showAlert:resstring :@"Attention"];
                //[self custom_alert:resstring :@"0"];
                  
                  CATransition *transition = [CATransition animation];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [self.view.window.layer addAnimation:transition forKey:nil];
                  
                  gblclass.add_bill_type=self.txtbilltype.text;
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"add_bill_otp"];
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
              }
              else
              {
                  [self showAlert:resstring :@"Attention"];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[dic objectForKey:@"strReturnMessage"] preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
              }
              
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              // [mine myfaildata];
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
          }];
}


-(void) AddUtilityCompanies:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:strTtId],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:strtxtNick],
                                [encrypt encrypt_Data:strtxtCustomerID], nil]
                               
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"strTtId",
                                                                   @"strAccessKey",
                                                                   @"strtxtNick",
                                                                   @"strtxtCustomerID",
                                                                   @"strOTPPIN", nil]];
    
    //NSLog(@"%@",dictparam);
    
    [manager POST:@"AddUtilityCompanies" parameters:dictparam progress:nil
    success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              responsecode = [dic objectForKey:@"Response"];
              
              if ([[dic objectForKey:@"Response"] isEqualToString:@"0"])
              {
                  NSString* resstring = [dic objectForKey:@"strReturnMessage"];
                  [self showAlert:resstring :@"Attention"];
                  //NSLog(@"Done");
                  //[self parsearray];
                  [hud hideAnimated:YES];
                  //  [_tableviewcompany reloadData];
                  
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return;
                  
              }
              else
              {
                  [hud hideAnimated:YES];
                  [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
              }
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
              
              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
              [alert addAction:ok];
              [self presentViewController:alert animated:YES completion:nil];
              
          }];
}

-(void) ConfirmAddISPBill:(NSString *)strIndustry{
    
    [hud showAnimated:YES];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    //    name = [[NSMutableArray alloc]init];
    //    ttid = [[NSMutableArray alloc]init];
    //    ttacceskey = [[NSMutableArray alloc]init];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                   ]];//baseURL];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
    
    NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                [encrypt encrypt_Data:gblclass.M3sessionid],
                                [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                [encrypt encrypt_Data:[NSString stringWithFormat:@"%@",strTtId]],
                                [encrypt encrypt_Data:strAccessKey],
                                [encrypt encrypt_Data:strtxtCustomerID],
                                [encrypt encrypt_Data:@""],
                                [encrypt encrypt_Data:gblclass.Udid],
                                [encrypt encrypt_Data:gblclass.token], nil]
                                                          forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                   @"strSessionId",
                                                                   @"IP",
                                                                   @"tt_ID",
                                                                   @"strAccessKey",
                                                                   @"strtxtCustomerID",
                                                                   @"strOTPPIN",
                                                                   @"Device_ID",
                                                                   @"Token", nil]];
    
    [manager.requestSerializer setTimeoutInterval:time_out];
    [manager POST:@"ConfirmAddISPBill" parameters:dictparam progress:nil
     
          success:^(NSURLSessionDataTask *task, id responseObject) {
              //            NSError *error;
              
              NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
              // NSString* responsecode =(NSDictionary *)[dic objectForKey:@"Response"];
              
              NSString* responsecode1 =[dic objectForKey:@"Response"];
              
              if([responsecode1 isEqualToString:@"0"])
              {
                  chk_confirm_add_isp=@"1";
                  branch_selected_value=@"";
                  selected_bill_type=@"ISP";
                  bIsConfirmation_required=@"false";
                  _txtphonenumber.enabled=NO;
                  chck_isp_nick=@"1";
                  
                  _normalview.hidden=NO;
                  _normaltxtnick.hidden=NO;
                  _normaltxtnick.enabled=YES;
                  _normaltxtnick.text=[dic objectForKey:@"strtxtNick"];
                  // strtxtNick": "SYSTEM",
                  //NSLog(@"Response done");
              }
              else if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"])
              {
                  
                  chk_ssl=@"logout";
                  [hud hideAnimated:YES];
                  [self showAlert:[dic objectForKey:@"strReturnMessage"] :@"Attention"];
                  return ;
                  
              }
              else
              {
                  NSString* resstring =[dic objectForKey:@"strReturnMessage"];
                  //   [self showAlert:resstring :@"Attention"];
                  [self custom_alert:resstring :@"0"];
              }
              
              [hud hideAnimated:YES];
          }
     
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              [hud hideAnimated:YES];
              [self custom_alert:@"Retry" :@"0"];
              
          }];
}


-(IBAction)btn_back:(id)sender
{
    CATransition *transition = [ CATransition animation ];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window. layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)btn_Pay:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"payee"]; //pay_within_acct
    
    [self presentViewController:vc animated:NO completion:nil];
}

-(IBAction)btn_more:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"more_slide"];
    vc.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

-(IBAction)btn_logout:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btn_settings:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self presentViewController:vc animated:NO completion:nil];
}


-(IBAction)btn_account:(id)sender
{
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"accounts"];
    
    [self presentViewController:vc animated:NO completion:nil];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    vw_down_chck  = @"1";
    [txtbillservice resignFirstResponder];;
    [_txtbilltype resignFirstResponder];;
    [_txtcompany resignFirstResponder];;
    [_txtphonenumber resignFirstResponder];;
    [_normaltxtnick resignFirstResponder];
    [_retypemobilenumbernick resignFirstResponder];
    
    
    self.billtype.hidden=true;
    self.company.hidden=true;
}

#define kOFFSET_FOR_KEYBOARD 65.0

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
           [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
//        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        if ([vw_down_chck isEqualToString:@"1"])
        {
            [self setViewMovedUp:NO];
            vw_down_chck = @"0";
        }
        //[self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:sender.text])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    //    //NSLog(@"%d",rect );
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        
        //NSLog(@"%f",kOFFSET_FOR_KEYBOARD);
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
        
        
        //NSLog(@"%f", rect.origin.y );
        //NSLog(@"%f",rect.size.height);
        
        
    }
    else
    {
        
       
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
        [self.view endEditing:YES];
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       //  self.fundtextfield.delegate = self;
                       
                       
                       //         [mine showmyhud];
                       //         [mine showWhileExecuting:@selector(getFolionumber) onTarget: self withObject:nil animated:YES];
                       
                       //                       [self getFolionumber];
                   });
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(IBAction)btn_vw_hide:(id)sender
{
    vw_table.hidden=YES;
    _tableviewcompany.hidden=YES;
    _tableviewbilltype.hidden=YES;
}

///////////************************** SSL PINNING START ******************************////////////////




- (IBAction)MakeHTTPRequestTapped
{
    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
    
    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
    [request setHTTPMethod:@"POST"];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self.connection start];
    
    if ([self isSSLPinning])
    {
        [self printMessage:@"Making pinned request"];
    }
    else
    {
        [self printMessage:@"Making non-pinned request"];
    }
    
}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        
        if ([chk_ssl isEqualToString:@"otp"])
        {
            chk_ssl=@"";
            [self Generate_OTP:@""];
        }
        else if ([chk_ssl isEqualToString:@"add_bill"])
        {
            chk_ssl=@"";
            [self AddBillCompanies:@""];
        }
        else if ([chk_ssl isEqualToString:@"select_service"])
           {
               chk_ssl=@"";
               [self GetBillCompanies_ServiceTypes_UAE:@""];
           }
        else if ([chk_ssl isEqualToString:@"isp"])
        {
            chk_ssl=@"";
            [self ConfirmAddISPBill:@""];
        }
        else if ([chk_ssl isEqualToString:@"logout"])
        {
            chk_ssl=@"";
            [self mob_App_Logout:@""];
        }
        
    }
    else
    {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    
    
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            
            [hud hideAnimated:YES];
            
            return ;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    [self.connection cancel];
    if ([chk_ssl isEqualToString:@"otp"])
    {
        chk_ssl=@"";
        [self Generate_OTP:@""];
    }
    else if ([chk_ssl isEqualToString:@"add_bill"])
    {
        chk_ssl=@"";
        [self AddBillCompanies:@""];
    }
    else if ([chk_ssl isEqualToString:@"select_service"])
    {
        chk_ssl=@"";
        [self GetBillCompanies_ServiceTypes_UAE:@""];
    }
    else if ([chk_ssl isEqualToString:@"isp"])
    {
        chk_ssl=@"";
        [self ConfirmAddISPBill:@""];
    }
    else if ([chk_ssl isEqualToString:@"logout"])
    {
        chk_ssl=@"";
        [self mob_App_Logout:@""];
    }
    
    [self.connection cancel];
    
}



-(void) mob_App_Logout:(NSString *)strIndustry
{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.user_id],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:@"abcd"],
                                    [encrypt encrypt_Data:gblclass.token], nil]
                                                              forKeys:[NSArray arrayWithObjects:@"strUserId",
                                                                       @"strDeviceID",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"hCode",
                                                                       @"Token", nil]];
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"mobAppLogout" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  NSMutableArray* arr_display_trans;
                  
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"login_new"];
                  [self presentViewController:vc animated:YES completion:nil];
                  
                  
                  [hud hideAnimated:YES];
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
                  vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
                  //                  alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
                  //
                  //                  UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  //
                  //                  [alert addAction:ok];
                  //                  [self presentViewController:alert animated:YES completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
        //        alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Please try again later."  preferredStyle:UIAlertControllerStyleAlert];
        //
        //        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        //        [alert addAction:ok];
        //        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}



- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 600 533 335" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
   // NSLog(@"%@", error.localizedDescription);
}



- (NSData *)skabberCert
{
//    NSLog(@"%@", gblclass.SSL_name);
//    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    NSString *existingMessage = self.textOutput.text;
    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
 //   NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////


#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            [hud hideAnimated:YES];
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons
{
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    storyboard = [UIStoryboard storyboardWithName:gblclass.story_board bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

@end

