//
//  AccountProgressVC.m
//  ubltestbanking
//
//  Created by Asim Raza Khan on 19/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import "AccountProgressVC.h"
#import "UIView+UIViewCatagory.h"
#import "GlobalStaticClass.h"
#import <PeekabooConnect/PeekabooConnect.h>
#import "GlobalClass.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "Encrypt.h"
#import <AVFoundation/AVFoundation.h>

@interface AccountProgressVC () {
    
    NSURLConnection *connection;
    GlobalStaticClass *gblclass;
    UIStoryboard *mainStoryboard;
    NSMutableData *responseData;
    Reachability *internetReach;
    UIViewController *vc;
    BOOL netAvailable;
    MBProgressHUD* hud;
    NSString* chk_ssl;
    Encrypt *encrypt;
    NSUserDefaults *defaults;
    NSString* ssl_count;
    NSString* t_n_c;
    NSMutableArray* a;
    NSString*  first_time_chk;
    NSString *stepedVC;
}

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *responseData;

@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic, strong) AVPlayer *avplayer;

@property (strong, nonatomic) IBOutlet UIView *outerCircleView;
@property (strong, nonatomic) IBOutlet UIView *innerCircleView;
@property (strong, nonatomic) IBOutlet UILabel *trackingLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpOutlet;

- (IBAction)showAccountStatus:(id)sender;

- (IBAction)haveAnAccount:(id)sender;
- (IBAction)btn_cancel:(id)sender;

- (IBAction)btn_back:(id)sender;
- (IBAction)btn_next:(id)sender;
- (IBAction)btn_help:(id)sender;

- (IBAction)btn_feature:(id)sender;
- (IBAction)btn_offer:(id)sender;
- (IBAction)btn_find_us:(id)sender;
- (IBAction)btn_faq:(id)sender;

@end

@implementation AccountProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self Play_bg_video];
    [self playerStartPlaying];
    
    
    encrypt = [[Encrypt alloc] init];
    gblclass = [GlobalStaticClass getInstance];
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    defaults = [NSUserDefaults standardUserDefaults];
    t_n_c = [defaults valueForKey:@"t&c"];
    a = [[NSMutableArray alloc] init];
    gblclass.noOfPerformedAttempts = 0;
    
    ssl_count = @"0";
    
    first_time_chk = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"enable_touch"];
    
    stepedVC = @"";
    
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    [self checkinternet];
    if (netAvailable)
    {
        chk_ssl=@"getOnBoardStatus";
        [self SSL_Call];
    }
    
    //    [self uiMakeup];
}

-(void)viewWillAppear:(BOOL)animated
{
    gblclass.step_no_onbording = @"6";
//    gblclass.step_no_help_onbording = @"10";
    gblclass.step_no_help_onbording = @"DOB09";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

// MARK: - Custom functions

-(void)Play_bg_video {
    
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    
    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    
    if ([self.avplayer respondsToSelector:NSSelectorFromString(@"_preventsSleepDuringVideoPlayback")]) {
        [self.avplayer setValue:@(NO) forKey:@"preventsSleepDuringVideoPlayback"];
    }
    
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Config dark gradient view
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [[UIScreen mainScreen] bounds];
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
    
    //    //Not affecting background music playing
    //    NSError *sessionError = nil;
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    //    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    //
    //    //Set up player
    //    NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
    //
    //    //   NSURL *movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"welcome_video" ofType:@"mp4"]];
    //
    //    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    //    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    //    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    //    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    //    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    //    [self.movieView.layer addSublayer:avPlayerLayer];
    //
    //    //Config player
    //    [self.avplayer seekToTime:kCMTimeZero];
    //    [self.avplayer setVolume:0.0f];
    //    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerItemDidReachEnd:)
    //                                                 name:AVPlayerItemDidPlayToEndTimeNotification
    //                                               object:[self.avplayer currentItem]];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(playerStartPlaying)
    //                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    //
    //    //Config dark gradient view
    //    CAGradientLayer *gradient = [CAGradientLayer layer];
    //    gradient.frame = [[UIScreen mainScreen] bounds];
    //    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(0x030303) CGColor], (id)[[UIColor clearColor] CGColor], (id)[UIColorFromRGB(0x030303) CGColor],nil];
    //    [self.gradientView.layer insertSublayer:gradient atIndex:0];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)playerStartPlaying {
    [self.avplayer play];
}


-(void)uiMakeup {
    
    [self.innerCircleView addCircleViewBorderWidth:7 withColor:[[UIColor colorWithWhite:1 alpha:0.5] CGColor]];
    [self.innerCircleView animateCircleDuration:1.5 divideCircleBy:1];
    
    [self.innerCircleView addCircleViewBorderWidth:7 withColor:[[UIColor whiteColor] CGColor]];
    [self.innerCircleView animateCircleDuration:1.5 divideCircleBy:1/([[(NSDictionary *)[(NSArray *)[[gblclass.accountStatus objectForKey:@"dtGetCurrentStatus"] objectForKey:@"CURRENT_STATUS"] lastObject] objectForKey:@"PERCENTAGE"] floatValue]/100)];
    self.outerCircleView.layer.cornerRadius = self.outerCircleView.frame.size.height/2;
    self.outerCircleView.layer.borderWidth = 1;
    self.outerCircleView.layer.borderColor = [[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:0.2] CGColor];
}

-(void)moreInfo:(UITapGestureRecognizer *)recognizer {
    
    //    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //
    //    [self slide_right];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccountStatusVC"];
    //    [self presentViewController:vc animated:NO completion:nil];
}


-(void) removeUserdata {
    [defaults removeObjectForKey:@"name"];
    [defaults removeObjectForKey:@"cnic"];
    [defaults removeObjectForKey:@"mobile_num"];
    [defaults removeObjectForKey:@"email"];
    [defaults removeObjectForKey:@"nationality"];
    [defaults removeObjectForKey:@"req_id"];
    [defaults synchronize];
}

-(void)custom_alert :(NSString* )msg1 : (NSString*)icons {
    gblclass.custom_alert_msg=msg1;
    gblclass.custom_alert_img=icons;
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.view.alpha = alpha1;
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)slide_right {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
}


-(void)slide_left {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [ self.view.window.layer addAnimation:transition forKey:nil];
}

- (void)receiveNetworkChnageNotification:(NSNotification *)notification
{
    NSLog(@"%@",notification);
}


//////////************************** LOCATION MANAGER DELEGATE ***************************///////////

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    gblclass.current_location_onbording = [locations lastObject];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [self custom_alert:@"Failed to get your location." :@"0"];
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied){
        [self slide_left];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"EnableGpsVC"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}


//////////************************** END LOCATION MANAGER DELEGATE ***************************///////////

///////////************************** SSL PINNING START ******************************////////////////


//- (IBAction)MakeHTTPRequestTapped
//{
//    //NSURL *httpsURL = [NSURL URLWithString:@"https://secure.skabber.com/json/"];
//
//    NSURL *httpsURL = [NSURL URLWithString:ssl_pinning_url];
//
//    NSURLRequest *request = [NSURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
//    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
//    [self.connection start];
//
//    if ([self isSSLPinning])
//    {
//        [self printMessage:@"Making pinned request"];
//    }
//    else
//    {
//        [self printMessage:@"Making non-pinned request"];
//    }
//
//}

-(void)SSL_Call
{
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""]) {
        
        if ([chk_ssl isEqualToString:@"getOnBoardData"])
        {
            chk_ssl=@"";
            [self GetOnBoardData:@""];
        }
        else if ([chk_ssl isEqualToString:@"getOnBoardStatus"])
        {
            chk_ssl=@"";
            [self GetOnBoardStatus:@""];
        }
        else if ([chk_ssl isEqualToString:@"getOnBoardCustomerInfo"])
        {
            chk_ssl=@"";
            [self GetOnBoardCustomerInfo:@""];
        } else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
        {
            chk_ssl=@"";
            [self NeedOnBoardHelp:@""];
        }
        else if ([chk_ssl isEqualToString:@"getTokenRequest"])
        {
            chk_ssl = @"";
            //            [self GetTokenRequest:@""];
        }
        else if ([chk_ssl isEqualToString:@"qr"])
        {
            chk_ssl=@"";
            [self Is_Instant_Pay_Allow:@""];
        }
        
    } else {
        NSURL *httpsURL = [NSURL URLWithString:gblclass.mainurl1];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:httpsURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15.0f];
        [request setHTTPMethod:@"POST"];
        self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
        [self.connection start];
    }
    
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSData *skabberCertData = [self skabberCert];
    
    if ([self isSSLPinning])
    {
        if ([remoteCertificateData isEqualToData:skabberCertData])
        {
            [self printMessage:@"The server's certificate is the valid SSL PINNING certificate. Allowing the request."];
            gblclass.ssl_pin_check=@"1";
            
            //[self Existing_UserLogin:@""];
            
        }
        else
        {
            [self.connection cancel];
            
            if ([ssl_count isEqualToString:@"0"])
            {
                ssl_count = @"1";
                [gblclass SSL_name_set:gblclass.SSL_Certificate_name];
                [gblclass SSL_name_get];
                
                [self SSL_Call];
                return;
            }
            
            [self custom_alert:@"Authentication Failed." :@"0"];
            [self printMessage:@"The server's certificate does not match secure.skabber.com. Continuing anyway."];
            
            gblclass.ssl_pin_check=@"0";
            [hud hideAnimated:YES];
            
            return;
        }
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    
    
    if ([chk_ssl isEqualToString:@"getOnBoardData"])
    {
        chk_ssl=@"";
        [self GetOnBoardData:@""];
    }
    else if ([chk_ssl isEqualToString:@"getOnBoardStatus"])
    {
        chk_ssl=@"";
        [self GetOnBoardStatus:@""];
    } else if ([chk_ssl isEqualToString:@"needOnBoardHelp"])
    {
        chk_ssl=@"";
        [self NeedOnBoardHelp:@""];
    }
    else if ([chk_ssl isEqualToString:@"getOnBoardCustomerInfo"])
    {
        chk_ssl=@"";
        [self GetOnBoardCustomerInfo:@""];
    }
    else if ([chk_ssl isEqualToString:@"qr"])
    {
        chk_ssl=@"";
        [self Is_Instant_Pay_Allow:@""];
        [self.connection cancel];
    }
    
    //    if ([chk_ssl isEqualToString:@"getOnBoardStatus"])
    //    {
    //        chk_ssl=@"";
    //        [self GetOnBoardStatus:@""];
    //    } else if ([chk_ssl isEqualToString:@"getOnBoardCustomerInfo"]) {
    //        chk_ssl=@"";
    //        [self GetOnBoardCustomerInfo:@""];
    //    }
    
    [self.connection cancel];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [NSMutableData dataWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *response = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    [self printMessage:response];
    self.responseData = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [hud hideAnimated:YES];
    if(error.code == NSURLErrorTimedOut) {
        [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
    } else if (error.code == NSURLErrorNotConnectedToInternet) {
        [self custom_alert:@"Internet Access Not Available" :@"0"];
    }
    //NSLog(@"%@", error.localizedDescription);
}


- (NSData *)skabberCert
{
    //    NSLog(@"%@", gblclass.SSL_name);
    //    NSLog(@"%@", gblclass.SSL_type);
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:gblclass.SSL_name ofType:gblclass.SSL_type];
    
    return [NSData dataWithContentsOfFile:cerPath];
}


- (void)printMessage:(NSString *)message
{
    //    NSString *existingMessage = self.textOutput.text;
    //    self.textOutput.text = [existingMessage stringByAppendingFormat:@"\n%@", message];
}


- (BOOL)isSSLPinning
{
    
    //  NSLog(@"%@ - No",gblclass.ssl_pinning_url1);
    
    if ([gblclass.ssl_pinning_url1 isEqualToString:@""])
    {
        return false;
    }
    else if (gblclass.ssl_pinning_url1 == nil)
    {
        return false;
    }
    else
    {
        return true;
    }
    
    //NSString *envValue = [[[NSProcessInfo processInfo] environment] objectForKey:@"SSL_PINNING"];
    //if (envValue==nil){
    //    return false;}
    //else{
    //    return [envValue boolValue];
    //}
    
}


///////////************************** SSL PINNING END ******************************////////////////

-(void)GetOnBoardStatus:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [defaults valueForKey:@"sr_num"], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token",
                                                                       @"SRNumber", nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardStatus" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  } else if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      gblclass.accountStatus = dic;
                      self.trackingLabel.text = [[[[dic objectForKey:@"dtGetCurrentStatus"] objectForKey:@"CURRENT_STATUS"] lastObject] objectForKey:@"STATUS_DESC"];
                      
                      if ([[[[[dic objectForKey:@"dtGetCurrentStatus"] objectForKey:@"CURRENT_STATUS"] lastObject] objectForKey:@"STATUS_CODE"] isEqualToString:@"011"])//013
                      {
                          
                          if (!([dic  isKindOfClass:[NSNull class]] || [dic count] == 0))
                          {
                              gblclass.accountStatus = dic;
                          }
                          
                          [self custom_alert:@"Your request has been successfuly completed!" :@"0"];
                          
                          //                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:self.trackingLabel.text
                          //                                                                            message:@"Your request has been successfuly completed!"
                          //                                                                           delegate:self
                          //                                                                  cancelButtonTitle:@"OK"
                          //                                                                  otherButtonTitles:nil];
                          //
                          //                          [alertView show];
                          
                          self.needHelpOutlet.hidden = YES;
                          [self removeUserdata];
                          [defaults removeObjectForKey:@"apply_acct"];
                          [defaults synchronize];
                          
                          
                          //                          [self slide_right];
                          //                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                          //                          [self presentViewController:vc animated:NO completion:nil];
                          
                      }
                      
                      self.outerCircleView.hidden = NO;
                      [self uiMakeup];
                  }  else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  //                  {
                  //                      CompletionStatus = "<null>";
                  //                      Response = 1;
                  //                      dtGetCurrentStatus =     {
                  //                          "CURRENT_STATUS" =         (
                  //                                                      {
                  //                                                          PERCENTAGE = 34;
                  //                                                          "STATUS_CODE" = 013;
                  //                                                          "STATUS_DESC" = "Account fully activated";
                  //                                                      }
                  //                                                      );
                  //                      };
                  //                      dtGetStatusDetail =     {
                  //                          "STATUS_ROW" =         (
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = Y;
                  //                                                      "STATUS_CODE" = 001;
                  //                                                      "STATUS_DESC" = "You have been assigned a UBL representative";
                  //                                                  },
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = N;
                  //                                                      "STATUS_CODE" = 002;
                  //                                                      "STATUS_DESC" = "Meeting with the representative setup / Waiting for your branch visit";
                  //                                                  },
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = N;
                  //                                                      "STATUS_CODE" = 004;
                  //                                                      "STATUS_DESC" = "Congrats! Account number is created";
                  //                                                  },
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = N;
                  //                                                      "STATUS_CODE" = 010;
                  //                                                      "STATUS_DESC" = "Welcome Pack dispatched";
                  //                                                  },
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = N;
                  //                                                      "STATUS_CODE" = 011;
                  //                                                      "STATUS_DESC" = "Welcome pack delivered-Call UBL to activate your debit Card";
                  //                                                  },
                  //                                                  {
                  //                                                      "ALL_STATUS_Id" = 0;
                  //                                                      INDICATION = C;
                  //                                                      "STATUS_CODE" = 013;
                  //                                                      "STATUS_DESC" = "Account fully activated";
                  //                                                  }
                  //                                                  );
                  //                      };
                  //                      strReturnMessage = "Transaction sucessfull";
                  //                  }
                  
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


-(void) Is_Instant_Pay_Allow:(NSString *)strIndustry{
    
    @try {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
                                                                                       ]];//baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        // [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:gblclass.header_user password:gblclass.header_pw];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.validatesDomainName = YES;
        manager.securityPolicy = policy;
        
        if ([t_n_c isEqualToString:@"2"])
        {
            t_n_c = @"1";
        }
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:@"1234"],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.device_chck],
                                    [encrypt encrypt_Data:t_n_c], nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"isRooted",
                                                                       @"isTncAccepted", nil]];
        
        
        //        public void IsInstantPayAllowRooted(string strSessionId, string IP, string Device_ID, Int64 isRooted, string isTncAccepted)
        
        
        //NSLog(@"%@",dictparam);
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"IsInstantPayAllowRooted" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  // NSError *error;
                  
                  NSDictionary *dic2 = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  
                  if ([[dic2 objectForKey:@"Response"] isEqualToString:@"0"])
                  {
                      
                      //   setQRInstant
                      if ([[dic2 objectForKey:@"is_InstantPay"] isEqualToString:@"1"]) {
                          
                          gblclass.chk_qr_demo_login=@"1";
                          gblclass.instant_credit_chk=[dic2 objectForKey:@"isCredit"];
                          gblclass.instant_debit_chk=[dic2 objectForKey:@"isDebit"];
                          gblclass.M3Session_ID_instant=[dic2 objectForKey:@"strRetSessionId"];
                          gblclass.token_instant=[dic2 objectForKey:@"Token"];
                          gblclass.user_id=[dic2 objectForKey:@"strUserId"];
                          
                          gblclass.arr_instant_qr=[[NSMutableArray alloc] init];
                          
                          
                          [gblclass.arr_instant_qr removeAllObjects];
                          
                          NSString* acct_name=[dic2 objectForKey:@"is_InstantPay"];
                          
                          if (acct_name.length==0 || [acct_name isEqualToString:@"<nil>"] || [acct_name isEqualToString:NULL])
                          {
                              acct_name=@"N/A";
                              [a addObject:acct_name];
                          }
                          else
                          {
                              [a addObject:acct_name];
                          }
                          
                          NSString* strAccountTitle=[dic2 objectForKey:@"strAccountTitle"];
                          
                          if (strAccountTitle.length==0 || [strAccountTitle isEqualToString:@"<nil>"] || [strAccountTitle isEqualToString:NULL])
                          {
                              strAccountTitle=@"N/A";
                              [a addObject:strAccountTitle];
                          }
                          else
                          {
                              [a addObject:strAccountTitle];
                          }
                          
                          NSString* strDefaultAccQRCode=[dic2 objectForKey:@"strDefaultAccQRCode"];
                          
                          if (strDefaultAccQRCode.length==0 || [strDefaultAccQRCode isEqualToString:@"<nil>"] || [strDefaultAccQRCode isEqualToString:NULL])
                          {
                              strDefaultAccQRCode=@"N/A";
                              [a addObject:strDefaultAccQRCode];
                          }
                          else
                          {
                              [a addObject:strDefaultAccQRCode];
                          }
                          
                          NSString* strDefaultAccountMask=[dic2 objectForKey:@"strDefaultAccountMask"];
                          
                          if (strDefaultAccountMask.length==0 || [strDefaultAccountMask isEqualToString:@"<nil>"] || [strDefaultAccountMask isEqualToString:NULL])
                          {
                              strDefaultAccountMask=@"N/A";
                              [a addObject:strDefaultAccountMask];
                          }
                          else
                          {
                              [a addObject:strDefaultAccountMask];
                          }
                          
                          NSString* strMobileNo=[dic2 objectForKey:@"strMobileNo"];
                          
                          if (strMobileNo ==(NSString *) [NSNull null])
                          {
                              strMobileNo=@"";
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          else
                          {
                              gblclass.Mobile_No=strMobileNo;
                              [a addObject:strMobileNo];
                          }
                          
                          //
                          
                          NSString *bbb = [a componentsJoinedByString:@"|"];//returns a pointer to NSString
                          [gblclass.arr_instant_qr addObject:bbb];
                          
                          bbb=@"";
                          
                          
                          
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.3;
                          transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromRight;
                          [self.view.window.layer addAnimation:transition forKey:nil];
                          
                          mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"send_qr_login"]; //   receive_qr_login login_instant_qr
                          [self presentViewController:vc animated:NO completion:nil];
                          
                          
                      }
                      
                      [hud hideAnimated:YES];
                  }
                  else
                  {
                      [hud hideAnimated:YES];
                      
                      gblclass.custom_alert_msg=[dic2 objectForKey:@"strReturnMessage"];
                      gblclass.custom_alert_img=@"0";
                      
                      mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                      vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                      vc.view.alpha = alpha1;
                      [self presentViewController:vc animated:NO completion:nil];
                  }
                  
              }
         
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  // [mine myfaildata];
                  [hud hideAnimated:YES];
                  
                  gblclass.custom_alert_msg=@"Please try again later.";
                  gblclass.custom_alert_img=@"0";
                  
                  
                  CATransition *transition = [ CATransition animation ];
                  transition.duration = 0.3;
                  transition.timingFunction = [ CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseInEaseOut];
                  transition.type = kCATransitionPush;
                  transition.subtype = kCATransitionFromRight;
                  [ self.view.window. layer addAnimation:transition forKey:nil];
                  
                  
                  mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                  vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
                  vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                  vc.view.alpha = alpha1;
                  [self presentViewController:vc animated:NO completion:nil];
                  
              }];
        
    }
    @catch (NSException *exception)
    {
        [hud hideAnimated:YES];
        
        gblclass.custom_alert_msg=@"Please try again later.";
        gblclass.custom_alert_img=@"0";
        
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"alert"];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.view.alpha = alpha1;
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}



-(void)GetOnBoardCustomerInfo:(NSString *)strIndustry
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.token],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"RequestId",    nil]];
        
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardCustomerInfo" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  //                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==1) {
                      
                      gblclass.user_name_onbording = [[[dic objectForKey:@"Request"] lastObject] objectForKey:@"NAME"];
                      gblclass.cnic_onbording = [[[dic objectForKey:@"Request"] lastObject] objectForKey:@"CNIC"];
                      gblclass.email_onbording = [[[dic objectForKey:@"Request"] lastObject] objectForKey:@"EMAIL"];
                      gblclass.mobile_number_onbording = [[[dic objectForKey:@"Request"] lastObject] objectForKey:@"MOBILE"];
                      
                      gblclass.sr_number = [[[dic objectForKey:@"Request"] lastObject] objectForKey:@"TRACKING_ID"];
                      
                      [self GetOnBoardStatus:@""];
                      
                  }  else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

-(void)GetOnBoardData:(NSString *)strIndustry {
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
                                                                       [encrypt encrypt_Data:gblclass.M3sessionid],
                                                                       [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                                                       [encrypt encrypt_Data:gblclass.Udid],
                                                                       [encrypt encrypt_Data:gblclass.request_id_onbording],
                                                                       [encrypt encrypt_Data:gblclass.token],  nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
                                                                       @"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"RequestId",
                                                                       @"Token", nil]];
        
        
        //    strSessionId:
        //    IP:
        //    Device_ID:
        //    RequestId:
        //    Token:
        //    strCNIC:
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"GetOnBoardData" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                  }
                  else if ([[dic objectForKey:@"Response"] integerValue]==0) {
                      
                      [[dic objectForKey:@"Accounts"] enumerateObjectsUsingBlock:^(NSArray *item, NSUInteger index, BOOL *stop) {
                          [gblclass.selected_services_onbording addObject:item];
                      }];
                      
                      [[dic objectForKey:@"SelectedProducts"] enumerateObjectsUsingBlock:^(NSArray *item, NSUInteger index, BOOL *stop) {
                          [gblclass.selected_services_onbording addObject:item];
                      }];
                      
                      
                      NSDictionary *otherInfo = [[dic objectForKey:@"OtherInfo"] firstObject];
                      
                      gblclass.branch_code_onbording = [otherInfo objectForKey:@"strBranchCode"];
                      gblclass.mode_of_meeting = [otherInfo objectForKey:@"ModeOfMeeting"];
                      gblclass.preferred_time = [otherInfo objectForKey:@"PreferredTime"];
                      gblclass.debitcard_name_onbording = [otherInfo objectForKey:@"DebitCardName"];
                      //                      gblclass.user_name_onbording = [otherInfo objectForKey:@"fullname"];
                      //                      gblclass.cnic_onbording = [otherInfo objectForKey:@"cnic"];
                      //                      gblclass.email_onbording = [otherInfo objectForKey:@"emailaddress"];
                      //                      gblclass.mobile_number_onbording = [otherInfo objectForKey:@"mobileno"];
                      gblclass.step_no_onbording = [otherInfo objectForKey:@"StepNo"];
                      
                      
                      //                      All the data provided by customer till the stage including stepno as follows:
                      //                      {
                      //                          "Accounts":[
                      //                          {"id":"1","Name":"Current","Description":"Current Account","Document":"NIC^Passport Size Picture"}],
                      //                          "selectedproducts":[
                      //                          {"id":"1","Name":"Credit Card","Description":"Credit Card","Document":"NIC^Passport Size Picture"},
                      //                          {"id":"10","Name":"Personal Loan","Description":"Personal Loan","Document":"NIC^Passport Size Picture"}],
                      //                          "otherinfo":
                      //                          [{"strBranchCode":"08090",
                      //                              "modeofmeeting":"R",
                      //                              "preferredtime":"M",
                      //                              "debitcardname":"Customer Name",
                      //                              "stepno":"5"
                      //                              "fullname":"Customer Name",
                      //                              "cnic":"4201019874561",
                      //                              "emailaddress":"a@b.com",
                      //                              "mobileno":"03213343344"
                      //                          }]
                      //                      }
                      
                      
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"RequriedDocumentsVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                  } else {
                      [hud hideAnimated:YES];
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}


//-(void)GetTokenRequest:(NSString *)strIndustry{
//
//    @try {
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
//                                                                                       ]];//baseURL];
//
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//        manager.securityPolicy = policy;
//        manager.securityPolicy.validatesDomainName = YES;
//        manager.securityPolicy = policy;
//
//        gblclass.request_id_onbording = req_id;
//
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
//                                    [encrypt encrypt_Data:@"1234"],
//                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:req_id],  nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
//                                                                       @"strSessionId",
//                                                                       @"IP",
//                                                                       @"Device_ID",
//                                                                       @"RequestId",   nil]];
//
//        //NSLog(@"%@",dictparam);
//
//        [manager.requestSerializer setTimeoutInterval:time_out];
//        [manager POST:@"GetTokenRequest" parameters:dictparam progress:nil
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//
//                  [hud hideAnimated:YES];
//                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"]){
//
//                      gblclass.token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"Token"]];
//                      gblclass.M3sessionid = [NSString stringWithFormat:@"%@", [dic objectForKey:@"SessionId"]];
//
//                      if ([btn_chk isEqualToString:@"track"]) {
//                          [hud hideAnimated:YES];
//                          [self slide_right];
//                          storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                          vc = [storyboard instantiateViewControllerWithIdentifier:@"VerifyTrackPinVC"];
//                          [self presentViewController:vc animated:NO completion:nil];
//                      }
//                      else {
//                          [self On_Boarding_Cancle:@""];
//                      }
//
//                  } else {
//                      [hud hideAnimated:YES];
//                      // vw_JB.hidden=YES;
//                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//                  }
//              }
//
//              failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  // [mine myfaildata];
//
//                  [hud hideAnimated:YES];
//
//                  [self custom_alert:@"Try again later." :@"0"];
//
//              }];
//
//    }
//    @catch (NSException *exception)
//    {
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Try again later." :@"0"];
//    }
//
//}
//
//
//
//
//-(void)On_Boarding_Cancle:(NSString *)strIndustry{
//
//    @try {
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1
//                                                                                       ]];//baseURL];
//
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//        manager.securityPolicy = policy;
//        manager.securityPolicy.validatesDomainName = YES;
//        manager.securityPolicy = policy;
//
//        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
//                                   [NSArray arrayWithObjects:[defaults valueForKey:@"cnic"],
//                                    [encrypt encrypt_Data:gblclass.M3sessionid],
//                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
//                                    [encrypt encrypt_Data:gblclass.Udid],
//                                    [encrypt encrypt_Data:gblclass.token],
//                                    [encrypt encrypt_Data:req_id], nil]
//
//                                                              forKeys:[NSArray arrayWithObjects:@"strCNIC",
//                                                                       @"strSessionId",
//                                                                       @"IP",
//                                                                       @"Device_ID",
//                                                                       @"Token",
//                                                                       @"RequestId",   nil]];
//
//
//        //NSLog(@"%@",dictparam);
//
//        [manager.requestSerializer setTimeoutInterval:time_out];
//        [manager POST:@"CancelOnBoarding" parameters:dictparam progress:nil
//
//              success:^(NSURLSessionDataTask *task, id responseObject) {
//                  // NSError *error;
//
//                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
//
//                  if ([[dic objectForKey:@"Response"] isEqualToString:@"1"]) {
//                      [hud hideAnimated:YES];
//                      // vw_JB.hidden=YES;
//
//                      [defaults setValue:@"" forKey:@"req_id"];
//                      [defaults setValue:@"" forKey:@"apply_acct"];
//                      [defaults synchronize];
//
//                      [self Apply_acct_buttons];
//
//                  } else {
//                      [hud hideAnimated:YES];
//                      // vw_JB.hidden=YES;
//                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
//                  }
//              }
//
//              failure:^(NSURLSessionDataTask *task, NSError *error) {
//                  // [mine myfaildata];
//
//                  [hud hideAnimated:YES];
//                  [self custom_alert:@"Try again later." :@"0"];
//
//              }];
//
//    } @catch (NSException *exception) {
//
//        [hud hideAnimated:YES];
//        [self custom_alert:@"Try again later." :@"0"];
//
//    }
//
//}

-(void)NeedOnBoardHelp:(NSString *)strIndustryOmai
{
    @try {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:gblclass.mainurl1]];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        NSString*  first_time_chk =[[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"mobile_num"];
        gblclass.mobile_number_onbording = (NSString*)[encrypt de_crypt_url:[[NSUserDefaults standardUserDefaults] stringForKey:@"mobile_num"]];
        
        //   NSLog(@"%@",gblclass.mobile_number_onbording);
        
        
        gblclass.email_onbording = (NSString*)[encrypt de_crypt_url:[[NSUserDefaults standardUserDefaults] stringForKey:@"email"]];
        
        
        gblclass.cnic_onbording = (NSString*)[encrypt de_crypt_url:[[NSUserDefaults standardUserDefaults] stringForKey:@"cnic"]];
        
        gblclass.user_name_onbording = (NSString*)[encrypt de_crypt_url:[[NSUserDefaults standardUserDefaults] stringForKey:@"name"]];
        
        
        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                    [encrypt encrypt_Data:gblclass.Udid],
                                    [encrypt encrypt_Data:gblclass.token],
                                    [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                    [encrypt encrypt_Data:gblclass.email_onbording],
                                    [encrypt encrypt_Data:gblclass.request_id_onbording],
                                    [encrypt encrypt_Data:gblclass.cnic_onbording],
                                    [encrypt encrypt_Data:gblclass.user_name_onbording],
                                    [encrypt encrypt_Data:gblclass.step_no_help_onbording],nil]
                                   
                                   
                                   //        NSDictionary *dictparam = [NSDictionary dictionaryWithObjects:
                                   //                                   [NSArray arrayWithObjects:[encrypt encrypt_Data:gblclass.M3sessionid],
                                   //                                    [encrypt encrypt_Data:[GlobalStaticClass getPublicIp]],
                                   //                                    [encrypt encrypt_Data:gblclass.Udid],
                                   //                                    [encrypt encrypt_Data:gblclass.token],
                                   //                                    [encrypt encrypt_Data:gblclass.mobile_number_onbording],
                                   //                                    [encrypt encrypt_Data:gblclass.email_onbording],
                                   //                                    [encrypt encrypt_Data:gblclass.request_id_onbording],
                                   //                                    [encrypt encrypt_Data:gblclass.cnic_onbording],
                                   //                                    [encrypt encrypt_Data:gblclass.user_name_onbording],
                                   //                                    [encrypt encrypt_Data:gblclass.step_no_help_onbording],nil]
                                   
                                                              forKeys:[NSArray arrayWithObjects:@"strSessionId",
                                                                       @"IP",
                                                                       @"Device_ID",
                                                                       @"Token",
                                                                       @"strMobileNo",
                                                                       @"strEmailAddress",
                                                                       @"RequestId",
                                                                       @"strCNIC",
                                                                       @"Name",
                                                                       @"StepNo", nil]];
        
        
        //        {
        //            NoOfAttempts = "10";
        //            Response = "0";
        //            TimerLimit = "120";
        //            strReturnMessage = "message to be on alertiew";
        //        }
        
        [manager.requestSerializer setTimeoutInterval:time_out];
        [manager POST:@"NeedOnBoardHelp" parameters:dictparam progress:nil
         
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  [hud hideAnimated:YES];
                  NSError *error=nil;
                  NSDictionary *dic = (NSDictionary *)[encrypt de_crypt_Data:responseObject];
                  
                  if ([[dic objectForKey:@"Response"] isEqualToString:@"-78"] || [[dic objectForKey:@"Response"] isEqualToString:@"-79"]) {
                      
                      [hud hideAnimated:YES];
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Attention"
                                                                          message:[dic objectForKey:@"strReturnMessage"]
                                                                         delegate:self
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                      
                      [alertView show];
                      [self slide_right];
                      //                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
                      //                      [self presentViewController:vc animated:NO completion:nil];
                      
                      if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
                      {
                          stepedVC = @"login_new";
                      }
                      else
                      {
                          stepedVC = @"login";
                      }
                      
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
                      [self presentViewController:vc animated:NO completion:nil];
                      return ;
                      
                      
                      // Changed 4 Apr.
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"0"] || [[dic objectForKey:@"Response"] isEqualToString:@"-1"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      gblclass.timmerLimitOnBording = [[dic objectForKey:@"TimerLimit"] intValue];
                      [self slide_right];
                      vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
                      [self presentViewController:vc animated:NO completion:nil];
                      
                      //                      [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      
                      //                                        [self createTimer:[[dic objectForKey:@"TimerLimit"] intValue]];
                      //                                        gblclass.helpidentifier = [dic objectForKey:@"HelpIdentifier"];
                      
                  } else if ([[dic objectForKey:@"Response"] isEqualToString:@"1"] || [[dic objectForKey:@"Response"] isEqualToString:@"2"] || [[dic objectForKey:@"Response"] isEqualToString:@"4"]) {
                      gblclass.noOfPerformedAttempts++;
                      gblclass.noOfAvailableAttempts = [[dic objectForKey:@"NoOfAttempts"] intValue] -1;
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                  } else {
                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                      //                      [self slide_left];
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[dic objectForKey:@"strReturnMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                      //                      [alertView show];
                      //                      [self dismissViewControllerAnimated:NO completion:nil];
                      //                      [self custom_alert:[dic objectForKey:@"strReturnMessage"] :@"0"];
                  }
                  
                  
              } failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [hud hideAnimated:YES];
                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
                  
                  //                  [self custom_alert:@"The service is currently not available kindly try again later or call customer care at 111825888" :@"0"];
              }];
        
    } @catch (NSException *exception) {
        [hud hideAnimated:YES];
        [self custom_alert:@"Try again later." :@"0"];
    }
    
}

//////////************************** START TABBAR CONTROLS ***************************///////////

- (IBAction)showAccountStatus:(id)sender {
    [self slide_right];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"AccountStatusVC"];
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (IBAction)haveAnAccount:(id)sender {
    [self slide_right];
    
    
    if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
    {
        stepedVC = @"login_new";
    }
    else
    {
        stepedVC = @"login";
    }
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (IBAction)btn_cancel:(id)sender {
    [self custom_alert:@"Sevices Required" :@"0"];
    //    if (netAvailable)
    //    {
    //        chk_ssl=@"getTokenRequest";
    //        [self SSL_Call];
    //    }
    
}

-(IBAction)btn_back:(id)sender {
    [self slide_left];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
    if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
    {
        stepedVC = @"login_new";
    }
    else
    {
        stepedVC = @"login";
    }
    
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (IBAction)btn_next:(id)sender
{
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    [self checkinternet];
    
    //    gblclass.step_no_onbording = @"3";
    gblclass.parent_vc_onbording = @"AccountProgressVC";
    
    if (netAvailable)
    {
        chk_ssl=@"getOnBoardData";
        [self SSL_Call];
    }
}

-(void)btn_help:(id)sender
{
    if (gblclass.noOfPerformedAttempts <= gblclass.noOfAvailableAttempts)
    {
        [hud showAnimated:YES];
        [self.view addSubview:hud];
        [self checkinternet];
        
        if (netAvailable)
        {
            gblclass.selected_account_onbording = @"";
            chk_ssl = @"needOnBoardHelp";
            [self SSL_Call];
        }
        else
        {
            [hud hideAnimated:YES];
            [self custom_alert:@"Please make sure you have connected to wifi" :@"0"];
        }
        
        //        [self slide_right];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NeedHelpVC"];
        //        [self presentViewController:vc animated:NO completion:nil];
    }
    else
    {
        [self custom_alert:[NSString stringWithFormat:@"You have made %d attempts. Please contact us on 111-825-888 (UAN) or  in order to help you.", gblclass.noOfPerformedAttempts] :@"0"];
    }
}

-(IBAction)btn_feature:(id)sender
{
    NSString* first_time_chk = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"enable_touch"];
    
    if ([first_time_chk isEqualToString:@"1"] || [first_time_chk isEqualToString:@"11"])
    {
        [hud showAnimated:YES];
        //        if ([t_n_c isEqualToString:@"1"] && [gblclass.TnC_chck_On isEqualToString:@"1"])
        //        {
        [self checkinternet];
        if (netAvailable)
        {
            chk_ssl=@"qr";
            [self SSL_Call];
        }
        //        }
        //        else if ([t_n_c isEqualToString:@"0"])
        //        {
        //            [hud hideAnimated:YES];
        //            [self custom_alert:@"Please Accept Term & Condition" :@"0"];
        //        }
        //        else
        //        {
        //            [self checkinternet];
        //            if (netAvailable)
        //            {
        //                chk_ssl=@"qr";
        //                [self SSL_Call];
        //            }
        //        }
        
    }
    else
    {
        [hud hideAnimated:YES];
        [self custom_alert:@"Please register your User." :@"0"];
    }
    
}

-(IBAction)btn_offer:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
 [self peekabooSDK:@"deals"];  
    
    
    //    NSURL *jsCodeLocation;
    //
    //    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
    //    RCTRootView *rootView =
    //    [[RCTRootView alloc] initWithBundleURL : jsCodeLocation
    //                         moduleName        : @"peekaboo"
    //                         initialProperties :
    //     @{
    //       @"peekabooOwnerKey" : @"7db159dc932ec461c1a6b9c1778bb2b0",
    //       @"peekabooEnvironment" : @"production", //beta
    //       @"peekabooSdkId" : @"UBL",
    //       @"peekabooTitle" : @"UBL Offers",
    //       @"peekabooSplashImage" : @"true",
    //       @"peekabooWelcomeMessage" : @"EMPTY",
    //       @"peekabooGooglePlacesApiKey" : @"AIzaSyDFboVPDMOU0oxazlAJeOPbWRoj8zdiFuC0",
    //       @"peekabooContactUsWidget" : @"customer.services@ubl.com.pk",
    //       @"peekabooPoweredByFooter" : @"false",
    //       @"peekabooColorPrimary" : @"#0060C6",  //   004681  0060C6
    //       @"peekabooColorPrimaryDark" : @"#004681",
    //       @"peekabooColorStatus" : @"#2d2d2d",
    //       @"peekabooColorSplash" : @"#0060C6", // efefef
    //       }
    //                          launchOptions    : nil];
    //
    //
    //    vc = [[UIViewController alloc] init];
    //    vc.view = rootView;
    //    [self presentViewController:vc animated:YES completion:nil];
    
    //google api m3tech
    //AIzaSyDQrwvHcseoSB8KLGO_WYi2QnwyHEjQ1jg
    //sky
    //AIzaSyDFboVPDMOU0oxalAJeOPbWRoj8zdiFuC0
    
}

-(IBAction)btn_find_us:(id)sender
{
    gblclass.tab_bar_login_pw_check=@"login";
    
    [self peekabooSDK:@"locator"];
    
    
    
    //
    //     [self slide_right];
    //
    //    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"find_us"];
    //    [self presentViewController:vc animated:NO completion:nil];
    
}

-(IBAction)btn_faq:(id)sender
{
    [self slide_right];
    
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"faq"];
    [self presentViewController:vc animated:NO completion:nil];
}


//////////************************** END TABBAR CONTROLS ***************************///////////

#pragma mark - Check Internet -

-(void)checkinternet
{
    
    ///******************CHECK REACHIBILTY***********************
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    
    ///******************CHECK REACHIBILTY***********************
    
}

-(void) setdelegfornet: (Reachability*) curReach
{
    
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    NSString* statusString= @"";
    
    switch (netStatus)
    {
        case NotReachable:
        {
            
            //[self getinfo];
            statusString = @"Internet Access Not Available";
            netAvailable = NO;
            
            
            [self custom_alert:statusString :@"0"];
            
            //            alert  = [UIAlertController alertControllerWithTitle:@"Attention" message:statusString  preferredStyle:UIAlertControllerStyleAlert];
            //
            //            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            //            [alert addAction:ok];
            //            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            netAvailable = YES;
            //NSLog(@"net avaible WWan");
            
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            netAvailable = YES;
            //NSLog(@"net avaible");
            
            break;
        }
    }
    
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    [self setdelegfornet:curReach];
}

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(void) showAlert:(NSString *)exception : (NSString *) Title{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:Title
                                                          message:exception
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self slide_left];
        //        vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"login_new"];
        //        [self presentViewController:vc animated:NO completion:nil];
        
        if ([first_time_chk isEqualToString:@"11"] || [first_time_chk isEqualToString:@"1"])
        {
            stepedVC = @"login_new";
        }
        else
        {
            stepedVC = @"login";
        }
        
        vc = [mainStoryboard instantiateViewControllerWithIdentifier:stepedVC];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
}



- (void)peekabooSDK:(NSString *)type {
    [self presentViewController:getPeekabooUIViewController(@{
                                                              @"environment" : @"production",
                                                              @"pkbc" : @"app.com.brd",
                                                              @"type": type,
                                                              @"country": @"Pakistan",
                                                              @"userId": gblclass.peekabo_kfc_userid
                                                              }) animated:YES completion:nil];
}


@end

