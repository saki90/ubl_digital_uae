//
//  Dt2.m
//
//  Created by   on 05/04/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Dt2.h"


NSString *const kDt2ID = @"ID";
NSString *const kDt2Question = @"Question";


@interface Dt2 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Dt2

@synthesize iDProperty = _iDProperty;
@synthesize question = _question;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kDt2ID fromDictionary:dict] doubleValue];
            self.question = [self objectOrNilForKey:kDt2Question fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kDt2ID];
    [mutableDict setValue:self.question forKey:kDt2Question];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kDt2ID];
    self.question = [aDecoder decodeObjectForKey:kDt2Question];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kDt2ID];
    [aCoder encodeObject:_question forKey:kDt2Question];
}

- (id)copyWithZone:(NSZone *)zone
{
    Dt2 *copy = [[Dt2 alloc] init];
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.question = [self.question copyWithZone:zone];
    }
    
    return copy;
}


@end
