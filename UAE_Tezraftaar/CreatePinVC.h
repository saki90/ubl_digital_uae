//
//  CreatePinVC.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 21/11/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtpTextField.h"

@interface CreatePinVC : UIViewController <OtpTextFieldDelegate>

@property (strong, nonatomic) IBOutlet OtpTextField *txt_1;
@property (strong, nonatomic) IBOutlet OtpTextField *txt_2;
@property (strong, nonatomic) IBOutlet OtpTextField *txt_3;
@property (strong, nonatomic) IBOutlet OtpTextField *txt_4;
@property (strong, nonatomic) IBOutlet OtpTextField *txt_5;
@property (strong, nonatomic) IBOutlet OtpTextField *txt_6;

@property (strong, nonatomic) IBOutlet OtpTextField *txt2_1;
@property (strong, nonatomic) IBOutlet OtpTextField *txt2_2;
@property (strong, nonatomic) IBOutlet OtpTextField *txt2_3;
@property (strong, nonatomic) IBOutlet OtpTextField *txt2_4;
@property (strong, nonatomic) IBOutlet OtpTextField *txt2_5;
@property (strong, nonatomic) IBOutlet OtpTextField *txt2_6;

@end
