//
//  GnbPayAnyOneList.m
//
//  Created by   on 17/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GnbPayAnyOneList.h"


NSString *const kGnbPayAnyOneListTYPE = @"TYPE";
NSString *const kGnbPayAnyOneListACCOUNTNAME = @"ACCOUNT_NAME";
NSString *const kGnbPayAnyOneListLASTPAYDATE = @"LAST_PAY_DATE";
NSString *const kGnbPayAnyOneListBANKIMD = @"BANK_IMD";
NSString *const kGnbPayAnyOneListACCOUNTNO = @"ACCOUNT_NO";
NSString *const kGnbPayAnyOneListCDGCODE = @"CDG_CODE";
NSString *const kGnbPayAnyOneListBRANCHCODE = @"BRANCH_CODE";
NSString *const kGnbPayAnyOneListBANKCODE = @"BANK_CODE";
NSString *const kGnbPayAnyOneListBENEFICIARYID = @"BENEFICIARY_ID";
NSString *const kGnbPayAnyOneListTTACCESSKEY = @"TT_ACCESS_KEY";
NSString *const kGnbPayAnyOneListUSERACCOUNTID = @"USER_ACCOUNT_ID";
NSString *const kGnbPayAnyOneListLastPayDate1 = @"last_pay_date_1";


@interface GnbPayAnyOneList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GnbPayAnyOneList

@synthesize tYPE = _tYPE;
@synthesize aCCOUNTNAME = _aCCOUNTNAME;
@synthesize lASTPAYDATE = _lASTPAYDATE;
@synthesize bANKIMD = _bANKIMD;
@synthesize aCCOUNTNO = _aCCOUNTNO;
@synthesize cDGCODE = _cDGCODE;
@synthesize bRANCHCODE = _bRANCHCODE;
@synthesize bANKCODE = _bANKCODE;
@synthesize bENEFICIARYID = _bENEFICIARYID;
@synthesize tTACCESSKEY = _tTACCESSKEY;
@synthesize uSERACCOUNTID = _uSERACCOUNTID;
@synthesize lastPayDate1 = _lastPayDate1;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.tYPE = [self objectOrNilForKey:kGnbPayAnyOneListTYPE fromDictionary:dict];
            self.aCCOUNTNAME = [self objectOrNilForKey:kGnbPayAnyOneListACCOUNTNAME fromDictionary:dict];
            self.lASTPAYDATE = [self objectOrNilForKey:kGnbPayAnyOneListLASTPAYDATE fromDictionary:dict];
            self.bANKIMD = [[self objectOrNilForKey:kGnbPayAnyOneListBANKIMD fromDictionary:dict] doubleValue];
            self.aCCOUNTNO = [self objectOrNilForKey:kGnbPayAnyOneListACCOUNTNO fromDictionary:dict];
            self.cDGCODE = [self objectOrNilForKey:kGnbPayAnyOneListCDGCODE fromDictionary:dict];
            self.bRANCHCODE = [[self objectOrNilForKey:kGnbPayAnyOneListBRANCHCODE fromDictionary:dict] doubleValue];
            self.bANKCODE = [self objectOrNilForKey:kGnbPayAnyOneListBANKCODE fromDictionary:dict];
            self.bENEFICIARYID = [[self objectOrNilForKey:kGnbPayAnyOneListBENEFICIARYID fromDictionary:dict] doubleValue];
            self.tTACCESSKEY = [self objectOrNilForKey:kGnbPayAnyOneListTTACCESSKEY fromDictionary:dict];
            self.uSERACCOUNTID = [[self objectOrNilForKey:kGnbPayAnyOneListUSERACCOUNTID fromDictionary:dict] doubleValue];
            self.lastPayDate1 = [self objectOrNilForKey:kGnbPayAnyOneListLastPayDate1 fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.tYPE forKey:kGnbPayAnyOneListTYPE];
    [mutableDict setValue:self.aCCOUNTNAME forKey:kGnbPayAnyOneListACCOUNTNAME];
    [mutableDict setValue:self.lASTPAYDATE forKey:kGnbPayAnyOneListLASTPAYDATE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.bANKIMD] forKey:kGnbPayAnyOneListBANKIMD];
    [mutableDict setValue:self.aCCOUNTNO forKey:kGnbPayAnyOneListACCOUNTNO];
    [mutableDict setValue:self.cDGCODE forKey:kGnbPayAnyOneListCDGCODE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.bRANCHCODE] forKey:kGnbPayAnyOneListBRANCHCODE];
    [mutableDict setValue:self.bANKCODE forKey:kGnbPayAnyOneListBANKCODE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.bENEFICIARYID] forKey:kGnbPayAnyOneListBENEFICIARYID];
    [mutableDict setValue:self.tTACCESSKEY forKey:kGnbPayAnyOneListTTACCESSKEY];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uSERACCOUNTID] forKey:kGnbPayAnyOneListUSERACCOUNTID];
    [mutableDict setValue:self.lastPayDate1 forKey:kGnbPayAnyOneListLastPayDate1];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.tYPE = [aDecoder decodeObjectForKey:kGnbPayAnyOneListTYPE];
    self.aCCOUNTNAME = [aDecoder decodeObjectForKey:kGnbPayAnyOneListACCOUNTNAME];
    self.lASTPAYDATE = [aDecoder decodeObjectForKey:kGnbPayAnyOneListLASTPAYDATE];
    self.bANKIMD = [aDecoder decodeDoubleForKey:kGnbPayAnyOneListBANKIMD];
    self.aCCOUNTNO = [aDecoder decodeObjectForKey:kGnbPayAnyOneListACCOUNTNO];
    self.cDGCODE = [aDecoder decodeObjectForKey:kGnbPayAnyOneListCDGCODE];
    self.bRANCHCODE = [aDecoder decodeDoubleForKey:kGnbPayAnyOneListBRANCHCODE];
    self.bANKCODE = [aDecoder decodeObjectForKey:kGnbPayAnyOneListBANKCODE];
    self.bENEFICIARYID = [aDecoder decodeDoubleForKey:kGnbPayAnyOneListBENEFICIARYID];
    self.tTACCESSKEY = [aDecoder decodeObjectForKey:kGnbPayAnyOneListTTACCESSKEY];
    self.uSERACCOUNTID = [aDecoder decodeDoubleForKey:kGnbPayAnyOneListUSERACCOUNTID];
    self.lastPayDate1 = [aDecoder decodeObjectForKey:kGnbPayAnyOneListLastPayDate1];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_tYPE forKey:kGnbPayAnyOneListTYPE];
    [aCoder encodeObject:_aCCOUNTNAME forKey:kGnbPayAnyOneListACCOUNTNAME];
    [aCoder encodeObject:_lASTPAYDATE forKey:kGnbPayAnyOneListLASTPAYDATE];
    [aCoder encodeDouble:_bANKIMD forKey:kGnbPayAnyOneListBANKIMD];
    [aCoder encodeObject:_aCCOUNTNO forKey:kGnbPayAnyOneListACCOUNTNO];
    [aCoder encodeObject:_cDGCODE forKey:kGnbPayAnyOneListCDGCODE];
    [aCoder encodeDouble:_bRANCHCODE forKey:kGnbPayAnyOneListBRANCHCODE];
    [aCoder encodeObject:_bANKCODE forKey:kGnbPayAnyOneListBANKCODE];
    [aCoder encodeDouble:_bENEFICIARYID forKey:kGnbPayAnyOneListBENEFICIARYID];
    [aCoder encodeObject:_tTACCESSKEY forKey:kGnbPayAnyOneListTTACCESSKEY];
    [aCoder encodeDouble:_uSERACCOUNTID forKey:kGnbPayAnyOneListUSERACCOUNTID];
    [aCoder encodeObject:_lastPayDate1 forKey:kGnbPayAnyOneListLastPayDate1];
}

- (id)copyWithZone:(NSZone *)zone
{
    GnbPayAnyOneList *copy = [[GnbPayAnyOneList alloc] init];
    
    if (copy) {

        copy.tYPE = [self.tYPE copyWithZone:zone];
        copy.aCCOUNTNAME = [self.aCCOUNTNAME copyWithZone:zone];
        copy.lASTPAYDATE = [self.lASTPAYDATE copyWithZone:zone];
        copy.bANKIMD = self.bANKIMD;
        copy.aCCOUNTNO = [self.aCCOUNTNO copyWithZone:zone];
        copy.cDGCODE = [self.cDGCODE copyWithZone:zone];
        copy.bRANCHCODE = self.bRANCHCODE;
        copy.bANKCODE = [self.bANKCODE copyWithZone:zone];
        copy.bENEFICIARYID = self.bENEFICIARYID;
        copy.tTACCESSKEY = [self.tTACCESSKEY copyWithZone:zone];
        copy.uSERACCOUNTID = self.uSERACCOUNTID;
        copy.lastPayDate1 = [self.lastPayDate1 copyWithZone:zone];
    }
    
    return copy;
}


@end
