//
//  CustomTableViewCell.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 02/12/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;
- (IBAction)changeAccount:(id)sender;

@end

