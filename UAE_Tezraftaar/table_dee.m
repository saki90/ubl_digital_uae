//
//  Table2.m
//
//  Created by   on 22/02/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Table_dee.h"


NSString *const kTable2IsActive = @"Is_Active";
NSString *const kTable2FundsTransferEnabled = @"FundsTransferEnabled";
NSString *const kTable2MobileNo = @"Mobile_No";
NSString *const kTable2UserAccountRole = @"userAccountRole";
NSString *const kTable2IsEmployee = @"IsEmployee";
NSString *const kTable2IsFCY = @"isFCY";
NSString *const kTable2UserId = @"userId";
NSString *const kTable2RelationshipID = @"RelationshipID";
NSString *const kTable2IsFA = @"isFA";
NSString *const kTable2IsPBC = @"isPBC";
NSString *const kTable2IsPinValid = @"isPinValid";
NSString *const kTable2IsENRP = @"isENRP";
NSString *const kTable2FullName = @"FullName";
NSString *const kTable2IsSTAFF = @"isSTAFF";
NSString *const kTable2IsCO = @"isCO";
NSString *const kTable2StatusCode = @"StatusCode";
NSString *const kTable2LoginName = @"LoginName";
NSString *const kTable2TempMobileNo = @"tempMobileNo";
NSString *const kTable2Email = @"Email";
NSString *const kTable2UserAccountTypeCode = @"userAccountTypeCode";
NSString *const kTable2IsCCARD = @"isCCARD";
NSString *const kTable2IsCoGroupID = @"isCoGroupID";
NSString *const kTable2BOUserID = @"BO_UserID";
NSString *const kTable2IsEnrp = @"isEnrp";
NSString *const kTable2IsETA = @"isETA";
NSString *const kTable2IsORION = @"isORION";
NSString *const kTable2EmployeeNo = @"EmployeeNo";
NSString *const kTable2UserType = @"UserType";
NSString *const kTable2BaseCCY = @"baseCCY";


@interface Table_dee ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Table_dee

@synthesize isActive = _isActive;
@synthesize fundsTransferEnabled = _fundsTransferEnabled;
@synthesize mobileNo = _mobileNo;
@synthesize userAccountRole = _userAccountRole;
@synthesize isEmployee = _isEmployee;
@synthesize isFCY = _isFCY;
@synthesize userId = _userId;
@synthesize relationshipID = _relationshipID;
@synthesize isFA = _isFA;
@synthesize isPBC = _isPBC;
@synthesize isPinValid = _isPinValid;
@synthesize isENRP = _isENRP;
@synthesize fullName = _fullName;
@synthesize isSTAFF = _isSTAFF;
@synthesize isCO = _isCO;
@synthesize statusCode = _statusCode;
@synthesize loginName = _loginName;
@synthesize tempMobileNo = _tempMobileNo;
@synthesize email = _email;
@synthesize userAccountTypeCode = _userAccountTypeCode;
@synthesize isCCARD = _isCCARD;
@synthesize isCoGroupID = _isCoGroupID;
@synthesize bOUserID = _bOUserID;
@synthesize isEnrp = _isEnrp;
@synthesize isETA = _isETA;
@synthesize isORION = _isORION;
@synthesize employeeNo = _employeeNo;
@synthesize userType = _userType;
@synthesize baseCCY = _baseCCY;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isActive = [self objectOrNilForKey:kTable2IsActive fromDictionary:dict];
            self.fundsTransferEnabled = [self objectOrNilForKey:kTable2FundsTransferEnabled fromDictionary:dict];
            self.mobileNo = [self objectOrNilForKey:kTable2MobileNo fromDictionary:dict];
            self.userAccountRole = [self objectOrNilForKey:kTable2UserAccountRole fromDictionary:dict];
            self.isEmployee = [self objectOrNilForKey:kTable2IsEmployee fromDictionary:dict];
            self.isFCY = [self objectOrNilForKey:kTable2IsFCY fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kTable2UserId fromDictionary:dict];
            self.relationshipID = [self objectOrNilForKey:kTable2RelationshipID fromDictionary:dict];
            self.isFA = [self objectOrNilForKey:kTable2IsFA fromDictionary:dict];
            self.isPBC = [self objectOrNilForKey:kTable2IsPBC fromDictionary:dict];
            self.isPinValid = [self objectOrNilForKey:kTable2IsPinValid fromDictionary:dict];
            self.isENRP = [self objectOrNilForKey:kTable2IsENRP fromDictionary:dict];
            self.fullName = [self objectOrNilForKey:kTable2FullName fromDictionary:dict];
            self.isSTAFF = [self objectOrNilForKey:kTable2IsSTAFF fromDictionary:dict];
            self.isCO = [self objectOrNilForKey:kTable2IsCO fromDictionary:dict];
            self.statusCode = [self objectOrNilForKey:kTable2StatusCode fromDictionary:dict];
            self.loginName = [self objectOrNilForKey:kTable2LoginName fromDictionary:dict];
            self.tempMobileNo = [self objectOrNilForKey:kTable2TempMobileNo fromDictionary:dict];
            self.email = [self objectOrNilForKey:kTable2Email fromDictionary:dict];
            self.userAccountTypeCode = [self objectOrNilForKey:kTable2UserAccountTypeCode fromDictionary:dict];
            self.isCCARD = [self objectOrNilForKey:kTable2IsCCARD fromDictionary:dict];
            self.isCoGroupID = [self objectOrNilForKey:kTable2IsCoGroupID fromDictionary:dict];
            self.bOUserID = [self objectOrNilForKey:kTable2BOUserID fromDictionary:dict];
            self.isEnrp = [self objectOrNilForKey:kTable2IsEnrp fromDictionary:dict];
            self.isETA = [self objectOrNilForKey:kTable2IsETA fromDictionary:dict];
            self.isORION = [self objectOrNilForKey:kTable2IsORION fromDictionary:dict];
            self.employeeNo = [self objectOrNilForKey:kTable2EmployeeNo fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kTable2UserType fromDictionary:dict];
            self.baseCCY = [self objectOrNilForKey:kTable2BaseCCY fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.isActive forKey:kTable2IsActive];
    [mutableDict setValue:self.fundsTransferEnabled forKey:kTable2FundsTransferEnabled];
    [mutableDict setValue:self.mobileNo forKey:kTable2MobileNo];
    [mutableDict setValue:self.userAccountRole forKey:kTable2UserAccountRole];
    [mutableDict setValue:self.isEmployee forKey:kTable2IsEmployee];
    [mutableDict setValue:self.isFCY forKey:kTable2IsFCY];
    [mutableDict setValue:self.userId forKey:kTable2UserId];
    [mutableDict setValue:self.relationshipID forKey:kTable2RelationshipID];
    [mutableDict setValue:self.isFA forKey:kTable2IsFA];
    [mutableDict setValue:self.isPBC forKey:kTable2IsPBC];
    [mutableDict setValue:self.isPinValid forKey:kTable2IsPinValid];
    [mutableDict setValue:self.isENRP forKey:kTable2IsENRP];
    [mutableDict setValue:self.fullName forKey:kTable2FullName];
    [mutableDict setValue:self.isSTAFF forKey:kTable2IsSTAFF];
    [mutableDict setValue:self.isCO forKey:kTable2IsCO];
    [mutableDict setValue:self.statusCode forKey:kTable2StatusCode];
    [mutableDict setValue:self.loginName forKey:kTable2LoginName];
    [mutableDict setValue:self.tempMobileNo forKey:kTable2TempMobileNo];
    [mutableDict setValue:self.email forKey:kTable2Email];
    [mutableDict setValue:self.userAccountTypeCode forKey:kTable2UserAccountTypeCode];
    [mutableDict setValue:self.isCCARD forKey:kTable2IsCCARD];
    [mutableDict setValue:self.isCoGroupID forKey:kTable2IsCoGroupID];
    [mutableDict setValue:self.bOUserID forKey:kTable2BOUserID];
    [mutableDict setValue:self.isEnrp forKey:kTable2IsEnrp];
    [mutableDict setValue:self.isETA forKey:kTable2IsETA];
    [mutableDict setValue:self.isORION forKey:kTable2IsORION];
    [mutableDict setValue:self.employeeNo forKey:kTable2EmployeeNo];
    [mutableDict setValue:self.userType forKey:kTable2UserType];
    [mutableDict setValue:self.baseCCY forKey:kTable2BaseCCY];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.isActive = [aDecoder decodeObjectForKey:kTable2IsActive];
    self.fundsTransferEnabled = [aDecoder decodeObjectForKey:kTable2FundsTransferEnabled];
    self.mobileNo = [aDecoder decodeObjectForKey:kTable2MobileNo];
    self.userAccountRole = [aDecoder decodeObjectForKey:kTable2UserAccountRole];
    self.isEmployee = [aDecoder decodeObjectForKey:kTable2IsEmployee];
    self.isFCY = [aDecoder decodeObjectForKey:kTable2IsFCY];
    self.userId = [aDecoder decodeObjectForKey:kTable2UserId];
    self.relationshipID = [aDecoder decodeObjectForKey:kTable2RelationshipID];
    self.isFA = [aDecoder decodeObjectForKey:kTable2IsFA];
    self.isPBC = [aDecoder decodeObjectForKey:kTable2IsPBC];
    self.isPinValid = [aDecoder decodeObjectForKey:kTable2IsPinValid];
    self.isENRP = [aDecoder decodeObjectForKey:kTable2IsENRP];
    self.fullName = [aDecoder decodeObjectForKey:kTable2FullName];
    self.isSTAFF = [aDecoder decodeObjectForKey:kTable2IsSTAFF];
    self.isCO = [aDecoder decodeObjectForKey:kTable2IsCO];
    self.statusCode = [aDecoder decodeObjectForKey:kTable2StatusCode];
    self.loginName = [aDecoder decodeObjectForKey:kTable2LoginName];
    self.tempMobileNo = [aDecoder decodeObjectForKey:kTable2TempMobileNo];
    self.email = [aDecoder decodeObjectForKey:kTable2Email];
    self.userAccountTypeCode = [aDecoder decodeObjectForKey:kTable2UserAccountTypeCode];
    self.isCCARD = [aDecoder decodeObjectForKey:kTable2IsCCARD];
    self.isCoGroupID = [aDecoder decodeObjectForKey:kTable2IsCoGroupID];
    self.bOUserID = [aDecoder decodeObjectForKey:kTable2BOUserID];
    self.isEnrp = [aDecoder decodeObjectForKey:kTable2IsEnrp];
    self.isETA = [aDecoder decodeObjectForKey:kTable2IsETA];
    self.isORION = [aDecoder decodeObjectForKey:kTable2IsORION];
    self.employeeNo = [aDecoder decodeObjectForKey:kTable2EmployeeNo];
    self.userType = [aDecoder decodeObjectForKey:kTable2UserType];
    self.baseCCY = [aDecoder decodeObjectForKey:kTable2BaseCCY];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_isActive forKey:kTable2IsActive];
    [aCoder encodeObject:_fundsTransferEnabled forKey:kTable2FundsTransferEnabled];
    [aCoder encodeObject:_mobileNo forKey:kTable2MobileNo];
    [aCoder encodeObject:_userAccountRole forKey:kTable2UserAccountRole];
    [aCoder encodeObject:_isEmployee forKey:kTable2IsEmployee];
    [aCoder encodeObject:_isFCY forKey:kTable2IsFCY];
    [aCoder encodeObject:_userId forKey:kTable2UserId];
    [aCoder encodeObject:_relationshipID forKey:kTable2RelationshipID];
    [aCoder encodeObject:_isFA forKey:kTable2IsFA];
    [aCoder encodeObject:_isPBC forKey:kTable2IsPBC];
    [aCoder encodeObject:_isPinValid forKey:kTable2IsPinValid];
    [aCoder encodeObject:_isENRP forKey:kTable2IsENRP];
    [aCoder encodeObject:_fullName forKey:kTable2FullName];
    [aCoder encodeObject:_isSTAFF forKey:kTable2IsSTAFF];
    [aCoder encodeObject:_isCO forKey:kTable2IsCO];
    [aCoder encodeObject:_statusCode forKey:kTable2StatusCode];
    [aCoder encodeObject:_loginName forKey:kTable2LoginName];
    [aCoder encodeObject:_tempMobileNo forKey:kTable2TempMobileNo];
    [aCoder encodeObject:_email forKey:kTable2Email];
    [aCoder encodeObject:_userAccountTypeCode forKey:kTable2UserAccountTypeCode];
    [aCoder encodeObject:_isCCARD forKey:kTable2IsCCARD];
    [aCoder encodeObject:_isCoGroupID forKey:kTable2IsCoGroupID];
    [aCoder encodeObject:_bOUserID forKey:kTable2BOUserID];
    [aCoder encodeObject:_isEnrp forKey:kTable2IsEnrp];
    [aCoder encodeObject:_isETA forKey:kTable2IsETA];
    [aCoder encodeObject:_isORION forKey:kTable2IsORION];
    [aCoder encodeObject:_employeeNo forKey:kTable2EmployeeNo];
    [aCoder encodeObject:_userType forKey:kTable2UserType];
    [aCoder encodeObject:_baseCCY forKey:kTable2BaseCCY];
}

- (id)copyWithZone:(NSZone *)zone
{
    Table_dee *copy = [[Table_dee alloc] init];
    
    if (copy) {

        copy.isActive = [self.isActive copyWithZone:zone];
        copy.fundsTransferEnabled = [self.fundsTransferEnabled copyWithZone:zone];
        copy.mobileNo = [self.mobileNo copyWithZone:zone];
        copy.userAccountRole = [self.userAccountRole copyWithZone:zone];
        copy.isEmployee = [self.isEmployee copyWithZone:zone];
        copy.isFCY = [self.isFCY copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.relationshipID = [self.relationshipID copyWithZone:zone];
        copy.isFA = [self.isFA copyWithZone:zone];
        copy.isPBC = [self.isPBC copyWithZone:zone];
        copy.isPinValid = [self.isPinValid copyWithZone:zone];
        copy.isENRP = [self.isENRP copyWithZone:zone];
        copy.fullName = [self.fullName copyWithZone:zone];
        copy.isSTAFF = [self.isSTAFF copyWithZone:zone];
        copy.isCO = [self.isCO copyWithZone:zone];
        copy.statusCode = [self.statusCode copyWithZone:zone];
        copy.loginName = [self.loginName copyWithZone:zone];
        copy.tempMobileNo = [self.tempMobileNo copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.userAccountTypeCode = [self.userAccountTypeCode copyWithZone:zone];
        copy.isCCARD = [self.isCCARD copyWithZone:zone];
        copy.isCoGroupID = [self.isCoGroupID copyWithZone:zone];
        copy.bOUserID = [self.bOUserID copyWithZone:zone];
        copy.isEnrp = [self.isEnrp copyWithZone:zone];
        copy.isETA = [self.isETA copyWithZone:zone];
        copy.isORION = [self.isORION copyWithZone:zone];
        copy.employeeNo = [self.employeeNo copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.baseCCY = [self.baseCCY copyWithZone:zone];
    }
    
    return copy;
}


@end
