//
//  SignUpWithAtmCardViewController.h
//  ubltestbanking
//
//  Created by Asim Raza Khan on 15/06/2017.
//  Copyright © 2017 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface SignUpWithAtmCardViewController : UIViewController
{
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
}
@end

