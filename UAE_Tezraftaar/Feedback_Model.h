//
//  Feedback_Model.h
//
//  Created by   on 14/03/2016
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Feedback_Model : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *fResponse;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
