//
//  ManagePayee.h
//  ubltestbanking
//
//  Created by ammar on 13/04/2016.
//  Copyright © 2016 ammar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "TransitionDelegate.h"

@interface ManagePayee : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    // IBOutlet UITableView* payeelisttable;
    
    
    
    //**********Reachability*********
    Reachability* internetReach;
    BOOL netAvailable;
    //**********Reachability*********
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@property (weak, nonatomic)  IBOutlet UITableView* payeelisttable;

-(IBAction)btn_Pay:(id)sender;
-(IBAction)btn_back:(id)sender;



@end

