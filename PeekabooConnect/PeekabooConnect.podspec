Pod::Spec.new do |s|
  s.name           = "PeekabooConnect"
  s.version        = "2.0.0"
  s.summary        = "Empowering Digital Experiences"
  s.description    = "Digital Experiences for fintech"
  s.license        = ""
  s.author         = "Fetch Sky Technologies"
  s.homepage       = "https://fetchsky.com"
  s.platform       = :ios, "7.0"
  s.source         = { :git => "https://github.com/fetchsky/peekaboo-connect-sdk.git", :tag => "master" }
  s.source_files   = "*.{h,m}"
  s.requires_arc   = true
  s.vendored_frameworks = "PeekabooConnect.framework"
  s.resource       = "PeekabooConnect.bundle"
end
